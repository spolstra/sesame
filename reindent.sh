#!/bin/bash

find . -type f \( -name "*.cpp" -o -name "*.c" -o -name "*.h" \) | xargs astyle --options=none --style=kr --indent=spaces=4 --indent-classes --indent-switches --indent-namespaces --indent-preprocessor --min-conditional-indent=0 --break-blocks --pad-oper --pad-header --unpad-paren --add-brackets --convert-tabs --align-pointer=name --indent-col1-comments --align-reference=name
find . -name "*.*.orig" | xargs rm

