dnl - Determine where and which version of xerces is installed on the system
dnl - Author: Joseph Coffland jcoffland@xsmail.com (2003-2004)

AC_DEFUN([AC_CHECK_XERCES],[
    MAJOR=$1
    MINOR=$2

    if test "$MAJOR" = ""; then MAJOR=0; fi
    if test "$MINOR" = ""; then MINOR=0; fi

    XERCESC_ERROR_MSG="
    You must have Xerces version $MAJOR.$MINOR or greater
    installed and set XERCESCROOT or use --with-xercesc.
    It may also be necessary to set your library path if
    your system does not support the -rpath link option.
    (i.e. LD_LIBRARY_PATH=/path/to/xercesc/lib)"

    has_xerces=false

    CFLAGS=${CFLAGS--O}

    AC_ARG_WITH(xercesc,
        [--with-xercesc=dir      Specify the Xerces directory],
        [
            if test x$withval != x; then
                XERCESCROOT=$withval
            fi
        ])

    if test "$LD_HAS_RPATH" == ""; then
        AC_LD_HAS_RPATH
    fi

    if test "$XERCESCROOT" != ""; then
        XERCESC_INCLUDE_DIR=$XERCESCROOT/include
        XERCESC_INCLUDE=-I$XERCESC_INCLUDE_DIR
        XERCESC_LIB_DIR=$XERCESCROOT/lib
                XERCESC_LIBS=-L$XERCESC_LIB_DIR
                if test $LD_HAS_RPATH == true; then
                  XERCESC_LIBS="-Wl,-rpath -Wl,$XERCESC_LIB_DIR $XERCESC_LIBS"
                fi
    fi
    XERCESC_LIBS="$XERCESC_LIBS -lxerces-c -lpthread"

    AC_MSG_CHECKING(for xerces-c version $MAJOR.$MINOR or greater)

    AC_LANG_PUSH(C++)

    save_libs=$LIBS
    save_cxxflags=$CXXFLAGS

    LIBS="$XERCESC_LIBS $LIBS"
    CPPFLAGS="$XERCESC_INCLUDE $CPPFLAGS"
        
    AC_LINK_IFELSE(
        [AC_LANG_PROGRAM([
            #include <xercesc/util/XercesVersion.hpp>
            #include <stdio.h>
        ],[
            printf("%d.%d.%d\n", XERCES_VERSION_MAJOR, XERCES_VERSION_MINOR,
                                 XERCES_VERSION_REVISION);
            return 0;
        ])],
        [
            XERCESC_VER=`./conftest${ac_exeext}`
            if test $? -ne 0 ; then AC_MSG_ERROR($XERCESC_ERROR_MSG); fi
        ],
        [AC_MSG_ERROR($XERCESC_ERROR_MSG)]
    )

    LIBS=$save_libs
    CXXFLAGS=$save_cxxflags

    AC_LANG_POP(C++)


    XERCESC_VER_MAJOR=`echo $XERCESC_VER | sed -e 's/\(.*\)\.\(.*\)\.\(.*\)/\1/'`
    XERCESC_VER_MINOR=`echo $XERCESC_VER | sed -e 's/\(.*\)\.\(.*\)\.\(.*\)/\2/'`
    XERCESC_VER_REVISION=`echo $XERCESC_VER | sed -e 's/\(.*\)\.\(.*\)\.\(.*\)/\3/'`

    correct_xerces_version=false
        if test "${XERCESC_VER_MAJOR}" -gt "${MAJOR}"; then
        correct_xerces_version=true
    fi
        if test "${XERCESC_VER_MAJOR}" -eq "${MAJOR}"; then
            if test ${XERCESC_VER_MINOR} -ge ${MINOR}; then
            correct_xerces_version=true
        fi
    fi

    if test "${correct_xerces_version}" = "true"; then
        AC_MSG_RESULT([found version ${XERCESC_VER}])
    else
        AC_MSG_ERROR([WRONG version ${XERCESC_VER} 
                     ${XERCESC_ERROR_MSG}])
    fi

    AC_SUBST(XERCESC_LIB_DIR)
    AC_SUBST(XERCESC_LIBS)
    AC_SUBST(XERCESC_INCLUDE)
    AC_SUBST(XERCESC_INCLUDE_DIR)
    AC_SUBST(XERCESC_VER)
    AC_SUBST(XERCESCROOT)
])
