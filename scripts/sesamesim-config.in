#!/bin/sh

prefix=@prefix@
exec_prefix=@exec_prefix@
exec_prefix_set=no

usage()
{
  cat <<EOF
Usage: @PACKAGE@-config [OPTIONS]
Options:
  [--prefix[=DIR]]
  [--exec-prefix[=DIR]]
  [--version]
  [--build_os]
  [--build_cpu]
  [--ymlpearl-libs]
  [--ymlpearl-cflags]
  [--ymlpearl-path]
  [--PNRunner-libs]
  [--PNRunner-cflags]
EOF
  exit $1
}

if test $# -eq 0; then
  usage 1 1>&2
fi

while test $# -gt 0; do
  case "$1" in
  -*=*) optarg=`echo "$1" | sed 's/[-_a-zA-Z0-9]*=//'` ;;
  *) optarg= ;;
  esac

  case $1 in
    --prefix=*)
      prefix=$optarg
      if test $exec_prefix_set = no ; then
        exec_prefix=$optarg
      fi
      ;;
    --prefix)
      echo_prefix="yes"
      ;;
    --exec-prefix=*)
      exec_prefix=$optarg
      exec_prefix_set="yes"
      ;;
    --exec-prefix)
      echo_exec_prefix="yes"
      ;;
    --version)
      echo @VERSION@
      exit 0
      ;;
    --build_os)
      echo @target_os@
      exit 0
      ;;
    --build_cpu)
      echo @target_cpu@
      exit 0
      ;;
    --ymlpearl-cflags)
      echo_ymlpearl_cflags="yes"
      ;;
    --ymlpearl-libs)
      echo_ymlpearl_libs="yes"
      ;;
    --ymlpearl-path)
      echo_ymlpearl_path="yes"
      ;;
    --PNRunner-cflags)
      echo_PNRunner_cflags="yes"
      ;;
    --PNRunner-libs)
      echo_PNRunner_libs="yes"
      ;;
    *)
      usage 1 1>&2
      ;;
  esac
  shift
done

if test "$echo_prefix" = "yes"; then
  echo $prefix
fi
if test "$echo_exec_prefix" = "yes"; then
  echo $exec_prefix
fi

if test "$echo_ymlpearl_cflags" = "yes"; then
  echo -I@includedir@
fi
if test "$echo_ymlpearl_libs" = "yes"; then
  ymlpearl_LIBS="-L@libdir@/@PACKAGE@ -lymlpearl"
        if [ "@LD_HAS_RPATH@" = "true" ]; then
          ymlpearl_LIBS="-Wl,-rpath -Wl,@libdir@/@PACKAGE@ $ymlpearl_LIBS"
        fi

  echo $ymlpearl_LIBS
fi
if test "$echo_ymlpearl_path" = "yes"; then
  echo "@libdir@/@PACKAGE@"
fi

if test "$echo_PNRunner_cflags" = "yes"; then
  case "@target_os@" in
    *solaris*) 
      oscflags="-DSOLARIS -pthreads -fpic" ;;
    *linux*)
      oscflags="-DLINUX -pthread -DPIC -fPIC";;
    *) ;;
  esac

  echo ${oscflags} -I@includedir@/yapi -I@includedir@
fi

if test "$echo_PNRunner_libs" = "yes"; then
  case @target_os@ in
  	*darwin*) oslibs="-bundle -undefined dynamic_lookup" ;;
    *solaris*) oslibs="-G" ;;
        *linux*) oslibs="-shared" ;;
    *) ;;
  esac

  echo ${oslibs}
fi
