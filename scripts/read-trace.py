import sys
import struct

# execute types
execute = 1
read = 2
write = 3
endoftrace = 65535

# event size constants
fieldsize = 2
nrfields = 4
tidsize = 4
annotsize = 4 + 4  # two annotation fields

if len(sys.argv) != 2:
    print "usage %s <trace-file>\n" % sys.argv[0]
    sys.exit(2)

with open (sys.argv[1], "rb") as f:
    while True:
        data = f.read(fieldsize * nrfields + annotsize + tidsize) # read one event
        if data == b"": # stop if EOF
            break

        # unpack into fields and print
        etype, operid, size, commchan, tid, annot, annot2 = \
                struct.unpack('!HHHHIII', data)
        print "%5d %5d %5d %5d %10d %10d %10d" % (etype, operid, size,
                                             commchan, tid, annot, annot2)

        # Do some sanity checking
        assert (etype == endoftrace or (etype >= execute and etype <= write))
        if etype == execute:
            assert (size == 0 and commchan == 0)

