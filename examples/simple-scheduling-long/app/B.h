#ifndef B_H
#define B_H

#include "B_Base.h"

class B : public B_Base
{
    public:
        B(Id n, B_Ports *ports);
        virtual ~B();

        void main();
};
#endif // B_H
