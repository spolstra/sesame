
#include "C.h"

#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

C::C(Id n, C_Ports *ports) : C_Base(n, ports) {}
C::~C() {}

void C::main() {
    int x;
    int count = 0;
    int arg = atoi(getArgv()[1]);
    int print_interval = arg/10;


    while (true) {
        ports->in0.read(x);
        //cout << getFullName() << " received " << x << endl;
        count++;

        ports->in1.read(x);
        //cout << getFullName() << " received " << x << endl;
        count++;
        execute("op_EndLoop");
        if (count % print_interval == 0)
            cout << getFullName() << "     received " << count << " items" << endl;
    }
}
