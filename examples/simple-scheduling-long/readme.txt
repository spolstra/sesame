Simple static scheduling example
--------------------------------
The simple-static example shows the use of the static scheduling
policy on a simple three task application.
 
Task A and task B each write 10 integers to task C. Task A always
writes the value 1 and B always writes the value 2.

                   1,1..
     +------------------------------------+
     |                                    v
+----+---+         +--------+  2,2.. +----+---+
|   A(1) |         |   B(2) +------->+   C(3) |
|        |         |        |        |        |
+--------+         +--------+        +--------+
    \             /                    /   
     \           /                    /     
 w(1),w(1)..   w(2),w(2)..        r(A),r(B),r(A).. 
       \       /                   /     
   +------------+        +------------+    
   |   sched 0  |        |    sched 1 |
   |            |        |            |
   +-----+------+        +-----+------+
         |                     |
 w(1),w(2),w(1),w(2)..   r(A),r(B),r(A),r(B)..
         |                     |
         v                     v
   +------------+        +------------+    
   |   Proc X   |        |   Proc Y   |
   |            |        |            |
   +-----+------+        +-----+------+
         |                     |      
   ------+-----------+---------+-------
                     |                
             +-------+------+         
             |    Memory    |         
             |              |         
             +--------------+       

sched0 will impose the scheduling policy that you give it.
Here we schedule the events from task A and B according to a
static schedule.


The picture above schedules according to the static schedule:
[1,2]
This means: first a loop iteration from 1 (task A) following by a loop
iteration from 2 (task B).

Processor X will handle the writes in the scheduled order:

arch: application.architecture.X: in write size=4 time=0 chid=0 vid=3
arch: application.architecture.X: in write size=4 time=1 chid=0 vid=4
arch: application.architecture.X: in write size=4 time=3 chid=0 vid=3
arch: application.architecture.X: in write size=4 time=5 chid=0 vid=4
[..]

vid 3 is task A and vid 4 is task B.

But we can also use a more complicated schedule such as:
[1,1,2,1,1,2,2,1,2,2]

sched1 for Proc Y is simple, because there is only task C (with task
number 3) mapped onto it:
[3] 

Scheduling Policies
-------------------

vproc_sched.pi:
policy_object.pi:   Policy base class
glob_sched.pi:      Contains the event structure and event queue.
hwscheduler.pi:
fcfs.pi:
static.pi:

