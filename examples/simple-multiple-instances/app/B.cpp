#include "B.h"

#include <iostream>
using namespace std;

#define NPORTS 4

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int sum;
    int x[NPORTS];

    while (true) {
        ports->in0.read(x[0]);
        ports->in1.read(x[1]);
        ports->in2.read(x[2]);
        ports->in3.read(x[3]);
        for (int i = 0; i < NPORTS; i++) {
            // cout << getFullName() << " received " << x[i] << endl;
            sum += x[i];
        }
        cout << getFullName() << " Total: " << sum << endl;
    }
}
