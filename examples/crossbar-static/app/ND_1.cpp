// File automatically generated by ESPAM

#include "ND_1.h"

ND_1::ND_1(Id n, ND_1_Ports *ports) : ND_1_Base(n, ports) {}
ND_1::~ND_1() {}

void ND_1::_mainVideoIn( struct TBlocks &out_0 ) {
    mainVideoIn( &out_0 );
}


void ND_1::main() {

    // Input Arguments 

    // Output Arguments 
    struct TBlocks out_0ND_1;

    for( int c0 =  ceil1(0); c0 <=  floor1(7); c0 += 1 ) {
      for( int c1 =  ceil1(0); c1 <=  floor1(15); c1 += 1 ) {
        for( int c2 =  ceil1(0); c2 <=  floor1(7); c2 += 1 ) {

            _mainVideoIn(out_0ND_1) ;
            execute("op_mainVideoIn");


            ports->OG_1.write( out_0ND_1 );

// line automatically added by ../bin/patch_appyml.pl to accomodate static schedule modeling:

execute("op_EndLoop");
        } // for c2
      } // for c1
    } // for c0
} // main
