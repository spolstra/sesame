#include "B.h"
#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int i;
    int data = 2;
    int count = atoi(getArgv()[1]);

    for (i = 0; i < count; i++) {
        ports->out0.write(data);
        cout << getFullName() << "     sent " << data << endl;
        execute("op_EndLoop");
    }
}
