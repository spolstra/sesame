#ifdef VERBOSE
#       define VP(s)    (printf s) ;
#else
#       define VP(s)    ;
#endif

#define assert(E, M)   (if (!(E)) { printf("*** Assertion failed: ***\n"); printf M; printf("\n"); exit(2)})

