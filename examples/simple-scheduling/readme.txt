Simple static scheduling example
--------------------------------
The simple-static example shows the use of the static scheduling
policy on a simple three task application.
 
Task A and task B each write 10 integers to task C. Task A always
writes the value 1 and B always writes the value 2.

                   1,1..
     +------------------------------------+
     |                                    v
+----+---+         +--------+  2,2.. +----+---+
|   A(1) |         |   B(2) +------->+   C(3) |
|        |         |        |        |        |
+--------+         +--------+        +--------+
    \             /                    /   
     \           /                    /     
 w(1),w(1)..   w(2),w(2)..        r(A),r(B),r(A).. 
       \       /                   /     
   +------------+        +------------+    
   |   sched 0  |        |    sched 1 |
   |            |        |            |
   +-----+------+        +-----+------+
         |                     |
 w(1),w(2),w(1),w(2)..   r(A),r(B),r(A),r(B)..
         |                     |
         v                     v
   +------------+        +------------+    
   |   Proc X   |        |   Proc Y   |
   |            |        |            |
   +-----+------+        +-----+------+
         |                     |      
   ------+-----------+---------+-------
                     |                
             +-------+------+         
             |    Memory    |         
             |              |         
             +--------------+       

sched0 will impose the scheduling policy that you give it.
Here we schedule the events from task A and B according to a
static schedule.


The picture above schedules according to the static schedule:
[1,2]
This means: first a loop iteration from 1 (task A) following by a loop
iteration from 2 (task B).

Processor X will handle the writes in the scheduled order:

arch: application.architecture.X: in write size=4 time=0 chid=0 vid=3
arch: application.architecture.X: in write size=4 time=1 chid=0 vid=4
arch: application.architecture.X: in write size=4 time=3 chid=0 vid=3
arch: application.architecture.X: in write size=4 time=5 chid=0 vid=4
[..]

vid 3 is task A and vid 4 is task B.

But we can also use a more complicated schedule such as:
[1,1,2,1,1,2,2,1,2,2]

sched1 for Proc Y is simple, because there is only task C (with task
number 3) mapped onto it:
[3] 

Scheduling Policies
-------------------
static: checked, and works
roundrobin: executes but results seem wrong

Files
-----
vproc_sched.pi:
policy_object.pi:   Policy base class
glob_sched.pi:      Contains the event structure and event queue.
hwscheduler.pi:
fcfs.pi:
static.pi:

Summary
-------
Refer to picture nb-p.54
And notes nb-p.76

vproc_sched calls schedule_event() with parameters:
(src)this:  Pearl component id.
sched_id:   Unique identifier taken from the global counter unique_id.
vp_id:      virtual_layer_gen replaces ID marker in vproc with `taskid`
            taken from mapped task in app yml.
event_type: numbers as defined in `event.h` E,R,W == 1,2,3
            ENDLOOP == 0, NO_EVENT == -1, QUIT == -1.
chid:(*)    This id is negotiated at startup. Used as a lookup in the
            ports array so the processor will use the correct
            communication channel (eg. bus, fifo..)
tracecommid:(*) Virtual channel identifier, used for routing the
             read/write messages over bus/crossbar
tracesize:(*) Size of the data transferred. Used to model latency in the
            architecture.

(*) = data sent the underlying processor.

Q:
Why do we need both sched_id and vp_id?

An event contains the following fields:
event = { event_type : integer,
          chid : integer,  (*)
          vid : integer,   (*)
          size : integer,  (*)
          src_id : integer,  (==sched_id in vproc_sched, from unique cnter)
          vp_id : integer
}
event_q  = [MAX_EVENTS] event

wiki quote on round-robin scheduling:
"""
In best-effort packet switching and other statistical multiplexing,
round-robin scheduling can be used as an alternative to first-come
first-served queuing.

A multiplexer, switch, or router that provides round-robin scheduling
has a separate queue for every data flow, where a data flow may be
identified by its source and destination address. The algorithm lets
every active data flow that has data packets in the queue to take
turns in transferring packets on a shared channel in a periodically
repeated order. The scheduling is work-conserving, meaning that if one
flow is out of packets, the next data flow will take its place. Hence,
the scheduling tries to prevent link resources from going unused.

Round-robin scheduling results in max-min fairness if the data packets
are equally sized, since the data flow that has waited the longest
time is given scheduling priority. It may not be desirable if the size
of the data packets varies widely from one job to another. A user that
produces large packets would be favored over other users. In that case
fair queuing would be preferable.

If guaranteed or differentiated quality of service is offered, and not
only best-effort communication, deficit round-robin (DRR) scheduling,
weighted round-robin (WRR) scheduling, or weighted fair queuing (WFQ)
may be considered.

In multiple-access networks, where several terminals are connected to
a shared physical medium, round-robin scheduling may be provided by
token passing channel access schemes such as token ring, or by polling
or resource reservation from a central control station.

In a centralized wireless packet radio network, where many stations
share one frequency channel, a scheduling algorithm in a central base
station may reserve time slots for the mobile stations in a
round-robin fashion and provide fairness. However, if link adaptation
is used, it will take a much longer time to transmit a certain amount
of data to "expensive" users than to others since the channel
conditions differ. It would be more efficient to wait with the
transmission until the channel conditions are improved, or at least to
give scheduling priority to less expensive users. Round-robin
scheduling does not utilize this. Higher throughput and system
spectrum efficiency may be achieved by channel-dependent scheduling,
for example a proportionally fair algorithm, or maximum throughput
scheduling. Note that the latter is characterized by undesirable
scheduling starvation. This type of scheduling is one of the very
basic algorithms for Operating Systems in computers which can be
implemented through circular queue Data Structure.
"""
