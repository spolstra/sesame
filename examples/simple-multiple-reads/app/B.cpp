#include "B.h"

#include <iostream>

#define RECV_BUF_SIZE 10
using namespace std;

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int a[RECV_BUF_SIZE];

    cout << "Read in buffer of " << RECV_BUF_SIZE << " elements" << endl;
    ports->in0.read(a, 1, 2, 3, RECV_BUF_SIZE);
    cout << "Printing receiver buffer" << endl;
    for (int i = 0; i < RECV_BUF_SIZE; i++) {
        cout << getFullName() << " received " << a[i] << endl;
    }
}
