#include "A.h"

#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

#define SEND_BUF_SIZE 4

A::A(Id n, A_Ports *ports) : A_Base(n, ports) {}
A::~A() {}

void A::main()
{
    int i;
    int send_buf[SEND_BUF_SIZE];
    int count = atoi(getArgv()[1]);

    // Send items one buffer at a time.
    for (i = 0; i < count/SEND_BUF_SIZE; i++) {
        // fill send buffer
        for (int b = 0; b < SEND_BUF_SIZE; b++) {
            send_buf[b] = i * SEND_BUF_SIZE + b;
        }
        ports->out0.write(send_buf, 10, 20, 30, SEND_BUF_SIZE);
        cout << getFullName() << " sent " << i << " buffer of size " <<
            SEND_BUF_SIZE << endl;
    }

    // Less than 1 buffer full remaining. Send these remaining items.
    for (i = 0; i < count % SEND_BUF_SIZE; i++) {
        int item = SEND_BUF_SIZE * (count / SEND_BUF_SIZE) + i;
        ports->out0.write(item);
        cout << getFullName() << " sent " << i << " single item" << endl;
    }
}
