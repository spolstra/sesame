echo "[3 channel active application]"
rm -f app
ln -sf app-3channel app
echo "use mem0, mem and fifo"
ln -sf simple_map_3channel.yml simple_map.yml
make run | grep Elapsed
make clean > /dev/null
echo

echo "use self-loop fifos"
ln -sf simple_map_3loop.yml simple_map.yml
make run | grep Elapsed
make clean > /dev/null
echo

echo "[1 channel active application]"
rm -f app
ln -sf app-1channel app

echo "all channels on mem0"
ln -fs simple_map_mem0.yml simple_map.yml
make run | grep Elapsed
echo

echo "all channels on mem"
ln -fs simple_map_mem.yml simple_map.yml
make run | grep Elapsed
echo

echo "all channels on fifo"
ln -fs simple_map_fifo.yml simple_map.yml
make run | grep Elapsed
make clean > /dev/null
echo

echo "all channels on dfifo"
ln -fs simple_map_dfifo.yml simple_map.yml
make run | grep Elapsed
make clean > /dev/null
