#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt

N = 8
read_fifo = ( 27042091,194,10,38071255,140,28906802,0,34918986 )
write_fifo = ( 0,0,38619303,0,87010,0,39233764,0 )

ind = np.arange(N)  # the x locations for the groups
width = 0.15       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, read_fifo, width, color='r')
#rects1 = ax.bar(ind, write_fifo, width, color='r')

read_crossbar = ( 33968733,250,10,43203199,170,10425054,0,17634069 )
write_crossbar = ( 0,7160278,44306464,0,6752057,0,46793945,0 )

rects2 = ax.bar(ind+width, read_crossbar, width, color='y')
#rects2 = ax.bar(ind+width, write_crossbar, width, color='y')

# add some
ax.set_ylabel('Cycles')
ax.set_title('Blocking times read for fifo and crossbar')
#ax.set_title('Blocking times write for fifo and crossbar')
ax.set_xticks(ind+width)
ax.set_xticklabels( ( 'Control', 'DCT', 'DMUX', 'Q', 'RGB2YUV', 'VLE', 'Video_In', 'Video_Out' ) )

ax.legend( (rects1[0], rects2[0]), ('fifo', 'crossbar') )

plt.show()

