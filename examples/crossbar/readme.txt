Exploring different buffersizes:
--------------------------------
crossbar:
~~~~~~~~~
make
make -C app runtrace
cd arch
# This is the command that is run for "make runtrace".
# We add: vbufinit="arch,5" to it.
./crossbar -T ../trace -m ../crossbar_appvirt_map.yml crossbar_virtarch.yml vbufinit="arch,5"

Virtual channelsize     Elapsed time
2                       48247588
5                       47647876
10                      47377457
20                      47199463
100                     46835192
200                     46610026
400                     46277223                    



fifo:
~~~~~
virtual channel is vdfifo, which gets it's size from the architecture
component dfifo.
We use script and a variable to set the space in the architecture
component dfifo.

Fifo version is not sensitive to the size of the virtual buffer:

./crossbar -T ../trace -m ../crossbar_appvirt_map.yml
crossbar_virtarch.yml vbufinit=2
upto
./crossbar -T ../trace -m ../crossbar_appvirt_map.yml
crossbar_virtarch.yml vbufinit=200

all give:
Elapsed time: 40076112 units
