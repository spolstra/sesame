#include "RGB2YUV.h"

RGB2YUV::RGB2YUV(Id n, RGB2YUV_Ports *ports) : RGB2YUV_Base(n, ports) {}
RGB2YUV::~RGB2YUV() {}

void RGB2YUV::main()
{
    TBlockData R_Block, G_Block, B_Block;
    TBlockData Y_Block, U1_Block, V1_Block, U2_Block, V2_Block;
    int p;

    while (1) {
        ports->BlockData_in.read(R_Block);
        ports->BlockData_in.read(G_Block);
        ports->BlockData_in.read(B_Block);

        // Convert RGB block to YUV block
        for (int i = 0; i < BLOCKSIZE; i++) {
            Y_Block.pixel[i] = (299 * R_Block.pixel[i] + 587 * G_Block.pixel[i] + 114 * B_Block.pixel[i]) / 1000;
            U1_Block.pixel[i] = (-169 * R_Block.pixel[i] - 331 * G_Block.pixel[i] + 500 * B_Block.pixel[i]) / 1000;
            V1_Block.pixel[i] = (500 * R_Block.pixel[i] - 419 * G_Block.pixel[i] - 81 * B_Block.pixel[i]) / 1000;
        }

        ports->BlockData_out.write(Y_Block);
        execute("op_RGB2YUV");

        ports->BlockData_in.read(R_Block);
        ports->BlockData_in.read(G_Block);
        ports->BlockData_in.read(B_Block);

        // Convert RGB block to YUV block
        for (int j = 0; j < BLOCKSIZE; j++) {
            Y_Block.pixel[j] = (299 * R_Block.pixel[j] + 587 * G_Block.pixel[j] + 114 * B_Block.pixel[j]) / 1000;
            U2_Block.pixel[j] = (-169 * R_Block.pixel[j] - 331 * G_Block.pixel[j] + 500 * B_Block.pixel[j]) / 1000;
            V2_Block.pixel[j] = (500 * R_Block.pixel[j] - 419 * G_Block.pixel[j] - 81 * B_Block.pixel[j]) / 1000;
        }

        ports->BlockData_out.write(Y_Block);
        execute("op_RGB2YUV");

        // make sub-sampling of U and V blocks
        p = 0;

        for (int k = 0; k < 8; k++) {
            for (int l = 0; l < 4; l++) {
                U1_Block.pixel[p] = U1_Block.pixel[8 * k + 2 * l];
                V1_Block.pixel[p] = V1_Block.pixel[8 * k + 2 * l];
                p++;
            }

            for (int m = 0; m < 4; m++) {
                U1_Block.pixel[p] = U2_Block.pixel[8 * k + 2 * m];
                V1_Block.pixel[p] = V2_Block.pixel[8 * k + 2 * m];
                p++;
            }
        }

        ports->BlockData_out.write(U1_Block);
        ports->BlockData_out.write(V1_Block);
    }
}
