import matplotlib.pyplot as plt

# list of virtual_channelsizes and Elapsed times
raw_data=[2,48247588,5,47647876,10,47377457,20,47199463,100,46835192,200,46610026,400,46277223]

indices = raw_data[0:][::2] # even entries are the x-values (bufsize)
values = raw_data[1:][::2] # odd entries are the y-values (cycles)

plt.plot(indices, values, 'ro-')
#plt.axis([0, 6, 0, 20])
plt.show()

