
#include "C.h"

#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

C::C(Id n, C_Ports *ports) : C_Base(n, ports) {}
C::~C() {}

void C::main() {
    int x;

    while (true) {
        // Read from B.
        ports->in1.read(x);
        cout << getFullName() << " received " << x << endl;

        // Read from A.
        ports->in0.read(x);
        cout << getFullName() << " received " << x << endl;
        execute("op_EndLoop");
    }
}
