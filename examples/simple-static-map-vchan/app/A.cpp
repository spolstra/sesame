#include "A.h"
#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

A::A(Id n, A_Ports *ports) : A_Base(n, ports) {}
A::~A() {}

void A::main()
{
    int i;
    int data = 1;
    int count = atoi(getArgv()[1]);

    for (i = 0; i < count; i++) {
        ports->out0.write(data);
        cout << getFullName() << "     sent " << data << endl;
        execute("op_EndLoop");
    }
}
