#define VERBOSE 1

#ifdef VERBOSE
#       define VP(s)    (printf s) ;
#else
#       define VP(s)    ;
#endif

#define MAX_EVENTS 20
#define EXECUTE     1
#define READ        2
#define WRITE       3
#define NO_EVENT   -1
#define QUIT       -1

// ENDLOOP needs to match the operation defined in the app.yml and
// map.yml files.
#define ENDLOOP     0
