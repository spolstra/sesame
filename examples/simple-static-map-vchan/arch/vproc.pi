#include "event.h"
#include "debug.h"

class vproc
  arch        : processor
  nvchannels  : integer
  vchannels_t = [nvchannels] vchannel
  vchannels   : vchannels_t

  ch    : vchannel
  jobid : integer

  vch_map  : integer = intmap_create()
  ch_map   : integer = intmap_create()

statistics:() {}

{
  i     : integer
  vchid : integer
  chid  : integer
  ttype : integer

  // This initiates virtual channel negotiation which allows
  // processors to identify virtual channels with actual hardware
  // communication paths with out port mapping information.
  for (i = 0; i < nvchannels; i += 1) {
    // Get the virtual channel id
    vchid = vchannels[i] ! get_vchannel_id();

    // Ask the processor what id to use when refering to this
    // virtual channel.
    chid = arch ! get_channel_id(vchannels[i] ! get_channel_id());

    // Store this information
    intmap_put(vch_map, vchid, i);
    intmap_put(ch_map, vchid, chid);
  };

  // Delay the start of the simulation, so that other vprocs can finish
  // their communication channel initialisation.
  blockt(0);

  // We should start processing trace events at cycle 0.
  assert((timer() == 0), ("%s: Initialisation took too long! [t: %d]", whoami  (), timer()));

  // Start processing trace events
  while (true) {
    ttype = nextTrace();

    if (ttype == READ || ttype == WRITE) {
      // Look up channel id
      ch = vchannels[intmap_get(vch_map, traceCommID())];
      chid = intmap_get(ch_map, traceCommID());
    };

    switch (ttype) {
    READ {
      ch ! check_data();
      arch ! read(chid, traceCommID(), traceSize());
      printf("Event R: %s -> %s\n", whoami(), who(arch));
      ch !! signal_room();
    }

    WRITE {
      ch ! check_room();
      arch ! write(chid, traceCommID(), traceSize());
      printf("Event W: %s -> %s\n", whoami(), who(arch));
      ch !! signal_data();
    }

    EXECUTE {
      // Although we are running in unscheduled mode, the application
      // might still be generation ENDLOOP execute events. We should
      // ignore these.
      if ( traceInstruction() != ENDLOOP ) {
          arch ! execute(traceInstruction());
          printf("Event E: %s -> %s\n", whoami(), who(arch));
      }
    }
    QUIT {break;}
    }
  }
}
