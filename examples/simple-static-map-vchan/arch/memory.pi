class memory

  latency : integer
  space   : integer
  bus     : bus

  buffers    : integer = intmap_create()
  reads      : integer = 0
  writes     : integer = 0
  read_time  : integer = 0
  write_time : integer = 0

// Check that this channel id was really allocated with create_channel()
// If the vid is not in buffers map intmap_get will generate an error.
check_channel:integer(vid:integer) {
  return intmap_get(buffers, vid);
}

read:(vid:integer, size:integer) -> void {
  s : integer = check_channel(vid);

  blockt(latency);

  reads += 1;
  read_time += latency;

  reply();
}

write:(vid:integer, size:integer) -> void {
  s : integer = check_channel(vid);

  blockt(latency);

  writes += 1;
  write_time += latency;

  reply();
}

get_id:() -> integer {
  reply(bus ! get_id());
}

// Called by vmembus
create_channel:(vid:integer, size:integer) -> void {
  // Allocate space
  space -= size;
  if (space < 0) {panic("Memory over allocated!");};

  // Save allocation under this virtual channel id
  intmap_put(buffers, vid, size);

  // Add a route in the bus to this component for vid
  bus ! add_route(vid, this);

  reply();
}

statistics:()  {
  printf("%s-statistics()\n", whoami());
  printf("%s-Reads\t\t%d\n", whoami(), reads);
  printf("%s-Writes\t\t%d\n", whoami(), writes);
  printf("%s-Read time\t\t%d\n", whoami(), read_time);
  printf("%s-Write time\t%d\n", whoami(), write_time);
  printf("%s-Busy time\t\t%d\n", whoami(), read_time + write_time);
  printf("%s-Idle time\t\t%d\n", whoami(), timer() - (read_time + write_time));
}

{
  while (true) {
    block(any);
  };
}
