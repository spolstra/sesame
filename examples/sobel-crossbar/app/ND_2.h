// File automatically generated by ESPAM

#ifndef ND_2_H
#define ND_2_H

#include "ND_2_Base.h"

class ND_2 : public ND_2_Base {
public:
  ND_2(Id n, ND_2_Ports *ports);
  virtual ~ND_2();

void _gradient( int in_0, int in_1, int in_2, int in_3, int in_4, int in_5, int &out_6 );

  void main();
};
#endif // ND_2_H
