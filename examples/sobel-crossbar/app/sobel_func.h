#ifndef SOBEL_FUNC_H
#define SOBEL_FUNC_H

void gradient( const int *a1, const int *a2, const int *a3, const int *a4, const int *a5, const int *a6, int *output );

void absVal( const int *x, const int *y, int *output );

void readPixel( int *output );

void  writePixel( const int *pp );

#endif
