#include "stdlib.h"
#include "imageIO.h"

//----------------------------------------------
// mrOpen() opens a file for read
//----------------------------------------------
FILE *mrOpen(char *filename) {

	FILE *fh;
	if ((fh = fopen(filename,"rb"))==NULL) {
	    printf("Cannot read input file %s\n", filename);
	    exit(1);
	}
	return(fh);

}

//-------------------------------------------------
// mwOpen() creates a file for write
//-------------------------------------------------
FILE *mwOpen(char *filename) {

	FILE *fh;
	if ((fh = fopen(filename,"wb"))==NULL) {
	    printf("Cannot create file %s\n", filename);
	    exit(1);
	}
	return(fh);

}


//------------------------------------
// mClose() closes a file
//------------------------------------
void mClose(FILE *fh) {

    fclose(fh);

}

//-------------------------------------------------------
//mGetc() gets a character from a file
//-------------------------------------------------------
int mGetc(FILE *fh) {

    return( getc(fh) );

}

//-------------------------------------------------
//mPutc puts a character to a file
//-------------------------------------------------
void mPutc(FILE *fh, int a) {

    putc(a, fh);

}





