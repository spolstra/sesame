// File automatically generated by ESPAM

#ifndef ND_1_H
#define ND_1_H

#include "ND_1_Base.h"

class ND_1 : public ND_1_Base {
public:
  ND_1(Id n, ND_1_Ports *ports);
  virtual ~ND_1();

void _gradient( int in_0, int in_1, int in_2, int in_3, int in_4, int in_5, int &out_6 );

  void main();
};
#endif // ND_1_H
