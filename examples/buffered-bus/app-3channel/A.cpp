#include "A.h"

#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

A::A(Id n, A_Ports *ports) : A_Base(n, ports) {}
A::~A() {}

void A::main()
{
    int i, reply;
    int count = atoi(getArgv()[1]);

    for (i = 0; i < count; i++) {
        ports->out0.write(i);
        ports->in0.read(reply);
        if (reply == i)
            ports->out1.write(i);
        else {
            cout << "values do not match" << endl;
            return; // Stops PNRunner
        }

        cout << getFullName() << "     sent " << i << endl;
        // sleep(2);
    }
}
