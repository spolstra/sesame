#include "B.h"

#include <iostream>
#include <unistd.h>
using namespace std;

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int x;

    while (true) {
        ports->in0.read(x);
        // write back same value
        ports->out0.write(x);
        ports->in1.read(x);
        cout << getFullName() << " received " << x << endl;
        // sleep(1);
    }
}
