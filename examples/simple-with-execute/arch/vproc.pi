#include "operations.h"
#include "debug.h"

class vproc

/* Module parameters */
arch        : processor
nvchannels  : integer
vchannels_t = [nvchannels] vchannel
vchannels   : vchannels_t

/* Local module variables */
ch    : vchannel
jobid : integer

vch_map  : integer = intmap_create()
ch_map   : integer = intmap_create()
readblock  : integer = 0
writeblock : integer = 0

statistics:() {
    printf("%s.statistics()\n", whoami());
    printf("%s-Total readblock time %d\n", whoami(),readblock);
    printf("%s-Total writeblock time %d\n", whoami(),writeblock);
}

{
    i     : integer
    vchid : integer
    chid  : integer
    ttype : integer
    t     : integer
  
    // This initiates virtual channel negotiation which allows
    // processors to identify virtual channels with actual hardware
    // communication paths with out port mapping information.
    for (i = 0; i < nvchannels; i += 1) {
        // Get the virtual channel id
        vchid = vchannels[i] ! get_vchannel_id();
  
        // Ask the processor what id to use when refering to this
        // virtual channel.
        chid = arch ! get_channel_id(vchannels[i] ! get_channel_id());
  
        // Store this information
        intmap_put(vch_map, vchid, i);
        intmap_put(ch_map, vchid, chid);
    };
  
    // Delay the start of the simulation, so that other vprocs can finish
    // their communication channel initialisation.
    blockt(0);
  
    // We should start processing trace events at cycle 0.
    assert((timer() == 0), ("%s: Initialisation took too long! [t: %d]", whoami  (), timer()));
  
    // Start processing trace events
    while (true) {
        ttype = nextTrace();
  
        if (ttype == READ || ttype == WRITE) {
            // Look up channel id
            ch = vchannels[intmap_get(vch_map, traceCommID())];
            chid = intmap_get(ch_map, traceCommID());
        };
  
        switch (ttype) {
        READ {
            t = timer();                       // start timer
            ch ! check_data();
            readblock += timer() - t;
            arch ! read(chid, traceCommID(), traceSize());
            ch !! signal_room();
        }
  
        WRITE {
            t = timer();                       // start timer
            ch ! check_room();
            writeblock += timer() - t;
            arch ! write(chid, traceCommID(), traceSize());
            ch !! signal_data();
        }
  
        EXECUTE {
            arch ! execute(traceInstruction());
        }
  
        QUIT {break;}
        }
    }
}
