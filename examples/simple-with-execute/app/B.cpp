#include "B.h"

#include <iostream>
using namespace std;

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int x;

    while (true) {
        ports->in0.read(x);
        execute("execute2");
        cout << getFullName() << " received " << x << endl;
    }
}
