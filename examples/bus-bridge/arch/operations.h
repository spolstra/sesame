#define EXECUTE     1
#define READ        2
#define WRITE       3
#define QUIT        -1

#define NO_ROUTE    0
// data direction can be: READ, WRITE or BOTH
#define BOTH        4
// has_route response can be NO_ROUTE, READ, WRITE or BOTH

