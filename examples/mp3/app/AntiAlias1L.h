// File automatically generated by ESPAM

#ifndef AntiAlias1L_H
#define AntiAlias1L_H

#include "AntiAlias1L_Base.h"

class AntiAlias1L : public AntiAlias1L_Base
{
    public:
        AntiAlias1L(Id n, AntiAlias1L_Ports *ports);
        virtual ~AntiAlias1L();

        void main();
};
#endif // AntiAlias1L_H
