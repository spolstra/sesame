// File automatically generated by ESPAM

#include "AntiAlias2R.h"

AntiAlias2R::AntiAlias2R(Id n, AntiAlias2R_Ports *ports) : AntiAlias2R_Base(n, ports) {}
AntiAlias2R::~AntiAlias2R() {}

void AntiAlias2R::main()
{

    // Input Arguments
    t_channel_packet ch;

    while (true) {
        //Read packet
        ports->IG_1.read(ch);

        //Process packet
        MPG_L3_Antialias(&ch);
        execute("op_MPG_L3_Antialias");

        //Pass it on
        ports->OG_1.write(ch);
    }
} // main
