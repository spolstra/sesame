/******************************************************************************
*
* Filename: MPG_Bitstream.h
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This file contains definitions and declarations needed by
*              functions using the bitstream functions.
*
******************************************************************************/

#ifndef _MPG_BITSTREAM_H_
#define _MPG_BITSTREAM_H_

/* Include files */
#include <string>

#include "MP3_Decoder.h"
#include "MP3_Main.h"

/* External functions and variables (defined elsewhere, and used here) */


/* Global functions and variables (defined here, and used here & elsewhere) */
void  MPG_Set_Song(std::string filename,
                   t_file *stream,
                   t_bit_main_data *bit_main);

UINT32 MPG_Get_Byte(t_file *fp);
STATUS MPG_Get_Bytes(t_file *fp, UINT32 no_of_bytes, UINT32 data_vec[]);
UINT32 MPG_Get_Filepos(t_file *fp);
UINT32 MPG_Get_Filesize(t_file *fp);
UINT32 MPG_FEOF(t_file *fp);

/* Local functions and variables (defined here, used here) */


/* Definitions */

#define C_MPG_EOF           0xffffffff

#endif /* _MPG_BITSTREAM_H_ */
