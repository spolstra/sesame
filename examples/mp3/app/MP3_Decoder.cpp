/******************************************************************************
*
* Filename: MPG_Decode_L3.c
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This file contains the Level 3 decoding function.
*
******************************************************************************/

/* Include files */
#include "MP3_Main.h"
#include "MP3_Decoder.h"
#include "MP3_Bitstream.h"
#include "MP3_Synth_Table.h"

/******************************************************************************
*
* Name: MPG_Decode_L3_Init_Song
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This function is used to reinit the decoder before playing a new
*              song, or when seeking inside the current song.
* Parameters: None
* Return value: None
*
******************************************************************************/
void
MPG_Decode_L3_Init_Song(t_bit_main_data *bitmain)
{
    bitmain->top = 0;     /* Clear bit reservoir */
}

/******************************************************************************
*
* Name: MPG_L3_Requantize
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Requantize(t_channel_packet *chan,
                       const UINT32 *sfreq)
{
    UINT32 sfb /* scalefac band index */;
    UINT32 next_sfb /* frequency of next sfb */;
    UINT32 i, j, win, win_len;


    /* Determine type of block to process */
    if ((chan->info.win_switch_flag == 1) &&
        (chan->info.block_type == 2)) { /* Short blocks */

        /* Check if the first two subbands
         * (=2*18 samples = 8 long or 3 short sfb's) uses long blocks */
        if (chan->info.mixed_block_flag != 0) { /* 2 longbl. sb  first */

            /*
             * First process the 2 long block subbands at the start
             */
            sfb = 0;
            next_sfb = g_sf_band_indices[*sfreq].l[sfb + 1];

            for (i = 0; i < 36; i++) {
                if (i == next_sfb) {
                    sfb++;
                    next_sfb = g_sf_band_indices[*sfreq].l[sfb + 1];
                } /* end if */

                MPG_Requantize_Process_Long(chan, i, sfb);
            }

            /*
             * And next the remaining, non-zero, bands which uses short blocks
             */
            sfb = 3;
            next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
            win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                      g_sf_band_indices[*sfreq].s[sfb];

            for (i = 36; i < chan->info.count1; /* i++ done below! */) {

                /* Check if we're into the next scalefac band */
                if (i == next_sfb) {    /* Yes */
                    sfb++;
                    next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
                    win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                              g_sf_band_indices[*sfreq].s[sfb];
                } /* end if (next_sfb) */

                for (win = 0; win < 3; win++) {
                    for (j = 0; j < win_len; j++) {
                        MPG_Requantize_Process_Short(chan, i, sfb, win);
                        i++;
                    } /* end for (win... */
                } /* end for (j... */

            } /* end for (i... */

        } else {            /* Only short blocks */

            sfb = 0;
            next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
            win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                      g_sf_band_indices[*sfreq].s[sfb];

            for (i = 0; i < chan->info.count1; /* i++ done below! */) {

                /* Check if we're into the next scalefac band */
                if (i == next_sfb) {    /* Yes */
                    sfb++;
                    next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
                    win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                              g_sf_band_indices[*sfreq].s[sfb];
                } /* end if (next_sfb) */

                for (win = 0; win < 3; win++) {
                    for (j = 0; j < win_len; j++) {
                        MPG_Requantize_Process_Short(chan, i, sfb, win);
                        i++;
                    } /* end for (win... */
                } /* end for (j... */

            } /* end for (i... */

        } /* end else (only short blocks) */

    } else {          /* Only long blocks */

        sfb = 0;
        next_sfb = g_sf_band_indices[*sfreq].l[sfb + 1];

        for (i = 0; i < chan->info.count1; i++) {
            if (i == next_sfb) {
                sfb++;
                next_sfb = g_sf_band_indices[*sfreq].l[sfb + 1];
            } /* end if */

            MPG_Requantize_Process_Long(chan, i, sfb);
        }

    } /* end else (only long blocks) */

    /* Done */
    return;

}


/******************************************************************************
*
* Name: MPG_Requantize_Process_Long
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This function is used to requantize a sample in a subband
*              that uses long blocks.
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_Requantize_Process_Long(t_channel_packet *chan, UINT32 is_pos, UINT32 sfb)
{
    FLOAT32 res, tmp1, tmp2, tmp3, sf_mult, pf_x_pt;
    static FLOAT32 pretab[21] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                  1, 1, 1, 1, 2, 2, 3, 3, 3, 2
                                };


    sf_mult = chan->info.scalefac_scale ? 1.0 : 0.5;
    pf_x_pt = chan->info.preflag * pretab[sfb];

    tmp1 =
        pow(2.0, -(sf_mult * (chan->data.scalefac_l[sfb] + pf_x_pt)));

    tmp2 = pow(2.0, 0.25 * ((INT32) chan->info.global_gain - 210));

    if (chan->data.is[is_pos] < 0.0) {
        tmp3 = pow(-chan->data.is[is_pos], 4.0 / 3.0);
        tmp3 = -tmp3;
    } else {
        tmp3 = pow(chan->data.is[is_pos], 4.0 / 3.0);
    }

    res = chan->data.is[is_pos] = tmp1 * tmp2 * tmp3;

    /* Done */
    return;

}


/******************************************************************************
*
* Name: MPG_Requantize_Process_Short
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This function is used to requantize a sample in a subband
*              that uses short blocks.
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_Requantize_Process_Short(t_channel_packet *chan, UINT32 is_pos,
                                  UINT32 sfb, UINT32 win)
{
    FLOAT32 res, tmp1, tmp2, tmp3, sf_mult;


    sf_mult = chan->info.scalefac_scale ? 1.0 : 0.5;

    tmp1 =
        pow(2.0, -(sf_mult * chan->data.scalefac_s[sfb][win]));

    tmp2 =
        pow(2.0, 0.25 * ((FLOAT32) chan->info.global_gain - 210.0 -
                         8.0 * (FLOAT32) chan->info.subblock_gain[win]));

    if (chan->data.is[is_pos] < 0.0) {
        tmp3 = pow(-chan->data.is[is_pos], 4.0 / 3.0);
        tmp3 = -tmp3;
    } else {
        tmp3 = pow(chan->data.is[is_pos], 4.0 / 3.0);
    }

    res = chan->data.is[is_pos] = tmp1 * tmp2 * tmp3;

    /* Done */
    return;

}


/******************************************************************************
*
* Name: MPG_L3_Reorder
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Reorder(t_channel_packet *chan,
                    const UINT32 *sfreq)
{
    UINT32 i, j, next_sfb, sfb, win_len, win;
    FLOAT32 re[576];


    /* Only reorder short blocks */
    if ((chan->info.win_switch_flag == 1) &&
        (chan->info.block_type == 2)) { /* Short blocks */

        /* Check if the first two subbands
         * (=2*18 samples = 8 long or 3 short sfb's) uses long blocks */
        if (chan->info.mixed_block_flag != 0) { /* 2 longbl. sb  first */

            /* Don't touch the first 36 samples */

            /*
             * Reorder the remaining, non-zero, bands which uses short blocks
             */
            sfb = 3;
            next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
            win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                      g_sf_band_indices[*sfreq].s[sfb];

        } else { /* Only short blocks */

            sfb = 0;
            next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
            win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                      g_sf_band_indices[*sfreq].s[sfb];

        } /* end else (only short blocks) */


        for (i = ((sfb == 0) ? 0 : 36); i < 576; /* i++ done below! */) {

            /* Check if we're into the next scalefac band */
            if (i == next_sfb) {  /* Yes */

                /* Copy reordered data back to the original vector */
                for (j = 0; j < 3 * win_len; j++) {
                    chan->data.is[3 * g_sf_band_indices[*sfreq].s[sfb] + j] =
                        re[j];
                }

                /* Check if this band is above the rzero region, if so we're done */
                if (i >= chan->info.count1) {
                    /* Done */
                    return;
                }

                sfb++;
                next_sfb = g_sf_band_indices[*sfreq].s[sfb + 1] * 3;
                win_len = g_sf_band_indices[*sfreq].s[sfb + 1] -
                          g_sf_band_indices[*sfreq].s[sfb];

            } /* end if (next_sfb) */

            /* Do the actual reordering */
            for (win = 0; win < 3; win++) {
                for (j = 0; j < win_len; j++) {
                    re[j * 3 + win] = chan->data.is[i];
                    i++;
                } /* end for (j... */
            } /* end for (win... */

        }   /* end for (i... */

        /* Copy reordered data of the last band back to the original vector */
        for (j = 0; j < 3 * win_len; j++) {
            chan->data.is[3 * g_sf_band_indices[*sfreq].s[12] + j] = re[j];
        }

    } else {          /* Only long blocks */
        /* No reorder necessary, do nothing! */
        return;

    } /* end else (only long blocks) */

    /* Done */
    return;

}


/******************************************************************************
*
* Name: MPG_L3_Stereo
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Stereo(const t_mpeg1_header *header,
                   t_channel_packet *chan_l,
                   t_channel_packet *chan_r)
{
    UINT32 max_pos, i;
    FLOAT32 left, right;
    UINT32 sfb /* scalefac band index */;
    UINT32 sfreq;



    /* Do nothing if joint stereo is not enabled */
    if ((header->mode != 1) || (header->mode_extension == 0)) {
        /* Done */
        return;
    }

    /* Do Middle/Side ("normal") stereo processing */
    if (header->mode_extension & 0x2) {

        /* Determine how many frequency lines to transform */
        if (chan_l->info.count1 > chan_r->info.count1) {
            max_pos = chan_l->info.count1;
        } else {
            max_pos = chan_r->info.count1;
        }

        /* Do the actual processing */
        for (i = 0; i < max_pos; i++) {
            left = (chan_l->data.is[i] + chan_r->data.is[i])
                   * (C_INV_SQRT_2);
            right = (chan_l->data.is[i] - chan_r->data.is[i])
                    * (C_INV_SQRT_2);
            chan_l->data.is[i] = left;
            chan_r->data.is[i] = right;
        } /* end for (i... */

    } /* end if (ms_stereo... */

    /* Do intensity stereo processing */
    if (header->mode_extension & 0x1) {

        /* Setup sampling frequency index */
        sfreq = header->sampling_frequency;

        /* The first band that is intensity stereo encoded is the first band
         * scale factor band on or above the count1 frequency line.
         * N.B.: Intensity stereo coding is only done for the higher subbands,
         * but the logic is still included to process lower subbands.
         */

        /* Determine type of block to process */
        if ((chan_l->info.win_switch_flag == 1) &&
            (chan_l->info.block_type == 2)) { /* Short blocks */

            /* Check if the first two subbands
             * (=2*18 samples = 8 long or 3 short sfb's) uses long blocks */
            if (chan_l->info.mixed_block_flag != 0) { /* 2 longbl. sb  first */

                /*
                 * First process the 8 sfb's at the start
                 */
                for (sfb = 0; sfb < 8; sfb++) {

                    /* Is this scale factor band above count1 for the right channel? */
                    if (g_sf_band_indices[sfreq].l[sfb] >= chan_r->info.count1) {
                        MPG_Stereo_Process_Intensity_Long(sfreq, sfb, chan_l, chan_r);
                    }

                } /* end if (sfb... */

                /*
                 * And next the remaining bands which uses short blocks
                 */
                for (sfb = 3; sfb < 12; sfb++) {

                    /* Is this scale factor band above count1 for the right channel? */
                    if (g_sf_band_indices[sfreq].s[sfb] * 3 >= chan_r->info.count1) {

                        /* Perform the intensity stereo processing */
                        MPG_Stereo_Process_Intensity_Short(sfreq, sfb, chan_l, chan_r);
                    }
                }

            } else {          /* Only short blocks */

                for (sfb = 0; sfb < 12; sfb++) {

                    /* Is this scale factor band above count1 for the right channel? */
                    if (g_sf_band_indices[sfreq].s[sfb] * 3 >= chan_r->info.count1) {

                        /* Perform the intensity stereo processing */
                        MPG_Stereo_Process_Intensity_Short(sfreq, sfb, chan_l, chan_r);
                    }
                }

            } /* end else (only short blocks) */

        } else {            /* Only long blocks */

            for (sfb = 0; sfb < 21; sfb++) {

                /* Is this scale factor band above count1 for the right channel? */
                if (g_sf_band_indices[sfreq].l[sfb] >= chan_r->info.count1) {

                    /* Perform the intensity stereo processing */
                    MPG_Stereo_Process_Intensity_Long(sfreq, sfb, chan_l, chan_r);
                }
            }

        } /* end else (only long blocks) */

    } /* end if (intensity_stereo processing) */

}


/******************************************************************************
*
* Name: MPG_Stereo_Process_Intensity_Long
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This function is used to perform intensity stereo processing
*              for an entire subband that uses long blocks.
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_Stereo_Process_Intensity_Long(UINT32 sfreq, UINT32 sfb,
                                       t_channel_packet *chan_l,
                                       t_channel_packet *chan_r)
{
    static int init = 0;
    static FLOAT32 is_ratios[6];
    UINT32 i;
    UINT32 sfb_start, sfb_stop;
    UINT32 is_pos;
    FLOAT32 is_ratio_l, is_ratio_r;
    FLOAT32 left, right;


    /* First-time init */
    if (init == 0) {
        init = 1;

        for (i = 0; i < 6; i++) {
            is_ratios[i] = tan((i * C_PI) / 12.0);
        }

    }

    /* Check that ((is_pos[sfb]=scalefac) != 7) => no intensity stereo */
    if ((is_pos = chan_l->data.scalefac_l[sfb]) != 7) {

        sfb_start = g_sf_band_indices[sfreq].l[sfb];
        sfb_stop = g_sf_band_indices[sfreq].l[sfb + 1];

        /* tan((6*PI)/12 = PI/2) needs special treatment! */
        if (is_pos == 6) {
            is_ratio_l = 1.0;
            is_ratio_r = 0.0;
        } else {
            is_ratio_l = is_ratios[is_pos] / (1.0 + is_ratios[is_pos]);
            is_ratio_r = 1.0 / (1.0 + is_ratios[is_pos]);
        }

        /* Now decode all samples in this scale factor band */
        for (i = sfb_start; i < sfb_stop; i++) {
            left = is_ratio_l * chan_l->data.is[i];
            right = is_ratio_r * chan_l->data.is[i];
            chan_l->data.is[i] = left;
            chan_r->data.is[i] = right;
        }
    }

    /* Done */
    return;

} /* end MPG_Stereo_Process_Intensity_Long() */


/******************************************************************************
*
* Name: MPG_Stereo_Process_Intensity_Short
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This function is used to perform intensity stereo processing
*              for an entire subband that uses short blocks.
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_Stereo_Process_Intensity_Short(UINT32 sfreq, UINT32 sfb,
                                        t_channel_packet *chan_l,
                                        t_channel_packet *chan_r)
{
    UINT32 i;
    FLOAT32 left, right;
    UINT32 sfb_start, sfb_stop;
    UINT32 is_pos;
    FLOAT32 is_ratio_l, is_ratio_r, is_ratios[6]; /* ORIGINALLY: UINT32, BUT THAT SEEMS WEIRD WITH ASSIGNMENT TO 1.0 AND 0.0*/
    UINT32 win, win_len;


    /* The window length */
    win_len = g_sf_band_indices[sfreq].s[sfb + 1] -
              g_sf_band_indices[sfreq].s[sfb];

    /* The three windows within the band has different scalefactors */
    for (win = 0; win < 3; win++) {

        /* Check that ((is_pos[sfb]=scalefac) != 7) => no intensity stereo */
        if ((is_pos = chan_l->data.scalefac_s[sfb][win]) != 7) {

            sfb_start = g_sf_band_indices[sfreq].s[sfb] * 3 + win_len * win;
            sfb_stop = sfb_start + win_len;

            /* tan((6*PI)/12 = PI/2) needs special treatment! */
            if (is_pos == 6) {
                is_ratio_l = 1.0;
                is_ratio_r = 0.0;
            } else {
                is_ratio_l = is_ratios[is_pos] / (1.0 + is_ratios[is_pos]);
                is_ratio_r = 1.0 / (1.0 + is_ratios[is_pos]);
            }

            /* Now decode all samples in this scale factor band */
            for (i = sfb_start; i < sfb_stop; i++) {
                left = is_ratio_l = chan_l->data.is[i];
                right = is_ratio_r = chan_l->data.is[i];
                chan_l->data.is[i] = left;
                chan_r->data.is[i] = right;
            }
        } /* end if (not illegal is_pos) */
    } /* end for (win... */

    /* Done */
    return;

} /* end MPG_Stereo_Process_Intensity_Short() */


/******************************************************************************
*
* Name: MPG_L3_Antialias
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Antialias(t_channel_packet *chan)
{
    UINT32 sb /* subband of 18 samples */, i, sblim, ui, li;
    FLOAT32 ub, lb;
    static FLOAT32 cs[8], ca[8];
    static FLOAT32 ci[8] = { -0.6,   -0.535, -0.33,   -0.185,
                             -0.095, -0.041, -0.0142, -0.0037
                           };
    static UINT32 init = 1;


    if (init) {
        for (i = 0; i < 8; i++) {
            cs[i] = 1.0 / sqrt(1.0 + ci[i] * ci[i]);
            ca[i] = ci[i] / sqrt(1.0 + ci[i] * ci[i]);
        }

        init = 0;
    }

    /* No antialiasing is done for short blocks */
    if ((chan->info.win_switch_flag == 1) &&
        (chan->info.block_type == 2) &&
        (chan->info.mixed_block_flag) == 0) {
        /* Done */
        return;
    }

    /* Setup the limit for how many subbands to transform */
    if ((chan->info.win_switch_flag == 1) &&
        (chan->info.block_type == 2) &&
        (chan->info.mixed_block_flag) == 1) {
        sblim = 2;
    } else {
        sblim = 32;
    }

    /* Do the actual antialiasing */
    for (sb = 1; sb < sblim; sb++) {
        for (i = 0; i < 8; i++) {
            li = 18 * sb - 1 - i;
            ui = 18 * sb + i;
            lb = chan->data.is[li] * cs[i] - chan->data.is[ui] * ca[i];
            ub = chan->data.is[ui] * cs[i] + chan->data.is[li] * ca[i];
            chan->data.is[li] = lb;
            chan->data.is[ui] = ub;
        }
    }

    /* Done */
    return;

}


/******************************************************************************
*
* Name: MPG_L3_Hybrid_Synthesis
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD => ASSUMING chan_in == chan and samples_in == samples
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Hybrid_Synthesis(t_channel_packet *chan,
                             t_hybrid_samples *samples)
{
    UINT32 sb, i, bt;
    FLOAT32 rawout[36];

    if (samples->init) {
        /* Clear stored samples vector */
        for (sb = 0; sb < 32; sb++) {
            for (i = 0; i < 18; i++) {
                samples->sample[sb][i] = 0.0;
            }
        }

        samples->init = 0;
    } /* end if (init) */

    /* Loop through all 32 subbands */
    for (sb = 0; sb < 32; sb++) {

        /* Determine blocktype for this subband */
        if ((chan->info.win_switch_flag == 1) &&
            (chan->info.mixed_block_flag == 1) && (sb < 2)) {
            bt = 0;           /* Long blocks in first 2 subbands */
        } else {
            bt = chan->info.block_type;
        }

        /* Do the inverse modified DCT and windowing */
        MPG_IMDCT_Win(&(chan->data.is[sb * 18]), rawout, bt);

        /* Overlapp add with stored vector into main_data vector */
        for (i = 0; i < 18; i++) {

            chan->data.is[sb * 18 + i] = rawout[i] + samples->sample[sb][i];
            samples->sample[sb][i] = rawout[i + 18];

        } /* end for (i... */

    } /* end for (sb... */

    /* Done */
    return;

}


/******************************************************************************
*
* Name: MPG_IMDCT_Win
* Author: Krister Lagerström (krister@kmlager.com)
* Description: Does inverse modified DCT and windowing.
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_IMDCT_Win(FLOAT32 in[18], FLOAT32 out[36], UINT32 block_type)
{
    static FLOAT32 g_imdct_win[4][36];
    UINT32 i, m, N, p;
    FLOAT32 tmp[12], sum;
    FLOAT32 tin[18];
    static UINT32 init = 1;


    /* Setup the four (one for each block type) window vectors */
    if (init) {

        /* Blocktype 0 */
        for (i = 0; i < 36; i++) {
            g_imdct_win[0][i] = sin(C_PI / 36 * (i + 0.5));
        }

        /* Blocktype 1 */
        for (i = 0; i < 18; i++) {
            g_imdct_win[1][i] = sin(C_PI / 36 * (i + 0.5));
        }

        for (i = 18; i < 24; i++) {
            g_imdct_win[1][i] = 1.0;
        }

        for (i = 24; i < 30; i++) {
            g_imdct_win[1][i] = sin(C_PI / 12 * (i + 0.5 - 18.0));
        }

        for (i = 30; i < 36; i++) {
            g_imdct_win[1][i] = 0.0;
        }

        /* Blocktype 2*/
        for (i = 0; i < 12; i++) {
            g_imdct_win[2][i] = sin(C_PI / 12 * (i + 0.5));
        }

        for (i = 12; i < 36; i++) {
            g_imdct_win[2][i] = 0.0;
        }

        /* Blocktype 3 */
        for (i = 0; i < 6; i++) {
            g_imdct_win[3][i] = 0.0;
        }

        for (i = 6; i < 12; i++) {
            g_imdct_win[3][i] = sin(C_PI / 12 * (i + 0.5 - 6.0));
        }

        for (i = 12; i < 18; i++) {
            g_imdct_win[3][i] = 1.0;
        }

        for (i = 18; i < 36; i++) {
            g_imdct_win[3][i] = sin(C_PI / 36 * (i + 0.5));
        }

        init = 0;
    } /* end of init */

    for (i = 0; i < 36; i++) {
        out[i] = 0.0;
    }

    for (i = 0; i < 18; i++) {
        tin[i] = in[i];
    }

    if (block_type == 2) {    /* 3 short blocks */
        N = 12;

        for (i = 0; i < 3; i++) {

            for (p = 0; p < N; p++) {
                sum = 0.0;

                for (m = 0; m < N / 2; m++) {
                    sum += tin[i + 3 * m] * cos(C_PI / (2 * N) * (2 * p + 1 + N / 2) * (2 * m + 1));
                }

                tmp[p] = sum * g_imdct_win[block_type][p];
            }

            for (p = 0; p < N; p++) {
                out[6 * i + p + 6] += tmp[p];
            }

        } /* end for (i... */

    } else { /* block_type != 2 */

        N = 36;

        for (p = 0; p < N; p++) {

            sum = 0.0;

            for (m = 0; m < N / 2; m++) {
                sum += in[m] * cos(C_PI / (2 * N) * (2 * p + 1 + N / 2) * (2 * m + 1));
            }

            out[p] = sum * g_imdct_win[block_type][p];

        }

    }

}


/******************************************************************************
*
* Name: MPG_L3_Frequency_Inversion
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Frequency_Inversion(t_channel_packet *chan)
{
    UINT32 sb, i;


    for (sb = 1; sb < 32; sb += 2) {
        for (i = 1; i < 18; i += 2) {
            chan->data.is[sb * 18 + i] = -chan->data.is[sb * 18 + i];
        }
    }

    /* Done */
    return;

}

std::string tobits(int val)
{
    std::string result = "";
    int i;

    for (i = 0; i < 32; ++i) {
        if (val & (2 << i)) {
            result.append("1");
        } else {
            result.append("0");
        }
    }

    return result;
}

/******************************************************************************
*
* Name: MPG_L3_Subband_Synthesis
* Author: Krister Lagerström (krister@kmlager.com)
* Description: TBD => ASSUMPTION samples_in == samples
* Parameters: TBD
* Return value: TBD
*
******************************************************************************/
void MPG_L3_Subband_Synthesis(const t_mpeg1_header *header,
                              const t_channel_packet *chan_l,
                              const t_channel_packet *chan_r,
                              t_subband_samples *samples,
                              t_data *outdata)
{
    FLOAT32 u_vec[512];
    FLOAT32 s_vec[32], sum; /* u_vec can be used instead of s_vec */
    int samp;
    UINT32 ss;
    UINT32 i, j;
    UINT32 ch, nch;
    static UINT32 init = 1;
    static FLOAT32 g_synth_n_win[64][32];
    const t_channel_packet *chan[2];


    /* Number of channels (1 for mono and 2 for stereo) */
    chan[0] = chan_l;
    chan[1] = chan_r;
    nch = (header->mode == mpeg1_mode_single_channel ? 1 : 2);

    if (init || samples->init) {
        /* Setup the v_vec intermediate vector */
        for (i = 0; i < 2; i++) {
            for (j = 0; j < 1024; j++) {
                samples->v[i][j] = 0.0;
            }
        }

        samples->init = 0;
    } /* end if (synth_init) */

    /* Setup the n_win windowing vector */
    if (init) {

        for (i = 0; i < 64; i++) {
            for (j = 0; j < 32; j++) {
                g_synth_n_win[i][j] = cos(((FLOAT32)(16 + i) * (2 * j + 1)) * (C_PI / 64.0));
            }
        }

        init = 0;
    } /* end if (init) */

    for (ch = 0; ch < nch; ++ch) {
        /* Loop through the 18 samples in each of the 32 subbands */
        for (ss = 0; ss < 18; ss++) {

            /* Shift up the V vector */
            for (i = 1023; i > 63; i--) {
                samples->v[ch][i] = samples->v[ch][i - 64];
            }

            /* Copy the next 32 time samples to a temp vector */
            for (i = 0; i < 32; i++) {
                s_vec[i] = ((FLOAT32) chan[ch]->data.is[i * 18 + ss]);
            }

            /* Matrix multiply the input data vector with the n_win[][] matrix */
            for (i = 0; i < 64; i++) {
                sum = 0.0;

                for (j = 0; j < 32; j++) {
                    sum += g_synth_n_win[i][j] * s_vec[j];
                }

                samples->v[ch][i] = sum;
            } /* end for (i... */

            /* Build the U vector */
            for (i = 0; i < 8; i++) {
                for (j = 0; j < 32; j++) {
                    /* u_vec[i*64 + j]      = v_vec[ch][i*128 + j];
                       u_vec[i*64 + j + 32] = v_vec[ch][i*128 + j + 96]; */
                    u_vec[(i << 6) + j]      = samples->v[ch][(i << 7) + j];
                    u_vec[(i << 6) + j + 32] = samples->v[ch][(i << 7) + j + 96];
                }
            } /* end for (i... */

            /* Window by u_vec[i] with g_synth_dtbl[i] */
            for (i = 0; i < 512; i++) {
                u_vec[i] = u_vec[i] * g_synth_dtbl[i];
            }

            /* Calculate 32 samples and store them in the outdata vector */
            for (i = 0; i < 32; i++) {
                sum = 0.0;

                for (j = 0; j < 16; j++) {
                    /* sum += u_vec[j*32 + i]; */
                    sum += u_vec[(j << 5) + i];
                }

                /* sum now contains time sample 32*ss+i. Convert to 16-bit signed int */
                samp = (INT32)(sum * 32767.0);

                if (samp > 32767) {
                    samp = 32767;
                } else if (samp < -32767) {
                    samp = -32767;
                }

                samp &= 0xffff;

                /* This function must be called for channel 0 first */
                if (ch == 0) {
                    /* We always run the audio system in stereo mode, and duplicate
                     * the channels here for mono */
                    if (nch == 1) {
                        outdata->f[32 * ss + i] = (samp << 16) | (samp);
                    } else {
                        outdata->f[32 * ss + i] = samp << 16;
                    }
                } else {
                    outdata->f[32 * ss + i] |= samp;
                }
            } /* end for (i... */
        } /* end for (ss... */
    } /* end for (ch... */

    /* Done */
    return;
}
