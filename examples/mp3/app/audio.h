#ifndef AUDIO_H
#define AUDIO_H

#include "MP3_Decoder.h"
#include "MP3_Main.h"

#define NSAMPLES 576
void audio_write(const t_data *samples,
                 const t_mpeg1_header *header);

#endif
