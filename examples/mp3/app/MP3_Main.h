/******************************************************************************
*
* Filename: MPG_Main.h
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This file contains the definitions used by most parts of the
*              MPEG decoder.
*
******************************************************************************/

#ifndef _MPG_MAIN_H_
#define _MPG_MAIN_H_

#define SIM_UNIX

#define OK         0
#define ERROR     -1
#define SKIP      -2
#define TRUE       1
#define FALSE      0

#include <stdint.h>
typedef uint32_t UINT32;
typedef int32_t  INT32;
typedef int16_t  INT16;
typedef uint16_t UINT16;
typedef uint8_t  UINT8;
typedef float    FLOAT32;
typedef double   FLOAT64;
typedef int8_t   BOOL;
typedef int32_t  STATUS;
typedef char    *STRING;

/* Include files */
#include <stdio.h>
#include <string>
#include <math.h>

#define DBG(str, args...) { printf (str, ## args); printf ("\n"); }
#define ERR(str, args...) { fprintf (stderr, str, ## args) ; fprintf (stderr, "\n"); }
#define EXIT(str, args...) { printf (str, ## args);  printf ("\n"); exit (0); }

/* Global definitions */

#define C_MPG_SYNC             0xfff00000

#define C_PI                   3.14159265358979323846
#define C_INV_SQRT_2           0.70710678118654752440

#define Hz                           1
#define kHz                    1000*Hz
#define bit_s                        1
#define kbit_s                 1000*bit_s

/* Types used in the frame header */
typedef struct {
    FILE *fp;
    std::string fname;
} t_file;

/* Packet type */
typedef enum {
    frame_std   = 0,
    frame_first = 1,
    frame_last  = 2,
    frame_left  = 4,
    frame_right = 8
} t_channel_type;

/* Layer number */
typedef enum {
    mpeg1_layer_reserved = 0,
    mpeg1_layer_3        = 1,
    mpeg1_layer_2        = 2,
    mpeg1_layer_1        = 3
} t_mpeg1_layer;

/* Modes */
typedef enum {
    mpeg1_mode_stereo = 0,
    mpeg1_mode_joint_stereo,
    mpeg1_mode_dual_channel,
    mpeg1_mode_single_channel
} t_mpeg1_mode;

/* Bitrate table for all three layers.  */
extern UINT32 g_mpeg1_bitrates[3 /* lay 1-3 */][15 /* header bitrate_index */];

/* Sampling frequencies in hertz (valid for all layers) */
extern UINT32 g_sampling_frequency[3];

/* MPEG1 Layer 1-3 frame header */
typedef struct {
    UINT32 id;                      /* 1 bit */
    t_mpeg1_layer layer;                /* 2 bits */
    UINT32 protection_bit;              /* 1 bit */
    UINT32 bitrate_index;               /* 4 bits */
    UINT32 sampling_frequency;              /* 2 bits */
    UINT32 padding_bit;                 /* 1 bit */
    UINT32 private_bit;                 /* 1 bit */
    t_mpeg1_mode mode;                  /* 2 bits */
    UINT32 mode_extension;              /* 2 bits */
    UINT32 copyright;               /* 1 bit */
    UINT32 original_or_copy;            /* 1 bit */
    UINT32 emphasis;                /* 2 bits */
} t_mpeg1_header;

/* MPEG1 Layer 3 Side Information */
/* [2][2] means [gr][ch] */
typedef struct {
    UINT32 main_data_begin;                     /* 9 bits */
    UINT32 private_bits;                /* 3 bits in mono, 5 in stereo */
    UINT32 scfsi[2][4];                 /* 1 bit */
    UINT32 part2_3_length[2][2];            /* 12 bits */
    UINT32 big_values[2][2];            /* 9 bits */
    UINT32 global_gain[2][2];           /* 8 bits */
    UINT32 scalefac_compress[2][2];         /* 4 bits */
    UINT32 win_switch_flag[2][2];           /* 1 bit */
    /* if (win_switch_flag[][]) */
    UINT32 block_type[2][2];            /* 2 bits */
    UINT32 mixed_block_flag[2][2];          /* 1 bit */
    UINT32 table_select[2][2][3];           /* 5 bits */
    UINT32 subblock_gain[2][2][3];          /* 3 bits */
    /* else */
    /* table_select[][][] */
    UINT32 region0_count[2][2];             /* 4 bits */
    UINT32 region1_count[2][2];             /* 3 bits */
    /* end */
    UINT32 preflag[2][2];               /* 1 bit */
    UINT32 scalefac_scale[2][2];            /* 1 bit */
    UINT32 count1table_select[2][2];        /* 1 bit */
    UINT32 count1[2][2];            /* Not in file, calc. by huff.dec.! */
    UINT8  chtype;
} t_mpeg1_side_info;

/* MPEG1 Layer 3 Main Data */
typedef struct {
    UINT32  scalefac_l[2][2][21];               /* 0-4 bits */
    UINT32  scalefac_s[2][2][12][3];        /* 0-4 bits */
    FLOAT32 is[2][2][576];              /* Huffman coded freq. lines */
} t_mpeg1_main_data;

/* MPEG1 Layer 3 Side Information */
/* Meant for single channel */
typedef struct {
    UINT32 global_gain;                 /* 8 bits */
    UINT32 win_switch_flag;             /* 1 bit */
    UINT32 block_type;                  /* 2 bits */
    UINT32 mixed_block_flag;            /* 1 bit */
    UINT32 subblock_gain[3];            /* 3 bits */
    UINT32 preflag;                 /* 1 bit */
    UINT32 scalefac_scale;              /* 1 bit */
    UINT32 count1;                  /* Not in file, calc. by huff.dec.! */
} t_channel_side_info;

/* MPEG1 Layer 3 Main Data */
/* Meant for single channel */
typedef struct {
    UINT32  scalefac_l[21];                 /* 0-4 bits */
    UINT32  scalefac_s[12][3];              /* 0-4 bits */
    FLOAT32 is[576];                /* Huffman coded freq. lines */
} t_channel_main_data;

/* Combined packet for ease of programming */
typedef struct {
    t_channel_side_info info;
    t_channel_main_data data;
    UINT8               type;
} t_channel_packet;

/* Scale factor band indices, for long and short windows */
typedef struct  {
    UINT32 l[23];
    UINT32 s[14];
} t_sf_band_indices;

/* Bit reservoir for main data */
typedef struct {
    UINT32  vec[2 * 1024]; /* Large static data */
    UINT32 *ptr;         /* Pointer into the reservoir */
    UINT32  idx;         /* Index into the current byte (0-7) */
    UINT32  top;         /* Number of bytes in reservoir (0-1024) */
} t_bit_main_data;

/* Bit reservoir for side info */
typedef struct {
    UINT32  vec[32 + 4];
    UINT32 *ptr;  /* Pointer into the reservoir */
    UINT32  idx;  /* Index into the current byte (0-7) */
} t_bit_side_info;


/* Global functions and variables (defined here, and used here & elsewhere) */
void MPG_Split_Frame(const t_mpeg1_header *g_frame_header,
                     const t_mpeg1_side_info *g_side_info,
                     const t_mpeg1_main_data *g_main_data,
                     t_channel_packet *left1,
                     t_channel_packet *right1,
                     t_channel_packet *left2,
                     t_channel_packet *right2);

STATUS MPG_Read_Frame(t_file *fp,
                      t_bit_main_data *bit_main,
                      t_mpeg1_header *g_frame_header,
                      t_mpeg1_side_info *g_side_info,
                      t_mpeg1_main_data *g_main_data,
                      UINT32 *sfreq);

STATUS MPG_Read_Header(t_file *fp, t_mpeg1_header *g_frame_header) ;
STATUS MPG_Read_CRC(t_file *fp);
STATUS MPG_Read_Audio_L3(const t_mpeg1_header *g_frame_header,
                         t_mpeg1_side_info *g_side_info,
                         t_file *fp);
STATUS MPG_Read_Main_L3(const t_mpeg1_header *g_frame_header,
                        t_mpeg1_side_info *g_side_info,
                        t_mpeg1_main_data *g_main_data,
                        t_bit_main_data *bitmain,
                        t_file *fp);
void   MPG_Read_Ancillary(void);
UINT32 MPG_Get_Main_Bits(t_bit_main_data *bitmain, UINT32 number_of_bits);
UINT32 MPG_Get_Main_Bit(t_bit_main_data *bitmain);
STATUS MPG_Set_Main_Pos(t_bit_main_data *bitmain, UINT32 bit_pos);
UINT32 MPG_Get_Main_Pos(const t_bit_main_data *bitmain);

/* Global static values */
extern UINT32 g_mpeg1_bitrates[3][15];
extern UINT32 g_sampling_frequency[3];
extern t_sf_band_indices g_sf_band_indices[3];

#endif /* _MPG_MAIN_H_ */
