// File automatically generated by ESPAM

#ifndef Hybrid2L_H
#define Hybrid2L_H

#include "Hybrid2L_Base.h"

class Hybrid2L : public Hybrid2L_Base
{
    public:
        Hybrid2L(Id n, Hybrid2L_Ports *ports);
        virtual ~Hybrid2L();

        void main();
};
#endif // Hybrid2L_H
