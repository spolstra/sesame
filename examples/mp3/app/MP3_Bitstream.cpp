/******************************************************************************
*
* Filename: MPG_Bitstream.c
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This file contains an abstraction of the bitstream I/O used
*              by the MPEG decoder. It will map high-level calls to the
*              appropiate file I/O provided by the underlying OS.
*
******************************************************************************/

/* Include files */
#include <stdlib.h>
#include <string>

#include "MP3_Bitstream.h"
#include "MP3_Huffman_Table.h"

using namespace std;

/******************************************************************************
*
* Name: MPG_Set_Song
* Author: Peter van Stralen (p.vanstralen@uva.nl)
* Description: A function that opens the file from which the bits are read.
*              It is assumed that this function is called before any other
*              functions in this file to save out an if function in each
               function
* Parameters: Filename
* Return value: The file pointer and some initialized buffers for samples
*
******************************************************************************/
void MPG_Set_Song(std::string filename,
                  t_file *stream,
                  t_bit_main_data *bit_main)
{
    size_t pos;

    if ((stream->fp = fopen(filename.c_str(), "r")) == NULL) {
        EXIT("Cannot open mp3 file \"%s\"\n", filename.c_str());
    }

    pos = filename.rfind(".");

    stream->fname = filename.substr(0, pos);
    stream->fname += ".raw";

#if 0
    DBG("Starting decode, Layer: %d, Rate: %6d, Sfreq: %05d",
        g_frame_header->layer,
        g_mpeg1_bitrates[g_frame_header->layer - 1][g_frame_header->bitrate_index],
        g_sampling_frequency[g_frame_header->sampling_frequency]);
#endif
    MPG_Decode_L3_Init_Song(bit_main);
}


/******************************************************************************
*
* Name: MPG_Get_Byte
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This file returns the next byte from the bitstream, or EOF.
*              If we're not on an byte-boundary the bits remaining until
*              the next boundary are discarded before getting that byte.
* Parameters: File pointer
* Return value: The next byte in bitstream in the lowest 8 bits, or C_MPG_EOF.
*
******************************************************************************/
UINT32
MPG_Get_Byte(t_file *stream)
{
    UINT32 val;

    /* Get byte */
    val = fgetc(stream->fp) & 0xff;

    /* EOF? */
    if (feof(stream->fp)) {
        val = C_MPG_EOF;
    }

    /* Done */
    return (val);
}


/******************************************************************************
*
* Name: MPG_Get_Bytes
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This file reads 'no_of_bytes' bytes of data from the input
*              stream into 'data_vec[]'.
* Parameters: File pointer, number of bytes to read, vector pointer where to
              store them.
* Return value: OK or ERROR if the operation couldn't be performed.
*
******************************************************************************/
STATUS
MPG_Get_Bytes(t_file *stream, UINT32 no_of_bytes, UINT32 data_vec[])
{
    unsigned int i;
    UINT32 val;

    for (i = 0; i < no_of_bytes; i++) {
        val = MPG_Get_Byte(stream);

        if (val == C_MPG_EOF) {
            return (C_MPG_EOF);
        } else {
            data_vec[i] = val;
        }
    }

    return (OK);

}


/******************************************************************************
*
* Name: MPG_Get_Filepos
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This function returns the current file position in bytes.
* Parameters: File pointer
* Return value: File pos in bytes, or 0 if no file open.
*
******************************************************************************/
UINT32
MPG_Get_Filepos(t_file *stream)
{

    /* File open? */
    if (stream->fp == (FILE *) NULL) {
        return (0);
    }

    if (feof(stream->fp)) {
        return (C_MPG_EOF);
    } else {
        return ((UINT32) ftell(stream->fp));
    }

}

/******************************************************************************
*
* Name: MPG_FEOF
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This function checks if we are at the EOF.
* Parameters: File pointer
* Return value: 1 if file is at the end, or 0 if the file is not at the end.
*
******************************************************************************/
UINT32
MPG_FEOF(t_file *stream)
{

    /* File open? */
    if (stream->fp == (FILE *) NULL) {
        return 1;
    }

    if (feof(stream->fp)) {
        return 1;
    } else {
        return ((UINT32) ftell(stream->fp)) == MPG_Get_Filesize(stream);
    }

}

/******************************************************************************
*
* Name: MPG_Get_Filesize
* Author: Krister Lagerstr�m (krister@kmlager.com)
* Description: This function returns the current file size in bytes.
* Parameters: File pointer
* Return value: File size in bytes, or 0 if no file open.
*
******************************************************************************/
UINT32
MPG_Get_Filesize(t_file *stream)
{
    UINT32 curr_pos, size;


    /* File open? */
    if (stream->fp == (FILE *) NULL) {
        return (0);
    }

    curr_pos = MPG_Get_Filepos(stream);

    fseek(stream->fp, 0, SEEK_END);

    size = ftell(stream->fp);

    fseek(stream->fp, curr_pos, SEEK_SET);

    return (size);

}
