#ifndef _INC_MP3DTAB_H_
#define _INC_MP3DTAB_H_

extern FLOAT32 g_synth_n_win[64][32];
extern FLOAT32 v_vec[2 /* ch */][1024];

extern FLOAT32 g_synth_dtbl[512];

#endif /* _INC_MP3DTAB_H_ */
