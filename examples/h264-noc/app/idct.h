/***************************************************************************************
 * 	File: 	idct.h
 * 	Description: header file for IDCT module
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef IDCT_H_
#define IDCT_H_

typedef struct IDCTParams
{
	unsigned char i16MB;
	int qPy;
	int qPc;
}IDCTParams;

typedef int	block4x4[4][4];

typedef struct MacroBlock{
	block4x4 Y[16];
	block4x4 U[4];
	block4x4 V[4];
}MacroBlock;

void zigzagscan(int coeffs[16], block4x4 c, unsigned char dc_block);
void prepare_dc_chroma_block(int coeffs[4], block4x4 c);
unsigned short levelScale4x4(unsigned char m, unsigned char i, unsigned char j);
void scale(const IDCTParams *params, block4x4 c, block4x4 out,
		YUV_TYPE yuv, unsigned char dc_block);
void transform4x4(block4x4 in, unsigned char dc_block, block4x4 out);
void Idct(const IDCTParams *params, const transformCoeffType *coeffs, MacroBlock *out);

#endif
