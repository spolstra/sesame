/**********************************************************************mbType*****************
 * 	File: 	cavlc.c
 * 	Description: Implements CAVLC decoder
 * 	Author: Adarsha RaombType
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h" 

//extern FILE *tracefile;

static unsigned char totalCoeffLumaLeft[4];
static unsigned char totalCoeffLumaTop[32 * 4];
static unsigned char totalCoeffChromaLeftU[2];
static unsigned char totalCoeffChromaLeftV[2];
static unsigned char totalCoeffChromaTopU[32 * 2];
static unsigned char totalCoeffChromaTopV[32 * 2];


void cavlc(const cavlcParams *cavlcIps1, const nalUnit *nal, const dword *bitPosIn, transformCoeffType *coeffs, dword *bitPosOut)
{
	unsigned char i4x4;

	cavlcParams *cavlcIps;
        cavlcIps = (cavlcParams *)cavlcIps1;

	if(cavlcIps->i16MB)
	{
		cavlcIps->cavlcType = Intra16x16DCLevel;
		residualBlockCavlc(cavlcIps, (nalUnit *)nal, coeffs, (dword *)bitPosIn, bitPosOut);
	}

	for(i4x4 = 0; i4x4 < 16; ++ i4x4)
	{
		cavlcIps->blkIdx = i4x4;
		cavlcIps->cavlcType = (cavlcIps->i16MB) ?
							Intra16x16ACLevel :  LumaLevel;
		residualBlockCavlc(cavlcIps, (nalUnit *)nal, coeffs, (dword *)bitPosIn, bitPosOut);
	}

	 // chroma DC values.
	cavlcIps->blkIdx = 0;
	cavlcIps->cavlcType = ChromaDCLevelU;
		residualBlockCavlc(cavlcIps, (nalUnit *)nal, coeffs, (dword *)bitPosIn, bitPosOut);
	cavlcIps->cavlcType = ChromaDCLevelV;
		residualBlockCavlc(cavlcIps, (nalUnit *)nal, coeffs, (dword *)bitPosIn, bitPosOut);

	cavlcIps->cavlcType = ChromaACLevelU;
	for(i4x4 = 0; i4x4 < 4; ++i4x4)
	{
		cavlcIps->blkIdx = i4x4;
		residualBlockCavlc(cavlcIps, (nalUnit *)nal, coeffs, (dword *)bitPosIn, bitPosOut);
	} // end of u block

	cavlcIps->cavlcType = ChromaACLevelV;
	for(i4x4 = 0; i4x4 < 4; ++i4x4)
	{
		cavlcIps->blkIdx = i4x4;
		residualBlockCavlc(cavlcIps, (nalUnit *)nal, coeffs, (dword *)bitPosIn, bitPosOut);
	} // end of u block

}

void getTotalCoeff( char nc, unsigned char * _totalCoeff, unsigned char * _trailing1s,
		nalUnit *nal, dword *bitPosIn, dword *bitPosOut) // table 9-5
{
	// nc = -2
	static unsigned char tcTab1[8] = {6, 5, 4, 3, 3, 2, 2, 1}; 
	static unsigned char t1Tab1[8] = {3, 3, 2, 2, 1, 1, 0, 0}; 
	static unsigned char tcTab2[4] = {5, 4, 4, 3}; 
	static unsigned char t1Tab2[4] = {2, 1, 0, 0}; 
	static unsigned char tcTab3[4] = {7, 6, 5, 5}; 
	static unsigned char t1Tab3[4] = {3, 2, 1, 0}; 
	static unsigned char tcTab4[4] = {8, 7, 6, 6}; 
	static unsigned char t1Tab4[4] = {3, 2, 1, 0}; 
	static unsigned char tcTab5[4] = {8, 8, 7, 7}; 
	static unsigned char t1Tab5[4] = {2, 1, 1, 0}; 

	// nc = -1
	static unsigned char tcTab6[4] = {2, 3, 2, 1}; 
	static unsigned char t1Tab6[4] = {0, 3, 1, 0}; 
	
	// 0 <=nc < 2
	static unsigned char tcTab7[4] = {6, 4, 3, 2}; 
	static unsigned char t1Tab7[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab8[4] = {7, 5, 4, 3}; 
	static unsigned char t1Tab8[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab9[4] = {8, 6, 5, 4}; 
	static unsigned char t1Tab9[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab10[4] = {9, 7, 6, 5}; 
	static unsigned char t1Tab10[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab11[8] = {8, 9, 8, 7, 10, 8, 7, 6}; 
	static unsigned char t1Tab11[8] = {0, 2, 1, 0,  3, 2, 1, 0}; 

	static unsigned char tcTab12[8] = {12 ,11 ,10 ,10 ,11 ,10 ,9 ,9 }; 
	static unsigned char t1Tab12[8] = { 3 , 2 , 1 , 0 , 3 , 2 ,1 ,0 }; 

	static unsigned char tcTab13[8] = {14 ,13 ,12 ,12 ,13 ,12 ,11 ,11 }; 
	static unsigned char t1Tab13[8] = { 3 , 2 , 1 , 0 , 3 , 2 , 1 , 0 }; 

	static unsigned char tcTab14[8] = {16 ,15 ,15 ,14 ,15 ,14 ,14 ,13 }; 
	static unsigned char t1Tab14[8] = { 3 , 2 , 1 , 0 , 3 , 2 , 1 , 0 }; 

	static unsigned char tcTab15[4] = {16, 16, 16, 15}; 
	static unsigned char t1Tab15[4] = { 0,  2,  1,  0}; 

	// 2 <=nc < 4
	
	static unsigned char tcTab16[4] = { 6, 3, 3, 1}; 
	static unsigned char t1Tab16[4] = { 3, 2, 1, 0}; 

	static unsigned char tcTab17[4] = {7, 4, 4, 2}; 
	static unsigned char t1Tab17[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab18[4] = {8, 5, 5, 3}; 
	static unsigned char t1Tab18[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab19[4] = { 5, 6, 6, 4}; 
	static unsigned char t1Tab19[4] = { 0, 2, 1, 0}; 

	static unsigned char tcTab20[4] = {9, 7, 7, 6}; 
	static unsigned char t1Tab20[4] = {3, 2, 1, 0}; 

	static unsigned char tcTab21[8] = {11, 9, 9, 8, 10, 8, 8, 7}; 
	static unsigned char t1Tab21[8] = { 3, 2, 1, 0,  3, 2, 1, 0}; 

	static unsigned char tcTab22[8] = {11, 11, 11, 10, 12, 10, 10, 9}; 
	static unsigned char t1Tab22[8] = { 0,  2,  1,  0,  3,  2,  1, 0}; 

	static unsigned char tcTab23[8] = {14, 13, 13, 13, 13, 12, 12, 12}; 
	static unsigned char t1Tab23[8] = { 3,  2,  1,  0,  3,  2,  1,  0}; 

	static unsigned char tcTab24[4] = {15, 15, 15, 14}; 
	static unsigned char t1Tab24[4] = { 1,  0,  2,  1}; 
	
	// 4 <= nc < 8
	
	static unsigned char tcTab25[8] = {7, 6, 5, 4, 3, 2, 1, 0}; 
	static unsigned char t1Tab25[8] = {3, 3, 3, 3, 3, 2, 1, 0}; 

	static unsigned char tcTab26[8] = {5, 5, 4, 4, 3, 8, 3, 2}; 
	static unsigned char t1Tab26[8] = {1, 2, 1, 2, 1, 3, 2, 1}; 

	static unsigned char tcTab27[8] = {3, 7, 7, 2, 9, 6, 6, 1}; 
	static unsigned char t1Tab27[8] = {0, 2, 1, 0, 3, 2, 1, 0}; 

	static unsigned char tcTab28[8] = {7, 6, 9, 5, 10, 8, 8, 4}; 
	static unsigned char t1Tab28[8] = {0, 0, 2, 0,  3, 2, 1, 0}; 

	static unsigned char tcTab29[8] = {12, 11, 10, 9, 11, 10, 9, 8}; 
	static unsigned char t1Tab29[8] = { 3,  2,  1, 0,  3,  2, 1, 0}; 

	static unsigned char tcTab30[8] = {12, 13, 12, 11, 13, 12, 11, 10}; 
	static unsigned char t1Tab30[8] = { 0,  2,  1,  0,  3,  2,  1,  0}; 

	static unsigned char tcTab31[4] = {15, 14, 14, 14, }; 
	static unsigned char t1Tab31[4] = { 1,  0,  3,  2, }; 

	//static unsigned char tcTab32[4] = {16, 15, 15, 15, }; 
	//static unsigned char t1Tab32[4] = { 1,  0,  3,  2, }; 

	unsigned char totalCoeff = 0;
	unsigned char trailing1s = 0;
	unsigned char  bitsRead = 0;
	unsigned short videoData = 0;

	videoData = getVideoData(nal,FLC,0);

	if(nc == -2)
	{
		if(videoData >> 15 & 0x01 )// 1
		{
			totalCoeff = 0;
			trailing1s = 0;
			bitsRead = 1;
		}
		else if(videoData >> 14 & 0x01 )// 01
		{
			totalCoeff = 1;
			trailing1s = 1;
			bitsRead = 2;
		}
		else if(videoData >> 13 & 0x01 )// 001
		{
			totalCoeff = 2;
			trailing1s = 2;
			bitsRead = 3;
		}
		else if(videoData >> 12 & 0x01 ) // 0001 xxx
		{
			unsigned short temp = videoData >> 9 & 0x07;
		 	totalCoeff = tcTab1[temp];
			trailing1s = t1Tab1[temp];
			bitsRead = 7;
				
		}
		else if(videoData >> 11 & 0x01)// 0000 1
		{
			totalCoeff = 3;
			trailing1s = 3;
			bitsRead = 5;
		}
		else if(videoData >> 10 & 0x01)// 0000 01
		{
			totalCoeff = 4;
			trailing1s = 3;
			bitsRead = 6;
		}
		else if(videoData >> 9 & 0x01)// 0000 001x x
		{
			unsigned short temp = videoData >> 7 & 0x03;
		 	totalCoeff = tcTab2[temp];
			trailing1s = t1Tab2[temp];
			bitsRead = 9;
		}
		else if(videoData >> 8 & 0x01)// 0000 0001 xx
		{
			unsigned short temp = videoData >> 6 & 0x03;
		 	totalCoeff = tcTab3[temp];
			trailing1s = t1Tab3[temp];
			bitsRead = 10;
		}
		else if(videoData >> 7 & 0x01)// 0000 0000 1xx
		{
			unsigned short temp = videoData >> 5 & 0x03;
		 	totalCoeff = tcTab4[temp];
			trailing1s = t1Tab4[temp];
			bitsRead = 11;
		}
		else if(videoData >> 6 & 0x01)// 0000 0000 01xx
		{
			unsigned short temp = videoData >> 4 & 0x03;
		 	totalCoeff = tcTab5[temp];
			trailing1s = t1Tab5[temp];
			bitsRead = 12;
		}
		else if((videoData >> 3 & 0x07) == 0x07)// 0000 0000 0011 1
		{
		 	totalCoeff = 8;
			trailing1s = 0;
			bitsRead = 13;
		}
	}		// if nc = -2

	if( nc == -1)
	{
		if(videoData >> 15 & 0x01 )// 1
		{
			totalCoeff = 1;
			trailing1s = 1;
			bitsRead = 1;
		}
		else if(videoData >> 14 & 0x01 )// 01
		{
			totalCoeff = 0;
			trailing1s = 0;
			bitsRead = 2;
		}
		else if(videoData >> 13 & 0x01 )// 001
		{
			totalCoeff = 2;
			trailing1s = 2;
			bitsRead = 3;
		}
		else if(videoData >> 12 & 0x01 ) // 0001 xx
		{
			unsigned short temp = videoData >> 10 & 0x03;
		 	totalCoeff = tcTab6[temp];
			trailing1s = t1Tab6[temp];
			bitsRead = 6;
				
		}
		else if(videoData >> 11 & 0x01)// 0000 1x
		{
			totalCoeff = (videoData >> 10 & 0x01) ? 3 : 4;
			trailing1s = 0;
			bitsRead = 6;
		}
		else if(videoData >> 10 & 0x01)// 0000 01x
		{
			totalCoeff = 3;
			trailing1s = (videoData >> 9 & 0x01) ? 1 : 2;
			bitsRead = 7;
		}
		else if(videoData >> 9 & 0x01)// 0000 001x
		{
			totalCoeff = 4;
			trailing1s = (videoData >> 8 & 0x01) ? 1 : 2;
			bitsRead = 8;
		}
		else // 0000 000
		{
			totalCoeff = 4;
			trailing1s = 3;
			bitsRead = 7;
		}
		
	}// nc = -1

	if(0 <= nc && nc < 2)
	{
		if(videoData >> 15 & 0x01 )// 1
		{
			totalCoeff = 0;
			trailing1s = 0;
			bitsRead = 1;
		}
		else if(videoData >> 14 & 0x01 )// 01
		{
			totalCoeff = 1;
			trailing1s = 1;
			bitsRead = 2;
		}
		else if(videoData >> 13 & 0x01 )// 001
		{
			totalCoeff = 2;
			trailing1s = 2;
			bitsRead = 3;
		}
		else if(videoData >> 12 & 0x01 ) // 0001 xx
		{
			if(videoData >> 11 & 0x01 ) // 0001 1
		 	{
				totalCoeff = 3; 
				trailing1s = 3;
				bitsRead = 5;
			}
			else if(videoData >> 10 & 0x01 ) // 0001 01
			{
				totalCoeff = 1;
				trailing1s = 0;
				bitsRead = 6;
			}
			else // 0001 00
			{
				totalCoeff = 2;
				trailing1s = 1;
				bitsRead = 6;
			}
				
		}

		else if(videoData >> 11 & 0x01)// 0000 1x
		{
			if(videoData >> 10 & 0x01)// 0000 11
			{
				totalCoeff = 4;
				trailing1s = 3;
				bitsRead = 6;
			}
			else if(videoData >> 9 & 0x01)// 0000 101
			{
				totalCoeff = 3;
				trailing1s = 2;
				bitsRead = 7;
			}
			else// 0000 100
			{
				totalCoeff = 5;
				trailing1s = 3;
				bitsRead = 7;
			}
		}
		else if(videoData >> 10 & 0x01)// 0000 01xx
		{
			unsigned short temp = videoData >> 8 & 0x03;
		 	totalCoeff = tcTab7[temp];
			trailing1s = t1Tab7[temp];
			bitsRead = 8;
		}
		else if(videoData >> 9 & 0x01)// 0000 001x x
		{
			unsigned short temp = videoData >> 7 & 0x03;
		 	totalCoeff = tcTab8[temp];
			trailing1s = t1Tab8[temp];
			bitsRead = 9;
		}
		else if(videoData >> 8 & 0x01)// 0000 0001 xx
		{
			unsigned short temp = videoData >> 6 & 0x03;
		 	totalCoeff = tcTab9[temp];
			trailing1s = t1Tab9[temp];
			bitsRead = 10;
		}
		else if(videoData >> 7 & 0x01)// 0000 0000 1xx
		{
			unsigned short temp = videoData >> 5 & 0x03;
		 	totalCoeff = tcTab10[temp];
			trailing1s = t1Tab10[temp];
			bitsRead = 11;
		}
		else if(videoData >> 6 & 0x01)// 0000 0000 01xx x
		{
			unsigned short temp = videoData >> 3 & 0x07;
		 	totalCoeff = tcTab11[temp];
			trailing1s = t1Tab11[temp];
			bitsRead = 13;
		}
		else if(videoData >> 5 & 0x01)// 0000 0000 001x xx
		{
			unsigned short temp = videoData >> 2 & 0x07;
		 	totalCoeff = tcTab12[temp];
			trailing1s = t1Tab12[temp];
			bitsRead = 14;
		}
		else if(videoData >> 4 & 0x01)// 0000 0000 0001 xxx
		{
			unsigned short temp = videoData >> 1 & 0x07;
		 	totalCoeff = tcTab13[temp];
			trailing1s = t1Tab13[temp];
			bitsRead = 15;
		}
		else if(videoData >> 3 & 0x01)// 0000 0000 0000 1xxx
		{
			unsigned short temp = videoData & 0x07;
		 	totalCoeff = tcTab14[temp];
			trailing1s = t1Tab14[temp];
			bitsRead = 16;
		}
		else if(videoData >> 2 & 0x01)// 0000 0000 0000 01xx
		{
			unsigned short temp = videoData & 0x03;
		 	totalCoeff = tcTab15[temp];
			trailing1s = t1Tab15[temp];
			bitsRead = 16;
		}
		else if(videoData >> 1 & 0x01)// 0000 0000 0000 001
		{
		 	totalCoeff = 13;
			trailing1s = 1;
			bitsRead = 15;
		}

	} // if(0 <= nc < 2)

	if ( 2 <= nc && nc < 4)
	{
		if(videoData >> 15 & 0x01 )// 1x
		{
			if(videoData >> 14 & 0x01 )// 11
			{
				totalCoeff = 0;
				trailing1s = 0;
				bitsRead = 2;
			}
			else 		// 10
			{
				totalCoeff = 1;
				trailing1s = 1;
				bitsRead = 2;
			}
		}
		else if(videoData >> 14 & 0x01 )// 01xx
		{
			if(videoData >> 13 & 0x01 ) // 011
		 	{
				totalCoeff = 2; 
				trailing1s = 2;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01 ) // 0101
			{
				totalCoeff = 3;
				trailing1s = 3;
				bitsRead = 4;
			}
			else // 0100
			{
				totalCoeff = 4;
				trailing1s = 3;
				bitsRead = 4;
			}
		}
		else if(videoData >> 13 & 0x01 ) // 001 xxx
		{
			if(videoData >> 12 & 0x01 ) // 0011 x 
		 	{
				if(videoData >> 11 & 0x01 ) // 0011 1 
				{
					totalCoeff = 2; 
					trailing1s = 1;
					bitsRead = 5;
				}
				else // 0011 0 
				{
					totalCoeff = 5; 
					trailing1s = 3;
					bitsRead = 5;
				}
			}
			else // 0010 xx table
			{
				unsigned short temp = videoData >> 10 & 0x03;
				totalCoeff = tcTab16[temp];
				trailing1s = t1Tab16[temp];
				bitsRead = 6;
			}
		}

		else if(videoData >> 12 & 0x01)// 0001 xx table
		{
			unsigned short temp = videoData >> 10 & 0x03;
		 	totalCoeff = tcTab17[temp];
			trailing1s = t1Tab17[temp];
			bitsRead = 6;
		}
		else if(videoData >> 11 & 0x01)// 0000 1xx //table
		{
			unsigned short temp = videoData >> 9 & 0x03;
		 	totalCoeff = tcTab18[temp];
			trailing1s = t1Tab18[temp];
			bitsRead = 7;
		}
		else if(videoData >> 10 & 0x01)// 0000 01xx //table
		{
			unsigned short temp = videoData >> 8 & 0x03;
		 	totalCoeff = tcTab19[temp];
			trailing1s = t1Tab19[temp];
			bitsRead = 8;
		}
		else if(videoData >> 9 & 0x01)// 0000 001x x //table
		{
			unsigned short temp = videoData >> 7 & 0x03;
		 	totalCoeff = tcTab20[temp];
			trailing1s = t1Tab20[temp];
			bitsRead = 9;
		}
		else if(videoData >> 8 & 0x01)// 0000 0001 xxx
		{
			unsigned short temp = videoData >> 5 & 0x07;
		 	totalCoeff = tcTab21[temp];
			trailing1s = t1Tab21[temp];
			bitsRead = 11;
		}
		else if(videoData >> 7 & 0x01)// 0000 0000 1xxx
		{
			unsigned short temp = videoData >> 4 & 0x07;
		 	totalCoeff = tcTab22[temp];
			trailing1s = t1Tab22[temp];
			bitsRead = 12;
		}
		else if(videoData >> 6 & 0x01)// 0000 0000 01xx x
		{
			unsigned short temp = videoData >> 3 & 0x07;
		 	totalCoeff = tcTab23[temp];
			trailing1s = t1Tab23[temp];
			bitsRead = 13;
		}
		else if(videoData >> 5 & 0x01)// 0000 0000 001x x
		{
			// there are multiple cases here.
			if(videoData >> 4 & 0x01)// 0000 0000 0011 x
			{
				totalCoeff = 14;
				trailing1s = (videoData >> 3 & 0x01) ? 0: 2;
				bitsRead = 13;
			}
			else // 0000 0000 0010 xx //table look up
			{
				unsigned short temp = videoData >> 2 & 0x03;
				totalCoeff = tcTab24[temp];
				trailing1s = t1Tab24[temp];
				bitsRead = 14;

			}
		}
		else if(videoData >> 4 & 0x01)// 0000 0000 0001 xx
		{
			unsigned short temp = videoData >> 2 & 0x03;
		 	totalCoeff = 16;
			trailing1s = 3 -temp;
			bitsRead = 14;
		}
		else if(videoData >> 3 & 0x01)// 0000 0000 0000 1
		{
		 	totalCoeff = 15; 
			trailing1s = 3;
			bitsRead = 13;
		}
		
	} // if(2 <= nc < 4)

	if ( 4 <= nc && nc < 8)
	{
		if(videoData >> 15 & 0x01 )// 1xxx table
		{
			unsigned short temp = videoData >> 12 & 0x07;
		 	totalCoeff = tcTab25[temp];
			trailing1s = t1Tab25[temp];
			bitsRead = 4;
		}
		else if(videoData >> 14 & 0x01 )// 01xxx table
		{
			unsigned short temp = videoData >> 11 & 0x07;
		 	totalCoeff = tcTab26[temp];
			trailing1s = t1Tab26[temp];
			bitsRead = 5;
		}
		else if(videoData >> 13 & 0x01 )// 001xxx table
		{
			unsigned short temp = videoData >> 10 & 0x07;
		 	totalCoeff = tcTab27[temp];
			trailing1s = t1Tab27[temp];
			bitsRead = 6;
		}
		else if(videoData >> 12 & 0x01 ) // 0001 xxx table
		{
			unsigned short temp = videoData >> 9 & 0x07;
		 	totalCoeff = tcTab28[temp];
			trailing1s = t1Tab28[temp];
			bitsRead = 7;
		}
		else if(videoData >> 11 & 0x01)// 0000 1xxx
		{
			unsigned short temp = videoData >> 8 & 0x07;
		 	totalCoeff = tcTab29[temp];
			trailing1s = t1Tab29[temp];
			bitsRead = 8;
		}
		else if(videoData >> 10 & 0x01)// 0000 01xxx
		{
			unsigned short temp = videoData >> 7 & 0x07;
		 	totalCoeff = tcTab30[temp];
			trailing1s = t1Tab30[temp];
			bitsRead = 9;
		}
		else if(videoData >> 9 & 0x01)// 0000 001x x
		{
			if(videoData >> 8 & 0x01)// 0000 0011 xx
			{
				if(videoData >> 7 & 0x01)// 0000 0011 1
				{
					totalCoeff = 13;
					trailing1s = 1;
					bitsRead = 9;
				}
				else // 0000 0011 0x
				{
					totalCoeff = (videoData >> 6 & 0x01) ? 13 : 14;
					trailing1s = (videoData >> 6 & 0x01) ?  0 :  1;
					bitsRead = 10;
				}
			}
			else // 0000 0010 xx
			{
				unsigned short temp = videoData >> 6 & 0x03;
				totalCoeff = tcTab31[temp];
				trailing1s = t1Tab31[temp];
				bitsRead = 10;
			}
		}
		else if(videoData >> 8 & 0x01)// 0000 0001 xx
		{
			unsigned short temp = videoData >> 6 & 0x03;
			totalCoeff = tcTab31[temp] + 1;
			trailing1s = t1Tab31[temp];
			bitsRead = 10;
		}
		else if(videoData >> 7 & 0x01)// 0000 0000 1x
		{
		 	totalCoeff =  16;
			trailing1s = (videoData >> 6 & 0x01) ? 2:3;
			bitsRead = 10;
		}
		else if(videoData >> 6 & 0x01)// 0000 0000 01
		{
		 	totalCoeff = 16;
			trailing1s = 0;
			bitsRead = 10;
		}
	} // if(4 <= nc < 8)

	if ( 8 <= nc )
	{
		if((videoData >> 12 & 0xf) == 0x00) // 0000 xx
		{
			if(videoData >> 11) // 0000 1x
			{
				totalCoeff = 0;
				trailing1s = 0;
				bitsRead = (videoData >> 10) ? 6 : 0;
			}
			else  // 0000 0x
			{
				totalCoeff = 1;
				trailing1s = (videoData >> 10);
				bitsRead = 6;
			}
		}
		else
		{
			totalCoeff = (videoData >> 12 & 0x0f) + 1;
			trailing1s = (videoData >> 10 & 0x03);
			bitsRead = 6;
		}
	} // if(8 <= nc )


	* _totalCoeff = totalCoeff;
	* _trailing1s = trailing1s;
	getVideoData(nal,FLC,bitsRead);
}	


void calcNc(cavlcParams *ip, nalUnit *nal, unsigned char *leftContext,
unsigned char *topContext, unsigned char *totalCoeff, unsigned char *trailing1s, dword *bitPosIn, dword *bitPosOut)
{

	NeighborIps n;
	Neighbors nbr;
	char nA, nB, nC = 0;
	unsigned char zeroBlkB ,zeroBlkA, xoff, yoff;

	n.blkIdx = ip->blkIdx;
	n.blkSize = 4;
	n.yuv = ((ip->cavlcType == LumaLevel) || (ip->cavlcType == Intra16x16ACLevel) || 
			 (ip->cavlcType == Intra16x16DCLevel)) ? Y : U;
	n.leftAvailable = ip->leftAvail;
	n.topAvailable = ip->topAvail;
	n.topRightAvailable = 1;
	n.topLeftAvailable = 1;

	nbr = getNeighbors_4x4(n);

	if(ip->cavlcType == ChromaDCLevelU || ip->cavlcType == ChromaDCLevelV)
	{
		nC = -1;
		getTotalCoeff(nC, totalCoeff, trailing1s,nal,bitPosIn, bitPosOut);
		//fprintf(tracefile,"ChromaDC nc = %d, totalCoeff = %d, Trailing1s = %d \n", nC, *totalCoeff, *trailing1s);
	}
	else
	{
		zeroBlkA = nbr.left.mbIdx && (ip->leftSkipped || ip->leftInter);
		zeroBlkB = nbr.top.mbIdx && (ip->topSkipped || ip->topInter);

		xoff = ip->mb_x*4 + (((ip->blkIdx/4)%2)*2+ (ip->blkIdx%4)%2);
		yoff = (((ip->blkIdx/4)/2)*2 + (ip->blkIdx%4)/2);

		if(nbr.left.avail && !zeroBlkA)
			nA = leftContext[yoff];
		else
			nA = 0;

		if(nbr.top.avail && !zeroBlkB)
			nB = topContext[xoff];
		else
			nB = 0;
		
		if(nbr.top.avail && nbr.left.avail)
			nC = (nA + nB + 1) >> 1;
		else
			nC = nA + nB;

		getTotalCoeff(nC, totalCoeff, trailing1s,nal, bitPosIn, bitPosOut);

		//fprintf(tracefile,"blkIdx = %d, nA = %d , nB = %d\n",ip->blkIdx, nA,nB);
		//fprintf(tracefile,"nc = %d , totalCoeff = %d, trailing1s = %d\n",nC,*totalCoeff, *trailing1s);
	}
}


unsigned char getTotalZeros( unsigned char totalCoeff, unsigned char maxNumCoeff,
		nalUnit *nal, dword *bitPosIn, dword *bitPosOut) // table 9-7, 9-8, 9-9
{
	unsigned char totalZeros = 0;
	unsigned char bitsRead = 0;
	unsigned short videoData = 0;

	videoData = getVideoData(nal,FLC,0);

	if(maxNumCoeff == 4) // 2x2 chroma
	{
		if(totalCoeff == 3)
		{
			totalZeros = !(videoData >> 15 & 0x01);
			bitsRead = 1;
		}
		else if(totalCoeff == 2)
		{
			if(videoData >> 15 & 0x01)
			{
				totalZeros = 0;
				bitsRead = 1;
			}
			else
			{
				totalZeros = (videoData >> 14 & 0x01) ? 1 : 2;
				bitsRead = 2;
			}
		}
		else
		{
			if(videoData >> 15 & 0x01)
			{
				totalZeros = 0;
				bitsRead = 1;
			}
			else if(videoData >> 14 & 0x01)
			{
				totalZeros = 1;
				bitsRead = 2;
			}
			else
			{
				totalZeros = (videoData >> 13 & 0x01) ? 2 : 3;
				bitsRead = 3;
			}
		}
	}// max coeff = 4
	else
	{
		if(totalCoeff == 1)
		{
			if(videoData >> 15 & 0x01)
			{
				totalZeros = 0;
				bitsRead = 1;
			}
			else if(videoData >> 14 & 0x01)
			{
				totalZeros = (videoData >> 13 & 0x01) ? 1 : 2;
				bitsRead = 3;
			}
			else if(videoData >> 13 & 0x01)
			{
				totalZeros = (videoData >> 12 & 0x01) ? 3 : 4;
				bitsRead = 4;
			}
			else if(videoData >> 12 & 0x01)
			{
				totalZeros = (videoData >> 11 & 0x01) ? 5 : 6;
				bitsRead = 5;
			}
			else if(videoData >> 11 & 0x01)
			{
				totalZeros = (videoData >> 10 & 0x01) ? 7 : 8;
				bitsRead = 6;
			}
			else if(videoData >> 10 & 0x01)
			{
				totalZeros = (videoData >> 9 & 0x01) ? 9 : 10;
				bitsRead = 7;
			}
			else if(videoData >> 9 & 0x01)
			{
				totalZeros = (videoData >> 8 & 0x01) ? 11 : 12;
				bitsRead = 8;
			}
			else if(videoData >> 8 & 0x01)
			{
				totalZeros = (videoData >> 7 & 0x01) ? 13 : 14;
				bitsRead = 9;
			}
			else if(videoData >> 7 & 0x01)
			{
				totalZeros = 15;
				bitsRead = 9;
			}
			else
			{
				totalZeros = 0;
				bitsRead = 0;
			}
		}

		if(totalCoeff == 2)
		{
			if(videoData >> 15 & 0x01)
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: totalZeros = 3;break;
					case 1: totalZeros = 2;break;
					case 2: totalZeros = 1;break;
					case 3: totalZeros = 0;break;
				}
				bitsRead = 3;
			}
			else if(videoData >> 14 & 0x01)
			{ 		// 3 cases
				if(videoData >> 13 & 0x01) // 011
				{
					totalZeros = 4;
					bitsRead = 3;
				}
				else
				{
					totalZeros = (videoData >> 12 & 0x01) ? 5 : 6;
					bitsRead = 4;
				}
			}
			else if(videoData >> 13 & 0x01) //001x
			{ 		
				totalZeros = (videoData >> 12 & 0x01) ? 7 : 8;
				bitsRead = 4;
			}
			else if(videoData >> 12 & 0x01) //0001 x
			{
				totalZeros = (videoData >> 11 & 0x01) ? 9 : 10;
				bitsRead = 5;
			}
			else if(videoData >> 11 & 0x01) // 0000 1x
			{ 			
				totalZeros = (videoData >> 10 & 0x01) ? 11 : 12;
				bitsRead = 6;
			}
			else // 0000 0x
			{
				totalZeros = (videoData >> 10 & 0x01) ? 13 : 14;
				bitsRead = 6;
			}
		} // ttlcoeff = 2
		if(totalCoeff == 3)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: totalZeros = 6;break;
					case 1: totalZeros = 3;break;
					case 2: totalZeros = 2;break;
					case 3: totalZeros = 1;break;
				}
				bitsRead = 3;
			}
			else if(videoData >> 14 & 0x01) //01xx
			{ 		// 3 cases
				if(videoData >> 13 & 0x01) // 011
				{
					totalZeros = 7;
					bitsRead = 3;
				}
				else
				{
					totalZeros = (videoData >> 12 & 0x01) ? 0 : 4;
					bitsRead = 4;
				}
			}
			else if(videoData >> 13 & 0x01) // 001x 
			{ 		
				totalZeros = (videoData >> 12 & 0x01) ? 5 : 8;
				bitsRead = 4;
			}
			else if(videoData >> 12 & 0x01) // 0001 x
			{
				totalZeros = (videoData >> 11 & 0x01) ? 9 : 10;
				bitsRead = 5;
			}
			else if(videoData >> 11 & 0x01) // 0000 1
			{ 		
				totalZeros = 12;
				bitsRead = 5;
			}
			else // 0000 0x
			{
				totalZeros = (videoData >> 10 & 0x01) ? 11 : 13;
				bitsRead = 6;
			}
		}
		if(totalCoeff == 4)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: totalZeros = 6;break;
					case 1: totalZeros = 5;break;
					case 2: totalZeros = 4;break;
					case 3: totalZeros = 1;break;
				}
				bitsRead = 3;
			}
			else if(videoData >> 14 & 0x01) //01xx
			{ 		// 3 cases
				if(videoData >> 13 & 0x01) // 011
				{
					totalZeros = 8;
					bitsRead = 3;
				}
				else
				{
					totalZeros = (videoData >> 12 & 0x01) ? 2 : 3;
					bitsRead = 4;
				}
			}
			else if(videoData >> 13 & 0x01) // 001x 
			{ 		
				totalZeros = (videoData >> 12 & 0x01) ? 7 : 9;
				bitsRead = 4;
			}
			else if(videoData >> 12 & 0x01) // 0001 x
			{
				totalZeros = (videoData >> 11 & 0x01) ? 0 : 10;
				bitsRead = 5;
			}
			else // 0000 x 
			{ 		
				totalZeros = (videoData >> 11 & 0x01) ? 11 : 12;
				bitsRead = 5;
			}
		}
		if(totalCoeff == 5)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: totalZeros = 6;break;
					case 1: totalZeros = 5;break;
					case 2: totalZeros = 4;break;
					case 3: totalZeros = 3;break;
				}
				bitsRead = 3;
			}
			else if(videoData >> 14 & 0x01) //01xx
			{ 		// 3 cases
				if(videoData >> 13 & 0x01) // 011
				{
					totalZeros = 7;
					bitsRead = 3;
				}
				else
				{
					totalZeros = (videoData >> 12 & 0x01) ? 0 : 1;
					bitsRead = 4;
				}
			}
			else if(videoData >> 13 & 0x01) // 001x 
			{ 		
				totalZeros = (videoData >> 12 & 0x01) ? 2 : 8;
				bitsRead = 4;
			}
			else if(videoData >> 12 & 0x01) // 0001 
			{
				totalZeros = 10;
				bitsRead = 4;
			}
			else // 0000 x 
			{ 		
				totalZeros = (videoData >> 11 & 0x01) ? 9 : 11;
				bitsRead = 5;
			}
		}
		if(totalCoeff == 6)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: totalZeros = 5;break;
					case 1: totalZeros = 4;break;
					case 2: totalZeros = 3;break;
					case 3: totalZeros = 2;break;
				}
				bitsRead = 3;
			}
			else if(videoData >> 14 & 0x01) //01x
			{ 		
				totalZeros = (videoData >> 13 & 0x01) ? 6 : 7;
				bitsRead = 3;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 9;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01) // 0001 
			{
				totalZeros = 8;
				bitsRead = 4;
			}
			else if(videoData >> 11 & 0x01) // 0000 1 
			{
				totalZeros = 1;
				bitsRead = 5;
			}
			else // 0000 0x
			{ 		
				totalZeros = (videoData >> 10 & 0x01) ? 0 : 10;
				bitsRead = 6;
			}
		}
		if(totalCoeff == 7)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				if(videoData >> 14 & 0x01) // 11
				{
					totalZeros = 5;
					bitsRead = 2;
				}
				else
				{
					totalZeros = (videoData >> 13 & 0x01) ? 2 : 3;
					bitsRead = 3;
				}
			}
			else if(videoData >> 14 & 0x01) //01x
			{ 		
				totalZeros = (videoData >> 13 & 0x01) ? 4 : 6;
				bitsRead = 3;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 8;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01) // 0001 
			{
				totalZeros = 7;
				bitsRead = 4;
			}
			else if(videoData >> 11 & 0x01) // 0000 1 
			{
				totalZeros = 1;
				bitsRead = 5;
			}
			else // 0000 0x
			{ 		
				totalZeros = (videoData >> 10 & 0x01) ? 0 : 9;
				bitsRead = 6;
			}
		}
		if(totalCoeff == 8)
		{
			if(videoData >> 15 & 0x01) //1x
			{
				totalZeros = (videoData >> 14 & 0x01) ? 4 : 5;
				bitsRead = 2;
			}
			else if(videoData >> 14 & 0x01) //01x
			{ 		
					totalZeros = (videoData >> 13 & 0x01) ? 3 : 6;
					bitsRead = 3;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 7;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01) // 0001 
			{
				totalZeros = 1;
				bitsRead = 4;
			}
			else if(videoData >> 11 & 0x01) // 0000 1 
			{
				totalZeros = 2;
				bitsRead = 5;
			}
			else // 0000 0x
			{ 		
				totalZeros = (videoData >> 10 & 0x01) ? 0 : 8;
				bitsRead = 6;
			}
		}
		if(totalCoeff == 9)
		{
			if(videoData >> 15 & 0x01) //1x
			{
				totalZeros = (videoData >> 14 & 0x01) ? 3 : 4;
				bitsRead = 2;
			}
			else if(videoData >> 14 & 0x01) //01
			{ 		
				totalZeros = 6; 
				bitsRead = 2;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 5;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01) // 0001 
			{
				totalZeros = 2;
				bitsRead = 4;
			}
			else if(videoData >> 11 & 0x01) // 0000 1 
			{
				totalZeros = 7;
				bitsRead = 5;
			}
			else // 0000 0x
			{ 		
				totalZeros = (videoData >> 10 & 0x01) ? 0 : 1;
				bitsRead = 6;
			}
		}
		if(totalCoeff == 10)
		{
			if(videoData >> 15 & 0x01) //1x
			{
				totalZeros = (videoData >> 14 & 0x01) ? 3 : 4;
				bitsRead = 2;
			}
			else if(videoData >> 14 & 0x01) //01
			{ 		
				totalZeros = 5; 
				bitsRead = 2;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 2;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01) // 0001 
			{
				totalZeros = 6;
				bitsRead = 4;
			}
			else // 0000 x
			{ 		
				totalZeros = (videoData >> 11 & 0x01) ? 0 : 1;
				bitsRead = 5;
			}
		}
		if(totalCoeff == 11)
		{
			if(videoData >> 15 & 0x01) //1
			{
				totalZeros = 4;  
				bitsRead = 1;
			}
			else if(videoData >> 14 & 0x01) //01x
			{ 		
				totalZeros = (videoData >> 13 & 0x01) ? 5 : 3;
				bitsRead = 3;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 2;
				bitsRead = 3;
			}
			else // 000x
			{ 		
				totalZeros = (videoData >> 12 & 0x01) ? 1 : 0;
				bitsRead = 4;
			}
		}
		if(totalCoeff == 12)
		{
			if(videoData >> 15 & 0x01) //1
			{
				totalZeros = 3;  
				bitsRead = 1;
			}
			else if(videoData >> 14 & 0x01) //01
			{ 		
				totalZeros = 2; 
				bitsRead = 2;
			}
			else if(videoData >> 13 & 0x01) // 001 
			{ 		
				totalZeros = 4;
				bitsRead = 3;
			}
			else // 000x
			{ 		
				totalZeros = (videoData >> 12 & 0x01) ? 1 : 0;
				bitsRead = 4;
			}
		}
		if(totalCoeff == 13)
		{
			if(videoData >> 15 & 0x01) //1
			{
				totalZeros = 2;  
				bitsRead = 1;
			}
			else if(videoData >> 14 & 0x01) //01
			{ 		
				totalZeros = 3; 
				bitsRead = 2;
			}
			else // 00x
			{ 		
				totalZeros = (videoData >> 13 & 0x01) ? 1 : 0;
				bitsRead = 3;
			}
		}
		if(totalCoeff == 14)
		{
			if(videoData >> 15 & 0x01) //1
			{
				totalZeros = 2;  
				bitsRead = 1;
			}
			else // 0x
			{ 		
				totalZeros = (videoData >> 14 & 0x01) ? 1 : 0;
				bitsRead = 2;
			}
		}
		if(totalCoeff == 15)
		{
			totalZeros = (videoData >> 15 & 0x01); //x
			bitsRead = 1;
		}
	} // if maxcoeff = 16

	//fprintf(tracefile,"total zeros = %d\n",totalZeros);

	getVideoData(nal,FLC,bitsRead);

	return totalZeros;
}

unsigned char getRunBefore(unsigned char zerosLeft, nalUnit *nal, dword *bitPosIn, dword *bitPosOut) // table 9-10
{
	unsigned char runBefore;
	unsigned char bitsRead;
	unsigned short videoData;

	videoData = getVideoData(nal,FLC,0);
	bitsRead = 0;

		if(zerosLeft > 6)
		{
			if(videoData >> 15 & 0x01) //1xx
			{ // 
				switch(videoData >> 13 & 0x03)
				{
					case 0: runBefore = 3; break;
					case 1: runBefore = 2; break;
					case 2: runBefore = 1; break;
					case 3: runBefore = 0; break;
				}
				bitsRead = 3;
			}
			else if(videoData >> 14 & 0x01) //01x
			{
				runBefore = (videoData >> 13 & 0x01) ? 4 : 5;
				bitsRead = 3;
			}
			else if(videoData >> 13 & 0x01) //001
			{
				runBefore = 6;
				bitsRead = 3;
			}
			else if(videoData >> 12 & 0x01) //0001
			{
				runBefore = 7;
				bitsRead = 4;
			}
			else if(videoData >> 11 & 0x01) //0000 1
			{
				runBefore = 8;
				bitsRead = 5;
			}
			else if(videoData >> 10 & 0x01) //0000 01
			{
				runBefore = 9;
				bitsRead = 6;
			}
			else if(videoData >> 9 & 0x01) //0000 001
			{
				runBefore = 10;
				bitsRead = 7;
			}
			else if(videoData >> 8 & 0x01) //0000 0001
			{
				runBefore = 11;
				bitsRead = 8;
			}
			else if(videoData >> 7 & 0x01) //0000 0000 1
			{
				runBefore = 12;
				bitsRead = 9;
			}
			else if(videoData >> 6 & 0x01) //0000 0000 01
			{
				runBefore = 13;
				bitsRead = 10;
			}
			else if(videoData >> 5 & 0x01) //0000 0000 001
			{
				runBefore = 14;
				bitsRead = 11;
			}
		}
		if(zerosLeft == 6)
		{
			if(videoData >> 15 & 0x01) //1xx
			{ // 
				if(videoData >> 14 & 0x01)
				{
					runBefore = 0;
					bitsRead = 2;
				}
				else
				{
					runBefore = (videoData >> 13 & 0x01) ? 5 : 6;
					bitsRead = 3;
				}
			}
			else //0xx
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: runBefore = 1; break;
					case 1: runBefore = 2; break;
					case 2: runBefore = 4; break;
					case 3: runBefore = 3; break;
				}
				bitsRead = 3;
			}
		}
		if(zerosLeft == 5)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				runBefore = (videoData >> 14 & 0x01) ? 0 : 1;
				bitsRead = 2;
			}
			else //0xx
			{
				switch(videoData >> 13 & 0x03)
				{
					case 0: runBefore = 5; break;
					case 1: runBefore = 4; break;
					case 2: runBefore = 3; break;
					case 3: runBefore = 2; break;
				}
				bitsRead = 3;
			}
		}
		if(zerosLeft == 4)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				runBefore = (videoData >> 14 & 0x01) ? 0 : 1;
				bitsRead = 2;
			}
			else //0xx
			{
				if(videoData >> 14 & 0x01)
				{
					runBefore = 2;
					bitsRead = 2;
				}
				else
				{
					runBefore = (videoData >> 13 & 0x01) ? 3:4;
					bitsRead = 3;
				}
			}
		}
		if(zerosLeft == 3)
		{
			switch(videoData >> 14 & 0x03) //1xx
			{
				case 0: runBefore = 3; break;
				case 1: runBefore = 2; break;
				case 2: runBefore = 1; break;
				case 3: runBefore = 0; break;
			}
			bitsRead = 2;
		}
		if(zerosLeft == 2)
		{
			if(videoData >> 15 & 0x01) //1xx
			{
				runBefore = 0;
				bitsRead = 1;
			}
			else
			{
				runBefore = (videoData >> 14 & 0x01) ? 1 : 2;
				bitsRead = 2;
			}
		}
		if(zerosLeft == 1)
		{
			runBefore = !(videoData >> 15 & 0x01);
			bitsRead = 1;
		}
		//fprintf(tracefile,"run before = %d\n",runBefore);

		getVideoData(nal,FLC,bitsRead);
	return runBefore;
}

unsigned char getLevelPrefix(nalUnit *nal, dword *bitPosIn, dword *bitPosOut)  // table 9-6
{
	unsigned char levelPrefix;
	unsigned char bitsRead;
	unsigned short videoData;

	videoData = getVideoData(nal,FLC,0);

	for(bitsRead = 1; bitsRead <= 16; ++bitsRead)
	{
		if(videoData & 0X8000)
			break;
		videoData = videoData << 1;
	}
	levelPrefix = bitsRead - 1;

	getVideoData(nal,FLC,bitsRead);
	return levelPrefix;
}

void calcLevel(unsigned char totalCoeff,  unsigned char trailing1s, short* level, nalUnit *nal, 
               dword *bitPosIn, dword *bitPosOut)
{
	unsigned char suffixLength = 0;
	unsigned char levelPrefix = 0;
	unsigned char levelSuffixSize = 0;
	unsigned char levelSuffix = 0;
	short i = 0;
	unsigned short videoData = 0;
	short levelCode = 0;

	for(i = 0; i< 16; ++i)
		level[i]  = 0;

	switch(trailing1s)
	{
		case 1: videoData = getVideoData(nal,FLC,1);
				level[0] = (videoData >> 15 &0x01) ? -1 : 1;
				//fprintf(tracefile,"trailing 1s sign Flag = %d\n",(videoData >> 15 &0x01) );
				break;
		case 2: videoData = getVideoData(nal,FLC,2);
				level[0] = (videoData >> 15 &0x01) ? -1 : 1;
				level[1] = (videoData >> 14 &0x01) ? -1 : 1;
				//fprintf(tracefile,"trailing 1s sign Flag = %d",(videoData >> 15 &0x03));
				//fprintf(tracefile,"%d\n",(videoData >> 14 &0x01));
				break;
		case 3: videoData = getVideoData(nal,FLC,3);
				level[0] = (videoData >> 15 &0x01) ? -1 : 1;
				level[1] = (videoData >> 14 &0x01) ? -1 : 1;
				level[2] = (videoData >> 13 &0x01) ? -1 : 1;
				//fprintf(tracefile,"trailing 1s sign Flag = %d", (videoData >> 15 &0x01));
				//fprintf(tracefile,"%d", (videoData >> 14 &0x01));
				//fprintf(tracefile,"%d\n", (videoData >> 13 &0x01));
				break;
	}

	suffixLength = (totalCoeff > 10 && trailing1s < 3 ) ? 1 : 0;
	

	for(i = trailing1s ; i < totalCoeff; ++ i)
	{

		levelSuffix = 0;
		levelPrefix = getLevelPrefix(nal,bitPosIn, bitPosOut);

		if(levelPrefix == 14 && suffixLength == 0)
			levelSuffixSize = 4;
		else if( levelPrefix > 15 )
			levelSuffixSize = levelPrefix - 3;
		else
			levelSuffixSize = suffixLength;

		if(levelSuffixSize > 0 || levelPrefix >= 14)
		{
			videoData = getVideoData(nal,FLC,levelSuffixSize);
			levelSuffix = videoData >> (16 - levelSuffixSize);
		}

		levelCode = (MIN(15, levelPrefix) << suffixLength) + levelSuffix;

		if(levelPrefix >= 15 && suffixLength == 0)
			levelCode += 15;

		if(levelPrefix >= 16)
			levelCode += (1<<(levelPrefix -3) - 4096);

		if( i == trailing1s && trailing1s < 3)
			levelCode += 2;

			level[i] = (levelCode % 2) ? ((-levelCode-1)>>1) : ((levelCode + 2) >> 1);

			//fprintf(tracefile,"level[%d] = %d\n",i,level[i]);

		if(suffixLength == 0)
			suffixLength = 1;

		if(abs(level[i]) > (3<<(suffixLength - 1)) && (suffixLength < 6))
				++suffixLength;
	}
}

	////////////////// run decoding //////////////////
void calcRun(unsigned char totalCoeff, unsigned char maxNumCoeff, 
		unsigned short* run, nalUnit *nal, dword *bitPosIn, dword *bitPosOut)
{
	unsigned char zerosLeft;
	int i = 0;

	// first clear the old values
	for(i = 0; i< 16; ++i)
		run[i]  = 0;


	if((totalCoeff == maxNumCoeff) || (totalCoeff == 0))
		zerosLeft = 0;
	else // get total_zeros
		zerosLeft = getTotalZeros(totalCoeff, maxNumCoeff, nal,bitPosIn, bitPosOut);

	for ( i = 0; i < totalCoeff - 1; ++i)
	{ 										// table 9-10
		if(zerosLeft)
			run[i] = getRunBefore(zerosLeft,nal,bitPosIn, bitPosOut);
		else
			run[i] = 0 ;
		
		zerosLeft -= run[i];
	}// end of for loop

}// end of function

void residualBlockCavlc(cavlcParams *ip, nalUnit *nal, transformCoeffType *coeffs, dword *bitPosIn, dword *bitPosOut)
{
	int i;
	short level[16];
	unsigned short run[16];
	int *coeffLevel;
	unsigned char *leftContext;	
	unsigned char *topContext;	
	int xoff, yoff;

	unsigned char totalCoeff;
	unsigned char trailing1s;
	unsigned char coeffNum;
	unsigned char maxNumCoeff;
	unsigned char enable_decode;

	xoff = ip->mb_x*4 + (((ip->blkIdx/4)%2)*2+ (ip->blkIdx%4)%2);
	yoff = (((ip->blkIdx/4)/2)*2 + (ip->blkIdx%4)/2);

	switch(ip->cavlcType)
	{
		case LumaLevel:
			leftContext = totalCoeffLumaLeft;
			topContext = totalCoeffLumaTop;
			enable_decode = ip->cbpLuma & (1<<(ip->blkIdx/4));
			coeffLevel = coeffs->lumaAC[ip->blkIdx];
			maxNumCoeff = 16;
		break;
		case Intra16x16DCLevel:
			leftContext = totalCoeffLumaLeft;
			topContext = totalCoeffLumaTop;
			enable_decode = 1; 
			coeffLevel = coeffs->lumaDC;
			maxNumCoeff = 16;
		break;
		case Intra16x16ACLevel:
			leftContext = totalCoeffLumaLeft;
			topContext = totalCoeffLumaTop;
			enable_decode = ip->cbpLuma & (1<<(ip->blkIdx/4));
			coeffLevel = coeffs->lumaAC[ip->blkIdx];
			maxNumCoeff = 15;
		break;
		case ChromaDCLevelU:
			leftContext = 0;
			topContext = 0;
			enable_decode = (ip->cbpChroma & 0x03);
			coeffLevel = coeffs->chromaDCU;
			maxNumCoeff = 4;
		break;
		case ChromaDCLevelV:
			leftContext = 0;
			topContext = 0;
			enable_decode = (ip->cbpChroma & 0x03);
			coeffLevel = coeffs->chromaDCV;
			maxNumCoeff = 4;
		break;
		case ChromaACLevelU:
			leftContext = totalCoeffChromaLeftU;
			topContext = totalCoeffChromaTopU;
			enable_decode = (ip->cbpChroma & 0x02);
			coeffLevel = coeffs->chromaACU[ip->blkIdx];
			maxNumCoeff = 15;
		break;
		case ChromaACLevelV:
			leftContext = totalCoeffChromaLeftV;
			topContext = totalCoeffChromaTopV;
			enable_decode = (ip->cbpChroma & 0x02);
			maxNumCoeff = 15;
			coeffLevel = coeffs->chromaACV[ip->blkIdx];
		break;
	}
	
	for(i = 0; i < maxNumCoeff ; ++i)
		coeffLevel[i] = 0;

	if(enable_decode)
	{
		calcNc(ip, nal, leftContext, topContext,  &totalCoeff, &trailing1s,bitPosIn, bitPosOut);
		if(leftContext && (ip->cavlcType != Intra16x16DCLevel))
			leftContext[yoff] = totalCoeff;
		if(topContext && (ip->cavlcType != Intra16x16DCLevel))
			topContext[xoff] = totalCoeff;

		calcLevel(totalCoeff, trailing1s, level, nal,bitPosIn, bitPosOut);
		calcRun(totalCoeff,maxNumCoeff, run,nal,bitPosIn, bitPosOut);

		coeffNum = -1;
		for( i = totalCoeff-1 ; i >= 0; --i)
		{
			coeffNum += run[i] + 1;
			coeffLevel[coeffNum] = level[i];
		}
	}
	else
	{
		if(leftContext)
			leftContext[yoff] = 0;
		if(topContext)
			topContext[xoff] = 0;
	}
}


availInfo rightBlock(availInfo x)
{
	unsigned char rb = 0;
	unsigned char mbIdx = 0;
	unsigned char valid = 1;
	availInfo op;

	if(x.yuv == Y)
	{
		switch(x.blkIdx) {
		case 0: rb=1;mbIdx=0;break; case 1: rb=4;mbIdx=0;break; 
		case 2: rb=3;mbIdx=0;break; case 3: rb=6;mbIdx=0;valid = 0;break; 
		case 4: rb=5;mbIdx=0;break; case 5: rb=0;mbIdx=1;break;
		case 6: rb=7;mbIdx=0;break; case 7: rb=2;mbIdx=1;valid = 0;break; 
		case 8: rb=9;mbIdx=0;break; case 9: rb=12;mbIdx=0;break; 
		case 10: rb=11;mbIdx=0;break; case 11: rb=14;mbIdx=0;valid = 0;break;
		case 12: rb=13;mbIdx=0;break; case 13: rb=8;mbIdx=1;valid = 0;break; 
		case 14: rb=15;mbIdx=0;break; case 15: rb=10;mbIdx=1;valid = 0;break;
		}
	}
	else
	{
		switch(x.blkIdx) {
		case 0: rb=1;mbIdx=0;break; case 1: rb=0;mbIdx=1;valid = 0;break; 
		case 2: rb=3;mbIdx=0;break; case 3: rb=2;mbIdx=1;valid = 0;break; 
		}
	}
	op.avail = x.avail && valid;
	op.mbIdx = x.mbIdx || mbIdx;
	op.blkIdx = rb;
	return op;
}

availInfo leftBlock(availInfo x)
{
	unsigned char rb = 0;
	unsigned char mbIdx = 0;
	availInfo op;

	if(x.yuv == Y)
	{
		switch(x.blkIdx) {
		case 0: rb=5;mbIdx=1;break; case 1: rb=0;mbIdx=0;break; 
		case 2: rb=7;mbIdx=1;break; case 3: rb=2;mbIdx=0;break; 
		case 4: rb=1;mbIdx=0;break; case 5: rb=4;mbIdx=0;break;
		case 6: rb=3;mbIdx=0;break; case 7: rb=6;mbIdx=0;break; 
		case 8: rb=13;mbIdx=1;break; case 9: rb=8;mbIdx=0;break; 
		case 10: rb=15;mbIdx=1;break; case 11: rb=10;mbIdx=0;break;
		case 12: rb=9;mbIdx=0;break; case 13: rb=12;mbIdx=0;break; 
		case 14: rb=11;mbIdx=0;break; case 15: rb=14;mbIdx=0;break;
		}
	}
	else
	{
		switch(x.blkIdx) {
		case 0: rb=1;mbIdx=1;break; case 1: rb=0;mbIdx=0;break; 
		case 2: rb=3;mbIdx=1;break; case 3: rb=2;mbIdx=0;break; 
		}
	}
	op.blkIdx = rb;
	op.mbIdx = x.mbIdx || mbIdx;
	return op;
}

availInfo topBlock(availInfo x)
{
	unsigned char rb = 0;
	unsigned char mbIdx = 0;
	availInfo op;

	if(x.yuv == Y)
	{
		switch(x.blkIdx) {
		case 0: rb=10;mbIdx=1;break; case 1: rb=11;mbIdx=1;break; 
		case 2: rb=0;mbIdx=0;break; case 3: rb=1;mbIdx=0;break; 
		case 4: rb=14;mbIdx=1;break; case 5: rb=15;mbIdx=1;break;
		case 6: rb=4;mbIdx=0;break; case 7: rb=5;mbIdx=0;break; 
		case 8: rb=2;mbIdx=0;break; case 9: rb=3;mbIdx=0;break; 
		case 10: rb=8;mbIdx=0;break; case 11: rb=9;mbIdx=0;break;
		case 12: rb=6;mbIdx=0;break; case 13: rb=7;mbIdx=0;break; 
		case 14: rb=12;mbIdx=0;break; case 15: rb=13;mbIdx=0;break;
		}
	}
	else
	{
		switch(x.blkIdx) {
		case 0: rb=2;mbIdx=1;break; case 1: rb=3;mbIdx=1;break; 
		case 2: rb=0;mbIdx=0;break; case 3: rb=1;mbIdx=0;break; 
		}
	}
	op.blkIdx = rb;
	op.mbIdx = x.mbIdx || mbIdx;
	return op;
}

Neighbors getNeighbors_4x4(NeighborIps x)
{
		Neighbors nbr;
		availInfo curr = {1,0,x.blkIdx,x.yuv};

		nbr.left = leftBlock(curr);
		nbr.top = topBlock(curr);
		nbr.topRight = rightBlock(topBlock(curr));
		nbr.topLeft = leftBlock(topBlock(curr));

		if(x.blkSize == 8)
			nbr.topRight = rightBlock(rightBlock(topBlock(curr)));
		else if(x.blkSize == 16)
			nbr.topRight = rightBlock(rightBlock(rightBlock(nbr.topRight)));

		nbr.left.avail = !(nbr.left.mbIdx) || x.leftAvailable;
		nbr.top.avail = !(nbr.top.mbIdx) || x.topAvailable;
		nbr.topLeft.avail = !(nbr.topLeft.mbIdx) || (x.leftAvailable && x.topAvailable);
		nbr.topRight.avail &= (!(nbr.topRight.mbIdx) || x.topRightAvailable);

		return nbr;
}
