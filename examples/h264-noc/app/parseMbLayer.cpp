/***************************************************************************************
 * 	File: 	mbLayer.c
 * 	Description: Implements macroblock Layer FSM
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h"

//extern FILE *tracefile;

unsigned char cbpLumaLeft;
unsigned char cbpChromaLeft;
unsigned char leftSkipped;
unsigned char leftInter;
unsigned char leftIntra4x4;
unsigned char leftqPy;
unsigned char leftqPc;
unsigned char leftIntraPredMode4x4[16];

unsigned char cbpLumaTop[MAX_PIC_WIDTH];
unsigned char cbpChromaTop[MAX_PIC_WIDTH];
unsigned char topSkipped[MAX_PIC_WIDTH];
unsigned char topInter[MAX_PIC_WIDTH];
unsigned char topIntra4x4[MAX_PIC_WIDTH];
unsigned char topqPy[MAX_PIC_WIDTH];
unsigned char topqPc[MAX_PIC_WIDTH];
unsigned char topIntraPredMode4x4[64][16];

void nextMb (SpsStruct *sps, MblStruct *MblIp, MblStruct *MblOp)
{
	// context store
		leftSkipped = MblIp->skipped;
		topSkipped[MblIp->mb_x] = MblIp->skipped;

		cbpLumaLeft = MblIp->cbpLuma;
		cbpLumaTop[MblIp->mb_x] = MblIp->cbpLuma;

		cbpChromaLeft = MblIp->cbpChroma;
		cbpChromaTop[MblIp->mb_x] = MblIp->cbpChroma;
		
		leftInter = (MblIp->mbType < I_NxN);
		topInter[MblIp->mb_x] = (MblIp->mbType < I_NxN);

		leftIntra4x4 = (MblIp->mbPredMode == Intra_4x4); 
		topIntra4x4[MblIp->mb_x] =  (MblIp->mbPredMode == Intra_4x4); 

		leftqPy = MblIp->qPy;
		topqPy[MblIp->mb_x] = MblIp->qPy;
		leftqPc = MblIp->qPc;
		topqPc[MblIp->mb_x] = MblIp->qPc;

		// next block calc
		MblOp->mbNum++ ;
		MblOp->mb_y = MblIp->mbNum / sps->picWidthInMbs;
		MblOp->mb_x = MblIp->mbNum % sps->picWidthInMbs;

		MblOp->mbAddrA = MblIp->mbNum - 1;
		MblOp->mbAddrB = MblIp->mbNum - sps->picWidthInMbs;
		MblOp->leftAvail = ((MblIp->mbNum % sps->picWidthInMbs) != 0);
		MblOp->topAvail = (MblIp->mbNum >= sps->picWidthInMbs);
		MblOp->leftSkipped = 	leftSkipped;
		MblOp->topSkipped =  	topSkipped[MblOp->mb_x];
		MblOp->leftInter = 	leftInter;
		MblOp->topInter =  	topInter[MblOp->mb_x];
		MblOp->leftIntra4x4 = 	leftIntra4x4;
		MblOp->topIntra4x4 =  	topIntra4x4[MblOp->mb_x];
		MblOp->cbpLumaLeft =   cbpLumaLeft;
		MblOp->cbpLumaTop =    cbpLumaTop[MblOp->mb_x];
		MblOp->cbpChromaLeft = cbpChromaLeft;
		MblOp->cbpChromaTop =  cbpChromaTop[MblOp->mb_x];
		MblOp->qPyLeft = leftqPy;
		MblOp->qPyTop =  topqPy[MblOp->mb_x];
		MblOp->qPcLeft = leftqPc;
		MblOp->qPcTop =  topqPc[MblOp->mb_x];
}

void parseMacroBlockLayer(nalUnit *nal, SpsStruct *sps, PpsStruct *pps,
		ShStruct *sliceHeader, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut)
{

	unsigned short videoData;
	short mbQpDelta;

	getMbType(nal, sliceHeader, Mbl, bitPosIn, bitPosOut);
	mbPred(nal, sps, pps, sliceHeader, Mbl, bitPosIn, bitPosOut);

	if(Mbl->mbPredMode != Intra_16x16)
		getMappedEg(nal,sps,Mbl, bitPosIn, bitPosOut);

	if(Mbl->cbpChroma > 0 || Mbl->cbpLuma > 0 || Mbl->mbPredMode == Intra_16x16)
	{
			videoData = getVideoData(nal,SE,0);
			mbQpDelta = videoData;

			Mbl->qPy = ((Mbl->qPy + mbQpDelta + 52 + 2*sps->qpBdOffsetY)%(52 + sps->qpBdOffsetY)) 
							- sps->qpBdOffsetY;
			Mbl->qPc = Mbl->qPy + pps->chromaQpIndexOffset;
	}

	//fprintf(tracefile,"mb_num = %lu\n",Mbl->mbNum);
	//fprintf(tracefile,"mb_type = %d\n",Mbl->mbType);
	//fprintf(tracefile,"lumacodedBlockPattern = %d \n",Mbl->cbpLuma);
	//fprintf(tracefile,"chromacodedBlockPattern = %d \n",Mbl->cbpChroma);
	//fprintf(tracefile,"intra4x4_pred_mode = ");
	//for(i = 0; i<16; ++i)
		//fprintf(tracefile," %d",Mbl->Intra4x4PredMode[i]);
	//fprintf(tracefile,"\nintra16x16_pred_mode = %d\n",Mbl->intra16x16PredMode );
	//fprintf(tracefile,"intra_chroma_pred_mode = %d\n", Mbl->intraChromaPredMode);
	//fprintf(tracefile,"MbQpDelta = %d\n",mbQpDelta);
}

void getMbType(nalUnit *nal, ShStruct *sliceHeader, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut)
{
	unsigned char mbType;
	unsigned short videoData;
	unsigned short sliceType = sliceHeader->sliceType;
	
	videoData = getVideoData(nal,UE,0);
	mbType = videoData & 0xff;

	if(sliceType == I_SLICE)
	{
		Mbl->mbType = (MB_TYPE) (mbType + 28);
		if(mbType == 0)
				Mbl->mbPredMode = Intra_4x4;
		else
			Mbl->mbPredMode = Intra_16x16;
	}

	if(Mbl->mbType > I_NxN)
	{
		unsigned char temp = Mbl->mbType - I_16x16_0_0_0;

		if((temp < 4) || ( temp >= 12 && temp < 16))
				Mbl->cbpChroma= 0;
		if((temp >=4 && temp < 8) || ( temp >= 16 && temp < 20))
				Mbl->cbpChroma= 1;
		if((temp >=8 && temp < 12) || ( temp >= 20 && temp < 24))
				Mbl->cbpChroma= 2;

		Mbl->cbpLuma= (Mbl->mbType >= I_16x16_0_0_1 ) ? 15 : 0;
		Mbl->intra16x16PredMode = getI16x16PredMode(Mbl->mbType);
	}
}

void mbPred (nalUnit *nal, SpsStruct *sps, PpsStruct *pps,
		ShStruct *sliceHeader, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut)
{
	unsigned short mbPartIdx;
	unsigned short videoData;
	unsigned char prevIntra4x4PredModeFlag[16];
	unsigned char remIntra4x4PredMode[16];

	if(Mbl->mbPredMode == Intra_4x4 ||
			Mbl->mbPredMode == Intra_8x8 ||
			Mbl->mbPredMode == Intra_16x16)
	{
			if(Mbl->mbPredMode == Intra_4x4)
			{
				for(mbPartIdx = 0; mbPartIdx < 16; ++mbPartIdx)
				{
					videoData = getVideoData(nal,FLC,1);
					prevIntra4x4PredModeFlag[mbPartIdx] = videoData >> 15 & 0x01;
					remIntra4x4PredMode[mbPartIdx] =  -1;

					if(prevIntra4x4PredModeFlag[mbPartIdx] == 0)
					{
						videoData = getVideoData(nal,FLC,3);
						remIntra4x4PredMode[mbPartIdx] = (videoData >> 13 & 0x07);
					}
				}
				IntraPredModeContextInfo x = {Mbl->mb_x, Mbl->leftAvail, Mbl->topAvail,
												Mbl->leftInter, Mbl->topInter, Mbl->leftIntra4x4,
												Mbl->topIntra4x4};
				calcIntra4x4PredMode(x, prevIntra4x4PredModeFlag, remIntra4x4PredMode, 
						Mbl->Intra4x4PredMode);
			}

		videoData = getVideoData(nal,UE,0);
		Mbl->intraChromaPredMode = videoData;
	}
}

void getMappedEg (nalUnit *nal, SpsStruct *sps, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut)
{
	unsigned short videoData;
	unsigned char code;
	unsigned char cbp;
	unsigned char mapTableIntra[64] = { 47, 31, 15, 0, 23, 27, 29, 30,
										7, 11, 13, 14, 39, 43, 45, 46,
										16, 3, 5, 10, 12, 19, 21, 26,
										28, 35, 37, 42, 44, 1, 2, 4,
										8, 17, 18, 20, 24, 6, 9, 22,
										25, 32, 33, 34, 36, 40, 38, 41,
										15, 0, 7, 11, 13, 14, 3, 5,
										10, 12, 1, 2, 4, 8, 6, 9 };
	unsigned char mapTableInter[64] = { 0, 16, 1, 2, 4, 8, 32, 3,
										5, 10, 12, 15, 47, 7, 11, 13,
										14, 6, 9, 31, 35, 37, 42, 44,
										33, 34, 36, 40, 39, 43, 45, 46,
										17, 18, 20, 24, 19, 21, 26, 28,
										23, 27, 29, 30, 22, 25, 38, 41,
										0, 1, 2, 4, 8, 3, 5, 10,
										12, 15, 7, 11, 13, 14, 6, 9 };

	videoData = getVideoData(nal,UE,0);
	code = videoData;

	if(sps->chromaFormatIdc != 0)
	{
		if(Mbl->mbPredMode == Intra_4x4 || Mbl->mbPredMode == Intra_8x8)
			cbp = mapTableIntra[code]; 
		else
			cbp = mapTableInter[code];
	}
	else
	{
		if(Mbl->mbPredMode == Intra_4x4 || Mbl->mbPredMode == Intra_8x8)
			cbp = mapTableIntra[code + 48]; 
		else
			cbp = mapTableInter[code + 48];
	}

	Mbl->cbpLuma = cbp%16;
	Mbl->cbpChroma = cbp/16;
}


void calcIntra4x4PredMode(IntraPredModeContextInfo params, unsigned char prevIntra4x4PredMode[16], 
		unsigned char remIntra4x4PredMode[16], unsigned char Intra4x4PredMode[16])
{
	unsigned char dcPredModePredictedFlag;
	unsigned char blkIdx;
	unsigned char intraPredModeA, intraPredModeB;
	unsigned char predIntra4x4PredMode ;
	unsigned char *leftArray, *topArray;

	NeighborIps n;
	Neighbors nbr;

	n.blkSize = 4;
	n.yuv = Y;
	n.leftAvailable = (params.leftAvailable && !(params.leftInter) && params.leftIntra4x4);
	n.topAvailable = (params.topAvailable && !(params.topInter) && params.topIntra4x4);
	n.topRightAvailable = 1;
	n.topLeftAvailable = 1;

	for(blkIdx = 0; blkIdx < 16; ++blkIdx)
	{
		n.blkIdx = blkIdx;
		nbr = getNeighbors_4x4(n);
		dcPredModePredictedFlag = !(nbr.left.avail) || !(nbr.top.avail);
		leftArray = (nbr.left.mbIdx) ? leftIntraPredMode4x4: Intra4x4PredMode;
		topArray = (nbr.top.mbIdx) ? topIntraPredMode4x4[params.mb_x]: Intra4x4PredMode;

		if(dcPredModePredictedFlag)
		{
			intraPredModeA = 2;
			intraPredModeB = 2;
		}
		else
		{
			intraPredModeA = leftArray[nbr.left.blkIdx];
			intraPredModeB = topArray[nbr.top.blkIdx];
		}

		predIntra4x4PredMode = (intraPredModeB < intraPredModeA) ? intraPredModeB : intraPredModeA;

		if(prevIntra4x4PredMode[blkIdx] == 1)
			Intra4x4PredMode[blkIdx] = predIntra4x4PredMode;
		else
		{
			if( remIntra4x4PredMode[blkIdx] < predIntra4x4PredMode )
				 Intra4x4PredMode[blkIdx] = remIntra4x4PredMode[blkIdx];
			else
				 Intra4x4PredMode[blkIdx] = remIntra4x4PredMode[blkIdx] + 1;
		}

		leftIntraPredMode4x4[blkIdx] = Intra4x4PredMode[blkIdx];
		topIntraPredMode4x4[params.mb_x][blkIdx] = Intra4x4PredMode[blkIdx];
	}
}

unsigned char getI16x16PredMode(MB_TYPE mbType)
{
	unsigned char retVal = 0;
	switch(mbType)
	{
		case I_16x16_0_0_0:  retVal = 0; break;
		case I_16x16_1_0_0:  retVal = 1; break; 
		case I_16x16_2_0_0:  retVal = 2; break;
		case I_16x16_3_0_0:  retVal = 3; break; 
		case I_16x16_0_1_0:  retVal = 0; break; 
		case I_16x16_1_1_0:  retVal = 1; break; 
		case I_16x16_2_1_0:  retVal = 2; break; 
		case I_16x16_3_1_0:  retVal = 3; break; 
		case I_16x16_0_2_0:  retVal = 0; break; 
		case I_16x16_1_2_0:  retVal = 1; break; 
		case I_16x16_2_2_0:  retVal = 2; break; 
		case I_16x16_3_2_0:  retVal = 3; break; 
		case I_16x16_0_0_1:  retVal = 0; break; 
		case I_16x16_1_0_1:  retVal = 1; break; 
		case I_16x16_2_0_1:  retVal = 2; break; 
		case I_16x16_3_0_1:  retVal = 3; break; 
		case I_16x16_0_1_1:  retVal = 0; break; 
		case I_16x16_1_1_1:  retVal = 1; break; 
		case I_16x16_2_1_1:  retVal = 2; break; 
		case I_16x16_3_1_1:  retVal = 3; break; 
		case I_16x16_0_2_1:  retVal = 0; break; 
		case I_16x16_1_2_1:  retVal = 1; break; 
		case I_16x16_2_2_1:  retVal = 2; break; 
		case I_16x16_3_2_1:  retVal = 3; break;
		default: retVal = 0; break;
		}
	return retVal;
}
