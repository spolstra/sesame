/***************************************************************************************
 * 	File: sliceLayer.h	
 * 	Description: Header file for Slice Layer FSM
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef SLICE_LAYER_H_
#define SLICE_LAYER_H_


#define STOP_COUNT 60 

typedef enum SLICE_TYPE_DEF { I_SLICE = 0,
			      B_SLICE,
			      P_SLICE,
			      SP_SLICE,
			      SI_SLICE
	}SLICE_TYPE;

typedef struct ShStruct{
	unsigned short firstMbInSlice;
	SLICE_TYPE sliceType;
	unsigned char picParameterSetId;
	unsigned short frameNum;
	unsigned char fieldPicFlag;
	unsigned char bottomFieldFlag;
	unsigned char idrPicId;
	unsigned short picOrderCntLsb; 
	unsigned char deltaPicOrderCntBottom; 
	unsigned short picOrderCnt[2]; 
	unsigned short redundantPicCnt; 
	unsigned char directSpacialMvPredFlag; 
	unsigned char numRefIdxActiveOverrideFlag; 
	unsigned char numRefIdxL0ActiveMinus1; 
	unsigned char numRefIdxL1ActiveMinus1; 
	unsigned char cabacInitIdc ; 
	unsigned char sliceQpDelta ; 
	unsigned char disableDeblockingFilterIdc; 
	unsigned char sliceAlphaC0OffsetDiv2 ;
	unsigned char sliceBetaOffsetDiv2 ;
	// SEs for ref list reorder
	unsigned char refPicListReorderFlgL0;
	unsigned char refPicListReorderFlgL1;
	unsigned char reorderingOfPicNumIdc;
	unsigned char absDiffPicNumMinus1;
	unsigned char longTermPicNum;
	// SEs for dec ref pic marking
	unsigned char noOutputOfPriorPicsFlag ;	
	unsigned char longTermReferenceFlag ;	
	unsigned char adaptiveRefPicMarkingModeFlag;
	unsigned char memoryManagementControlOperation;
	unsigned char differenceOfPicNumsMinus1 	;
	unsigned char longTermFrameIdx; 	
	unsigned char maxLongTermFrameIdxPlus1;

	char sliceQp;
}ShStruct;

void parseSliceHeader(const nalUnit *nal, 
                      const NalStruct *NalParams,
		      const SpsStruct *sps, 
                      const PpsStruct *pps, 
		      const dword *bitPosIn, 
                      ShStruct *sliceHeader, 
                      dword *bitPosOut, 
                      unsigned char* firstMbFlag);
void refPicListReordering(nalUnit *nal, ShStruct *sliceHeader, dword *bitPosIn, dword *bitPosOut);
void decRefPicMarking(nalUnit *nal, NalStruct *NalParams, ShStruct *sliceHeader, dword *bitPosIn, dword *bitPosOut);
#endif
