#include "decoderCommon.h"

/*
void parseStreamMetaData(SpsStruct *Sps, PpsStruct *Pps)
{
	NalStruct NalParams;
	static nalUnit nal;
	dword bitPos;

	streamBuffer(&nal, &bitPos); 

	streamBuffer(&nal, &bitPos); 
	nalUnitDecode(&nal, &bitPos, &NalParams, &bitPos);
	seqParamSet(&nal, Sps, &bitPos, &bitPos);
		
	streamBuffer(&nal, &bitPos); 
	nalUnitDecode(&nal, &bitPos, &NalParams, &bitPos);
	pictureParamSet(&nal, Pps, &bitPos, &bitPos);
}
*/

void parseStreamMetaData(SFILE *inputStream, SpsStruct *Sps, PpsStruct *Pps)
{
	NalStruct NalParams;
	static nalUnit nal;
	dword bitPos;
        SFILE sstream;

	static FILE *stream=NULL;
        char filename[] = "test.264";
        if( stream==NULL) {
		if ((stream = fopen(filename,"r"))==NULL) {
			printf("\nCannot read input file: %s\n", filename);
		} 
	//printf("\nFile: %s in parseStreamMetaData() opened.\n", filename);
	}

        sstream.pFile = stream;

	streamBuffer(&sstream, &nal, &bitPos, &sstream); 

	streamBuffer(&sstream, &nal, &bitPos, &sstream); 
	nalUnitDecode(&nal, &bitPos, &NalParams, &bitPos);
	seqParamSet(&nal, Sps, &bitPos, &bitPos);
		
	streamBuffer(&sstream, &nal, &bitPos, &sstream); 
	nalUnitDecode(&nal, &bitPos, &NalParams, &bitPos);
	pictureParamSet(&nal, Pps, &bitPos, &bitPos);

	inputStream->pFile = sstream.pFile;
}

void parser(    const nalUnit *nal, 
		const SpsStruct *Sps, 
		const PpsStruct *Pps, 
		const ShStruct *Sh, 
		const dword *bitPosIn, 
		const unsigned char *firstMbFlagIn,
		cavlcParams *cavlcIps, 
		IDCTParams *idctIps, 
		intraParams *intraPredIps, 
		dbParams *dbIps,
		dword *bitPosOut,
		unsigned char *firstMbFlagOut
	    )
{
	static	MblStruct Mbl;

	if(*firstMbFlagIn)
	{
		init_mb((SpsStruct *)Sps, (PpsStruct *)Pps, (ShStruct *)Sh, &Mbl);

	}
	else
		nextMb((SpsStruct *)Sps,&Mbl,&Mbl);

	*firstMbFlagOut = 0;
	parseMacroBlockLayer((nalUnit *)nal, (SpsStruct *)Sps, (PpsStruct *)Pps, (ShStruct *)Sh, &Mbl, (dword *)bitPosIn, bitPosOut);
	getParams(&Mbl, (ShStruct *)Sh, cavlcIps, idctIps, intraPredIps, dbIps);

}

void getParams(MblStruct *Mbl, ShStruct *Sh, cavlcParams *cavlcIps, IDCTParams *idctIps, intraParams *intraIps, dbParams *dbIps)
{
	int i;

	cavlcIps->mb_x = Mbl->mb_x;
	cavlcIps->i16MB = (Mbl->mbPredMode == Intra_16x16); 		
	cavlcIps->leftAvail = Mbl->leftAvail;
	cavlcIps->topAvail = Mbl->topAvail;
	cavlcIps->leftSkipped = Mbl->leftSkipped;
	cavlcIps->topSkipped = Mbl->topSkipped;
	cavlcIps->leftInter = Mbl->leftInter;
	cavlcIps->topInter = Mbl->topInter;
	cavlcIps->cbpLuma = Mbl->cbpLuma;
	cavlcIps->cbpLumaLeft = Mbl->cbpLumaLeft;
	cavlcIps->cbpLumaTop = Mbl->cbpLumaTop;
	cavlcIps->cbpChroma = Mbl->cbpChroma;
	cavlcIps->cbpChromaLeft = Mbl->cbpChromaLeft;
	cavlcIps->cbpChromaTop = Mbl->cbpChromaTop;
	cavlcIps->blkIdx = 0;
	cavlcIps->cavlcType = Intra16x16DCLevel;

	idctIps->i16MB = (Mbl->mbPredMode == Intra_16x16); 
	idctIps->qPy = Mbl->qPy;
	idctIps->qPc = Mbl->qPc;

	intraIps->mb_x = Mbl->mb_x;
	intraIps->i16MB = (Mbl->mbPredMode == Intra_16x16); 
	intraIps->up_avail = Mbl->topAvail;
	intraIps->left_avail = Mbl->leftAvail;
	intraIps->c_ipred_mode = (I_CHROMA_MODES)Mbl->intraChromaPredMode;
	intraIps->i_16_predmode = (I_16_MODE)Mbl->intra16x16PredMode;
	for(i = 0; i<16; ++i)
	 	intraIps->predmode[i] = (I_4X4_MODE)Mbl->Intra4x4PredMode[i];

	dbIps->mb_x = Mbl->mb_x;
	dbIps->qPy  = Mbl->qPy;                     
	dbIps->qPyLeft = Mbl->qPyLeft; 
	dbIps->qPyTop = Mbl->qPyTop;
	dbIps->qPc    = Mbl->qPc; 
	dbIps->qPcLeft = Mbl->qPcLeft; 
	dbIps->qPcTop  = Mbl->qPcTop;
	dbIps->disableDblkFilterIdc = 0;
	dbIps->leftAvail = Mbl->leftAvail; 
	dbIps->topAvail  = Mbl->topAvail;
	dbIps->filterOffsetA = Sh->sliceAlphaC0OffsetDiv2 * 2; 
	dbIps->filterOffsetB = Sh->sliceBetaOffsetDiv2 * 2;
}                              


void printMB(const MacroBlock *macroblock)
{
	int block, row, col;

	static FILE *opFile=NULL;
        char filename[] = "output.yuv";
        if( opFile==NULL) {
		if ((opFile = fopen(filename, "w"))==NULL) {
			printf("\nCannot create/open file: %s\n", filename);
		} 
//	printf("\nFile: %s in printMB() opened.\n", filename);
	}

	for(block=0;block<16;++block)
	{
		for(row=0;row<4;++row)
		{
			for(col=0; col<4;++col)
				fprintf(opFile," %3d ",macroblock->Y[block][row][col]);
			fprintf(opFile,"\n");
		}
		fprintf(opFile,"\n");
	}
	
	for(block=0;block<4;++block)
	{
		for(row=0;row<4;++row)
		{
			for(col=0; col<4;++col)
				fprintf(opFile," %3d ",macroblock->U[block][row][col]);
			fprintf(opFile,"\n");
		}
		fprintf(opFile,"\n");
	}

	for(block=0;block<4;++block)
	{
		for(row=0;row<4;++row)
		{
			for(col=0; col<4;++col)
	 			fprintf(opFile," %3d ",macroblock->V[block][row][col]);
			fprintf(opFile,"\n");
		}
		fprintf(opFile,"\n");
	}
		fprintf(opFile,"\n");
}

/*
void writeMB(MacroBlock *const macroblock, const int frame_num, const int mb_num, FILE *opFile)
{
	int vblock, hblock, row, col;
	int offset, mb_x, mb_y;
	int scan_order[16] = {0,1,4,5,2,3,6,7,8,9,12,13,10,11,14,15};
	char temp;

	mb_x = mb_num%11;
	mb_y = mb_num/11;

	for(vblock=0;vblock<4;++vblock)
		for(row=0;row<4;++row)
		{
			offset = frame_num*(176*144*3/2) + (mb_y*16 + vblock*4 + row)*176 + mb_x*16;
			fseek(opFile, offset, SEEK_SET);
			for(hblock=0;hblock<4;++hblock)
				for(col=0; col<4;++col)
				{
					temp = (char)(macroblock->Y[scan_order[vblock*4+hblock]][row][col]);
	 				fwrite(&temp, 1, 1, opFile);
				}
		}

	for(vblock=0;vblock<2;++vblock)
		for(row=0;row<4;++row)
		{
			offset = frame_num*(176*144*3/2) + (176*144) + 
				(mb_y*8 + vblock*4 + row)*88 + mb_x*8;
			fseek(opFile, offset, SEEK_SET);
			for(hblock=0;hblock<2;++hblock)
				for(col=0; col<4;++col)
				{
					temp = (char)(macroblock->U[scan_order[vblock*4+hblock]][row][col]);
	 				fwrite(&temp, 1, 1, opFile);
				}
		}
	
	for(vblock=0;vblock<2;++vblock)
		for(row=0;row<4;++row)
			offset = frame_num*(176*144*3/2) + (176*144) + (88*72) + 
				(mb_y*8 + vblock*4 + row)*88 + mb_x*8;
			fseek(opFile, offset, SEEK_SET);
			for(hblock=0;hblock<2;++hblock)
				for(col=0; col<4;++col)
				{
					temp = (char)(macroblock->V[scan_order[vblock*4+hblock]][row][col]);
	 				fwrite(&temp, 1, 1, opFile);
				}
}
*/

void printdbParams(dbParams *dbIps)
{
	printf("mb_x                = %d\n", 	dbIps->mb_x);
	printf("qPy                 = %d\n",	dbIps->qPy);
	printf("qPyLeft             = %d\n",	dbIps->qPyLeft);
	printf("qPyTop              = %d\n",	dbIps->qPyTop);
	printf("qPc                 = %d\n",	dbIps->qPc);
	printf("qPcLeft             = %d\n",	dbIps->qPcLeft);
	printf("qPcTop              = %d\n",	dbIps->qPcTop);
	printf("disableDblkFilterIdc= %d\n",	dbIps->disableDblkFilterIdc);
	printf("leftAvail           = %d\n",	dbIps->leftAvail);
	printf("topAvail            = %d\n",	dbIps->topAvail);
	printf("filterOffsetA       = %d\n",	dbIps->filterOffsetA);
	printf("filterOffsetB       = %d\n",	dbIps->filterOffsetB);
}


#undef getVideoData
unsigned short getVideoData1(nalUnit *nal, codeType t, int codeLen, const dword *bitPosIn, dword *bitPosOut)
#define getVideoData(x,y,z) getVideoData1(x,y,z,bitPosIn,bitPosOut)
{
	videoDataStruct videoData;
	offsetStruct offset;
	unsigned short retVal = 0;

	offset.newNalu = 0;
	offset.byteAlign = 0;
	offset.numBits = 0;

	decodeVideoData (nal, &offset,&videoData, (dword *)bitPosIn, bitPosOut);

	if(t == SE)
		retVal = videoData.se;
	else if(t == UE)
		retVal = videoData.ue;
	else
		retVal = videoData.u;

	if(t == FLC)
		offset.numBits = codeLen;
	else
		offset.numBits = videoData.numBits;

	decodeVideoData (nal, &offset, &videoData, (dword *)bitPosIn,  bitPosOut);
	return retVal;
}

void decodeVideoData (nalUnit *nal, offsetStruct *offset, videoDataStruct *videoData, dword *bitPosIn,dword *bitPosOut)
{
	unsigned int buffer = 0;
	unsigned int bits_read, currBytePos, readPtr;
	
	bits_read = offset->numBits;

	*bitPosOut = *bitPosIn + bits_read;
	currBytePos = *bitPosOut/8;
	readPtr = *bitPosOut%8;

	buffer =((unsigned int)nal->x[currBytePos + 0] << 24) + 
			((unsigned int)nal->x[currBytePos + 1] << 16) +
			((unsigned int)nal->x[currBytePos + 2] << 8)  +
			((unsigned int)nal->x[currBytePos + 3]); 

	videoData->u = (unsigned short)(buffer >> (16 - readPtr));
	videoData->numBits = getVLCSymbol(&videoData->ue, videoData->u);

	if(videoData->ue == 0)
		videoData->se = 0;
	else
		if(videoData->ue % 2 )
			videoData->se = (videoData->ue/2 + 1) ;
		else
			videoData->se = -(videoData->ue/2);
}
