/***************************************************************************************
 * 	File: nalUnit.h	
 * 	Description: Header file for NAL unit decoder
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef NAL_UNIT_H_
#define NAL_UNIT_H_

typedef unsigned int dword;
//typedef unsigned long long dword;

typedef struct nalUnit
{
	unsigned char x[5000];
}nalUnit;

typedef struct videoDataStruct{
	unsigned short u;
	unsigned char ue;
	signed char se;
	unsigned int numBits;
}videoDataStruct;

typedef struct offsetStruct{
	unsigned char newNalu;
	unsigned char byteAlign;
	unsigned int numBits;
}offsetStruct;

typedef enum codeType_DEF {SE, UE, FLC} codeType;

typedef struct NalStruct{
	unsigned char forbiddenBit ;
	unsigned char nalRefIdc ;
	unsigned char nalUnitType ;
}NalStruct ;

typedef struct SFILE{
	FILE *pFile;
}SFILE;

//void streamBuffer(nalUnit* nal, dword *bitPosOut);
void streamBuffer(const SFILE *inputStream, nalUnit *nal, dword *bitPosOut, SFILE *outputStream);
void nalUnitDecode(const nalUnit *nal, const dword *bitPosIn, NalStruct *nalParams, dword *bitPosOut);
unsigned int getVLCSymbol (unsigned char *output, unsigned short input);

#endif

