/***************************************************************************************
 * 	File: 	nalunit.c
 * 	Description: Implements NAL unit decoder and system buffer
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h"

//extern FILE *tracefile;
/*
void streamBuffer(nalUnit* nal, dword *bitPosOut)
{
	unsigned int x = 0;
	unsigned char temp;
	unsigned long numBytesInNALU = 0;

	static FILE *stream=NULL;
        char filename[] = "test.264";
        if( stream==NULL) {
		if ((stream = fopen(filename,"r"))==NULL) {
			printf("\nCannot read input file: %s\n", filename);
		} 
//	printf("\nFile: %s in StreamBuffer() opened.\n", filename);
	}

	numBytesInNALU = 0;
	*bitPosOut = 0;
		do
		{
			fread(&temp,1,1,stream); // read two more bytes from the file
			nal->x[numBytesInNALU++] = temp;
			x = (x << 8) | temp;
		}while(x != 1 && !feof(stream));

//	return (numBytesInNALU - 4);
}
*/
void streamBuffer(const SFILE *inputStream, nalUnit* nal, dword *bitPosOut, SFILE *outputStream)
{
	unsigned int x = 0;
	unsigned char temp;
	unsigned long numBytesInNALU = 0;

	numBytesInNALU = 0;
	*bitPosOut = 0;
	do 
	{
		fread(&temp,1,1,inputStream->pFile); // read two more bytes from the file
		nal->x[numBytesInNALU++] = temp;
		x = (x << 8) | temp;
	}while(x != 1 && !feof(inputStream->pFile));
	
	outputStream->pFile = inputStream->pFile;
}


void nalUnitDecode(const nalUnit *nal, const dword *bitPosIn, NalStruct *nalParams, dword *bitPosOut)
{
	unsigned short videoData;

	nalParams->forbiddenBit = 0;;
	nalParams->nalRefIdc = 0;
	nalParams->nalUnitType = 0;

	videoData = getVideoData((nalUnit *)nal, FLC, 8);

	nalParams->forbiddenBit = (videoData >> 15) & 0x01;
	nalParams->nalRefIdc = (videoData >> 13) & 0x03;
	nalParams->nalUnitType = (videoData >> 8) & 0x01f;

	//fprintf(tracefile,"nalRefIdc = %d\n", nalParams->nalRefIdc);
	//fprintf(tracefile,"forbidden_bit = %d\n", nalParams->forbiddenBit);
	//fprintf(tracefile,"nalUnitType = %d\n", nalParams->nalUnitType);
}

unsigned int getVLCSymbol (unsigned char * output,unsigned short input)
{

  register char inf;
  long byteoffset;      // byte from start of buffer
  int bitoffset;      // bit from start of byte
  int ctr_bit=0;      // control bit for current bit posision
  int bitcounter=1;
  int len;
  int info_bit;
  char buffer[2];

  buffer [1] = input & 0xff;
  buffer [0] = (input >> 8) & 0xff;

  byteoffset= 0;
  bitoffset= 7;
  ctr_bit = (buffer[byteoffset] & (0x01<<bitoffset));   // set up control bit

  len=1;
  
  while (ctr_bit==0)
  {                 // find leading 1 bit
    len++;
    bitoffset-=1;           
    bitcounter++;
    if (bitoffset<0)
    {                 // finish with current byte ?
      bitoffset=bitoffset+8;
      byteoffset++;
    }
    ctr_bit = buffer[byteoffset] & (0x01<<(bitoffset));
  }
    // make infoword
  inf=1;                          // shortest possible code is 1, then info is always 0
  for(info_bit=0;(info_bit<(len-1)); info_bit++)
  {
    bitcounter++;
    bitoffset-=1;
    if (bitoffset<0)
    {                 // finished with current byte ?
      bitoffset=bitoffset+8;
      byteoffset++;
    }
	
	inf=(inf<<1);
    if(buffer[byteoffset] & (0x01<<(bitoffset)))
      inf |=1;
  }


  *output = inf-1;

  return (bitcounter);
}
