/***************************************************************************************
 * 	File: decoderCommon.h	
 * 	Description: header file common to all modules in the decoder 
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef PARSER_COMMON_H_
#define PARSER_COMMON_H_

#define TRACE 1
#define MAX_MBS_IN_SLICE  500

#include <stdio.h>
#include "StreamBuffer.h"
#include "parseSequenceParams.h"
#include "parsePictureParams.h"
#include "parseSliceLayer.h"
#include "parseMbLayer.h"
#include "cavlc.h"
#include "idct.h"
#include "intra_prediction.h"
#include "deblocking_filter.h"

#define MAX_PIC_WIDTH 32

//void parseStreamMetaData(FILE * const inputStream, 
//                         SpsStruct *Sps, PpsStruct *Pps);

//void parseStreamMetaData(SpsStruct *Sps, PpsStruct *Pps);
void parseStreamMetaData(SFILE *inputStream, SpsStruct *Sps, PpsStruct *Pps);


void init_mb(SpsStruct *sps, PpsStruct *pps, ShStruct *sliceHeader, 
             MblStruct *Mbl);

void nextMb (SpsStruct *sps, MblStruct *MblIp, 
             MblStruct *MblOp);

void getParams(MblStruct *Mbl, ShStruct *Sh, 
	       cavlcParams *cavlcIps, IDCTParams *idctIps, intraParams *intraIps, dbParams *dbIps);

//void writeMB(MacroBlock *const macroblock, const int frame_num, const int mb_num, 
//             FILE *opFile);

//void printMB(const MacroBlock *macroblock, FILE *opFile);
void printMB(const MacroBlock *macroblock);

void parser(    const nalUnit *nal, 
		const SpsStruct *Sps, 
		const PpsStruct *Pps, 
		const ShStruct *Sh, 
		const dword *bitPosIn, 
		const unsigned char *firstMbFlagIn,
		
		cavlcParams *cavlcIps, 
		IDCTParams *idctIps, 
		intraParams *intraPredIps, 
		dbParams *dbIps,
		dword *bitPosOut,
		unsigned char *firstMbFlagout
            );

#undef getVideoData
unsigned short getVideoData1(nalUnit *nal, codeType t, int codeLen, const dword *bitPosIn, dword *bitPosOut);
#define getVideoData(x,y,z) getVideoData1(x,y,z,bitPosIn,bitPosOut)

void decodeVideoData (nalUnit *nal, offsetStruct *offset, videoDataStruct *videoData, dword *bitPosIn, dword *bitPosOut);

#endif

