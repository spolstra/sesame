/***************************************************************************************
 * 	File: pps.h	
 * 	Description: Header file for Picture parameter set FSM
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef PPS_H_
#define PPS_H_

typedef struct PpsStruct{
	unsigned char picParamSetId;
	unsigned char seqParamSetId;
	unsigned char entropyCodingModeFlag;
	unsigned char picOrderPresentFlag;
	unsigned char numSliceGroupsMinus1;
	unsigned char sliceGroupMapType;
	unsigned char numRefIdxL0ActiveMinus1; 
	unsigned char numRefIdxL1ActiveMinus1; 
	unsigned char weightedPredFlag;
	unsigned char weigthedBiPredIdc;
	unsigned char picInitQpMinus26;
	unsigned char picInitQsMinus26;
	unsigned char chromaQpIndexOffset;
	unsigned char deblockingFilterControlPresentFlag;
	unsigned char intraPredFlag;
	unsigned char redundantPicCntPresentFlag;
	unsigned char transform8x8ModeFlag;
}PpsStruct;


void pictureParamSet(nalUnit *nal, PpsStruct *pps, dword *bitPosIn, dword *bitPosOut);
#endif
