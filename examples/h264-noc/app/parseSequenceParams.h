/***************************************************************************************
 * 	File: sps.h	
 * 	Description: Header file for Sequence Parameter Set FSM
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef SPS_H_
#define SPS_H_

typedef struct SpsStruct{
	unsigned char profileIdc 						;
	unsigned char constraint_set0_flag 				;
	unsigned char constraint_set1_flag 				;
	unsigned char constraint_set2_flag 				;
	unsigned char constraint_set3_flag 				;
	unsigned char levelIdc 							;
	unsigned char seqParamSetId 					;
	unsigned char chromaFormatIdc 					;
	unsigned char residualColorTransformFlag 		;
	unsigned char bitDepthLumaMinus8 				;
	unsigned char bitDepthChromaMinus8 				;
	unsigned char qpPrimeYZeroTransformBypassFlag 	;
	unsigned char seqScalingMatrixPresentFlag 		;
	unsigned char log2MaxFrameNumMinus4 			;  			
	unsigned char picOrderCntType 					; 
	unsigned char log2MaxPicOrderCntLsbMinus4 		;
	unsigned char deltaPicOrderAlwaysZeroFlag 		;
	unsigned char numRefFrames 						;
	unsigned char gapsInFrameNumAllowedFlag 		;
	unsigned char picHeightInMbs 					;
	unsigned char picWidthInMbs 					;
	unsigned char frameMbsOnlyFlag 					;
	unsigned char frameCropBottomOffset 			;
	unsigned char vuiParametersPresentFlag 			;
	unsigned char frameCropTopOffset 				;
	unsigned char frameCropRightOffset 				;
	unsigned char frameCropLeftOffset 				;
	unsigned char frameCroppingFlag 				;
	unsigned char direct8x8InferenceFlag 			;
	unsigned char mbAdaptiveFieldFrameFlag 			;

	unsigned char qpBdOffsetY 						;
}SpsStruct;

void seqParamSet(nalUnit *nal, SpsStruct *Sps, dword *bitPosIn, dword *bitPosOut);
#endif
