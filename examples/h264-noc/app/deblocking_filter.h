/***************************************************************************************
 * 	File: dblock.h	
 * 	Description: header file for deblocking filter module 
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef _DBLOCK_H
#define _DBLOCK_H

#define Clip1(a)            ((a)>255? 255:((a)<0?0:(a)))
#define Clip1_Chr(a)        ((a)>255? 255:((a)<0?0:(a)))
#define Clip3(min,max,val)  (((val) < (min))? (min) : ( ( (val) > (max) ) ? (max) : (val)))
#define abs(x) 		( x > 0 ? x : -x )
#define MY_ABS(x,y) ((x > y) ? (x-y) : (y-x))

typedef struct pixelRow
{
	int p[4], q[4];
}pixelRow;

typedef struct filterParams
{
	unsigned char BS; 
	unsigned char leftAvailable; 
	unsigned char chromaEdgeFlag; 
	unsigned char filterOffsetA, filterOffsetB;
	char qP, qPLeft;
}filterParams;

typedef struct dbParams{
	int mb_x;
	int qPy, qPyLeft, qPyTop;
	int qPc, qPcLeft, qPcTop;
	unsigned char disableDblkFilterIdc , leftAvail, topAvail;
	int filterOffsetA, filterOffsetB;
}dbParams;

void filterCore(filterParams params, pixelRow *x, pixelRow *op);
void filterPipeline(filterParams params, block4x4 p, block4x4 q, 
		block4x4 pprime, block4x4 qprime);
void transpose( block4x4 x);
void copy_block(const block4x4 x, block4x4 y);
void lumaFilter(dbParams *params, MacroBlock *ip_mb, MacroBlock * op_mb, 
		block4x4 *lb, block4x4 *tb, int dir);
void chromaFilter(dbParams *params, MacroBlock *ip_mb, MacroBlock *op_mb, 
		block4x4 *lb, block4x4 *tb, int dir, int uv);
void storeContext(dbParams *params, MacroBlock *op_mb, 
		block4x4 *lby, block4x4 *tby, block4x4 *lbu, block4x4 *tbu, 
		block4x4 *lbv, block4x4 *tbv);
void deblocking_filter(const dbParams *params, const MacroBlock *ip_mb, MacroBlock *op_mb);
void clip_mb(MacroBlock *mb);


#endif
