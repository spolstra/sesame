NAME=h264Decoder

APP_DIR=app
ARCH_DIR=arch

APP=$(APP_DIR)/$(NAME)_app
VIRTUAL=$(ARCH_DIR)/$(NAME)_virtual
ARCH=$(ARCH_DIR)/$(NAME)_arch
MAP=$(NAME)_map

VIRTUALARCH=$(ARCH_DIR)/$(NAME)_virtarch
APPVIRTUAL_MAP=$(NAME)_appvirt_map

SHMFLAG=-S

all: build

build: yml build-app build-arch

yml: $(VIRTUAL).yml $(VIRTUALARCH).yml $(APPVIRTUAL_MAP).yml

$(VIRTUALARCH).yml: $(NAME).yml $(VIRTUAL).yml $(ARCH).yml
	ArchitectureCompiler  $(NAME).yml

.PHONY: $(VIRTUAL).yml
$(VIRTUAL).yml: $(NAME).yml $(APP).yml $(ARCH).yml $(MAP).yml
	VirtualLayerGenerator $(NAME).yml

$(APPVIRTUAL_MAP).yml: $(APP).yml
	AppVirtualMapGenerator $(APP).yml >$@;\
	if [ $$? -ne 0 -o ! -s $@ ]; then rm -f $@; exit 1; fi

build-app:
	$(MAKE) -C $(APP_DIR)

build-arch:
	$(MAKE) -C $(ARCH_DIR)

run: build
	LD_LIBRARY_PATH=`sesamesim-config --prefix`/lib:.:$(LD_LIBRARY_PATH) APPFLAGS="$(SHMFLAG) -C $(APP_DIR) -L . -l lib${NAME}.so" ARCHFLAGS="$(SHMFLAG)" SESAMERunner $(ARCH_DIR)/$(NAME) $(APP).yml $(APPVIRTUAL_MAP).yml $(VIRTUALARCH).yml

runapp: build-app
	$(MAKE) -C $(APP_DIR) run

runtrace: build
	$(MAKE) -C $(APP_DIR) runtrace
	$(MAKE) -C $(ARCH_DIR) runtrace

trace:
	$(MAKE) -C $(APP_DIR) runtrace

runarch: yml
	$(MAKE) -C $(ARCH_DIR) runtrace

yml-clean:
	rm -f $(VIRTUAL).yml $(VIRTUALARCH).yml $(APPVIRTUAL_MAP).yml

tidy:
	rm -f *~ \#* core* trace*

clean: tidy yml-clean
	rm -f map*.yml
	$(MAKE) -C $(APP_DIR) clean
	$(MAKE) -C $(ARCH_DIR) clean
