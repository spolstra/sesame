#define VERBOSE 1
#include "component_ids.h"
#include "operations.h"
#include "debug.h"

class processor

id  : integer
noc_out : proc_interface
// FIXME Should be channel. not proc interface
noperations  : integer
operations_t = [noperations] integer
operations   : operations_t
nchannels    : integer
channels_t   = [nchannels] channel
channels     : channels_t

space        : integer = 100 // Default size of internal buffers

execute_time  : integer = 0
read_size     : integer = 0
read_time     : integer = 0
write_size    : integer = 0
write_time    : integer = 0

read:(chid:integer, vid:integer, size:integer) -> void {
    t : integer = timer();

    // NOTE: Is blocking during the read method below not enough?
    blockt(FIFO_GET_DATA);

    // TODO: Should extend channel interface and use that
    // read/write dont go over noc, don't need route
    noc_out ! read(vid,size,-1);

    read_time += timer() - t;
    read_size += size;

    reply();
}

write:(chid:integer, vid:integer, size:integer) -> void {
    t : integer = timer();

    // NOTE: Is blocking during the write method below not enough?
    blockt(FIFO_PUT_DATA);

    // TODO: Should extend channel interface and use that
    // read/write dont go over noc, don't need route
    noc_out ! write(vid,size,-1);

    write_time += timer() - t;
    write_size += size;

    reply();
}

execute:(operindx : integer) -> void {
    t : integer;

    assert(operindx < noperations, ("%s: EXECUTE NR TO HIGH!\n", whoami()));
    t = operations[operindx];
    execute_time += t;
    blockt(t);

    reply();
}

execute_lat:(cycles : integer) -> void {
    execute_time += cycles;
    blockt(cycles);
    reply();
}

get_channel_id:(chid:integer) -> integer {
    i : integer;

    // Try to find the port that is connected to the requested channel
    for (i = 0; i < nchannels; i += 1) {
        VP( VINIT, ("channel checking %d = %d\n", i, channels[i] ! get_id() ));
        // We can have ranges of id's (eg. for a switching network)
        if (channels[i] ! get_id() == chid) {
            VP(VINIT, ("%s chid %d=%d\n", whoami(), chid, i));
            reply(i);
            return;
        };
    };

    // If this is an internal mapping (i.e. vself) reply -1
    if (chid == this) {
        reply(-1);
        return;

    } else if (i == nchannels) {
        panic("Channel id=%d not found\n", chid);
        reply(-2); // avoid compiler warning.
    };

    /* Should not get here */
    panic("Problem retrieving channel_id: %d, i:%d, nchannels:%d\n",
                      chid, i, nchannels);
    reply(-2);

}

get_id:() -> integer {
           reply(this);
}

// Called by vproc to allocate an internal buffer
create_channel:(vid:integer, size:integer) -> void {
    space -= size;
    if (space < 0) {panic("Processors internal buffers over allocated!");};
    reply();
}

send_request:(vchan : vchannel, dest: integer) -> void {
    VP(VSEND, ("%s send_request called with vid: %d, chindex: %d\n", whoami(),
            vchan, dest));
    noc_out !! send_request(vchan, dest);

    reply(); // processor can do something useful while send_request is sent.
}

statistics:() {
    if (execute_time + read_size + write_size > 0) {
        printf("%s.statistics()\n", whoami());
        printf("  Channels %d\n", nchannels);
        printf("  Total units read %d\n", read_size);
        printf("  Total units written %d\n", write_size);
        printf("  Time spent reading %d\n", read_time);
        printf("  Time spent writing %d\n", write_time);
        printf("  Time spent executing %d\n", execute_time);
        printf("  Idle time %d\n", timer() - (read_time + write_time + execute_time));
    }
}

{
    VP(VINIT, ("%s Before set_noc_id\n", whoami()));
    // issue NI with noc_id
    noc_out ! set_noc_id( id );
    VP(VINIT, ("%s After set_noc_id\n", whoami()));

    while (true) {
        block(any);
    }
}
