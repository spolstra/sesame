// Usage example:
// VP(3, ("%s, after get_channel_id\n", whoami()));

// SP: Cannot get pearl to acknowledge defines on the command line.
#define VERBOSE 1

// Levels
#define VINIT 1
#define VSEND 2
#define VNOC  4
#define VSYNC 8
#define VALL 16
#define VROUTE 32
#define VGEN 64
#define VSPEC 128

//#define VLEVEL  (VINIT|VSEND|VNOC|VSYNC|VALL|VROUTE|VGEN|VSPEC)
//#define VLEVEL  (VINIT | VSPEC )
#define VLEVEL  VINIT

#ifdef VERBOSE
#define VP(L, s)    (if ((L & VLEVEL) > 0) {printf s})
#else
#define VP(L, s)    (if (false) { printf s})
#endif

#define routeassert(E, M)   (if (!(E)) { printf("*** Route Assertion failed: ***\n"); printf M; exit(7)})
#define assert(E, M)   (if (!(E)) { printf("*** Assertion failed: ***\n"); printf M; exit(2)})

// Model codes
// max value of a signed int: (1u << 31u) - 1
#define ERROR_ROUTE 2147483647

// Model timing parameters

#define SWITCH_LAT 1

// To be determined
#define FIFO_CHECK_EMPTY 0
#define DMA_SEND_MSG 0
#define FIFO_CHECK_FULL 0
#define FIFO_PUT_DATA 0
#define FIFO_GET_DATA 0


