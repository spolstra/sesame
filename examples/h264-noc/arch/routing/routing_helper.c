#include <stdio.h>
#include <assert.h>
#include <math.h>

// Provided with platform description.
extern int route_packets_c (unsigned int src_ID,
        unsigned int dest_ID);


/* Helper function:
 * Creates a mask of size "size"
 */
int get_mask(int size) {
    int i;
    int mask = 0;

    for (i = 0; i < size; i++) {
        mask |= 1<<i;
    }
    return mask;
}

/* Helper function:
 * Calculate the shift width needed for nswitches
 */
int get_shift(int nswitches) {
    return (int) ceil(log2(nswitches));
}

int port_select(int route, int nswitches){
    assert (nswitches > 0);

    return route & get_mask(get_shift(nswitches));
}

int update_route(int route, int nswitches){
    assert (nswitches > 0);

    return route >> get_shift(nswitches);
}

// Define routing defined in separate file
// int define_routing (int src_ID, int dest_ID)
int define_routing (int src_ID, int dest_ID){
    return route_packets_c(src_ID, dest_ID);
}

/* End routing functions */
