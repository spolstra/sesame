#include "B.h"

#include <iostream>
#include <cstdlib>
using namespace std;

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int x;

    srand(0);
    // Do work so producer can run ahead.
    execute("op_StartUp");
    while (true) {
        ports->in0.read(x);
        cout << getFullName() << " received " << x << endl;
        // randomly generate execute (even with latency = 0, reorders
        // pearl schedule in the architecture.
        if (rand() % 2 == 0)
            execute("op_DoWork");
    }
}
