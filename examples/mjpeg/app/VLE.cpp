#include "VLE.h"

#include <stdlib.h>

VLE::VLE(Id n, VLE_Ports *ports) :
    VLE_Base(n, ports), current_write_byte(0), write_position(7),
    byte_index(0), LastDC_Y(0), LastDC_U(0), LastDC_V(0), BitRate(0),
    LastDC_sY(0), LastDC_sU(0), LastDC_sV(0)
{

    for (int i = 0; i < PACKETSIZE; i++) {
        current_packet.byte[i] = 0;
    }
}

VLE::~VLE() {}

/*
  FrequencyAC() is used to accumulate statistics on what AC codes occur
  most frequently.
*/
void VLE::FrequencyAC(int *matrix)
{
    int i, k, r, ssss, cofac;

    for (k = r = 0; ++k < BLOCKSIZE;) {     // Like EncodeAC below except don't write out
        cofac = abs(matrix[k]);     // Find absolute size

        if (cofac < 256) {
            ssss = csize[cofac];

        } else {
            cofac = cofac >> 8;
            ssss = csize[cofac] + 8;
        }

        if (matrix[k] == 0) {         // Check for zeroes
            if (k == BLOCKSIZE - 1) {   // If end of block, then process
                ACFrequency[0]++;         // Increment EOB frequency
                break;
            }

            r++;

        } else {
            while (r > 15) {           // Convert, r, ssss, into RLE
                ACFrequency[240]++;      // Increment ZRL extender freq
                r -= 16;
            }

            i = 16 * r + ssss;      // Make code
            r = 0;
            ACFrequency[i]++;       // Increment frequency of such code.
        }
    }
}

/*
  EncodeAC() takes the matrix and encodes it by passing the values
  of the codes found to the Huffman package.
*/
void VLE::EncodeAC(int *matrix)
{
    int i, k, r, ssss, cofac;

    for (k = r = 0; ++k < BLOCKSIZE;) {
        cofac = abs(matrix[k]);            // Find absolute size

        if (cofac < 256) {
            ssss = csize[cofac];

        } else {
            cofac = cofac >> 8;
            ssss = csize[cofac] + 8;
        }

        if (matrix[k] == 0) {                // Check for zeroes
            if (k == BLOCKSIZE - 1) {
                EncodeHuffman(0);
                break;
            }

            r++;                           // Increment run-length of zeroes

        } else {
            while (r > 15) {                  // If run-length > 15, time for
                // Run-length extension
                EncodeHuffman(240);
                r -= 16;
            }

            i = 16 * r + ssss;             // Now we can find code byte
            r = 0;
            EncodeHuffman(i);              // Encode RLE code

            if (matrix[k] < 0) {              // Follow by significant bits
                fputv(ssss, matrix[k] - 1);
                BitRate += ssss;

            } else {
                fputv(ssss, matrix[k]);
                BitRate += ssss;
            }
        }
    }
}

/*
  FrequencyDC() is used to accumulate statistics on what DC codes occur
  most frequently.
*/
void VLE::FrequencyDC(int coef, int *LastDC)
{
    int s, diff, cofac;

    diff = coef - *LastDC;         // Do DPCM
    *LastDC = coef;
    cofac = abs(diff);

    if (cofac < 256) {              // Find "code"
        s = csize[cofac];

    } else {
        cofac = cofac >> 8;
        s = csize[cofac] + 8;
    }

    DCFrequency[s]++;              // Increment frequency of such code
}

/*
  EncodeDC() encodes the input coefficient to the stream using the
  currently installed DC Huffman table.
*/
void VLE::EncodeDC(int coef, int *LastDC)
{
    int s, diff, cofac;

    diff = coef - *LastDC;
    *LastDC = coef;                // Do DPCM
    cofac = abs(diff);

    if (cofac < 256) {
        s = csize[cofac];          // Find true size

    } else {
        cofac = cofac >> 8;
        s = csize[cofac] + 8;
    }

    EncodeHuffman(s);              // Encode size

    if (diff < 0) {
        diff--;  // Encode difference
    }

    fputv(s, diff);
    BitRate += s;
}

// EncodeHuffman() places the Huffman code for the value onto the stream.
void VLE::EncodeHuffman(int value)
{
    fputv(ehufsi[value], ehufco[value]);
    BitRate += ehufsi[value];
}

// fputv() puts n bits from b onto the writer stream.
void VLE::fputv(int n, int b)
{
    int p;

    n--;
    b &= lmask[n];
    p = n - write_position;

    if (!p) {                            // Can do parallel save immediately
        current_write_byte |= b;

        if (byte_index < PACKETSIZE - 1) { // put the byte in the packet
            current_packet.byte[byte_index] = current_write_byte;
            byte_index++;
            current_write_byte = 0;
            write_position = 7;

        } else { // put the byte in the packet and send it
            current_packet.byte[byte_index] = current_write_byte;
            ports->BitStream.write(current_packet);
            ports->PacketFlag.write(NLP);

            for (int i = 0; i < PACKETSIZE; i++) {
                current_packet.byte[i] = 0;
            }

            byte_index = 0;
            current_write_byte = 0;
            write_position = 7;
        }

        return;

    } else if (p < 0) {                  // if can fit, we have to shift byte
        p = -p;
        current_write_byte |= (b << p);
        write_position = p - 1;
        return;
    }

    current_write_byte |= (b >> p);       // cannot fit. we must do putc's

    if (byte_index < PACKETSIZE - 1) {    // put the byte in the packet
        current_packet.byte[byte_index] = current_write_byte;
        byte_index++;

    } else {                        // put the byte in the packet and send it
        current_packet.byte[byte_index] = current_write_byte;
        ports->BitStream.write(current_packet);
        ports->PacketFlag.write(NLP);

        for (int i = 0; i < PACKETSIZE; i++) {
            current_packet.byte[i] = 0;
        }

        byte_index = 0;
    }

    // Save off  remainder
    while (p > 7) {                    // Save off bytes while remaining > 7
        p -= 8;
        current_write_byte = (b >> p) & lmask[7];

        if (byte_index < PACKETSIZE - 1) {   // put the byte in the packet
            current_packet.byte[byte_index] = current_write_byte;
            byte_index++;

        } else {                            // put the byte in the packet and send it
            current_packet.byte[byte_index] = current_write_byte;
            ports->BitStream.write(current_packet);
            ports->PacketFlag.write(NLP);

            for (int i = 0; i < PACKETSIZE; i++) {
                current_packet.byte[i] = 0;
            }

            byte_index = 0;
        }
    }

    if (!p) {                         // If zero then reset position
        write_position = 7;
        current_write_byte = 0;
    } else {                             // Otherwise reset write byte buffer
        write_position = 8 - p;
        current_write_byte = (b << write_position) & lmask[7];
        write_position--;
    }
}

void VLE::UseHuffman(int *code, int *size)
{
    for (int i = 0; i < 257; i++) {
        ehufsi[i] = size[i];
        ehufco[i] = code[i];
    }
}

void VLE::SetFrequency(int *DC, int *AC)
{
    for (int i = 0; i < 257; i++) {
        DC[i] = DCFrequency[i];
        AC[i] = ACFrequency[i];
    }
}

void VLE::main()
{
    TBlockData  BlockData;
    THuffTables LuminanceHuffTables, ChrominanceHuffTables;
    TCommand    Command;
    TStatistics Statistics;

    while (1) {
        ports->Command.read(Command);

        if (Command == EndOfFrame) {
            current_packet.byte[byte_index] = current_write_byte;
            ports->BitStream.write(current_packet);
            ports->PacketFlag.write(LP);
            current_write_byte = 0;
            write_position     = 7;

            for (int i = 0; i < PACKETSIZE; i++) {
                current_packet.byte[i] = 0;
            }

            byte_index         = 0;
            LastDC_Y           = 0;
            LastDC_U           = 0;
            LastDC_V           = 0;
            LastDC_sY          = 0;
            LastDC_sU          = 0;
            LastDC_sV          = 0;

        } else {
            if (Command == NewTables) {
                ports->HuffTables.read(LuminanceHuffTables);
                ports->HuffTables.read(ChrominanceHuffTables);
            }

            for (int i = 0; i < 2; i++) {
                ports->BlockData.read(BlockData);

                // clear the statistics of previous 8x8 block
                for (int j = 0; j < 257; DCFrequency[j++] = 0);

                for (int j = 0; j < 257; ACFrequency[j++] = 0);

                BitRate = 0;

                // make statistics of current 8x8 block
                FrequencyDC(*BlockData.pixel, &LastDC_sY);
                FrequencyAC(BlockData.pixel);
                SetFrequency(Statistics.DCFrequency, Statistics.ACFrequency);

                execute("op_MakeStatistics");

                // make Huffman encoding of current 8x8block and put the code into a bitstream
                UseHuffman(LuminanceHuffTables.DCcode, LuminanceHuffTables.DCsize);
                EncodeDC(*BlockData.pixel, &LastDC_Y);
                UseHuffman(LuminanceHuffTables.ACcode, LuminanceHuffTables.ACsize);
                EncodeAC(BlockData.pixel);

                execute("op_VLE");

                // send the statistics of current 8x8 block
                Statistics.BitRate = BitRate;
                ports->Statistics.write(Statistics);
            }

            for (int j = 0; j < 2; j++) {
                ports->BlockData.read(BlockData);

                // clear the statistics of previous 8x8 block
                for (int k = 0; k < 257; DCFrequency[k++] = 0);

                for (int k = 0; k < 257; ACFrequency[k++] = 0);

                BitRate = 0;

                // make statistics of current 8x8 block
                if (j == 0) {
                    FrequencyDC(*BlockData.pixel, &LastDC_sU);
                } else {
                    FrequencyDC(*BlockData.pixel, &LastDC_sV);
                }

                FrequencyAC(BlockData.pixel);
                SetFrequency(Statistics.DCFrequency, Statistics.ACFrequency);

                execute("op_MakeStatistics");

                // make Huffman encoding of current 8x8block and put the code into a bitstream
                UseHuffman(ChrominanceHuffTables.DCcode, ChrominanceHuffTables.DCsize);

                if (j == 0) {
                    EncodeDC(*BlockData.pixel, &LastDC_U);
                } else {
                    EncodeDC(*BlockData.pixel, &LastDC_V);
                }

                UseHuffman(ChrominanceHuffTables.ACcode, ChrominanceHuffTables.ACsize);
                EncodeAC(BlockData.pixel);

                execute("op_VLE");

                // send the statistics of current 8x8 block
                Statistics.BitRate = BitRate;
                ports->Statistics.write(Statistics);
            }
        }
    }
}
