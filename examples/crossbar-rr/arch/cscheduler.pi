#include "event.h"

class cscheduler

policy : policy_object
comp   : channel

eq        : event_q
events    : integer = 0
ev_ptr    : integer = 0
new_event : boolean
init_phase: boolean = true

schedule_event : (id, vid, size: integer) -> void
{
   if (events == MAX_EVENTS) {
      printf("%s: cannot store new events anymore, select bigger event array!\n",
              whoami());
      exit(0);
   };
   // Should always find a free spot because MAX_EVENTS > events
   // Every agent will have at most one outstanding event.
   while (eq[ev_ptr].event_type != NO_EVENT) {
      ev_ptr = (ev_ptr + 1) % MAX_EVENTS;
   };

   VP(("%s: need to schedule event from %s at %d (vp_id = %d), inserting at %d\n", whoami(), who(source), timer(), id, ev_ptr));

   eq[ev_ptr].event_type = READ; // hard code read event
   eq[ev_ptr].chid = 0; // Not used
   eq[ev_ptr].vid = vid;
   eq[ev_ptr].size = size;
   eq[ev_ptr].vp_id = id;
   eq[ev_ptr].src_id = source;
   events += 1;
   new_event = true;
   // Although this is an synchronous method, don't reply here.
   // This will give a warning, that we can ignore.
   // Reply will done when this event has been scheduled and processed.
}

statistics:() {
  printf("%s statistics:\n", whoami());
  printf(" events pending: %d\n", events);
}

forward : (e : event)
{
   switch(e.event_type) {
      READ {
         VP(("Event R: %s -> %s\n", who(e.src_id), who(comp)));
         comp ! read(e.vid, e.size);
      }
      WRITE {
         VP(("Event W: %s -> %s\n", who(e.src_id), who(comp)));
         comp ! write(e.vid, e.size);
      }
   }
}


{
   initcnt, i, src : integer

   for (i = 0; i < MAX_EVENTS; i += 1) {
      eq[i].event_type = NO_EVENT;
   };

   while (true) {
      blockt(0);
      // Get all events at the current time
      new_event = false;
      blockt(0, schedule_event);
      while (new_event) {
         new_event = false;
         blockt(0, schedule_event);
      };

      if (events == 0) {
         block(schedule_event);
      } else {
         src = policy ! pick_event(eq);
         if (src == -1) {
            VP(("%s: Could not find event for scheduling, waiting for new events\n", whoami()));
            block(schedule_event);
         } else {
            VP(("%s: picking event(%d(size: %d)) from %s\n",
                     whoami(), eq[src].event_type, eq[src].size,
                     who(eq[src].src_id)));
            // Synchronous call to processor with chosen event data.
            forward(eq[src]);
            // Clear the event from eq array.
            eq[src].event_type = NO_EVENT;
            events -= 1;
            // Wakeup the agent that scheduled this event.
            eq[src].src_id !! reply_void();
         }
      }
   }
}
