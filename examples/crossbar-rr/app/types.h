/*******************************************************************\

       SESAME project software license

        Copyright (C) 2002 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

      For information regarding the SESAME software project see
        http://sesamesim.sourceforge.net or email
        jcofflan@users.sourceforge.net

\*******************************************************************/

#ifndef TYPES_DONE
#define TYPES_DONE

#include "param.h"
#include "csize.h"
#include "tables.h"

typedef int  TNumOfBlocks;
typedef int  TPixel;

enum TBlockType   {RGB, YUV};
enum TCommand     {NewTables, OldTables, EndOfFrame};
enum TPacketFlag  {NLP, LP};


struct TBlockData {
    TPixel pixel[BLOCKSIZE];
};

struct TBitStreamPacket {
    int byte[PACKETSIZE];
};

struct TFrameSize {
    int Hor;
    int Ver;
};

struct THeaderInfo {
    TFrameSize   FrameSize;
    TNumOfBlocks NumOfBlocks;
    TBlockType   BlockType;
};

struct TQTables {
    int QCoef[BLOCKSIZE];
};


struct THuffTables {
    int DCcode[257];
    int DCsize[257];
    int ACcode[257];
    int ACsize[257];
};

struct TStatistics {
    int DCFrequency[257];
    int ACFrequency[257];
    int BitRate;
};

struct TTablesInfo {
    TQTables    QTable;
    int DCHuffBits[17];
    int DCHuffVal[257];
    int ACHuffBits[17];
    int ACHuffVal[257];

};

#endif
