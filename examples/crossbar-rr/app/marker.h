/*******************************************************************\

       SESAME project software license

        Copyright (C) 2002 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

      For information regarding the SESAME software project see
        http://sesamesim.sourceforge.net or email
        jcofflan@users.sourceforge.net

\*******************************************************************/

/*
************************************************************
marker.h

Some basic definitions of commonly occurring markers.

************************************************************
*/

#ifndef MARKER_DONE
#define MARKER_DONE

#define END_QUANTIZATION_TABLE 0xFF
#define END_CODE_TABLE 0xFF

#define MARKER_MARKER 0xff
#define MARKER_FIL 0xff

#define MARKER_SOI 0xd8
#define MARKER_EOI 0xd9
#define MARKER_SOS 0xda
#define MARKER_DQT 0xdb
#define MARKER_DNL 0xdc
#define MARKER_DRI 0xdd
#define MARKER_DHP 0xde
#define MARKER_EXP 0xdf

#define MARKER_DHT 0xc4

#define MARKER_SOF 0xc0
#define MARKER_RSC 0xd0
#define MARKER_APP 0xe0
#define MARKER_JPG 0xf0

#define MARKER_RSC_MASK 0xf8

#endif

