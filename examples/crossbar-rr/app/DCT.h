/*******************************************************************\

       SESAME project software license

        Copyright (C) 2005 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

      For information regarding the SESAME software project see
        http://sesamesim.sourceforge.net or email
        jcofflan@users.sourceforge.net

\*******************************************************************/
#ifndef DCT_H
#define DCT_H

#include "DCT_Base.h"

class DCT : public DCT_Base
{
        char DataPrecision;

    public:
        DCT(Id n, DCT_Ports *ports);
        virtual ~DCT();

        void ReferenceDct(int *matrix, int *newmatrix);
        void PreshiftDctMatrix(int *matrix, int shift);
        void BoundDctMatrix(int *matrix, int Bount);
        void ReferenceDct1D(int *ivec, int *ovec);
        void TransposeMatrix(int *matrix, int *newmatrix);

        void main();
};
#endif // DCT_H
