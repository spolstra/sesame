#include "B.h"

#include <stdlib.h>
#include <iostream>
using namespace std;

B::B(Id n, B_Ports *ports) : B_Base(n, ports) {}
B::~B() {}

void B::main()
{
    int count = atoi(getArgv()[1]);

    for (int i = 0; i < count; i++) {
        execute("execute1");
        cout << getFullName() << " execute1" << endl;
    }
}
