#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <sesamesim/libsim/misc.h>
#include <sesamesim/libsim/schedule.h>
#include <sesamesim/libsim/blocker.h>
#include <sesamesim/libsim/startup.h>
#include <sesamesim/libsim/mesq.h>
#include <sesamesim/libsim/class.h>

extern int method_statistics;
int numjobs();
int instanceof(int jobid, char *subtype_name);
char *who(int i);
char *whoami();
char *class_name();
int virtualChannelID();
void pearlwaiting(int reply_wait);
int pearl_methodinstall(char *name);

float powf(float, float);
float ftimer();
float _mli2float();

long long int ltimer() {
    // 2^33
    return 8589934592LL;
}

#ifndef type_int_64
#define type_int_64
typedef struct {int _[2];} int_64;
#endif //type_int_64

// Method labels of class tell_time
typedef struct tell_time_env {
    intptr_t state;
    intptr_t *sp;
    intptr_t *pp;
    intptr_t do_copy;
    intptr_t jobnr;
} tell_time_env;

extern int method_statistics;

int to_tell_time(const int job) {
    if (job < 0 || numjobs() <= job) {
          panic("to_tell_time() invalid job id!");
    }
    if (!instanceof(job, "tell_time")) {
          panic("%d is not an instance of tell_time", job);
    }
    return job;
}

int tell_time_call(tell_time_env *env) {
    int return_state = -1;
    intptr_t *ENV = (intptr_t *)(env + 1);
    
    intptr_t *sp = env->sp;
    intptr_t *pp = env->pp;
    
Start_Switch:
    switch (env->state) {

Fstatistics:
      case 1:;
        sp += 4;
        sp[-2] = (intptr_t)pp;
        sp[-1] = return_state;
        pp = sp;
        (printf(("%s: statistics\n"), (whoami())));
Rstatistics:
        env->state = sp[-1];
        pp = (intptr_t *)sp[-2];
        sp -= 4;
        goto Start_Switch;
      case 2:;
        (printf(("%s: started\n"), (whoami())));
        (printf(("%s: current time: %d\n"), (whoami()), (timer())));
        sp[0] = 0x0A;
        hold_to(sp[0]);
        env->state = 3;
        goto Freturn;
      case 3:;
        sp[0] = 0;
        (printf(("%s: current time: %lld\n"), (whoami()), (ltimer())));
        (printf(("%s: done\n"), (whoami())));
        {
            extern intptr_t block_001_tell_time; 
            if (!pearlreceive(block_001_tell_time, &sp[0])) {
                env->state = 4;
                goto Freturn;
            }
        }
      case 4:;
        sp += 1;
        return_state = 5;
        goto Fstatistics;
      case 5:;
        sp -= 1;
        env->state = 4;
        goto Freturn;
      default:
        panic("Illegal state '10' entered.", env->state);
    }
    
Freturn:
    env->sp = sp;env->pp = pp;
    return 0;
}

intptr_t tell_time_create(char *name) {
    tell_time_env *env;
    int l;
    extern int tell_time_methods[];
    env = (tell_time_env *)malloc(sizeof(tell_time_env));
    env->state = 2;
    env->jobnr = create_proc(name, tell_time_call, (intptr_t *)env, "tell_time", 4);
    install_mesg_q(env->jobnr, tell_time_methods);
    
    
    return env->jobnr;
}

struct initiate_message {
    intptr_t n;
    intptr_t size;
    intptr_t data[1];
};

tell_time_env *tell_time_initiate(struct tell_time_env *env, struct initiate_message *msg) {
    int maxsize;
    intptr_t *msgx, *sp, *ENV;
    msgx = &msg->data[msg->n];
    
    
    env = realloc(env, sizeof(intptr_t) * ((0) + 1) + sizeof(*env));
    ENV = (intptr_t *)(env + 1);
    memset(ENV, 0, sizeof(intptr_t) * ((0) + 1));
    
    
    maxsize = 128;
    if (maxsize * 100 > 16384) {
        if (maxsize * 2 > 16384) {
            maxsize *= 2;
            printf("tell_time: Warning allocating %d byte stack\n", 2 * maxsize);
        } else {
            maxsize = 16384;
        }
    } else {
        maxsize *= 100;
    }
    env->sp = env->pp = (intptr_t *)malloc(maxsize * sizeof(intptr_t));
    
    return env;
}

void tell_time_module() {
    
    int class_id = installclass(tell_time_create, "tell_time", (void *)tell_time_initiate);
}

intptr_t block_000_tell_time;
intptr_t block_001_tell_time;
int tell_time_methods[2] = {1};

void tell_time_install_mesgidq(const int meth_id) {
    static int ni = 1;
    tell_time_methods[ni++] = meth_id;
}

void tell_time_blocks() {
    blocker_class();
    blocker_def(method_statistics, 1);
    tell_time_install_mesgidq(method_statistics);
    blocker_new();
    block_000_tell_time  = blocker_close();
    blocker_new();
    blocker_append(method_statistics);
    block_001_tell_time  = blocker_close();
}

