#include "debug.h"

#define DATA_SIZE_ARRAY 100

class vmembus_stochastic

/* Module parameters */
arch  : memory
space : integer = 1
mu    : integer
sigma : integer

/* Local variables */
fill  : integer = 0
data_size_t = [DATA_SIZE_ARRAY] integer
data_sizes : data_size_t
head : integer
tail : integer = 0
temp_counter : integer = 100

// Return randomly generated token size stored for this read.
check_data:() -> integer {
  token_size : integer;
  while (fill == 0) {block(any);};
  token_size = data_sizes[head];
  VP(("Retrieving: %d from position %d\n", token_size, head));
  head = (head + 1) % space;
  reply(token_size);
}

signal_room:() {
  fill -= 1;
  if (fill < 0) {
    panic("%s.signal_room negative fill!", whoami());
  };
}

// Return a randomly generated data size for token to be written.
check_room:() -> integer {
  token_size : integer
  while (fill == space) {block(any);};
  token_size = random_norm(mu, sigma);
  if (token_size < 1) { token_size = 1; };
  data_sizes[tail] = token_size; // FIXME: change to random.
  VP(("Storing: %d at position %d\n", token_size, tail));
  temp_counter += 1;
  tail = (tail + 1) % space;
  reply(token_size);
}

signal_data:() {
  fill += 1;
  if (fill > space) {
    panic("%s.signal_data overflow!", whoami());
  };
}

get_vchannel_id:() -> integer {
  reply(virtualChannelID());
}

get_channel_id:() -> integer {
  reply(arch ! get_id());
}

statistics:() {}

{
  if (space > DATA_SIZE_ARRAY) {
      panic("PANIC: channel size of %d larger than static size array", space);
  };
  VP(("%s: stochastic parameters, mu: %d, sigma: %d\n", whoami(), mu, sigma));
  srandom(1);

  // Ask the memory to allocate a channel with the virtual channel id
  // that was mapped to this component.
  arch ! create_channel(virtualChannelID(), space);

  while (true) {
    block(any);
  };
}
