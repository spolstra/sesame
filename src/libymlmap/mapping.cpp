/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mapping.h"

#include "mappingfactory.h"
#include "mappingsaxhandler.h"
#include "mappingcontainer.h"

#include "../ymlresources/ymlresources.h"

#include <string>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/validators/common/Grammar.hpp>
XERCES_CPP_NAMESPACE_USE;

#include "../XercesUtils/XercesStr.h"
#include "../XercesUtils/BasicErrorHandler.h"

using namespace std;

/**
 * Construct a new Mapping and attempt to load it.
 *
 * @param factory The MappingFactory to use.
 * @param filename The path to the YML map file.
 * @param side The mapping side to construct.  Either SOURCE_SIDE or DEST_SIDE.
 */
Mapping::Mapping(MappingFactory *factory, const string filename, side_t side) :
    filename(filename), side(side), loaded(false), _isOpen(false), handler(factory, side)
{
    ASSERT_OR_THROW("NULL filename!", !filename.empty());

    XMLPlatformUtils::Initialize();

    load();
}

/**
 * Destruct the mapping.  Closes the mapping if it is open.
 *
 */
Mapping::~Mapping()
{
    XMLPlatformUtils::Terminate();
}

/**
 * Automaticly called by the constructor.  Attempts to load and construct
 * the YML mapping.  isLoaded() will return true if it succeded, false
 * otherwise.
 */
void Mapping::load()
{
    if (loaded) {
        return;
    }

    SAX2XMLReader *parser = XMLReaderFactory::createXMLReader();
    parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
    parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
    parser->setFeature(XMLUni::fgXercesSchema, true);
    parser->setFeature(XMLUni::fgXercesSchemaFullChecking, true);
    parser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);

    LocalFileInputSource schemaInput(XStrG(YML_MAP_SCHEMA));

    if (!parser->loadGrammar(schemaInput, Grammar::SchemaGrammarType, true)) {
        throw BasicException("Error loading schema grammar");
    }

    parser->setContentHandler(&handler);
    BasicErrorHandler errorHandler;
    parser->setErrorHandler(&errorHandler);

    LocalFileInputSource input(XStrG(filename));

    try {
        parser->parse(input);

    } catch (const XMLException &toCatch) {
        cerr << "Exception message is: "
             << XMLString::transcode(toCatch.getMessage()) << endl;

    } catch (const SAXParseException &toCatch) {
        cerr << "Exception message is: "
             << XMLString::transcode(toCatch.getMessage()) << endl;

        //} catch (...) {
        //cerr << "Unexpected Exception while loading mapping" << endl;
    }

    loaded = true;
    delete parser;
}

/**
 * Return the root trace context.  This is a pointer to the top level of the
 * mapping data structure.
 *
 * @return A pointer to the root TraceContext.
 */
TraceContext *Mapping::getTraceContext()
{
    return handler.getRoot();
}

/**
 * Open the mapping.  Will fail if the mapping is not both loaded.
 *
 * @return true on success, false otherwise.
 */
bool Mapping::open()
{
    if (!isLoaded()) {
        return false;
    }

    if (isOpen()) {
        return true;
    }

    channelList_t channelList = handler.getChannelList();
    channelList_t::iterator it = channelList.begin();

    if (!openChannels(channelList, it)) {
        cerr << "Error opening channels." << endl;
        return false;
    }

    _isOpen = true;
    return true;
}

/**
 * Close the mapping.  Calls closeChannel for each of the open channels.
 */
void Mapping::close()
{
    if (!isOpen()) {
        return;
    }

    channelList_t channelList = handler.getChannelList();
    channelList_t::iterator it;

    for (it = channelList.begin(); it != channelList.end(); it++) {
        closeChannel(*it);
    }

    _isOpen = false;
}

/**
 * A recursive function for opening the channels that assures that if one
 * channels fails to open all others will be closed.
 *
 * @param list The channel list.
 * @param it The current channel list pointer.
 *
 * @return true on success, false otherwise.
 */
bool Mapping::openChannels(channelList_t &list, channelList_t::iterator &it)
{
    if (it == list.end()) {
        return true;
    }

    ChannelMapping *channel = *it;

    if (!openChannel(channel)) {
        return false;
    }

    it++;

    if (!openChannels(list, it)) {
        closeChannel(channel);
        return false;
    }

    return true;
}


