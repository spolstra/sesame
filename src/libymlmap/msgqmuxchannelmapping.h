/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   msgqmuxchannelmapping.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:41:37 2003
 *
 * @brief  Implemenation of a ChannelMapping for message queue mappings.
 *
 * Uses BasicMUXChannels to implement a ChannelMapping.
 */
#ifndef MSGQMUXCHANNELMAPPING_H
#define MSGQMUXCHANNELMAPPING_H

#include "channelmapping.h"
#include "../libbasic/basicmuxchannel.h"

#include <string>
#include <tr1/memory>

class BasicMsgQueueMUX;

class MsgQMUXChannelMapping : public ChannelMapping
{
        BasicMUXChannel<trace_event_t> channel;

    public:
        MsgQMUXChannelMapping(const std::string name,
                              std::tr1::shared_ptr<InstructionContext> instrContext,
                              const int id);
        virtual ~MsgQMUXChannelMapping();

        bool openChannel(BasicMsgQueueMUX *msgQMUX);
        void closeChannel(BasicMsgQueueMUX *msgQMUX);

        // Begin TraceChannel interface
        virtual void send(trace_event_t &event);
        virtual trace_event_t next();
        // End TraceChannel interface
};
#endif
