/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   mappingstrs.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:43:02 2003
 *
 * @brief  Defines the XML strings used by YMLMap
 *
 *
 */
#ifndef MAPPINGSTRS_H
#define MAPPINGSTRS_H

// Elements
#define mappingElementStr "mapping"
#define instructionElementStr "instruction"
#define mapElementStr "map"
#define portElementStr "port"

// Attributes
#define sideAttrStr "side"
#define nameAttrStr "name"
#define sourceAttrStr "source"
#define destAttrStr "dest"

// Attribute values
#define sideAttrValueSource "source"
#define sideAttrValueDest "dest"

#endif
