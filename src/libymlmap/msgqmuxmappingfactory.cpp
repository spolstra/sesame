/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "msgqmuxmappingfactory.h"
#include "msgqmuxchannelmapping.h"

#include <string>
#include <tr1/memory>

/**
 * Create a new message queue mux implementation of a ChannelMapping.
 * Callers are responsible for deallocating the returned ChannelMapping.
 *
 * @param name The channel name.
 * @param instrContext The channel's instruction context.
 * @param id The channel id.
 *
 * @return A pointer to the new ChannelMapping.
 */
ChannelMapping *MsgQMUXMappingFactory::
createChannelMapping(const std::string name,
                     std::tr1::shared_ptr<InstructionContext> instrContext,
                     const int id)
{
    return new MsgQMUXChannelMapping(name, instrContext, id);
}
