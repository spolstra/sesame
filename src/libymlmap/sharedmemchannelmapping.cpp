/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sharedmemchannelmapping.h"
#include "instructioncontext.h"

#include <string>
#include <tr1/memory>
#include <unistd.h>

using namespace std;

/**
 * Construct a new SharedMemChannelMapping.
 *
 * @param name The channel name.
 * @param instrContext The channel's instruction context.
 * @param id The channel id.
 */
SharedMemChannelMapping::
SharedMemChannelMapping(const string name,
                        std::tr1::shared_ptr<InstructionContext> instrContext,
                        const int id) :
    ChannelMapping(name, instrContext, id)
{
}

SharedMemChannelMapping::~SharedMemChannelMapping()
{
}

/**
 * Shared memory implementation of openChannel.
 *
 * @param key The channel key.
 *
 * @return The channel key on success, 0 otherwise.
 */
int SharedMemChannelMapping::openChannel(key_t key)
{
    return channel.open(key);
}

/**
 * Shared memory implementation of createChannel.
 *
 * @param size The number of messages that may be in the channel at one time.
 *
 * @return The channel key on success, 0 otherwise.
 */
int SharedMemChannelMapping::createChannel(long size)
{
    return channel.create(0, size);
}

/**
 * Shared memory implementation of releaseChannel.  Releases all resources
 * associated with the channel.
 *
 */
void SharedMemChannelMapping::releaseChannel()
{
    // This assures that the sender cannot open the channel, write some data, and
    // close the channel before the reader has a chance to connect.
    while (!channel.isEmpty() && !channel.isConnected()) {
        usleep(1000);
    }

    channel.close();
}

/**
 * Shared memory implementation of send.
 *
 * @param event The trace event to send.
 */
void SharedMemChannelMapping::send(trace_event_t &event)
{
    channel.write((unsigned char *)&event, sizeof(trace_event_t));
}

/**
 * Shared memory implementation of next().  Get the next trace_event_t from
 * the channel.  This function will block until there is a new event on the
 * channel or the read is interupted.
 *
 * @return The trace event on success, a trace event with type = T_NULL
 *         otherwise.
 */
trace_event_t SharedMemChannelMapping::next()
{
    trace_event_t event;
    int count = channel.read((unsigned char *)&event, sizeof(trace_event_t));

    if (count != sizeof(trace_event_t)) {
        cerr << "Error reading trace!!!" << endl;
        trace_event_t empty = {T_NULL, -1, -1, -1, -1, -1, -1};
        return empty;
    }

    return event;
}
