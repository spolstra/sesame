/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   sharedmemmapping.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:31:11 2003
 *
 * @brief  The shared memory implementation of the mapping layer.
 *
 * SharedMemMapping encapsulates the functionality of the shared memory
 * implementation of the mapping layer via the Mapping interface.
 */
#ifndef SHAREDMEMMAPPING_H
#define SHAREDMEMMAPPING_H

#include "../libbasic/basicsharedmemqueue.h"

#include "sharedmemmappingfactory.h"
#include "mappingsaxhandler.h"
#include "mappingcontainer.h"
#include "mapping.h"

#include <string>

//The size of the shared memory that is used per channel
#define SHAREDMEM_SIZE 100000

class SAX2XMLReader;

class SharedMemMapping : public Mapping
{
        SharedMemMappingFactory factory;
        BasicSharedMemQueue keyChannel;
        key_t keyChannelKey;

    public:

        /**
         * Construct a new SharedMemMapping from a mapping file for the
         * specified side.
         *
         * @param filename The path to the YML mapping file.
         * @param side Which side we are on SOURCE_SIDE or DEST_SIDE.
         */
        SharedMemMapping(const std::string filename, side_t side);
        virtual ~SharedMemMapping();

        key_t openKeyChannel(key_t key);

        /**
         * Get the key channel key.
         * The key channel key is the system id associated with the
         * key exchanging shared memory.
         *
         * @return the key channel key.
         */
        key_t getKey() {
            return keyChannelKey;
        }

        bool open();
        void close();

    private:
        bool openChannel(ChannelMapping *channel);
        void closeChannel(ChannelMapping *channel);

        bool sendKey(key_t);
        key_t receiveKey();
};
#endif
