/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "filemapping.h"

#include "mappingobj.h"
#include "filechannelmapping.h"
#include "mappingsaxhandler.h"

#include <iostream>
#include <errno.h>

#include "../BasicUtils//BasicException.h"
#include <string>

using namespace std;

/**
 * Construct a new file based implentation of a Mapping.
 *
 * @param filename The path to the YML mapping file.
 * @param side Our mapping side either SOURCE_SIDE or DEST_SIDE.
 */
FileMapping::FileMapping(const string filename, side_t side) :
    Mapping(new FileMappingFactory, filename, side)
{
    close();
}

FileMapping::~FileMapping() {}

/**
 * Open file based mapping object.
 *
 * @param traceBaseName Prefix of the trace file's name
 *
 * @return true on success, false otherwise.
 */
bool FileMapping::open(const string traceBaseName)
{
    this->traceBaseName = traceBaseName;
    return Mapping::open();
}

/**
 * fileChannelMapping implementation of open.  This function should not be
 * called. Use open(char* traceBaseName) instead.
 *
 * @return false.
 */
bool FileMapping::open()
{
    THROW("Use FileMapping::open(char* traceBaseName) instead!");
}

/**
 * fileChannelMapping implementation of openChannel.
 *
 * @param channel The channel to open.
 *
 * @return true on success, false otherwise.
 */
bool FileMapping::openChannel(ChannelMapping *channel)
{
    //cout << "opening channel " << channel->getId() << endl;

    if (getSide() == SOURCE_SIDE) {
        return ((FileChannelMapping *)channel)->createChannel(traceBaseName);
    } else {
        return ((FileChannelMapping *)channel)->openChannel(traceBaseName);
    }
}

/**
 * fileChannelMapping implementation of closeChannel.
 *
 * @param channel The channel to close.
 */
void FileMapping::closeChannel(ChannelMapping *channel)
{
    //cout << "closing channel " << channel->getId() << endl;
    ((FileChannelMapping *)channel)->closeChannel();
}
