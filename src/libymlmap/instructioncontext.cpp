/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "instructioncontext.h"

#include <iostream>
#include <tr1/memory>

using namespace std;

int InstructionContext::nextExecId = 0;

/**
 * Construct a default InstructionContext.
 *
 * @param parent The context parent, or NULL if this is the root.
 */
InstructionContext::InstructionContext(tr1::shared_ptr<InstructionContext>
                                       parent) :
    parent(parent)
{
}

/**
 * Look up an instruction id.
 * This function will first look in the local context for the instruction.  If
 * it is not found it will ask its parent.  This matching is simalar to the
 * idea of scope in C/C++.
 *
 * @param instr The instruction name.
 *
 * @return The id on success, -1 otherwise.
 */
int InstructionContext::getInstructionId(const std::string instr)
{
    instrMap_t::iterator it = instrMap.find(instr);

    if (it == instrMap.end()) {
        if (parent == NULL || parent.get() == this) {
            int id = getNextExecId();
            addInstruction(instr, id);

            cerr << "Warning: no instruction mapping for '" << instr
                 << "' assigning id " << id << endl;
            return id;
        }

        return parent->getInstructionId(instr);
    } else {
        return it->second;
    }
}

int InstructionContext::getNextExecId()
{
    ++nextExecId;
    return (nextExecId - 1);
}
