/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "filechannelmapping.h"

#include "instructioncontext.h"
#include "../libbasic/basicmuxmessage.h"
#include "../libbasic/basicmsgqueuemux.h"

#include <netinet/in.h>
#include <string>
#include <tr1/memory>
#include "../BasicUtils/BasicException.h"

using namespace std;

/**
 * Construct a new fileChannelMapping that reads traces from a trace file.
 *
 * @param name The name of the channel.
 * @param instrContext The channel's instruction context.
 * @param id The channel id.
 */
FileChannelMapping::
FileChannelMapping(const string name,
                   std::tr1::shared_ptr<InstructionContext> instrContext,
                   const int id) :
    ChannelMapping(name, instrContext, id)
{
    //cerr << "Creating filechannelmapping " << name << " " << id << endl;
}

FileChannelMapping::~FileChannelMapping() {}


/**
 * Creates a trace file.
 *
 * @param traceBaseName  The prefix of the trace file name.
 *
 * @return true on sucess, false otherwise.
 */
bool FileChannelMapping::createChannel(const string traceBaseName)
{

    string filename = traceBaseName + "-" + getName();

    ASSERT_OR_THROW(string("Trace file '") + filename + "' all ready open!",
                    !traceFile.is_open());

    traceFile.open(filename.c_str(), ios::out | ios::trunc | ios::binary);

    ASSERT_OR_THROW(string("Opening trace file '") + filename + "'!",
                    traceFile.is_open());

    return true;
}

/**
 * Open this channel.
 *
 * @param traceBaseName The prefix of the trace file name.
 *
 * @return true on success, false otherwise.
 */
bool FileChannelMapping::openChannel(const string traceBaseName)
{

    string filename = traceBaseName + "-" + getName();

    ASSERT_OR_THROW(string("Trace file '") + filename + "' all ready open!",
                    !traceFile.is_open());

    traceFile.open(filename.c_str(), ios::in | ios::binary);

    ASSERT_OR_THROW(string("Opening trace file '") + filename + "'!",
                    traceFile.is_open());

    return true;
}

/**
 * Close this channel and the close access to the corresponding trace file.
 *
 */
void FileChannelMapping::closeChannel()
{
    traceFile.close();
}

/**
 * The fileChannelMapping implementation of send.
 *
 * @param event The event to send.
 */
void FileChannelMapping::send(trace_event_t &event)
{

    // buffer for four shorts and one 32bit integer
    short buf[buf_size];

    buf[0] = htons(event.type);
    buf[1] = htons(event.id);
    buf[2] = htons(event.size);
    buf[3] = htons(event.commid);
    buf[4] = htons(event.tid >> 16);
    buf[5] = htons(event.tid & 0xFFFF);
    buf[6] = htons(event.annot >> 16);
    buf[7] = htons(event.annot & 0xFFFF);
    buf[8] = htons(event.annot2 >> 16);
    buf[9] = htons(event.annot2 & 0xFFFF);

    traceFile.write((char *)buf, sizeof(short) * buf_size);
    ASSERT_OR_THROW("Writing trace file.", !traceFile.fail());
}

/**
 * The fileChannelMapping implementation of next.
 * If read fails throws a BasicException.
 *
 * @return The read trace event on success/
 */
trace_event_t FileChannelMapping::next()
{
    trace_event_t nextevent;
    short buf[buf_size];

    // read in trace data
    traceFile.read((char *)buf, sizeof(short) * buf_size);
    ASSERT_OR_THROW("Reading trace file.", !traceFile.fail());

    // fill the trace_event_t structure
    nextevent.type = (trace_t)ntohs(buf[0]);
    nextevent.id = ntohs(buf[1]);
    nextevent.size = ntohs(buf[2]);
    nextevent.commid = ntohs(buf[3]);
    nextevent.tid = ntohs(buf[5]) | (ntohs(buf[4]) << 16);
    nextevent.annot = ntohs(buf[7]) | (ntohs(buf[6]) << 16);
    nextevent.annot2 = ntohs(buf[9]) | (ntohs(buf[8]) << 16);
    //cerr << nextevent.type << " " << nextevent.id << " "
    //     << nextevent.size << endl;

    return nextevent;
}
