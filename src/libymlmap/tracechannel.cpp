/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "tracechannel.h"

/**
 * Send a Quit event
 * Will block if channel is full
 *
 */
void TraceChannel::sendQuit()
{
    trace_event_t e;
    e.type = T_QUIT;
    e.id = 0;
    e.size = 0;
    e.commid = 0;
    e.tid = 0;
    e.annot = 0;
    e.annot2 = 0;
    send(e);
}

/**
 * Send an annotated Execution event
 * Will block if channel is full
 *
 * @param operId The execution id
 * @param annot  The annotation
 */
void TraceChannel::sendExec(int operId, int tid, int annot, int annot2)
{
    trace_event_t e;
    e.type = T_EXECUTE;
    e.id = operId;
    e.size = 0;
    e.commid = 0;
    e.tid = tid; // FIXME add tid to execute trace
    e.annot = annot;
    e.annot2 = annot2;
    send(e);
}

/**
 * Send a Read event.
 * Will block if channel is full.
 *
 * @param portId Id of the port on which the read has/will occur(ed).
 * @param size The size of the read in bytes.
 */
void TraceChannel::sendRead(int portId, int size, int commChannelId,
        int tid, int annot, int annot2)
{
    trace_event_t e;
    e.type = T_READ;
    e.id = portId;
    e.size = size;
    e.commid = commChannelId;
    e.tid = tid;
    e.annot = annot;
    e.annot2 = annot2;
    send(e);
}

/**
 * Send a Write event.
 * Will block if channel is full.
 *
 * @param portId Id of the port on which the write has/will occur(ed).
 * @param size The size of the write in bytes.
 */
void TraceChannel::sendWrite(int portId, int size, int commChannelId,
        int tid, int annot, int annot2)
{
    trace_event_t e;
    e.type = T_WRITE;
    e.id = portId;
    e.size = size;
    e.commid = commChannelId;
    e.tid = tid;
    e.annot = annot;
    e.annot2 = annot2;
    send(e);
}
