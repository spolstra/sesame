/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "sharedmemmapping.h"

#include "mappingobj.h"
#include "sharedmemmappingfactory.h"
#include "sharedmemchannelmapping.h"
#include "mappingsaxhandler.h"

#include <string.h>
#include <iostream>
#include <string>
#include <errno.h>

using namespace std;

//#define SHAREDMEMMAPPING_DEBUG

/**
 * Construct a new SharedMemMapping.
 *
 * @param filename The path to the YML mapping.
 * @param side The side we are on either SOURCE_SIDE or DEST_SIDE
 */
SharedMemMapping::SharedMemMapping(const string filename, side_t side) :
    Mapping(new SharedMemMappingFactory, filename, side), keyChannelKey(0)
{
}

SharedMemMapping::~SharedMemMapping()
{
    close();
}

/**
 * Open the key channel.
 * If side is SOURCE_SIDE then ignore key and create a new key channel.
 * The key channel key will be choosen at random.  If side is DEST_SIDE
 * the key will be used to attempt to open an existing channel.
 *
 * @param key The system identifier for the key channel.
 *
 * @return The key channel key on success, 0 otherwise.
 */
key_t SharedMemMapping::openKeyChannel(key_t key)
{
    if (keyChannel.isOpen()) {
        return keyChannelKey;
    }

    keyChannelKey = 0;

    if (!key && getSide() == DEST_SIDE) {
        cerr << "Must specify a key on DEST side!." << endl;
        return 0;
    }

    if (getSide() == SOURCE_SIDE) {
        keyChannelKey = keyChannel.create(0, sizeof(key_t) * 256);

        if (!keyChannelKey) {
            cerr << "Failed to create key channel." << endl;
            return 0;
        }

    } else if (getSide() == DEST_SIDE) {
        keyChannelKey = keyChannel.open(key);

        if (!keyChannelKey) {
            cerr << "Failed to open key channel at " << key << "." << endl;
            return 0;
        }
    }

    return keyChannelKey;
}

/**
 * Open the shared memory mapping.  If the key channel has not yet
 * been opened this function will automaticly fail.
 *
 * @return true on success, false otherwise.
 */
bool SharedMemMapping::open()
{
    if (!keyChannelKey) {
        return false;
    }

    return Mapping::open();
}

/**
 * Close both the key channel and the mapping.
 *
 */
void SharedMemMapping::close()
{
    while (!keyChannel.isEmpty() && !keyChannel.isConnected()) {
        usleep(1000);
    }

    keyChannel.close();
    Mapping::close();
}

/**
 * Open a trace channel.
 *
 * @param channel The ChannelMapping for which the channel is to be opened.
 *
 * @return true on success, false otherwise.
 */
bool SharedMemMapping::openChannel(ChannelMapping *channel)
{
    SharedMemChannelMapping *channelMapping = (SharedMemChannelMapping *)channel;

    key_t key;

    if (getSide() == SOURCE_SIDE) {
        key = channelMapping->createChannel(SHAREDMEM_SIZE);

        if (!key) {
            cerr << "Cound not create channel '" << channelMapping->getFullName()
                 << "': " << strerror(errno) << endl;
            return false;
        }

        sendKey(key);

    } else if (getSide() == DEST_SIDE) {
        key = receiveKey();
        channelMapping->openChannel(key);

    } else {
        return false;
    }

#ifdef SHAREDMEMMAPPING_DEBUG
    cout << "Opened channel '" << channelMapping->getFullName()
         << "'' at " << key << "."
         << endl;
#endif

    return true;
}

/**
 * Close a trace channel.
 *
 * @param channel The ChannelMapping to be closed.
 */
void SharedMemMapping::closeChannel(ChannelMapping *channel)
{
    SharedMemChannelMapping *channelMapping = (SharedMemChannelMapping *)channel;
    channelMapping->releaseChannel();

#ifdef SHAREDMEMMAPPING_DEBUG
    cout << "Released channel '" << channelMapping->getFullName()
         << "'." << endl;
#endif
}

/**
 * Send the key of a newly created channel through the key channel to the
 * other side.  This function is only used by the SOURCE_SIDE.
 *
 * @param key The new key to transmit.
 *
 * @return true on success, false otherwise.
 */
bool SharedMemMapping::sendKey(key_t key)
{
#ifdef SHAREDMEMMAPPING_DEBUG
    cout << "Sending key " << hex << key << dec << endl;
#endif

    int count = keyChannel.write((const unsigned char *)&key, sizeof(key_t));

    if (count != sizeof(key_t)) {
#ifdef SHAREDMEMMAPPING_DEBUG
        cout << "Send failed!" << endl;
#endif
        return false;
    }

    return true;
}

/**
 * Recieve the next channel key.  This function is used only by the DEST_SIDE.
 *
 * @return The recieved key on success, 0 otherwise.
 */
key_t SharedMemMapping::receiveKey()
{
    key_t key = 0;
    int count = keyChannel.read((unsigned char *)&key, sizeof(key_t));

    if (count != sizeof(key_t)) {
#ifdef SHAREDMEMMAPPING_DEBUG
        cout << "Recieve falied" << endl;
#endif

        return 0;
    }

#ifdef SHAREDMEMMAPPING_DEBUG
    else {
        cout << "Recieved key " << hex << key << dec << endl;
    }

#endif

    return key;
}


//#define SHAREDMEMMAPPING_TEST
#ifdef SHAREDMEMMAPPING_TEST
#include <stdio.h>

/**
 * Program for testing the SharedMemMapping class.
 *
 * @param argc number of arguments + 1
 * @param argv file name + command line arguments.
 *
 * @return 0 on success, 1 otherwise.
 */
int main(int argc, char **argv)
{
    if (argc < 2 || 3 < argc) {
        cerr << "Syntax: " << argv[0] << " <mapping> [key]" << endl;
        exit(1);
    }

    side_t side = (argc > 2 ? DEST_SIDE : SOURCE_SIDE);
    SharedMemMapping mapping(argv[1], side);
    key_t key = 0;

    if (argc > 2) {
        key = atoi(argv[2]);
    }

    key = mapping.openKeyChannel(key);
    cout << "Key is " << mapping.getKey() << endl;

    if (!mapping.open()) {
        cerr << "Error opening mapping." << endl;
        exit(1);
    }

    getchar();

    mapping.close();
    cout << "Disconnected" << endl;

    return 0;
}
#endif
