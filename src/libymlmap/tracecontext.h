/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   tracecontext.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:14:30 2003
 *
 * @brief  TraceContext interface definition
 *
 * The TraceContext class describes the interface simulations use
 * to traverse the mapping context.
 */
#ifndef TRACECONTEXT_H
#define TRACECONTEXT_H

#include <vector>

class TraceChannel;

class TraceContext
{
    public:

        virtual ~TraceContext() {}

        /**
          * Request a child context of this context.
          *
          * @param name The name of the child context.
          *
          * @return The child context or NULL if no TraceContext is associated
          *         with @param name
          */
        virtual TraceContext *getChildContext(const std::string name) = 0;

        /**
         * Request a child channel of this context.
         *
         * @param name The name of the child channel.
         *
         * @return A pointer to the child TraceChannel or NULL if no channel
         * is associated with @param name
         */
        virtual TraceChannel *getChildChannel(const std::string name) = 0;

        /**
         * Get the parent of this TraceContext.
         *
         * @return the parent TraceContext pointer.
         */
        virtual TraceContext *getParentContext() = 0;

        virtual int getCommChannelID(const std::string name) = 0;

        /**
         * Add all descendant TraceChannel pointers to the array in depth first
         * order.
         *
         * @param array A BaiscArray collect the TraceChannel pointers into.
         */
        virtual void collectChannels(std::vector<TraceChannel *> *array) = 0;
};
#endif
