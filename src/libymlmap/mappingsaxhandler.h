/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   mappingsaxhandler.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:43:39 2003
 *
 * @brief  Xerces SAX2 handler for YMLMap
 *
 *
 */
#ifndef MAPPINGSAXHANDLER_H
#define MAPPINGSAXHANDLER_H

#include "../BasicUtils//BasicException.h"

#include <xercesc/sax2/DefaultHandler.hpp>

#include <stack>
#include <list>
#include <tr1/memory>

class MappingFactory;
class MappingObj;
class MappingContainer;
class InstructionContext;
class ChannelMapping;

typedef enum {SOURCE_SIDE, DEST_SIDE} side_t;
typedef std::list<ChannelMapping *> channelList_t;

class MappingSAXHandler : public XERCES_CPP_NAMESPACE::DefaultHandler
{
        // Attributes
    public:
        /// The factory for creating mapping objects
        MappingFactory *factory;
        /// Specifies the side for which we are parsing.
        side_t side;

        /// The next channel id to use.
        int nextChannelId;

        /// The root element of the mapping tree.
        MappingContainer *root;

        /// A list of ChannelMapping pointers.
        channelList_t channelList;

        /// The current parsing depth.
        int depth;

        /// The current instruction context.
        std::tr1::shared_ptr<InstructionContext> instrContext;

        /// The SAX parsing stack.  The top element is the current mapping object.
        std::stack<MappingObj *> context;

        bool commChannelMap;

        // Operations
    public:
        // Constructor
        MappingSAXHandler(MappingFactory *factory, side_t side);
        virtual ~MappingSAXHandler();

        MappingContainer *getRoot() {
            return root;
        }
        channelList_t getChannelList() {
            return channelList;
        }

        // Begin Mapping SAX interface
        void startMappingElement(const XERCES_CPP_NAMESPACE::Attributes &attributes);
        void startInstructionElement(const XERCES_CPP_NAMESPACE::Attributes &);
        void startMapElement(const XERCES_CPP_NAMESPACE::Attributes &attributes);
        void startPortElement(const XERCES_CPP_NAMESPACE::Attributes &attributes);
        void endMappingElement();
        void endInstructionElement();
        void endMapElement();
        void endPortElement();

        // Begin SAX2 interface
        void startDocument();
        void endDocument();
        void startElement(const XMLCh *const uri, const XMLCh *const localname,
                          const XMLCh *const qname,
                          const XERCES_CPP_NAMESPACE::Attributes &attributes);
        void endElement(const XMLCh *const uri, const XMLCh *const localname,
                        const XMLCh *const qname);
        void characters(const XMLCh *const chars, const XMLSize_t length);

    private:
        int getNextChannelId() {
            ++nextChannelId;
            return (nextChannelId - 1);
        }
        void upInstrContext();
        void downInstrContext();
};

#endif
