/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   channelmapping.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 20:07:17 2003
 *
 * @brief  The base ChannelMapping class
 *
 *
 */
#ifndef CHANNELMAPPING_H
#define CHANNELMAPPING_H

#include "mappingobj.h"
#include "tracechannel.h"
#include "instructioncontext.h"

#include <map>
#include <string>
#include <tr1/memory>

class ChannelMapping: public MappingObj, public TraceChannel
{
        // Attributes
    private:
        /// This ChannelMappings id.
        int id;

        typedef std::map<std::string, int> ports_t;
        /// A map of port names to port ids.
        ports_t ports;

        /// The next port id to use.
        int nextPortId;

        /// The instruction context for this ChannelMapping
        std::tr1::shared_ptr<InstructionContext> instrContext;

        // Operations
    public:
        ChannelMapping(const std::string name,
                       std::tr1::shared_ptr<InstructionContext> instrContext, int id);
        virtual ~ChannelMapping() {}

        // Begin TraceChannel interface
        int getId() {
            return id;
        }
        int getPortId(const std::string name);
        int getInstructionId(const std::string name);
        // End TraceChannel interface

        /**
         * Get MappingObj type.
         *
         * @return CHANNEL_MAPPING
         */
        mapping_obj_t getType() {
            return CHANNEL_MAPPING;
        }

    protected:
        int addPort(const std::string name);

        /**
         * Get the next port id and increment the port id variable.
         *
         * @return The next port id.
         */
        int getNextPortId() {
            nextPortId++;
            return nextPortId - 1;
        }

        friend class MappingSAXHandler;
};
#endif
