/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "msgqmuxchannelmapping.h"

#include "instructioncontext.h"
#include "../libbasic/basicmuxmessage.h"
#include "../libbasic/basicmsgqueuemux.h"

#include <string>
#include <tr1/memory>

using namespace std;

/**
 * Construct a new message queue mux ChannelMapping.
 *
 * @param name The name of the channel.
 * @param instrContext The channel's instruction context.
 * @param id The channel id.
 */
MsgQMUXChannelMapping::
MsgQMUXChannelMapping(const std::string name,
                      std::tr1::shared_ptr<InstructionContext> instrContext,
                      const int id) :
    ChannelMapping(name, instrContext, id), channel(id, 10000)
{
}

MsgQMUXChannelMapping::~MsgQMUXChannelMapping()
{
}

/**
 * Open this channel in the provided message queue mux.
 *
 * @param msgQMUX The message queue mux to open the channel in.
 *
 * @return true on success, false otherwise.
 */
bool MsgQMUXChannelMapping::openChannel(BasicMsgQueueMUX *msgQMUX)
{
    return msgQMUX->addChannel(&channel);
}

/**
 * Close this channel's connection in the message queue mux.
 *
 * @param msgQMUX The message queue mux.
 */
void MsgQMUXChannelMapping::closeChannel(BasicMsgQueueMUX *)
{
    //cout << "Flushing " << channel.getKey() << endl;
    channel.flush();
    //msgQMUX->removeChannel(&channel);
}

/**
 * The message queue mux implementation of send.
 * This function will block if the channel is full.
 *
 * @param event The event to send.
 */
void MsgQMUXChannelMapping::send(trace_event_t &event)
{
    channel.writeT(event);
}

/**
 * The message queue mux implementation of next.
 * This function will block if until there is a trace event to read.
 *
 * @return The read trace event on success, a trace event of type
 * T_NULL otherwise.
 */
trace_event_t MsgQMUXChannelMapping::next()
{
    return channel.readT();
}
