/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mappingcontainer.h"
#include "channelmapping.h"
#include "mappingobj.h"

#include "../BasicUtils//BasicException.h"

#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>

using namespace std;

/**
 * Construct a default MappingContainer.
 *
 * @param name The mapping name.
 */
MappingContainer::MappingContainer(const string name) : MappingObj(name)
{
}

/**
 * Add a child MappingContainer.
 *
 * @param container The child to add.
 */
void MappingContainer::addContainer(MappingContainer *container)
{
    ASSERT_OR_THROW("NULL container!", container);

    if (children.find(container->getName()) != children.end())
        THROW(string("Cannot add duplicate child '") + container->getName() +
              "' in '" + getFullName() + "'.");

    container->setParent(this);
    children[container->getName()] = container;
    child_list.push_back(container);
}

/**
 * Add a child ChannelMapping.
 *
 * @param channel The child to add.
 */
void MappingContainer::addChannel(ChannelMapping *channel)
{
    if (children.find(channel->getName()) != children.end())
        THROW(string("Cannot add duplicate child '") + channel->getName() +
              "' in '" + getFullName() + "'.");

    channel->setParent(this);
    children[channel->getName()] = channel;
    child_list.push_back(channel);
}

void MappingContainer::addCommChannelMap(const string name, const int id)
{
    comm_channel_map[name] = id;
}

/**
 * Get a child by name.
 *
 * @param name The name of the child.
 *
 * @return A pointer to the MappingObj associated with name, NULL if there is
 *         none.
 */
MappingObj *MappingContainer::getChild(const string name)
{
    children_t::iterator it = children.find(name);

    if (it == children.end()) {
        return NULL;
    }

    return (*it).second;
}

/**
 * Get a child channel by name.
 *
 * @param name The name of the child channel.
 *
 * @return A pointer to the ChannelMapping associated with name, NULL if there
 *         is none.
 */
ChannelMapping *MappingContainer::getChannel(const string name)
{
    MappingObj *entry = getChild(name);

    if (!entry || entry->getType() != CHANNEL_MAPPING) {
        return NULL;
    }

    return (ChannelMapping *)entry;
}

/**
 * Get a child container by name.
 *
 * @param name The name of the child container.
 *
 * @return A pointer to the MappingContainer associated with name, NULL if
 *         there is none.
 */
MappingContainer *MappingContainer::getContainer(const string name)
{
    MappingObj *entry = getChild(name);

    if (!entry || entry->getType() != MAPPING_CONTAINER) {
        return NULL;
    }

    return (MappingContainer *)entry;
}

int MappingContainer::getCommChannelID(const string name)
{
    comm_channel_map_t::iterator it = comm_channel_map.find(name);

    if (it == comm_channel_map.end()) {
        return -1;
    }

    return it->second;
}

/**
 * Collect all channels into a std::vector.
 *
 * @param array The array.
 */
void MappingContainer::collectChannels(std::vector<TraceChannel *> *array)
{
    child_list_t::iterator it;

    for (it = child_list.begin(); it != child_list.end(); it++) {
        MappingObj *mapping = *it;

        if (mapping->getType() == CHANNEL_MAPPING) {
            TraceChannel *traceChannel = (ChannelMapping *)mapping;
            array->push_back(traceChannel);

        } else if (mapping->getType() == MAPPING_CONTAINER) {
            ((MappingContainer *)mapping)->collectChannels(array);
        }
    }
}






