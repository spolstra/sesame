/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   msgqmuxmapping.h
 * @author Joseph Coffland & Mark Thompson
 * @date   Fri Feb 21 19:39:43 2003
 *
 * @brief  Implements a file mapping.
 *
 */
#ifndef FILEMAPPING_H
#define FILEMAPPING_H

#include "filemappingfactory.h"
#include "mappingsaxhandler.h"
#include "mappingcontainer.h"
#include "mapping.h"

#include <string>

class FileMapping : public Mapping
{
        FileMappingFactory factory;
        std::string traceBaseName; // prefix of the trace file's name

    public:
        FileMapping(const std::string filename, side_t side);
        virtual ~FileMapping();

        bool open(const std::string traceBaseName);
        bool open();

    protected:
        bool openChannel(ChannelMapping *channel);
        void closeChannel(ChannelMapping *channel);
};
#endif
