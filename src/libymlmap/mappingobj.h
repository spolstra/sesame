/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   mappingobj.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:50:02 2003
 *
 * @brief  The base mapping object
 *
 * This is the super class for MappingContainer and ChannelMapping objects.
 * It contains functionality common to the two classes.
 */
#ifndef MAPPINGOBJ_H
#define MAPPINGOBJ_H

#include <string>
#include <iostream>

typedef enum {MAPPING_CONTAINER, CHANNEL_MAPPING} mapping_obj_t;

class MappingContainer;
class InstructionContext;

class MappingObj
{
        // Attributes
    private:
        MappingObj *parent;
        std::string name;

        // Operations
    public:
        MappingObj(const std::string name);
        virtual ~MappingObj();

        /**
         * Get the name of this mapping object.
         *
         * @return It's name.
         */
        const std::string getName() {
            return name;
        }

        /**
         * Abstract function for the MappingObject type.
         *
         * @return either MAPPING_CONTAINER or CHANNEL_MAPPING
         */
        virtual mapping_obj_t getType() = 0;

        /**
         * Set this MappingObj's parent.
         *
         * @param parent The parent.
         */
        void setParent(MappingObj *parent) {
            this->parent = parent;
        }

        /**
         * Get this MappingObj's parent.
         *
         * @return The parent or NULL if this is the root object.
         */
        MappingObj *getParent() {
            return parent;
        }

        std::string getFullName();
};
#endif
