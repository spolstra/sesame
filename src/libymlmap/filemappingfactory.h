/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   filemappingfactory.h
 * @author Joseph Coffland & Mark Thompson
 * @date   Fri Feb 21 19:41:08 2003
 *
 * @brief  MappingFactory for the file implementation.
 *
 *
 */
#ifndef FILEMAPPINGFACTORY_H
#define FILEMAPPINGFACTORY_H

#include "mappingfactory.h"

#include <string>
#include <tr1/memory>

class FileMappingFactory : public MappingFactory
{
    public:
        virtual ChannelMapping *
        createChannelMapping(const std::string name,
                             std::tr1::shared_ptr<InstructionContext> instrContext,
                             const int id);
};

#endif
