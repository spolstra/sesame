/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   mappingcontainer.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 20:01:22 2003
 *
 * @brief  Contains the base class MappingContainer.
 *
 */
#ifndef MAPPINGCONTAINER_H
#define MAPPINGCONTAINER_H

#include <map>
#include <list>
#include <string>
#include <vector>

#include "mappingobj.h"
#include "tracecontext.h"
#include "channelmapping.h"

class MappingContainer: public TraceContext, public MappingObj
{
        // Attributes
    private:
        typedef std::map<std::string, MappingObj *> children_t;
        children_t children;
        typedef std::list<MappingObj *> child_list_t;
        child_list_t child_list;
        typedef std::map<std::string, int> comm_channel_map_t;
        comm_channel_map_t comm_channel_map;

        // Operations
    public:
        MappingContainer(const std::string name);
        virtual ~MappingContainer() {}

        MappingObj *getChild(const std::string name);
        ChannelMapping *getChannel(const std::string name);
        MappingContainer *getContainer(const std::string name);

        /**
         * Get the MapingObj type.
         *
         * @return MAPPING_CONTAINER
         */
        mapping_obj_t getType() {
            return MAPPING_CONTAINER;
        }

        // Begin TraceContext interface
        /**
         * @see TraceContext
         */
        virtual TraceContext *getChildContext(const std::string name) {
            return getContainer(name);
        }
        /**
         * @see TraceContext
         */
        virtual TraceChannel *getChildChannel(const std::string name) {
            return getChannel(name);
        }
        /**
         * @see TraceContext
         */
        virtual TraceContext *getParentContext() {
            return (MappingContainer *)getParent();
        }

        virtual int getCommChannelID(const std::string name);

        /**
         * @see TraceContext
         */
        virtual void collectChannels(std::vector<TraceChannel *> *array);
        // End TraceContext interface

    protected:
        void addContainer(MappingContainer *container);
        void addChannel(ChannelMapping *channel);
        void addCommChannelMap(const std::string name, const int id);

        friend class MappingSAXHandler;
};

#endif
