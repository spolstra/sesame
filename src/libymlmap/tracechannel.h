/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   tracechannel.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:23:20 2003
 *
 * @brief  Defines the TraceChannel interface.
 *
 * Simulations use this interface to communicate traces over the mapping.
 */

#ifndef TRACECHANNEL_H
#define TRACECHANNEL_H

#include "tracedefs.h"

#include <string>

class TraceChannel
{
    public:

        virtual ~TraceChannel() {}
        /**
         * Get this TraceChannel's id.
         *
         *
         * @return The id.
         */
        virtual int getId() = 0;

        /**
         * Get the id associated with the named port.
         *
         * @param name The name of the port.
         *
         * @return The port id or -1 if no port is associated with the name.
         */
        virtual int getPortId(const std::string name) = 0;

        /**
         * Get the id associated with the named instruction.
         *
         * @param name The name of the instruction.
         *
         * @return The instruciton id or -1 if no instruction is assiciated with
         *         this name.
         */
        virtual int getInstructionId(const std::string name) = 0;


        /**
         * Send a trace event.
         * This function may block if the trace channel is full.
         *
         * @param event The event to be sent.
         */
        virtual void send(trace_event_t &event) = 0;

        /**
         * Get the next trace event.
         * This function may block if there are no traces events currently avaliable.
         *
         * @return The next trace event.
         */
        virtual trace_event_t next() = 0;


        /**
         * Send the quit event.
         * This function may block if the channel is full.
         */
        virtual void sendQuit();

        /**
         * Send an execution event with the specified instruction id.
         * This function may block if the channel is full.
         *
         * @param operId The instruction id.
         * @param tid    The task identifier.
         * @param annot  The first annotation field.
         * @param annot2 The second annotation field.
         */
        virtual void sendExec(int operId, int tid, int annot, int annot2);

        /**
         * Send a read event of the specified size on the specified port.
         * This function may block if the channel is full.
         *
         * @param portId The id of the port on which the read event will occur.
         * @param size The size of the read.
         */
        virtual void sendRead(int portId, int size, int commChannelId,
                              int tid, int annot, int annot2);

        /**
         * Send a write event of the sepecified size on the specified port.
         * This function may block if the channel is full.
         *
         * @param portId The id of the port on which the write event will occur.
         * @param size The size of the write.
         */
        virtual void sendWrite(int portId, int size, int commChannelId,
                               int tid, int annot, int annot2);
};
#endif
