/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "channelmapping.h"
#include "instructioncontext.h"

#include "../BasicUtils/BasicException.h"

#include <stdlib.h>
#include <iostream>
#include <string>
#include <tr1/memory>

using namespace std;

/**
 * Construct a new default ChannelMapping.
 *
 * @param name The channel name.
 * @param instrContext The channel's instruction context.
 * @param id The channel id.
 */
ChannelMapping::ChannelMapping(const string name,
                               std::tr1::shared_ptr<InstructionContext>
                               instrContext, int id) :
    MappingObj(name), id(id), nextPortId(0), instrContext(instrContext)
{
}

/**
 * Get the id for a named port.
 *
 * @param name The name of the port.
 *
 * @return The port id on success, -1 otherwise.
 */
int ChannelMapping::getPortId(const string name)
{
    ports_t::iterator it = ports.find(name);

    if (it == ports.end()) {
        return -1;
    } else {
        return it->second;
    }
}

/**
 * Add a port to this ChannelMapping.
 *
 * @param name The name of the port to add.
 *
 * @return The id of the added port.
 */
int ChannelMapping::addPort(const string name)
{
    ASSERT_OR_THROW("NULL name!", !name.empty());

    if (ports.find(name) != ports.end())
        THROW(string("Cannot add duplicate port '") + name + "' to '" +
              getFullName() + "'.");

    int id = getNextPortId();
    ports[name] = id;

    return id;
}

/**
 * Look up the id of a named instruction in this channels instruction context.
 *
 * @param name The name of the instruction.
 *
 * @return The instruction id on success, -1 otherwise.
 */
int ChannelMapping::getInstructionId(const string name)
{
    return instrContext->getInstructionId(name);
}
