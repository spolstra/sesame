/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   msgqmuxmapping.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:39:43 2003
 *
 * @brief  Implements a msg queue mapping.
 *
 * Encapsulates a message queue Mapping implementation using a BasicMsgQueueMUX.
 */
#ifndef MSGQMUXMAPPING_H
#define MSGQMUXMAPPING_H

#include "msgqmuxmappingfactory.h"
#include "mappingsaxhandler.h"
#include "mappingcontainer.h"
#include "mapping.h"
#include "../libbasic/basicmsgqueuemux.h"

#include <string>

class MsgQMUXMapping : public Mapping
{
        MsgQMUXMappingFactory factory;
        BasicMsgQueueMUX msgQMUX;
        key_t key;

    public:
        MsgQMUXMapping(const std::string filename, side_t side);
        virtual ~MsgQMUXMapping();

        bool open(key_t key);
        key_t getKey() {
            return key;
        }
        bool open();
        void close();

    private:
        bool openChannel(ChannelMapping *channel);
        void closeChannel(ChannelMapping *channel);
};
#endif
