/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "msgqmuxmapping.h"

#include "mappingobj.h"
#include "msgqmuxchannelmapping.h"
#include "mappingsaxhandler.h"

#include <string.h>
#include <iostream>
#include <errno.h>
#include <string>
#include <stdlib.h>

using namespace std;

//#define MSGQMUXMAPPING_DEBUG

/**
 * Construct a new message queue mux implentation of a Mapping.
 *
 * @param filename The path to the YML mapping file.
 * @param side Our mapping side either SOURCE_SIDE or DEST_SIDE.
 */
MsgQMUXMapping::MsgQMUXMapping(const string filename, side_t side) :
    Mapping(new MsgQMUXMappingFactory, filename, side), key(0)
{
}

MsgQMUXMapping::~MsgQMUXMapping()
{
    close();
}

/**
 * Open the message queue mux with the provided key.
 * If side is SOURCE_SIDE and key = 0 a random key will
 * be choosen.
 *
 * @param _key The message queue id.
 *
 * @return true on success, false otherwise.
 */
bool MsgQMUXMapping::open(key_t _key)
{
    if (!Mapping::open()) {
        return false;
    }

    if (getSide() == SOURCE_SIDE) {
        key = msgQMUX.create(_key);

        if (!key) {
            key = 0;
            return false;
        }
    } else {
        if (!msgQMUX.open(_key)) {
            return false;
        }

        key = _key;
    }

    return true;
}

/**
 * Message queue mux implementation of open.  This function should not be
 * called.  Use open(key_t key) instead.
 *
 * @return false.
 */
bool MsgQMUXMapping::open()
{
    THROW("Use MsgQMUXMapping::open(key_t key) instead!");
}

/**
 * Message queue mux implementation of close.
 * Calls Mapping::close() to close all the channels.  Then closes the message
 * queue mux.
 */
void MsgQMUXMapping::close()
{
    //cout << "Closing" << endl;
    Mapping::close();
    msgQMUX.close();
}

/**
 * The message queue mux implementation of openChannel.
 *
 * @param channel The channel to open.
 *
 * @return true on success, false otherwise.
 */
bool MsgQMUXMapping::openChannel(ChannelMapping *channel)
{
    MsgQMUXChannelMapping *channelMapping = (MsgQMUXChannelMapping *)channel;
    return channelMapping->openChannel(&msgQMUX);
}

/**
 * The message queue mux implementatino of closeChannel.
 *
 * @param channel The channel to close.
 */
void MsgQMUXMapping::closeChannel(ChannelMapping *channel)
{
    MsgQMUXChannelMapping *channelMapping = (MsgQMUXChannelMapping *)channel;
    channelMapping->closeChannel(&msgQMUX);
}
