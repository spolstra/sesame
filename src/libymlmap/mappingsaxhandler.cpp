/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mappingsaxhandler.h"

#include "mappingfactory.h"
#include "mappingcontainer.h"
#include "channelmapping.h"
#include "mappingstrs.h"
#include "instructioncontext.h"

#include "../XercesUtils/XercesStr.h"

#include "../BasicUtils//BasicException.h"

#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax2/Attributes.hpp>

XERCES_CPP_NAMESPACE_USE;

#include <iostream>
#include <string>
#include <tr1/memory>

using namespace std;

#include <stdlib.h>
#include <string.h>


//#define MAPPINGSAXHANDLER_DEBUG

/**
 * A helper function to extract and convert XERCES attributes.
 *
 * @param attributes The attributes.
 * @param name The requested attribute name.
 *
 * @return The requested attribute, or NULL on failure.
 */
const string getAttributeValue(const Attributes &attributes, const string name)
{
    const XMLCh *value = attributes.getValue(XStrG(name));
    ASSERT_OR_THROW(string("Attribute '") + name + "' not found!", value);
    return XStrL(value);
}

/**
 * Construct a XercesC MappingSAXHandler.
 *
 * @param factory The MappingFactory used to created the mapping data
 *                structure.
 * @param side The mapping side to construct.  Either SOURCE_SIDE or DEST_SIDE.
 */
MappingSAXHandler::MappingSAXHandler(MappingFactory *factory, side_t side) :
    factory(factory), side(side), nextChannelId(0), root(),
    depth(0)
{

    ASSERT_OR_THROW("NULL factory!", factory);
}

MappingSAXHandler::~MappingSAXHandler()
{
    if (root) {
        delete root;
    }

    delete factory;

    while (!channelList.empty()) {
        delete channelList.back();
        channelList.pop_back();
    }
}


/**
 * Add a layer to the instruction context.
 *
 */
void MappingSAXHandler::upInstrContext()
{
    tr1::shared_ptr<InstructionContext> newInstrContext(
        factory->createInstructionContext(instrContext));
    instrContext = newInstrContext;
}

/**
 * Move down the instruction context.
 * WARNING: this code causes a memory leak, because it is not always possible
 * to get to every InstructionContext after it is created.
 */
void MappingSAXHandler::downInstrContext()
{
    instrContext = instrContext->getParent();
}

/**
 * Creates a new mapping element and pushes it on the stack and adds a
 * new instruction context.
 *
 * @param attributes the mapping attributes.
 */
void MappingSAXHandler::startMappingElement(const Attributes &attributes)
{
    string elemSideStr = getAttributeValue(attributes, sideAttrStr);

    side_t elemSide;

    if (elemSideStr == sideAttrValueSource) {
        elemSide = SOURCE_SIDE;
    } else if (elemSideStr == sideAttrValueDest) {
        elemSide = DEST_SIDE;
    } else THROW(string("ERROR: unknown mapping side '") + elemSideStr +
                     "'.");

    upInstrContext();

    if (elemSide == side) {
        string name = getAttributeValue(attributes, nameAttrStr);

        MappingContainer *container = NULL;

        if (!context.empty()) {
            ASSERT_OR_THROW("Expected mapping container!",
                            context.top()->getType() == MAPPING_CONTAINER);
            MappingContainer *parent = (MappingContainer *)context.top();

            container = parent->getContainer(name);

        } else if (root) {
            if (root->getName().compare(name) != 0) {
                THROW("Cannot have multiple roots");
            }

            container = root;
        }

        if (!container) {
            container = factory->createMappingContainer(name);
        }

        if (!root) {
            root = container;
        } else if (!context.empty()) {
            ((MappingContainer *)context.top())->addContainer(container);
        }

        context.push(container);
    } else {
        if (!context.empty()) {
            context.push(context.top());  // place holder
        }
    }
}

/**
 * Pop the last mapping element off the stack and pop
 * the instruction context.
 */
void MappingSAXHandler::endMappingElement()
{
    downInstrContext();

    if (!context.empty()) {
        ASSERT_OR_THROW("Expected mapping container!",
                        context.top()->getType() == MAPPING_CONTAINER);
        context.pop();
    }
}

/**
 * Add a new instruction mapping to the current instruction context.
 *
 * @param attributes the instruction's attributes.
 */
void MappingSAXHandler::startInstructionElement(const Attributes &attributes)
{
    ASSERT_OR_THROW("NULL instruction context!", instrContext.get());

    string name;

    if (side == SOURCE_SIDE) {
        name = getAttributeValue(attributes, sourceAttrStr);
    } else if (side == DEST_SIDE) {
        name = getAttributeValue(attributes, destAttrStr);
    }

    int id = InstructionContext::getNextExecId();
    //cout << "Added instruction " << name << " id = " << id << endl;
    instrContext->addInstruction(name, id);
}

/**
 * This function does nothing it is hear for the purpose of symmetry.
 */
void MappingSAXHandler::endInstructionElement() {}

/**
 * Create a new map element, push it on the stack and add a new instruction
 * context.
 *
 * @param attributes The map's attributes.
 */
void MappingSAXHandler::startMapElement(const Attributes &attributes)
{
    ASSERT_OR_THROW("Context is empty!", !context.empty());
    ASSERT_OR_THROW("Expected mapping container!",
                    context.top()->getType() == MAPPING_CONTAINER);

    string name;

    if (side == SOURCE_SIDE) {
        name = getAttributeValue(attributes, sourceAttrStr);
    } else if (side == DEST_SIDE) {
        name = getAttributeValue(attributes, destAttrStr);
    }

    int id = getNextChannelId();

    if (strstr(name.c_str(), "->") != 0) {
        ((MappingContainer *)context.top())->addCommChannelMap(name, id);
        commChannelMap = true;
        //cout << "Added channel mapping id = " << id << " " << name.get() << endl;

    } else {
        upInstrContext();
        ChannelMapping *channelMapping =
            factory->createChannelMapping(name, instrContext, id);
        channelList.push_back(channelMapping);
        ((MappingContainer *)context.top())->addChannel(channelMapping);
        context.push(channelMapping);

        commChannelMap = false;
    }
}

/**
 * Pop the last map element and its instruction context.
 */
void MappingSAXHandler::endMapElement()
{
    if (!commChannelMap) {
        ASSERT_OR_THROW("Context is empty!", !context.empty());
        ASSERT_OR_THROW("Expected channel mapping!",
                        context.top()->getType() == CHANNEL_MAPPING);

        downInstrContext();
        context.pop();

    } else {
        commChannelMap = false;
    }
}

/**
 * Create a new port element and add it to the map on the top of the stack.
 *
 * @param attributes The port's attributes.
 */
void MappingSAXHandler::startPortElement(const Attributes &attributes)
{
    ASSERT_OR_THROW("Context is empty!", !context.empty());
    ASSERT_OR_THROW("Expected channel mapping!",
                    context.top()->getType() == CHANNEL_MAPPING);

    string name;

    if (side == SOURCE_SIDE) {
        name = getAttributeValue(attributes, sourceAttrStr);
    } else if (side == DEST_SIDE) {
        name = getAttributeValue(attributes, destAttrStr);
    }

    ChannelMapping *parent = (ChannelMapping *)context.top();
    parent->addPort(name);
}

/**
 * This function does nothing it is hear for the purpose of symmetry.
 */
void MappingSAXHandler::endPortElement() {}


// Begin SAX2 interface
/**
 * Initialized the mapping data structure.
 *
 */
void MappingSAXHandler::startDocument()
{
    if (root) {
        delete root;
    }

    root = 0;
    instrContext.reset();
    nextChannelId = 0;
}

/**
 * Do nothing.
 *
 */
void MappingSAXHandler::endDocument() {}

/**
 * Start a new mapping element.
 */
void MappingSAXHandler::startElement(const XMLCh *const /*_uri*/,
                                     const XMLCh *const _localname,
                                     const XMLCh *const /*_qname*/,
                                     const Attributes &attributes)
{

    XercesStr xLocalname(_localname);
    const char *localname = xLocalname.localForm();

#ifdef MAPPINGSAXHANDLER_DEBUG

    for (int i = 0; i < depth; i++) {
        cout << '\t';
    }

    cout << "<" << localname << ">" << endl;
#endif
    depth++;

    if (strcmp(localname, mappingElementStr) == 0) {
        startMappingElement(attributes);
    } else if (strcmp(localname, instructionElementStr) == 0) {
        startInstructionElement(attributes);
    } else if (strcmp(localname, mapElementStr) == 0) {
        startMapElement(attributes);
    } else if (strcmp(localname, portElementStr) == 0) {
        startPortElement(attributes);
    }
}

/**
 * End the last mapping element.
 */
void MappingSAXHandler::endElement(const XMLCh *const /*_uri*/,
                                   const XMLCh *const _localname,
                                   const XMLCh *const /*_qname*/)
{

    XercesStr xLocalname(_localname);
    const char *localname = xLocalname.localForm();

    depth--;
#ifdef MAPPINGSAXHANDLER_DEBUG

    for (int i = 0; i < depth; i++) {
        cout << '\t';
    }

    cout << "</" << localname << ">" << endl;
#endif

    if (strcmp(localname, mappingElementStr) == 0) {
        endMappingElement();
    } else if (strcmp(localname, instructionElementStr) == 0) {
        endInstructionElement();
    } else if (strcmp(localname, mapElementStr) == 0) {
        endMapElement();
    } else if (strcmp(localname, portElementStr) == 0) {
        endPortElement();
    }
}

/**
 * Do nothing.
 */
void MappingSAXHandler::characters(const XMLCh *const /*chars*/,
                                   const XMLSize_t /*length*/) {}
