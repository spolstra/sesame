/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "tracedefs.h"

#include <iostream>
#include <fstream>
#include <netinet/in.h>

using namespace std;


int main(int argc, char *argv[])
{
    if (argc < 2) {
        cerr << "Syntax: " << argv[0] << " <trace file>" << endl;
        return 1;
    }

    ifstream trace(argv[1]);

    if (!trace.is_open()) {
        cerr << "Error opening trace file '" << argv[1] << "'!" << endl;
        return 1;
    }

    trace_event_t e;

    while (!trace.eof()) {
        trace.read((char *)&e, sizeof(e));

        if (trace.eof()) {
            break;
        }

        e.type = ntohs(e.type);
        e.id = ntohs(e.id);
        e.size = ntohs(e.size);
        e.tid = ntohl(e.tid);
        e.annot = ntohl(e.annot);
        e.annot2 = ntohl(e.annot2);

        switch (e.type) {
            case T_QUIT:
                cout << "QUIT";
                break;

            case T_NULL:
                cout << "NULL";
                break;

            case T_EXECUTE:
                cout << "EXECUTE instr=" << e.id;
                break;

            case T_READ:
                cout << "READ port=" << e.id << " size=" << e.size;
                break;

            case T_WRITE:
                cout << "WRITE port=" << e.id << " size=" << e.size;
                break;
        }
        cout << " tid=" << e.tid << " annot=" << e.annot \
            << " annot2=" << e.annot2 << endl;
    }

    trace.close();

    return 0;
}
