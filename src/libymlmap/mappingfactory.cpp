/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "mappingfactory.h"
#include "mappingcontainer.h"
#include "instructioncontext.h"

#include <tr1/memory>
#include <string>

MappingFactory::MappingFactory() {}
MappingFactory::~MappingFactory() {}

using namespace std;

/**
 * Create the default MappingContainer.
 * Callers are responsible for deallocating the returned MappingContainer.
 *
 * @param name The mapping container name.
 *
 * @return A pointer to the newly created MappingContainer.
 */
MappingContainer *MappingFactory::createMappingContainer(const string name)
{
    return new MappingContainer(name);
}

/**
 * Create the default InstructionContext.
 * Callers are responsible for deallocating the returned InstructionContext.
 *
 * @param parent The parent context.
 *
 * @return A pointer to the newly created InstructionContext.
 */
InstructionContext *MappingFactory::
createInstructionContext(tr1::shared_ptr<InstructionContext> parent)
{
    return new InstructionContext(parent);
}



