/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef TRACEDEFS_H
#define TRACEDEFS_H
/**
 * @file   tracedefs.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:13:41 2003
 *
 * @brief  Defines the trace event structure and trace type codes
 *
 *
 */

typedef enum {T_QUIT = -1, T_NULL, T_EXECUTE, T_READ, T_WRITE} trace_t;

typedef struct {
    short type;
    short id;
    short size;
    short commid;
    int tid;
    int annot;
    int annot2;
} trace_event_t;


#endif
