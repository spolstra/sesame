/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   mapping.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:56:05 2003
 *
 * @brief  Defines the base mapping type.
 *
 * Specializations of this type implement the actual mapping.
 */
#ifndef MAPPING_H
#define MAPPING_H

#include "mappingsaxhandler.h"

#include <string>

// mapping type used in PNRunner and libymlpearl main function.
typedef enum {M_MSGQ, M_SHM, M_FILE} mapping_t;

class MappingFactory;
class TraceContext;

class Mapping
{
        const std::string filename;
        const side_t side;
        bool loaded;
        bool _isOpen;
        MappingSAXHandler handler;

    public:
        Mapping(MappingFactory *factory, const std::string filename, side_t side);
        virtual ~Mapping();

        side_t getSide() {
            return side;
        }

        /**
         * Check if the mapping is properly loaded.
         *
         * @return true if it is loaded, false otherwise.
         */
        virtual bool isLoaded() {
            return loaded;
        }
        virtual TraceContext *getTraceContext();

        /**
         * Check if the mapping is open for communication.
         *
         * @return true if it is open false otherwise.
         */
        virtual bool isOpen() {
            return _isOpen;
        }
        virtual bool open();
        virtual void close();

    protected:

        /**
         * Abstract function for opening a ChannelMapping in this Mapping
         *
         * @param channel The channel to open.
         *
         * @return true on success false otherwise
         */
        virtual bool openChannel(ChannelMapping *channel) = 0;

        /**
         * Abstract function for closing a ChannelMapping in this Mapping.
         *
         * @param channel The channel to close.
         */
        virtual void closeChannel(ChannelMapping *channel) = 0;

        virtual void load();
        bool openChannels(channelList_t &list, channelList_t::iterator &it);
};
#endif
