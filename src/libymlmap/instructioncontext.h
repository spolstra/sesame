/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   instructioncontext.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 20:05:29 2003
 *
 * @brief  InstructionContext class
 *
 * Hold the instruction mapings in different mapping contexts.
 */
#ifndef INSTRUCTIONCONTEXT_H
#define INSTRUCTIONCONTEXT_H

#include <map>
#include <string>
#include <tr1/memory>

class InstructionContext
{
        typedef std::map<std::string, int> instrMap_t;

        /// Instruction map
        instrMap_t instrMap;

        /// The parent instruction context
        std::tr1::shared_ptr<InstructionContext> parent;

        /// The next execution id to use.
        static int nextExecId;

    public:
        InstructionContext(std::tr1::shared_ptr<InstructionContext> parent);

        /**
         * Add a new instruction mapping.
         *
         * @param instr The instruction name.
         * @param id Its id.
         */
        void addInstruction(const std::string instr, int id) {
            instrMap[instr] = id;
        }

        int getInstructionId(const std::string instr);

        /**
         * Get the InstructionContext parent pointer.
         *
         * @return The parent.
         */
        std::tr1::shared_ptr<InstructionContext> getParent() {
            return parent;
        }

        static int getNextExecId();
};

#endif
