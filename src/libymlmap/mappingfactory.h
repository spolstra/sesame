/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   mappingfactory.h
 * @author Joseph Coffland
 * @date   Fri Feb 21 19:59:56 2003
 *
 * @brief  Abstract mapping factory
 *
 * This file defines the MappingFactory interface.
 */
#ifndef MAPPINGFACTORY_H
#define MAPPINGFACTORY_H

#include "instructioncontext.h"

#include <string>
#include <tr1/memory>

class MappingContainer;
class ChannelMapping;
class PortMapping;

class MappingFactory
{
    public:
        MappingFactory();
        virtual ~MappingFactory();

        virtual MappingContainer *createMappingContainer(const std::string name);
        virtual ChannelMapping *
        createChannelMapping(const std::string name,
                             std::tr1::shared_ptr<InstructionContext> instrContext,
                             const int id) = 0;
        virtual InstructionContext *
        createInstructionContext(std::tr1::shared_ptr<InstructionContext> parent);
};
#endif
