from sesamemap import appyml, archyml
from pyyml import yml, ymlmap
import sys
import collections
import logging

Dest = collections.namedtuple('Dest', ['node', 'port'])

policy_handled = None
gschedule = None
mapdict = None
arch = None
task_ids = None

# TODO assert for in/valid mapping here? (write testcases)

# The architecture compiler combines the virtual layer (XXX_virtual.yml)
# with the architecture layer (XXX_arch.yml) into the final
# XXX_virtarch.yml which is used for the simulation.
#
# Algorithm:
#
# 0. The start of the new virtarch is the virtual layer.
# 1. Add the whole architecture to the virtarch as a network called
#    'architecture'. We can remove all templates and mappings from the arch
#    components as the one we need has already been copied into the
#    virtual component by the VirtualLayerGenerator.
# 2. For every virtual component (vprocessor or vchannel) in the virtual layer:
#       Map the virtual component to the architecture component specified
#       in vcomp's mapping property.
#       Mapping is done in three parts: inside vcomp, inside
#       architecture, and between virtual and architecture.
#       The steps are:
#       a. Extract all the mapping info from the mapping property.
#       b. Map the vcomp node to its enclosing vcomp network.
#          Depending on the nesting, multiple links and ports will be
#          created.
#       c. Map (with a link) vcomp network to architecture network edge. Every
#          mapping will create a new port (mapportXX) on the network.
#       d. Finally create a link inside the architecture network from the
#          network edge to the architecture component.
# (The comments below use the algorithm step numbers above)


# For innermost link from node to its enclosing network.
def add_innermost_link(cur_vproc, cur_map):

    vproc_node = cur_vproc.get_node(cur_map.get('source'))[0]
    source_port_name = cur_map.port.get('source')

    # Check for port on vproc_node, and add if not already there.
    if not vproc_node.check_port(source_port_name):
        vproc_node.add_port(source_port_name, 'out')

    # add port to cur_vproc parent network
    cur_vproc.add_port('mapport', 'out')
    # add link from node to net edge
    cur_vproc.add_link(cur_map.get('source'), source_port_name,
                       'this', 'mapport')
    # return dest node and port, these are needed in main
    # TODO: actually need to link dest path as well.
    return Dest(cur_map.get('dest'), cur_map.port.get('dest'))


# Recurse into the vproc networks using the source side of the mapping
# property. Add links between the network coming out of the recursion.
def add_link_chain(cur_vproc, cur_map):

    if cur_map.get('side') == 'source':
        inner_vproc = cur_vproc.get_network(cur_map.get('name'))[0]
    elif cur_map.get('side') == 'dest':
        inner_vproc = cur_vproc
    else:
        print "Mapping error, unkown side: %s" % cur_map.get('side')
        exit(2)

    # Recurse into mapping levels first..
    if hasattr(cur_map, 'map'):
        dest = add_innermost_link(inner_vproc, cur_map.map)
    else:
        dest = add_link_chain(inner_vproc, cur_map.mapping)

    if cur_map.get('side') == 'source':
        # .. and then add port to parent network and link this->parentnet
        # on the way out (only for the source side of course).
        if cur_vproc.getparent() is not None:
            cur_vproc.add_port('mapport', 'out')
            cur_vproc.add_link(cur_map.get('name'), 'mapport',
                               'this', 'mapport')
    return dest


# For innermost link from node to its enclosing network.
def add_innermost_arch_link(cur_arch, cur_map, portname):
    # import pdb; pdb.set_trace()
    arch_node = cur_arch.get_node_or_network(cur_map.get('dest'))[0]
    arch_port_name = cur_map.port.get('dest')

    # Check for port on arch_node, and add if not already there.
    if not arch_node.check_port(arch_port_name):
        arch_node.add_port(arch_port_name, 'in')
    # add link from net edge to arch node
    cur_arch.add_link('this', portname, cur_map.get('dest'), arch_port_name)

    # Finally see if we need to add a local schedule to the policy
    policy = get_static_policy_object(cur_arch, arch_node)
    if policy is not None and policy.name not in policy_handled:
        add_local_schedule(policy, arch_node)
    logger.debug("add_innermost_arch_link, arch_node: %s" % arch_node)


def get_static_policy_object(network, arch_node):
    """Return the policy object connected to the arch_node.
    Use the network because it contains the policy object."""
    logger.debug("%s connected to: %s" % (arch_node.name,
                                          arch.neighbors(arch_node.name)))
    # Only look at schedulers here.
    if len(arch_node.get_property('class')) == 0 or \
       arch_node.get_property('class')[0].value != 'hwscheduler':
        return
    for name in arch.neighbors(arch_node.name):
        node = network.get_node(name)[0]
        if node.get_property('class')[0].value == 'static':
            logger.debug("found static policy: %s" % name)
            return node


def add_local_schedule(policy, arch_node):
    # global policy_handled
    """Add local schedule to policy object pobject.
    Use arch_node as mapping destination."""
    if not gschedule:
        logger.warn("System is using static scheduling. "
                    "Add globalschedule property.\n")
    logger.debug("Adding schedule to arch_node: %s" % arch_node.name)
    cur_init = policy.get_property('init')[0].value
    if cur_init.find("#[-1]") != 0:
        logger.warn("Static policy object %s missing '#[-1]' marker"
                    % policy.name)
        return
    # TODO calculate local schedule from global schedule.
    # filter global static schedule, so we only have the taskids that
    # are mapped onto this scheduler.
    tasks = set()
    for k, v in mapdict.iteritems():
        if v == arch_node.name:
            tasks.add(task_ids[k])
    logger.debug("set of mapped tasks ids is: %s" % tasks)
    lschedule = [t for t in gschedule if t in tasks]
    local_schedule_str = "#[" + ','.join(str(i) for i in lschedule) + "]"
    logger.debug("Calculated local schedule: %s" % local_schedule_str)

    new_init = cur_init.replace("#[-1]", local_schedule_str)
    policy.set_property('init', new_init)
    policy_handled.add(policy.name)


# Add links from virtual component outer network to inner architecture
# component.
# portname is a numbered portname that is only used when the network
# is 'architecture' (or just for the first dest might be better).
def add_arch_link_chain(cur_arch, cur_map, portname, isfirst):
    if cur_map.get('side') == 'dest':
        if isfirst:  # outer most arch level.
            isfirst = False
        else:
            # FIXME: will this break if we change the mapping order?
            # So should we use the same approach as the virtual side?
            inner_arch = cur_arch.get_network(cur_map.get('name'))[0]
            inner_arch.add_port('mapport', 'in')
            cur_arch.add_link('this', portname, inner_arch.name, 'mapport')
            portname = 'mapport'  # only used for outer level.
        # ..and go one level deeper into architecture.
        cur_arch = cur_arch.get_network(cur_map.get('name'))[0]
    elif cur_map.get('side') == 'source':
        pass
    else:
        print "Mapping error, unkown side: %s" % cur_map.get('side')
        exit(2)

    # Recurse..
    if hasattr(cur_map, 'map'):
        add_innermost_arch_link(cur_arch, cur_map.map, portname)
    else:
        add_arch_link_chain(cur_arch, cur_map.mapping, portname, isfirst)


def main():
    global mapdict
    global arch
    global policy_handled
    global task_ids
    global gschedule

    if len(sys.argv) != 2:
        print "usage: %s <yml-sim file>" % sys.argv[0]
        sys.exit(2)

    # Open main simulation, virtual and architecture yml files.
    simulation = yml.parse(sys.argv[1])
    # Start virtarch with contents of virtual layer (0)
    virtarch = yml.parse(simulation.virtual.get("src"))
    arch = archyml.Architecture(simulation.architecture.get("src"), False)

    # We need the application yml to extract the globalschedule.
    app = appyml.Application(simulation.application.get("src"))
    globalschedule = app.yml.get_property('globalschedule')
    # Don't worry if we cannot find a global schedule. Only matters if
    # we are using static scheduling.
    if len(globalschedule) == 1:
        globalschedule = globalschedule[0].get_value()
        gschedule = map(int, globalschedule.split(','))
        logger.debug("Found global schedule: %s" % gschedule)
    else:
        gschedule = None

    # Record the policy object that received their local schedule.
    # Because we check for every mapped task a policy might be checked
    # twice.
    policy_handled = set()

    # Need name->taskid mapping
    task_ids = app.get_task_ids()
    logger.debug("taskid mapping: %s" % task_ids)

    # We also need the a dict of the mapping if we need to generate
    # local schedules. ([1] because we have two mappings in the
    # simulation file)
    mapping = ymlmap.parse(simulation.mapping[1].get("src"))
    mapdict = ymlmap.to_dict(mapping)
    logger.debug("mapdict: %s" % mapdict)

    # Collect all the all the virtual components.
    vcomps = sorted(n for n in
                    virtarch.xpath("/ns:network/ns:network",
                                   namespaces={'ns': yml.NAMESPACE}))

    # Don't want all the templates and mappings in the architecture.
    # The virtual component has all that info already. (1)
    for p in arch.components:
        p.remove_all_properties('mapping')
        p.remove_all_properties('template')
    # Now add the cleaned architecture to new virtarch (1)
    virtarch.append(arch.yml)

    # Global counter for mapports on the architecture network edge.
    mapport_count = 0
    # Here we map every vcomp onto an architecture component (2)
    for vcomp in vcomps:
        # Add links from vproc through to outer most network. (2b)
        add_link_chain(virtarch, vcomp.get_property('mapping')[0].mapping)

        # Need to add a new mapport the architecture network for this mapping
        portname = "mapport%d" % mapport_count
        arch.yml.add_port(portname, 'in')
        mapport_count += 1
        # Add link from vcomp network to architecture network edge (2c)
        # FIXME: 'architecture' should not be hardcode name here.
        virtarch.add_link(vcomp.name, 'mapport', 'architecture', portname)

        # Add links from architecture components outernetwork to the
        # inner component.
        add_arch_link_chain(
            virtarch, vcomp.get_property('mapping')[0].mapping,
            portname, True)
        # We have all the info, now we can remove mapping property
        vcomp.remove_all_properties('mapping')
        # hopefully not needed.
        # import pdb; pdb.set_trace()

    # Work is done, write the new virtarch layer to file.
    tree = virtarch.getroottree()
    tree.write(simulation.virtarch.get("src"), pretty_print=True,
               xml_declaration=True, encoding='UTF-8')

# Try this for logging.
logger = logging.getLogger(__name__.split('.')[-1])
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

if __name__ == "__main__":
    main()
