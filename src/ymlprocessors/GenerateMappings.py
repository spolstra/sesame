import argparse
import subprocess
from sesamemap import appyml, archyml, apparchmap
from pyyml import ymlmap

"""Generate mappings from an application, architecture pair.
Mappings can be enumerated, or random."""


def output_mapping(m, name, arch, nopng=False):
    ymlf, dotf, imgf = [name + ext for ext in ['.yml', '.dot', '.png']]
    ymlm = m.export_yml()
    ymlmap.store_mapping(ymlf, ymlm)
    if not nopng:
        dot_graph = ymlmap.make_dot(ymlm, arch)
        dot_graph.write_dot(dotf)
        subprocess.call('dot -Tpng -o%(imgf)s %(dotf)s' % locals(),
                        shell=True)


def check_map(m, i):
    if m.is_valid():
        print 'generating mapping %d' % i
    else:
        print 'invalid mapping found, exiting..'
        exit(1)


def main():
    # Handle arguments
    argparser = argparse.ArgumentParser(description="Mapping Generator")
    argparser.add_argument("applicationyml",
                           metavar="application_yml",
                           help="Application YML file")
    argparser.add_argument("architectureyml",
                           metavar="architecture_yml",
                           help="Architecture YML file")
    argparser.add_argument("-n", "--num_maps",
                           help="Number of mappings to generate [default 20]",
                           type=int, default=20)
    argparser.add_argument("-m", "--max",
                           help="Limit number of processor in the mapping",
                           type=int, default=None)
    argparser.add_argument("-r", "--random", action='store_true',
                           default=False,
                           help="Generate random mappings")
    argparser.add_argument("--nopng", action='store_true',
                           default=False,
                           help="Do not generate png images")
    args = argparser.parse_args()

    # random.seed(1) # we do not get the same mapping with the same seed.

    # Read app and arch
    app = appyml.Application(args.applicationyml)
    arch = archyml.Architecture(args.architectureyml, True, 25)

    if args.random:
        # Generate random mappings.
        # AppArchMap returns a random mapping
        for i in range(args.num_maps):
            m = apparchmap.AppArchMap(app, arch) # returns random mapping
            if args.max:
                # Only randomize again if we need limited mapping
                m.randomize(args.max)
            check_map(m, i)
            output_mapping(m, 'map_%d' % i, arch, args.nopng)
    else:
        # Enumerate the mappings.
        im = apparchmap.IterMappings(app, arch)
        for i, m in enumerate(im):
            if i >= args.num_maps:
                break
            check_map(m, i)
            output_mapping(m, 'map_%d' % i, arch, args.nopng)

if __name__ == "__main__":
    main()
