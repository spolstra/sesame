import argparse
import subprocess
import os

from pyyml import ymlmap
from sesamemap import archyml


def main():
    argparser = argparse.ArgumentParser(
        description="Visualise mappings")
    argparser.add_argument("mapping_yml",
                           help="Mapping YML")
    argparser.add_argument("arch_yml", nargs='?',
                           help="With Architecture YML unmapped processors "
                           "are also shown")
    argparser.add_argument("-o", "--dotoutput", default='dgraph.dot',
                           help="dot output file. Defaults to 'dgraph.dot'")
    argparser.add_argument("-p", "--png", action='store_true',
                            default=False, help="Also generate png output")
    args = argparser.parse_args()

    root = ymlmap.parse(args.mapping_yml)

    # dot graph of the mapping.
    if args.arch_yml is not None:
        arch = archyml.Architecture(args.arch_yml, False)
        dot_graph = ymlmap.make_dot(root, arch)
    else:
        dot_graph = ymlmap.make_dot(root)
    if dot_graph is not None:
        dot_graph.write_dot(args.dotoutput)
        if args.png:
            try:
                basename = os.path.splitext(args.mapping_yml)[0]
                subprocess.call(['dot', '-T', 'png', '-o',
                                 '{}.{}'.format(basename, 'png'),
                                 args.dotoutput])
            except Exception as e:
                print "Failed to generate png image:", e
    else:
        print "pydot is not installed"
