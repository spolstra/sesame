import sys
import xml.dom
import xml.dom.minidom

def main():
    if len(sys.argv) != 3:
        print "usage: python %s <in-file> <out-file>" % sys.argv[0]
        sys.exit(2)
    
    # Remove image properties 
    doc = xml.dom.minidom.parse(sys.argv[1])
    all_props = doc.getElementsByTagName('property')
    for prop in all_props:
        if prop.getAttribute('name') == 'image':
            prop.parentNode.removeChild(prop)
    
    file = open(sys.argv[2], 'w')
    doc.writexml(file)
