import pyyml.yml as yml

from lxml import etree
import sys

mapping = {}
created_procs = {}

NAMESPACE = 'http://sesamesim.sourceforge.net/YML_Map'

# Should read this from a library. But for now a string will do.
# Name attribute to be reset.
fifo_yml = """\
<network name="DFIFO" class="pearl_object">
  <property name="cost" value="10"/>
  <property name="class" value="dfifo"/>
  <node name="input" class="pearl_object">
    <property name="class" value="fifo_input"/>
    <property name="header" value="fifo_input.ps"/>
    <property name="source" value="fifo_input.pi"/>
    <port name="in0" dir="in">
      <property name="type" value="int"/>
    </port>
    <port name="dfifo" dir="out">
      <property name="type" value="int"/>
    </port>
    <property name="init" value="dfifo,1"/>
    <property name="header" value="channel.ps"/>
    <property name="source" value="channel.pi"/>
  </node>
  <node name="output" class="pearl_object">
    <property name="class" value="fifo_output"/>
    <property name="header" value="fifo_output.ps"/>
    <property name="source" value="fifo_output.pi"/>
    <port name="in1" dir="in">
      <property name="type" value="int"/>
    </port>
    <port name="dfifo" dir="out">
      <property name="type" value="int"/>
    </port>
    <property name="init" value="dfifo,1"/>
    <property name="header" value="vchannel.ps"/>
    <property name="source" value="vchannel.pi"/>
  </node>
  <port name="write" dir="in">
    <property name="datadir" value="in"/>
    <property name="type" value="int"/>
  </port>
  <port name="read" dir="in">
    <property name="datadir" value="out"/>
    <property name="type" value="int"/>
  </port>
  <link innode="this" inport="read" outnode="output" outport="in1">
  </link>
  <link innode="this" inport="write" outnode="input" outport="in0">
  </link>
  <node name="dfifo" class="pearl_object">
    <property name="class" value="dfifo"/>
    <property name="header" value="dfifo.ps"/>
    <property name="source" value="dfifo.pi"/>
    <port name="in0" dir="in">
      <property name="type" value="int"/>
    </port>
    <port name="in1" dir="in">
      <property name="type" value="int"/>
    </port>
    <property name="init" value="200"/>
  </node>
  <link innode="input" inport="dfifo" outnode="dfifo" outport="in0">
  </link>
  <link innode="output" inport="dfifo" outnode="dfifo" outport="in1">
  </link>
  <property name="template" value="vfifo">
    <network name="DFIFO" class="net">
      <port name="read" dir="in">
        <property name="type" value="vchannel"/>
      </port>
      <port name="write" dir="in">
        <property name="type" value="vchannel"/>
      </port>
      <node name="vdfifo" class="pearl_object">
        <property name="class" value="vdfifo"/>
        <property name="header" value="vdfifo.ps"/>
        <property name="source" value="vdfifo.pi"/>
        <port name="arch" dir="out">
          <property name="type" value="int"/>
        </port>
        <port name="in0" dir="in">
          <property name="type" value="int"/>
        </port>
        <port name="in1" dir="in">
          <property name="type" value="int"/>
        </port>
        <property name="init" value="arch"/>
      </node>
      <link innode="this" inport="write" outnode="vdfifo" outport="in0"/>
      <link innode="this" inport="read" outnode="vdfifo" outport="in1"/>
    </network>
  </property>
  <property name="mapping" value="vfifo">
    <mapping side="source" name="DFIFO">
      <mapping side="dest" name="architecture">
        <mapping side="dest" name="DFIFO">
          <map source="vdfifo" dest="dfifo">
            <port source="arch" dest="virtual"/>
          </map>
        </mapping>
      </mapping>
    </mapping>
  </property>
  <property name="defaulttemplate" value="vfifo"/>
</network>
"""

# Name attribute to be reset by create func.
processor_yml = """\
<node name="mp" class="pearl_object">
  <property name="cost" value="90"/>
  <property name="class" value="processor"/>
  <property name="header" value="processor.ps"/>
  <property name="source" value="processor.pi"/>
  <property name="init" value="#[8000,5000,5000,6000,7000,8000,1900,15000,11000,12000],#&lt;channel&gt;"/>
  <property name="template" value="vproc">
    <network name="X" class="net">
      <node name="vproc" class="pearl_object">
        <property name="class" value="vproc"/>
        <property name="header" value="vproc.ps"/>
        <property name="source" value="vproc.pi"/>
        <property name="init" value="trace,#&lt;vchannel&gt;"/>
        <port name="out0" dir="out">
          <property name="type" value="int"/>
        </port>
        <port name="trace" dir="out">
          <property name="type" value="int"/>
        </port>
        <property name="header" value="operations.h"/>
      </node>
      <port name="channels" dir="out">
        <property name="type" value="int"/>
      </port>
      <link innode="vproc" inport="out0" outnode="this" outport="channels"/>
    </network>
  </property>
  <property name="mapping" value="vproc">
    <mapping side="source" name="X">
      <mapping side="dest" name="architecture">
        <map source="vproc" dest="mp">
          <port source="trace" dest="virtual"/>
        </map>
      </mapping>
    </mapping>
  </property>
  <property name="template" value="vself">
    <network name="vself" class="net">
      <port name="read" dir="in">
        <property name="type" value="vchannel"/>
      </port>
      <port name="write" dir="in">
        <property name="type" value="vchannel"/>
      </port>
      <node name="vself" class="pearl_object">
        <property name="class" value="vself"/>
        <property name="header" value="vself.ps"/>
        <property name="source" value="vself.pi"/>
        <port name="in0" dir="in">
          <property name="type" value="int"/>
        </port>
        <port name="in1" dir="in">
          <property name="type" value="int"/>
        </port>
        <property name="init" value="arch"/>
        <port name="arch" dir="out">
          <property name="type" value="int"/>
        </port>
        <property name="header" value="vchannel.ps"/>
        <property name="source" value="vchannel.pi"/>
      </node>
      <link innode="this" inport="write" outnode="vself" outport="in0"/>
      <link innode="this" inport="read" outnode="vself" outport="in1"/>
    </network>
  </property>
  <property name="mapping" value="vself">
    <mapping side="source" name="vself">
      <mapping side="dest" name="architecture">
        <map source="vself" dest="mp">
          <port source="arch" dest="virtual"/>
        </map>
      </mapping>
    </mapping>
  </property>
  <property name="defaulttemplate" value="vself"/>
</node>
"""

YML_NS = "http://sesamesim.sourceforge.net/YML"
YML = "{%s}" % YML_NS
NSMAP = {None: YML_NS}


# Setup YML document
def get_doc():
    design = etree.Element(YML + "network", nsmap=NSMAP)
    tree = etree.ElementTree(design)
    return tree


def create_fifo(name):
    fifo = etree.fromstring(fifo_yml)
    fifo.set("name", name)
    # I should be using apparchmap class instead of doing this myself.
    # But for now I will fix the template mappings by hand.
    # for all properties with name=mapping
    # Here we assume only 1 mapping element per fifo. See create_proc
    # if we need to handle multiple.
    for e in fifo.findall(".//property[@name='mapping']")[0].iter():
        if e.get("name") == "DFIFO" and e.get("side") == "dest":
            e.set("name", name)
    return fifo


def create_proc(name):
    proc = etree.fromstring(processor_yml)
    proc.set("name", name)
    # fix-up template mappings, just as done in create_fifo
    for maps in proc.findall(".//property[@name='mapping']"):
        for e in maps.iter():
            if e.get("dest") == "mp":
                e.set("dest", name)
    return proc


def add_port(proc, portname, direction):
    port = etree.SubElement(proc, YML + "port")
    # set attr name and dir
    port.set("name", portname)
    port.set("dir", direction)
    # add property "type"
    prop = etree.SubElement(port, YML + "property")
    prop.set("name", "type")
    prop.set("value", "int")


def main():
    if len(sys.argv) != 3:
        print "usage: FifoFullConnect <num_procs> <yml-arch outfile>"
        sys.exit(2)

    # Create a fully connected fifo architecture with num_proc processors.
    num_proc = int(sys.argv[1])

    # Create empty architecture.
    design = yml.YMLNetwork(name="architecture")

    for n in range(num_proc):

        # Create processor n.
        proc_name = "P%d" % n
        proc = create_proc(proc_name)
        # and add it to the architecture.
        design.append(proc)

        # Because the all links connected to this processor are
        # channels that are collected by type into an array, we can
        # just use the same single port for all the links.
        add_port(proc, "out0", "out")

    # We need to keep track of the current read port to use for every
    # proc. Read port start at port number: num_proc-1

    # connect every proc to all other procs in the architecture.
    for source in range(num_proc):
        outport_count = 0
        dests = range(num_proc)
        # No need to connect proc to itself of course
        dests.remove(source)
        for dest in dests:
            # Create fifo between source and dest.
            fifo_name = "P%dP%d" % (source, dest)
            design.append(create_fifo(fifo_name))
            # Link source to fifo. (the write side)
            design.add_link("P%d" % source, "out0", fifo_name, 'write')
            # Link dest to fifo. (the read side)
            design.add_link("P%d" % dest, "out0", fifo_name, 'read')

    # Write architecture and mapping file.
    arch_file = open(sys.argv[2], 'w')

    tree = design.getroottree()
    tree.write(sys.argv[2], pretty_print=True)
    arch_file.close()
