import argparse
import re
from collections import defaultdict
from lxml import etree
from pyyml import ymlmap

SCC_PROC_BASE = "core_"

# core name generator
def core_names():
    i = 0;
    while True:
        yield SCC_PROC_BASE + str(i)
        i = i+1;

def main():
    # Handle arguments
    argparser = argparse.ArgumentParser(
                            description='YML_Map to SCC_Map converter')
    argparser.add_argument('ymlmapping', metavar='mapping.yml',
                           help='YML mapping description')
    argparser.add_argument('-o', help='SCC_Map output file',
                           dest='outfile')
    argparser.add_argument('-v', '--verbose', action='store_true',
                           default=False)
    args = argparser.parse_args()
    outfile = args.outfile if args.outfile is not None else 'SCCmap.xml'

    # SCC Dal tool wants processors names to be: core_NUMBER
    # Convert blabla_NUMBER --> core_NUMBER
    parse_procname = re.compile(r"(\D+)(\d+)")
    # If cannot parse a core name we map it to a valid name.
    core_map = defaultdict(core_names().next)

    # Parse yml mapping
    mymap = ymlmap.parse(args.ymlmapping)

    # Create SCCmap by hand
    # Set up namespace stuff
    SCC_NS = "http://www.tik.ee.ethz.ch/~euretile/schema/MAPPING"
    SCC = "{%s}" % SCC_NS
    XSI_NS = "http://www.w3.org/2001/XMLSchema-instance"
    NSMAP = {None : SCC_NS,
             "xsi" : XSI_NS}

    # Create root element 'mapping'
    # The process network attribute of the mapping element is set
    # to the base of the input file name.
    # The name attribute of the mapping element is set the name of the
    # outmost yml mapping name.
    sccmap = etree.Element(SCC + "mapping", nsmap=NSMAP, name=mymap.name,
                           processnetwork=args.ymlmapping.split('.')[0])
    # Set the location attribute.
    location_attribute = '{%s}schemaLocation' % XSI_NS
    sccmap.set(location_attribute, "http://www.tik.ee.ethz.ch/~euretile/schema/MAPPING  http://www.tik.ee.ethz.ch/~euretile/schema/mapping.xsd")


    # Read yml mapping and create SCC elements.
    for task,proc in ymlmap.to_dict(mymap).iteritems():
        parsedproc = parse_procname.match(proc)
        if parsedproc is not None:
            proc_name = SCC_PROC_BASE + parsedproc.group(2)
        else:
            # Guess mapping.
            proc_name = core_map[proc]
            print 'Warning: Could not convert processor name "%s" to core_number format\nGuessing: "%s"' % (proc, proc_name)

        if args.verbose:
            print "%s - %s" % (task, proc)
        binding = etree.SubElement(sccmap, SCC + "binding", name=task)
        etree.SubElement(binding, SCC + "process", name=task)
        etree.SubElement(binding, SCC + "processor", name=proc_name)

    # Write to file
    tree = sccmap.getroottree()
    tree.write(outfile, pretty_print=True,xml_declaration=True,encoding='UTF-8')
