import sys
import os
from collections import defaultdict

from sesamemap import appyml, archyml
from pyyml import yml, ymlmap
import copy
import argparse

# Input:
#   Application yml:  used for the topology
#   Architecture yml: used for the template code and template mapping
#   Mapping yml:      used to select the target template code and mapping
#
# Output:
#   Virtual layer yml
#
# Algorithm:
# The topology of the application is created with the templates
# selected by the mapping.
#
# Support for static scheduling: taskid's are found and inserted into
# the init string.
#
# Error checking:
# * First the mapping is checked to see if the component exists and
#   contains the selected template.
# * Then the template mapping is compared with the actual template of the
#   component. Errors are generated when a mismatch is detected.
# * Inconsistent use of taskid for static scheduling is also reported.


# global linkid counter.
linkid = 0


# Check if the template mapping is correct. Check the mapping for
# both the template side (source) and the architecture side
# (destination).
def check_mapping(arch, vname, curmap, tmpl, tmplmap):
    # Go down into mapping property to find the map.
    # Check every source mapping with the template to see if its
    # correct.
    # get_inner_mapping loop?
    maplevel = tmplmap
    tmpl_level = tmpl
    arch_level = None
    while (len(maplevel.xpath('ns:mapping',
                              namespaces={'ns': yml.NAMESPACE})) != 0):
        maplevel = maplevel.mapping
        if maplevel.get_side() == 'source':
            # assert list should have length 1.
            tmpl_level = tmpl_level.get_node_or_network(
                maplevel.get_name())[0]
            # for now [0] will trigger an error.
        elif maplevel.get_side() == 'dest':
            if arch_level is None:
                if arch.yml.name == maplevel.name:  # special: toplevel
                    arch_level = arch.yml
                else:
                    raise yml.YMLError("Cannot find mapping for '%s', found"
                                       " destination '%s' instead" %
                                       (arch.yml.name, maplevel.name),
                                       maplevel.sourceline)
            else:
                # assert list should have length 1.
                arch_level = arch_level.get_node_or_network(
                    maplevel.get_name())[0]
            # for now [0] will trigger an error.
        else:
            raise yml.YMLError("Error in template mapping!")

    # maplevel now contains map element, check the map
    try:
        tmpl_level.get_node(maplevel.map.get_source())
    except yml.YMLError:
        raise ymlmap.MappingError('Error in mapping: %s --> %s.%s\n'
                'Template mapping specifies "%s", but I cannot find this'\
                ' in template itself' % (
                    vname, curmap.get("dest"), curmap.get_dtmpl(),
                    maplevel.map.get_source()))
    except Exception:
        raise yml.YMLError(
            'Error creating "%s" virtual processor for application'
            'node "%s"' % (curmap.get_dtmpl(), vname))

    try:
        arch_level.get_node_or_network(maplevel.map.get_dest())
    except yml.YMLError as e:
        msg1 = 'Invalid mapping: %s --> %s.%s' % (
            vname, curmap.get('dest'), curmap.get_dtmpl())
        msg2 =  'Mapping specified "%s" and template mapping specified "%s"'\
            % (curmap.get('dest'), maplevel.map.get_dest())
        raise yml.YMLError("\n".join([e.msg, msg1, msg2]))
    try:
        tmpl_level.get_node(maplevel.map.get_source())[0].get_port(
            maplevel.map.port.get('source'))
    except yml.YMLError as e:
        # TODO: give whole template mapping. collect in check phase.
        msg1 = "Invalid template mapping found: %s --> %s.%s" % (
            vname, curmap.get('dest'), curmap.get_dtmpl())
        msg2 = '"%s" does not have a port named "%s"' % (
            tmpl_level.node.name, maplevel.map.port.get('source'))
        raise yml.YMLError("\n".join([e.msg, msg1, msg2]))
    if maplevel.map.port.get('dest') != 'virtual':
        msg1 = "Invalid template mapping found: %s --> %s.%s" % (
            vname, curmap.get('dest'), curmap.get_dtmpl())
        msg2 = 'Destination port should be named "virtual", not "%s"' % (
            maplevel.map.port.get('dest'))
        raise ymlmap.MappingError("\n".join([msg1, msg2]))

# Set name of outermost mapping source element.
def set_source_mapping(templ_map, name):
    if templ_map.get('side') == 'source':
        templ_map.set("name", name)
    else:
        if templ_map.get('side') != 'dest':
            raise yml.YMLError("Error in template of application task %s"
                    % name)
        set_source_mapping(templ_map.mapping, name)


# Record memory footprint of app tasks on arch components.
# And check if they are overallocated.
def record_memfootprint(appnode, proc, footprint):
    # Check if task and component both have memory size attributes.
    mem_total = proc.get_memory_size()
    mem_requested = appnode.get_prop_val_safe('totalmem')
    if mem_total is None or mem_requested is None:
        return
    mem_requested = int(mem_requested)
    mem_total = int(mem_total)

    # Check for overallocation.
    if footprint[proc.name] + mem_requested > mem_total:
        raise ymlmap.MappingError("Component {} overallocated:\n"
                    "Task {} requests: {}, Component capacity: {}, "
                    "already allocated {}".\
                    format(proc.name, appnode.name, mem_requested, mem_total,
                        footprint[proc.name]))
    # Add task memory request to component footprint.
    footprint[proc.name] += mem_requested


# Get mapping element curmap and set vname
# Need to do things different for task and channel
def get_map_and_vname(appnode, mapping, unique_tids, tid_map):
    global linkid
    try:
        if isinstance(appnode, appyml.FIFOChannel):
            # App channel: Do lookup on the original x.x->y.y name
            curmap = mapping.get_map(appnode.__str__())
            # But give the vchannel a generated name
            vname = "_link%d" % linkid
            linkid += 1
        else:  # We have an App task
            vname = appnode.name
            curmap = mapping.get_map(vname)
            # Check if task has a task id for scheduling
            if len(appnode.get_property('taskid')) == 1:
                tid = appnode.get_property('taskid')[0].get_value()
                if tid in unique_tids:
                    raise yml.YMLError('Task %s reuses taskid %s'
                                       % (vname, tid))
                else:
                    # Store tid map and record its use.
                    tid_map[vname] = tid
                    unique_tids.add(tid)

    except ymlmap.NoMappingFound as e:
        if args.nomap_ok:
            print 'Warning: Application component "%s" not mapped'\
                % e.msg
            return (None, None)
        else:
            raise ymlmap.NoMappingFound('No mapping found for application '
                    'node "%s"' % e.msg)
    except yml.YMLError as e:
        print "Error: Task id's in the application must be unique."
        print "Error: %s" % e.msg
        raise
    return (curmap, vname)


def main():
    try:
        virtuallayer_generator()
    except ymlmap.MultipleMappingsFound as e:
        print "{} exiting due to error:".format(
                os.path.basename(sys.argv[0]))
        print 'Application node "%s" is mapped more than once.' % e.msg
        exit(1)
    except ymlmap.MappingError as map_error:
        print "{} exiting due to mapping error:".format(
                os.path.basename(sys.argv[0]))
        print map_error
        exit(1)
    except yml.YMLError as e:
        print "{} exiting due to error:".format(
                os.path.basename(sys.argv[0]))
        print e
        exit(1)


def get_proc_and_tmpl(arch, vname, curmap):
    proc = tmpl = tmplmap = None
    try:
        proc = arch.component_table[curmap.get("dest")]
        tmpl = proc.get_templates()[curmap.get_dtmpl()]  # store dict?
        tmplmap = proc.get_template_mappings()[curmap.get_dtmpl()]
    except KeyError as e:
        msg1 = 'Invalid mapping: %s --> %s' % (vname, curmap.get("dest"))
        if proc is not None:
            if tmpl is not None:
                msg2 = 'Template mapping for "%s" not found'\
                    % curmap.get_dtmpl()
            else:
                msg2 = 'Template "%s" not found' % curmap.get_dtmpl()
        else:
            msg2 = 'Processor "%s" not found' % curmap.get("dest")
        raise ymlmap.MappingError("\n".join([msg1, msg2]))
    return (proc, tmpl, tmplmap)


def virtuallayer_generator():
    # Handle arguments
    argparser = argparse.ArgumentParser(
        description="Virtual Layer Generator")
    argparser.add_argument("simulationyml", metavar="simulation_yml",
                           help="The simulation yml file")
    argparser.add_argument("--nomap-ok", action="store_true",
                           dest='nomap_ok',
                           help="Ignore application tasks without mappings")
    global args
    args = argparser.parse_args()

    # Open application, mapping and architecture yml files.
    simulation = yml.parse(args.simulationyml)
    app = appyml.Application(simulation.application.get("src"))
    try:
        arch = archyml.Architecture(simulation.architecture.get("src"),
                                    False)
    except yml.YMLError as e:
        raise yml.YMLError(e.msg + "\nError while parsing architecture "
                "yml file '%s'" % simulation.architecture.get("src"))

    # Simulation file contains two mapping elements.
    # We assume the order: 1) appsync_map, 2) map
    # So here we select the real mapping (map)
    mapping = ymlmap.parse(simulation.mapping[1].get("src"))

    # Create empty virtual layer.
    virtual = yml.YMLNetwork(name='application')

    unique_tids = set()
    tid_map = dict()
    footprint = defaultdict(int) # TODO make class?
    # Translate every application tasks en channels
    for appnode in app.processes + app.channels:
        curmap, vname = get_map_and_vname(appnode, mapping, unique_tids,
                                          tid_map)
        if curmap is None:
            continue  # no mapping found.

        # Find the target proc, template code and template mapping.
        proc, tmpl, tmplmap = get_proc_and_tmpl(arch, vname, curmap)

        # check template mapping
        try:
            check_mapping(arch, vname, curmap, tmpl, tmplmap)
        except AttributeError:
            raise ymlmap.MappingError('Architecture component "%s" is '
                  'missing attributes, cannot check mapping' %
                  arch.yml.name)
        except yml.YMLError as e:
            raise ymlmap.MappingError(e.msg + "\nWhile mapping application "
                            "task '%(vname)s'" % locals())

        # Need to copy because we change them
        newtmpl = copy.deepcopy(tmpl.network)
        newtmplmap = copy.deepcopy(tmplmap)

        # Find init string:
        # First find the template (virt proc) that is used in the mapping.
        m = newtmplmap.xpath("//ns:map", namespaces={'ns': yml.NAMESPACE})
        # Get the name from the mapping.
        node_name = m[0].get('source')
        n = newtmpl.xpath("//ns:node[@name='%s']" %
                node_name, namespaces={'ns': yml.NAMESPACE})[0]
        cur_init = n.get_property('init')[0].get_value()

        # record memory footprint of app tasks on arch components.
        record_memfootprint(appnode, proc, footprint)

        # Need to do extra work for a vchannel
        if isinstance(appnode, appyml.FIFOChannel):
            # If it's a vchannel add linkid
            newtmpl.add_property('linkid', appnode.__str__())
            # And add links from virtual procs to this vchannel
            virtual.add_link(appnode.get('innode'), 'channels', vname,
                             'write')
            virtual.add_link(appnode.get('outnode'), 'channels', vname, 'read')

            # Get bufsize from app yml and append it to init string vchannel
            buf_val = appnode.get_prop_val_safe('bufsize')
            if buf_val is not None:
                new_init = cur_init + "," + buf_val
                n.set_property('init', new_init)

        elif vname in tid_map:
            # If the application task has a task id we try to replace
            # the ID marker in it's vproc template with that task_id.
            # Warn user if ID marker is missing.

            # For an task node check if we need to add the task id for
            # scheduling.

            # replace ID in template vproc's init string with the taskid
            # of the application task.
            if cur_init.find("ID") != 0:
                print "WARNING: Application task '%s' contains a taskid." %\
                    vname
                print "WARNING: But template '%s' used in the mapping has"\
                    "no ID marker." % node_name

            new_init = cur_init.replace("ID", tid_map[vname])
            n.set_property('init', new_init)

        # Give the virtual processor the correct name (was 'X' or templ name)
        newtmpl.set("name", vname)
        # Rename the template mapping and add the template mapping
        # to the new virtual processor
        set_source_mapping(newtmplmap.mapping, vname)

        newtmpl.append(newtmplmap)
        virtual.append(newtmpl)

    # Output generated virtual layer to file
    tree = virtual.getroottree()
    tree.write(simulation.virtual.get("src"),
               pretty_print=True, xml_declaration=True, encoding='UTF-8')


if __name__ == "__main__":
    main()
