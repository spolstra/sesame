#!/usr/bin/env bash

trap cleanup EXIT

if [[ $# -ne 1 ]]; then
    echo "usage $0 <file.yml>"
    exit 1
fi

tmp_dot=$(mktemp)
ymlpp -d $1 > $tmp_dot
dot -Tpdf $tmp_dot -o$(basename $1 .yml).pdf

cleanup() {
    rm -f $tmp_dot
}
