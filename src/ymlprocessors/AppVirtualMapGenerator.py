from sesamemap import appyml
from pyyml import ymlmap
import sys


def main():
    if len(sys.argv) != 2:
        print "usage: %s <yml-app file>" % sys.argv[0]
        sys.exit(2)

    # Open application yml
    app = appyml.Application(sys.argv[1])

    # Create an new mapping with application as source and dest
    outer_mapping = ymlmap.new_mapping("application", "application")
    # Need to add our map's to the innermapping ofcourse
    mapping = outer_mapping.get_inner_mapping()

    # Add a map for every operation
    for op in app.operations:
        mapping.add_instruction(op.get_name(), op.get_name())

    # Add a map for every appnode
    for node in app.processes:
        curmap = mapping.add_map(node.name, node.name)
        # add all the app ports to this map
        for port in node.ports():
            curmap.add_port(port.name, port.name)

    # Add a map for every link
    for link in app.channels:
        mapping.add_map(str(link), str(link))

    # Mapping created, send to stdout
    ymlmap.store_mapping(sys.stdout, mapping)

if __name__ == "__main__":
    main()
