/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef CPPPROCESS_INCLUDE_ONCE
#define CPPPROCESS_INCLUDE_ONCE

#include "yapi/process.h"
#include "processloader.h"
#include <setjmp.h>

#include <string>

/// Specialization of Process for C++ users.
/**
 * Alows user processes written in C++ to be connected to a PN.
 *
 * @see ProcessLoader
 */
class CPPProcessLoader : public ProcessLoader
{
    private:
        /// A string giving the location of the shared library holding the C++ code.
        std::string libName;
        /// A string giving the name of the C++ class to be loaded.
        std::string className;

        jmp_buf jmpEnv;

        Process *process;

    public:
        CPPProcessLoader();
        virtual ~CPPProcessLoader();

        virtual bool load();
        void run();
        void exit();
};

#endif // CPPPROCESS_INCLUDE_ONCE

