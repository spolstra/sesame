#ifndef SELECT_CC
#define SELECT_CC

//// implementation of select()

template<class T0, class T1> inline
unsigned int select( InPort<T0>& p0,
             InPort<T1>& p1) {
  return 0;
}

template<class T0, class T1> inline
unsigned int select( InPort<T0>& p0,
            OutPort<T1>& p1) {
  return 0;
}

template<class T0, class T1> inline
unsigned int select(OutPort<T0>& p0,
            OutPort<T1>& p1) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select( InPort<T0>& p0,
             InPort<T1>& p1,
             InPort<T2>& p2) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select( InPort<T0>& p0,
             InPort<T1>& p1,
            OutPort<T2>& p2) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select( InPort<T0>& p0,
            OutPort<T1>& p1,
            OutPort<T2>& p2) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select(OutPort<T0>& p0,
            OutPort<T1>& p1,
            OutPort<T2>& p2) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0,
             InPort<T1>& p1,
             InPort<T2>& p2,
             InPort<T3>& p3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0,
             InPort<T1>& p1,
             InPort<T2>& p2,
            OutPort<T3>& p3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0,
             InPort<T1>& p1,
            OutPort<T2>& p2,
            OutPort<T3>& p3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0,
            OutPort<T1>& p1,
            OutPort<T2>& p2,
            OutPort<T3>& p3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select(OutPort<T0>& p0,
            OutPort<T1>& p1,
            OutPort<T2>& p2,
            OutPort<T3>& p3) {
  return 0;
}

//////////////////////////////////////////////////////

template<class T0, class T1> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
             InPort<T1>& p1, unsigned int n1) {
  return 0;
}

template<class T0, class T1> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
            OutPort<T1>& p1, unsigned int n1) {
  return 0;
}

template<class T0, class T1> inline
unsigned int select(OutPort<T0>& p0, unsigned int n0, 
            OutPort<T1>& p1, unsigned int n1) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
             InPort<T1>& p1, unsigned int n1,
             InPort<T2>& p2, unsigned int n2) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
             InPort<T1>& p1, unsigned int n1,
            OutPort<T2>& p2, unsigned int n2) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
            OutPort<T1>& p1, unsigned int n1,
            OutPort<T2>& p2, unsigned int n2) {
  return 0;
}

template<class T0, class T1, class T2> inline
unsigned int select(OutPort<T0>& p0, unsigned int n0, 
            OutPort<T1>& p1, unsigned int n1,
            OutPort<T2>& p2, unsigned int n2) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
             InPort<T1>& p1, unsigned int n1,
             InPort<T2>& p2, unsigned int n2,
             InPort<T3>& p3, unsigned int n3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
             InPort<T1>& p1, unsigned int n1,
             InPort<T2>& p2, unsigned int n2,
            OutPort<T3>& p3, unsigned int n3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
             InPort<T1>& p1, unsigned int n1,
            OutPort<T2>& p2, unsigned int n2,
            OutPort<T3>& p3, unsigned int n3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select( InPort<T0>& p0, unsigned int n0, 
            OutPort<T1>& p1, unsigned int n1,
            OutPort<T2>& p2, unsigned int n2,
            OutPort<T3>& p3, unsigned int n3) {
  return 0;
}

template<class T0, class T1, class T2, class T3> inline
unsigned int select(OutPort<T0>& p0, unsigned int n0, 
            OutPort<T1>& p1, unsigned int n1,
            OutPort<T2>& p2, unsigned int n2,
            OutPort<T3>& p3, unsigned int n3) {
  return 0;
}

#endif
