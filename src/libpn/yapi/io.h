/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef IO_H
#define IO_H

//class Connector;
class Id;

template<class T>
class In
{
    public:
        virtual ~In() {}

        virtual void read(T &s) = 0;
        virtual void read(T &s, int tid, int annot, int annot2) = 0;
        virtual void read(T *p, int tid, int annot, int annot2,
                unsigned int n) = 0;
        virtual void setIn(Id *in) {};

        //virtual Connector* connector() = 0;
};

template<class T>
class Out
{
    public:
        virtual ~Out() {}

        virtual void write(const T &s) = 0;
        virtual void write(const T &s, int tid, int annot, int annot2) = 0;
        virtual void write(const T *p, int tid, int annot, int annot2,
                unsigned int n) = 0;
        virtual void setOut(Id *out) {};

        //virtual Connector* connector() = 0;
};

#endif

