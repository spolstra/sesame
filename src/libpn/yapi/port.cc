#ifndef PORT_CC
#define PORT_CC

#include "../../libyml/ymldefs.h"
#include <typeinfo>

extern bool ymlgen_outputTypes;

//// Implementation of class InPort

template<class T> 
InPort<T>::InPort(const Id& n, In<T> *i) : Id(n), in(i) {
  in->setIn(this);
}

template<class T> inline
void InPort<T>::read(T& t) {
  in->read(t, 0, 0, 0);
}

template<class T> inline
void InPort<T>::read(T& t, int tid, int annot, int annot2) {
  in->read(t, tid, annot, annot2);
}

template<class T> inline
void InPort<T>::read(T* p, int tid, int annot, int annot2, unsigned int n) {
  in->read(p, tid, annot, annot2, n);
}

template<class T> inline
void InPort<T>::dumpYML(std::ostream &stream, int tablevel) {
  tabs(stream, tablevel);
  stream << '<' << YML_ELEM_PORT << ' ';
  stream << YML_ATTR_NAME << "=\"" << name() << "\" ";
  stream << YML_ATTR_DIR << "=\"" << YML_ATTR_DIR_IN << "\">" << std::endl;

  if (ymlgen_outputTypes) {
    tabs(stream, tablevel + 1);
    dumpYMLProp(stream, PROP_NAME_TYPE, typeid(T).name());
  }

  tabs(stream, tablevel);  
  stream << "</" << YML_ELEM_PORT << ">" << std::endl;
}

//template<class T> inline
//Connector* InPort<T>::connector() {
//}


//// Implementation of class OutPort

template<class T> 
OutPort<T>::OutPort(const Id& n, Out<T>* o) : Id(n), out(o) { 
  out->setOut(this);
}

template<class T> inline
void OutPort<T>::write(const T& t) {
  out->write(t, 0, 0, 0);
}

template<class T> inline
void OutPort<T>::write(const T& t, int tid, int annot, int annot2) {
  out->write(t, tid, annot, annot2);
}

template<class T> inline
void OutPort<T>::write(const T* p, int tid, int annot, int annot2,
        unsigned int n) {
  out->write(p, tid, annot, annot2, n);
}

template<class T> inline
void OutPort<T>::dumpYML(std::ostream &stream, int tablevel) {
  tabs(stream, tablevel);
  stream << '<' << YML_ELEM_PORT << ' ';
  stream << YML_ATTR_NAME << "=\"" << name() << "\" ";
  stream << YML_ATTR_DIR << "=\"" << YML_ATTR_DIR_OUT << "\">" << std::endl;

  if (ymlgen_outputTypes) {
    tabs(stream, tablevel + 1);
    dumpYMLProp(stream, PROP_NAME_TYPE, typeid(T).name());
  }

  tabs(stream, tablevel);  
  stream << "</" << YML_ELEM_PORT << ">" << std::endl;
}


//// implementation of utility functions

template<class T> inline
void read(InPort<T>& s, T& t) {
  s.read(t);
}

template<class T> inline
void read(InPort<T>& s, T* p, unsigned int n) {
  s.read(p, n);
}

template<class T> inline
void write(OutPort<T>& s, const T& t) {
  s.write(t);
}

template<class T> inline
void write(OutPort<T>& s, const T* p, unsigned int n) {
  s.write(p, n);
}
#endif


