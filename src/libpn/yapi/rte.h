/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef RTE_H
#define RTE_H

/*class rteNetwork;
class rteProcess;
class rteFifo;
class rteSelect;

class NetworkImpl;
class ProcessImpl;
class FifoImpl;
class SelectImpl;

class Rte
{
public:
  Rte();
  virtual ~Rte();

  virtual rteNetwork* create(NetworkImpl& n) = 0;
  virtual rteProcess* create(ProcessImpl& p) = 0;
  virtual rteFifo*    create(FifoImpl& f) = 0;
  virtual rteSelect*  create(SelectImpl& s) = 0;
};*/

#define YMLGEN_ARGS_VAR "YMLGENARGS"

class ProcessNetwork;
void start(ProcessNetwork &n);

#endif

