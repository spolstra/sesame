/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "process.h"
#include "../../libpn/processloader.h"
#include "../../libpn/pnxmltags.h"
#include "../../libyml/ymldefs.h"
#include <iostream>

using namespace std;

Process::Process(const Id &n) : Id(n), processLoader(0)
{
}

string Process::getFullName() const
{
    return processLoader ? processLoader->getFullName() : "";
}

void Process::main(ProcessLoader *processLoader)
{
    this->processLoader = processLoader;
    main();
}

void Process::execute(const char *instr)
{
    execute(instr, 0, 0, 0);
}

void Process::execute(const char *instr, int tid, int annot, int annot2)
{
    processLoader->execute(instr, tid, annot, annot2);
}


Id Process::id(const char *name)
{
    return Id(name, this);
}

void Process::dumpYML(std::ostream &stream, int tablevel)
{
    tabs(stream, tablevel);
    stream << '<' << YML_ELEM_NODE << ' ';
    stream << YML_ATTR_NAME << "=\"" << name() << "\" ";
    stream << YML_ATTR_CLASS << "=\"" << CPP_PROCESS <<  "\">" << endl;

    tabs(stream, tablevel + 1);
    dumpYMLProp(stream, PROP_NAME_CLASS, type());

    dumpYMLChildren(stream, tablevel + 1);

    tabs(stream, tablevel);
    stream << "</" << YML_ELEM_NODE << '>' << endl;
}
