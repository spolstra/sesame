/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "id.h"
#include "process.h"
#include "../../libyml/ymldefs.h"

#include "../../BasicUtils/BasicException.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

Id id(const char *n)
{
    return Id(n, NULL);
}

Id::Id(const Id &i)
{
    nm = strdup(i.nm);
    pa = i.pa;
    portNum = i.portNum;

    if (pa) {
        ASSERT_OR_THROW("Unknown YAPI type!", pa->getYAPIType() != yapiUnknown);
        Id *me = this;
        pa->children.push_back(me);
    }
}

Id::Id(const char *n, Id *p)
{
    nm = strdup(n);
    pa = p;
    portNum = -1;

    if (pa) {
        ASSERT_OR_THROW("Unknown YAPI type!", pa->getYAPIType() != yapiUnknown);
        Id *me = this;

        if (getYAPIType() != yapiUnknown) {
            pa->children.push_back(me);
        }
    }
}

Id::~Id()
{
    if (nm) {
        free(nm);
    }
}

Id &Id::operator=(const Id &i)
{
    if (nm) {
        free(nm);
    }

    nm = strdup(i.nm);
    pa = i.pa;
    portNum = i.portNum;
    return *this;
}

const char *Id::fullName(char *buf) const
{
    ASSERT_OR_THROW("NULL buf!", buf);

    if (pa && pa != this) {
        pa->fullName(buf);
    }

    strcat(buf, nm);
    return buf;
}

Id Id::id(const char *n)
{
    return Id(n, this);
}


void Id::dumpYMLProp(std::ostream &stream, const char *name,
                     const char *value)
{
    stream << '<' << YML_ELEM_PROPERTY << ' ';
    stream << YML_ATTR_NAME << "=\"" << name << "\" ";
    stream << YML_ATTR_VALUE << "=\"" << value << "\"/>" << std::endl;
}

void Id::dumpFullName(ostream &stream)
{
    if (pa) {
        pa->dumpFullName(stream);
        stream << '.';
    }

    stream << name();
}

void Id::dumpYMLChildren(std::ostream &stream, int tablevel)
{
    for (unsigned int i = 0; i < children.size(); i++) {
        children[i]->dumpYML(stream, tablevel);
    }
}

void Id::tabs(ostream &stream, int num)
{
    int i;

    for (i = 0; i < num; i++) {
        stream << '\t';
    }
}

ostream &operator<<(ostream &stream, Id &i)
{
    i.dumpYML(stream, 0);
    return stream;
}





