/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef IOCONNECTIONS_INCLUDE_ONCE
#define IOCONNECTIONS_INCLUDE_ONCE

#include "io.h"
#include "string.h"
#include "../cppprocessloader.h"
#include "../datablockclass.h"

#include "../../BasicUtils/BasicException.h"
#include "../../BasicUtils/BasicString.h"

#include <unistd.h>

#ifdef DEBUG
// acquire lock before printing, avoid messing up messages.
extern BasicLockable g_debug_output_lock;
#endif // DEBUG

template<class T>
class Reader : public In<T>
{
        CPPProcessLoader *loader;
        int portId;
        const char *name;

    public:
        Reader(CPPProcessLoader *loader, const char *name);
        virtual ~Reader() {}

        virtual void read(T &value);
        virtual void read(T &value, int tid, int annot, int annot2);
        virtual void read(T *p, int tid, int annot, int annot2, unsigned int n);
        //virtual FifoBase* selector() const;
};

template<class T> inline
Reader<T>::Reader(CPPProcessLoader *loader, const char *name) :
    loader(loader), name(name)
{

    portId = loader->findPort(name)->getID();
}

template<class T> inline
void Reader<T>::read(T &value)
{
    read(value, 0, 0, 0);
}

template<class T> inline
void Reader<T>::read(T &value, int tid, int annot, int annot2)
{
#ifdef DEBUG
    g_debug_output_lock.lock();
    cerr << "Reading from " << loader->getName() << "." << name << endl;
    cerr << std::flush;
    g_debug_output_lock.unlock();
#endif

    DataBlockClass *data = loader->read(portId, tid, annot, annot2);
    ASSERT_OR_THROW("NULL data block", data);

    if (data->getSize() != sizeof(T))
        THROW(string("Read datablock size ") + BasicString(data->getSize()) +
              " does not match expected size " + BasicString(sizeof(T)) + ".");

    value = *((T *)data->getData());
    delete((T *)data->getData());  // Data
    delete data->detatch(); // DataBlock
    delete data;  // DataBlockClass
}

template<class T> inline
void Reader<T>::read(T *p, int tid, int annot, int annot2, unsigned int n)
{
    //  THROW("Not implemented!");

    for (unsigned int i = 0; i < n; i++) {
        read(p[i], tid, annot, annot2);
    }
}

// *** Writer ***
template<class T>
class Writer : public Out<T>
{
        CPPProcessLoader *loader;
        int portId;
        const char *name;

    public:
        Writer(CPPProcessLoader *loader, const char *name);
        virtual ~Writer() {}


        virtual void write(const T &value);
        virtual void write(const T &value, int tid, int annot, int annot2);
        virtual void write(const T *p, int tid, int annot,
                int annot2, unsigned int n);
        //virtual FifoBase* selector() const;
};

template<class T> inline
Writer<T>::Writer(CPPProcessLoader *loader, const char *name) :
    loader(loader), name(name)
{

    portId = loader->findPort(name)->getID();
}

template<class T> inline
void Writer<T>::write(const T &value)
{
    write(value, 0, 0, 0);
}

template<class T> inline
void Writer<T>::write(const T &value, int tid, int annot, int annot2)
{
#ifdef DEBUG
    g_debug_output_lock.lock();
    cerr << "Writing to " << loader->getName() << '.' << name << endl;
    cerr << std::flush;
    g_debug_output_lock.unlock();
#endif

    DataBlock *dataBlock = new DataBlock;

    T *copy = new T(value);
    dataBlock->size = sizeof(T);
    dataBlock->data = (char *)copy;
    DataBlockClass *data = new DataBlockClass(dataBlock);
    loader->write(portId, data, tid, annot, annot2);
}

template<class T> inline
void Writer<T>::write(const T *p, int tid, int annot,
        int annot2, unsigned int n)
{
    //  THROW("Not implemented!");

    for (unsigned int i = 0; i < n; i++) {
        write(p[i], tid, annot, annot2);
    }
}
#endif // IOCONNECTIONS_INCLUDE_ONCE
