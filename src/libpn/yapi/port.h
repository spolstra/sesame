/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PORT_H
#define PORT_H

#include "../pnxmltags.h"
#include <iostream>

#include "id.h"
#include "io.h"

#define YMLGEN_PORT_BASENAME "YMLGen_Port_"

template<class T>
class InPort : public Id, public In<T>
{
        In<T> *in;

    public:
        InPort(const Id &n, In<T> *i);
        virtual ~InPort() {
            delete in;
        }

        void read(T &value);
        void read(T &value, int tid, int annot, int annot2);
        void read(T *p, int tid, int annot, int annot2, unsigned int n);

        yapiObject_t getYAPIType() {
            return yapiPort;
        }
        virtual void dumpYML(std::ostream &stream, int tablevel);
};

template<class T>
class OutPort : public Id, public Out<T>
{
        Out<T> *out;

    public:
        OutPort(const Id &n, Out<T> *i);
        virtual ~OutPort() {
            delete out;
        }

        void write(const T &t);
        void write(const T &t, int tid, int annot, int annot2);
        void write(const T *p, int tid, int annot, int annot2, unsigned int n);

        yapiObject_t getYAPIType() {
            return yapiPort;
        }
        virtual void dumpYML(std::ostream &stream, int tablevel);
};


//// Utility functions

template<class T>
void read(InPort<T> &s, T &t);

template<class T>
void read(InPort<T> &s, T *p, unsigned int n);

template<class T>
void write(OutPort<T> &s, const T &t);

template<class T>
void write(OutPort<T> &s, const T *p, unsigned int n);


//// Select
//#include "select.h"

// include implementation of template classes
#include "port.cc"

#endif
