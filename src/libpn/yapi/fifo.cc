#ifndef FIFO_CC
#define FIFO_CC

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "port.h"

//// Implementation of class Fifo

template<class T>
Fifo<T>::Fifo(const Id& n) : Id(n) { 
  in = 0;
  out = 0;
}

template<class T>
Fifo<T>::Fifo(const Id& n, unsigned int l) : Id(n) {
  in = 0;
  out = 0;
}

template<class T>
Fifo<T>::Fifo(const Id& n, unsigned int l, unsigned int h) : Id(n) {
  in = 0;
  out = 0;
}

template<class T> inline
void Fifo<T>::read(T& t)  {
}

template<class T> inline
void Fifo<T>::read(T* p, unsigned int n) {
}

template<class T> inline
void Fifo<T>::write(const T& t) {
}

template<class T> inline
void Fifo<T>::write(const T* p, unsigned int n) {
}

template<class T> inline
void Fifo<T>::dumpYML(std::ostream &stream, int tablevel) {
  assert(in && out);


  // Fill in missing ports and fifos
  Id *thisParent = parent();

  // Trace down out port
  Id *outParent = out->parent();
  Id *lastOutPort = out;
  while (outParent != thisParent && outParent->parent() != thisParent) {
    Fifo<T> *fifo = new Fifo<T>(outParent->parent()->id("???fifo???"));

    char *portName = (char *)malloc(strlen(YMLGEN_PORT_BASENAME) + 20);
    sprintf(portName, "%s%d", YMLGEN_PORT_BASENAME, outParent->parent()->getNextPortNum());
    OutPort<T> *port = new OutPort<T>(outParent->parent()->id(portName), *fifo);
    free(portName);

    fifo->setIn(port);
    fifo->setOut(lastOutPort);

    lastOutPort = port;
    outParent = outParent->parent();
  }

  const char *outParentName;
  if (outParent == thisParent) outParentName = "this";
  else outParentName = outParent->name();

  // Trace down in port
  Id *inParent = in->parent();
  Id *lastInPort = in;
  while (inParent != thisParent && inParent->parent() != thisParent) {
    Fifo<T> *fifo = new Fifo<T>(inParent->parent()->id("???fifo???"));

    char *portName = (char *)malloc(strlen(YMLGEN_PORT_BASENAME) + 20);
    sprintf(portName, "%s%d", YMLGEN_PORT_BASENAME, inParent->parent()->getNextPortNum());
    InPort<T> *port = new InPort<T>(inParent->parent()->id(portName), *fifo);
    free(portName);

    fifo->setOut(port);
    fifo->setIn(lastInPort);

    lastInPort = port;
    inParent = inParent->parent();
  }
  
  const char *inParentName;
  if (inParent == thisParent) inParentName = "this";
  else inParentName = inParent->name();


  // Output YML
  tabs(stream, tablevel);
  stream << '<' << YML_ELEM_LINK << ' ';
  stream << YML_ATTR_OBJECT1 << "=\"" << inParentName << "\" ";
  stream << YML_ATTR_PORT1 << "=\"" << lastInPort->name() << "\" ";
  stream << YML_ATTR_OBJECT2 << "=\"" << outParentName << "\" ";
  stream << YML_ATTR_PORT2 << "=\"" << lastOutPort->name() << "\"";
  stream << "/>" << std::endl;
}
#endif
