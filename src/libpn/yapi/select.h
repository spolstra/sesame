/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SELECT_H
#define SELECT_H

#ifndef PORT_H
#include "port.h"
#endif

template<class T0, class T1> inline
unsigned int select(InPort<T0> &p0,
                    InPort<T1> &p1);

template<class T0, class T1> inline
unsigned int select(InPort<T0> &p0,
                    OutPort<T1> &p1);

template<class T0, class T1> inline
unsigned int select(OutPort<T0> &p0,
                    OutPort<T1> &p1);

template<class T0, class T1, class T2> inline
unsigned int select(InPort<T0> &p0,
                    InPort<T1> &p1,
                    InPort<T2> &p2);

template<class T0, class T1, class T2> inline
unsigned int select(InPort<T0> &p0,
                    InPort<T1> &p1,
                    OutPort<T2> &p2);

template<class T0, class T1, class T2> inline
unsigned int select(InPort<T0> &p0,
                    OutPort<T1> &p1,
                    OutPort<T2> &p2);

template<class T0, class T1, class T2> inline
unsigned int select(OutPort<T0> &p0,
                    OutPort<T1> &p1,
                    OutPort<T2> &p2);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0,
                    InPort<T1> &p1,
                    InPort<T2> &p2,
                    InPort<T3> &p3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0,
                    InPort<T1> &p1,
                    InPort<T2> &p2,
                    OutPort<T3> &p3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0,
                    InPort<T1> &p1,
                    OutPort<T2> &p2,
                    OutPort<T3> &p3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0,
                    OutPort<T1> &p1,
                    OutPort<T2> &p2,
                    OutPort<T3> &p3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(OutPort<T0> &p0,
                    OutPort<T1> &p1,
                    OutPort<T2> &p2,
                    OutPort<T3> &p3);

//////////////////////////////////////////////////////

template<class T0, class T1> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    InPort<T1> &p1, unsigned int n1);

template<class T0, class T1> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    OutPort<T1> &p1, unsigned int n1);

template<class T0, class T1> inline
unsigned int select(OutPort<T0> &p0, unsigned int n0,
                    OutPort<T1> &p1, unsigned int n1);

template<class T0, class T1, class T2> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    InPort<T1> &p1, unsigned int n1,
                    InPort<T2> &p2, unsigned int n2);

template<class T0, class T1, class T2> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    InPort<T1> &p1, unsigned int n1,
                    OutPort<T2> &p2, unsigned int n2);

template<class T0, class T1, class T2> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    OutPort<T1> &p1, unsigned int n1,
                    OutPort<T2> &p2, unsigned int n2);

template<class T0, class T1, class T2> inline
unsigned int select(OutPort<T0> &p0, unsigned int n0,
                    OutPort<T1> &p1, unsigned int n1,
                    OutPort<T2> &p2, unsigned int n2);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    InPort<T1> &p1, unsigned int n1,
                    InPort<T2> &p2, unsigned int n2,
                    InPort<T3> &p3, unsigned int n3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    InPort<T1> &p1, unsigned int n1,
                    InPort<T2> &p2, unsigned int n2,
                    OutPort<T3> &p3, unsigned int n3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    InPort<T1> &p1, unsigned int n1,
                    OutPort<T2> &p2, unsigned int n2,
                    OutPort<T3> &p3, unsigned int n3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(InPort<T0> &p0, unsigned int n0,
                    OutPort<T1> &p1, unsigned int n1,
                    OutPort<T2> &p2, unsigned int n2,
                    OutPort<T3> &p3, unsigned int n3);

template<class T0, class T1, class T2, class T3> inline
unsigned int select(OutPort<T0> &p0, unsigned int n0,
                    OutPort<T1> &p1, unsigned int n1,
                    OutPort<T2> &p2, unsigned int n2,
                    OutPort<T3> &p3, unsigned int n3);

// include implementation of template functions
#include "select.cc"

#endif
