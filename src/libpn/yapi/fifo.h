/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef FIFO_H
#define FIFO_H

#include <pnxmltags.h>
#include <ymldefs.h>
#include <iostream>

#include "io.h"
#include "id.h"

template<class T>
class Fifo : public Id, public In<T>, public Out<T>
{
        Id *in;
        Id *out;

    public:
        Fifo(const Id &n);
        Fifo(const Id &n, unsigned int lo);
        Fifo(const Id &n, unsigned int lo, unsigned int hi);

        void read(T &t);
        void read(T *p, unsigned int n);
        void setIn(Id *in) {
            this->in = in;
        }

        void write(const T &t);
        void write(const T *p, unsigned int n);
        void setOut(Id *out) {
            this->out = out;
        }

        yapiObject_t getYAPIType() {
            return yapiFIFO;
        }
        virtual void dumpYML(std::ostream &stream, int tablevel);
};

#include "fifo.cc"

#endif
