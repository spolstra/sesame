/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "network.h"
#include "../../libpn/pnxmltags.h"
#include "../../libyml/ymldefs.h"
#include <iostream>
#include <vector>

using namespace std;

ProcessNetwork::ProcessNetwork(const Id &n) : Id(n)
{
}

Id ProcessNetwork::id(const char *name)
{
    return Id(name, this);
}

extern std::vector<char *> ymlgen_libraries;

void ProcessNetwork::dumpYML(std::ostream &stream, int tablevel)
{
    tabs(stream, tablevel);
    stream << '<' << YML_ELEM_NETWORK << ' ';
    stream << YML_ATTR_NAME << "=\"" << name() << "\" ";
    stream << YML_ATTR_CLASS << "=\"PN\">" << endl;

    // If we are at the top level add in libaries from the command line
    if (!parent() || parent() == this) {
        unsigned int i;

        for (i = 0; i < ymlgen_libraries.size(); i++) {
            tabs(stream, tablevel + 1);
            dumpYMLProp(stream, PROP_NAME_LIB, ymlgen_libraries[i]);
        }
    }

    dumpYMLChildren(stream, tablevel + 1);

    tabs(stream, tablevel);
    stream << "</" << YML_ELEM_NETWORK << '>' << endl;
}
