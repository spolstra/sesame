/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef ID_H
#define ID_H

// class Id : Identification of objects by name and type

#include <iostream>
#include <vector>

class Id
{
        std::vector<Id *> children;

    public:
        typedef enum yapiObject_t {yapiUnknown = -1,
                                   yapiFIFO,
                                   yapiPort,
                                   yapiProcess,
                                   yapiNetwork
                                  } yapiObject_t;

        Id(const Id &i);
        Id(const char *n, Id *p);
        Id &operator=(const Id &i);
        virtual ~Id();

        Id *parent() const {
            return pa;
        }
        const char *name() const {
            return nm;
        }
        const char *fullName(char *buf = 0) const;
        operator const char *() const {
            return name();
        }

        Id id(const char *n);

        int getNextPortNum() {
            return ++portNum;
        }
        virtual yapiObject_t getYAPIType() {
            return yapiUnknown;
        }
        void dumpYMLProp(std::ostream &stream, const char *name, const char *value);
        virtual void dumpYML(std::ostream &/*stream*/, int /*tablevel*/) {};
        virtual void dumpYMLChildren(std::ostream &stream, int tablevel);
        void dumpFullName(std::ostream &stream);
        void tabs(std::ostream &stream, int num);

        friend std::ostream &operator<<(std::ostream &stream, Id &i);

    private:
        char *nm; // name
        Id   *pa; // parent
        int portNum;
};

std::ostream &operator<<(std::ostream &stream, Id &i);

Id id(const char *n);
#endif
