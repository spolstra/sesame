/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "rte.h"
#include "network.h"
#include "id.h"

#include "../../libyml/ymldefs.h"

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vector>

using namespace std;

void Syntax()
{
    cerr << "<YMLGenProg> [options]" << endl;
    cerr << "\t-h\tThis help page" << endl;
    cerr << "\t-t\tOutput port types (mangled types only)" << endl;
    exit(1);
}

bool ymlgen_outputTypes = false;
std::vector<char *> ymlgen_libraries;

void start(ProcessNetwork &n)
{
    char *argstr = getenv(YMLGEN_ARGS_VAR);

    if (argstr) {
        char **argv = (char **)malloc(2 * sizeof(const char *));
        argv[0] = (char *)"";
        argv[1] = strtok(argstr, " \t");
        int argc = 1;

        while (argv[argc]) {
            argc++;
            argv = (char **)realloc(argv, (argc + 1) * sizeof(const char *));
            argv[argc] = strtok(NULL, " \t");
        }

        if (argv[argc]) {
            argc++;
        }

        int c;

        while ((c = getopt(argc, argv, "htl:")) != -1) {
            switch (c) {
                case 'h':
                    Syntax();
                    break;

                case 't':
                    ymlgen_outputTypes = true;
                    break;

                case 'l':
                    ymlgen_libraries.push_back(optarg);
                    break;

                default:
                    Syntax();
                    break;
            }
        }
    }

    cout << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
    cout << "<!DOCTYPE ";

    switch (n.getYAPIType()) {
        case Id::yapiNetwork:
            cout << YML_ELEM_NETWORK;
            break;

        case Id::yapiProcess:
            cout << YML_ELEM_NODE;
            break;

        case Id::yapiFIFO:
            cout << YML_ELEM_LINK;
            break;

        case Id::yapiPort:
            cout << YML_ELEM_PORT;
            break;

        default:
            cerr << "Invalid YAPIType=" << n.getYAPIType() << endl;
            exit(1);
    }

    cout << " SYSTEM \"YML.dtd\">" << endl;
    cout << n << endl;
}
