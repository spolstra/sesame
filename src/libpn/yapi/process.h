/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PROCESS_H
#define PROCESS_H

#include "id.h"
#include <iostream>
#include <string>
using namespace std;

class ProcessLoader;

class Process : public Id
{
        int argc;
        char **argv;
        ProcessLoader *processLoader;

    public:
        Process(const Id &n);

        virtual void main(ProcessLoader *processLoader);
        void setArgs(int argc, char **argv) {
            this->argc = argc;
            this->argv = argv;
        }
        void printWorkload(std::ostream &/*o*/) {}

        int getArgc() const {
            return argc;
        }
        char **getArgv() const {
            return argv;
        }
        std::string getFullName() const;
        ProcessLoader *getLoader() const {
            return processLoader;
        }

        yapiObject_t getYAPIType() {
            return yapiProcess;
        }
        virtual void dumpYML(std::ostream &stream, int tablevel);

        // atrace needs to acces this execute
        void execute(const char *instr, int tid, int annot, int annot2);
    protected:
        virtual const char *type() const {
            return "Process";
        }

        Id id(const char *);
        virtual void main() = 0;
        void execute(const char *instr);
};

#endif
