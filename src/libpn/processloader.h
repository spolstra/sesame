/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PROCESSLOADER_H
#define PROCESSLOADER_H

#include "../BasicUtils//BasicThread.h"
#include "../libbasic/libmanager.h"
#include "../libyml/ymlnode.h"

#include <string.h>
#include <string>
#include <iostream>
#include <map>

// Forward declarations
class PNConnection;
class DataBlockClass;

/// The base ProcessLoader
/**
 * Implements the base functionality of a ProcessLoader common to
 * all types.  Specializations of this class provide access to
 * processes of different languages.
 * @see CProcessLoader
 * @see CPPProcessLoader
 */
class ProcessLoader : public BasicThread, public YMLNode
{
        LibManager libManager;

        /// Number of process arguments
        int pargc;

        /// Process arguments
        char **pargs;

        typedef enum {S_STOPPED, S_RUNNING, S_READING, S_SENDING_READ,
                      S_WRITING, S_SENDING_WRITE, S_SENDING_EXECUTE
                     } state_t;
        state_t state;
        int stateID;

    public:
        ProcessLoader();
        virtual ~ProcessLoader();

        DataBlockClass *read(int portId, int tid, int annot, int annot2);
        void write(int portId, DataBlockClass *dataBlock, int tid,
                int annot, int annot2);
        void execute(const char *operation, int tid, int annot, int annot2);
        void execute(int operation, int tid, int annot, int annot2);

        LibManager *getLibManager() {
            return &libManager;
        }
        void loadLibraries();

        void loadProcessArgs();
        int getProcessArgc() {
            return pargc;
        }
        char **getProcessArgs() {
            return pargs;
        }

        virtual void init();

        virtual bool load();
        virtual void exit();

        std::string getStateString();

        friend std::ostream &operator<<(std::ostream &stream, ProcessLoader &p);
};

std::ostream &operator<<(std::ostream &stream, ProcessLoader &p);

#endif
