/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PN_H
#define PN_H

#include "processloader.h"
#include "pnconnection.h"

#include "../libbasic/libmanager.h"
#include "../libyml/ymlnetwork.h"

#include <string>
#include <iostream>

class YMLPort;

///  Process Network
/**
 * Users of this class can build PNs by adding processes and connections
 * to an instance of PN.
 *
 * Warning: This class is not thread safe.
 */
class PN : public YMLNetwork
{
        LibManager libManager;

    public:
        PN();

        void start();
        void stop();
        void cancel();
        void join();

        bool isRunning();

        LibManager *getLibManager() {
            return &libManager;
        }
        void loadLibraries();

        virtual void init();

        void printState(std::ostream &stream, unsigned int indent = 0);

        friend std::ostream &operator<<(std::ostream &stream, PN &pn);

    private:
        void prestart();
        void realstart();
};

std::ostream &operator<<(std::ostream &stream, PN &pn);

#endif
