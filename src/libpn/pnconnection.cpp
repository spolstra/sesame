/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pnconnection.h"
#include "processloader.h"

#include <unistd.h>
#include <string.h>

using namespace std;


PNConnection::PNConnection()
{
    sizeLimit = 0;
    reader = writer = NULL;
    readerPort = writerPort = NULL;
    numReads = numWrites = 0;
    dataQueueSize = 0;
    // Added dataBlockSize field to fill size field in trace event
    dataBlockSize = 0;

}

PNConnection::~PNConnection() {}


/**
 * Block until a datablock is available.  However if it is found
 * that the writers thread has exited a NULL will be returned.
 *
 * @return the data read or NULL
 */
DataBlockClass *PNConnection::read()
{
    DataBlockClass *data;

    lock();

    while (dataQueue.empty()) {

        // Check on the writer thread
        if (writer && !writer->isRunning()) {
            unlock();
            reader->exit(); // Should not return
            THROW("Returned from reader exit function.");
        }

        // This function automaticly releases lock on entrance
        // and aquires it again on exit.
        // Times out in in milliseconds
        timedWait(100);
    }

    // If we get here there is data availiable.
    data = dataQueue.back();
    dataQueue.pop_back();
    dataQueueSize--;
    numReads++;
    signal();
    unlock();

    return data;
}

/**
 * Write a datablock to the queue.  This is a non-blocking
 * operation unless a sizeLimit is set on the queue in which
 * case blocking is possible.  If a limit is set, the queue
 * is full and the reader thread is no longer running the
 * data will not be written to the queue.
 *
 * @param data data to be written
 */
void PNConnection::write(DataBlockClass *data)
{
    lock();

    while (sizeLimit && dataQueueSize >= sizeLimit) {
        if (reader && !reader->isRunning()) {
            unlock();
            return; // Sorry cannot write data.
        }

        // This call releases and then reaquires the lock.
        timedWait(100);
    }

    // Set dataBlockSize field on first write (simon)
    if (dataBlockSize == 0) {
        dataBlockSize = data->getSize();
    }

    dataQueue.push_front(data);
    dataQueueSize++;
    numWrites++;
    signal();
    unlock();
}

/**
 * Check if the queue is empty.
 *
 * @return True if empty
 */
bool PNConnection::isEmpty()
{
    bool empty;
    lock();
    empty = dataQueue.empty();
    unlock();
    return empty;
}

/**
 * Get the current number of datablocks in the queue.
 *
 * @return the number of dataBlocks in the queue
 */
unsigned int PNConnection::getSize()
{
    lock();
    unsigned int size = dataQueueSize;
    unlock();
    return size;
}

/**
 * Get the size of datablock type of the connection.
 * The first write will set field to the correct value.
 * The first read could send a trace event with datasize == 0
 *
 * @return the data size of the dataBlock type in the queue
 *
 */
unsigned int PNConnection::getDataBlockSize()
{
    lock();

    // If dataBlockSize is not set, preempt thread. This increases the
    // chance of a write setting dataBlockSize
    while (dataBlockSize == 0) {
        timedWait(100);

        // We could be waiting for a write that will never happen.
        // If our writer is finished, stop waiting and exit.
        if (dataBlockSize == 0 && writer && !writer->isRunning()) {
            unlock();
            reader->exit(); // Should not return
            THROW("Returned from reader exit function.");
        }
    }

    unsigned int size = dataBlockSize;
    unlock();
    return size;
}

ostream &operator<<(ostream &stream, PNConnection &c)
{
    if (!c.getWriter()) {
        stream << "Unconnected ";
    } else stream << c.getWriter()->getName() << '.'
                      << c.getWriterPort()->getName() << ' ';

    stream << "--> ";

    if (!c.getReader()) {
        stream << "Unconnected";
    } else stream << c.getReader()->getName() << '.'
                      << c.getReaderPort()->getName();

    return stream;
}


#ifdef PNCONNECTION_TEST
#include "basicthread.h"
#include "basiclockable.h"
#include <stdio.h>

void *producer(void *thread)
{
#ifdef PNCTEST_STREAMS
    cout << "Producer started" << endl;
#else
    printf("Producer started\n");
#endif


    BasicThread *threadPtr = (BasicThread *)thread;
    PNConnection *connection = (PNConnection *)threadPtr->getData();

    int i = 0;

    while (!threadPtr->shouldShutdown()) {
        if (connection->getSize() < 100) {
            DataBlockClass *dataBlock = new DataBlockClass((char *)&i, sizeof(int));

#ifdef PNCTEST_STREAMS
            cout << "Producer sending block " << i << endl;
#else
            printf("Producer sending block %d\n", i);
#endif
            i++;
            connection->write(dataBlock);
        }

        sleep(0);
    }

#ifdef PNCTEST_STREAMS
    cout << "Producer ended" << endl;
#else
    printf("Producer ended\n");
#endif
    return NULL;
}

void *consumer(void *thread)
{
#ifdef PNCTEST_STREAMS
    cout << "Consumer started" << endl;
#else
    printf("Consumer started\n");
#endif

    BasicThread *threadPtr = (BasicThread *)thread;
    PNConnection *connection = (PNConnection *)threadPtr->getData();

    while (!threadPtr->shouldShutdown()) {
        DataBlockClass *dataBlock = connection->read();

        int i = *(int *)dataBlock->getData();
#ifdef PNCTEST_STREAMS
        cout << "Consumer received block " << i << endl;
#else
        printf("Consumer received block %d\n", i);
#endif
        delete dataBlock;
        sleep(0);
    }

#ifdef PNCTEST_STREAMS
    cout << "Consumer ended" << endl;
#else
    printf("Consumer ended\n");
#endif
    return NULL;
}


int main(int argc, char *argv[])
{
    BasicThread producerThread1(producer);
    BasicThread consumerThread1(consumer);
    PNConnection connection1;

    BasicThread producerThread2(producer);
    BasicThread consumerThread2(consumer);
    PNConnection connection2;

#ifdef PNCTEST_STREAMS
    cout << "Starting producer1" << endl;
#else
    printf("Starting producer1\n");
#endif
    producerThread1.setData(&connection1);
    producerThread1.start();

#ifdef PNCTEST_STREAMS
    cout << "Starting consumer1" << endl;
#else
    printf("Starting consumer1\n");
#endif
    consumerThread1.setData(&connection1);
    consumerThread1.start();

#ifdef PNCTEST_STREAMS
    cout << "Starting producer2" << endl;
#else
    printf("Starting producer2\n");
#endif
    producerThread2.setData(&connection2);
    producerThread2.start();

#ifdef PNCTEST_STREAMS
    cout << "Starting consumer2" << endl;
#else
    printf("Starting consumer2\n");
#endif
    consumerThread2.setData(&connection2);
    consumerThread2.start();

    getchar();

    // Stopping consumer first because it blocks until it receives a message.
    // This could cause it to hang on shutdown waiting for a message that will
    // not come if the producer has already stopped.
#ifdef PNCTEST_STREAMS
    cout << "Stopping and joining consumer1" << endl;
#else
    printf("Stopping and joining consumer1\n");
#endif
    consumerThread1.stop();
    consumerThread1.join();

#ifdef PNCTEST_STREAMS
    cout << "Stopping and joining producer1" << endl;
#else
    printf("Stopping and joining producer1\n");
#endif
    producerThread1.stop();
    producerThread1.join();

#ifdef PNCTEST_STREAMS
    cout << "Stopping and joining consumer2" << endl;
#else
    printf("Stopping and joining consumer2\n");
#endif
    consumerThread2.stop();
    consumerThread2.join();

#ifdef PNCTEST_STREAMS
    cout << "Stopping and joining producer2" << endl;
#else
    printf("Stopping and joining producer2\n");
#endif
    producerThread2.stop();
    producerThread2.join();

    return 0;
}
#endif



