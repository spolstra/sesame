/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "processloader.h"
#include "pn.h"
#include "pnxmltags.h"

#include "../libyml/ymlport.h"
#include "../libymlmap/tracechannel.h"

#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "../BasicUtils//BasicException.h"
#include "../BasicUtils//BasicString.h"

#include <sstream>
#include <string>
#include <vector>

using namespace std;

/**
 * Constructs a ProcessLoader
 *
 */
ProcessLoader::ProcessLoader() : pargc(0), pargs(0), state(S_STOPPED),
    stateID(-1) {}

ProcessLoader::~ProcessLoader()
{
    if (pargs) {
        int i;

        for (i = 0; i < pargc; i++) {
            free(pargs[i]);
        }

        delete [] pargs;
    }
}

void ProcessLoader::init()
{
    PN *pn = (PN *)getNetwork();
    libManager.setParent(pn->getLibManager());
}

bool ProcessLoader::load()
{
    state = S_RUNNING;

    loadLibraries();

    return true;
}

void  ProcessLoader::exit()
{
    state = S_STOPPED;
}

/*
First emit trace event, then do read or write!

The problem is caused by the combination of fixed sized trace buffers
between the application model and architecture model and the
non-preemptive scheduling in Pearl.

Let's assume that the trace event is generated *after* the read or
write is done and show a deadlock scenario.

    A <--- C <--- B
   (r)
   | |    | |    |w|
   | |    | |    |w|
   | |    | |    |w|
   | |    | |    |w|
   \ /    \ /    \ /   app
- - - - - - - - - - - - - -
VP [X]    [ ]    [ ]  arch

figure: Deadlock scenario with trace after read.

Task A's trace queue is empty and wants to read, and B has filled up it's
trace queue completely with writes.
Task C will first try to read data from B and then write data to A.

Task B has filled its trace queue and has stopped writing, task C will
not get all the data it needs to generate data for A.  Task A will try
to read, but no trace event is generated because there is no data so
the read does not complete.

VP_A (The virtual processor of A) is now scheduled by pearl and will
try to read a trace event. Since the trace queue is still empty VP_A
will block and halt the whole pearl simulation. This will deadlock the
whole system simulation, because the writes in B's trace queue will
never be consumed so the application simulation cannot continue.

If we generate the trace event before we read/write in the application
model the deadlock will never occur:
Task A will first generate a read event and then try to read. If VP_A
is scheduled first it will not lock the simulation because it will have a
read event to read.  B's trace events will be handled, and C will get
all its data needed to generate data for A and A will eventually
get it's data.
*/

/**
 *  Calls come from the user code via an exposed API.
 *  Reads from the connected port.
 *
 *  Also sends a read event through the trace channel if mapped.
 *
 * @see PNConnection::read()
 *
 * @param portID The id of the in port
 *
 * @return A data block if successful NULL otherwise.  NULLs occur
 *   if the port is not connected.
 */
DataBlockClass *ProcessLoader::read(int portID, int tid,
        int annot, int annot2)
{
    try {

        YMLPort *port = getPort(portID);
        PNConnection *connection = (PNConnection *)port->getConnection();

        ASSERT_OR_THROW("Cannot read from an out port!",
                        port->getDir() == YMLPort::dIn);

        state = S_SENDING_READ;
        stateID = portID;

        // SIMON: Use new getDataBlockSize function to get the size.
        if (getTraceChannel()) {
            getTraceChannel()->sendRead(portID, connection->getDataBlockSize(), connection->getCommChannelID(), tid, annot, annot2);
        }

        state = S_READING;
        DataBlockClass *dataBlock = connection->read();

        state = S_RUNNING;
        return dataBlock;
    } catch (...) {
        state = S_RUNNING;
        throw;
    }
}

/**
 *  Calls come from the user code via an exposed API.
 *  Writes to the connected port.  The write will not occur
 *  if the port specified is not connected or invalid or the
 *  PNConnection has a limit, is full, and the reader thread has
 *  already exited.
 *
 *  Also sends a write event through the trace channel if mapped.
 *
 * @see PNConnection::write()
 *
 * @param portID The id of the in port
 * @param dataBlock The data to be written.
 */
void ProcessLoader::write(int portID, DataBlockClass *dataBlock, int tid,
        int annot, int annot2)
{
    try {
        ASSERT_OR_THROW("Cannot write NULL data block!", dataBlock);

        YMLPort *port = getPort(portID);
        PNConnection *connection = (PNConnection *)port->getConnection();

        ASSERT_OR_THROW("Cannot write to an in port!",
                        port->getDir() == YMLPort::dOut);

        state = S_SENDING_WRITE;
        stateID = portID;

        // SIMON: works for writing, because we have the datablock. read
        // more difficult. (see above)
        // The old fix doesn't work (sizeof is a compile-time operator)
        // The Reader or Writer class (our caller) knows the datasize
        // because it is templated. It could pass it.
        if (getTraceChannel()) {
            getTraceChannel()->sendWrite(portID, dataBlock->getSize(),
                    connection->getCommChannelID(), tid, annot, annot2);
        }

        state = S_WRITING;
        connection->write(dataBlock);

        state = S_RUNNING;
    } catch (...) {
        state = S_RUNNING;
        throw;
    }
}

/**
 * Sends an execute event through the trace channel if mapped.
 *
 * @param operation The operation being executed
 */
void ProcessLoader::execute(int operation, int tid, int annot, int annot2)
{
    state = S_SENDING_EXECUTE;
    stateID = operation;

    try {

        if (getTraceChannel()) {
            getTraceChannel()->sendExec(operation, tid, annot, annot2);
        }

    } catch (...) {
        state = S_RUNNING;
        throw;
    }

    state = S_RUNNING;
}

/**
 * Sends an execute event through the trace channel if mapped.
 *
 * @param operation The operation being executed
 */
void ProcessLoader::execute(const char *operation, int tid,
        int annot, int annot2)
{
    if (getTraceChannel()) {
        long id = getTraceChannel()->getInstructionId(operation);
        ASSERT_OR_THROW(string("'") + operation + "' instruction not mapped!",
                        id >= 0);

        execute(id, tid, annot, annot2);
    }
}

void ProcessLoader::loadLibraries()
{
    unsigned int i;

    for (i = 0; i < getNumProperties(); i++) {
        if (getPropName(i).compare(PROP_NAME_LIB) == 0) {
            libManager.loadLibrary(getPropValue(i));
        }
    }
}

void ProcessLoader::loadProcessArgs()
{
    std::vector<string> args_array;
    args_array.push_back(getName());

    for (unsigned int i = 0; i < getNumProperties(); i++) {
        if (getPropName(i).compare(PROP_NAME_ARG) == 0) {
            args_array.push_back(getPropValue(i));
        }
    }

    pargc = args_array.size();
    pargs = new char *[pargc];

    for (int i = 0; i < pargc; i++) {
        pargs[i] = strdup(args_array[i].c_str());
    }
}

string ProcessLoader::getStateString()
{
    string stateStr = "<state value=\"";
    YMLPort *port;

    switch (state) {
        case S_STOPPED:
            stateStr += "STOPPED";
            break;

        case S_RUNNING:
            stateStr += "RUNNING";
            break;

        case S_SENDING_READ:
            stateStr += "SENDING READ";
            break;

        case S_READING:
            stateStr += "READING";
            break;

        case S_SENDING_WRITE:
            stateStr += "SENDING WRITE";
            break;

        case S_WRITING:
            stateStr += "WRITING";
            break;

        case S_SENDING_EXECUTE:
            stateStr += "SENDING EXECUTE";
            break;

        default:
            stateStr += "UNKNOWN";
            break;
    }

    stateStr += "\" ";

    switch (state) {
        case S_SENDING_READ:
        case S_READING:
        case S_SENDING_WRITE:
        case S_WRITING:
            port = getPort(stateID);
            stateStr += "port=\"";

            if (port) {
                stateStr += port->getName();
            } else {
                stateStr += "(null)";
            }

            stateStr += "\"";
            break;

        case S_SENDING_EXECUTE:
            stateStr += "instruction=\"";
            stateStr += BasicString(stateID);
            stateStr += "\"";
            break;

        default:
            break;
    }

    stateStr += "/>";

    return stateStr;
}

ostream &operator<<(ostream &stream, ProcessLoader &p)
{
    stream << p.getName();
    return stream;
}
