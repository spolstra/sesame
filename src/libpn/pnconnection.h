/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PNCONNECTION_H
#define PNCONNECTION_H

#include "datablockclass.h"

#include "../libyml/ymlconnection.h"
#include "../BasicUtils//BasicCondition.h"
#include "../BasicUtils//BasicException.h"

#include <pthread.h>
#include <iostream>
#include <list>

// Forward declarations
class ProcessLoader;
class YMLPort;

/// Process Network Connection
/**
 * This class implements the connections of the Kahn paradigm.
 * Reading is blocking and writing non-blocking.  However if
 * sizeLimit is set blocking during a write may occur.
 * It is expected that reading and writing be done by different
 * threads so the class has been made thread safe.
 *
 */
class PNConnection : public BasicCondition, public YMLConnection
{
        // Attributes
    private:
        std::list<DataBlockClass *> dataQueue;
        unsigned int dataQueueSize;

        ProcessLoader *reader;
        ProcessLoader *writer;
        YMLPort *readerPort;
        YMLPort *writerPort;

        /// This variable sets a maximum size for the queue.
        /// A limit of zero means no limit.
        unsigned int sizeLimit;

        /// Number of reads since creation.
        int numReads;

        /// Number of writes since creation.
        int numWrites;

        // SIMON: datablocksize (set at first write)
        int dataBlockSize;

        // Operations
    public:
        PNConnection();
        ~PNConnection();

        DataBlockClass *read();
        void write(DataBlockClass *data);
        int getNumReads() {
            return numReads;
        }
        int getNumWrites() {
            return numWrites;
        }


        /// Clears all data in the queue
        void clear() {
            lock();
            dataQueue.clear();
            dataQueueSize = 0;
            unlock();
        }
        bool isEmpty();
        unsigned int getSize();
        unsigned int getDataBlockSize();

        ProcessLoader *getWriter() {
            return writer;
        }
        ProcessLoader *getReader() {
            return reader;
        }
        void setReader(ProcessLoader *reader, YMLPort *port) {
            ASSERT_OR_THROW("NULL parameter", reader && port);
            this->reader = reader;
            readerPort = port;
        }
        void setWriter(ProcessLoader *writer, YMLPort *port) {
            ASSERT_OR_THROW("NULL parameter", writer && port);
            this->writer = writer;
            writerPort = port;
        }
        YMLPort *getReaderPort() {
            return readerPort;
        }
        YMLPort *getWriterPort() {
            return writerPort;
        }
};

std::ostream &operator<<(std::ostream &stream, PNConnection &c);
#endif

