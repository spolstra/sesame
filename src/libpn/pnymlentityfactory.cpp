/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pnymlentityfactory.h"

#include "../libyml/ymlport.h"
#include "../libyml/ymllink.h"

#include "../BasicUtils//BasicException.h"

#include "pn.h"
#include "pnconnection.h"
#include "cppprocessloader.h"
#include "pnxmltags.h"

#include <string>

using namespace std;

YMLConnection *PNYMLEntityFactory::createConnection(YMLPort *inPort,
        YMLPort *outPort)
{
    ASSERT_OR_THROW("Null port!", inPort && outPort);

    // Check port types
    const string inType = inPort->findProperty(PROP_NAME_TYPE);
    const string outType = outPort->findProperty(PROP_NAME_TYPE);

    if (!inType.empty() && !outType.empty() && inType.compare(outType) != 0)
        THROW(string("Port ") + inPort->getFullName() + " has type '" + inType +
              "' and port " + outPort->getFullName() + " has type '" + outType +
              "' so they cannot be connected.");

#ifdef PNYMLENTITYFACTORY_DEBUG
    cout << "Connecting ";
    inPort->dumpFullName(cout);
    cout << '<' << inType << "> ";
    cout << "to ";
    outPort->dumpFullName(cout);
    cout << '<' << outType << "> ";
    cout << endl;
#endif

    PNConnection *pnconnection = new PNConnection();

    ProcessLoader *inProc = (ProcessLoader *)inPort->getParent();
    ProcessLoader *outProc = (ProcessLoader *)outPort->getParent();
    pnconnection->setWriter(inProc, inPort);
    pnconnection->setReader(outProc, outPort);

    return pnconnection;
}

YMLNetwork *PNYMLEntityFactory::createNetwork(const std::string,
                                              const std::string)
{
    return new PN();
}

YMLNode *PNYMLEntityFactory::createNode(const std::string,
                                        const std::string className)
{
    if (className == C_PROCESS) {
        THROW(string("Unsupported process class '") + className + "'");
    } else if (className == CPP_PROCESS) {
        return new CPPProcessLoader();
    } else if (className == JAVA_PROCESS) {
        THROW(string("Unsupported process class '") + className + "'");
    } else {
        THROW(string("Unknown process class '") + className + "'");
    }
}

YMLLink *PNYMLEntityFactory::createLink(const std::string,
                                        const std::string,
                                        const std::string,
                                        const std::string)
{
    return new YMLLink();
}

YMLPort *PNYMLEntityFactory::createPort(const std::string)
{
    return new YMLPort();
}
