/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pntypeinfogen.h"
#include "pnxmltags.h"

#include "../libbasic/basicutilfuncts.h"
#include "../libyml/ymlnode.h"
#include "../libyml/ymlnetwork.h"
#include "../libyml/ymlport.h"

#include <string.h>
#include <sstream>
#include <fstream>
#include <string>

#include "../BasicUtils//BasicException.h"

using namespace std;

void dumpStandardHeader(ostream &stream)
{
    stream << "// *****************************************" << endl;
    stream << "// * WARNING: This is auto generated code. *" << endl;
    stream << "// *   Direct editing is not recommended.  *" << endl;
    stream << "// *****************************************" << endl;
    stream << endl;
}

void dumpCPPTypeImplementation(YMLNetwork *net, ostream &stream,
                               const string userInclude)
{
    dumpStandardHeader(stream);

    // Include definition
    stream << "#include <cppprocessloader.h>" << endl;
    stream << "#include <ioconnections.h>" << endl;

    if (!userInclude.empty()) {
        stream << "#include \"" << userInclude << "\"" << endl;
    }

    dumpCPPTypeImplementationNetwork(net, stream);
}

void dumpCPPTypeImplementationNetwork(YMLNetwork *net, ostream &stream)
{
    for (unsigned int i = 0; i < net->getNumNodes(); i++) {
        YMLNode *node = net->getNode(i);

        if (node->getType() == ymlNetwork) {
            dumpCPPTypeImplementationNetwork((YMLNetwork *)node, stream);
        } else {
            dumpCPPTypeImplementationNode(node, stream);
        }
    }
}

void dumpCPPTypeImplementationNode(YMLNode *node, ostream &stream)
{
    const string nodeClass = node->findProperty(PROP_NAME_CLASS);

    if (nodeClass.empty()) {
        return;
    }

    ostringstream fullName;
    node->dumpFullName(fullName);
    fullName << ends;

    // Dump loader head
    stream << "extern \"C\" Process *";
    dumpCleanCString(fullName.str().c_str(), stream);
    stream << CPP_LOADER_SUFFIX << '(' << endl;
    stream << "\tCPPProcessLoader *loader, int argc, char *argv[]) {" << endl;
    stream << endl;

    // Dump process construction call
    stream << "\tProcess *process = new ";
    dumpCleanCString(nodeClass, stream);
    stream << "(";

    // Id parameter
    stream << "*(new Id(\"";
    dumpCleanCString(fullName.str().c_str(), stream);
    stream << "\", NULL))";

    for (unsigned int i = 0; i < node->getNumPorts(); i++) {
        stream << ',' << endl << "\t\t";
        dumpCPPTypeImplementationPort(node->getPortByIdx(i), stream);
    }

    // Dump constructor args
    for (unsigned int i = 0; i < node->getNumProperties(); i++) {
        if (node->getPropName(i).compare(PROP_NAME_CARG) == 0) {
            stream << ',' << endl << "\t\t" << node->getPropValue(i);
        }
    }

    stream << ");" << endl;
    stream << "\tprocess->setArgs(argc, argv);" << endl;
    stream << "\treturn process;" << endl;
    stream << "}" << endl << endl;
}

void dumpCPPTypeImplementationPort(YMLPort *port, ostream &stream)
{
    stream << "*(new ";

    // Dump direction
    switch (port->getDir()) {
        case YMLPort::dIn:
            stream << "Reader";
            break;

        case YMLPort::dOut:
            stream << "Writer";
            break;

        default:
            THROW(string("Unrecognized direction for port '") + port->getFullName() +
                  "'!");
    }

    // Dump type
    const string type = port->findProperty(PROP_NAME_TYPE);
    ASSERT_OR_THROW("Could not find 'type' property!", !type.empty());
    stream << "<" << type << " >";

    stream << "(loader, ";
    stream << '\"' << port->getName() << '\"';
    stream << "))";
}
