/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef DATABLOCKCLASS_H
#define DATABLOCKCLASS_H

#include "datablock.h"

/// A C++ wrapper for struct DataBlock.
/**
 * The base unit data used in communication among Processes.
 * This class provides several constructions and destruction for
 * struct DataBlock.  The underlying struct exists to ease communciation
 * of data to other languages.
 * @see DataBlock
 */
class DataBlockClass
{
        // Attributes
    private:
        struct DataBlock *dataBlock;
        // Operations
    public:
        DataBlockClass();
        DataBlockClass(struct DataBlock *dataBlock);
        DataBlockClass(char *data, int size);
        ~DataBlockClass();

        struct DataBlock *detatch();
        char *getData();
        int getSize();
        void setData(char *data, int size);

    private:
        void freeData();
};

#endif

