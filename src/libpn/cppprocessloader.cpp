/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "cppprocessloader.h"
#include "pnxmltags.h"
#include "pntypeinfogen.h"
#include "../libymlmap/tracechannel.h"

#include "../BasicUtils//BasicException.h"
#include "../BasicUtils//Zap.h"

#include "../libbasic/basicutilfuncts.h"

#include <sstream>
#include <stdlib.h>
#include <string>

using namespace std;


//#define CPPPROCESSLOADER_DEBUG

CPPProcessLoader::CPPProcessLoader() : process(0) {}

CPPProcessLoader::~CPPProcessLoader()
{
    //Darwin suffers from a double delete, until fix ignore removal
    //zap(process);
}

bool CPPProcessLoader::load()
{
    ProcessLoader::load();

    const string className = findProperty(PROP_NAME_CLASS);
    ASSERT_OR_THROW(string("Class property not found for CPP_Process '") +
                    getName() + "'.", !className.empty());

    ostringstream loaderFunctName;
    ostringstream fullName;
    dumpFullName(fullName);
    fullName << ends;

    dumpCleanCString(fullName.str().c_str(), loaderFunctName);
    loaderFunctName << CPP_LOADER_SUFFIX << ends;

    // Get users process method
    Process *(*loaderFunct)(void *, int, char **);
    loaderFunct = (Process * ( *)(void *, int, char **))
                  getLibManager()->loadSymbol(loaderFunctName.str().c_str());

    // Setup args
    loadProcessArgs();

    process = (*loaderFunct)(this, getProcessArgc(), getProcessArgs());

    if (!process) {
        return false;
    }

    return true;
}

void CPPProcessLoader::run()
{
    ASSERT_OR_THROW("NULL process!", process);

    try {
#ifdef CPPPROCESSLOADER_DEBUG

        if (getName()) {
            cout << getName() << " started." << endl;
        }

#endif

        if (setjmp(jmpEnv) == 0) {
            process->main(this);
        }

#ifdef CPPPROCESSLOADER_DEBUG

        if (getName()) {
            cout << getName() << " stopped." << endl;
        }

#endif

        if (getTraceChannel()) {
            getTraceChannel()->sendQuit();
        }
    } catch (BasicException &e) {
        cerr << "Exception in call to user process '" << getName() << "': "
             << e << endl;
    } catch (...) {
        cerr << "Caught unknown exception in call to user process '"
             << getName() << "'!" << endl;
    }
}

void CPPProcessLoader::exit()
{
    ProcessLoader::exit();

    longjmp(jmpEnv, 1);
}
