/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PNTYPEINFOGEN_INCLUDE_ONCE
#define PNTYPEINFOGEN_INCLUDE_ONCE

#include <iostream>
#include <string>

/**
 * @file   pntypeinfogen.h
 * @author Joe Coffland
 * @date   Thu Sep 27 20:11:37 2001
 *
 * @brief  Functions for writing PN type info to a stream from a YML description
 */

#define CPP_LOADER_SUFFIX "_loader"
#define CPP_USERDEF_SUFFIX "_userdefs.h"
#define CPP_IMPL_SUFFIX "_stub.cpp"

class YMLNetwork;
class YMLNode;
class YMLPort;

/* Prototypes */
void dumpStandardHeader(std::ostream &stream);

void dumpCPPTypeImplementation(YMLNetwork *net, std::ostream &stream,
                               const std::string userInclude = 0);
void dumpCPPTypeImplementationNetwork(YMLNetwork *net, std::ostream &stream);
void dumpCPPTypeImplementationNode(YMLNode *node, std::ostream &stream);
void dumpCPPTypeImplementationPort(YMLPort *port, std::ostream &stream);

#endif // PNTYPEINFOGEN_INCLUDE_ONCE

