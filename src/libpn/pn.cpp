/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pn.h"
#include <string.h>

#include "pnxmltags.h"

#include "../BasicUtils//BasicException.h"

#include <string>

using namespace std;

PN::PN() {}

void PN::start()
{
    prestart();
    realstart();
}

void PN::init()
{
    PN *parent = (PN *)getNetwork();

    if (parent) {
        libManager.setParent(parent->getLibManager());
    }
}

void PN::prestart()
{
    loadLibraries();

    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            ((PN *)entity)->prestart();

        } else if (entity->getType() == ymlNode) {
            ProcessLoader *proc = (ProcessLoader *)entity;

            if (!proc->load()) {
                THROW(string("Failed to load '") + proc->getName() + "'.");
            }

            proc->setRunning(true);

        } else {
            THROW("Unexpected yml node type!");
        }
    }
}

void PN::realstart()
{
    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            ((PN *)entity)->realstart();
        } else if (entity->getType() == ymlNode) {
            ((ProcessLoader *)entity)->start();

        } else {
            THROW("Unexpected yml node type!");
        }
    }
}

/**
 * This function requests each Process stop.
 */
void PN::stop()
{
    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            ((PN *)entity)->stop();
        } else if (entity->getType() == ymlNode) {
            ((ProcessLoader *)entity)->stop();
        } else {
            THROW("Unexpected yml node type!");
        }
    }
}

void PN::cancel()
{
    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            ((PN *)entity)->cancel();
        } else if (entity->getType() == ymlNode) {
            ((ProcessLoader *)entity)->cancel();
        } else {
            THROW("Unexpected yml node type!");
        }
    }
}

/**
 * Blocks the current process on each of the Processes until
 * their threads exit.  This function may never return.
 */
void PN::join()
{
    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            ((PN *)entity)->join();
        } else if (entity->getType() == ymlNode) {
            ((ProcessLoader *)entity)->join();
        } else {
            THROW("Unexpected yml node type!");
        }
    }
}

/**
 * Check if the PN is running.
 *
 * @return True if at least one thread is running
 */
bool PN::isRunning()
{
    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            if (((PN *)entity)->isRunning()) {
                return true;
            }
        } else if (entity->getType() == ymlNode) {
            if (((ProcessLoader *)entity)->isRunning()) {
#ifdef PN_DEBUG
                ((ProcessLoader *)entity)->dumpFullName(cout);
                cout << " is running." << endl;
#endif
                return true;
            }
        } else {
            THROW("Unexpected yml node type!");
        }
    }

    return false;
}

void PN::loadLibraries()
{
    for (unsigned int i = 0; i < getNumProperties(); i++) {
        if (getPropName(i).compare(PROP_NAME_LIB) == 0) {
            libManager.loadLibrary(getPropValue(i));
        }
    }
}

void PN::printState(std::ostream &stream, unsigned int indent)
{
    for (unsigned int j = 0; j < indent; j++) {
        stream << " ";
    }

    stream << "<network name=\"" << getName() << "\">" << endl;
    indent += 2;

    for (unsigned int i = 0; i < getNumNodes(); i++) {
        YMLEntity *entity = getNode(i);

        if (entity->getType() == ymlNetwork) {
            ((PN *)entity)->printState(stream, indent);

        } else if (entity->getType() == ymlNode) {
            ProcessLoader *proc = (ProcessLoader *)entity;

            for (unsigned int j = 0; j < indent; j++) {
                stream << " ";
            }

            stream << "<node name=\"" << proc->getName() << "\">"
                   << proc->getStateString()
                   << "</node>" << endl;

        } else {
            THROW("Unexpected yml node type!");
        }
    }

    indent -= 2;

    for (unsigned int j = 0; j < indent; j++) {
        stream << " ";
    }

    stream << "</network>" << endl;
}

/**
 * Pretty print a PN to an ostream
 *
 * @param stream The output stream. (e.g. cout)
 * @param pn The Network to be printed
 *
 * @return The stream
 */
ostream &operator<<(ostream &stream, PN &pn)
{
    // Network
    stream << "Network: ";
    pn.dumpFullName(stream);
    stream << endl;

    // Processes
    stream << "Processes:";

    for (unsigned int i = 0; i < pn.getNumNodes(); i++) {
        YMLNode *node = pn.getNode(i);
        stream << " '" << node->getName() << '\'';
    }

    stream << endl;

    // Connections
    stream << "Connections:";

    for (unsigned int i = 0; i < pn.getNumConnections(); i++) {
        PNConnection *con = (PNConnection *)pn.getConnection(i);

        stream << " '" << *con << '\'';
    }

    stream << endl;

    // Subnetworks
    for (unsigned int i = 0; i < pn.getNumNodes(); i++) {
        YMLNode *node = pn.getNode(i);

        if (node->getType() == ymlNetwork) {
            stream << *((PN *)node);
        }
    }

    return stream;
}
