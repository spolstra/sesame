/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "datablockclass.h"

#include "../BasicUtils//BasicException.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/// Default constructor
DataBlockClass::DataBlockClass()
{
    dataBlock = NULL;
}

/**
 * Construct a DataBlockClass from existing data.
 * Internally new memory is allocated and the data is copyed.
 *
 * @param data A pointer to the data
 * @param size The size of the data
 */
DataBlockClass::DataBlockClass(char *data, int size)
{
    dataBlock = NULL;
    setData(data, size);
}

/**
 * Construct from an existing dataBlock.  No new memory is allocated.
 * Destruction of the struct DataBlock will now be handled by the newly
 * constructed DataBlockClass
 *
 * @param dataBlock
 */
DataBlockClass::DataBlockClass(struct DataBlock *dataBlock)
{
    this->dataBlock = dataBlock;
}

DataBlockClass::~DataBlockClass()
{
    freeData();
}

/**
 * Release the internal struct DataBlock.  This ensures that destruction
 * of the DataBlockClass will not destruct the struct DataBlock.
 *
 * @return The struct DataBlock
 */
struct DataBlock *DataBlockClass::detatch() {
    struct DataBlock *dataBlock = this->dataBlock;
    this->dataBlock = NULL;
    return dataBlock;
}

/**
 * Deallocate both the data in the struct DataBlock and the struct DataBlock
 * itself.
 */
void DataBlockClass::freeData()
{
    if (dataBlock) {
        if (dataBlock->data) {
            free(dataBlock->data);
        }

        free(dataBlock);
        dataBlock = NULL;
    }
}

/**
 * Frees any current data and copies the new data.
 *
 * @param data The data to be copied.
 * @param size The size of the data to be copied
 */
void DataBlockClass::setData(char *data, int size)
{
    freeData();
    dataBlock = (struct DataBlock *)malloc(sizeof(struct DataBlock));
    ASSERT_OR_THROW("Error allocating DataBlock", dataBlock);

    dataBlock->size = size;
    dataBlock->data = (char *)malloc(size);
    ASSERT_OR_THROW("Error allocating data in DataBlock", dataBlock->data);

    memcpy(dataBlock->data, data, size);
}


char *DataBlockClass::getData()
{
    if (!dataBlock) {
        return NULL;
    }

    return dataBlock->data;
}

int DataBlockClass::getSize()
{
    if (!dataBlock) {
        return 0;
    }

    return dataBlock->size;
}

