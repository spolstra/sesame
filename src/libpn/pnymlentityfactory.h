/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PNYMLENTITYFACTORY_H
#define PNYMLENTITYFACTORY_H

#include "../libyml/ymlentityfactory.h"

class PNYMLEntityFactory : public YMLEntityFactory
{
    public:
        virtual YMLConnection *createConnection(YMLPort *inPort, YMLPort *outPort);
        virtual YMLNetwork *createNetwork(const std::string name, const std::string className);
        virtual YMLNode *createNode(const std::string name, const std::string className);
        virtual YMLLink *createLink(const std::string object1, const std::string port1,
                                    const std::string object2, const std::string port2);
        virtual YMLPort *createPort(const std::string name);
};

#endif // PNYMLENTITYFACTORY_H
