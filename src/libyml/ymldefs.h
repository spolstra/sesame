/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymldefs.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:07:36 2003
 *
 * @brief  Definitions of YML enity strings.
 *
 * The file defines the element and attributes names used in YML.
 */

#ifndef YMLDEFS_H
#define YMLDEFS_H

#define YML_ELEM_NETWORK  "network"
#define YML_ELEM_NODE     "node"
#define YML_ELEM_LINK     "link"
#define YML_ELEM_PORT     "port"
#define YML_ELEM_PROPERTY "property"
#define YML_ELEM_DOC      "doc"
#define YML_ELEM_SET      "set"
#define YML_ELEM_SCRIPT   "script"

#define YML_ATTR_NAME     "name"
#define YML_ATTR_CLASS    "class"
#define YML_ATTR_OBJECT1  "innode"
#define YML_ATTR_PORT1    "inport"
#define YML_ATTR_OBJECT2  "outnode"
#define YML_ATTR_PORT2    "outport"
#define YML_ATTR_DIR      "dir"
#define YML_ATTR_DIR_IN   "in"
#define YML_ATTR_DIR_OUT  "out"
#define YML_ATTR_DATADIR  "datadir"
#define YML_ATTR_VALUE    "value"
#define YML_ATTR_INIT     "init"
#define YML_ATTR_COND     "cond"
#define YML_ATTR_LOOP     "loop"

#endif // YMLDEFS_H

