/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   nullinterpreter.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:20:29 2003
 *
 * @brief  NullIntrepreter class definition.
 *
 * The NullIntrepreter is an instance of Interpreter which provides
 * only default behavior for the Intrepreter interface.
 */

#ifndef NULLINTERPRETER_H
#define NULLINTERPRETER_H

#include "interpreter.h"

// Interpreter
class NullInterpreter: public Interpreter
{
    public:
        // Operations
        NullInterpreter() {}
        virtual ~NullInterpreter() {}

        virtual void start() {}
        virtual void stop() {}
        virtual void *define(const std::string var, const std::string value) {
            return (void *)1;
        }
        virtual bool defined(const std::string var) {
            return true;
        }
        virtual void *eval(const std::string script) {
            return (void *)1;
        }
        virtual int toint(void *value) {
            return 0;
        }
        virtual const std::string tostring(void *value) {
            return "";
        }
        virtual double todouble(void *value) {
            return 0;
        }
        virtual bool tobool(void *value) {
            return false;
        }
        virtual void upscope() {}
        virtual void downscope() {}
};
#endif
