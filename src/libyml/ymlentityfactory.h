/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlentityfactory.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:59:04 2003
 *
 * @brief  YMLEntityFactory class definition.
 *
 * The YMLEntityFactory defines the interface used for creating new
 * YML entities.  This class can be overloaded to provide user specific
 * implementation of the YML entity classes.
 * It is often usefull to overload DefaultYMLEntityFactory rather than
 * this class directly as it provides default implemntations for all
 * the functions.
 */

#ifndef YMLENTITYFACTORY_H
#define YMLENTITYFACTORY_H

#include <string>

class YMLNetwork;
class YMLNode;
class YMLLink;
class YMLConnection;
class YMLPort;

class YMLEntityFactory
{
    public:

        virtual ~YMLEntityFactory() {}

        /**
         * This function is called to create a resolved YMLConnection.
         * YMLConnections go directly from node to node with out intermeadiate ports.
         * YMLConnections are created is a second pass after all nodes, netowrks, and
         * links are been added.
         *
         * @param inPort The incomming port.
         * @param outPort The outgoing port.
         *
         * @return A pointer to a newly constructed YMLConnection.
         */
        virtual YMLConnection *createConnection(YMLPort *inPort,
                                                YMLPort *outPort) = 0;
        /**
         * Create a new YMLNetwork entity.
         *
         * @param name The name of the network.
         * @param className The network class.  This can be used to decide which
         *                  type of YMLNetwork to create.
         *
         * @return A pointer to the YMLNetwork.
         */
        virtual YMLNetwork *createNetwork(const std::string name,
                                          const std::string className) = 0;
        /**
         * Create a new YMLNode enity.
         *
         * @param name The name of the node.
         * @param className The node class.  This can be used to decide which type
         *                  of YMLNode to create.
         *
         * @return
         */
        virtual YMLNode *createNode(const std::string name, const std::string className) = 0;
        /**
         * Create a new YMLLink entity.  It is not necessary to actually connect
         * the objects in implementations of this function.
         *
         * @param object1 The name of the first node or network.
         * @param port1 The name of the first port.
         * @param object2 The name of the second node or network.
         * @param port2 The name of the second port.
         *
         * @return A pointer to the newly created YMLNode.
         */
        virtual YMLLink *createLink(const std::string object1, const std::string port1,
                                    const std::string object2, const std::string port2) = 0;
        /**
         * Create a new YMLPort entity.
         *
         * @param name The port name.
         *
         * @return A pointer to the newly created YMLPort.
         */
        virtual YMLPort *createPort(const std::string name) = 0;
};

#endif
