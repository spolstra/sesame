/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlentity.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:47:51 2003
 *
 * @brief  YMLEntity class definition
 *
 * YMLEntity is the base type for all YML data structures in libyml.
 */

#ifndef YMLENTITY_H
#define YMLENTITY_H

#include <string.h>
#include <map>
#include <iostream>
#include <vector>
#include <string>

typedef enum {ymlUnknown = -1, ymlNetwork, ymlNode,
              ymlLink, ymlPort, ymlConnection
             } ymlentity_t;

class XMLSerializer;

class YMLEntity
{
        // Attributes
    private:
        /// A list entity argument strings.
        std::vector<std::string> args;

        /// An array of YML properties.
        std::vector< std::vector<std::string> > properties;

        /// A name value map of YML properties.
        std::map<std::string, std::string> property_map;

        /// A pointer to this elements documentation.
        std::string doc;

        // Operations
    public:
        YMLEntity() {};
        virtual ~YMLEntity() {};

        void addProperty(const std::string name, const std::string value);
        /**
         * @return The number of properties for this element.
         */
        unsigned int getNumProperties() {
            return properties.size();
        }
        /**
         * @param i The property number.
         *
         * @return The name of property i.
         */
        const std::string getPropName(const unsigned int i) {
            return properties.at(i)[0];
        }
        /**
         * @param i The property number.
         *
         * @return The value of property i.
         */
        const std::string getPropValue(const unsigned int i) {
            return properties.at(i)[1];
        }
        /**
         * Get the property value by name.
         *
         * @param name The property name.
         *
         * @return The property value on success, NULL otherwise.
         */
        const std::string findProperty(const std::string name) {
            return property_map[name];
        }

        virtual void setArgs(const std::vector<std::string> &args);
        /**
         * @return The number of XML arguments for this entity.
         */
        unsigned int getArgc() {
            return args.size();
        }

        const std::string getArg(unsigned int i);

        /**
         * Set the documentation for this entity.
         * The documentation is copied in to an internal buffer.
         *
         * @param doc A pointer to this entities documentation.
         */
        void setDoc(const std::string doc) {
            this->doc = doc;
        }
        /**
         * @return A pointer to this entites documentation.
         */
        const std::string getDoc() {
            return doc.c_str();
        }

        /**
         * @return This enties type.
         */
        virtual ymlentity_t getType() = 0;
        /**
         * Dump the XML for this entity to the given stream.
         *
         * @param stream The output stream.
         */
        virtual void dumpXML(XMLSerializer &stream) = 0;
        /**
         * Dump the GraphViz dot file representation of this entity.
         * Each line of printing is prefixed by tabLevel number of tabs.
         * The current line is assume to already be tabbed.
         *
         * @param stream The output stream.
         * @param tabLevel The current tab level.
         */
        virtual void dumpDOT(std::ostream &, int) {};
        /**
         * This function is overriden to allow classes to execute code to finalize
         * their data structure.
         */
        virtual void finalize() {}
        /**
         * This function is called by the loader after parsing is finished.
         * It should call finalized for itself and all its children.
         */
        virtual void doFinalize() = 0;

        void dumpXMLProperties(XMLSerializer &stream);
        static void tabs(std::ostream &stream, const unsigned int num);
};
#endif
