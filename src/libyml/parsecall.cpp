/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "parsecall.h"

#include <string.h>
#include <string>
#include <vector>

using namespace std;

/**
 * Construct a ParseCall
 *
 * @param type The parse call type.
 * @param args The argument array.
 */
ParseCall::ParseCall(call_t type, const vector<string> args) : type(type)
{
    for (unsigned int i = 0; i < args.size(); i++) {
        this->args.push_back(args[i]);
    }
}


