/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef YMLSAXFILTER_H
#define YMLSAXFILTER_H

#include <xercesc/parsers/SAX2XMLFilterImpl.hpp>

class YMLSAXFilter : public XERCES_CPP_NAMESPACE::SAX2XMLFilterImpl
{
        int property_count;

    public:
        YMLSAXFilter(SAX2XMLReader *parent);

        void startElement(const   XMLCh *const    uri,
                          const   XMLCh *const    localname,
                          const   XMLCh *const    qname,
                          const   XERCES_CPP_NAMESPACE::Attributes &attributes);

        void endElement(const   XMLCh *const    uri,
                        const   XMLCh *const    localname,
                        const   XMLCh *const    qname);
};

#endif

