/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlentity.h"
#include "ymldefs.h"

#include "../BasicUtils/BasicException.h"
#include "../XMLCereal/XMLSerializer.h"

#include <string.h>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

/**
 * Add a YML property to this entity.
 *
 * @param name The property name.
 * @param value The property value.
 */
void YMLEntity::addProperty(const string name, const string value)
{
    vector<string> property;
    property.push_back(name);
    property.push_back(value);
    properties.push_back(property);

    property_map[name] = value;
}

/**
 * Set this enitites arguments.
 *
 * @param args An array of arguments.
 */
void YMLEntity::setArgs(const vector<string> &args)
{
    for (unsigned int i = 0; i < args.size(); i++) {
        this->args.push_back(args[i]);
    }
}

/**
 * Get an XML argument.
 *
 * @param i The number of the argument.
 *
 * @return A pointer to the XML argument.
 */
const std::string YMLEntity::getArg(unsigned int i)
{
    ASSERT_OR_THROW("Out of range!", i < args.size());
    return args[i];
}

/**
 * Print num number of tabs.
 *
 * @param stream The output stream.
 * @param num The number of tabs.
 */
void YMLEntity::tabs(std::ostream &stream, const unsigned int num)
{
    for (unsigned int i = 0; i < num; i++) {
        stream << '\t';
    }
}

/**
 * Print this Entities properties as XML.
 *
 * @param stream The output stream.
 */
void YMLEntity::dumpXMLProperties(XMLSerializer &stream)
{
    for (unsigned int i = 0; i < getNumProperties(); i++) {
        XMLSerializer::attributes_t attrs;
        attrs["name"] = getPropName(i);
        attrs["value"] = getPropValue(i);

        stream.startElement(YML_ELEM_PROPERTY, attrs);
        stream.endElement();
    }
}
