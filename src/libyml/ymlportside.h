/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlportside.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 17:54:29 2003
 *
 * @brief  Definition of YMLPortSide class.
 *
 * This class represents a specific side of a port.  Either the inside
 * or the outside.  The side determines the port perspective.  This
 * is used to differentiate conections.
 */

#ifndef YMLPORTSIDE_H
#define YMLPORTSIDE_H

#include <vector>

class YMLPort;

class YMLPortSide
{
    public:
        typedef enum {sIn, sOut} side_t;
    private:

        /// The connected ports.
        std::vector<YMLPortSide *> links;

        /// A pointer to the other port side.
        YMLPortSide *otherSide;

        /// A pointer to the port.
        YMLPort *port;

        /// Our side type.
        side_t type;

        int commChannelID;

    public:
        YMLPortSide(YMLPort *port, YMLPortSide *otherSide, side_t type);

        /**
         * Get the other side.
         *
         * @return A pointer to the other side.
         */
        YMLPortSide *getOtherSide() {
            return otherSide;
        }

        /**
         * Get the associated port.
         *
         * @return A pointer to the port class.
         */
        YMLPort *getPort() {
            return port;
        }

        /**
         * Get the side type.
         *
         * @return Either YMLPortSide::sIn or YMLPortSide::sOut.
         */
        side_t getType() {
            return type;
        }
        int getDir();

        /**
         * @return The number of links connected to this port.
         */
        unsigned int getNumLinks() {
            return links.size();
        }

        /**
         * Add a link to this port side.
         *
         * @param side The remote port side to be linked.
         *
         * @return The link id.
         */
        int addLink(YMLPortSide *side) {
            links.push_back(side);
            return links.size() - 1;
        }

        /**
         * Get a link by id.
         *
         * @param i The link id.
         *
         * @return A pointer to the link.
         */
        YMLPortSide*&getLink(const unsigned int i) {
            return links[i];
        }

        /**
         * a convience function for getLink();
         *
         * @param i The link id.
         *
         * @return A pointer to the link.
         */
        YMLPortSide*&operator[](const unsigned int i) {
            return getLink(i);
        }

        void setCommChannelID(const int id) {
            commChannelID = id;
        }
        int getCommChannelID() {
            return commChannelID;
        }
};
#endif // YMLPORTSIDE_H

