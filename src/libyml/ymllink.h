/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymllink.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:44:14 2003
 *
 * @brief  YMLLink class definition.
 *
 * YMLLinks encapsulate the YML link entity.  YMLLinks differ from
 * YMLConnections in that one the are directly related the a YML element
 * and two their ends are always both within the same network.
 */

#ifndef YMLLINK_H
#define YMLLINK_H

#include "ymlentity.h"

class YMLPortSide;
class YMLNetwork;

// YMLLink
class YMLLink: public YMLEntity
{
        /// A array for the two YMLPortSides.
        YMLPortSide *portSides[2];

        /// The parent network
        YMLNetwork *network;

        int commChannelID;

    public:
        YMLLink();

        void setPortSide(const unsigned int i, YMLPortSide *portSide);
        YMLPortSide *getPortSide(const unsigned int i);

        const std::string getNodeName(const unsigned int i);
        const std::string getPortName(const unsigned int i);

        /**
         * Get the parent network.
         *
         * @return A pointer to the parent YMLNetwork.
         */
        YMLNetwork *getNetwork() {
            return network;
        }

        /**
         * Set the parent network.
         *
         * @param network A pointer to the parent YMLNetwork.
         */
        void setNetwork(YMLNetwork *network) {
            this->network = network;
        }

        void setCommChannelID(const int id) {
            commChannelID = id;
        }
        int getCommChannelID() {
            return commChannelID;
        }

        virtual void dumpXML(XMLSerializer &stream);
        virtual void dumpDOT(std::ostream &stream, int tabLevel);
        /**
         * Simply calls finalize.
         */
        virtual void doFinalize() {
            finalize();
        }
        /**
         * Get the YMLEntity type.
         *
         * @return ymlLink.
         */
        virtual ymlentity_t getType() {
            return ymlLink;
        }
};

#endif // YMLLINK_H

