/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlport.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:09:01 2003
 *
 * @brief  YMLPort class.
 *
 * Encapsulates a YML port.
 */

#ifndef YMLPORT_H
#define YMLPORT_H

#include "ymlentity.h"
#include "ymlportside.h"

#include <string>
#include <vector>

class YMLNode;
class YMLEntityFactory;
class YMLPortSide;
class YMLConnection;

class YMLPort: public YMLEntity
{
    public:
        /// Location of the name attribute.
        static const int aName = 0;
        /// Location of the dir attribute.
        static const int aDir = 1;
        typedef enum {dUnknown = -1, dIn, dOut} dir_t;
    private:

        /// The parent node.
        YMLNode *parent;

        /// The port direction
        dir_t dir;

        /// The port inside.
        YMLPortSide inSide;

        /// The port outside.
        YMLPortSide outSide;

        std::vector<YMLConnection *> connections;

        int id;

    public:
        YMLPort();

        /**
         * Get the port name property.
         *
         * @return The port name.
         */
        const std::string getName() {
            return getArg(aName);
        }
        void dumpFullName(std::ostream &stream);
        std::string getFullName();

        /**
         * Get the port direction property as a dir_t.
         *
         * @return The port direction.
         */
        dir_t getDir() {
            if (dir == dUnknown) {
                resolveDir();
            }

            return dir;
        }

        /**
         * Get the parent node.
         *
         * @return A pointer to the parent node.
         */
        YMLNode *getParent() {
            return parent;
        }

        /**
         * Set the parent node.
         *
         * @param parent A pointer to the parent node.
         */
        void setParent(YMLNode *parent) {
            this->parent = parent;
        }

        /**
         * Get the inside YMLPortSide.
         *
         * @return A pointer to the inside YMLPortSide.
         */
        YMLPortSide *getInSide() {
            return &inSide;
        }

        /**
         * Get the outside YMLPortSide.
         *
         * @return A pointer to the outside YMLPortSide.
         */
        YMLPortSide *getOutSide() {
            return &outSide;
        }


        YMLPort *getRemotePort();
        void getRemotePorts(std::vector<YMLPort *> *ports);

        void addConnection(YMLConnection *con) {
            connections.push_back(con);
        }
        YMLConnection *getConnection();
        const std::vector<YMLConnection *> &getConnections() {
            return connections;
        }

        void setID(int id);
        int getID();

        virtual void dumpXML(XMLSerializer &stream);

        /**
         * Calls the finalize function.
         */
        virtual void doFinalize() {
            finalize();
        }

        /**
         * Get the YMLEntity type.
         *
         * @return ymlPort
         */
        ymlentity_t getType() {
            return ymlPort;
        }

    protected:
        YMLPort *getRemotePort(YMLConnection *connection);
        void resolveDir();
};
#endif
