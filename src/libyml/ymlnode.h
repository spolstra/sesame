/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlnode.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:14:39 2003
 *
 * @brief  YMLNode class definition.
 *
 * The class encapsulates a YML node enity.
 */

#ifndef YMLNODE_H
#define YMLNODE_H

#include "ymlentity.h"
#include "ymlport.h"

#include <map>
#include <string>
#include <vector>

class YMLNetwork;
class YMLConnection;
class YMLEntityFactory;
class TraceChannel;

#define NAME_DELIM '.'

/**
 * A YMLNode
 */
class YMLNode: public YMLEntity
{
    public:
        /// Location of the name property.
        static const int aName = 0;
        /// Location of the class property.
        static const int aClass = 1;

    private:
        /// Typedef to simplify portmap usage.
        typedef std::map<std::string, YMLPort *> portMap_t;

        /// Map from name to YMLPort pointer for all ports on this node.
        portMap_t portMap;

        /// Array of unmapped ports on this node.
        std::vector<YMLPort *> ports;

        /// Array of mapped ports on this node.
        std::vector<YMLPort *> mappedPorts;

        /// The parent network
        YMLNetwork *network;

        /// A pointer to this nodes tracechannel
        TraceChannel *traceChannel;

        /// The channel id.
        int channelID;


    public:
        YMLNode();
        virtual ~YMLNode();

        void addPort(YMLPort *port);

        /**
         * Find a port by name.
         *
         * @param name The port name.
         *
         * @return A pointer to the YMLPort on success, NULL otherwise.
         */
        YMLPort *findPort(const std::string name);

        /**
         * Get the number of ports for this node.
         *
         * @return number of ports.
         */
        unsigned int getNumPorts() {
            return ports.size();
        }

        /**
         * Get a port by id.
         *
         * @param id the port id.
         *
         * @return A pointer to the YMLPort on success, NULL otherwise.
         */
        YMLPort *getPort(const unsigned int id);

        /**
         * Get a port by index
         *
         * @param idx index of the port.
         *
         * @return A pointer to the YMLPort on success.
         */
        YMLPort *getPortByIdx(const unsigned int idx) {
            return ports[idx];
        }

        /**
         * Get the parent network.
         *
         * @return A pointer to the parent YMLNetwork.
         */
        YMLNetwork *getNetwork() const {
            return network;
        }

        /**
         * Set the parent network.
         *
         * @param network A pointer to the parent YMLNetwork.
         */
        void setNetwork(YMLNetwork *network) {
            this->network = network;
        }
        void dumpFullName(std::ostream &stream);
        std::string getFullName();

        /**
         * @return The node name attribute.
         */
        const std::string getName() {
            return getArg(aName);
        }

        /**
         * @return The node class attribute.
         */
        const std::string getClass() {
            return getArg(aClass);
        }

        /**
         * Get the YMLEntity type.
         * @return ymlNode.
         */
        virtual ymlentity_t getType() {
            return ymlNode;
        }

        void setTraceChannel(TraceChannel *traceChannel);
        TraceChannel *getTraceChannel();

        /**
         * Set the channel id of this node.
         *
         * @param channelID the new channelID of this node.
         */
        void setChannelID(int channelID) {
            this->channelID = channelID;
        }

        int getChannelID() const;

        void dumpXMLPorts(XMLSerializer &stream);
        virtual void dumpXML(XMLSerializer &stream);
        virtual void dumpDOT(std::ostream &stream, int tabLevel);
        virtual void doFinalizePorts();
        virtual void doFinalize();
        /**
         * Do nothing.
         */
        virtual void init() {}
};

#endif
