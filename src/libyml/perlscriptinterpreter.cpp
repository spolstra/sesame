/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "perlscriptinterpreter.h"

#include "../BasicUtils//BasicException.h"

#include <iostream>
#include <string>
using namespace std;

#include <EXTERN.h>
#define HAS_BOOL 1
#include <perl.h>

#include <stdlib.h>

PerlScriptInterpreter *PerlScriptInterpreter::singleton = NULL;

/**
 * Construct a PerlScriptInterpreter.
 * Allocates a new embedded perl instance.
 * WARNING: Will throw a BasicException if this constructor is call more
 * than once.
 */
PerlScriptInterpreter::PerlScriptInterpreter() :
    started(false)
{

    ASSERT_OR_THROW("Singleton!", !singleton);
    singleton = this;
    my_perl = perl_alloc();
}

/**
 * Destruct a PearlScriptInterpreter.
 * Deallocates the embedded pearl instance.
 */
PerlScriptInterpreter::~PerlScriptInterpreter()
{
    if (started) {
        stop();
    }

    perl_free(my_perl);
}

/**
 * Get a singleton instance of the PerlScriptInterpreter.
 * There must only be once instance of this class because embedded perl
 * has problems with there being more than one.  If an instance has
 * not yet been constructed one will be created.
 *
 * @return A pointer to the single instance of this class.
 */
PerlScriptInterpreter *PerlScriptInterpreter::getSingleton()
{
    if (!singleton) {
        singleton = new PerlScriptInterpreter();
    }

    return singleton;
}

/**
 * Start the perl intrepreter.
 */
void PerlScriptInterpreter::start()
{
    char *embedding[] = {(char *)"", (char *)"-e", (char *)"0"};

    perl_construct(my_perl);
    perl_parse(my_perl, NULL, 3, embedding, NULL);
    perl_run(my_perl);
    started = true;
}

/**
 * Stop the perl intrepreter.
 */
void PerlScriptInterpreter::stop()
{
    ASSERT_OR_THROW("Not started!", started);

    //TODO: This call crashes on Mac. As perl_destruct is for cleanup purposes, /
    //for now the call is just commented out
    //perl_destruct(my_perl);
    started = false;
}

/**
 * Define a variable in the interpreter.
 *
 * @param var The variable name.
 * @param value The variable value.
 *
 * @return A void pointer to the resulting value.
 */
void *PerlScriptInterpreter::define(const string var, const string value)
{
    string statement = string("$") + var + "=\"" + value + "\";";
    return eval(statement);
}

/**
 * Check if a variable is defined in this interpreter.
 *
 * @param var The variable name.
 *
 * @return true if it is defined, false otherwise.
 */
bool PerlScriptInterpreter::defined(const string var)
{
    ASSERT_OR_THROW("Not started!", started);

    string perlinput = string("defined ") + var + ";";
    return tobool(eval(perlinput));
}

/**
 * Evaluage a string of perl script.
 *
 * @param script The script.
 *
 * @return A void pointer to the resulting value.
 */
void *PerlScriptInterpreter::eval(const string script)
{
    ASSERT_OR_THROW("Not started!", started);
    ASSERT_OR_THROW("NULL script!", !script.empty());
    return perl_eval_pv((char *)script.c_str(), TRUE);
}

/**
 * Convert a void pointer value to an integer.
 *
 * @param value A pointer to the value.
 *
 * @return The integer result.
 */
int PerlScriptInterpreter::toint(void *value)
{
    ASSERT_OR_THROW("Not started!", started);

    return SvIV((SV *) value);
}

/**
 * convert a void pointer value to a string.
 *
 * @param value A pointer to the value.
 *
 * @return The string result.
 */
const std::string PerlScriptInterpreter::tostring(void *value)
{
    ASSERT_OR_THROW("Not started!", started);

    STRLEN len;
    return SvPV((SV *) value, len);
}

/**
 * Convert a void pointer value to a double.
 *
 * @param value A pointer to the value.
 *
 * @return The double result.
 */
double PerlScriptInterpreter::todouble(void *value)
{
    ASSERT_OR_THROW("Not started!", started);

    return SvNV((SV *) value);
}

/**
 * Convert a void pointer value to a bool.
 *
 * @param value A pointer to the value.
 *
 * @return The bool result.
 */
bool PerlScriptInterpreter::tobool(void *value)
{
    ASSERT_OR_THROW("Not started!", started);

    return SvTRUE((SV *) value);
}

/**
 * Up the variable scope.  This doesn't currently do anything useful in perl.
 */
void PerlScriptInterpreter::upscope()
{
    ENTER;
    SAVETMPS;
}

/**
 * Down the variable scope.  This doesn't currently do anything useful in perl.
 */
void PerlScriptInterpreter::downscope()
{
    FREETMPS;
    LEAVE;
}
