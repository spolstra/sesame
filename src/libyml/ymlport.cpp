/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlentityfactory.h"
#include "ymlport.h"
#include "ymlnetwork.h"
#include "ymllink.h"
#include "ymldefs.h"
#include "ymlconnection.h"

#include "../BasicUtils//BasicException.h"
#include "../BasicUtils//BasicString.h"
#include "../XMLCereal/XMLSerializer.h"

#include <iostream>
#include <string>
using namespace std;

/**
 * Construct a YMLPort.
 *
 */
YMLPort::YMLPort() : parent(NULL), dir(dUnknown),
    inSide(this, &outSide, YMLPortSide::sIn),
    outSide(this, &inSide, YMLPortSide::sOut),
    id(-1)
{
}

YMLPort *YMLPort::getRemotePort(YMLConnection *connection)
{
    YMLPort *remotePort;

    if (connection->getPortSide(0)->getPort() == this) {
        remotePort = connection->getPortSide(1)->getPort();
    } else if (connection->getPortSide(1)->getPort() == this) {
        remotePort = connection->getPortSide(0)->getPort();
    } else {
        THROW("Port does not match either side of connection!");
    }

    return remotePort;
}

YMLPort *YMLPort::getRemotePort()
{
    return getRemotePort(getConnection());
}

void YMLPort::getRemotePorts(std::vector<YMLPort *> *ports)
{
    for (unsigned int i = 0; i < connections.size(); i++) {
        ports->push_back(getRemotePort(connections[i]));
    }
}

YMLConnection *YMLPort::getConnection()
{
    ASSERT_OR_THROW(string("Port '") + getFullName() +
                    "' must have one connection not " +
                    BasicString(connections.size()) +
                    " to get connection!",
                    connections.size() == 1);

    return connections[0];
}


void YMLPort::setID(int id)
{
    ASSERT_OR_THROW(string("Port '") + getFullName() + "' ID all ready set!",
                    this->id == -1);
    this->id = id;
}

int YMLPort::getID()
{
    ASSERT_OR_THROW(string("Port '") + getFullName() + "' ID not set!",
                    this->id != -1);
    return id;
}

/**
 * Print the fullname of this node to the provided stream.
 * The fullname is the concatenation the ports ancestors starting
 * with the root and separated by NAME_DELIM.  NAME_DELIM is '.' by default.
 *
 * @param stream The output stream.
 */
void YMLPort::dumpFullName(std::ostream &stream)
{
    ASSERT_OR_THROW("Port must have parent!", parent);

    if (parent->getType() == ymlNode || parent->getType() == ymlNetwork) {
        ((YMLNode *)parent)->dumpFullName(stream);
        stream << NAME_DELIM;
    }

    stream << getName();
}

string YMLPort::getFullName()
{
    string fullname;

    if (parent->getType() == ymlNode || parent->getType() == ymlNetwork) {
        fullname = ((YMLNode *)parent)->getFullName() + NAME_DELIM;
    }

    return fullname + getName();
}

/**
 * Print the port as XML to the provided output stream.
 * New lines are prefixed by tabLevel number of tabs.
 *
 * @param stream The output stream.
 */
void YMLPort::dumpXML(XMLSerializer &stream)
{
    XMLSerializer::attributes_t attrs;
    attrs[YML_ATTR_NAME] = getName();
    attrs[YML_ATTR_DIR] = getArg(aDir);

    stream.startElement(YML_ELEM_PORT, attrs);
    dumpXMLProperties(stream);
    stream.endElement();
}

/**
 * Look up the direction attribute string and convert it to a
 * direction type.
 */
void YMLPort::resolveDir()
{
    if (getArg(aDir).compare(YML_ATTR_DIR_IN) == 0) {
        dir = dIn;
    } else if (getArg(aDir).compare(YML_ATTR_DIR_OUT) == 0) {
        dir = dOut;
    } else {
        THROW(string("Unknown direction type '") + getArg(aDir) + "'.");
    }
}


