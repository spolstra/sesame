/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlnetwork.h"
#include "ymllink.h"
#include "ymlconnection.h"
#include "ymlnode.h"
#include "ymlport.h"
#include "ymldefs.h"
#include "ymlentityfactory.h"

#include "../XMLCereal/XMLSerializer.h"

#include "../libbasic/basicutilfuncts.h"

#include "../BasicUtils//BasicException.h"

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

YMLNetwork::YMLNetwork() : traceContext(0) {}

YMLNetwork::~YMLNetwork()
{
    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (nodes[i]) {
            delete nodes[i];
        }
    }

    for (unsigned int i = 0; i < links.size(); i++) {
        if (links[i]) {
            delete links[i];
        }
    }

    for (unsigned int i = 0; i < connections.size(); i++) {
        if (connections[i]) {
            delete connections[i];
        }
    }
}

/**
 * Add a node to this network.
 * If the node name is already in use the function will throw a
 * BasicException
 *
 * @param node The node to add.
 */
void YMLNetwork::addNode(YMLNode *node)
{
    ASSERT_OR_THROW(string("Duplicate node name '") + getFullName() +
                    NAME_DELIM + node->getName() + "'!",
                    node_map[node->getName()] == 0);

    node_map[node->getName()] = node;
    nodes.push_back(node);
    node->setNetwork(this);
}

/**
 * Copy all of the nodes in this network and sub-networks in order in to
 * the provided list.
 *
 * @param list The list of collected nodes.
 */
void YMLNetwork::collectAllNodes(node_list_t &list)
{
    int numNodes = getNumNodes();

    for (int i = 0; i < numNodes; i++) {
        YMLNode *node = getNode(i);

        if (node->getType() == ymlNetwork) {
            ((YMLNetwork *)node)->collectAllNodes(list);
        } else if (node->getType() == ymlNode) {
            list.push_back(node);
        }
    }
}

/**
 * Get the correct side of a port in the context of this network given the
 * node and port names.  The function will throw a BasicException if the
 * node or port are not found.
 *
 * @param nodeName The node name.
 * @param portName The port name.
 *
 * @return The port side.
 */
YMLPortSide *YMLNetwork::getPortSide(const string nodeName,
                                     const string portName)
{
    YMLPort *port;
    YMLNode *node;
    bool inSide = false;

    // Find node
    if (nodeName.compare("this") == 0) {
        node = this;
        inSide = true;
    } else {
        node = findNode(nodeName);
        ASSERT_OR_THROW(string("Node '") + getFullName() + NAME_DELIM +
                        nodeName + "' not found!", node);
    }

    // Find port
    port = node->findPort(portName);
    ASSERT_OR_THROW(string("Port '") + getFullName() + NAME_DELIM +
                    portName + "' not found!", port);

    // Return side
    if (inSide) {
        return port->getInSide();
    } else {
        return port->getOutSide();
    }
}

/**
 * Connected links and check directions and types.
 *
 * @return true on success, false otherwise.
 */
bool YMLNetwork::connect()
{
    // Connect all links
    for (unsigned int i = 0; i < links.size(); i++) {
        YMLLink *link = links[i];

        // Get ports and sides
        YMLPortSide *side1 =
            getPortSide(link->getNodeName(0), link->getPortName(0));
        YMLPortSide *side2 =
            getPortSide(link->getNodeName(1), link->getPortName(1));
        YMLPort *port1 = side1->getPort();
        YMLPort *port2 = side2->getPort();

        if (port1->getParent() == this && port2->getParent() == this) {
            THROW(string("Network loop backs not allowed. '") + getFullName() + "'");
        }

        // Check directions
        int dir1 = side1->getDir();

        if (dir1 != YMLPort::dOut)
            THROW(string("Port '") + port1->getFullName() +
                  "' with dir='" + port1->getArg(YMLPort::aDir) +
                  "' cannot be linked as an inport!!");

        int dir2 = side2->getDir();

        if (dir2 != YMLPort::dIn)
            THROW(string("Port '") + port2->getFullName() +
                  "' with dir='" + port2->getArg(YMLPort::aDir) +
                  "' cannot be linked as an outport!!");

        // Make the connection
        side1->addLink(side2);

        if (side1 != side2) {
            side2->addLink(side1);
        }

        link->setPortSide(0, side1);
        link->setPortSide(1, side2);

        link->setNetwork(this);

        int commChannelID = link->getCommChannelID();

        if (commChannelID != -1) {
            if ((side1->getCommChannelID() != -1 &&
                 side1->getCommChannelID() != commChannelID) ||
                (side2->getCommChannelID() != -1 &&
                 side2->getCommChannelID() != commChannelID)) {
                THROW("Communication channel id already set!");
            }

            side1->setCommChannelID(commChannelID);
            side2->setCommChannelID(commChannelID);
        }

#ifdef YMLNETWORK_DEBUG
        cout << "Connected '";
        port1->dumpFullName(cout);
        cout << "' --> '";
        port2->dumpFullName(cout);
        cout << "'." << endl;
#endif
    }

    return true;
}

/**
 * Connect port to ports with direct connections.  This function bypasses
 * YMLLinks with YMLConnections.  Starting from side createDirectConnections
 * branches out recursively until it hits a YMLNode.  When an node is
 * encountered a YMLConnection is created between it and the source port.
 *
 * @param factory The YMLEntityFactory to use for createing YMLConnections.
 * @param source The source port.
 * @param side The port side pointer indicating the direction to branch out.
 */
void YMLNetwork::createDirectConnections(YMLEntityFactory *factory,
        YMLPort *source,
        YMLPortSide *side,
        int commChannelID)
{
    ASSERT_OR_THROW("NULL source!", source);
    ASSERT_OR_THROW("NULL side!", side);
    YMLPort *thisPort = side->getPort();
    ASSERT_OR_THROW("NULL port!", thisPort);
    YMLNode *thisNode = thisPort->getParent();

    if (commChannelID == -1) {
        commChannelID = side->getCommChannelID();
    }

    // TODO: This is a hack which disables communication channel mapping
    // when the YMLConnection is made up of more than one YMLLink.
    // There is a design flaw in the negotiation of channel trace ids
    // which makes this necessary for now.
    // Email sesame@joe.coffland.com for more info.
    if (commChannelID != side->getCommChannelID()) {
        commChannelID = -2;
    }

    // Base case: We hit the outside of a node
    if (side->getType() == YMLPortSide::sOut && thisNode->getType() == ymlNode) {
        YMLConnection *connection = factory->createConnection(source, thisPort);
        connections.push_back(connection);
        connection->setPortSide(0, source->getOutSide());
        connection->setPortSide(1, thisPort->getOutSide());
        connection->setCommChannelID(commChannelID);

        source->addConnection(connection);
        thisPort->addConnection(connection);

    } else { // Recursive case
        side = side->getOtherSide();

        for (unsigned int i = 0; i < side->getNumLinks(); i++) {
            YMLPortSide *oside = (*side)[i];
            ASSERT_OR_THROW("NULL side!", oside);

            if (oside->getDir() == YMLPort::dIn) {
                createDirectConnections(factory, source, oside, commChannelID);
            }
        }
    }
}

/**
 * Create direct connections for all of the connections originating in this
 * network.
 *
 * @param factory The YMLEntityFactory to use for creating YMLConnections.
 */
void YMLNetwork::createDirectConnections(YMLEntityFactory *factory)
{
    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getType() == ymlNetwork) {
            ((YMLNetwork *)nodes[i])->createDirectConnections(factory);
        } else {
            for (unsigned int j = 0; j < nodes[i]->getNumPorts(); j++) {
                YMLPort *port = nodes[i]->getPortByIdx(j);

                if (port->getDir() == YMLPort::dOut) {
                    createDirectConnections(factory, port, port->getInSide(), -1);
                }
            }
        }
    }
}


/**
 * Print this network as a GraphViz dot graph.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 * @param flat If true do not recur to sub networks.
 */
void YMLNetwork::dumpDOT(std::ostream &stream, int tabLevel, bool flat)
{
    tabs(stream, tabLevel);
    stream << "digraph ";

    ostringstream fullName;
    dumpFullName(fullName);
    fullName << ends;
    dumpCleanCString(fullName.str().c_str(), stream);

    stream << " {" << endl;

    tabs(stream, tabLevel + 1);
    stream << "rankdir=LR;" << endl;

    dumpDOTNodes(stream, tabLevel + 1);
    dumpDOTSubNets(stream, tabLevel + 1, flat);
    dumpDOTConnections(stream, tabLevel + 1, flat);

    tabs(stream, tabLevel);
    stream << "}";
}

/**
 * Print this networks subnets as a GraphViz dot graph.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 * @param flat If true do not recur to sub networks and print them as normal
 *             nodes.
 */
void YMLNetwork::dumpDOTSubNet(std::ostream &stream, int tabLevel, bool flat)
{
    tabs(stream, tabLevel);

    if (!flat) {
        stream << "subgraph cluster";
    }

    ostringstream fullName;
    dumpFullName(fullName);
    fullName << ends;
    dumpCleanCString(fullName.str().c_str(), stream);

    if (flat) {
        stream << " [label=\"";
        dumpCleanCString(getName(), stream);
        stream << "\"];";
    } else {
        stream << " {" << endl;

        dumpDOTNodes(stream, tabLevel + 1);
        dumpDOTSubNets(stream, tabLevel + 1);

        tabs(stream, tabLevel);
        stream << "}";
    }
}

/**
 * Print this networks connections as a GraphViz dot graph.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 * @param flat If true do not print connections that extend outside of this
 *             network.
 */
void YMLNetwork::dumpDOTConnections(std::ostream &stream, int tabLevel,
                                    bool flat)
{
    if (flat) {
        for (unsigned int i = 0; i < links.size(); i++) {
            links[i]->dumpDOT(stream, tabLevel);
            stream << endl;
        }
    } else {
        for (unsigned int i = 0; i < connections.size(); i++) {
            connections[i]->dumpDOT(stream, tabLevel);
            stream << endl;
        }

        // Dump subnet connections
        for (unsigned int i = 0; i < nodes.size(); i++)
            if (nodes[i]->getType() == ymlNetwork) {
                ((YMLNetwork *)nodes[i])->dumpDOTConnections(stream, tabLevel);
            }
    }
}

/**
 * Print this networks nodes as a GraphViz dot graph.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 */
void YMLNetwork::dumpDOTNodes(std::ostream &stream, int tabLevel)
{
    for (unsigned int i = 0; i < nodes.size(); i++)
        if (nodes[i]->getType() == ymlNode) {
            nodes[i]->dumpDOT(stream, tabLevel);
            stream << endl;
        }
}

/**
 * Print this networks subnetworks as a GraphViz dot graph.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 * @param flat If true subnetworks should only print one level deep.
 */
void YMLNetwork::dumpDOTSubNets(std::ostream &stream, int tabLevel,
                                bool flat)
{
    for (unsigned int i = 0; i < nodes.size(); i++)
        if (nodes[i]->getType() == ymlNetwork) {
            ((YMLNetwork *)nodes[i])->dumpDOTSubNet(stream, tabLevel, flat);
            stream << endl;
        }
}

/**
 * Print this networks links as XML.
 *
 * @param stream The output stream.
 */
void YMLNetwork::dumpXMLLinks(XMLSerializer &stream)
{
    for (unsigned int i = 0; i < links.size(); i++) {
        links[i]->dumpXML(stream);
    }
}

/**
 * Print this networks nodes as XML.
 *
 * @param stream The output stream.
 */
void YMLNetwork::dumpXMLNodes(XMLSerializer &stream)
{
    for (unsigned int i = 0; i < nodes.size(); i++) {
        nodes[i]->dumpXML(stream);
    }
}


/**
 * Print this network as XML.
 *
 * @param stream The output stream.
 */
void YMLNetwork::dumpXML(XMLSerializer &stream)
{
    XMLSerializer::attributes_t attrs;
    attrs[YML_ATTR_NAME] = getName();
    attrs[YML_ATTR_CLASS] = getClass();

    if (getNetwork() == 0) {
        attrs["xmlns"] = "http://sesamesim.sourceforge.net/YML";
    }

    stream.startElement(YML_ELEM_NETWORK, attrs);
    dumpXMLProperties(stream);
    dumpXMLPorts(stream);
    dumpXMLNodes(stream);
    dumpXMLLinks(stream);
    stream.endElement();
}

/**
 * Call finalize on all nodes.
 */
void YMLNetwork::doFinalizeNodes()
{
    for (unsigned int i = 0; i < nodes.size(); i++) {
        nodes[i]->doFinalize();
    }
}

/**
 * Call finalize on all links.
 */
void YMLNetwork::doFinalizeLinks()
{
    for (unsigned int i = 0; i < links.size(); i++) {
        links[i]->doFinalize();
    }
}

/**
 * Call doFinalizeNodes(), doFinalizeLinks(), doFinalizePorts(), then
 * finalize().
 */
void YMLNetwork::doFinalize()
{
    doFinalizeNodes();
    doFinalizeLinks();
    doFinalizePorts();
    finalize();
}
