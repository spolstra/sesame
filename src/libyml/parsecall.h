/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   parsecall.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:18:17 2003
 *
 * @brief  ParseCall class definition.
 *
 * The ParseCall class encapsulates a YML SAX parse call.  It is used
 * for recording parse calls so they can be replayed multiple times
 * in YML sets.
 */

#ifndef PARSECALL_H
#define PARSECALL_H

#include <string>
#include <vector>

typedef enum {eStartNetwork, eEndNetwork, eStartNode,
              eEndNode, eStartLink, eEndLink, eStartPort,
              eEndPort, eStartProp, eEndProp, eStartSet,
              eEndSet, eScript, eDoc
             } call_t;

class ParseCall
{
        call_t type;
        std::vector<std::string> args;

    public:
        // Operations
        ParseCall(call_t type, const std::vector<std::string> args);

        call_t getType() {
            return type;
        }

        unsigned int getArgc() {
            return args.size();
        }

        const std::string getArg(const unsigned int i) {
            return args[i];
        }

        const std::vector<std::string> getArgs() {
            return args;
        }
};

#endif
