/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlsaxparser.h"
#include "ymldefs.h"
#include "interpreter.h"

#include <xercesc/sax2/Attributes.hpp>
#include <iostream>
#include <sstream>

#include "../BasicUtils//BasicException.h"
#include "../XercesUtils/XercesStr.h"

using namespace std;

#ifdef XERCES_HAS_CPP_NAMESPACE
XERCES_CPP_NAMESPACE_USE;
#endif

/**
 * Construct a YMLSAXParser
 *
 */
YMLSAXParser::YMLSAXParser() : interpreter(NULL), setDepth(0)
{
#ifdef YMLSAXPARSER_DEBUG
    cout << "YMLSAXParser()" << endl;
#endif
}

/**
 * Destruct a YMLSAXParser
 *
 */
YMLSAXParser::~YMLSAXParser()
{
#ifdef YMLSAXPARSER_DEBUG
    cout << "~YMLSAXParser()" << endl;
#endif
    clearParseCalls();
}

void YMLSAXParser::clearParseCalls()
{
    parseCalls_t::iterator i;

    for (i = parseCalls.begin(); i != parseCalls.end(); i++) {
        delete(*i);
    }

    parseCalls.clear();
}

/**
 * Lookup a variable as a string value in the Intrepreter.
 *
 * @param var The variable name.
 *
 * @return The variable string value on success, the empty string otherwise.
 */
const string YMLSAXParser::lookup(const string var)
{
    if (var[0] != '$') {
        return var;
    }

    ASSERT_OR_THROW("Interpereter not set!", interpreter);

    if (!interpreter->defined(var)) {
        ostringstream errStr;
        errStr << "Undefined variable \'" << var << '\'' << ends;
        SAXParseException exception(XStrG(errStr.str().c_str()), 0, 0, 0, 0);
        throw exception;
    }

    void *value = interpreter->eval(var);

    if (!value) {
        return "";
    }

    return interpreter->tostring(value);
}


/**
 * Calls the individual start and end Network, Node, Link, Port, Property
 * and doc functions. Reimplementing this function is a good way to specialize
 * the YMLSAXParser for your needs.
 *
 * @param type The parse call type.
 * @param argc The number of arguments to this call.
 * @param args The arguments for this call.
 */
void YMLSAXParser::parseCall(const call_t type, vector<string> &args)
{
    switch (type) {
        case eStartNetwork:
            startNetwork(args[0], args[1]);
            break;

        case eEndNetwork:
            endNetwork();
            break;

        case eStartNode:
            startNode(args[0], args[1]);
            break;

        case eEndNode:
            endNode();
            break;

        case eStartLink:
            startLink(args[0], args[1],
                      args[2], args[3]);
            break;

        case eEndLink:
            endLink();
            break;

        case eStartPort:
            startPort(args[0], args[1]);
            break;

        case eEndPort:
            endPort();
            break;

        case eStartProp:
            startProperty(args[0], args[1]);
            break;

        case eEndProp:
            endProperty();
            break;

        case eDoc:
            doc(args[0]);
            break;

        default:
            break;
            // TO DO add error handling
    }
}

/**
 * Handles interpreter scope control, looks up variable arguments in the
 * intrepreter and calls parseCall.
 *
 * @param type
 * @param argc
 * @param args
 */
void YMLSAXParser::localParseCall(const call_t type,
                                  const vector<string> &args)
{
    switch (type) {

            // Non local parse calls
        case eStartNetwork:
        case eStartNode:
        case eStartLink:
        case eStartPort:
        case eStartProp:
            if (interpreter) {
                interpreter->upscope();
            }

            break;

        case eEndNetwork:
        case eEndNode:
        case eEndLink:
        case eEndPort:
        case eEndProp:
            if (interpreter) {
                interpreter->downscope();
            }

            break;

        case eDoc:
            break;

            // Local parse calls
        case eStartSet:
            THROW("StartSet in localParseCall!");
            return;

        case eEndSet:
            endSet();
            return;

        case eScript:
            script(args[0]);
            return;

        default:
            THROW("Invalid parse call type!");
            return;
    }

    vector<string> pargs;
    lookupArgs(args, pargs);
    parseCall(type, pargs);
}

/**
 * Look up arguments which begin with $ as variables in the intrepreter.
 *
 * @param argc Number of arguments.
 * @param args An array of pointers to the arguments.
 * @param pargs Array to hold the argument results.
 */
void YMLSAXParser::lookupArgs(const vector<string> &args,
                              vector<string> &pargs)
{
    for (unsigned int i = 0; i < args.size(); i++) {
        pargs.push_back(lookup(args[i]));
    }
}

/**
 * Dispatches a parse call to parseCall or localParseCall depending
 * on if we are currently replaying a set or not.
 *
 * @param type
 * @param argc
 * @param args
 */
void YMLSAXParser::dispatch(const call_t type, vector<string> &args)
{
    if (type == eStartSet) {
        setDepth++;
    }

    if (type == eEndSet) {
        setDepth--;
    }

    if (setDepth) {
        parseCalls.push_back(new ParseCall(type, args));
    } else {
        localParseCall(type, args);
    }
}

/**
 * Execute a set by replaying the parse calls in side the set as perscribed by
 * the sets loop control arguments.
 *
 * @param start Pointer to where this set starts.
 */
void YMLSAXParser::executeSet(parseCalls_t::iterator start)
{
    ParseCall *startSetCall = *start;
    ASSERT_OR_THROW("Invalid type!", startSetCall->getType() == eStartSet);
    start++;

    const string init = startSetCall->getArg(0);
    const string cond = startSetCall->getArg(1);
    const string loop = startSetCall->getArg(2);

    ASSERT_OR_THROW("Interpreter not set!", interpreter);
    // Init
    interpreter->eval(init);

    // Condition
    while (interpreter->tobool(interpreter->eval(cond))) {

        parseCalls_t::iterator i;

        for (i = start; i != parseCalls.end(); i++) {
            ParseCall *call = *i;

            if (call->getType() == eEndSet) {
                break;
            }

            if (call->getType() == eStartSet) {
                executeSet(i);

                // Skip over sub set
                int localSetDepth = 1;

                while (localSetDepth) {
                    i++;
                    ASSERT_OR_THROW("Missing eEndSet", i != parseCalls.end());

                    if ((*i)->getType() == eStartSet) {
                        localSetDepth++;
                    }

                    if ((*i)->getType() == eEndSet) {
                        localSetDepth--;
                    }
                }

                continue;
            }

            localParseCall(call->getType(), call->getArgs());
        }

        // Loop
        interpreter->eval(loop);
    }
}

/**
 * This function should never be called.
 *
 * @param loop loop argument
 * @param init init argument
 * @param cond cond argument
 */
void YMLSAXParser::startSet(const string, const string,
                            const string)
{
    THROW("startSet should never be called!");
}

/**
 * Called on the end of a set element.  Calls executeSet and then deallocates
 * parse calls.
 *
 */
void YMLSAXParser::endSet()
{
    executeSet(parseCalls.begin());

    // Cleanup set
    clearParseCalls();
}

/**
 * Execute a bit of script in the intrepreter.
 *
 * @param data The script.
 */
void YMLSAXParser::script(const string data)
{
    ASSERT_OR_THROW("Interpreter not set!", interpreter);
    interpreter->eval(data);
}


/**
 * Does nothing.  Part of the DocumentHandler interface.
 *
 */
void YMLSAXParser::startDocument()
{
#ifdef YMLSAXPARSER_DEBUG
    cout << "Start document" << endl;
#endif
}

/**
 * Does nothing.  Part of the DocumentHandler interface.
 *
 */
void YMLSAXParser::endDocument()
{
#ifdef YMLSAXPARSER_DEBUG
    cout << "End document" << endl;
#endif
}


const string getAttr(const Attributes &attrs, const string name, const bool required = true)
{
    XMLCh *gName = XMLString::transcode(name.c_str());
    const XMLCh *value = attrs.getValue(0, gName);
    XMLString::release(&gName);

    ASSERT_OR_THROW(string("Attribute '") + name + "' not found!", value || !required);

    if (!value) {
        return "";
    }

    string res = XStrL(value);

    return res;
}

/**
 * Checks for the known YML types and dispatches parse calls for them.
 *
 * @param uri Element uniform resource identifier.
 * @param localname Element local name.
 * @param qname Element fully qualified name.
 * @param attributes The attributes.
 */
void YMLSAXParser::startElement(const XMLCh *const /*uri*/,
                                const XMLCh *const localname,
                                const XMLCh *const /*qname*/,
                                const Attributes &attrs)
{
    vector<string> args;
    const string name = XStrL(localname);

#ifdef YMLSAXPARSER_DEBUG
    cout << "Start Element " << name << endl;
#endif

    // TO DO error checking on attribute values
    if (name == YML_ELEM_NETWORK) {
        args.push_back(getAttr(attrs, YML_ATTR_NAME));
        args.push_back(getAttr(attrs, YML_ATTR_CLASS));

        dispatch(eStartNetwork, args);
    } else if (name == YML_ELEM_NODE) {
        args.push_back(getAttr(attrs, YML_ATTR_NAME));
        args.push_back(getAttr(attrs, YML_ATTR_CLASS));

        dispatch(eStartNode, args);
    } else if (name == YML_ELEM_LINK) {
        args.push_back(getAttr(attrs, YML_ATTR_OBJECT1)); //innode
        args.push_back(getAttr(attrs, YML_ATTR_PORT1)); //inport
        args.push_back(getAttr(attrs, YML_ATTR_OBJECT2)); //outnode
        args.push_back(getAttr(attrs, YML_ATTR_PORT2)); //outport

        dispatch(eStartLink, args);
    } else if (name == YML_ELEM_PORT) {
        args.push_back(getAttr(attrs, YML_ATTR_NAME));
        args.push_back(getAttr(attrs, YML_ATTR_DIR));
        args.push_back(getAttr(attrs, YML_ATTR_DATADIR, false));

        dispatch(eStartPort, args);
    } else if (name == YML_ELEM_PROPERTY) {
        args.push_back(getAttr(attrs, YML_ATTR_NAME));
        args.push_back(getAttr(attrs, YML_ATTR_VALUE, false));

        dispatch(eStartProp, args);
    } else if (name == YML_ELEM_DOC) {
        clearText();
    } else if (name == YML_ELEM_SET) {
        args.push_back(getAttr(attrs, YML_ATTR_INIT));
        args.push_back(getAttr(attrs, YML_ATTR_COND));
        args.push_back(getAttr(attrs, YML_ATTR_LOOP));

        dispatch(eStartSet, args);
    } else if (name == YML_ELEM_SCRIPT) {
        clearText();
    }
}

/**
 * Checks for the known YML types and dispatches parse calls for them.
 *
 * @param uri Element uniform resource identifier.
 * @param localname Element local name.
 * @param qname Element fully qualified name.
 */
void YMLSAXParser::endElement(const XMLCh *const /*uri*/,
                              const XMLCh *const localname,
                              const XMLCh *const /*qname*/)
{
    const string name = XStrL(localname);
    vector<string> args;

#ifdef YMLSAXPARSER_DEBUG
    cout << "End Element " << name << endl;
#endif

    if (name == YML_ELEM_NETWORK) {
        dispatch(eEndNetwork, args);
    } else if (name == YML_ELEM_NODE) {
        dispatch(eEndNode, args);
    } else if (name == YML_ELEM_LINK) {
        dispatch(eEndLink, args);
    } else if (name == YML_ELEM_PORT) {
        dispatch(eEndPort, args);
    } else if (name == YML_ELEM_PROPERTY) {
        dispatch(eEndProp, args);
    } else if (name == YML_ELEM_DOC) {
        if (!text.empty()) {
            args.push_back(text);
        } else {
            args.push_back("");
        }

        dispatch(eDoc, args);
    } else if (name == YML_ELEM_SET) {
        dispatch(eEndSet, args);
    } else if (name == YML_ELEM_SCRIPT) {
        if (!text.empty()) {
            args.push_back(text);
        } else {
            args.push_back("");
        }

        dispatch(eScript, args);
    }
}

/**
 * Records text data.
 *
 * @param chars The character data.
 * @param length Its length.
 */
void YMLSAXParser::characters(const XMLCh *const chars,
                              const XMLSize_t /*length*/)
{
    text = XStrL(chars);
}
