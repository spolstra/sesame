/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlnode.h"
#include "ymlnetwork.h"
#include "ymldefs.h"

#include "../libymlmap/tracechannel.h"
#include "../libbasic/basicutilfuncts.h"

#include "../BasicUtils//BasicException.h"
#include "../BasicUtils//BasicString.h"
#include "../XMLCereal/XMLSerializer.h"

#include <string>
#include <sstream>
#include <iostream>

using namespace std;

#define IDX_OFFSET (1 << 29)

/**
 * Construct a new YMLNode.
 *
 */
YMLNode::YMLNode() : network(0), traceChannel(0), channelID(-1) {}

/**
 * Destruct a YMLNode
 *
 * We delete all the ports in our portMap.
 */
YMLNode::~YMLNode()
{
    for (portMap_t::iterator it = portMap.begin(); it != portMap.end(); it++) {
        delete it->second;
    }
}

/**
 * Add a port to this node.  It will fail to add the port and throw a
 * BasicException if the port name is already in use in this node.
 *
 * @param port The port to add.
 */
void YMLNode::addPort(YMLPort *port)
{
    ASSERT_OR_THROW(string("Duplicate port name '") + getName() + NAME_DELIM +
                    port->getName() + "'!",
                    portMap.find(port->getName()) == portMap.end());

    portMap[port->getName()] = port;

    int id = -1;

    if (traceChannel) {
        id = traceChannel->getPortId(port->getName());
    }

    if (id == -1) {
        id = ports.size() + IDX_OFFSET;
    } else {
        if ((unsigned int)id >= mappedPorts.size()) {
            mappedPorts.resize(id + 1, NULL);
        }

        mappedPorts[id] = port;
    }

    ports.push_back(port);
    port->setID(id);

    port->setParent(this);
}

/**
 * Find a port with a given name
 *
 * @param name The name of the port to find
 *
 * @return The found YMLPort.
 */
YMLPort *YMLNode::findPort(const string name)
{
    portMap_t::iterator it = portMap.find(name);
    ASSERT_OR_THROW(string("Port '") + name + "' not found!",
                    it != portMap.end());

    return it->second;
}

YMLPort *YMLNode::getPort(const unsigned int id)
{
    YMLPort *port;

    if (id >= IDX_OFFSET) {
        port = ports[id - IDX_OFFSET];
    } else {
        port = mappedPorts[id];
    }

    ASSERT_OR_THROW(string("Port not found for id ") + BasicString(id) + "!",
                    port);
    return port;
}

/**
 * Print the nodes fullname to the provided stream.
 *
 * @param stream The output stream.
 */
void YMLNode::dumpFullName(std::ostream &stream)
{
    YMLNode *parent = (YMLNode *)getNetwork();

    if (parent != NULL && parent != this) {
        parent->dumpFullName(stream);
        stream << NAME_DELIM;
    }

    stream << getName();
}

/**
 * Get the nodes fullname as a std::string.
 *
 * @return fullname string.
 */
std::string YMLNode::getFullName()
{
    std::string fullName = getName();

    YMLNode *parent = (YMLNode *)getNetwork();

    if (parent != NULL && parent != this) {
        return parent->getFullName() + NAME_DELIM + fullName;
    }

    return fullName;
}

/**
 * Set the tracchannel for this YMLNode
 *
 * @param traceChannel The TraceChannel to use as TraceChannel for this node.
 */
void YMLNode::setTraceChannel(TraceChannel *traceChannel)
{
    ASSERT_OR_THROW("Trace channel all ready set!", !this->traceChannel);

    this->traceChannel = traceChannel;
}

/**
 * Get the TraceChannel for this node.
 *
 * @return The tracechannel for this node. If this node has no private TraceChannel
 * the TraceChannel of the network this node belongs to is returnd. If this
 * node also no network a NULL pointer is returned.
 */
TraceChannel *YMLNode::getTraceChannel()
{
    if (traceChannel) {
        return traceChannel;
    }

    if (getNetwork()) {
        return getNetwork()->getTraceChannel();
    }

    return NULL;
}

/**
 * Return the Channel id of this YMLNode
 *
 * @return The channel id of this node is returned. If this node has no channel
 * id then the channel id of the network this node belongs to is returned. If
 * this node also has no network we return -1.
 */
int YMLNode::getChannelID() const
{
    if (channelID != -1) {
        return channelID;
    }

    if (getNetwork()) {
        return getNetwork()->getChannelID();
    }

    return -1;
}

/**
 * Print the ports as XML to the provides stream with the specified tabLevel.
 *
 * @param stream The output stream.
 */
void YMLNode::dumpXMLPorts(XMLSerializer &stream)
{
    portMap_t::iterator it;

    for (it = portMap.begin(); it != portMap.end(); it++) {
        it->second->dumpXML(stream);
    }
}

/**
 * Print the node as part of a GraphViz graph.
 *
 * @param stream The output stream
 * @param tabLevel The number of tabs to use.
 */
void YMLNode::dumpDOT(std::ostream &stream, int tabLevel)
{
    tabs(stream, tabLevel);

    ostringstream fullName;
    dumpFullName(fullName);
    fullName << ends;

    dumpCleanCString(fullName.str().c_str(), stream);

    stream << " [label=\"";
    dumpCleanCString(getName(), stream);
    stream << "\"];";
}

/**
 * Print this node as XML to the provided stream using the specified number
 * of tabs.
 *
 * @param stream The output stream.
 */
void YMLNode::dumpXML(XMLSerializer &stream)
{
    XMLSerializer::attributes_t attrs;
    attrs[YML_ATTR_NAME] = getName();
    attrs[YML_ATTR_CLASS] = getClass();

    stream.startElement(YML_ELEM_NODE, attrs);
    dumpXMLProperties(stream);
    dumpXMLPorts(stream);
    stream.endElement();
}

/**
 * Call finalize on all of the ports.
 */
void YMLNode::doFinalizePorts()
{
    portMap_t::iterator it;

    for (it = portMap.begin(); it != portMap.end(); it++) {
        it->second->doFinalize();
    }
}

/**
 * Call doFinalizePorts than call finalize on this node.
 */
void YMLNode::doFinalize()
{
    doFinalizePorts();
    finalize();
}
