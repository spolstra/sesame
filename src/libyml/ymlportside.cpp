/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlportside.h"
#include "ymlport.h"

#include "../BasicUtils/BasicException.h"

/**
 * Construct a port side.
 *
 * @param port A pointer to the parent port.
 * @param otherSide A pointer to the other side.
 * @param type The side type.
 */
YMLPortSide::YMLPortSide(YMLPort *port, YMLPortSide *otherSide, side_t type) :
    otherSide(otherSide), port(port), type(type), commChannelID(-1)
{
    ASSERT_OR_THROW("NULL port!", port);
}

/**
 * Get the port direction.
 *
 * @return The port direction.  Either YMLPort::dIn, or dOut
 */
int YMLPortSide::getDir()
{
    YMLPort::dir_t dir = port->getDir();

    if (type == sOut) {
        return dir;
    }

    switch (dir) {
        case YMLPort::dIn:
            return YMLPort::dOut;

        case YMLPort::dOut:
            return YMLPort::dIn;

        default:
            return YMLPort::dUnknown;
    }
}

