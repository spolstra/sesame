/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlnetwork.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:26:36 2003
 *
 * @brief  YMLNetwork class definition.
 *
 * This class encapsulates the YML network entity
 */

#ifndef YMLNETWORK_H
#define YMLNETWORK_H

#include <map>
#include <list>
#include <vector>


#include "ymlnode.h"

class YMLLink;
class YMLEntityFactory;
class YMLConnection;
class TraceContext;

typedef std::list<YMLNode *> node_list_t;

// YMLNode
class YMLNetwork: public YMLNode
{
        /// Name to YMLNode * map.
        std::map<std::string, YMLNode *> node_map;
        /// Array of YMLLinks in the network.
        std::vector<YMLLink *> links;
        /// Array of YMLNodes in this network.
        std::vector<YMLNode *> nodes;
        /// Array of YMLConnections in this network.
        std::vector<YMLConnection *> connections;

        TraceContext *traceContext;

    public:
        YMLNetwork();
        virtual ~YMLNetwork();

        // Link operations
        /**
         * Add a link to the network.
         *
         * @param link A pointer to the link.
         */
        void addLink(YMLLink *link) {
            links.push_back(link);
        }
        /**
         * @return The number of links currently in this network.
         */
        unsigned int getNumLinks() {
            return links.size();
        }
        /**
         * Get a link by id.  Ids are from 0 to getNumLinks() - 1.
         *
         * @param i The link id.
         *
         * @return A pointer to the YMLLink.
         */
        YMLLink *getLink(const unsigned int i) {
            return links[i];
        }

        // Node operations
        void addNode(YMLNode *node);
        /**
         * Find a YMLNode by name.
         *
         * @param name The node name.
         *
         * @return A pointer to the YMLNode on success, NULL otherwise.
         */
        YMLNode *findNode(const std::string name) {
            return node_map[name];
        }
        /**
         * @return The number of nodes in this network.  Does not include
         *         grandchildren.
         */
        unsigned int getNumNodes() {
            return nodes.size();
        }
        /**
         * Get a node by id.  Ids are from 0 to getNumNodes() - 1.
         *
         * @param i The node id.
         *
         * @return A pointer to the node.
         */
        YMLNode *getNode(const unsigned int i) {
            return nodes[i];
        }
        void collectAllNodes(node_list_t &list);

        // Connection operations
        /**
         * @return The number of connections in this network.
         */
        unsigned int getNumConnections() {
            return connections.size();
        }
        /**
         * Get a connection by id. Ids are from 0 to getNumConnections() - 1.
         *
         * @param i The connection id.
         *
         * @return A pointer to the YMLConnection.
         */
        YMLConnection *getConnection(const unsigned int i) {
            return connections[i];
        }
        bool connect();
        void createDirectConnections(YMLEntityFactory *factory, YMLPort *source,
                                     YMLPortSide *side, int commChannelID);
        void createDirectConnections(YMLEntityFactory *factory);

        void setTraceContext(TraceContext *traceContext) {
            this->traceContext = traceContext;
        }
        TraceContext *getTraceContext() {
            return traceContext;
        }


        // XML operations
        virtual void dumpXML(XMLSerializer &stream);
        void dumpXMLLinks(XMLSerializer &stream);
        void dumpXMLNodes(XMLSerializer &stream);

        // DOT operations
        virtual void dumpDOT(std::ostream &stream, int tablevel) {
            dumpDOT(stream, tablevel, false);
        }
        virtual void dumpDOT(std::ostream &stream, int tabLevel, bool flat);
        void dumpDOTConnections(std::ostream &stream, int tabLevel,
                                bool flat = false);
        void dumpDOTNodes(std::ostream &stream, int tabLevel);
        void dumpDOTSubNets(std::ostream &stream, int tabLevel, bool flat = false);
        void dumpDOTSubNet(std::ostream &stream, int tabLevel, bool flat = false);

        // Finalize operations
        void doFinalizeNodes();
        void doFinalizeLinks();
        void doFinalize();

        /**
         * Get the YMLEntity type.
         *
         * @return ymlNetwork.
         */
        ymlentity_t getType() {
            return ymlNetwork;
        }

    private:
        YMLPortSide *getPortSide(const std::string nodeName, const std::string portName);
};

#endif
