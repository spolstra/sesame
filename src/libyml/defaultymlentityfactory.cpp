/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "defaultymlentityfactory.h"

#include "ymlnetwork.h"
#include "ymlnode.h"
#include "ymllink.h"
#include "ymlconnection.h"
#include "ymlport.h"


/**
 * Construct the default YMLConnection.
 *
 * @param inPort Not used.
 * @param outPort Not used.
 *
 * @return A pointer to the new YMLConnection.
 */
YMLConnection *DefaultYMLEntityFactory::createConnection(YMLPort *,
                                                         YMLPort *)
{
    return new YMLConnection();
}

/**
 * Construct the default YMLNetwork.
 *
 * @param name  Not used.
 * @param className  Not used.
 *
 * @return A pointer to the new YMLNetwork.
 */
YMLNetwork *DefaultYMLEntityFactory::createNetwork(const std::string,
                                                   const std::string)
{
    return new YMLNetwork();
}

/**
 * Construct the default YMLNode.
 *
 * @param name  Not used.
 * @param className  Not used.
 *
 * @return A pointer to the new YMLNode.
 */
YMLNode *DefaultYMLEntityFactory::createNode(const std::string,
                                             const std::string)
{
    return new YMLNode();
}

/**
 * Construct the default YMLLink.
 *
 * @param object1  Not used.
 * @param port1  Not used.
 * @param object2  Not used.
 * @param port2  Not used.
 *
 * @return A pointer to the new YMLLink.
 */
YMLLink *DefaultYMLEntityFactory::createLink(const std::string,
                                             const std::string,
                                             const std::string,
                                             const std::string)
{
    return new YMLLink();
}

/**
 * Construct the default YMLPort.
 *
 * @param name  Not used.
 *
 * @return A pointer to the new YMLPort.
 */
YMLPort *DefaultYMLEntityFactory::createPort(const std::string)
{
    return new YMLPort();
}
