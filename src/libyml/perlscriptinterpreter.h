/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   perlscriptinterpreter.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:14:03 2003
 *
 * @brief  PerlScriptIntrepreter class definition
 *
 * The PerlScriptIntrepreter class is a specialization of Intrepreter
 * which uses Lary Walls perl language as the YML scripter.
 *
 * WARNING: Because of the way embedded perl works it is no currently
 * possible to have more than one instance of PearlScriptIntrepreter.
 * The class ensures this via the Singelton pattern.  This is definately
 * not thread safe!!!
 */

#ifndef PERLSCRIPTINTERPRETER_H
#define PERLSCRIPTINTERPRETER_H

#include "interpreter.h"

#include <string>

struct interpreter;
typedef struct interpreter PerlInterpreter;


// Interpreter
class PerlScriptInterpreter: public Interpreter
{
        /// A pointer to the internal perl intrepreter.
        PerlInterpreter *my_perl;

        /// True if the intrepreter has been started,false otherwise.
        bool started;

        /// A pointer to the singleton instance of the PerlScriptInterpreter.
        static PerlScriptInterpreter *singleton;

    public:
        PerlScriptInterpreter();
        virtual ~PerlScriptInterpreter();

        static PerlScriptInterpreter *getSingleton();

        virtual void start();
        virtual void stop();
        virtual void *define(const std::string var, const std::string value);
        virtual bool defined(const std::string var);
        virtual void *eval(const std::string script);
        virtual int toint(void *value);
        virtual const std::string tostring(void *value);
        virtual double todouble(void *value);
        virtual bool tobool(void *value);
        virtual void upscope();
        virtual void downscope();
};
#endif
