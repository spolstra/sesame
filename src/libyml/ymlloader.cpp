/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlloader.h"
#include "ymlentityfactory.h"
#include "interpreter.h"
#include "ymlsaxfilter.h"

#include "../XercesUtils/StreamInputSource.h"

#include "../libymlmap/tracecontext.h"
#include "../ymlresources/ymlresources.h"

#include <stdio.h>

#include <iostream>
#include <string>

#include "ymlnode.h"
#include "ymlnetwork.h"
#include "ymllink.h"

using namespace std;

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/validators/common/Grammar.hpp>
XERCES_CPP_NAMESPACE_USE;

#include "../BasicUtils//BasicException.h"
#include "../BasicUtils//BasicProcess.h"

#include "../XercesUtils/XercesStr.h"
#include "../XercesUtils/BasicErrorHandler.h"


/**
 * Construct a YMLLoader.
 *
 * @param factory The YMLEntity factory to use.
 * @param interpreter The script Intrepreter to use.
 */
YMLLoader::YMLLoader(YMLEntityFactory *factory, Interpreter *interpreter) :
    factory(factory), interpreter(interpreter)
{
    ASSERT_OR_THROW("NULL factory!", factory);

    try {
        XMLPlatformUtils::Initialize();
    } catch (const XMLException &e) {
        THROW(XStrL(e.getMessage()));
    }

    setInterpreter(interpreter);
}

YMLLoader::~YMLLoader()
{
    try {
        XMLPlatformUtils::Terminate();
    } catch (const XMLException &e) {
        THROW(XStrL(e.getMessage()));
    }
}


/**
 * Parse YML from an istream.
 *
 * @param stream The input stream.
 * @param traceContext The trace mapping context.
 *
 * @return The top level YMLEntity on success, NULL otherwise.
 */
YMLNetwork *YMLLoader::parse(istream &stream, TraceContext *traceContext)
{
    StreamInputSource input(stream);
    return parse(&input, traceContext);
}

/**
 * Parase YML from a file.
 *
 * @param filename The path to the YML file.
 * @param traceContext The trace mapping context.
 *
 * @return The top level YMLEntity on success, NULL otherwise.
 */
YMLNetwork *YMLLoader::parse(const string filename,
                             TraceContext *traceContext)
{
    try {
        LocalFileInputSource input(XStrG(filename));

        return parse(&input, traceContext);
    } catch (const XMLException &e) {
        THROW(XStrL(e.getMessage()));
    }
}




/**
 * Parse YML from a XercesC InputSource.
 *
 * @param input The input source.
 * @param traceContext The trace mapping context.
 *
 * @return The top level YMLEntity on success, NULL otherwise.
 */
YMLNetwork *YMLLoader::parse(InputSource *input, TraceContext *traceContext)
{
    try {
        SAX2XMLReader *parser;

        SAX2XMLReader *reader = XMLReaderFactory::createXMLReader();

        /* This SAX2 filter sits between the application and the parser,
         * and filters elements nested inside a property.
         * This allows the parser to parse architecture yml which has
         * templates and mappings nested inside properties */

        /* For filter uncomment next 2 lines */
        SAX2XMLReader *filter = new YMLSAXFilter(reader);
        parser = filter;

        /* Without filter uncomment next line */
        // parser = reader;

        // SP: xinclude throws exception.
        // parser->setFeature(XMLUni::fgXercesDoXInclude, true);

        parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
        parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);
        parser->setFeature(XMLUni::fgXercesUseCachedGrammarInParse, true);
        parser->setFeature(XMLUni::fgXercesSchemaFullChecking, true);

        LocalFileInputSource schemaInput(XStrG(YML_SCHEMA));

        if (!parser->loadGrammar(schemaInput, Grammar::SchemaGrammarType, true)) {
            throw BasicException("Error loading schema grammar");
        }

        parser->setContentHandler(this);
        BasicErrorHandler errorHandler;
        parser->setErrorHandler(&errorHandler);

        root = 0;
        rootTraceContext = traceContext;
        parser->parse(*input);

        // Not smart pointer, so do cleanup ourselves
        delete reader;
        delete filter;

    } catch (const XMLException &e) {
        THROW(XStrL(e.getMessage()));

    } catch (const SAXParseException &e) {
        THROW(XStrL(e.getMessage()));

    } catch (BasicException &e) {
        throw;

    } catch (...) {
        THROW("Unexpected Exception during parse!");
    }

    if (!root) {
        THROW("Fatal Error: Failed to load network.");
    }

    root->createDirectConnections(factory);
    root->doFinalize();


    return root;
}

/**
 * Reimplementation of parseCall.
 * Creates YMLEntities and puts them on the parse stack on start calls.
 * Pops YMLEntites of the parse stack on end calls.
 *
 * @param type The parse call type.
 * @param argc The number of arugments.
 * @param args The arguments.
 */
void YMLLoader::parseCall(const call_t type, vector<string> &args)
{
    switch (type) {

            // Network
        case eStartNetwork: {
            YMLNetwork *net = factory->createNetwork(args[0], args[1]);
            net->setArgs(args);

            if (!root) {
                root = net;
                root->setTraceContext(rootTraceContext);

            } else {
                YMLNetwork *parent = topNetwork();

                TraceContext *traceContext = parent->getTraceContext();

                if (traceContext) {
                    TraceContext *childContext =
                        traceContext->getChildContext(net->getName());

                    if (childContext) {
                        net->setTraceContext(childContext);
                    }

                    TraceChannel *childChannel =
                        traceContext->getChildChannel(net->getName());

                    if (childChannel) {
                        net->setTraceChannel(childChannel);
                    }

                    //else cerr << "WARNING No trace context for '" <<
                    //       net->getFullName() << "'." << endl;
                }
            }

            entity_stack.push(net);
            break;
        }

        case eEndNetwork: {
            // Get YMLNetwork
            YMLNetwork *net = topNetwork();
            entity_stack.pop();

            // Check for parent Network
            if (!entity_stack.empty()) {

                TraceContext *traceContext = topNetwork()->getTraceContext();

                if (traceContext) {
                    const string linkid = net->findProperty("linkid");

                    if (!linkid.empty()) {
                        int id = traceContext->getCommChannelID(linkid);
                        net->setChannelID(id);
                    }
                }

                topNetwork()->addNode(net);
            }

            net->connect();
            net->init();
            break;
        }


        // Node
        case eStartNode: {
            YMLNode *node = factory->createNode(args[0], args[1]);
            node->setArgs(args);

            YMLNetwork *parent = topNetwork();
            TraceContext *traceContext = parent->getTraceContext();

            if (traceContext) {
                TraceChannel *childChannel =
                    traceContext->getChildChannel(node->getName());

                if (childChannel) {
                    node->setTraceChannel(childChannel);
                }
            }

            entity_stack.push(node);
            break;
        }

        case eEndNode: {
            YMLNode *node = topNode();
            entity_stack.pop();

            TraceContext *traceContext = topNetwork()->getTraceContext();

            if (traceContext) {
                const string linkid = node->findProperty("linkid");

                if (!linkid.empty()) {
                    int id = traceContext->getCommChannelID(linkid);
                    node->setChannelID(id);
                    //cout << node->getName() << " set vchannel id = " << id
                    //     << " with linkid = " << linkid << endl;
                }
            }

            // Get parent Network
            topNetwork()->addNode(node);
            node->init();
            break;
        }


        // Link
        case eStartLink: {
            YMLLink *link = factory->createLink(args[0], args[1], args[2], args[3]);
            link->setArgs(args);

            YMLNetwork *parent = topNetwork();
            TraceContext *traceContext = parent->getTraceContext();

            if (traceContext) {
                string name = string(args[0]) + "." + args[1] + "->" +
                              args[2] + "." + args[3];

                int id = traceContext->getCommChannelID(name.c_str());
                //cout << "Communication Channel id set to " << id << endl;
                link->setCommChannelID(id);
            }

            entity_stack.push(link);
            break;
        }

        case eEndLink: {
            YMLLink *link = topLink();
            entity_stack.pop();

            topNetwork()->addLink(link);
            break;
        }


        // Port
        case eStartPort: {
            YMLPort *port = factory->createPort(args[0]);
            port->setArgs(args);
            entity_stack.push(port);
            break;
        }

        case eEndPort: {
            YMLPort *port = topPort();
            entity_stack.pop();

            YMLNode *parent = topNode();

            parent->addPort(port);
            break;
        }


        // Property
        case eStartProp: {
            YMLEntity *entity = topEntity();
            entity->addProperty(args[0], args[1]);
            break;
        }

        case eEndProp:
            break;


            // Doc
        case eDoc: {
            YMLEntity *entity = topEntity();
            entity->setDoc(args[0]);
            break;
        }

        default:
            THROW("Unsupported parse call");
    }
}


YMLNetwork *YMLLoader::topNetwork()
{
    YMLEntity *entity = entity_stack.top();
    ASSERT_OR_THROW("NULL entity!", entity);
    ASSERT_OR_THROW("Expected yml network!", entity->getType() == ymlNetwork);

    return (YMLNetwork *)entity;
}

YMLNode *YMLLoader::topNode()
{
    YMLEntity *entity = entity_stack.top();
    ASSERT_OR_THROW("NULL entity!", entity);
    ASSERT_OR_THROW("Expected yml node!", entity->getType() == ymlNode ||
                    entity->getType() == ymlNetwork);

    return (YMLNode *)entity;
}

YMLLink *YMLLoader::topLink()
{
    YMLEntity *entity = entity_stack.top();
    ASSERT_OR_THROW("NULL entity!", entity);
    ASSERT_OR_THROW("Expected yml link!", entity->getType() == ymlLink);

    return (YMLLink *)entity;
}

YMLPort *YMLLoader::topPort()
{
    YMLEntity *entity = entity_stack.top();
    ASSERT_OR_THROW("NULL entity!", entity);
    ASSERT_OR_THROW("Expected yml port!", entity->getType() == ymlPort);

    return (YMLPort *)entity;
}

YMLEntity *YMLLoader::topEntity()
{
    YMLEntity *entity = entity_stack.top();
    ASSERT_OR_THROW("NULL entity!", entity);
    return entity;
}
