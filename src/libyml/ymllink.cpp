/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymllink.h"
#include "ymldefs.h"
#include "ymlnode.h"

#include "../BasicUtils//BasicException.h"
#include "../XMLCereal/XMLSerializer.h"

#include "../libbasic/basicutilfuncts.h"

#include <sstream>
#include <iostream>
#include <string>

using namespace std;

/**
 * Construct a YMLLink.
 *
 */
YMLLink::YMLLink() : commChannelID(-1)
{
    portSides[0] = portSides[1] = 0;
}

/**
* Set a YMLPortSide.
*
* @param i The side to set.
* @param portSide A pointer to the YMLPortSide.
*/
void YMLLink::setPortSide(const unsigned int i, YMLPortSide *portSide)
{
    ASSERT_OR_THROW("Out of range!", i < 2);
    portSides[i] = portSide;
}

/**
* Get a YMLPortSide.
*
* @param i The side to get.
* @return  A pointer to the YMLPortSide.
*/
YMLPortSide *YMLLink::getPortSide(const unsigned int i)
{
    ASSERT_OR_THROW("Out of range!", i < 2);
    return portSides[i];
}

/**
 * Get the name of one of the two nodes this link is connected to.
 *
 * @param i The node number.  Either 0 or 1.
 *
 * @return The name of the node.
 */
const string YMLLink::getNodeName(const unsigned int i)
{
    ASSERT_OR_THROW("Invalid range!", i < 2);
    return getArg(i * 2);
}

/**
 * Get the name of one of the two ports this link is connected to.
 *
 * @param i The port number.  Either 0 or 1.
 *
 * @return The name of the port.
 */
const string YMLLink::getPortName(const unsigned int i)
{
    ASSERT_OR_THROW("Invalid range!", i < 2);
    return getArg(i * 2 + 1);
}

/**
 * Print this link as a GraphViz graph.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 */
void YMLLink::dumpDOT(std::ostream &stream, int tabLevel)
{
    tabs(stream, tabLevel);
    ASSERT_OR_THROW("NULL port side!", portSides[0] && portSides[1]);
    YMLNode *node1 = portSides[0]->getPort()->getParent();
    YMLNode *node2 = portSides[1]->getPort()->getParent();
    ASSERT_OR_THROW("NULL node!", node1 && node2);

    ostringstream fullName1;
    node1->dumpFullName(fullName1);
    fullName1 << ends;
    dumpCleanCString(fullName1.str().c_str(), stream);

    stream << " -> ";

    ostringstream fullName2;
    node2->dumpFullName(fullName2);
    fullName2 << ends;
    dumpCleanCString(fullName2.str().c_str(), stream);

    stream << " [dir=forward]";
}

/**
 * Print this link as XML.
 *
 * @param stream The output stream.
 * @param tabLevel The number of tabs to use.
 */
void YMLLink::dumpXML(XMLSerializer &stream)
{
    XMLSerializer::attributes_t attrs;
    attrs[YML_ATTR_OBJECT1] = getNodeName(0);
    attrs[YML_ATTR_PORT1] = getPortName(0);
    attrs[YML_ATTR_OBJECT2] = getNodeName(1);
    attrs[YML_ATTR_PORT2] = getPortName(1);

    stream.startElement(YML_ELEM_LINK, attrs);
    dumpXMLProperties(stream);
    stream.endElement();
}
