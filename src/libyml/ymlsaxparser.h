/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlsaxparser.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:02:07 2003
 *
 * @brief  The YML Xerces SAX2 parser
 *
 * The class is SAX2 parser handler for yml files.  The normal way
 * to use this class is to specialized it with specific implementations
 * of either the separate start and end yml entity functions or
 * just overload parseCall itself.
 */

#ifndef YMLSAXPARSER_H
#define YMLSAXPARSER_H

#include "parsecall.h"
#include <list>
#include <string>

#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>

class Interpreter;

class YMLSAXParser : public XERCES_CPP_NAMESPACE::DefaultHandler
{
    public:

        /// The script interpreter.
        Interpreter *interpreter;

        /// The last parsed text.
        std::string text;

        /// How many sets we are deep.
        int setDepth;

        typedef std::list<ParseCall *> parseCalls_t;
        /// Parse calls used to repeat sets.
        parseCalls_t parseCalls;


    public:
        YMLSAXParser();
        virtual ~YMLSAXParser();

        /**
         * Set the script interpreter.
         * This intrepreter is used for the yml scripting features.  Any
         * class which correctly implements the Intrepreter interface may be used.
         *
         * @param interpreter A pointer to the intrepreter.
         */
        void setInterpreter(Interpreter *interpreter) {
            this->interpreter = interpreter;
        }

        // YMLSAXParser Interface
        virtual void startNetwork(const std::string /*name*/, const std::string /*className*/) {}
        virtual void endNetwork() {}
        virtual void startNode(const std::string /*name*/, const std::string /*className*/) {}
        virtual void endNode() {}
        virtual void startLink(const std::string /*object1*/, const std::string /*port1*/,
                               const std::string /*object2*/, const std::string /*port2*/) {}
        virtual void endLink() {}
        virtual void startPort(const std::string /*name*/, const std::string /*dir*/) {}
        virtual void endPort() {}
        virtual void startProperty(const std::string /*name*/, const std::string /*value*/) {}
        virtual void endProperty() {}
        virtual void doc(const std::string /*data*/) {}
        virtual void parseCall(const call_t type, std::vector<std::string> &args);

        // YMLSAXParser pre-processing functions
        void executeSet(parseCalls_t::iterator start);
        void startSet(const std::string loop, const std::string init, const std::string cond);
        void endSet();
        void script(const std::string data);
        const std:: string lookup(const std::string);
        void lookupArgs(const std::vector<std::string> &args, std::vector<std::string> &pargs);
        void localParseCall(const call_t type, const std::vector<std::string> &args);
        void dispatch(const call_t type, std::vector<std::string> &args);
        void clearText() {
            text.clear();
        }

        // Begin SAX2 interface
        void startDocument();
        void endDocument();
        void startElement(const XMLCh *const uri, const XMLCh *const localname,
                          const XMLCh *const qname,
                          const XERCES_CPP_NAMESPACE::Attributes &attributes);
        void endElement(const XMLCh *const uri, const XMLCh *const localname,
                        const XMLCh *const qname);
        void characters(const XMLCh *const chars, const XMLSize_t length);

    protected:
        void clearParseCalls();
};
#endif

