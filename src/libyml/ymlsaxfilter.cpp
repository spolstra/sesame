/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "ymlsaxfilter.h"
#include "ymldefs.h"
#include <iostream>

using namespace std;

#ifdef XERCES_HAS_CPP_NAMESPACE
XERCES_CPP_NAMESPACE_USE;
#endif

YMLSAXFilter::YMLSAXFilter(SAX2XMLReader *parent) : SAX2XMLFilterImpl(parent),
    property_count(0)

{
}

void YMLSAXFilter::startElement(const   XMLCh *const    uri,
                                const   XMLCh *const    localname,
                                const   XMLCh *const    qname,
                                const   Attributes         &attributes)
{
    bool is_prop = false;
    char *name = XMLString::transcode(localname);

    // This code causes the parser to ignore all the parseCalls for
    // elements inside a property.
    // Counts nesting and only handle the outmost property.
    if (strcmp(name, YML_ELEM_PROPERTY) == 0) {
        is_prop = true;
        property_count++;
    }

    XMLString::release(&name);

    // Ignore everything until we're in the outermost nested property,
    // that one should be handled as usual.
    if (property_count > 1) { // not in outermost property so do nothing
        return;
    }

    if (property_count == 1 && !is_prop) { // ignore everything expect first
        // start prop.
        return;
    }

    SAX2XMLFilterImpl::startElement(uri, localname, qname, attributes);
}



void YMLSAXFilter::endElement(const XMLCh *const uri,
                              const XMLCh *const localname,
                              const XMLCh *const qname)
{
    char *name = XMLString::transcode(localname);

    if (strcmp(name, YML_ELEM_PROPERTY) == 0) {
        property_count--;
    }

    XMLString::release(&name);

    if (property_count >= 1) {
        return;
    }

    SAX2XMLFilterImpl::endElement(uri, localname, qname);
}
