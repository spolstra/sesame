/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   interpreter.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:21:55 2003
 *
 * @brief  Intrepreter interface definition.
 *
 * The Intrepreter interface is used to alow users to used an scripting
 * engine which can provide the required functionality for YML scripting.
 */

#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <string>

class Interpreter
{
    public:

        virtual ~Interpreter() {}

        /**
         * Stop the intrepreter and do and necessary cleanup.
         */
        virtual void stop() = 0;
        /**
         * Define a variable with in the intrepreter.
         *
         * @param var Variable name.
         * @param value Variable value.
         *
         * @return A void pointer to the value structure.
         */
        virtual void *define(const std::string var, const std::string value) = 0;
        /**
         * Check if a variable is defined in the intreperter with in the current
         * context.
         *
         * @param var The name of the variable.
         *
         * @return true if var is defined, false otherwise.
         */
        virtual bool defined(const std::string var) = 0;
        /**
         * Convert a value to an integer.
         *
         * @param value A pointer to the value to be converted.
         *
         * @return The integer result.
         */
        virtual int toint(void *value) = 0;
        /**
         * Evaluage a line of script.
         *
         * @param script The script.
         *
         * @return A void pointer to the return value.
         */
        virtual void *eval(const std::string script) = 0;
        /**
         * Convert a value pointer to a string.
         *
         * @param value The value pointer.
         *
         * @return The string value of the value.
         */
        virtual const std::string tostring(void *value) = 0;
        /**
         * Start the intrepreter and do any necessary initialization.
         */
        virtual void start() = 0;
        /**
         * Convert a value pointer to a double.
         *
         * @param value The value pointer.
         *
         * @return The value converted to a double.
         */
        virtual double todouble(void *value) = 0;
        /**
         * Convert a value pointer to a bool.
         *
         * @param value The value pointer.
         *
         * @return The boolean value
         */
        virtual bool tobool(void *value) = 0;
        /**
         * Create a new scope level in the intrepreter.
         * It is not exactly defined how this should effect scripting.
         * This function is called each time a node or network is entered.
         */
        virtual void upscope() = 0;
        /**
         * Move down a scope level in the intrepreter.
         * It is not exactly defined how this should effect scripting.
         * This function is called each time a node or network is exited.
         */
        virtual void downscope() = 0;
};
#endif
