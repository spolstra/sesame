/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlconnection.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:08:44 2003
 *
 * @brief  YMLConnection class definition.
 *
 * YMLConnections are direct connections between nodes unlike YMLLinks
 * which may pass through intermediate ports.  YMLConnections are resolved
 * in a second pass through the data structure by the loader after parsing.
 */

#ifndef YMLCONNECTION_H
#define YMLCONNECTION_H

#include "ymllink.h"

// YMLConnection
class YMLConnection: public YMLLink
{
    public:
        /**
         * Get the YMLEntity type.
         *
         * @return ymlConnection.
         */
        virtual ymlentity_t getType() {
            return ymlConnection;
        }
};

#endif // YMLCONNECTION_H
