/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * @file   ymlloader.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 18:37:45 2003
 *
 * @brief  YMLLoader class definition.
 *
 * The YMLLoader is an instanceof of YMLSAXParser which simply creates the
 * YML data structure.  This is simalar to the DOM parser in XML.
 */

#ifndef YMLLOADER_H
#define YMLLOADER_H

#include "ymlsaxparser.h"

#include <unistd.h>
#include <fstream>
#include <list>
#include <stack>
#include <string>
#include <vector>

// Forward Declarations
class Interpreter;
class YMLEntityFactory;
class YMLEntity;
class YMLNetwork;
class YMLNode;
class YMLPort;
class YMLLink;
class TraceContext;

namespace XERCES_CPP_NAMESPACE
{
    class InputSource;
};

class YMLLoader : public YMLSAXParser
{
        // Attributes
        /// The YMLEntityFactory used to create YML objects.
        YMLEntityFactory *factory;
        /// The script Intrepreter to use with the YML.
        Interpreter *interpreter;

        typedef std::stack<YMLEntity *> entity_stack_t;
        /// The YML entity parse stack.
        entity_stack_t entity_stack;

        /// A pointer to the root YMLNetwork
        YMLNetwork *root;

        TraceContext *rootTraceContext;

        // Operations
    public:
        YMLLoader(YMLEntityFactory *factory, Interpreter *interpreter);
        virtual ~YMLLoader();

        virtual void parseCall(const call_t type, std::vector<std::string> &args);

        YMLNetwork *parse(std::istream &stream, TraceContext *traceContext = 0);
        YMLNetwork *parse(const std::string filename, TraceContext *traceContext = 0);
        YMLNetwork *parse(XERCES_CPP_NAMESPACE::InputSource *input,
                          TraceContext *traceContext = 0);

    protected:
        YMLNetwork *topNetwork();
        YMLNode *topNode();
        YMLLink *topLink();
        YMLPort *topPort();
        YMLEntity *topEntity();
};

#endif
