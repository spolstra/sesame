/*******************************************************************\

                 Copyright (C) 2003 Joseph Coffland

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
        of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
             GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
                           02111-1307, USA.

            For information regarding this software email
                   jcofflan@users.sourceforge.net

\*******************************************************************/

#include "BasicErrorHandler.h"

#include "XercesStr.h"

#include <xercesc/sax/SAXParseException.hpp>
XERCES_CPP_NAMESPACE_USE;

#include "../BasicUtils/BasicException.h"

#include <string>
using namespace std;

void throwError(const string prefix, const SAXParseException &e)
{
    XercesStr message(e.getMessage());
    XercesStr fileName(e.getSystemId());

    throw BasicException(prefix + message.localForm(),
                         BasicFileLocation(e.getSystemId() ?
                                           XercesStr(e.getSystemId()).localForm() :
                                           "In Unknown File",
                                           e.getLineNumber(),
                                           e.getColumnNumber()));
}

// Begin Xerces ErrorHandler interface
void BasicErrorHandler::warning(const SAXParseException &e)
{
    throwError(prefix + " Warning: ", e);
}

void BasicErrorHandler::error(const SAXParseException &e)
{
    throwError(prefix + " Error: ", e);
}

void BasicErrorHandler::fatalError(const SAXParseException &e)
{
    throwError(prefix + "Fatal Error: ", e);
}
// End Xerces ErrorHandler interface

