/*******************************************************************\

              Copyright (C) 2002, 2003 Joseph Coffland

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
        of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
             GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
                           02111-1307, USA.

            For information regarding this software email
                   jcofflan@users.sourceforge.net

\*******************************************************************/
/**
 * @file   StreamBinInputStream.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:12:27 2003
 *
 * @brief  StreamBinInputStream class definition
 *
 * This class specialized the XercesC BinInputStream to allow XML
 * parsing from a C++ istream.
 */

#ifndef STREAMBININPUTSTREAM
#define STREAMBININPUTSTREAM

#include <xercesc/util/BinInputStream.hpp>
#include <iostream>

/**
 * A Xerces C++ BinInputStream for iostreams.
 */
class StreamBinInputStream : public XERCES_CPP_NAMESPACE::BinInputStream
{
        /// The input stream.
        std::istream *stream;
    public:

        StreamBinInputStream(std::istream &stream);

        virtual XMLFilePos curPos() const;
        virtual XMLSize_t readBytes(XMLByte *const toFill,
                                    const XMLSize_t maxToRead);

        virtual const XMLCh *getContentType() const;
};

#endif // STREAMBININPUTSTREAM

