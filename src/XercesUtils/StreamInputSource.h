/*******************************************************************\

        Copyright (C) 2002, 2003 Joseph Coffland

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

            For information regarding this software email
             jcofflan@users.sourceforge.net

\*******************************************************************/
/**
 * @file   StreamInputSource.h
 * @author Joseph Coffland
 * @date   Sat Feb 22 19:10:59 2003
 *
 * @brief  StreamInputSource class definition
 *
 * This class extends the XercesC InputSource class to provide use
 * a C++ istream as input to the XercesC parser.
 */

#ifndef STREAMINPUTSOURCE
#define STREAMINPUTSOURCE

#include <xercesc/sax/InputSource.hpp>
#include <iostream>

/**
 * A Xerces C++ InputSource for iostreams.
 */
class StreamInputSource: public XERCES_CPP_NAMESPACE::InputSource
{
        /// The input stream.
        std::istream *stream;

    public:
        StreamInputSource(std::istream &stream);

        XERCES_CPP_NAMESPACE::BinInputStream *makeStream() const;
};

#endif // STREAMINPUTSOURCE
