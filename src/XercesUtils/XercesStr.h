#ifndef XERCESSTR_H
#define XERCESSTR_H

#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/framework/XMLFormatter.hpp>
#include <iostream>
#include <string>

/**
 * Convert to and from Xerces C++ UniCode format.
 *
 * Included are two macros XStrL and XStrG which convert
 * strings to their local and UniCode forms respectively.
 */
class XercesStr
{
        /// The local converted format.
        char *local;

        /// The Xerces C++ UniCode format.
        XMLCh *generic;

    public:
        XercesStr(const XMLCh *const str);
        XercesStr(const std::string str);
        XercesStr(const char *str);
        ~XercesStr();

        /// Access the local string.
        const char *localForm() const;

        /// Access the UniCode string.
        const XMLCh *genericForm() const;

        /// Convert from Unicode to local format with length.
        static void convert(const XMLCh *const src, std::string &dest,
                            const unsigned int length);

        /// Convert from UniCode to local format.
        static void convert(const XMLCh *const src, std::string &dest);
};

/// Print a XercesStr to a stream.
inline std::ostream &operator<<(std::ostream &stream, const XercesStr &s)
{
    stream << s.localForm();
    return stream;
}

/// Print a XercesStr to a Xerces C++ XMLFormatter.
inline XERCES_CPP_NAMESPACE::XMLFormatter &
operator<<(XERCES_CPP_NAMESPACE::XMLFormatter &formatter,
           const XercesStr &s)
{

    formatter << s.genericForm();
    return formatter;
}

#define XStrL(x) XercesStr(x).localForm()
#define XStrG(x) XercesStr(x).genericForm()

#endif // XERCESSTR_H
