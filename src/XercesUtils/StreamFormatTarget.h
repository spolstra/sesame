/*******************************************************************\

              Copyright (C) 2003 Joseph Coffland

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
        of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
             GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
                           02111-1307, USA.

            For information regarding this software email
                   jcofflan@users.sourceforge.net

\*******************************************************************/

#ifndef STREAMFORMATTARGET_H
#define STREAMFORMATTARGET_H

#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/framework/XMLFormatter.hpp>

namespace XERCES_CPP_NAMESPACE
{
    class XMLFormatter;
};


#include <iostream>

/**
 * A Xerces C++ XMLFormatTarget for iostreams.
 */
class StreamFormatTarget : public XERCES_CPP_NAMESPACE::XMLFormatTarget
{
        std::ostream *out;

    public:
        StreamFormatTarget(std::ostream *out) : out(out) {}

        // Begin XMLFormatTarget interface
        void writeChars(const XMLByte *const toWrite,
                        const XMLSize_t count,
                        XERCES_CPP_NAMESPACE::XMLFormatter *const) {
            out->write((char *)toWrite, (int)count);
        }

        void flush() {
            out->flush();
        }
        // End XMLFormatTarget interface
};

#endif // STREAMFORMATTARGET_H
