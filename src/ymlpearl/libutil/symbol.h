/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * Symbol table structures
 *
 * Uses flexible typing.
 * The symboltable contains records with several fields, the size
 * of the record must be given at the creation of the new symboltable
 * addelement adds an element to the symboltable, it has two parameters,
 * the symboltable and a pointer to the record.
 * searchelement returns a number of the record with a matching name.
 * getstruct gives the structure given a integer
 *
 * Stolen from pl
 */

#ifndef SYMBOL_H

#define SYMBOL_H

typedef struct {
    int len ;
    int fill ;
    int size ;
    char **cont ;
} symboltable ;

#define getstruct( a,num )    (a->cont[num])
#define setstruct( a,num )    (getval(a,num))
#define tablelength( a )    ( a->fill )

symboltable *newsymboltable() ;
int addsymbol() ;
int searchsymbol() ;

#endif // SYMBOL_H
