/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * Universal Symbol table handler
 * Stolen from pl
 */

#include "symbol.h"
#include "error.h"
#include "../comp/lex_an.h"

#include <string.h>
#include <stdlib.h>

/*
 * The entry below is for robustness, it only cheks for the validity
 * of the call to newsymboltable
 */
#define MAXENTRYSIZE  128

symboltable *newsymboltable(int size)
{
    symboltable *a ;

    if ((size & 3) != 0 || size < 4 || size > MAXENTRYSIZE) {
        fatal("Symboltable of size %d allocated (adjust MAXENTRYSIZE)\n", size) ;
    }

    a = (symboltable *)malloc(sizeof(symboltable));
    a -> fill = 0 ;
    a -> len = 2 ;
    a -> size = size ;
    a -> cont = (char **)malloc(sizeof(char *) * a->len) ;

    return a ;
}

static int place ;
int addsymbol(register symboltable *a, register int *str)
{
    register int i, *d, *s ;

    if (**((char **)str) != '#') {   /* Compiler symbols start with '#' */
        if (searchsymbol(a, *((char **)str)) != -1) {
            error("identifier '%s' defined more than once\n", *str) ;
            return -1;
        }
    }

    if (a->len == a->fill) {
        a->len += 10 ;
        a->cont = (char **)realloc(a->cont, (sizeof(char *)) * a->len);
    }

    memcpy(a->cont + place + 1, a->cont + place, sizeof(int *) * (a->fill - place)) ;
    a->fill++ ;
    a->cont[ place ] = (char *)malloc(a->size) ;
    s = (int *) str ;
    d = (int *) a->cont[ place ] ;

    if (d == 0L) {
        fatal("Symboltable overflow ?\n") ;
    }

    for (i = 0 ; i < a->size ; i += 4) {
        *d++ = *s++ ;
    }

    return place ;
}

int searchsymbol(symboltable *a, char *str)
{
    int i = 0, x = 0 ;
    int low = 0 ;
    int high = a->fill - 1 ;

    while (low <= high) {
        i = (low + high) / 2 ;
        x = strcmp(*(char **)a->cont[i] , str) ;

        if (x == 0) {
            return i ;
        }

        if (x < 0) {
            low  = i + 1 ;
        } else {
            high = i - 1 ;
        }
    }

    place = i ;

    if (x < 0) {
        place++ ;
    }

    return -1 ;
}

void symbolcut(symboltable *a, int i)
{
    a->fill = i ;
}
