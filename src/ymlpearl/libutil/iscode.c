/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdarg.h>
#include <utime.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "error.h"

#define MODIFIED  (-1)
#define BUFSIZE   (1024)
#define LEN 256

static char *sourcefile ;
static int original ;

struct myfile {
    char name[ LEN ] ;
    int fd ;
    char *buf ;
    int len ;
    int statelen ;
    int open ;
} implfile, specfile ;

static void closefile(struct myfile *f)
{
    struct utimbuf times;

    if (f->statelen != MODIFIED) {
        if (f->statelen != f->len) {
            if (ftruncate(f->fd , f->statelen) == -1) {
                /* TODO: Handle error */
            }

            f->statelen = MODIFIED ;
        } else {
            if (sourcefile) {
                time(&(times.actime)) ;       /* Set time of source back. */
                time(&(times.modtime)) ;
                times.modtime -= 60;          /* One minute earlier */
                utime(sourcefile, &times) ;
            }
        }
    } else {
        /* Flush */
        if (write(f->fd, f->buf, f->len) == -1) {
            /* TODO: Handle error */
        }

        if (ftruncate(f->fd , lseek(f->fd, 0L, SEEK_CUR)) == -1) {
            /* TODO: Handle error */
        }
    }

    close(f->fd) ;
    free(f->buf) ;
    f->fd = -1 ;
    f->open = 0 ;
}

static int iscodeexit()
{
    closefile(&implfile) ;
    closefile(&specfile) ;
    return 0;
}

static void openfile(struct myfile *f, char *name, char *ext)
{
    struct stat sbuf ;
    static int first = 1 ;

    if (first) {
        first = 0 ;
        onerror(iscodeexit) ;
    }

    strcpy(f->name, name) ;
    strcat(f->name, ext) ;
    f->fd = open(f->name, O_RDWR | O_CREAT, 0644) ;

    if (f->fd < 0) {
        perror("open") ;
        fatal("Cannot open %s for writing", f->name) ;
    }

    fstat(f->fd, &sbuf) ;
    original = sbuf.st_mtime ;
    f->len = sbuf.st_size ;
    f->buf = malloc(f->len) ;

    if (read(f->fd, f->buf, f->len) != f->len) {
        fatal("File changed len") ;
    }

    f->statelen = 0 ;
    f->open = 1 ;
}

static void writefile(struct myfile *f, char *buf, int n)
{
    if (f->statelen != MODIFIED) {
        if (n > f->len - f->statelen || memcmp(f->buf + f->statelen, buf, n)) {
            if (ftruncate(f->fd , f->statelen) == -1) {
                /* TODO: Handle error */
            }

            lseek(f->fd , f->statelen, SEEK_SET) ;
            f->statelen = MODIFIED ;
            free(f->buf) ;
            f->buf = malloc(BUFSIZE) ;
            f->len = 0 ;
        } else {
            f->statelen += n ;
            return ;
        }
    }

    if (n + f->len > BUFSIZE) {
        if (write(f->fd, f->buf, f->len) == -1) {
            /* TODO: Handle error */
        }

        f->len = 0 ;
    }

    if (n > BUFSIZE) {
        if (write(f->fd, buf, n) == -1) {
            /* TODO: Handle error */
        }
    } else {
        memcpy(f->buf + f->len, buf, n) ;
        f->len += n ;
    }
}

void startcode(char *name, char *source)
{
    if (implfile.open) {
        closefile(&implfile) ;
        closefile(&specfile) ;

        if (name == NULL) {
            return ;
        }
    }

    sourcefile = source ;
    openfile(&implfile, name, ".pi") ;
    openfile(&specfile, name, ".ps") ;
}

static char buf[ BUFSIZE ] ;

void iscode(char *fmt, ...)
{
    va_list args ;

    va_start(args, fmt) ;
    fmt = va_arg(args, char *) ;
    vsprintf(buf, fmt, args) ;
    va_end(args) ;
    writefile(&specfile, buf, strlen(buf)) ;
    writefile(&implfile, buf, strlen(buf)) ;
}

void icode(char *fmt, ...)
{
    va_list args ;

    va_start(args, fmt) ;
    fmt = va_arg(args, char *) ;
    vsprintf(buf, fmt, args) ;
    va_end(args) ;
    writefile(&implfile, buf, strlen(buf)) ;
}

void scode(char *fmt, ...)
{
    va_list args ;

    va_start(args, fmt) ;
    fmt = va_arg(args, char *) ;
    vsprintf(buf, fmt, args) ;
    va_end(args) ;
    writefile(&specfile, buf, strlen(buf)) ;
}
