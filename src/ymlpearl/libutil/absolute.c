/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file absolute.c
 * @author Joseph Coffland
 *
 * @brief Makes the pathname absolute with respect to the pearl directory.
 *
 * Makes pathnames machine indpenpendent by allowing '#' in the pathname
 * which is replaced by the machine type.
 *
 * Examples:
 *   makepath( "-I", "include" , NULL  ) -->  "-I/usr/henkm/pearl/include"
 *   makepath( "", "bin.#/prog" , NULL ) --> "/usr/henkm/pearl/bin.mach/prog"
 * Third parameter is optional output, NULL -> memory is allocated.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "absolute.h"

static char *pearlpath = NULL; //!< Root dir of the Sesame installation.
static char *libpath = NULL;   //!< Library dir of the Sesame installation.


#ifdef hpux
#define MACHTYPE        "HP-UX"

#elif mc88k
#define MACHTYPE       "mc88000"

#elif sparc
#define MACHTYPE       "sparc"

#elif mc68020
#define MACHTYPE       "mc68020"

#else
#define MACHTYPE  "linux"
#endif

/**
 * Set the root directory #pearlpath of the Sesame installation.
 * Use the `PEARLDIR` environment variable if set, otherwise use @p path.
 *
 * @param path  Root directory of the Sesame installation.
 */
void initpearlpath(char *path)
{
    pearlpath = getenv("PEARLDIR") ;

    if (pearlpath == NULL) {
        pearlpath = strdup(path);
    }
}

/**
 * Set the library directory #libpath of the Sesame installation.
 * Use the `PEARLPATH` environment variable if set, otherwise use @p path.
 *
 * @param path  Library directory of the Sesame installation.
 */
void initlibpath(char *path)
{
    libpath = getenv("PEARLPATH") ;

    if (libpath == NULL) {
        libpath = strdup(path);
    }
}

/**
 * Construct a path: @p pre + #pearlpath + @p post. Substitute
 * #MACHTYPE (machine type) for `#` in @p post.
 * Copy the result into @p output.
 * If @p output is NULL, allocate memory first.
 *
 * @param pre    prefix path
 * @param post   postfix path, with `#` for machine type.
 * @param output Buffer with resulting path.
 * @return constructed path @p output.
 */
char *makepath(char *pre, char *post, char *output)
{
    char *x ;

    if (pearlpath == NULL) {
        initpearlpath(NULL) ;
    }

    while (*post == '/') {
        post++ ;
    }

    if (output == NULL) {
        output = malloc(strlen(pre) + strlen(post) + strlen(pearlpath) + 3 + 8);
    }

    // concat pre and pearlpath
    strcpy(output, pre) ;
    strcat(output, pearlpath) ;
    strcat(output, "/") ;
    x = output + strlen(output) ;

    // substute MACHTYPE for #
    do {
        if (*post == '#') {
            strcpy(x, MACHTYPE) ;
            x += strlen(MACHTYPE) ;
        } else {
            *x++ = *post ;
        }
    } while (*post++) ;

    return output ;
}
