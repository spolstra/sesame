/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

typedef int (*function)() ;
typedef struct functionlx {
    function f ;
    struct functionlx *next ;
} functionlx ;

static functionlx *head = NULL ;

void onerror(int (*f)())
{
    functionlx *x ;

    x = (functionlx *)malloc(sizeof(functionlx));
    x->f = f ;
    x->next = head ;
    head = x ;
}

void fatal(char *fmt, ...)
{
    va_list argp ;

    fprintf(stderr, "error: ");
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    fprintf(stderr, "\n");

    while (head) {
        (*head->f)() ;
        head = head->next ;
    }

    exit(1) ;
}

/* VARARGS 1 */
/*
void error(char *fmt, ...)
{
  va_list args ;

  va_start(args, fmt) ;
  fmt = va_arg(args, char *) ;
  fprintf(stderr, "Error: ") ;
  vfprintf(stderr, fmt, args) ;
  va_end(args) ;

  while (head) {
    (*head->f)() ;
    head = head->next ;
  }

  exit(0) ;
}
*/

/* VARARGS 1 */
/*
void panic(char *fmt, ...)
{
  va_list args ;

  va_start(args, fmt) ;
  fmt = va_arg(args, char *) ;
  fprintf(stderr, "Panic : ") ;
  vfprintf(stderr, fmt, args) ;
  va_end(args) ;
  abort() ;
}
*/
