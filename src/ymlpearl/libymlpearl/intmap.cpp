/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "intmap.h"

#include <map>
#include <vector>
using namespace std;

#include "../libsim/misc.h"

typedef map<int, int> intmap_t;
typedef vector<intmap_t> intmaps_t;

static intmaps_t intmaps(1);

unsigned int intmap_create()
{
    int idx = intmaps.size();
    intmaps.resize(intmaps.size() + 1);

    return idx;
}

void intmap_validate(unsigned int id)
{
    if (intmaps.size() <= id) {
        panic((char *)"invalid intmap id = %u", id);
    }
}

void intmap_put(unsigned int id, int key, int value)
{
    intmap_validate(id);

    intmaps[id][key] = value;
}

int intmap_get(unsigned int id, int key)
{
    intmap_validate(id);

    intmap_t::iterator it = intmaps[id].find(key);

    if (it == intmaps[id].end()) {
        panic((char *)"invalid intmap key = %u", key);
    }

    return it->second;
}

int intmap_has(unsigned int id, int key)
{
    intmap_validate(id);

    intmap_t::iterator it = intmaps[id].find(key);

    if (it == intmaps[id].end()) {
        return 0;
    }

    return 1;
}
