/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pearlymlentityfactory.h"

#include "../../libyml/ymlconnection.h"

#include "pearlymlnode.h"
#include "pearlymlnetwork.h"

#include <string>

YMLNode *PearlYMLEntityFactory::createNode(const std::string,
                                           const std::string)
{
    return new PearlYMLNode();
}


YMLNetwork *PearlYMLEntityFactory::createNetwork(const std::string,
                                                 const std::string)
{
    return new PearlYMLNetwork();
}

YMLConnection *PearlYMLEntityFactory::createConnection(YMLPort *,
                                                       YMLPort *)
{
    return new YMLConnection();
}
