/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "traceevent.h"

#include "pearlymlnode.h"

#include "../../libymlmap/tracechannel.h"
#include "../../libymlmap/mappingobj.h"
#include "../libsim/global.h"

#include "../../BasicUtils/BasicDebugger.h"

#include <stdlib.h>

#include <iostream>
using namespace std;

int trace_table_size = 0;
trace_event_t *trace_table = 0;

#if __STDC_VERSION__ < 199901L
# if __GNUC__ >= 2
#  define __func__ __FUNCTION__
# else
#  define __func__ "unknown"
# endif
#endif

#define ASSERT(x) \
    if (!(x)) {\
        panic((char *)"%s:%d %s(): Assertion `%s' failed in process %s.\n",\
              __FILE__, __LINE__, __func__, #x, whoami());\
    }

int initTraceTable(int numJobs)
{
    int i;

    trace_table_size = numJobs;
    trace_table = (trace_event_t *)malloc(sizeof(trace_event_t) * numJobs);

    for (i = 0; i < numJobs; i++) {
        trace_table[i].type = T_NULL;
    }

    return 0;
}

PearlYMLNode *getCurrentNode()
{
    ASSERT(pearl_current);
    ASSERT(pearl_current->ymlNode);
    return (PearlYMLNode *)pearl_current->ymlNode;
}

TraceChannel *getCurrentChannel()
{
    YMLNode *node = getCurrentNode();
    ASSERT(node->getTraceChannel());
    return node->getTraceChannel();
}

int jobId()
{
    ASSERT(pearl_current);
    return (int)(pearl_current - pearl_proc);
}

int nextTrace()
{
    int job = jobId();

    if (trace_table[job].type == T_QUIT) {
        return T_QUIT;
    }

    TraceChannel *channel = getCurrentChannel();
    trace_table[job] = channel->next();
    return trace_table[job].type;
}

int traceType()
{
    return trace_table[jobId()].type;
}

int traceSize()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_READ || type == T_WRITE);
    return trace_table[jobId()].size;
}

int tracePort()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_READ || type == T_WRITE);
    return trace_table[jobId()].id;
}

int traceCommID()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_READ || type == T_WRITE);

    if (trace_table[jobId()].commid == -2)
        panic((char *)"%s\n%s\n%s\n%s",
              "The current implementation does not allow communication channel",
              "mapping of application channels with more than one link connected"
              "in series.  You can get around this limitation by first flatening"
              "the application YML.");

    if (trace_table[jobId()].commid == -1) {
        panic((char *)"communication channel not mapped!");
    }

    return trace_table[jobId()].commid;
}

int virtualChannelID()
{
    int id = getCurrentNode()->getChannelID();

    if (id == -1) {
        panic((char *)"virtual channel not mapped!");
    }

    return id;
}

int traceInstruction()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_EXECUTE);
    return trace_table[jobId()].id;
}

int traceTID()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_EXECUTE || type == T_READ || type == T_WRITE || T_QUIT);
    return trace_table[jobId()].tid;
}

int traceAnnotation()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_EXECUTE || type == T_READ || type == T_WRITE || T_QUIT);
    return trace_table[jobId()].annot;
}

int traceAnnotation2()
{
    trace_t type = (trace_t)traceType();
    ASSERT(type == T_EXECUTE || type == T_READ || type == T_WRITE || T_QUIT);
    return trace_table[jobId()].annot2;
}



int getJobId(int job)
{
    return job;
}

void stackTrace()
{
    BasicDebugger::printStackTrace(cerr);
}
