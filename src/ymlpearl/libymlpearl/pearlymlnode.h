/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PEARLYMLNODE_H
#define PEARLYMLNODE_H

/**
 * @file   pearlymlnode.h
 * @author Joseph Coffland
 * @date   Tue Jan 28 13:33:20 EST 2003
 *
 * @brief  PearlYMLNode
 *
 * This class is constructed from a YML node by the YMLLoader. It
 * is used to initialize an instance of a pearl object from its
 * YML description.
 */

#include "../../libyml/ymlnode.h"
#include <map>
#include <list>
#include <vector>
#include <string>

class PearlYMLNode : public YMLNode
{
        typedef enum {INT_T, ARRAY_T, PORT_T} parm_type;

        struct _parm_t;
        typedef std::list<_parm_t> parmList_t;

        struct _parm_t {
            parm_type type;

            union {
                int integer;
                parmList_t *array;
                int peerID;
            } data;
        };

        typedef struct _parm_t parm_t;

        std::string input;
        parmList_t *initParms;
        parmList_t *subArray;
        int dataCount;

        int jobnr;

    public:
        PearlYMLNode();
        virtual ~PearlYMLNode();

        int getJobNum() {
            return jobnr;
        }

        void finalize();

        void initProcess();

        void getPeersByType(std::string type_name, std::vector<int> &peers);

    protected:
        void parseInit();
        void parseParmList();
        void parseInteger();
        void parseArray(bool addSize);
        void parseTypeMatch(bool addSize);
        void parseRegex();
        void getRegexMatches(const std::string regex);
        void addParm(parm_t &parm);
        void addIntParm(int i);
        void addPortParm(int peerID);
        void addArrayParm(parmList_t *array);
};
#endif
