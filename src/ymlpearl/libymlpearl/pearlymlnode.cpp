/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pearlymlnode.h"

#include "../../libyml/ymlconnection.h"
#include "../../libymlmap/tracechannel.h"

#include "../../BasicUtils/BasicRegex.h"
#include "../../BasicUtils/BasicException.h"

#include <string>
#include <vector>

#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "../libsim/global.h"
#include "../libsim/class.h"

extern "C" int createchild(char *name, char *className);

using namespace std;

//#define PEARLYMLNODE_DEBUG

PearlYMLNode::PearlYMLNode() : initParms(0), subArray(0), jobnr(-1)
{
}

PearlYMLNode::~PearlYMLNode()
{
    if (initParms) {
        for (parmList_t::iterator i = initParms->begin();
             i != initParms->end(); i++) {
            if (i->type == ARRAY_T) {
                delete i->data.array;
            }
        }

        delete initParms;
    }
}

void PearlYMLNode::finalize()
{
    string fullName = getFullName();

    const string className = findProperty("class");
    ASSERT_OR_THROW(string("'") + fullName +  "' has no class property.",
                    !className.empty());

    jobnr = createchild((char *)fullName.c_str(), (char *)className.c_str());
}

void PearlYMLNode::initProcess()
{
    try {
        parseInit();
    } catch (BasicException &e) {
        if (!input.empty()) {
            THROWC(string("Error parsing init string at: '") + input + "'!", e);
        } else {
            THROWC("Error parsing init string!", e);
        }
    }

    int numParms = initParms->size();
    int msgSize = 2 + 2 * numParms + dataCount;

    intptr_t *msg = new intptr_t[msgSize];


    // Number of parameters
    msg[0] = numParms;
    msg[1] = dataCount;

    // Add parameter sizes and parameters
    int i = 2;
    int msgx = numParms + i;
    int msgp = msgx + numParms;
    parmList_t::iterator it;
    parmList_t::iterator ait;

    for (it = initParms->begin(); it != initParms->end(); it++) {

        switch ((*it).type) {
            case INT_T:
                msg[i++] = 1;
                msg[msgx++] = msgp - 2;
                msg[msgp++] = (*it).data.integer;
                break;

            case ARRAY_T:
                msg[i++] = (*it).data.array->size();
                msg[msgx++] = msgp - 2;

                for (ait = (*it).data.array->begin(); ait != (*it).data.array->end();
                     ait++) {
                    switch ((*ait).type) {
                        case INT_T:
                            msg[msgp++] = (*ait).data.integer;
                            break;

                        case PORT_T:
                            msg[msgp++] = (*ait).data.peerID;
                            break;

                        default:
                            THROW("Invalid parameter type.");
                    }
                }

                break;

            case PORT_T:
                msg[i++] = 1;
                msg[msgx++] = msgp - 2;
                msg[msgp++] = (*it).data.peerID;
                break;

            default:
                THROW("Invalid parameter type.");
        }
    }

#ifdef PEARLYMLNODE_DEBUG
    cout << "Initializing '" << getFullName() << "'." << endl;
#endif
    processx *p = pearl_current = &pearl_proc[jobnr];

    // Set ymlNode pointer
    p->ymlNode = this;

    // Call process initialization function
    p->env =

        ((intptr_t * ( *)(intptr_t *, intptr_t *))(*cllist[p->classIdx].initfunc))(p->env, msg);


}

void PearlYMLNode::addParm(parm_t &parm)
{
    if (subArray) {
        subArray->push_back(parm);
    } else {
        initParms->push_back(parm);
    }

    dataCount++;
}

void PearlYMLNode::addIntParm(int i)
{
    parm_t parm;
    parm.type = INT_T;
    parm.data.integer = i;
    addParm(parm);
#ifdef PEARLYMLNODE_DEBUG
    cout << i << ": integer" << endl;
#endif
}

void PearlYMLNode::addPortParm(int peerID)
{
    parm_t parm;
    parm.type = PORT_T;
    parm.data.peerID = peerID;
    addParm(parm);
#ifdef PEARLYMLNODE_DEBUG
    cout << ": YMLPort" << endl;
#endif
}

void PearlYMLNode::addArrayParm(parmList_t *array)
{
    parm_t parm;
    parm.type = ARRAY_T;
    parm.data.array = array;
    addParm(parm);
#ifdef PEARLYMLNODE_DEBUG
    cout << '[' << array->size() << "]: array" << endl;
#endif
}

void PearlYMLNode::parseInit()
{
    input = findProperty("init");

    size_t l = input.find(' ');

    while (l != input.npos) {
        input.erase(l, 1);
        l = input.find(' ');
    }

    dataCount = 0;
    subArray = NULL;
    initParms = new parmList_t;

    if (!input.empty()) {
        parseParmList();
    }
}

void PearlYMLNode::parseParmList()
{
    bool addSize;

    while (!input.empty()) {
        if (input[0] == '#') {
            input = input.substr(1);
            addSize = true;
            ASSERT_OR_THROW("Expected something after #!", !input.empty());
        } else {
            addSize = false;
        }

        if (isdigit(input[0]) || input[0] == '-') {
            parseInteger();

        } else if ((input[0] == '[') && !subArray) {
            parseArray(addSize);

        } else if (input[0] == '<') {
            parseTypeMatch(addSize);

        } else {
            parseRegex();
        }

        if (subArray && input[0] == ']') {
            return;
        }

        if (!subArray && input[0] == '\0') {
            return;
        }

        ASSERT_OR_THROW("Parsing parameter!", input[0] == ',');
        input = input.substr(1);
    }
}

void PearlYMLNode::parseInteger()
{
    string tmp;

    if (input[0] == '-' || isdigit(input[0])) {
        tmp.push_back(input[0]);

        for (size_t i = 1; isdigit(input[i]); i++) {
            tmp.push_back(input[i]);
        }
    }

    if (tmp.empty() || (tmp.size() == 1 && tmp[0] == '-')) {
        THROW("Parsing integer!");
    }

    addIntParm(atoi(tmp.c_str()));
    input = input.substr(tmp.size());
}

void PearlYMLNode::parseArray(bool addSize)
{
    subArray = new parmList_t;
    input = input.substr(1);

    parseParmList();
    ASSERT_OR_THROW("Expected end of array!", input[0] == ']');
    input = input.substr(1);

    parmList_t *tmpArray = subArray;
    subArray = NULL;

    if (addSize) {
        addIntParm(tmpArray->size());
    }

    addArrayParm(tmpArray);
}

void PearlYMLNode::getPeersByType(string type_name, vector<int> &peers)
{
    ASSERT_OR_THROW(string("Cannot find peers by type of unknown class '") +
                    type_name + "'", findclass(type_name.c_str()));

    int numPorts = getNumPorts();

    for (int i = 0; i < numPorts; i++) {
        YMLPort *port = getPortByIdx(i);

        if (port->getDir() == YMLPort::dOut) {
            std::vector<YMLPort *> remotePorts;
            port->getRemotePorts(&remotePorts);

            for (unsigned int j = 0; j < remotePorts.size(); j++) {
                if (!subArray && j > 0)
                    THROW(string("Too many connections to ") + port->getFullName() +
                          " for single argument");

                int peerid = ((PearlYMLNode *)remotePorts[j]->getParent())->jobnr;

                if (instanceof(peerid, type_name.c_str())) {
                    peers.push_back(peerid);
                }
            }
        }
    }
}

void PearlYMLNode::parseTypeMatch(bool addSize)
{
    bool myArray = false;

    if (!subArray) {
        subArray = new parmList_t;
        myArray = true;

    } else {
        ASSERT_OR_THROW("# not allowed inside array!", !addSize);
    }

    input = input.substr(1);
    size_t end = input.find('>');

    ASSERT_OR_THROW("Expected end of type match!", end != input.npos);

    string buf = input.substr(0, end);

    vector<int> peers;
    getPeersByType(buf, peers);

    for (unsigned int i = 0; i < peers.size(); i++) {
        addPortParm(peers[i]);
    }

    if (myArray) {
        parmList_t *tmpArray = subArray;
        subArray = 0;

        if (addSize) {
            addIntParm(tmpArray->size());
        }

        addArrayParm(tmpArray);
    }

    input = input.substr(end + 1);
}

void PearlYMLNode::parseRegex()
{
    int squareb = 0;
    int paren = 0;
    int quote = 0;
    int dquote = 0;

    size_t i = 0;

    for (i = 0;
         squareb || paren || quote || dquote || (input[i] != ',' && input[i] != ']');
         i++) {
        if (i == input.size()) {
            break;
        }

        if (input[i] == '\\' && input[i + 1] != '\0') {
            i++;
            continue;
        }

        if (!(quote || dquote)) {
            if (input[i] == ']') {
                squareb = 0;
            }

            if (input[i] == '[') {
                squareb = 1;
            }
        }

        if (!squareb) {
            if (input[i] == '"' && !quote) {
                dquote = (dquote + 1) % 2;
            }

            if (input[i] == '\'' && !dquote) {
                quote = (quote + 1) % 2;
            }

            if (!(quote || dquote)) {
                if (input[i] == '(') {
                    paren++;
                }

                if (input[i] == ')') {
                    paren--;
                }
            }
        }
    }

    ASSERT_OR_THROW("Parsing regular expression!", i != 0);

    string buf = input.substr(0, i);
    input = input.substr(i);

    getRegexMatches(buf);
}

void PearlYMLNode::getRegexMatches(const std::string regexstr)
{
#ifdef PEARLYMLNODE_DEBUG
    cout << "Regex: '" << regexstr << "'" << endl;
#endif

    BasicRegEx regex(regexstr);

    if (regex.isError())
        THROW(string("Parsing regular expression '") + regexstr + "': " +
              regex.getErrorStr());

    int matchCount = 0;
    int numPorts = getNumPorts();

    for (int i = 0; i < numPorts; i++) {
        YMLPort *port = getPortByIdx(i);

        if (port->getDir() == YMLPort::dOut && regex.isMatch(port->getName())) {
            std::vector<YMLPort *> remotePorts;
            port->getRemotePorts(&remotePorts);

#ifdef PEARLYMLNODE_DEBUG
            cerr << "Regex port = " << port->getName();
#endif

            for (unsigned int j = 0; j < remotePorts.size(); j++) {
                if (!subArray && j > 0)
                    THROW(string("Too many connections to ") + port->getFullName() +
                          " for single argument");


                addPortParm(((PearlYMLNode *)remotePorts[j]->getParent())->jobnr);
                matchCount++;
            }
        }
    }

    if (!matchCount)
        cerr << "WARNING: '" << regexstr << "' in init string of object '"
             << getFullName() << "' did not match any ports!" << endl;
}
