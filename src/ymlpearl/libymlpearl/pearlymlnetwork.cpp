/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "pearlymlnetwork.h"

#include "pearlymlnode.h"

#include "../../libymlmap/tracecontext.h"
#include "../../libymlmap/tracechannel.h"

#include <string>

#include "../../BasicUtils/BasicException.h"

using namespace std;

PearlYMLNetwork::PearlYMLNetwork() {}
PearlYMLNetwork::~PearlYMLNetwork() {}


void PearlYMLNetwork::initProcesses()
{
    int count = getNumNodes();

    for (int i = 0; i < count; i++) {
        YMLNode *node = getNode(i);

        try {
            if (node->getType() == ymlNetwork) {
                ((PearlYMLNetwork *)node)->initProcesses();

            } else if (node->getType() == ymlNode) {
                ((PearlYMLNode *)node)->initProcess();
            }

        } catch (BasicException &e) {
            THROWC(string("Initializing '") + node->getFullName() + "'!", e);
        }
    }
}
