/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>

#include "../../libyml/ymlloader.h"

#include "pearlymlentityfactory.h"
#include "pearlymlnode.h"
#include "pearlymlnetwork.h"
#include "../../libymlmap/sharedmemmapping.h"
#include "../../libymlmap/msgqmuxmapping.h"
#include "../../libymlmap/filemapping.h"
#include "traceevent.h"

#include "../libsim/global.h"
#include "../libsim/mesq.h"

#include "../../BasicUtils/BasicException.h"
#include "../../BasicUtils/BasicProcess.h"
#include "../../BasicUtils/BasicDebugger.h"
#include "../../BasicUtils/BasicString.h"

using namespace std;

extern "C" {
    extern void main_install_modules();
    extern void main_install_blocks();

    extern int readregexp(char *filename, int pure);
    extern int pearl_methodinstall(char *name);
    extern int modulesinstalled();
    extern int minitstate(int np);
    extern int initxtop();
    extern void init_hash();
    extern void free_hashmem();
    extern int schedule();
    extern void runtree_epilog();
    extern int createchild(char *name, char *className);
    extern void emitstat(FILE *outFile);
}

using namespace std;

extern char *wt; /* window title */
extern FILE *statsFile;

/**
 * Dump the program syntax to stderr
 *
 * @param name the current program name
 */
void Syntax(char *name)
{
    cerr << endl
         << "YMLPearl" << endl
         << "Frank Terpstra and Joe Coffland" << endl
         << "University of Amsterdam" << endl
         << "Compile Date: " << __DATE__ << " at " << __TIME__ << endl << endl
         << "Syntax: " << name << " [OPTIONS]... <YML file> [variable=value]..."
         << endl
         << endl
         << "\t-I                \tPrint verbose debugging info." << endl
         << "\t-K <key>          \tSpecify mapping key" << endl
         << "\t-m <mapping>      \tUse specified mapping file" << endl
         << "\t-p <file>         \tPrint statistics to file.  '-' for stdout."
         << endl
         << "\t-q                \tSupress interpreter output (except on errors)"
         << endl
         << "\t-S                \tUse shared memory mapping interface" << endl
         << "\t-T <basename>     \tUse file mapping interface." << endl
         << "\t-x                \tPrint full stack trace on errors." << endl
         << "\t-d <file>         \tPrint debugging trace to file." << endl
         << endl;
    exit(1);
}

/**
 * Main function for compiled pearl simulations.
 * Provides a frontend interface and loads and
 * executes the simulation.
 *
 * @param argc argument count
 * @param argv arguments
 *
 * @return exit value
 */
int main(int argc, char *argv[])
{
    try {
        list<string> ppArgs; // Preprocessor arguments

        // Process Arguments
        mapping_t mappingType = M_MSGQ;

        char *mappingFileName = 0;
        char *traceBaseName = 0;
        int mappingKey = 0;
        char *statsFilename = 0;

        int c;

        while ((c = getopt(argc, argv, "Ii:K:m:p:qST:xd:")) != -1) {
            switch (c) {
                case 'I':
                    pearl_info = 1;
                    break;

                case 'i':
                    wt = optarg;
                    break;

                case 'K':
                    mappingKey = atoi(optarg);
                    break;

                case 'm':
                    mappingFileName = optarg;
                    break;

                case 'p':
                    statsFilename = optarg;
                    break;

                case 'q':
                    ppArgs.push_front("-q");
                    break;

                case 'S':
                    mappingType = M_SHM;
                    break;

                case 'T':
                    traceBaseName = optarg;
                    mappingType = M_FILE;
                    break;

                case 'x':
                    ppArgs.push_back("-x");
                    BasicDebugger::initStackTrace(argv[0]);
                    break;

                case 'd':
                    pdb_filename = optarg;
                    break;

                default:
                    Syntax(argv[0]);
                    break;
            }
        }

        // Filename argument
        const char *fileName = NULL;

        if (optind < argc) {
            fileName = argv[optind];
        } else {
            cerr << "Missing 'YML file' argument." << endl;
            Syntax(argv[0]);
        }

        ppArgs.push_back(fileName);

        /* Variable definitions eg: myvar="hello"
           Optind is the index of the yml file. Everything after that
           (stuff between optind+1 and argc) should be variable
           definitions.
           These variables are pushed into ppArgs so that ymlpp can
           substitute them during yml processing.
         */
        for (int def_index = optind + 1; def_index < argc; def_index++) {
            char *ptr = strchr(argv[def_index], '=');

            if (!ptr) {
                cerr << "Unrecognized variable argument \'";
                cerr << argv[def_index] << "'." << endl;
                Syntax(argv[0]);
            }

            ppArgs.push_back(argv[def_index]);
        }

        /* Pearl Initialisation section */

        /*
         * Install the statistics method by hand.
         * All the other methods are installed by the generated code
         *  in <project_name>..c
         */
        method_statistics = pearl_methodinstall((char *)"statistics");

        /*
         * main_install_modules() in <project_name>..c calls all the
         * class initialisation methods named <classname>_module().
         */
        main_install_modules();

        /*
         * After modules are installed allocate method_id to process
         * state table.
         */
        modulesinstalled();

        // Install blocker sets for all the Pearl modules.
        main_install_blocks();

        // Load mapping
        Mapping *mapping = NULL;

        if (mappingFileName) {

            if (mappingType == M_MSGQ || mappingType == M_SHM) {
                // Get key
                while (!mappingKey && !cin.eof()) {
                    cout << "Enter Mapping key: " << flush;
                    cin >> mappingKey;
                }
                cout << "Mapping key is " << mappingKey << endl;
            }

            switch (mappingType) {
                case M_MSGQ: {
                    MsgQMUXMapping *msgQMUXMapping = new MsgQMUXMapping(mappingFileName, DEST_SIDE);

                    if (!msgQMUXMapping->open(mappingKey)) {
                        THROW("Failed to open message queue mapping!");
                    }
                    mapping = msgQMUXMapping;
                }
                break;

                case M_SHM: {
                    SharedMemMapping *shmMapping = new SharedMemMapping(mappingFileName, DEST_SIDE);

                    if (!shmMapping->openKeyChannel(mappingKey)) {
                        THROW("Failed to shared memory key channel!");
                    }
                    if (!shmMapping->open()) {
                        THROW("Error open mapping.");
                    }
                    mapping = shmMapping;
                }
                break;

                case M_FILE: {
                    FileMapping *fileMapping = new FileMapping(mappingFileName, DEST_SIDE);
                    if (!fileMapping->open(traceBaseName)) {
                        THROW("Failed to open file mapping!");
                    }
                    mapping = fileMapping;
                }
                break;
            }
        }

        // Load YML
        PearlYMLEntityFactory factory;
        YMLLoader loader(&factory, NULL);

        // Spawn preprocessor
        BasicProcess ppProc;
        BasicPipe *ppPipe = ppProc.getChildPipe(BasicProcess::FROM_CHILD);
        ppArgs.push_front(BasicString(ppPipe->getInFD()));
        ppArgs.push_front("-p");
        ppArgs.push_front("ymlpp");

        istream *ppPipeStream = ppPipe->getOutStream();
        ppProc.exec(ppArgs);


        // Parse preprocess pipe
        TraceContext *traceContext = 0;

        if (mapping != NULL) {
            traceContext = mapping->getTraceContext();
            ASSERT_OR_THROW("Error getting mapping context.", traceContext);
        }

        PearlYMLNetwork *net;

        if (!(net = (PearlYMLNetwork *)loader.parse(*ppPipeStream, traceContext))) {
            THROW("Error parsing YML file.");
        }

        ppProc.wait();


        // Initialize statistics
        minitstate(pearl_njobs);

        if (statsFilename) {
            if (statsFilename[0] == '-' && statsFilename[1] == '\0') {
                statsFile = stdout;
            } else {
                statsFile = fopen(statsFilename, "a");

                ASSERT_OR_THROW(string("Error opening statistics file '") +
                                statsFilename + "'.", statsFile);
            }
        }

        // Setup message tables
        setup_mtables();

        // Initialize pearl objects
        net->initProcesses();

        initTraceTable(pearl_njobs);

        // Run simulation
        schedule();

        if (mapping != NULL) {
            mapping->close();
            delete mapping;
        }

        // Output statistics
        if (statsFile) {
            emitstat(statsFile);
        }

        delete net;

        return 0;

    } catch (BasicException &e) {
        cerr << e << endl;
    }

    return 1;
}
