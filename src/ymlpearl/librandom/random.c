/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <math.h>
#include <stdlib.h>

void rndseed(int i)
{
    srand48((long) i) ;
}

float frndexponential()
{
    return ((float) - log(1.0 - drand48())) ;
}

/* Box-Muller method. See Jain, Art of Comp.Syst.Perf.Anal. p.494 */
float frndnormal()
{
    static int flag = 0 ;
    static float R, phi ;
    float u, v ;

    if (flag) {
        flag = 0 ;
        return R * cos(phi) ;   /* may be correlated when u,v are from LCGs */
    }

    flag = 1 ;
    u = drand48() ;
    v = drand48() ;
    R = sqrt(- 2.0 * log(u)) ;
    phi = 2.0 * M_PI * v ;
    return R * (float) sin(phi) ;
}

float frnduniform()
{
    return (float) drand48();
}

/* Why not get a long random instead of this random float with a cast? */
int rnduniform(int average)
{
    return (int)(average * frnduniform()) ;
}

int rndnormal(int average)
{
    return (int)(average * frndnormal()) ;
}

int rndexponential(int average)
{
    return (int)(average * frndexponential()) ;
}
