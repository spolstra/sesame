/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file misc.h
 * @author B.J. Overeinder & J.G. Stil
 * @date   1989
 *
 * @brief Miscellaneous functions.
 */


#ifndef MISC_H
#define MISC_H

#ifdef __cplusplus
extern "C" {
#endif

    void panic(char *pformat, ...);

#ifdef __cplusplus
};
#endif
#endif // MISC_H
