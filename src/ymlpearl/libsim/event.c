/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 * Event history managing.
 *
 * This code is currently not called.
 * TODO: Are there compiler flags that generate calls to this code?
 */

#define consumed(consrec, object, time) ((consrec)->conttime[object] += time)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "event.h"
#include "statistics.h"
#include "global.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

typedef struct recx {
    double time;
    double conttime[1];
} recx;

typedef struct nodeendx {
    recx **recs;
    recx *total;
    double lasttime;
} nodeendx;

static nodeendx *nodes;
static int nobjects;

static recx **frees;

void makezero(recx *consrec)
{
    int i = nobjects;
    double *a = &consrec->conttime[0];

    while (i--) {
        *a++ = 0;
    }
}

static recx *allocrec(const int initlen)
{
    recx *t = (recx *) malloc(sizeof(recx) + (initlen - 1) * sizeof(double));
    makezero(t);
    return t;
}

/*
static int avelen = 0;
static int avelll = 0;
static int avesea = 0;
static int avesss = 0;
*/

static void copyrec(recx *to, recx *from)
{
    int i = nobjects - 1 + sizeof(struct recx) / sizeof(double);
    int *a = (int *) to;
    int *b = (int *) from;

    while (i--) {
        *a++ = *b++;
    }
}

void eventinit(const int nob)
{
    nobjects = nob;
    nodes = (nodeendx *) malloc(sizeof(nodeendx) * nobjects);
    frees = (recx **) malloc(2 * sizeof(recx *) * nobjects);

    for (int i = 0; i < nobjects; i++) {
        nodes[i].lasttime = 0.0;
        nodes[i].total = allocrec(nobjects);
        nodes[i].recs = (recx **) malloc(sizeof(recx *) * nobjects);

        for (int j = 0; j < nobjects; j++) {
            nodes[i].recs[j] = allocrec(nobjects);
        }
    }
}

void eventanal(const int object)
{
    double tot = 0.0, idlet, t;
    recx *r;
    int perc;
    static char *idlebuf = NULL;
    static unsigned idlebuflen = 0;
    int self;
    extern char *who();

    if (idlebuflen == 0) {
        idlebuflen = 256;
        idlebuf = (char *)malloc(idlebuflen);
    }

    idlebuf[0] = '\0';
    r = nodes[object].total;
    self = object;

    for (int i = 0; i < nobjects; i++)  {
        tot += r->conttime[i];
    }

    if (tot == 0.0) {
        return;
    }

    prstatname(NULL);
    prstatname("idle");
    idlet = tot - r->conttime[self];
    perc = (int)(idlet * 100.0 / tot);
    prstat(perc / 100.0, "%3d%%\n", perc);

    if (idlet == 0.0) {
        idlet = 0x7fffffff;
    }

    prstatname(NULL);
    prstatname("was in");

    /*prverbose("Tot  Idle Object. Time spent realtive to TOTAL time/IDLE time\n");*/
    for (int i = 0; i < nobjects; i++) {
        t = r->conttime[i];

        if (t == 0.0) {
            continue;
        }

        if (i == object) {
            sprintf(&idlebuf[strlen(idlebuf)] , "%3d%% ---- : Self (%.0f)\n",
                    (int)(t * 100 / tot) , t);
        } else {
            sprintf(&idlebuf[strlen(idlebuf)] , "%3d%% %3d%% : %s (%.0f)\n",
                    (int)((t * 100.0) / tot) , (int)((t * 100.0) / idlet) , who(i), t);
        }

        if (strlen(idlebuf) > idlebuflen - 80) {
            idlebuflen *= 2;
            idlebuf = (char *)realloc(idlebuf, idlebuflen);
        }
    }

    prstretch(idlebuflen);
    prstat(1.0, "%s", idlebuf);
    /*
      printf("avelen : %d/%d = %d (<=%d)\n", avelen, avelll, avelen / avelll, pearl_njobs);
      printf("avesea : %d/%d = %d\n", avesea, avesss, avesea / avesss);
    */
}

void eventbeenbusy(const int object, const double time)
{
    double t;
    recx **rp = nodes[object].recs;

    makezero(rp[object]);
    rp[object]->time = time;
    t = time - nodes[object].lasttime;
    nodes[object].lasttime = time;
    consumed(nodes[object].total, object, t);

    if (t != 0.0) {
        for (int i = 0; i < nobjects; i++) {
            if (i != object) {
                consumed(rp[i], object, t);
            }
        }
    }
}

void eventbeenidle(const int object, const double time)
{
    nodes[object].lasttime = time;
}

void eventsendmess(const int from, const int to, const double time, int *info)
{
    makezero(nodes[to].recs[from]);
    *info = (int)(nodes[to].recs[from]->time = time);
}

void eventrecvmess(const int from, const int to, const double time, int * UNUSED(info))
{
    recx **rpt, **rpf, *total, *fr;
    double reachpoint, idletime;

    rpf = nodes[from].recs;
    rpt = nodes[to].recs;

    for (int i = 0; i < nobjects; i++) {
        copyrec(rpt[i], rpf[i]);
    }

    total = nodes[to].total;
    fr = rpf[to];
    idletime = time - nodes[to].lasttime;
    reachpoint = time - nodes[to].recs[to]->time;

    if (reachpoint != idletime) {
        for (int i = 0; i < nobjects; i++) {
            if (fr->conttime[i]) {
                consumed(total, i, fr->conttime[i] * idletime / reachpoint);

                if (i == to) {
                    printf("Huh, myself %d in this list ?\n", to);
                }
            }
        }
    } else {
        for (int i = 0; i < nobjects; i++) {
            if (fr->conttime[i]) {
                consumed(total, i, fr->conttime[i]);

                if (i == to) {
                    printf("Huh, myself %d in this list ?\n", to);
                }
            }
        }
    }

    nodes[to].lasttime = time;
}
