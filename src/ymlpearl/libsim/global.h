/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *      B.J. Overeinder & J.G. Stil, Henk Muller
 */

#include "process.h"
#include "class.h"
#include "misc.h"

extern char *pdb_filename;

extern unsigned int pearl_nlabels;
extern int          method_statistics;
extern int          pearl_info;
extern timet        pearl_time;
extern processx    *pearl_head;
extern processx    *pearl_proc;
extern processx    *pearl_current;
extern unsigned int pearl_njobs;

#define VERBOSE_OFF 0
#define VERBOSE_ONCE  1
#define VERBOSE_ALL 2

/*
 * Test if the mask of a received message occurs in the list of masks of the
 * current process.
 */
#define test_mask(accept, method) ((accept)[method] >= 0)

#ifdef __cplusplus
extern "C" {
#endif

char *whoami();
int instanceof(const int jobid, const char *subtype_name);

/* Energy functions: update_energy called by user, collect_energy at the
   end of the simulation by the scheduler.
*/
void update_energy(int tstat, int pstat, int tdyn, int pdyn);
double collect_energy();
void stdflush(void);
int random_norm(int mu, int sigma);

#ifdef __cplusplus
}
#endif
