/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *      B.J. Overeinder & J.G. Stil, Henk Muller
 *
 * @file   global.c
 * @author B.J. Overeinder & J.G. Stil, Henk Muller
 * @date   Fri May 16 16:11:55 CEST 2014
 *
 * @brief
 * Implementation of the Pearl standard library functions declared in
 * `standardlib.pc`.
 *
 */

#include "global.h"
#include "class.h"
#include "misc.h"
#include "pdb.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <stdbool.h>

static int thisobject(); // local helper function

char *pdb_filename = NULL;

unsigned int pearl_nlabels = 0;  /**< Number of installed pearl methods. */
int method_statistics;       /**< identifier of the statistics method. */
int pearl_info = 0;          /**< Is set if info must be displayed. */
timet pearl_time = 0;        /**< Simulation time. */
processx *pearl_head = 0;    /**< Pearl schedule queue, sorted on time. */
processx *pearl_proc = 0;    /**< Pearl process table. */
processx *pearl_current = 0; /**< Current Pearl process. */


void stdflush(void)
{
  fflush(stdout);
}

/**
 * Return pearl simulation time.
 *
 * Takes the pearl_time and returns it as an int.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Simulation time.
 * @see pearl_time
 */
int timer()
{
    return (int) pearl_time;
}

/**
 * Return simulation time `pearl_time` as 64 bit unsigned integer.
 * NOTE: standardlib.pc declares the return type of ltimer as an int,
 * because we have no other option in Pearl. But code generation in
 * gimpl.c manually adds the correct prototype for ltimer to each
 * generated C file.  See ticket #94.
 *
 * @return Simulation time.
 */
unsigned long long int ltimer()
{
    return (unsigned long long int) pearl_time;
}

/**
 * Return simulation time as a float.
 *
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Simulation time.
 */
float ftimer()
{
    return (float) pearl_time;
}

/**
 * Return Pearl process name of process with id `i`.
 * id is the index in the process table.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @param i Pearl process id.
 * @return Pearl process name.
 */
char *who(const int i)
{
    return pearl_proc[i].name;
}

/**
 * Return name of the current Pearl process (the caller of this
 * function).
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Pearl process name.
 */
char *whoami()
{
    return pearl_current->name;
}

/**
 * Record a debug message in the pdb log.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @param format    Debug message.
 * @return 1
 */
int debug(char *format, ...)
{
    static char string[1024];

    va_list args;
    va_start(args, format);
    vsnprintf(string, 1024, format, args);
    va_end(args);

    pdb_record_log(pearl_time, thisobject(), string);

    return 1;
}

/**
 * Record a change to an attribute.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @param name  Attribute name.
 * @param value New attribute value.
 * @return 1
 */
int monitor(char *name, int value)
{
    pdb_record_attribute(pearl_time, thisobject(), name, value);

    return 1;
}

/**
 * Return the index of the current pearl component in the process
 * table. Helper function for the pearl debugger.
 *
 * @return Index of the current pearl component.
 */
static int thisobject()
{
    return pearl_current - pearl_proc;
}

/**
 * WIP
 */
void update_energy(int tstat, int pstat, int tdyn, int pdyn)
{
    double val;

    val = (double)tstat * pstat;
    val += (double)tdyn * pdyn;

    pearl_current->energy = val;
}

/**
 * Return the number of pending messages for this process.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Number of pending messages.
 */
int nr_pendingmsg()
{
    return pearl_current->mql;
}

/**
 * Return the number of pearl processes in the simulation.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return The number of pearl processes in the simulation.
 */
int numjobs()
{
    return pearl_njobs;
}

/**
 * Return the number of classes in the simulation.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return The number of classes in the simulation.
 */
int numclasses()
{
    return nclasses;
}

/**
 * Set line buffering on standard output.
 * Part of the Pearl standard library `standardlib.pc`.
 */
void stdoutlinebuf()
{
    /* Let setvbuf allocate the buffer itself. */
    if (setvbuf(stdout, (char*)NULL, _IOLBF, 0) != 0) {
        panic("Could not set line buffering");
    }
}

/**
 * Validate pearl process identifier.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @param jobid     pearl process id
 */
void validate_jobid(const unsigned int jobid)
{
    /* Catcher has jobid 0 and is not considered as a valid job
     * because it is not a user process. */
    if (jobid <= 0 || pearl_njobs <= jobid) {
        panic("invalid job id %d", jobid);
    }
}

/**
 * Check if pearl process @p jobid is an instance the class @p subtype_name.
 * This function is also used by `to_XXX` functions to check if we can
 * legally cast to process to `XXX`.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @param jobid         Pearl process identifier.
 * @param subtype_name  Class name.
 * @return If the class of process @p jobid is equal or a subclass of
 *         `subtype_name` return 1, otherwise return 0.
 */
int instanceof(const int jobid, const char *subtype_name)
{
    int class_id;
    subtypex *st;

    validate_jobid(jobid);
    class_id = pearl_proc[jobid].classIdx;

    // Check if the classes match.
    if (strcmp(subtype_name, cllist[class_id].name) == 0) {
        return 1;
    }

    // If not, check the super types of this class. (stored in subtypes)
    for (st = cllist[class_id].subtypes; st; st = st->next)
        if (strcmp(subtype_name, st->name) == 0) {
            return 1;
        }

    return 0;
}

/**
 * Return the index of the source of our last received message.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Index of the sender of the last message that this process
 *         accepted.
 */
int last_caller()
{
    return pearl_current->last_caller;
}

/**
 * Return the class of the caller process.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Class name of the caller.
 */
char *class_name()
{
    return pearl_current->className;
}

/**
 * Return the class of the process with index `jobid`.
 * Part of the Pearl standard library `standardlib.pc`.
 *
 * @return Class name of process with index `jobid`.
 */
char *class_name_of(const int jobid)
{
    validate_jobid(jobid);

    return pearl_proc[jobid].className;
}

/**
 * Return a random value from a normal distribution defined by:
 *
 * @param mu    mean of the normal distribution
 * @param sigma standard deviation of the normal distribution.
 * @return      random value with mean mu and std dev. sigma
 */
int random_norm(int mu_, int sigma_) {
    /* Algorithm used:
       The standard Box–Muller transform generates values from the standard
       normal distribution (i.e. standard normal deviates) with mean 0 and
       standard deviation 1. The implementation below in standard C++
       generates values from any normal distribution with mean μ
       {\displaystyle \mu } \mu and variance σ 2 {\displaystyle \sigma ^{2}}
       \sigma ^{2}. If Z {\displaystyle Z} Z is a standard normal deviate,
       then X = Z σ + μ {\displaystyle X=Z\sigma +\mu } X=Z\sigma +\mu will
       have a normal distribution with mean μ {\displaystyle \mu } \mu and
       standard deviation σ {\displaystyle \sigma } \sigma . Note that
       because the random number generator rand has not been seeded, the same
       series of values will always be returned from the
       generateGaussianNoise function */

    /* architecture module works with integers, so convert to double
     * and back to int when we return the random value. */
    double mu = mu_;
    double sigma = sigma_;

    static const double epsilon = DBL_EPSILON;
    static const double two_pi = 2.0*M_PI;

    static double z1;
    static bool generate;
    generate = !generate;

    if (!generate)
        return (int) (z1 * sigma + mu);

    double u1, u2;
    do
    {
        u1 = random() * (1.0 / RAND_MAX);
        u2 = random() * (1.0 / RAND_MAX);
    }
    while ( u1 <= epsilon );

    double z0;
    z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
    z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
    return (int) (z0 * sigma + mu);
}
