/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

static unsigned int cutbits[32] = {
    0xffffffff, 0x00000001, 0x00000003, 0x00000007,
    0x0000001f, 0x0000001f, 0x0000003f, 0x0000007f,
    0x000001ff, 0x000001ff, 0x000003ff, 0x000007ff,
    0x00001fff, 0x00001fff, 0x00003fff, 0x00007fff,
    0x0001ffff, 0x0001ffff, 0x0003ffff, 0x0007ffff,
    0x001fffff, 0x001fffff, 0x003fffff, 0x007fffff,
    0x01ffffff, 0x01ffffff, 0x03ffffff, 0x07ffffff,
    0x1fffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff
};

/* HP/UX and AIX do not know the IEEE math function scalbn.
   SunOs4 and Solaris do know. */

#ifdef hpux
double scalbn(const double x, const int n)
{
    return(x * (1 << n));
}
#endif

void _mlimul(const int * UNUSED(pl), const int * UNUSED(pr), const int nb)
{
    printf("Multiply on integers of length %d not implemented yet\n", nb);
    abort();
}

void _mlidiv(const int * UNUSED(pl), const int * UNUSED(pr), const int nb)
{
    printf("Divide on integers of length %d not implemented yet\n", nb);
    abort();
}

void _mlimod(const int * UNUSED(pl), const int * UNUSED(pr), const int nb)
{
    printf("Modulo on integers of length %d not implemented yet\n", nb);
    abort();
}

void _mliplus(unsigned int *pl, unsigned int *pr, const int nb)
{
    int accu = 0;
    int res;

    for (int i = 0; i < nb; i += 32) {
        accu += (*pl & 0xFFFF) + (*pr & 0xFFFF);
        res = accu & 0xFFFF;
        accu >>= 16;
        accu += (*pl >> 16) + (*pr >> 16);
        *pl = res | accu << 16;
        accu >>= 16;
        pl++;
        pr++;
    }

    pl[-1] &= cutbits[nb % 32];
}

void _mlimin(unsigned int *pl, unsigned int *pr, const int nb)
{
    int accu = 0;
    int res;

    for (int i = 0; i < nb; i += 32) {
        accu += (*pl & 0xFFFF) - (*pr & 0xFFFF);
        res = accu & 0xFFFF;
        accu >>= 16;
        accu += (*pl >> 16) - (*pr >> 16);
        *pl = res | accu << 16;
        accu >>= 16;
        pl++;
        pr++;
    }

    pl[-1] &= cutbits[nb % 32];
}

void _mlishleft(const int * UNUSED(pl), const int * UNUSED(pr), const int nb)
{
    printf("Shiftleft on integers of length %d not implemented yet\n", nb);
    abort();
}

void _mlishright(const int * UNUSED(pl), const int * UNUSED(pr), const int nb)
{
    printf("Shiftright on integers of length %d not implemented yet\n", nb);
    abort();
}

void _mliumin(unsigned int *pl, const int nb)
{
    int accu = 0;
    int res;

    for (int i = 0; i < nb; i += 32) {
        accu += 0 - (*pl & 0xFFFF);
        res = accu & 0xFFFF;
        accu >>= 16;
        accu += 0 - (*pl >> 16);
        *pl = res | accu << 16;
        accu >>= 16;
        pl++;
    }

    pl[-1] &= cutbits[nb % 32];
}

void _mlinot(int *pl, const int nb)
{
    for (int i = 0; i < nb; i += 32) {
        *pl = ~ *pl;
        pl++;
    }

    pl[-1] &= cutbits[nb % 32];
}

#define BITOP(NAME, OP) \
    void NAME(int *pl, int *pr, const int nb) {\
        for (int i=0; i<nb; i+=32) { \
            *pl OP *pr; \
            pl++; \
            pr++; \
        } \
    }

BITOP(_mliand, &=);
BITOP(_mlior, |=);
BITOP(_mlixor, ^=);

float _mli2float(int *pl, const int nb)
{
    int i, x, y;

    for (i = (nb - 1) / 32; i >= 0; i--) {
        if (pl[i] != 0) {
            break;
        }
    }

    if (i < 0) {
        return 0.0;
    }

    x = pl[i];
    y = i > 0 ? pl[i - 1] : 0;
    i *= 32;

    while (x > 0) {
        x <<= 1;

        if (y < 0) {
            x |= 1;
        }

        y <<= 1;
        i--;
    }

    return scalbn((double)(unsigned) x, i);
}
