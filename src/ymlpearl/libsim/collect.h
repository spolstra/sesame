/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef COLLECT_H
#define COLLECT_H

#include "message.h"

void prnewstate(int process);
void newstate(int process, int state);
void collectsend(const int dest, const messx *m);
void collectrecv(const int dest, const messx *m);
void collectsendrecv(const int src, const int dest, const int method, const int length);

#endif // COLLECT_H
