/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file startup.h
 * @author B.J. Overeinder & J.G. Stil
 * @date  1989
 *
 * @brief TODO brief here.
 *
 */

#ifndef STARTUP_H
#define STARTUP_H

#include "process.h"

char *pearl_methodname(int method);
int create_proc(char *name, function func, intptr_t *env,
                char *classname, int stats_state);

#endif // STARTUP_H
