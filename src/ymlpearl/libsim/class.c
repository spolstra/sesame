/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file class.c
 * @author B.J. Overeinder & J.G. Stil
 * @date   1989
 *
 * @brief Functions to manage Pearl classes.
 */

#include "class.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

static int maxclasses = 0;  //!< Space currently allocated for classes.
int nclasses = 0;           //!< Number of classes in the simulation.
classx *cllist;             //!< Global Pearl class table.

/**
 * Create a new Pearl class @p name.
 * #installclass is called from the generated `<classname>_module()`
 * function for every class in the simulation.
 *
 * @param func      Function called "<classname>_create" that creates a
 *                  pearl process of this class.
 * @param name      Name of the class created.
 * @param initfunc  Function called "<classname>_initiate" that
 *                  initialises a pearl process of this class.
 * @return          Class id of the created class.
 */
int installclass(intptr_t (*func)(), char *name, intptr_t * (*initfunc)())
{
    if (nclasses >= maxclasses) {
        if (maxclasses == 0) {
            maxclasses = 20;
            cllist = (classx *)malloc(sizeof(classx) * (unsigned) maxclasses);

        } else {
            maxclasses *= 2;
            cllist = (classx *)realloc((char *)cllist,
                                       sizeof(classx) * (unsigned)maxclasses);
        }
    }

    cllist[nclasses].name = name;
    cllist[nclasses].func = func;
    cllist[nclasses].initfunc = initfunc;
    cllist[nclasses].subtypes = 0;

    return nclasses++;
}

/**
 * Find class information by name.
 *
 * @param name  Name of class to find.
 * @return Pointer to the classx class information structure or NULL
 *         if the class name is not found.
 */
classx *findclass(const char *name)
{
    for (int i = 0; i < nclasses; i++)
        if (strcmp(cllist[i].name, name) == 0) {
            return &cllist[i];
        }

    return NULL;
}

/**
 * Add superclass @p name to the `subtypes`(inheritance) list of the
 * current class with @p class_id.
 * Name is confusing because subtypes is a list of supers of the
 * class. This is due to the Pearl declaration `subtypeof superclass`.
 *
 * @param class_id      Index into classtable of the current class.
 * @param name          Name of the superclass to store in the subtypes
 *                      field.
 */
int installsubtype(const int class_id, char *name)
{
    // Create a node for the super class @p name.
    // TODO this creates a small, but fixed size, memory leak
    subtypex *s = malloc(sizeof(subtypex));

    // And link this node into the superclass list of
    // the current class @p class_id.
    s->name = name;
    s->next = cllist[class_id].subtypes;

    cllist[class_id].subtypes = s;

    return 0;
}
