/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file libsim/process.h
 * @author B.J. Overeinder & J.G. Stil
 * @date 1989
 *
 * @brief Pearl process data structures.
 */

#ifndef  PROCESS_H
#define  PROCESS_H

#include <stdint.h>

#include "message.h"

typedef int (*function)();

typedef uint64_t timet;  /**< 64 bit type to hold the simulation time. */

/** Message queue element. The process messsage queue has #mesg_q
 * slot in it's message queue for all its method. */
typedef struct mesg_q {
    int method_id;          //!< method id of the messages in this slot.
    messx *mlist, *mtail;   //!< head and tail of message list.
} mesg_q;

/**
 * Description of the entries in the process table, i.e. the class instances.
 */
typedef struct processx {
    char              *name;    //!< process name.
    function          func;     //!< process C function.
    function          efunc;     //!< process energy function. (always NULL)
    intptr_t          state;    //!< process state.
    timet             time;     //!< process activation time.
    double            energy;   //!< Total energy consumed.
    intptr_t          *env;     //!< environment (params, locals).
    intptr_t          *sp;      //!< stack for expression evaluation.
    int               *mask;    //!< array of method id's the proc. accepts.
    mesg_q            *mqs;     //!< hashtable of message-slots. One slot for                                     every method of this process.
    int               mqsize;   //!< size of hashtable == number of methods.
    int               mq_fs;    //!< number of occupied slots.
    unsigned int      seq_nr;   //!< mseq nr = seq_nr + flip_nr*MAX_SEQ.
    unsigned int      flip_nr;  //!< message overflow counter.
    struct processx   *next;    //!< processes are linked for scheduling.
    char              *className;  //!< class name.
    int               classIdx;    //!< index into #cllist table.
    int               mql;         //!< message queue length.
    void              *ymlNode;    //!< link to the YML object.
    int               last_dest;   //!< destination of the last pearlsend.
    int               last_method; //!< method id of last pearlsend.
    int               reply_wait;  //!< 1 if waiting for a reply
    int               stats_state; //!< state id of statistics method.
    int               last_caller; //!< sender id of last processed message.
} processx;

/*
 * States
 */

/** TIME: Blocked on time, eg. blockt(10) */
#define  TIME   1

/** RECV: Blocked, waiting on messages, eg. block(foo) */
#define  RECV   2

/** TIME_RECV: Blocked on time, and willing to receive messages,
 * eg. blockt(10, foo) */
#define  TIME_RECV  (TIME | RECV)

/** READY: indicates that process will be activated next. */
#define  READY    4

/** NONEXISTING: Invalid user process state. This is the state of the Catcher
 * process. @see catcherfunc. */
#define  NONEXISTING  8

#endif //  PROCESS_H
