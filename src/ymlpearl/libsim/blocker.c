/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   blocker.c
 * @author B.J. Overeinder & J.G. Stil
 * @date   1989
 *
 * @brief Implementation blocker (mask) functions.
 *
 * Blocker masks tell the pearlreceive function which methods we are
 * willing to accept when we block.
 * A blocker mask also translates the global method id to a (local)
 * return state, so that the process can jump to the correct state
 * depending on which methods it received during a block.
 * #pearlreceive uses the mask to put the correct return state on the stack.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

static int *allmethods;     //!< Translate method ids to process states.
static int nmethods;        //!< Total number of methods in the simulation.
static int *curblock;       //!< Current blocker set that is constructed.

#define EMPTY -1

/**
 * Allocate reusable method id to process state table.
 * Called during initialisation once #pearl_nlabels is known.
 * (after every class called all its #pearl_methodinstall's)
 * #allmethods is refilled by every class.
 *
 * @param nlabels   Total number of methods (#pearl_nlabels) in
 *                  the simulation.
 */
void blocker_init(int nlabels)
{
    nmethods = nlabels;
    allmethods = (int *)malloc(sizeof(int) * nmethods);
}

/**
 * Clear the allmethods translation table. Every class calls this
 * function before it creates its blocker masks.
 */
void blocker_class()
{
    int i;

    for (i = 0; i < nmethods; i++) {
        allmethods[i] = EMPTY;
    }
}

/**
 * Associate method id with process state in table #allmethods.
 * #blocker_new, #blocker_append and #blocker_close use
 * #allmethods translation table to construct blocker sets.
 *
 * @param method    Global method id.
 * @param state     Pearl state id (local to the class).
 */
void blocker_def(int method, int state)
{
    allmethods[method] = state;
}

/**
 * A blocker mask is created by the call sequence:
 * #blocker_new, {N x #blocker_append..}, #blocker_close.
 *
 * blocker_new creates a new empty curblock that can be filled by
 * subsequent #blocker_append calls.
 */
void blocker_new()
{
    int i;

    curblock = (int *)malloc(nmethods * sizeof(int));

    for (i = 0; i < nmethods; i++) {
        curblock[i] = EMPTY;
    }
}

/**
 * Add method id to the current blocker mask.
 *
 * @param method    Method id to add.
 */
void blocker_append(int method)
{
    curblock[method] = allmethods[method];
}

/**
 * Return the blocker mask constructed with #blocker_append.
 *
 * @return Current blocker mask.
 */
intptr_t blocker_close()
{
    return (intptr_t)curblock;
}
