/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 * @file   schedule.c
 * @author B.J. Overeinder & J.G. Stil
 * @date   Fri May 16 13:08:41 CEST 2014
 *
 * @brief Implementation of the Pearl scheduler.
 *
 * Functions:
 * * @ref insert_q and @ref remove_q is the interface to the schedule
 *   queue.
 * * @ref hold_to, @ref pearlsend, @ref receive_time, @ref pearlwaiting
 *   and @ref pearlreceive
 *   is the interface that the generated pearl code uses to block, send
 *   and receive messages.
 * * @ref schedule is the scheduler that drives the discrete event
 *   simulation.
 * * @ref receive_message is a helper function for receiving messages
 */

#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <assert.h>
#include <stdbool.h>
#include <limits.h>

#include "schedule.h"
#include "mesq.h"
#include "collect.h"
#include "misc.h"
#include "statistics.h"
#include "event.h"
#include "startup.h"
#include "global.h"

static int receive_message(intptr_t mask, intptr_t *sp);

/// Pearl debugger include
#include "pdb.h"

char *wt = "pearl";

extern int data_type;
extern FILE *statsFile;

#define TIMEDIFF(a,b) ((long long)(((unsigned long long)(a))-((unsigned long long)(b))))


/**
 * Insert the process pointed to global 'pearl_current' in the
 * schedule queue 'pearl_head' at 'dt' step into the future.
 *
 * This queue is sorted on schedule time and the process is inserted
 * at the right position.  If other processes are already scheduled at
 * that point in time, 'pearl_current' will be scheduled as the last
 * process to run at that time.
 *
 * @param dt  Delta time.
 * @see pearl_head
 * @see pearl_current
 */
static void insert_q(timet dt)
{
    processx  *p, **last;

    if (pearl_info) {
        fprintf(stderr, "insert_q: dt = %llu\n", (long long unsigned) dt);
    }

    p = pearl_head;
    last = &pearl_head;
    // set 'wakeup' time
    pearl_current->time = pearl_time + dt;

    while (p != NULL) {
        if (TIMEDIFF(p->time, pearl_current->time) > 0) {
            break;
        }

        last = &p->next;
        p = p->next;
    }

    pearl_current->next = p;
    *last = pearl_current;
}


/**
 * Remove a process 'p' from the schedule queue.
 *
 * The process 'p' is unlinked from the process schedule queue 'pearl_head'.
 *
 * @param p  Process to remove.
 */
static void remove_q(processx *p)
{
    processx *q;

    if (pearl_head == p) {
        pearl_head = pearl_head->next;
        return;
    }

    for (q = pearl_head; q != NULL; q = q->next) {
        if (q->next == p) {
            q->next = p->next;
            return;
        }
    }

    if (q == NULL)
        panic("Send to TIME_RECV object \"%s\": not present in timer queue.",
              p->name);
}


/**
 * Delay the current process 'pearl_current' by 'dt' time units.
 *
 * Delay pearl_current and set it's state to TIME.
 *
 * @param dt  Time units.
 */
void hold_to(const int dt)
{
    if (dt < 0) {
        fprintf(stderr, "Hold_to with negative parameter!\n");
        exit(0);
    }

    // PDB: record a sleep for the current process pearl_current.
    pdb_record_sleep(pearl_time, pearl_current - pearl_proc, dt, NULL);

    insert_q((timet) dt);
    pearl_current -> state = TIME;
    newstate(pearl_current - pearl_proc, pearl_current->state);
}


/**
 * Indicate whether the 'pearl_current' is waiting for a reply from a
 * synchronous method call.  '1' indicates true, '0' is false.
 *
 * Set to true after the pearlsend, and reset to false when the
 * method_reply_X messages arrives.
 *
 * @param reply_wait  Wait state.
 */
void pearlwaiting(const int reply_wait)
{
    pearl_current->reply_wait = reply_wait;
}

/**
 * Send a message to 'dest'. Mask contains the identifier of the
 * remote method.  The contents of the message is on the stack,
 * starting at the position 'sp' points to. 'len' is the length of the
 * message. If the message is sent, first a check if the destination
 * expects the messages is done. If so, the message is written on the
 * destination's stack. If not, the message is put in the
 * destination's message queue.
 *
 *
 * @param dest    Pearl process to receive the message.
 * @param method  Method id of the message.
 * @param sp      Starting position of the message content.
 * @param len     Length of message.
 */

void pearlsend(const intptr_t dest, int method, intptr_t *sp, int len)
{
    messx *m;
    processx *pdest = &pearl_proc[dest];
    int source = pearl_current - pearl_proc;

    // Pearl debugger needs message content as int's.
    // So the message content on the stack is copied into an int[].
    // This should work on both 32 and 64 bit machines as ints are
    // likely to be 32 on both.
    static int pdb_args[512];
    int pdb_len = 0;

    while (pdb_len < len && pdb_len < 512) {
        pdb_args[pdb_len] = (int)sp[pdb_len];
        pdb_len++;
    }

    if (dest < 0 || pearl_njobs < (unsigned int)dest) {
        panic("pearlsend() invalid destination %d", dest);
    }

    if (pearl_info) {
        fprintf(stderr, "send: dest %" PRIdPTR ", method %d, sp %p, len %d\n",
                dest, method, sp, len);
        fprintf(stderr, "state of receiving object: %d\n", (int) pdest->state);
    }

    pearl_current->last_dest = dest;
    pearl_current->last_method = method;

    switch (pdest->state) {
        case RECV:
        case TIME_RECV:
            // Destination is waiting for messages.
            if (pearl_info) {
                fprintf(stderr, "address of mask = %p\n", pdest->mask);
            }

            // Check if it will accept this specific message.
            if (test_mask(pdest->mask, method)) {

                if (pearl_info) {
                    fprintf(stderr, "send: destination expected message\n");
                }

                // Record sendreceive for PDB pearl debugger.
                pdb_record_sendreceive(pearl_time, source, dest, method,
                        pdb_len, pdb_args);

                collectsendrecv(source, dest, method, len);
                // stack position sp[0] should contain the method id.
                pdest->sp[0] = pdest->mask[method];

                // The source of the message is placed on the stack
                // after the message content.
                pdest->sp[len + 1] = source;

                // Copy the message onto the stack of the destination
                // process.
                while (len--) {
                    pdest->sp[len + 1] = sp[len];
                }

                // Set schedule time to now.
                pdest->time = pearl_time;

                // If destination was in the schedule queue, remove it..
                if (pdest->state == TIME_RECV) {
                    remove_q(pdest);
                }
                // ..and put it at the front of the schedule queue.
                pdest->state = READY;
                pdest->next = pearl_head;
                pearl_head = pdest;
                break;

            } // else continue on to next case.

        case TIME:
        case READY:
            // If destination is blockt'ed of ready to run, it is not
            // ready to accept the message. So the message needs to be
            // stored in message queue of the destination process.
            if (pearl_info) {
                fprintf(stderr, "send: destination did not expect message\n");
                fprintf(stderr, "Mask: %p, state %" PRIdPTR "\n", pdest->mask, pdest->state);
            }

            // First record message send in PDB
            pdb_record_send(pearl_time, source, dest, pdest->seq_nr, method, pdb_len, pdb_args);

            // Create a 'messx' message
            m = allocm(len);
            m->source = source;
            m->method = method;
            m->next = NULL;
            m->seq_nr = pdest->seq_nr;
            m->flip_nr = pdest->flip_nr;
            update_sequencer(pdest);

            // Copy message data from stack to message.
            while (len--) {
                m->cont[len] = sp[len];
            }

            store_mesg(pdest, m);
            collectsend(dest, m);
            pdest->mql++; // update queue length.
            break;

        case NONEXISTING:
            panic("Sending to undefined object %d, sender was %d (%s)\n",
                  dest, source, pearl_current->name);

        default:
            panic("Destination \"%s\" in unknown state %d.\n",
                  pdest->name, pdest->state);
    }

    newstate(pearl_current - pearl_proc, pearl_current->state);
}


/**
 * Delay 'pearl_current' process for 'dt' units and check if there are
 * messages in its message queue. If so, remove process from schedule
 * queue again.
 *
 * A call to this function is generated for the pearl blockt statement
 * with both time and methods such as blockt(10, foo).
 *
 * @param mask  Set of messages to check for.
 * @param dt    Time units.
 * @param sp    Top of stack of pearl process.
 * @return      1 if a message is received, 0 otherwise.
 */
int receive_time(intptr_t mask, int dt, intptr_t *sp)
{
    int succeed;

    if (dt < 0) {
        fprintf(stderr, "Receive_time with negative parameter!\n");
        exit(0);
    }

    sp[0] = 0; // Clear stack position that indicates method id received.

    if (pearl_info) {
        fprintf(stderr, "receive_time: mask %p, dt %d, sp %p\n",
                (void *)mask, dt, sp);
    }

    // Schedule pearl_current for current time + dt
    insert_q((timet) dt);

    // Try to receive a message (from message queue) that matches
    // our method mask.
    if ((succeed = receive_message(mask, sp))) {
        remove_q(pearl_current);
    } else {
        // No suitable message found in the message queue, goto sleep
        pdb_record_sleep(pearl_time, pearl_current - pearl_proc, dt, (int *)mask);
        // set state to waiting for time or messages, -1 indicates no
        // valid method? FIXME
        pearl_current -> state = TIME_RECV;
        sp[0] = -1;
    }
    newstate(pearl_current - pearl_proc, pearl_current->state);

    return succeed;
}


/**
 * Check the message queue of the 'pearl_current' process for messages that
 * match 'mask'. If we find one, duplicate message onto process
 * stack 'sp'. If no suitable message is found the pearl process is
 * set into a receiving state (RECV).
 *
 * @param mask  Set of messages that we are willing the accept.
 * @param sp    Pearl process stack.
 *
 * @return      1 if a message is received or 0 if no suitable message is
 *              found.
 *
 * @see receive_message
 */
int pearlreceive(intptr_t _mask, intptr_t *sp)
{
    int succeed;
    int *mask = (int *) _mask; // TODO: not needed? replace with casts

    if (!(succeed = receive_message(_mask, sp))) {
        pdb_record_sleep(pearl_time, pearl_current - pearl_proc, 0, mask);

        // No messages, put process in RECV state. Save mask and sp so
        // we can put a message on the stack later on.
        pearl_current->state = RECV;
        pearl_current->mask = mask;
        pearl_current->sp = sp;

        if (pearl_info)
            fprintf(stderr,
                    "receive: no message available, mask = %p, sp = %p\n",
                    pearl_current->mask, pearl_current->sp);
    }

    newstate(pearl_current - pearl_proc, pearl_current->state);

    return succeed;
}


/**
 * Try to receive a message matching the message mask. Look for a
 * message with 'find_mesg'. If we find one, copy the message id and
 * the message content onto the stack 'sp' of the 'pearl_current'
 * process.
 *
 * @param _mask  Set of messages we are willing to accept.
 * @param sp     Top of stack of receving pearl process.
 * @return       1 if message is received, 0 otherwise.
 *
 * @see find_mesg
 */
static int receive_message(intptr_t _mask, intptr_t *sp)
{
    int len;
    messx *msg;
    int *mask = (int *)_mask;

    if (pearl_info)
        fprintf(stderr, "receive: mask = %p, sp = %p\n",
                mask, sp);

    // Check for messages
    msg = find_mesg(pearl_current, mask);

    if (msg != NULL) {
        // Found message, receive it now
        if (pearl_info)
            fprintf(stderr, "receive: message found with mask 0x%x\n",
                    (unsigned int)msg->method);
        pdb_record_receive(pearl_time, pearl_current - pearl_proc,
                           msg->seq_nr);
        len = msg->length;

        if (pearl_info) {
            fprintf(stderr, "Copying %d longs from %p to %p\n",
                    len, msg->cont, sp);
            fprintf(stderr, "State %d, src %d\n", mask[msg->method],
                    msg->source);
        }

        collectrecv(pearl_current - pearl_proc, msg);

        /* Copy the new return state onto the stack.
         * Block mask translates the method id to the correct
         * return state. */
        sp[0] = mask[msg->method];

        sp[len + 1] = msg->source; // Copy src onto stack after msg content.
        pearl_current->last_caller = msg->source;

        while (len--) { // Copy message content.
            sp[len + 1] = msg->cont[len];
        }

        freem(msg);
    }
    return msg != NULL;
}

/**
 * Convert Pearl state id to string.
 *
 * @param state  Pearl state id
 * @return       Pearl state string.
 */
char *stateString(const int state)
{
    switch (state) {
        case TIME:
            return "TIME";

        case RECV:
            return "RECEIVE";

        case TIME_RECV:
            return "TIME_RECEIVE";

        case READY:
            return "READY";

        case NONEXISTING:
        default:
            return "UNKNOWN";
    }
}

/**
 * Start the 'statistics' method of Pearl process with index 'procnr'.
 * If the process mask includes the statistics method, the method is
 * started by setting sp[0] to 'method_statistics' and calling the
 * process XXX_call function.
 * If 'statistics' is not included in the mask statistics are forced
 * by directly setting the state.
 *
 * Put 'statistics' method-label in process's stack and schedule process one
 * pearl_time.
 *
 * @param procnr  Index into 'pearl_proc' Pearl process list.
 */
void statistics(const int procnr)
{
    pearl_current = &pearl_proc[procnr];
    pearl_head = NULL;

    if (pearl_current->reply_wait)
        fprintf(stdout, "%s blocked on %s.%s()\n",
                pearl_current->name,
                pearl_proc[pearl_current->last_dest].name,
                pearl_methodname(pearl_current->last_method));

    if (pearl_current->mask != NULL &&
        pearl_current->mask[method_statistics] > 0) {

        pearl_current->sp[0] = pearl_current->mask[method_statistics];
        (*pearl_current->func)(pearl_current->env);

    } else {
        fprintf(stdout, "%s forcing statistics\n", pearl_current->name);

        pearl_current->env[0] = pearl_current->stats_state;
        (*pearl_current->func)(pearl_current->env);
        //fprintf(stderr, "%s does not like statistics\n", pearl_current->name);
    }
}

/**
 * `catcherfunc` is the function of the catcher process. The catcher
 * catches the end-of-simulation situation. It is scheduled once at
 * the beginning of the simulation. When the schedule loop finds that
 * there is only one process left, that process must be the catcher,
 * and the simulation is finished.
 * `catcherfunc` should be called only once at the beginning of the
 * simulation.
 *
 * @return 0
 */
int catcherfunc()
{
    static bool first = true;
    // Catcher does not check overflow anymore, see Ticket #93.
    // Should be called only once at the beginning of the simulation.
    if (first) {
        insert_q(1LL<<62); // schedule at time that can be still substracted
                          // from 0 and result in a valid signed long long.
                          // Then insert_q can schedule it as the last
                          // process.
        first = false;
    } else {
        assert(0); // TODO: use error reporting function.
    }

    return 0;
}

/**
 * Schedule processes from the schedule queue 'pearl_head' as long as
 * there are processes to be scheduled.
 * Schedule loop stops if the only process in the queue is the catcher
 * process.
 *
 */
void schedule()
{
    long tstart, tend; // Timers for wall clock time reporting.
    double energy;

    // If there are no jobs just return.
    if (!pearl_njobs) {
        return;
    }

    tstart = clock();

    // Link up all pearl proceses to create the inital schedule queue.
    // Skip pearljob id 0 since that is the catcher, and the catcher
    // is manually inserted in the schedule queue.
    for (unsigned int i = 1; i < pearl_njobs; i++) {
        pearl_proc[i].next =
                (i != pearl_njobs - 1) ? &pearl_proc[i + 1] : NULL;
    }
    pearl_head = &pearl_proc[1];

    // Schedule catcher process.
    pearl_current = &pearl_proc[0];
    catcherfunc();

    // Setup pearl debugger support.
    if (pdb_filename) {
        pdb_init(pdb_filename, pearl_njobs, pearl_nlabels);

        for (unsigned int i = 0; i < pearl_njobs; i++) {
            if (strcmp(pearl_proc[i].name, "Catcher") == 0) {
                pdb_register_process(pearl_proc[i].name, NULL);
            } else {
                pdb_register_process(pearl_proc[i].name, pearl_proc[i].className);
            }
        }

        for (unsigned i = 0; i < pearl_nlabels; i++) {
            pdb_register_message_type(pearl_methodname(i));
        }
    }

    // Schedule until only the catcher is left in the queue.
    while (pearl_head -> next != NULL) {
        // Take head from the queue and make it current. This will be
        // the process with the earliest schedule time since the queue
        // is sorted on time.
        pearl_current = pearl_head;
        pearl_head = pearl_head->next;

        // Advance pearl_time to the time of the current process.
        pearl_time = pearl_current->time;

        // Make active and record state.
        pearl_current->state = READY;
        newstate(pearl_current - pearl_proc, pearl_current->state);

        if (pearl_info) {
            fprintf(stderr, "Call %s\n", pearl_current->name);
        }

        // Start current pearl process, pass it's environment.
        (*pearl_current->func)(pearl_current->env);
    }
    // Simulation finished, only catcher left.

    // Finalise pearl debugger information.
    pdb_shutdown();

    // Call statistics on all processes.
    for (unsigned int x = 1; x < pearl_njobs; x++) {
        pearl_current = &pearl_proc[x];
        statistics(x);
    }

    // State recording at the end?
    for (unsigned int x = 1; x < pearl_njobs; x++) {
        pearl_current = &pearl_proc[x];
        newstate(x, READY);
    }
    for (unsigned int x = 1; x < pearl_njobs; x++) {
        pearl_current = &pearl_proc[x];
        prnewstate(x);
    }

    // Collect energy statistics and print Elapsed time and Energy
    // stats.
    energy = collect_energy();

    // TODO: make overflow an error condition in the catcher function.
    printf("\nElapsed time: %.0f units\n", (double) pearl_time);
    printf("Used energy: %.0f units\n", energy);
    if (statsFile) {
        fprintf(statsFile, "\nElapsed time: %.0f units\n",
                (double) pearl_time);
        fprintf(statsFile, "Used energy: %.0f units\n", energy);
    }
    fflush(stdout);

    tend = clock();
    printf("REAL TIME of simulation: %.2f secs.\n",
           (double)tend / CLOCKS_PER_SEC - (double)tstart / CLOCKS_PER_SEC);

    if (statsFile)
        fprintf(statsFile, "REAL TIME of simulation: %.2f secs.\n",
                (double)tend / CLOCKS_PER_SEC - (double)tstart / CLOCKS_PER_SEC);
}
