/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   message.c
 * @author B.J. Overeinder & J.G. Stil
 * @date   1989
 *
 * @brief Message handling functions.
 */

#include "message.h"

#include <stdlib.h>

/**
 * Allocate memory for a message of length 'n'.
 */
messx *allocm(const int n)
{
    messx *t = (messx *) malloc(sizeof(messx));

    t->method = -1;
    t->source = -1;
    t->next = NULL;
    t->length = n;
    t->seq_nr = 0;
    t->flip_nr = 0;
    t->extra = 0;

    int s = n == 0 ? 1 : n;
    t->cont = (int *) malloc(sizeof(int) * s);

    /// TODO: check malloc calls

    return t;
}

/**
 * Return message 'm' to free memory.
 */
void freem(messx *m)
{
    if (m->cont != NULL) {
        free(m->cont);
    }

    free(m);
}
