/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef MESQ_H
#define MESQ_H

#include "message.h"
#include "process.h"

#ifdef __cplusplus
extern "C" {
#endif

    void setup_mtables(void);
    void store_mesg(processx *obj, messx *m);
    void update_sequencer(processx *obj);
    void install_mesg_q(const int obj, int method_list[]);
    messx *find_mesg(processx *obj, int *mask);

#ifdef __cplusplus
}
#endif

#endif // MESQ_H
