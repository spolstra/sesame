#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "pdb.h"

typedef struct message {
    int src;
    int dest;
    long seq;
    long sent;
    long recv;
    int type;
    int args_size;
    int args_capacity;
    int *args;
    long disk_pos;
    struct message *next;
} message;

typedef struct attribute {
    int index;
    int proc;
    int value;
    char *name;
    struct attribute *next;
} attribute;

static long num_events;
static int num_processes;
static int num_message_types;

static long current_time = -1;

static message *messages_pool;
static message *messages_buffered;
static int messages_buffered_num;
static message *messages_pending;
static long message_file_position;

static long **num_process_events;
static long **num_channel_recv_events;
static long **num_channel_send_events;
static long prev_time_event;

static attribute *attributes;
static int num_attributes;

static FILE *master_file = NULL;
static FILE *trace_file = NULL;
static FILE *message_file = NULL;
static FILE *index_file = NULL;

static unsigned char *frame = NULL;
static int frame_num_events = 0;
static int frame_capacity = 0;
static int frame_size = 0;


static int fputi(int i, FILE *file)
{
    return fputc((i >> 24) & 0xff, file) != EOF &&
           fputc((i >> 16) & 0xff, file) != EOF &&
           fputc((i >> 8) & 0xff, file) != EOF &&
           fputc((i >> 0) & 0xff, file) != EOF ? 1 : EOF;
}

static int fputl(long long i, FILE *file)
{
    return fputi((i >> 32), file) != EOF &&
           fputi((i >> 0), file) != EOF ? 1 : EOF;
}

static int frame_ensure(int s)
{
    if (frame_size + s >= frame_capacity) {
        frame_capacity = 2 * frame_capacity + s;
        frame = realloc(frame, frame_capacity);
    }

    return 1;
}

static int frame_write_char(char c)
{
    frame_ensure(1);
    frame[frame_size++] = c;

    return 1;
}

static int frame_write_int(int i)
{
    frame_ensure(4);
    frame[frame_size++] = (i >> 24) & 0xff;
    frame[frame_size++] = (i >> 16) & 0xff;
    frame[frame_size++] = (i >> 8) & 0xff;
    frame[frame_size++] = (i >> 0) & 0xff;

    return 1;
}

/* SP: The parameter i is actually a long long, so it's
 * 64 bits on both 32 and 64 bits machines. */
static int frame_write_long(long long i)
{
    frame_ensure(8);
    frame[frame_size++] = (i >> 56) & 0xff;
    frame[frame_size++] = (i >> 48) & 0xff;
    frame[frame_size++] = (i >> 40) & 0xff;
    frame[frame_size++] = (i >> 32) & 0xff;
    frame[frame_size++] = (i >> 24) & 0xff;
    frame[frame_size++] = (i >> 16) & 0xff;
    frame[frame_size++] = (i >> 8) & 0xff;
    frame[frame_size++] = (i >> 0) & 0xff;

    return 1;
}

static int frame_write_string(char *c)
{
    int n = strlen(c) + 1;

    frame_ensure(n);
    memcpy(&frame[frame_size], c, n);
    frame_size += n;

    return 1;
}

static int frame_begin()
{
    int i, j, n;
    message *mess;
    attribute *attr;

    fputl(num_events, index_file);
    fputl(ftell(trace_file), index_file);

    frame_write_char(PDB_EVENT_SNAPSHOT);
    frame_write_long(num_events);
    frame_write_long(current_time);

    for (i = 0; i < num_processes; i++) {
        for (j = 0; j <= PDB_PROC_EVENT_LAST - PDB_PROC_EVENT_FIRST; j++) {
            frame_write_long(num_process_events[i][j]);
        }
    }

    n = 0;

    for (i = 0; i < num_processes; i++) {
        for (j = 0; j < num_processes; j++) {
            if (num_channel_send_events[i][j] > 0 ||
                num_channel_recv_events[i][j] > 0) {
                n++;
            }
        }
    }

    frame_write_int(n);

    for (i = 0; i < num_processes; i++) {
        for (j = 0; j < num_processes; j++) {
            if (num_channel_send_events[i][j] > 0 ||
                num_channel_recv_events[i][j] > 0) {
                frame_write_int(i);
                frame_write_int(j);
                frame_write_long(num_channel_send_events[i][j]);
                frame_write_long(num_channel_recv_events[i][j]);
            }
        }
    }

    n = 0;

    for (mess = messages_buffered; mess != NULL; mess = mess->next) {
        if (mess->recv == -1) {
            n++;
        }
    }

    for (mess = messages_pending; mess != NULL; mess = mess->next) {
        if (mess->recv == -1) {
            n++;
        }
    }

    frame_write_int(n);

    for (mess = messages_buffered; mess != NULL; mess = mess->next) {
        if (mess->recv == -1) {
            frame_write_long(mess->disk_pos);
        }
    }

    for (mess = messages_pending; mess != NULL; mess = mess->next) {
        if (mess->recv == -1) {
            frame_write_long(mess->disk_pos);
        }
    }

    frame_write_int(num_attributes);

    for (i = 0; i < num_attributes; i++) {
        attr = attributes;

        for (attr = attributes; attr != NULL; attr = attr->next) {
            if (attr->index == i) {
                frame_write_int(attr->value);
            }
        }
    }

    return 1;
}


static int frame_end()
{
    fwrite(frame, sizeof(char), frame_size, trace_file);
    fflush(trace_file);

    frame_num_events = 0;
    frame_size = 0;

    return 1;
}

static long frame_new_event(int event, long time, int proc)
{
    if (frame_num_events >= PDB_MIN_FRAME_SIZE) {
        frame_end();
        frame_begin();
    }

    if (time != current_time) {
        current_time = time;

        frame_write_char(PDB_EVENT_TIME);
        frame_write_long(time);

        frame_num_events++;
        prev_time_event = num_events++;
    }

    if (frame_num_events >= PDB_MIN_FRAME_SIZE) {
        frame_end();
        frame_begin();
    }

    if (PDB_PROC_EVENT_FIRST <= event && event <= PDB_PROC_EVENT_LAST) {
        num_process_events[proc][event - PDB_PROC_EVENT_FIRST]++;
    }

    frame_write_char(event);

    frame_num_events++;
    return num_events++;
}

static int flush_buffered_messages(int force)
{
    int i;
    message *mess = messages_buffered;

    while (mess != NULL && (force || messages_buffered_num > MAX_BUFFERED_MESSAGES || mess->recv != -1)) {
        fputi(mess->src, message_file);
        fputi(mess->dest, message_file);
        fputl(mess->sent, message_file);
        fputl(mess->recv, message_file);
        fputi(mess->type, message_file);
        fputi(mess->args_size, message_file);

        for (i = 0; i < mess->args_size; i++) {
            fputi(mess->args[i], message_file);
        }

        messages_buffered = messages_buffered->next;

        if (mess->recv != -1) {
            mess->next = messages_pool;
            messages_pool = mess;
        } else {
            mess->next = messages_pending;
            messages_pending = mess;
        }

        mess = messages_buffered;

        messages_buffered_num--;
    }

    if (force) {
        fflush(message_file);
    }

    return 1;
}


int pdb_init(char *path, int pnum, int mnum)
{
    int n, i;
    char filename[4096];

    if ((master_file = fopen(path, "wb")) == NULL) {
        fprintf(stderr, "Failed to open file '%s' for writing\n", path);
        return 0;
    }

    fputs(PDB_MAGIC_WORD_MASTER, master_file);
    fputi(pnum, master_file);
    fputi(mnum, master_file);
    fflush(master_file);


    snprintf(filename, sizeof(filename), "%s.trace", path);

    if ((trace_file = fopen(filename, "wb")) == NULL) {
        fprintf(stderr, "Failed to open file '%s' for writing\n", filename);
        return 0;
    }

    fputs(PDB_MAGIC_WORD_TRACE, trace_file);
    fflush(trace_file);


    snprintf(filename, sizeof(filename), "%s.index", path);

    if ((index_file = fopen(filename, "wb")) == NULL) {
        fprintf(stderr, "Failed to open file '%s' for writing\n", filename);
        return 0;
    }

    fputs(PDB_MAGIC_WORD_INDEX, index_file);
    fflush(index_file);

    snprintf(filename, sizeof(filename), "%s.message", path);

    if ((message_file = fopen(filename, "wb")) == NULL) {
        fprintf(stderr, "Failed to open file '%s' for writing\n", filename);
        return 0;
    }

    fputs(PDB_MAGIC_WORD_MESSAGE, message_file);
    fflush(message_file);

    num_events = 0;
    current_time = 0;
    message_file_position = ftell(message_file);

    frame_capacity = 1024;
    frame_size = 0;
    frame_num_events = 0;
    frame = malloc(sizeof(char) * frame_capacity);

    num_message_types = mnum;
    num_processes = pnum;

    messages_pool = NULL;
    messages_buffered = NULL;
    messages_buffered_num = 0;
    messages_pending = NULL;

    attributes = NULL;
    num_attributes = 0;

    prev_time_event = -1;
    num_process_events = calloc(sizeof(long *), num_processes);

    for (i = 0; i < num_processes; i++) {
        n = PDB_PROC_EVENT_LAST - PDB_PROC_EVENT_FIRST + 1;
        num_process_events[i] = calloc(sizeof(long), n);
    }

    num_channel_send_events = calloc(sizeof(long *), num_processes);
    num_channel_recv_events = calloc(sizeof(long *), num_processes);

    for (i = 0; i < num_processes; i++) {
        num_channel_send_events[i] = calloc(sizeof(long), num_processes);
        num_channel_recv_events[i] = calloc(sizeof(long), num_processes);
    }

    frame_begin();

    return 1;
}

int pdb_register_process(char *name, char *className)
{
    if (master_file == NULL) {
        return 0;
    }

    if (className == NULL) {
        className = "";
    }

    fwrite(name, sizeof(char), strlen(name) + 1, master_file);
    fwrite(className, sizeof(char), strlen(className) + 1, master_file);

    fflush(master_file);

    return 1;
}

int pdb_register_message_type(char *name)
{
    if (master_file == NULL) {
        return 0;
    }

    fwrite(name, sizeof(char), strlen(name) + 1, master_file);
    fflush(master_file);

    return 1;
}

int pdb_record_send(long time, int proc, int dest, long seq,
                    int type, int argc, int *argv)
{
    message *mess;
    message *tail;
    int event_index;

    if (master_file == NULL) {
        return 0;
    }

    if (messages_pool != NULL) {
        mess = messages_pool;
        messages_pool = messages_pool->next;
    } else {
        mess = calloc(sizeof(message), 1);
    }

    if (argc > mess->args_capacity) {
        free(mess->args);
        mess->args = malloc(sizeof(int) * argc);
        mess->args_capacity = argc;
    }

    event_index = frame_new_event(PDB_EVENT_SEND, time, proc);
    frame_write_int(proc);
    frame_write_int(dest);
    frame_write_long(message_file_position);

    mess->src = proc;
    mess->dest = dest;
    mess->seq = seq;
    mess->sent = event_index;
    mess->recv = -1;
    mess->type = type;
    mess->args_size = argc;
    mess->disk_pos = message_file_position;
    mess->next = NULL;
    memcpy(mess->args, argv, argc * sizeof(int));

    if (messages_buffered != NULL) {
        for (tail = messages_buffered; tail->next != NULL; tail = tail->next);

        tail->next = mess;
    } else {
        messages_buffered = mess;
    }

    messages_buffered_num++;
    message_file_position += 4 + 4 + 8 + 8 + 4 + 4 + 4 * argc;

    flush_buffered_messages(0);

    num_channel_send_events[proc][dest]++;

    return 1;
}


int pdb_record_receive(long time, int proc, long seq)
{
    message *prev;
    message *mess;
    int event_index;

    if (master_file == NULL) {
        return 0;
    }

    mess = messages_buffered;

    while (mess != NULL && (mess->dest != proc || mess->seq != seq || mess->recv != -1)) {
        mess = mess->next;
    }

    if (mess != NULL) {
        event_index = frame_new_event(PDB_EVENT_RECV, time, proc);
        frame_write_int(proc);
        frame_write_int(mess->src);
        frame_write_long(mess->disk_pos);

        mess->recv = event_index;

        flush_buffered_messages(0);

        num_channel_recv_events[mess->src][proc]++;

        return 1;
    }

    prev = NULL;
    mess = messages_pending;

    while (mess != NULL && (mess->dest != proc || mess->seq != seq)) {
        prev = mess;
        mess = mess->next;
    }

    if (mess != NULL) {
        if (prev != NULL) {
            prev->next = mess->next;
        } else {
            messages_pending = mess->next;
        }

        mess->next = messages_pool;
        messages_pool = mess;

        event_index = frame_new_event(PDB_EVENT_RECV, time, proc);
        frame_write_int(proc);
        frame_write_int(mess->src);
        frame_write_long(mess->disk_pos);

        fseek(message_file, mess->disk_pos + 4 + 4 + 8, SEEK_SET);
        fputl(event_index, message_file);
        fseek(message_file, 0, SEEK_END);

        num_channel_recv_events[mess->src][proc]++;

        return 1;
    }


    fprintf(stderr, "PDB Fatal error, received message that was never sent\n");
    pdb_record_log(time, proc, "Received message that was never sent");
    pdb_shutdown();

    return 0;
}


int pdb_record_sendreceive(long time, int src, int dest,
                           int type, int argc, int *argv)
{
    return pdb_record_send(time, src, dest, -1, type, argc, argv) &&
           pdb_record_receive(time, dest, -1);
}


int pdb_record_sleep(long time, int proc, long timeout, int *mask)
{
    char c = 0;
    int i;

    if (master_file == NULL) {
        return 0;
    }

    frame_new_event(PDB_EVENT_SLEEP, time, proc);
    frame_write_int(proc);
    frame_write_long(timeout);

    if (mask != NULL) {
        for (i = 0; i < num_message_types; i++) {
            if (mask[i] >= 0) {
                c |= 0x80 >> (i % 8);
            }

            if ((i + 1) % 8 == 0 || i + 1 == num_message_types) {
                frame_write_char(c);
                c = 0;
            }
        }

    } else {
        for (i = num_message_types; i > 0; i -= 8) {
            frame_write_char(0);
        }
    }

    return 1;
}


int pdb_record_log(long time, int proc, char *message)
{
    if (master_file == NULL) {
        return 0;
    }

    frame_new_event(PDB_EVENT_LOG, time, proc);
    frame_write_int(proc);
    frame_write_string(message);

    return 1;
}

// TODO: pearl time is timet (unsigned long long) not long.
int pdb_record_attribute(long time, int proc, char *name, int value)
{
    attribute *attr = attributes;
    attribute *prev = NULL;

    if (master_file == NULL) {
        return 0;
    }

    while (attr != NULL && (attr->proc != proc || strcmp(name, attr->name))) {
        prev = attr;
        attr = attr->next;
    }

    if (attr == NULL) {
        attr = malloc(sizeof(attribute));
        attr->name = malloc(sizeof(char) * (strlen(name) + 1));

        strcpy(attr->name, name);
        attr->proc = proc;
        attr->index = num_attributes++;

        prev = NULL;
        attr->next = attributes;
        attributes = attr;

        fputi(proc, master_file);
        fwrite(name, sizeof(char), strlen(name) + 1, master_file);
        fflush(master_file);
    }

    attr->value = value;

    frame_new_event(PDB_EVENT_ATTRIBUTE, time, proc);
    frame_write_int(attr->index);
    frame_write_int(attr->value);

    if (prev != NULL) {
        prev->next = attr->next;
        attr->next = attributes;
        attributes = attr;
    }

    return 1;
}

int pdb_shutdown()
{
    int i;
    message *mess;
    message *next_mess;
    attribute *attr;
    attribute *next_attr;

    if (master_file == NULL) {
        return 0;
    }

    flush_buffered_messages(1);
    frame_end();

    fclose(master_file);
    fclose(index_file);
    fclose(trace_file);
    fclose(message_file);

    master_file = NULL;
    index_file = NULL;
    trace_file = NULL;
    message_file = NULL;


    for (mess = messages_pool; mess != NULL; mess = next_mess) {
        next_mess = mess->next;
        free(mess->args);
        free(mess);
    }

    for (mess = messages_pending; mess != NULL; mess = next_mess) {
        next_mess = mess->next;
        free(mess->args);
        free(mess);
    }

    for (mess = messages_buffered; mess != NULL; mess = next_mess) {
        next_mess = mess->next;
        free(mess->args);
        free(mess);
    }

    for (attr = attributes; attr != NULL; attr = next_attr) {
        next_attr = attr->next;
        free(attr->name);
        free(attr);
    }

    for (i = 0; i < num_processes; i++) {
        free(num_process_events[i]);
        free(num_channel_send_events[i]);
        free(num_channel_recv_events[i]);
    }

    free(num_process_events);
    free(num_channel_send_events);
    free(num_channel_recv_events);
    free(frame);

    return 1;
}
