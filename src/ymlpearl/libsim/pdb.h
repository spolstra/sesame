#ifndef PDB_H
#define PDB_H


/**
 * Magic word for master files, the first few characters of a master file should
 * be these letters. Can be used to identify trace files and to detect corrupt
 * files.
 */
#define PDB_MAGIC_WORD_MASTER ("%PDBMASTER")

/**
 * Magic word for trace files.
 */
#define PDB_MAGIC_WORD_TRACE ("%PDBTRACE")


/**
 * Magic word for index files.
 */
#define PDB_MAGIC_WORD_INDEX ("%PDBINDEX")

/**
 * Magic word for messages files.
 */
#define PDB_MAGIC_WORD_MESSAGE ("%PDBMESSAGE")


/**
 * Minimal number of events in one frame.
 */
#define PDB_MIN_FRAME_SIZE (1024)


/**
 * The maximum amount of messages to keep in the memory before writing them to
 * the disk.
 */
#define MAX_BUFFERED_MESSAGES (1024)




/**
 * Events that are stored by the trace collector.
 */
#define PDB_EVENT_SNAPSHOT    (1)
#define PDB_EVENT_TIME        (2)
#define PDB_EVENT_SEND        (3)
#define PDB_EVENT_RECV        (4)
#define PDB_EVENT_SLEEP       (5)
#define PDB_EVENT_LOG         (6)
#define PDB_EVENT_ATTRIBUTE   (7)


#define PDB_PROC_EVENT_FIRST (PDB_EVENT_SEND)
#define PDB_PROC_EVENT_LAST  (PDB_EVENT_ATTRIBUTE)


/**
 * Initialize the debugger, opens the specified file and writes the magic
 * word. After calling this function it is safe to call
 * pdb_register_process, then pdb_register_message,
 * then any of the pdb_record_* and finally pdb_shutdown
 * to flush the buffers. After shutting the debugger down it is safe to
 * initialize it again.
 *
 * @param filename
 *      The file to which the event trace should be written. The file is
 *      created if it does not exist, otherwise it is truncated.
 * @return
 *      1 if the debugger was successfully initialized, 0 if the debugger was
 *      initialized before but pdb_shutdown was never called.
 */
int pdb_init(char *filename, int process_num, int message_num);


/**
 * Register an object with the specified name. This function should be called
 * right after pdb_init but before pdb_register_message
 * is called or any of the pdb_record_* functions.
 *
 * @param name
 *      The name of the object
 * @param name
 *      The class of the object
 * @return
 *      The index of this process or -1 if an error ocurred and the process
 *      could not be registered.
 */
int pdb_register_process(char *name, char *className);


/**
 * Register a message with the specified name. This function should be called
 * right after pdb_init and after pdb_register_process
 * but before any of the pdb_record_* function are called
 *
 * @param name
 *      The name of the message
 * @return
 *      The index of this message or -1 if an error ocurred and the message
 *      could not be registered.
 */
int pdb_register_message_type(char *name);


/**
 * A process has sent a message to another process.
 *
 * @param time
 *      Current time of simulation.
 * @param src_proc
 *      The process that has sent the message.
 * @param dest_proc
 *      The process that has received the message.
 * @param seq
 *      The sequence number of the message.
 * @param type
 *      The type of the message (registered using pdb_register_message).
 * @param argc
 *      Number of arguments
 * @param argv
 *      Array of arguments.
 */
int pdb_record_send(long time, int src_proc, int dest_proc, long seq,
                    int type, int argc, int *argv);

/**
 * A process received a message.
 *
 * @param time
 *      Current time of simulation.
 * @param proc
 *      The process that received the message.
 * @param seq
 *      Sequence number of process, see pdb_record_send
 */
int pdb_record_receive(long time, int proc, long seq);

/**
 * A process has sent a message and the recipient immediately received the
 * message.
 *
 * @param time
 *      Current time of simulation.
 * @param src
 *      The process that has sent the message.
 * @param dest
 *      The process that has received the message.
 * @param seq
 *      The sequence number of the message.
 * @param type
 *      The type of the message (registered using pdb_register_message).
 * @param argc
 *      Number of arguments
 * @param argv
 *      Array of arguments.
 */
int pdb_record_sendreceive(long time, int src, int dest,
                           int type, int argc, int *argv);

/**
 * A process is going to sleep until it receive a message or a fixed number of
 * time steps pass by.
 *
 * @param time
 *      Current time of simulation.
 * @param proc
 *      The process that is going to sleep
 * @param timeout
 *      Maximum amount of time units to wait, or -1 for infinity
 * @param mask
 *      The messages that this process is allowed to receive, or NULL if this
 *      process is blocked until the timeout expires. If mask[i] > 0 then
 *      this process will wake up if it receive a message of type i.
 */
int pdb_record_sleep(long time, int proc, long timeout, int *mask);


/**
 * Log an informative message about a process,
 *
 * @param time
 *      Current time of simulation.
 * @param proc
 *      The process that logged the message.
 * @param message
 *      The message.
 */
int pdb_record_log(long time, int proc, char *message);


/**
 * Log the value of an attribute of a process.
 *
 * @param proc
 *      The process that logged the message.
 * @param attribute
 *      The name of the attribute.
 * @param value
 *      The value of the attribute.
 */
int pdb_record_attribute(long time, int proc, char *attribute, int value);


/**
 * Shut the debugger down, flush the buffer and close the file stream. After
 * calling this function it is safe to initialize the debugger again with a
 * new file.
 */
int pdb_shutdown();

#endif
