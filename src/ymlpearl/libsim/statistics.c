/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "process.h"
#include "global.h"


static float threshold = 0.0;
FILE *statsFile = NULL;

typedef struct statx {
    double value;
    processx *object;
    char *string;
    char line[4];
} statx;

static int nstat, maxstat;
static statx **statis;
static char statclass[256];

void prstatname(const char *s)
{
    if (s == NULL) {
        strcpy(statclass, pearl_current->name);
    } else {
        strcat(statclass, "-");
        strcat(statclass, s);
    }
}

static int linelen;
static char *line;

void prstretch(int i)
{
    i += 80;

    if (i > linelen) {
        linelen = i;
        line = realloc(line, linelen);
    }
}

void prstat(double importance, char *fmt, ...)
{
    va_list ap;
    statx *st;

    va_start(ap, fmt);

    if (importance >= threshold) {
        if (linelen == 0) {
            linelen = 512;
            line = malloc(linelen);
        }

        vsprintf(line, fmt, ap);
        st = (statx *) malloc(sizeof(statx) + strlen(line));
        strcpy(st->line, line);
        st->object = pearl_current;
        st->string = strcpy(malloc(strlen(statclass) + 1), statclass);
        st->value = importance;

        if (nstat == maxstat) {
            if (maxstat == 0) {
                maxstat = 20;
                statis = (statx **) malloc(sizeof(statx *) * maxstat);
            } else {
                maxstat *= 2;
                statis = (statx **) realloc(statis, sizeof(statx *) * maxstat);
            }
        }

        statis[nstat ++] = st;
    }

    va_end(ap);
}

int compstat(statx **a, statx **b)
{
    double z = (*b)->value - (*a)->value;

    if (z < 0) {
        return -1;
    }

    if (z > 0) {
        return 1;
    }

    // If equal importance, sort alphabetically.
    return strcmp((*a)->string, (*b)->string);
}


void emitstat()
{
    int len;
    char *s, *x;
    int first;

    len = 1;

    for (int i = 0; i < nstat; i++) {
        if ((int)strlen(statis[i]->string) > len) {
            len = strlen(statis[i]->string);
        }
    }

    qsort(statis, nstat, sizeof(statx *),
          (int ( *)(const void *, const void *))compstat);

    for (int i = 0; i < nstat; i++) {
        /* fprintf(statsFile, "%*s ", -len, statis[i]->object->name); */
        fprintf(statsFile, "%*s ", -len, statis[i]->string);
        s = statis[i]->line;
        first = 1;

        do {
            x = strchr(s, '\n');

            if (x != NULL) {
                *x = '\0';
            }

            if (*s != '\0') {
                fprintf(statsFile, "%*s %s\n", first ? 0 : len + 1 , "", s);
                first = 0;
            }

            s = x + 1;
        } while (x != NULL);
    }
}

void Sprintf(const char *format, ...)
{
    va_list ap;

    if (!statsFile) {
        statsFile = stdout;
    }

    va_start(ap, format);
    vfprintf(statsFile, format, ap);
    va_end(ap);
}
