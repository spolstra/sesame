/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file   message.h
 * @author B.J. Overeinder & J.G. Stil
 * @date   1989
 *
 * @brief Message data structure.
 */

#ifndef MESSAGE_H
#define MESSAGE_H

typedef struct messx {
    int          method;          //!< destination method of message
    int          source;          //!< source proces
    struct       messx *next;     //!< next message
    int          length;          //!< message length
    unsigned int seq_nr, flip_nr; //!< sequence number + overflow counter
    int          extra;           //!< TODO
    int          *cont;           //!< message data
} messx;

#ifdef __cplusplus
extern "C" {
#endif

    messx *allocm(const int n);
    void freem(messx *m);

#ifdef __cplusplus
}
#endif
#endif // MESSAGE_H
