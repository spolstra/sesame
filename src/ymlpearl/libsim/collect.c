/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>

#include "collect.h"
#include "process.h"
#include "message.h"
#include "event.h"
#include "statistics.h"
#include "misc.h"
#include "startup.h"
#include "global.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

/* Forward Delcarations */
void collectmethods(int object);

double ftimer();

typedef struct statx {
    double *busys;      /* How long busy with i clients */
    double *idles;      /* How long idle with i clients */
    int maxlen;       /* Length of busys and idles */
    int surprises;      /* Surprising events */
    int savestate;      /* Old state of object */
    double savetime;      /* Old time of object */
    int k;        /* number of items */
    int *mcount;        /* Number of times method called */
    double *mtimes;     /* Time spent in methods */
    int *bits;        /* Number of bytes sent */
} statx;

static statx *stats;
static int nproc;

/**
 * initialize data structures used for statistics
 *
 * @param np number of processes
 */
void minitstate(const int np)
{
    nproc = np;
    stats = (statx *)malloc(sizeof(statx) * nproc);

    for (int i = 0; i < nproc; i++) {
        stats[i].maxlen = 1;
        stats[i].busys  = (double *) calloc(sizeof(double), stats[i].maxlen);
        stats[i].idles  = (double *) calloc(sizeof(double), stats[i].maxlen);
        stats[i].mcount = (int *)   calloc(sizeof(int), pearl_nlabels);
        stats[i].mtimes = (double *) calloc(sizeof(double), pearl_nlabels);
        stats[i].surprises = 0;
        stats[i].savetime = 0.0;
        stats[i].savestate = TIME;
    }
}

static double powint(const double d, int n)
{
    double r = 1.0;

    while (n-- > 0) {
        r *= d;
    }

    return r;
}

void prnewstate(const int process)
{
    double maximp, imp;
    statx *pr = &stats[process];
    char str[1024];
    double time = ftimer();

    if (stats == NULL) {
        return;
    }

    prstatname(NULL);

    if (pr->surprises) {
        prstatname("surprises");
        prstat(2.0, "%d surprises\n", pr->surprises);
        prstatname(NULL);
    }

    if (time == 0.0) {
        return;
    }

    maximp = -100.0;
    prstatname("time");
    strcpy(str, "idle %");

    if (pr->maxlen > 13) {
        pr->maxlen = 13;
    }

    for (int i = 0; i < pr->maxlen; i++) {
        int perc = (int)((200.0 * pr->idles[i] + time) / time / 2.0);
        imp = 1.0 - powint(1.0 - perc / 100.0, i + 1);

        if (imp > maximp) {
            maximp = imp;
        }

        sprintf(str + strlen(str), " %3d", perc);
    }

    if (pr->maxlen == 13) {
        strcat(str, "...");
    }

    strcat(str, "\nbusy %");

    for (int i = 0; i < pr->maxlen; i++) {
        int perc = (int)((200.0 * pr->busys[i] + time) / time / 2.0);
        imp = 1.0 - powint(1.0 - perc / 100.0, i + 1);

        if (imp > maximp) {
            maximp = imp;
        }

        sprintf(str + strlen(str), " %3d", perc);
    }

    if (pr->maxlen == 13) {
        strcat(str, "...");
    }

    prstat(maximp, "%s\n", str);
    prstatname(NULL);
    collectmethods(process);
}

static void beenbusy(const int process, const int qlen, const double tlen)
{
    stats[process].busys[qlen] += tlen;
}

static void beenidle(const int process, const int qlen, const double tlen)
{
    stats[process].idles[qlen] += tlen;
}

void arrayrealloc(statx *pr, const int len)
{
    const int olen = pr->maxlen;

    pr->maxlen = len + 1;
    pr->busys = (double *)realloc(pr->busys, sizeof(double) * pr->maxlen);
    pr->idles = (double *)realloc(pr->idles, sizeof(double) * pr->maxlen);

    for (int i = olen; i < (len + 1); i++) {
        pr->busys[i] = pr->idles[i] = 0.0;
    }
}

void newstate(const int process, const int state)
{
    statx *pr;
    int qlen;
    double tlen, time = ftimer();

    if (stats == NULL || process == 0) {
        return;
    }

    pr = &stats[process];
    qlen = pearl_proc[process].mql;
    tlen = time - pr->savetime;

    switch (pr->savestate) {
        case TIME:
        case TIME_RECV:
            if (qlen >= pr->maxlen) {
                arrayrealloc(pr, qlen);
            }

            beenbusy(process, qlen, tlen);
            break;

        case RECV:
            if (qlen >= pr->maxlen) {
                arrayrealloc(pr, qlen);
            }

            beenidle(process, qlen, tlen);
            break;

        case READY:
            if (tlen != 0.0) {
                pr->surprises ++;
            }

            break;

        default :
            panic("Unkown state %d in object\n", pr->savestate);
    }

    pr->savestate = state;
    pr->savetime = time;
}

void collectsend(const int dest, const messx * UNUSED(m))
{
    newstate(dest, pearl_proc[dest].state);
}

void collectrecv(const int dest, const messx *m)
{
    if (stats != NULL) {
        stats[dest].mcount[m->method] ++;
    }
}

void collectsendrecv(const int UNUSED(src), const int dest, const int method, const int UNUSED(length))
{
    if (stats != NULL) {
        stats[dest].mcount[method] ++;
    }
}

void collectmethods(const int object)
{
    int nmore = 0;
    int totm = 0;

    if (stats == NULL) {
        return;
    }

    for (unsigned int i = 0; i < pearl_nlabels; i++) {
        const int calls = stats[object].mcount[i];
        totm += calls;

        if (calls > 1) {
            nmore ++;
        }
    }

    if (totm <= 1) {
        return;
    }

    for (unsigned int i = 0; i < pearl_nlabels; i++) {
        const int calls = stats[object].mcount[i];

        if (calls <= 1) {
            continue;
        }

        prstatname("method");
        prstatname(pearl_methodname(i));
        prstat(fabs(calls / (1.0 * totm / nmore) - 1.0),
               "%d calls %3.0f%%\n", calls, calls * 100.0 / totm);
        prstatname(NULL);
    }
}
