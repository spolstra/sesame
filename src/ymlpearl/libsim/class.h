/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file class.h
 * @author B.J. Overeinder & J.G. Stil
 * @date   Fri May 16 13:08:41 CEST 2014
 *
 * @brief Data structures for Pearl classes.
 */

#ifndef CLASS_H
#define CLASS_H

#include <stdint.h>

/**
 * Element of the list of superclasses of a class.
 */
typedef struct subtypex {
    char *name;                 //!< Superclass name.
    struct subtypex *next;      //!< Next element in super list.
} subtypex;

/**
 * \typedef
 * \struct classx "class.h"
 * Runtime class information.
 * (For some reason doxygen merges this with classx in the compiler)
 */
typedef struct {
    char *name;              //!< Class name
    intptr_t (*func)();      //!< Process create function: classname_create
    intptr_t *(*initfunc)(); //!< Process init function: classname_initiate
    subtypex *subtypes;      //!< list of supers of this class.
} classx;

extern int nclasses;
extern classx *cllist;

#ifdef __cplusplus
extern "C" {
#endif
    extern classx *findclass(const char *name);
    int installclass(intptr_t (*func)(), char *name, intptr_t * (*initfunc)());
    int installsubtype(const int class_id, char *name);
#ifdef __cplusplus
}
#endif

#endif // CLASS_H
