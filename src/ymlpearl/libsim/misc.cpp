/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file misc.cpp
 * @author B.J. Overeinder & J.G. Stil
 * @date   1989
 *
 * @brief Miscellaneous functions.
 */

#include <stdarg.h>
#include <stdlib.h>

#include "misc.h"
#include "global.h"

#include <string>
#include <cstdio>

#include "../../BasicUtils/BasicException.h"
#include "../../BasicUtils/BasicString.h"

using namespace std;

/**
 * Print information about an object. This includes the state it is in
 * and the time it is scheduled to run.
 *
 * @param obj   Object to print.
 * @return      string with object information.
 */
string print_object(processx *obj)
{
    string objStr = BasicString(obj - pearl_proc) + " \"" + obj->name + "\" ";

    switch (obj->state) {
        case TIME:
            objStr += "time-wait";
            break;

        case RECV:
            objStr += "mess-wait";
            break;

        case TIME_RECV:
            objStr += "time/mess-wait";
            break;

        case READY:
            objStr += "ready";
            break;

        default:
            objStr = objStr + "(state " + BasicString((int)obj->state) + ")";
    }

    objStr = objStr + " time " + BasicString(obj->time);

    return objStr;
}


/**
 * Print panic message and print the state of all the objects in the
 * simulation.
 *
 * @param pformat   Error message
 * @return Does not return. This function stops the simulation.
 */
void panic(char *pformat, ...)
{
    va_list ap;
    va_start(ap, pformat);

    string errStr;
    string name;

    if (pearl_current) {
        name = string(pearl_current->name) + " class " + pearl_current->className;
    } else {
        name = "<UNKNOWN OBJECT>";
    }

    errStr = string("PANIC IN OBJECT ") + name;

    if (!pearl_head) {
        errStr += " during initialisation";
    }

    errStr += "\n    ";

    char buf[512];
    vsnprintf(buf, 512, pformat, ap);
    errStr += buf;

    va_end(ap);

    if (pearl_proc) {
        for (unsigned int i = 1; i < pearl_njobs; i++) {
            errStr += string("\n    ") + print_object(&pearl_proc[i]);
        }
    }

    // We can't throw C++ exceptions through the C stack frame!
    // However, this will output the stack trace when enabled.
    cerr << BasicException(errStr) << endl;
    exit(1);
}
