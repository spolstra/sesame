/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file mesq.c
 * @author Andy Pimentel
 * @date 1994
 *
 * @brief Message-queue handling.
 * A try to improve the message-queue handling in Pearl.
 * Simulations creating (very) long message-queues will _certainly_
 * benefit from this hack.
 */

#include "mesq.h"
#include "global.h"

#include <stdio.h>
#include <stdlib.h>

/** Messages are tagged with a sequence number to maintain ordering.
 * When the counter hits 'MAX_SEQ' the sequence counter is reset, and
 * the 'flip_nr' is incremented. @see messx */
#define MAX_SEQ     2000000000

/** A 2d array that maps [pearlproc_id][method_id] to a
 * hash slot in that pearlproc's processx.mqs
 */
static int **mtranstab = NULL;

/** Contains the size of the mtranstab for every pearl
 * object.
 */
static int *mtranssizes = NULL;

static int max = -1; //!< Global maximum of all method_id's.


/**
 * Setup 'mesg_q' for pearl component with 'obj' id
 * using the 'method_list' id's of that component.
 * Update global 'max' method_id when old maximum is exceeded.
 * This function is called by X_create function in generated module code.
 *
 *
 * @param obj    index in pearl_proc process table.
 * @param method_list  array of method ids prefixed with array length
 */
void install_mesg_q(const int obj, int method_list[])
{
    processx *p = &pearl_proc[obj];

    p->mqsize    = method_list[0]; // first entry contains method count
    p->mqs       = (mesg_q *) malloc(p->mqsize * sizeof(mesg_q));

    for (int i = 1; i <= p->mqsize; i++) {
        if (method_list[i] > max) {
            max = method_list[i];
        }

        p->mqs[i - 1].method_id = method_list[i];
        p->mqs[i - 1].mlist     = NULL;
        p->mqs[i - 1].mtail     = NULL;
    }
}

/** Allocate and initialise the translation table and the translation
 * size table.  The translation size table is set to largest method_id
 * found during install_mesg_q().  The translation table maps
 * (pearl_id, method_id) => hash slot entry in processx.mqs table.
 * @see mtranstab
 * @see mtranssizes
 * @see processx
 */
void setup_mtables()
{
    processx *p;

    if ((mtranstab = (int **)malloc(pearl_njobs * sizeof(int *))) == NULL) {
        fprintf(stderr, "Can't malloc!\n");
        exit(1);
    }

    if ((mtranssizes = (int *)malloc(pearl_njobs * sizeof(int *))) == NULL) {
        fprintf(stderr, "Can't malloc!\n");
        exit(1);
    }

    for (unsigned int j = 0; j < pearl_njobs; j++) {
        p = &pearl_proc[j];

        if (p->mqs != NULL) {
            if ((mtranstab[j] = (int *)malloc((max + 1) * sizeof(int))) == NULL) {
                fprintf(stderr, "Can't malloc!\n");
                exit(1);
            }

            mtranssizes[j] = max + 1;

            for (int i = 0; i < p->mqsize; i++) {
                mtranstab[j][p->mqs[i].method_id] = i;
            }
        }
    }
}

/** Find the oldest message in the message queue of 'obj' that is
 * included in 'mask'.  Because every methods has his own hash slot in
 * obj->mqs, messages for all the methods of this obj are examined.
 * If a message is found it is removed from the message queue.
 *
 * @param  obj  Pointer to pearl object
 * @param  mask Set of methods that the pearl object will accept
 * @return      The oldest message that matches or NULL if no suitable
 *              message is found.
 */
messx *find_mesg(processx *obj, int *mask)
{
    mesg_q *mq = obj->mqs;
    unsigned int sav_seq = 0;
    unsigned int sav_flip = 0;
    int nr = 0;
    int full_slots;
    messx *the_mesg = NULL, *m;

    full_slots = obj->mq_fs;

    /* Check every slot in the table, but stop if we know that there
     * are no full slots left. */
    for (int i = 0; i < obj->mqsize && full_slots; i++) {

        if (mq[i].mlist != NULL) {
            full_slots--;

            /* Check if method is included in mask */
            if (test_mask(mask, mq[i].method_id)) {
                m = mq[i].mlist;

                /* Set the_mesg if we haven't found one, or if this
                 * message is newer than our previously found message.*/
                if (the_mesg == NULL || (m->seq_nr < sav_seq &&
                                         m->flip_nr <= sav_flip)) {
                    the_mesg = m;
                    nr       = i;
                    sav_seq  = m->seq_nr;
                    sav_flip = m->flip_nr;
                }
            }
        }
    }

    /* If found, remove message from list and return it. */
    if (the_mesg != NULL) {
        mq[nr].mlist = the_mesg->next;

        if (mq[nr].mtail == the_mesg) {
            mq[nr].mtail = NULL;
            obj->mq_fs--;
        }

        obj->mql--;
        return the_mesg;

    } else {
        return NULL;
    }
}

/** Add message 'm' to the correct mesg queue slot of the
 * pearl process 'obj'. Messages are added to the end of list and the
 * if the slot was empty 'mq_fs' is incremented.
 *
 * @param obj Pearl object
 * @param m   Message to add
 */
void store_mesg(processx *obj, messx *m)
{
    mesg_q *mq = obj->mqs;
    int j;

    if (mtranssizes[obj - pearl_proc] <= m->method) {
        fprintf(stderr, "invalid method %d from %s to %s.\n",
                m->method, pearl_current->name, obj->name);
        return;
    }

    j = mtranstab[obj - pearl_proc][m->method];

    if (mq[j].mtail == NULL) {
        mq[j].mlist = m;
        obj->mq_fs++;

    } else {
        mq[j].mtail->next = m;
    }

    mq[j].mtail = m;
}

/** Increment sequence counter. Keep track of the number of times the
 * counter passes through MAX_SEQ.
 *
 * @param obj Pearl object to update
 */
void update_sequencer(processx *obj)
{
    if (obj->seq_nr < MAX_SEQ) {
        obj->seq_nr++;
    } else {
        obj->seq_nr = 0;
        obj->flip_nr++;
    }
}
