/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * Pearl Compiler (c) 1989 University of Amsterdam
 *
 * @file startup.c
 * @author B.J. Overeinder & J.G. Stil
 * @date  Fri May 16 13:08:41 CEST 2014
 *
 * @brief Functions related to simulation.
 *
 * Startup functions:
 * * Functions to create Pearl processes.
 * * Register methods.
 * * Blocker initialisation.
 *
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "misc.h"
#include "schedule.h"
#include "blocker.h"
#include "global.h"
#include "class.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

unsigned int pearl_njobs = 0;  ///< Total number of Pearl proc's in simulation

static char **methodnames = NULL; ///< Translate method id's to names.


/**
 * Replace '~' with '\0'.
 * TODO: `~` are not inserted in the process names.
 */
static void terminatename(int UNUSED(object), char *str)
{
    char *t;

    if ((t = strchr(str, '~')) != NULL) {
        *t = '\0';
    }
}

/**
 * After the pearl modules installed all their methods we know the
 * total number of methods in the system and we can allocate the
 * blocker table with a call to blocker_init.
 */
void modulesinstalled()
{
    blocker_init(pearl_nlabels);
}

/**
 * All pearl modules (classes) register their methods here.
 * The generated function `main_install_modules` calls
 * the generated function `<classname>_module` for every class in the
 * simulation. And these functions will register all their methods
 * here.
 *
 * @param name      Name of the method to install.
 * @return          Method id.
 */
int pearl_methodinstall(char *name)
{
    static unsigned int nlabelmax = 0;

    if (nlabelmax == 0) {
        nlabelmax = 10;
        methodnames = (char **)malloc(sizeof(char *) * nlabelmax);
    }

    if (pearl_nlabels == nlabelmax - 1) {
        nlabelmax *= 2;
        methodnames = (char **)realloc(methodnames, sizeof(char *) * nlabelmax);
    }

    methodnames[pearl_nlabels] = name; // methodnames: m_id --> m_name

    return pearl_nlabels++; // Track the total nr of installed methods.
}

/**
 * Return the method name of method id @p method.
 *
 * @param method    Method id.
 * @return          Method name.
 */
char *pearl_methodname(const int method)
{
    return methodnames[method];
}

/**
 * WIP
 */
double collect_energy()
{
    unsigned int i;
    double energy = 0.0;
    processx *oldc;

    oldc = pearl_current;

    for (i = 0; i < pearl_njobs; ++i) {
        pearl_current = pearl_proc + i;

        if (pearl_current->efunc) { // Always NULL in this version.
            (*pearl_current->efunc)(pearl_current->env);
        }

        energy += pearl_current->energy;
    }

    pearl_current = oldc;

    return energy;
}

/**
 * Create an entry in the process table for a Pearl object and link
 * the function pointer and the environment pointer.
 * This function is indirectly called by #createchild:
 * #createchild `--> <classname>_create -->` #create_proc
 *
 * @param name          Process entry name.
 * @param func          Entry point of the Pearl process instance.
 * @param env           Process environment.
 * @param classname     Class name of process.
 * @param stats_state   Statistics state identifier.
 * @return              Index of created process.
 */
int create_proc(char *name, function func, intptr_t *env, char *classname,
                int stats_state)
{
    static unsigned maxjobs = 0;

    // Increase pearl_proc table size when needed.
    if (pearl_njobs >= maxjobs) {

        // First time? Setup Catcher process.
        if (maxjobs == 0) {
            maxjobs = 20;
            pearl_proc = (processx *) malloc(sizeof(processx) * maxjobs);
            pearl_proc[pearl_njobs].name = "Catcher";
            pearl_proc[pearl_njobs].func = catcherfunc;
            pearl_proc[pearl_njobs].efunc = NULL;
            pearl_proc[pearl_njobs].energy = 0.0;
            pearl_proc[pearl_njobs].env = NULL;
            pearl_proc[pearl_njobs].state = NONEXISTING;
            pearl_proc[pearl_njobs].mqs = NULL;
            pearl_proc[pearl_njobs].mqsize = 0;
            pearl_proc[pearl_njobs].ymlNode = 0;
            pearl_njobs++;

        } else { // Double table size.
            maxjobs *= 2;
            pearl_proc = (processx *) realloc((char *) pearl_proc,
                                              sizeof(processx) * maxjobs);
        }

        // Initialise the new extension to the pearl_proc table.
        //        old            |             new
        // [0......,pearlnjobs-1, pearl_proc,....maxjobs-1]
        //  |---contains data--|  |----set this to zero--|
        memset(&pearl_proc[pearl_njobs], 0,
               sizeof(processx) * (maxjobs - pearl_njobs));
    }

    if (pearl_info) {
        fprintf(stderr, "Creating process %s with no. %d\n", name, pearl_njobs);
    }

    // Initialise new pearl_proc entry.
    terminatename(pearl_njobs, name);
    pearl_proc[pearl_njobs].name = name;
    pearl_proc[pearl_njobs].func = func;
    pearl_proc[pearl_njobs].env = env;
    pearl_proc[pearl_njobs].state = TIME;
    pearl_proc[pearl_njobs].efunc = NULL;   // From ses scenario, not used here
    pearl_proc[pearl_njobs].energy = 0.0;
    pearl_proc[pearl_njobs].time = 0;
    pearl_proc[pearl_njobs].mql = 0;
    pearl_proc[pearl_njobs].mqs = NULL;
    pearl_proc[pearl_njobs].mqsize = 0;
    pearl_proc[pearl_njobs].mq_fs = 0;
    pearl_proc[pearl_njobs].seq_nr = 0;
    pearl_proc[pearl_njobs].flip_nr = 0;
    pearl_proc[pearl_njobs].className = classname;
    pearl_proc[pearl_njobs].ymlNode = 0;
    pearl_proc[pearl_njobs].last_dest = -1;
    pearl_proc[pearl_njobs].last_method = -1;
    pearl_proc[pearl_njobs].reply_wait = 0;
    pearl_proc[pearl_njobs].stats_state = stats_state;

    return pearl_njobs++;
}

/**
 * Create an instance of class @p class called @p name.
 * Indirectly calls #create_proc.
 *
 * @param name      Name of process instance.
 * @param class     Class of process instance.
 * @return          Index into the pearl process table #pearl_proc.
 */
int createchild(char *name, char *class)
{
    int jobnr;
    char *s;

    if (class == NULL) {
        panic("Class is NULL\n");
        return -1;
    }

    // Find the class of this process to call the class init
    // function <class>_create (stored in cllist[].func).
    for (int i = 0; i < nclasses; i++)
        if (strcmp(cllist[i].name, class) == 0) {

            s = (char *)malloc(strlen(name) + 1);
            strcpy(s, name);

            // Now call generated instance creation function: <class>_create
            // <class>_create in turn calls create_proc to create the
            // pearl process.
            jobnr = (cllist[i].func)(s);
            pearl_proc[jobnr].classIdx = i;
            return jobnr;
        }

    // Class not found.
    panic("Class '%s' does not exist\n", class);
    return -1;
}
