/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *                        Pearl Compiler (c) 1989 University of Amsterdam
 *                                 by
 *                      B.J. Overeinder & J.G. Stil
 *
 * Extra henk muller Thu May 18 17:15:12 MET DST 1989
 * multi length integer code generation
 */


#include <stdlib.h>
#include <string.h>

#include "mli.h"
#include       "iscode.h"
#include       "ptree.h"
#include       "symboldef.h"
#include       "standardtypes.h"
#include       "util.h"
#include       "parse.h"
#include "g_tokstr.h"
#include "generate.h"
#include "gatomic.h"

unsigned int masks[33] = {
    0x00000000,
    0x00000001, 0x00000003, 0x00000007, 0x0000000F,
    0x0000001F, 0x0000003F, 0x0000007F, 0x000000FF,
    0x000001FF, 0x000003FF, 0x000007FF, 0x00000FFF,
    0x00001FFF, 0x00003FFF, 0x00007FFF, 0x0000FFFF,
    0x0001FFFF, 0x0003FFFF, 0x0007FFFF, 0x000FFFFF,
    0x001FFFFF, 0x003FFFFF, 0x007FFFFF, 0x00FFFFFF,
    0x01FFFFFF, 0x03FFFFFF, 0x07FFFFFF, 0x0FFFFFFF,
    0x1FFFFFFF, 0x3FFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF
};

extern int sp;

/*
 * Multi length integer arithmetic
 */

void mli(typex *type, int op)
{
    int nbits;

    nbits = atoi(type->name + 4);


    if (nbits <= 32) {
        switch (op) {
            case MUL:
            case DIV:
            case MOD:
            case PLUS:
            case MIN:
            case AND:
            case OR:
            case XOR:
            case SHLEFT:
            case SHRIGHT:
                icode("sp[%d] %s= sp[%d];\n", sp, g_tokstr(op), sp + 1);

                icode("sp[%d] &= 0x%x;\n", sp, masks[nbits]);
                break;

            case LT:
            case LE:
            case EQ:
            case NE:
            case GE:
            case GT:
                icode("sp[%d] = sp[%d] %s sp[%d+%s];\n",
                      sp, sp, g_tokstr(op), sp, type->typesize);
                break;

            case UMIN:
            case BITNOT:
                icode("sp[%d] = %s sp[%d] & 0x%x;", sp, g_tokstr(op), sp,
                      masks[nbits]);
                break;

            default:
                panic("This op %d should not enter mli mode\n", op);
        }

        return;
    }

    switch (op) {
        case MUL:
            icode("_mlimul(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case DIV:
            icode("_mlidiv(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case MOD:
            icode("_mlimod(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case PLUS:
            icode("_mliplus(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case MIN:
            icode("_mlimin(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case AND:
            icode("_mliand(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case OR:
            icode("_mlior(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case XOR:
            icode("_mlixor(&sp[%d], &sp[%d+%s], %d);\n", sp, sp, type->typesize,
                  nbits);
            break;

        case SHLEFT:
            icode("_mlishleft(&sp[%d], &sp[%d+%s], %d);\n", sp, sp,
                  type->typesize, nbits);
            break;

        case SHRIGHT:
            icode("_mlishright(&sp[%d], &sp[%d+%s], %d);\n", sp, sp,
                  type->typesize, nbits);
            break;

        case LT:
        case LE:
        case EQ:
        case NE:
        case GE:
        case GT:
            icode("sp[%d] = memcmp(&sp[%d], &sp[%d+%s], %s) %s 0;\n", /* Wrong */
                  sp, sp, sp, type->typesize, type->typesize, g_tokstr(op));
            break;

        case UMIN:
            icode("_mliumin(&sp[%d], %d);\n", sp, nbits);

        case BITNOT:
            icode("_mlinot(&sp[%d], %d);\n", sp, nbits);
            break;

        default:
            panic("This op %d should not enter mli mode\n", op);
    }
}


void mliasg(parsex *c)
{
    int nbits;
    int op = c->tok;
    typex *type = c->type;

    nbits = atoi(type->name + 4);

    if (nbits <= 32) {
        switch (op) {
            case PLUSASG:
            case MINASG:
            case MULASG:
            case DIVASG:
            case MODASG:
            case ANDASG:
            case ORASG:
            case XORASG:
            case SHLEFTASG:
            case SHRIGHTASG:
                incrsp(c->tsize);
                gatomic(c->left);
                decrsp(c->tsize);
                icode(" %s sp[%d];\n", g_tokstr(op), sp);
                gatomic(c->left);
                icode(" &= 0x%x;\n", masks[nbits]);
                break;

            default:
                panic("This op %d should not enter mli mode\n", op);
        }

        return;
    }

    incrsp(c->type->typesize);
    icode("sp[%d] = &", sp);
    gatomic(c->left);
    icode(";\n");
    decrsp(c->type->typesize);

    switch (op) {
        case MULASG:
            icode("_mlimul");
            break;

        case DIVASG:
            icode("_mlidiv");
            break;

        case MODASG:
            icode("_mlimod");
            break;

        case PLUSASG:
            icode("_mliplus");
            break;

        case MINASG:
            icode("_mlimin");
            break;

        case ANDASG:
            icode("_mliand");
            break;

        case ORASG:
            icode("_mlior");
            break;

        case XORASG:
            icode("_mlixor");
            break;

        case SHLEFTASG:
            icode("_mlishleft");
            break;

        case SHRIGHTASG:
            icode("_mlishright");
            break;

        default:
            panic("This op %d should not enter mli mode\n", op);
    }

    icode("(sp[%d+%s], &sp[%d], %d);\n", sp, type->typesize, sp, nbits);
}

void mlicast(typex *fromtype, typex *totype, int sp)
{
    int nbits, tbits;
    int i;

    nbits = atoi(fromtype->name + 4);

    if (nbits <= 0) {
        nbits = 32;
    }

    tbits = atoi(totype->name + 4);

    if (tbits <= 0) {
        tbits = 32;
    }

    if (nbits == tbits) {
        return;
    }

    if (nbits > tbits) {
        if (tbits % 32) {

            icode("sp[%d] &= 0x%x;\n", sp + tbits / 32, masks[tbits % 32]);
        }

        return;
    }

    for (i = (nbits - 1) / 32 + 1; i < (tbits - 1) / 32 + 1; i++) {

        icode("sp[%d] = 0;\n", sp + i);
    }
}

void mliconstant(typex *type, int sp, char *string)
{
    int nbits;
    int i, j;
    int irrelevant, truncated = 0;
    char str0, firstchar;
    static char mask4[] = { 15, 1, 3, 7 };

    nbits = atoi(type->name + 4);
    irrelevant = strlen(string) - 1 - (nbits + 3) / 4;

    if (irrelevant >= 0) {
        for (i = irrelevant; i >= 0; i--) {
            if (string[i] != '0') {
                truncated = 1;
                break;
            }
        }

        string += irrelevant + 1;
        irrelevant = -1;
    }

    if (irrelevant == -1) {
        str0 = string[0];
        firstchar = string[0] >= 'A' ? string[0] - 'A' + 10 : string[0] - '0';
        firstchar &= mask4[nbits % 4];
        string[0] = firstchar > 9 ? firstchar - 10 + 'A' : firstchar + '0';
        truncated = truncated || str0 != string[0];
    }

    if (truncated) {
        warning("Constant truncated to 0x%s\n", string);
    }

    j = 0;
    i = strlen(string) - 8;

    if (i < 0) {
        i = 0;
    }

    do {
        if (j) {

        }

        icode("sp[%d] = 0x%.8s;\n", sp + j, string + i);
        i -= 8;
        j++;
        nbits -= 32;
    } while (i >= 0);

    while (nbits > 0) {

        icode("sp[%d] = 0;\n", sp + j);
        j++;
        nbits -= 32;
    }
}
