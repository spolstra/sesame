/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PSPEC_H
#define PSPEC_H

#include "symboldef.h"

void pspec(parsex *spec_tree, symboltable *types, symboltable *variables, symboltable *functions, symboltable *subtypeof, bool no_meth, bool par_only);
void pusefiles(parsex *file_tree, symboltable *use_files, char *current_file);
void ptype_spec(symboltable *t, parsex *type_spec);
void pfunc_spec(symboltable *t, parsex *mf, bool ismethod);
void pclass_spec(symboltable *t, parsex *class_spec);

#endif // PSPEC_H
