/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"

/*
 * Set bits in 'flags' according to the bit pattern of 'fb'.
 */
void setflags(int *flags, int fb)
{
    *flags |= fb;
}



/*
 * Returns whether or not the 'METHOD' flag is set.
 */
bool ismethod(int flags)
{
    return (flags & METHOD);
}

bool isclasstype(typex *type)
{
    return type->type == TCLASS;
}


/*
 * Returns wether or not the 'VARARGS' flag is set.
 */
bool hasvarpar(int flags)
{
    return (flags & VARARGS);
}



/*
 * This function returns the translation of a basic type of Pearl or the
 * name of a self-defined type.
 */
char *get_c_type(typex *type)
{
    if (type == typeinteger) {
        return ("intptr_t");
    } else if (type == typefloat) {
        return ("float");
    } else if (type == typeboolean) {
        return ("intptr_t");
    } else if (type == typestring) {
        return ("char *");
    } else if (type->type == TCLASS) {
        return ("intptr_t");
    } else {
        return ("intptr_t");
    }
}



/*
 * Converts a string to uppercase
 */
char *toupper_s(char *s)
{
    int i, len;
    char *rs;

    len = strlen(s) + 1;
    rs = malloc((unsigned)len);

    for (i = 0; i < len; i++)
        if ((int)islower(s[i])) {
            rs[i] = toupper(s[i]);
        } else {
            rs[i] = s[i];
        }

    return (rs);
}


#define STRINGBASE  16

/*
 * Functions supporting constant conversion (binary, octal, hexadecimal
 * -> decimal
 */
static char *mult(char *ostring, int number)
{
    int len = strlen(ostring);
    int off = 1;
    char *temp, *string;
    int i, digit, carry;

    string = ostring;

    while (string[0] == '0' && len) {
        len--;
        string++;
    }

    if (len == 0) {
        return ostring;
    }

    temp = malloc(len + off);
    temp[len + off] = '\0';
    carry = 0;

    for (i = len - 1; i >= 0; i--) {
        digit = carry + (string[i] - '0') * number;
        temp[i + off] = digit % STRINGBASE + '0';
        carry = digit / STRINGBASE;
    }

    for (i = off - 1; i >= 0; i--) {
        temp[i] = carry % STRINGBASE + '0';
        carry = carry / STRINGBASE;
    }

    if (carry) {
        panic("OVERFLOW!\n");
    }

    free(ostring);
    return temp;
}

static char *adddigit(char *ostring, int number)
{
    int len = strlen(ostring);
    int off = 1;
    char *temp, *string;
    int i, digit, carry;

    string = ostring;

    while (string[0] == '0' && len) {
        len--;
        string++;
    }

    temp = malloc(len + off + 1);
    temp[len + off] = '\0';
    carry = number;

    for (i = len - 1; i >= 0; i--) {
        digit = carry + string[i] - '0';
        temp[i + off] = digit % STRINGBASE + '0';
        carry = digit / STRINGBASE;
    }

    for (i = off - 1; i >= 0; i--) {
        temp[i] = carry % STRINGBASE + '0';
        carry = carry / STRINGBASE;
    }

    if (carry) {
        panic("OVERFLOW!\n");
    }

    free(ostring);
    return temp;
}

/*
 * Converts a value in binary, octal, hexadecimal or decimal form to
 * a constant in decimal form. This constant looses no precision !
 */
char *valconv(char *str)
{
    int base;
    char *result;
    unsigned int i;
    int digit;

    switch (str[1]) {
        case 'x':
        case 'X':
            base = 16;
            str += 2;
            break;

        case 'o':
        case 'O':
            base = 8;
            str += 2;
            break;

        case 'b':
        case 'B':
            base = 2;
            str += 2;
            break;

        default:
            base = 10;
            break;
    }

    result = malloc(2);
    strcpy(result, "0");

    for (i = 0; i < strlen(str); i++) {
        result = mult(result, base);
        digit = str[i] <= '9' ? str[i] - '0' :
                islower((int)str[i]) ? str[i] - 'a' + 10 : str[i] - 'A' + 10;
        result = adddigit(result, digit);
    }

    for (i = 0; i < strlen(result); i++)
        if (result[i] > '9') {
            result[i] += 'A' - '9' - 1;
        }

    return result;
}

int atox(char *s)
{
    int sum = 0;

    while (*s) {
        sum = sum * 16 + (*s > '9' ? *s - 'A' + 10 : *s - '0');
        s++;
    }

    return sum;
}

char *atoaf(char *s)
{
    double sum = 0.0;
    static char buf[26];

    while (*s) {
        sum = sum * 16.0 + (*s > '9' ? *s - 'A' + 10 : *s - '0');
        s++;
    }

    sprintf(buf, "%24.18e", sum);

    return buf;
}
