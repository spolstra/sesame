/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BLOCKERS_H
#define BLOCKERS_H

int newblocker(void);
void appendblocker(char *meth);
void appendanonymousblocker(char *meth);
char *closeblocker(void);
void defblockers(char *name, int state);
void dumpblockersdef(void);
void dumpblockers(char *class);

#endif // BLOCKERS_H
