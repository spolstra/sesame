/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef PDECL_H
#define PDECL_H

void pdecl(parsex *decl_tree, symboltable *types, symboltable *variables, symboltable *functions, int scope, int varonly, int no_meth, symboltable *altvars);
void ptype_decl(symboltable *t, parsex *type_decl, int scope);
void parray_type(parsex *array_ptr, symboltable *t, int deg);
void pfunc_decl(symboltable *t, parsex *mf, bool ismethod, int scope);
void pclass(symboltable *t, parsex *class, int scope);

#endif // PDECL_H
