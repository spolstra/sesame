/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * Universal Symbol table handler.
 * Stolen from pl, but modified.
 */


#include <string.h>
#include <stdlib.h>

#include "symbol.h"
#include "lex_an.h"


/**
 * Actual symbol table implentation
 * It is hidden from the outside so we alone can modify it
 */
typedef struct _symboltable {
    unsigned int size;          /** Size of the elements we point to. !UNUSED! FIXME: Maybe delete? */
    unsigned int length;        /** The size of this symboltable */
    unsigned int fill;          /** The number of elements in the symbol table */
    char **cont;                /** The actual list of pointers */
} _symboltable;



/**
 * Create a new symboltable
 *
 * @param size The size, in bytes, of the content of the symboltable
 *
 * @return The allocated new symbol table.
 */
symboltable *newsymboltable(const unsigned int size)
{
    symboltable *new;

    new = (symboltable *) malloc(sizeof(symboltable));
    new->size = size;
    new->fill = 0;
    new->length = 2;
    new->cont = (char **)malloc((unsigned)(sizeof(char *) * new->length));

    return new;
} //  newsymboltable



/*
 * Add item to symboltable, only items that are not already in the table.
 */
void addsymbol(symboltable *table, char *item)
{

    for (unsigned int i = 0; i < table->fill; i++) {
        if (!strcmp(*(char **)table->cont[i], *(char **)item)) {
            error("Identifier '%s' defined more than once\n", *(char **)item);
            return;
        }
    }

    if (table->length == table->fill) { //  expand symboltable
        table->length += 10;
        table->cont = (char **)realloc((char *)table->cont,
                                       (unsigned)(sizeof(char *) *
                                               table->length));
    }

    table->cont[table->fill] = item;
    table->fill++;

    return;
} //  addsymbol


void insertsymbol(symboltable *table, char *item)
{
    if (table->length == table->fill) { //  expand table
        table->length += 10;
        table->cont =
            (char **)realloc((char *)table->cont,
                             (unsigned)(sizeof(char *) * table->length));
    }

    table->cont[table->fill] = item;
    table->fill++;
}


/*
 * Search for an item in a symboltable and return the pointer to the item and
 * return NULL if the item is not in the table.
 */
char *searchsymbol1(symboltable *t, char *str)
{
    if (t == NULL) {
        return NULL;
    }

    for (unsigned int i = 0; i < t->fill; i++) {
        if (!strcmp(*(char **)t->cont[i], str)) {
            return (char *)t->cont[i];
        }
    }

    return NULL;
} //  searchsymbol1



/*
 * Search for an item in a symboltable and return the index of the item and
 * return -1 if the item is not in the table.
 */
int searchsymbol2(symboltable *t, char *str)
{
    if (t == NULL) {
        return -1;
    }

    for (unsigned int i = 0; i < t->fill; i++) {
        if (!strcmp(*(char **)t->cont[i], str)) {
            return i;
        }
    }

    return -1;
} //  searchsymbol2

unsigned int symbol_getfill(symboltable *t)
{
    return t->fill;
}

char *symbol_getcont(symboltable *t, unsigned int i)
{
    return t->cont[i];
}


