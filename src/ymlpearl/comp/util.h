/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */



#ifndef  UTIL_H
#define  UTIL_H

#ifndef __cplusplus
typedef int bool;
#endif        // __cplusplus

#define  TRUE     1
#define  FALSE      0

#ifndef  NULL
#define  NULL     0
#endif        //  NULL

#ifndef MAXPATHLEN
#define   MAXPATHLEN    255
#endif

/*
 * Constants to set conditions in the 'flag' field.
 */

#define  FUNCTION   0x0
#define  METHOD           0x1
#define  TYPETYPE   0x0
#define  CLASSTYPE    0x2
#define  TYPERET    0x0
#define  CLASSRET   0x2
#define  TYPEREP    0x0
#define  CLASSREP   0x4
#define  FIXEDARGS    0x0
#define  VARARGS    0x8


#ifdef __cplusplus
extern "C" {
#endif

#include "symboldef.h"

    void setflags(int *flags, int fb);
    bool ismethod(int flags);
    bool isclasstype(struct typex *type);
    bool hasvarpar(int flags);
    char *get_c_type(struct typex *type);
    char *toupper_s(char *s);
    char *valconv(char *str);
    int atox(char *s);
    char *atoaf(char *s);

#ifdef __cplusplus
};
#endif
#endif        //  UTIL_H
