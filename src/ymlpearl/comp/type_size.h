/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef TYPE_SIZE_H
#define TYPE_SIZE_H

char *sizevariable(char *fmt, char *s0, char *s1);
void sizedumpinitdecl(void);
void sizedumpinit(classx *cl);
void freedumpinit(void);
char *sizeidentifier(varx *vp);
char *sizestring(int value);
int sizesimple(char *string);
char *sizeeval(char *fmt, char *val1, char *val2);
char *calc_range(parsex *range);
void calc_type_size(symboltable *tp_list);
void calc_type_sizes(void);
int compvart(varx **a, varx **b);
char *calc_var_place(symboltable *vt, char *place);
void fm_var_place(symboltable *fmt);
void class_var_place(symboltable *ct);

#endif // TYPE_SIZE_H
