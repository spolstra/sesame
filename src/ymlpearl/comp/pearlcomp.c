/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 *        Main Program
 */


#include <unistd.h>
#include <sys/types.h>
#include <sys/file.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>

#include "ptree.h"
#include "util.h"
#include "symboldef.h"
#include "pearlcomp.h"
#include "pimpl.h"
#include "pspecific.h"
#include "subtype.h"
#include "gcode.h"
#include "link_type.h"
#include "link_id.h"
#include "type_size.h"
#include "type_check.h"
#include "standardtypes.h"
#include "pearllib.h"
#include "Clib_func.h"
#include "spec_check.h"
#include "analysis.h"
#include "../libutil/absolute.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

extern int yyparse();

extern symboltable *modules;
extern specx *specs;
extern specx *totals;
extern symboltable *variables;
extern symboltable *functions;
extern symboltable *classes;
extern symboltable *types;
extern symboltable *subtype_table;
extern symboltable *Clib_functions;

extern int nerror;
extern int nwarning;

bool newopt = FALSE;
bool verbose = FALSE;
bool mulgaflag = FALSE;
bool force = FALSE;
bool debugger = FALSE;
char *optimize = "-O1";
bool profiling = FALSE;
bool boundcheck = FALSE;
int Cfunc_spec = 0;

symboltable *use_files;

#define MAXCLIBS  16

FILE *popen(), *fp_spec, *fp_impl;
char file_spec[256], file_impl[256];
char *Cfunc_spec_file[MAXCLIBS];

/*
 *
 */
void compile(char *filename, char *defines)
{
    parsex *spec_tree, *impl_tree, *dummy;
    filex *spec_file;

    initmodule();

    if (verbose) {
        printf("Parsing ...\n");
    }

    parse(filename, defines, &spec_tree, &impl_tree, FALSE);
    pimpl(impl_tree);
    pspecific(spec_tree, filename, 1);

    for (unsigned int i = 0; i < symbol_getfill(specs->use_files); i++) {
        spec_file = (filex *)symbol_getcont(specs->use_files, i);
        parse(spec_file->name, defines, &spec_file->spec, &dummy, TRUE);
        pspecific(spec_file->spec, filename, FALSE);
    }

    // Add sub-type relations
    add_relations(subtype_table, specs->classes);

    if (nerror) {
        setlf(NULL);
        error("Too many errors. No code generated.");
        exit(1);
    }

    if (verbose) {
        printf("Linking type identifiers ...\n");
    }

    link_typeid();

    if (nerror) {
        setlf(NULL);
        error("Too many errors. No code generated.");
        exit(1);
    }

    if (verbose) {
        printf("Linking identifiers in code ...\n");
    }

    link_id();

    if (nerror) {
        setlf(NULL);
        error("Too many errors. No code generated.");
        exit(1);
    }

    calc_type_sizes();

    if (verbose) {
        printf("Type checking ...\n");
    }

    type_check(classes, functions, variables);

    if (nerror) {
        setlf(NULL);
        error("Too many errors. No code generated.");
        exit(1);
    }

    class_var_place(classes);
    class_var_place(specs->classes);
    sizehasreplies();

    if (verbose) {
        printf("Analysis phases...\n");
    }

    reply_analysis(classes, functions, variables);

    if (verbose) {
        printf("Generating code ...\n");
    }

    gcode(filename);

    if (nerror) {
        setlf(NULL);
        error("Too many errors. Compiler exiting ...");
        exit(1);
    }


    addmodule(modules, filename, specs, types, variables, functions, classes);
}


int doesexist(char *file)
{
    return access(file, R_OK) == 0;
}

/*
 * Parse a specification file and if spec_only is false also the corresponding
 * implementation file
 */
void parse(char *file, char *defines, parsex **sp_tree, parsex **im_tree,
           int spec_only)
{

    extern parsex *spec, *impl;
    // command should be sizeof file_spec + length of defines
    // (+1 for string terminator)
    char *command = malloc(sizeof(file_spec) + strlen(defines)+1);

    if (librarypath(file, file_spec) < 0) {
        exit(1);
    }

    strcat(file_spec, ".ps");
    sprintf(command, "gcc -E %s - < %s", defines, file_spec);

    fp_spec = popen(command, "r");

    if (spec_only) {
        fp_impl = fopen(EMPTY_FILE, "r");
        strcpy(file_impl, EMPTY_FILE);

    } else {
        librarypath(file, file_impl);
        sprintf(command, "gcc -E %s - < %s.pi", defines, file_impl);
        strcat(file_impl, ".pi");

        if (!doesexist(file_impl)) {
            setlf(NULL);
            warning("No file \"%s\"\n", file_impl);
            fp_impl = fopen(EMPTY_FILE, "r");
            strcpy(file_impl, EMPTY_FILE);
            spec_only = 1;

        } else {
            fp_impl = popen(command, "r");
        }
    }

    yyparse();
    pclose(fp_spec);

    if (spec_only) {
        fclose(fp_impl);
    } else {
        pclose(fp_impl);
    }

    *sp_tree = spec;
    *im_tree = impl;

    // cleanup
    free (command);
}


/*
 * Copies use files to the list of files that have to be compiled
 */
symboltable *copy_use_files(symboltable *use_files, char *rootfile)
{
    symboltable *comp_list = newsymboltable(sizeof(namex));
    filex *file1;
    namex *file2;

    file2 = (namex *)malloc(sizeof(namex));
    file2->name = strdup(rootfile);
    file2->to_compile = TRUE;
    addsymbol(comp_list, (char *)file2);

    for (unsigned int i = 0; i < symbol_getfill(use_files); i++) {
        file1 = (filex *)symbol_getcont(use_files, i);
        file2 = (namex *)malloc(sizeof(namex));
        file2->name = strdup(file1->name);
        file2->to_compile = TRUE;
        addsymbol(comp_list, (char *)file2);
    }

    return (comp_list);
}

static FILE *usefile;
struct uselistx {
    char *name;
    struct uselistx *next;
} *head = NULL;

static void makeusefile(char *filename)
{
    char path[256];

    strcpy(path, filename);
    strcat(path, ".use");
    unlink(path); //  Make sure time stamp is updated
    usefile = fopen(path, "w");

    if (usefile == NULL) {
        error("Cannot open %s\n", path);
        usefile = stdout;
    }
}

void addusefile(char *name)
{
    char path[256];
    struct uselistx *z;

    if (name[0] == '"') {
        strcpy(path, name + 1);
        path[strlen(path) - 1] = '\0';

    } else {
        strcpy(path, name);
    }

    for (z = head; z != NULL; z = z->next)
        if (strcmp(z->name, path) == 0) {
            return;
        }

    z = (struct uselistx *)malloc(sizeof(struct uselistx));
    z->name = strdup(path);
    z->next = head;
    head = z;
    fprintf(usefile, "$%s\n", path);
}


static void closeusefile(void)
{
    fclose(usefile);
}

/*
 * Main program
 */
int main(int argc, char *argv[])
{
    int first;
    char *s, *cpy;
    char path[256], *filename = NULL;
    char defines[2048];
    char *lastdef;

    defines[0] = 0;

    if ((s = strrchr(argv[0], '/'))) {
        cpy = malloc(s - argv[0] + 4 + 1);
        strncpy(cpy, argv[0], s - argv[0]);
        cpy[s - argv[0]] = '\0';
        strcat(cpy, "/../");
        initlibpath(cpy);
        initpearlpath(cpy);
        libraryinit(cpy);

    } else {
        initlibpath("./");
        initpearlpath("./");
        libraryinit("./");
    }

    librarypath("standardlib", path);
    Cfunc_spec_file[Cfunc_spec++] = strdup(path);

    librarypath("tracelib", path);
    Cfunc_spec_file[Cfunc_spec++] = strdup(path);

    lastdef = defines; // Concat all defines.
    for (first = 1; first < argc; first++) {
        if (!strcmp(argv[first], "-v")) {
            verbose = TRUE;
        }

        else if (!strncmp(argv[first], "-L", 2)) {
            if (argv[first][2]) {
                addlibdir(argv[first] + 2);
            } else {
                addlibdir(argv[++first]);
            }

        } else if (!strncmp(argv[first], "-c", 2)) {
            librarypath(argv[first] + 2, path);
            Cfunc_spec_file[Cfunc_spec++] = strdup(path);

        } else if (!strncmp(argv[first], "-D", 2)) {
            lastdef = stpcpy(lastdef, argv[first]);
            lastdef = stpcpy(lastdef, " "); // space between defines

        } else if (!strcmp(argv[first], "-b")) {
            boundcheck = TRUE;

        } else {
            if (filename) {
                error("Too many files specified\n");
            }

            filename = argv[first];
        }
    }
    *lastdef='\0'; // Terminate defines.

    if (filename == NULL) {
        error("No file specified\n");
        exit(1);
    }

    makeusefile(filename);
    //  read standard C library and optional other C functions
    init_Cfunc_table();
    init_moduletable();
    init_subtypetable(&subtype_table);
    compile(filename, defines);
    check_subtypes();
    closeusefile();

    if (nerror) {
        exit(1);
    } else {
        exit(0);
    }
}
