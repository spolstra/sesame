/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * This function takes care of the parsing of the implementation file by means
 * of functions from the file "pdecl.c"
 */

#include "pimpl.h"
#include "ptree.h"
#include "symboldef.h"
#include "util.h"
#include "pdecl.h"


extern symboltable *types;
extern symboltable *variables;
extern symboltable *functions;
extern symboltable *classes;



/*
 * Parse implementation part of program by first parsing the global
 * declarations and after that the declarations in a class and the class
 * itself.
 */
void pimpl(parsex *parse_tree)
{
    if (parse_tree != NULL) {
        if (parse_tree->left != NULL)
            pdecl(parse_tree->left, types, variables, functions, GLOBALSYM,
                  FALSE, TRUE, 0);

        if (parse_tree->right != NULL) {
            pclass(classes, parse_tree->right, GLOBALSYM);
        }
    }
}       /* pimpl */
