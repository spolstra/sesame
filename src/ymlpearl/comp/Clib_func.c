/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * Create symbol table of C functions which are included to Pearl.
 * The standard C functions are specified in the CLIB file and the
 * user defined C function specification file, is given at the commmand
 * line after the "-C" option.
 */



#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Clib_func.h"
#include "iscode.h"
#include "pspec.h"
#include "ptree.h"
#include "symboldef.h"
#include "util.h"
#include "standardtypes.h"
#include "../libutil/error.h"

extern int Cfunc_spec;

extern char file_spec[], file_impl[];
extern char *Cfunc_spec_file[];

extern FILE *fopen(), *fp_spec, *fp_impl;

extern parsex *spec, *impl;

symboltable *Cuse_files, *Clib_functions;

static char path[256];
static int linenumber;


/*
 * Error routine ending in a error situation.
 */
void Clib_error(void)
{
    setlf(NULL);
    error("%s, line %d: malformed function declaration.", path, linenumber);
}       /* Clib_error */



/*
 * Match character 'c' against input on file 'fp'.
 */
bool match(int c, FILE *fp)
{
    int d;

    while (isspace(d = getc(fp))) {
        if (d == '\n') {
            linenumber++;
        }
    }

    /* read blanks */;

    if (c == d) {
        return (TRUE);
    } else {
        ungetc(d, fp);
        return (FALSE);
    }
}       /* match */



/*
 * Read next identifier from input file 'fp'.
 */
char *get_id(FILE *fp)
{
    char c, str[128];
    char *id = NULL;
    int i = 0;

    while (isspace((int)(c = getc(fp))))
        /* read blanks */;

    if (isalpha((int)c) || c == '_') {
        str[i] = c;

        do {
            i++;
            str[i] = c = getc(fp);
        } while (isalnum((int)c) || c == '_');

        str[i] = '\0';
        id = strdup(str);
    }

    ungetc(c, fp);
    return (id);
}       /* get_id */



/*
 * Create parameter symbol table of a C function.
 * C functions with a variable number of parameters has the following
 * appearance:
 *  function_name (#) : type
 * resulting in none symbol table.
 */
symboltable *create_param_table(FILE *fp)
{
    int i = 0;
    char idname[128];
    char *pname, *ptype;
    symboltable *t;

    if (!match('(', fp)) {
        Clib_error();
    }

    if (match('#', fp)) {
        if (!match(')', fp)) {
            Clib_error();
        }

        return (NULL);
    }

    t = newsymboltable(sizeof(varx));

    while ((ptype = get_id(fp))) {
        sprintf(idname, "%s%d", "id", i++);
        pname = strdup(idname);
        addvariable(t, pname, ptype, LOCALSYM, NULL, NULL);

        if (match(';', fp)) {
            continue;
        }

        if (match(',', fp)) {
            continue;
        }

        if (match(')', fp)) {
            break;
        }

        Clib_error();
    }

    match(')', fp);

    return (t);
}       /* create_param_table */



/*
 * Read description of C function and add it to the symbol table.
 */
void addCfunction(FILE *fp)
{
    char *name, *rettype;
    symboltable *param_table;

    if (!(name = get_id(fp))) {
        Clib_error();
    }

    if (!match(':', fp)) {
        Clib_error();
    }

    rettype = get_id(fp);
    param_table = create_param_table(fp);

    addfunction(Clib_functions, name, rettype, NULL, param_table,
                NULL, NULL, FALSE, GLOBALSYM, NULL);
}       /* addCfunction */



extern int yyparse();

/*
 * Read the C function specifications in file 'fname' with use of function
 * 'yyparse' and fill the symbol tables 'Cuse_files' and 'Clib_functions'
 * with resp. function 'puse_file' and 'spec'.
 */
void read_Cfunc_spec(char *fname)
{
    if ((fp_spec = fopen(fname, "r")) == NULL) {
        setlf(NULL);
        error("Cannot open file \"%s\".\n", fname);
        exit(0);
    }

    strcpy(file_spec, fname);

    fp_impl = fopen(EMPTY_FILE, "r");
    strcpy(file_impl, EMPTY_FILE);

    yyparse();

    fclose(fp_spec);
    fclose(fp_impl);

    if (spec != NULL) {
        if (spec->left != NULL) {
            pusefiles(spec->left, Cuse_files, fname);
        }

        if (spec->right != NULL) {
            pspec(spec->right, NULL, NULL, Clib_functions, NULL, TRUE, FALSE);
        }
    }
}       /* read_Cfunc_spec */



/*
 * Make C function symbol table, taking input from file 'CLIB' and under
 * option '-C' also from file 'Cfunc_spec_file'.
 */
void init_Cfunc_table(void)
{
    int i;
    FILE *fp;

    Cuse_files = newsymboltable(sizeof(filex));
    Clib_functions = newsymboltable(sizeof(mfpx));

    for (i = 0; i < Cfunc_spec; i++) {
        strcpy(path, Cfunc_spec_file[i]);
        strcat(path, ".pc");

        if ((fp = fopen(path, "r")) == NULL) {
            setlf(NULL);
            fatal("cannot open C lib file \"%s\".\n", path);
        }

        linenumber = 1;

        while (!match(EOF, fp)) {
            addCfunction(fp);
        }

        fclose(fp);
    }
}       /* init_Cfunc_table */

void Clib_code(void)
{
    for (unsigned int i = 0; i < symbol_getfill(Clib_functions); i++) {
        mfpx *c = (mfpx *)symbol_getcont(Clib_functions, i);

        typex *type = (typex *)c->realreturn;

        if (type == typefloat) {
            if (c->is_double) {
                icode("double ");
            } else {
                icode("float ");
            }
        } else {
            //TODO: Add other types!
            continue;
        }

        icode("%s(", c->name);

        if (c->parameters != NULL) {
            for (unsigned int j = 0; j < symbol_getfill(c->parameters); j++) {
                varx *p = (varx *)symbol_getcont(c->parameters, j);
                icode("%s", p->type);

                if (j != symbol_getfill(c->parameters) - 1) {
                    icode(", ");
                }
            }
        }

        icode(");\n");
    }
}
