/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *           by
 *      B.J. Overeinder & J.G. Stil
 *
 * Code generation.
 */

#include <stdlib.h>
#include <string.h>

#include "generate.h"
#include "iscode.h"
#include "ptree.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "parse.h"
#include "gfunc.h"
#include "gsimple.h"
#include "gsend.h"
#include "gswitch.h"
#include "gblock.h"
#include "greturn.h"
#include "gatomic.h"
#include "g_tokstr.h"
#include "gfinit.h"
#include "type_size.h"
#include "mli.h"

int sp;       /* stack pointer of generated object */
extern typex *roottype();

void incrsp(char *value)
{
    if (sizesimple(value)) {
        sp += atoi(value);
    } else if (atoi(value) != 0) {
        icode("sp += %s;\n", value);
    }
}

void decrsp(char *value)
{
    if (sizesimple(value)) {
        sp -= atoi(value);
    } else if (atoi(value) != 0) {
        icode("sp -= %s;\n", value);
    }
}

void incrsp2(char *value, int optok)
{
    if (sizesimple(value) && optok) {
        sp += atoi(value);
    } else if (atoi(value) != 0) {
        icode("sp += %s;\n", value);
    }
}

void decrsp2(char *value, int optok)
{
    if (sizesimple(value) && optok) {
        sp -= atoi(value);
    } else if (atoi(value) != 0) {
        icode("sp -= %s;\n", value);
    }
}

/*
 * Code generation for expression lists (i.e. body of
 * function or class, etc.).
 * In case of simple expressions shorter code is generated.
 */
void generate(parsex *c)
{
    generate_gen(c, 1);
}

void generate_void(parsex *c)
{
    generate_gen(c, 0);
}

void warnvoid(linex *lf)
{
    setlf(lf);
    warning("void action\n");
}

void glvalue(parsex *c)
{
    switch (c->tok) {
        case IDENT:
            icode(c->str);
            break;

        case ARRAY:
        case STRUCT:
            glvalue(c->left);
            break;

        default:
            panic("Illegal lvalue.\n");
            break;
    }
}

void gscope(parsex *c)
{
    varx *x;

    switch (c->tok) {
        case IDENT:
            x = (varx *) c->sym_entry;

            if (x->scope == CLASSSYM) {
                icode("1");
            } else {
                icode("0");
            }

            break;

        case ARRAY:
        case STRUCT:
            gscope(c->left);
            break;

        default:
            panic("Illegal lvalue.\n");
            break;
    }
}

void generate_gen(parsex *c, int with)
{

    if (c == NULL) {
        return;
    }

    c->type = roottype(c->type);

    switch (c->tok) {
        case SEMCOL:
            generate_void(c->left);

            if (c->right) {
                generate_gen(c->right, with);
            }

            break;

        case INTCONST:
        case FLTCONST:
        case BOOLCONST:
        case STRCONST:
        case ENUM_ELEM:
            if (!with) {
                warnvoid(c->lf);
            }

            gstack_atomic(c);
            break;

        case CASTTYPE:
            if (!with) {
                warnvoid(c->lf);
            }

            if (c->left->tok == INTCONST) {
                if (c->type == typefloat) {
                    icode("((float *)sp)[%d] = %s;\n", sp, atoaf(c->left->str));

                } else if (c->type == typeinteger) {

                    if (strlen(c->left->str) > 8) {
                        icode("sp[%d] = 0x%s;\n", sp,
                              c->left->str + strlen(c->left->str - 8));

                    } else {
                        icode("sp[%d] = 0x%s;\n", sp, c->left->str);
                    }

                } else if (isint_(c->type)) {
                    setlf(c->left->lf);
                    mliconstant(c->type, sp, c->left->str);
                } else {
                    panic("Cannot cast this constant\n");
                }

            } else {
                if (issimple(c)) {
                    icode("sp[%d] = ", sp);
                    incrsp(c->tsize);
                    gsimple(c);
                    decrsp(c->tsize);
                    icode(";\n");

                } else {
                    generate(c->left);
                }

                if (c->type == typefloat) {
                    setlf(c->lf);
                    castfloat(c->left->type, sp);

                } else if (c->type == typeinteger) {
                    castinteger(c->left->type, sp);

                } else if (isint_(c->type)) {
                    castint_(c->left->type, c->type, sp);

                } else {
                    panic("Cannot cast this type\n");
                }
            }

            break;

        case MUL:
        case DIV:
        case MOD:
        case PLUS:
        case MIN:
        case AND:
        case OR:
        case XOR:
        case SHLEFT:
        case SHRIGHT:
            if (!with) {
                warnvoid(c->lf);
            }

            if (issimple(c)) {
                icode("sp[%d] = ", sp);
                incrsp(c->tsize);
                gsimple(c);
                decrsp(c->tsize);
                icode(";\n");

            } else {
                generate(c->left);
                incrsp(c->tsize);
                generate(c->right);
                decrsp(c->tsize);

                if (c->type == typeinteger) {
                    icode("sp[%d] %s= sp[%d];\n", sp, g_tokstr(c->tok), sp + 1);

                } else if (isint_(c->type)) {
                    mli(c->type, c->tok);

                } else if (c->type == typefloat) {
                    icode("((float *)sp)[%d] %s= ((float *)sp)[%d];\n", sp,
                          g_tokstr(c->tok), sp + 1);

                } else {
                    panic("Cannot compute this type, token %d\n", c->tok);
                }
            }

            break;

        case LT:
        case LE:
        case EQ:
        case NE:
        case GE:
        case GT:
        case XORXOR:
            if (!with) {
                warnvoid(c->lf);
            }

            if (issimple(c)) {
                icode("sp[%d] = ", sp);
                incrsp(c->tsize);
                gsimple(c);
                decrsp(c->tsize);
                icode(";\n");

            } else {
                generate(c->left);
                incrsp(c->left->tsize);
                generate(c->right);
                decrsp(c->left->tsize);

                if (c->left->type == typeboolean || c->left->type == typeinteger) {
                    icode("sp[%d] = (sp[%d] %s sp[%d+%s]);\n",
                          sp, sp, g_tokstr(c->tok), sp, c->tsize);

                } else if (c->left->type == typefloat) {
                    icode("sp[%d] = ((float *)sp)[%d] %s ((float *)sp)[%d+%s];\n",
                          sp, sp, g_tokstr(c->tok), sp, c->tsize);

                } else if (isint_(c->left->type)) {
                    mli(c->left->type, c->tok);

                } else {
                    panic("Cannot compare this type, token %d\n", c->tok);
                }
            }

            break;

        case ANDAND:
        case OROR:
            if (!with) {
                warnvoid(c->lf);
            }

            if (issimple(c)) {
                icode("sp[%d] = ", sp);
                incrsp(c->tsize);
                gsimple(c);
                decrsp(c->tsize);
                icode(";\n");

            } else {
                generate(c->left);
                icode("if (%ssp[%d]) {\n", c->tok == ANDAND ? "" : "!", sp);
                generate(c->right);
                icode("}\n");
            }

            break;

        case UMIN:
        case BOOLNOT:
        case BITNOT:
            if (!with) {
                warnvoid(c->lf);
            }

            if (issimple(c)) {
                icode("sp[%d] = ", sp);
                incrsp(c->tsize);
                gsimple(c);
                decrsp(c->tsize);
                icode(";\n");

            } else {
                generate(c->left);

                if (c->type == typeinteger) {
                    icode("sp[%d] = %s sp[%d];", sp, g_tokstr(c->tok), sp);

                } else if (c->type == typeboolean) {
                    icode("sp[%d] = %s sp[%d];", sp, g_tokstr(c->tok), sp);

                } else if (isint_(c->type)) {
                    mli(c->type, c->tok);

                } else if (c->type == typefloat) {
                    icode("((float *)sp)[%d] = %s ((float *)sp)[%d];\n", sp,
                          g_tokstr(c->tok), sp);

                } else {
                    panic("Cannot compute this type, token %d\n", c->tok);
                }
            }

            break;

        case ASG:
            if (issimple(c)) {
                if (with) {

                    icode("sp[%d] = (intptr_t) ", sp);

                    icode("(");
                    incrsp(c->tsize);
                    gsimple(c);
                    decrsp(c->tsize);
                    icode(");\n");

                } else {
                    gsimple(c);
                    icode(";\n");
                }

            } else {
                generate(c->right);

                if (strcmp(c->tsize, "1") == 0) {
                    incrsp(c->tsize);
                    gatomic(c->left);
                    decrsp(c->tsize);

                    if (c->left->type == typefloat) {
                        icode(" = ((float *)sp)[%d];\n", sp);
                    } else {
                        icode(" = sp[%d];\n", sp);
                    }

                } else {
                    icode("bcopy(sp+%d, ", sp);

                    icode("&");
                    gatomic(c->left);

                    icode(", %s * sizeof(intptr_t));\n", c->tsize);
                    newbcopylist(c->tsize);
                }
            }

            break;

        case PLUSASG:
        case MINASG:
        case MULASG:
        case DIVASG:
        case MODASG:
        case ANDASG:
        case ORASG:
        case XORASG:
        case SHLEFTASG:
        case SHRIGHTASG:
            if (issimple(c)) {
                if (with) {
                    icode("sp[%d] = ", sp);
                    incrsp(c->tsize);
                    gsimple(c);
                    decrsp(c->tsize);
                    icode(";\n");

                } else {
                    gsimple(c);
                    icode(";\n");
                }

            } else {
                generate(c->right);

                if (c->type == typeinteger) {
                    incrsp(c->tsize);
                    gatomic(c->left);
                    decrsp(c->tsize);
                    icode(" %s sp[%d];\n", g_tokstr(c->tok), sp);

                } else if (isint_(c->type)) {
                    mliasg(c);

                } else if (c->type == typefloat) {
                    incrsp(c->tsize);
                    gatomic(c->left);
                    decrsp(c->tsize);
                    icode(" %s ((float *)sp)[%d];\n", g_tokstr(c->tok), sp);

                } else {
                    panic("Cannot compute this type, token %d\n", c->tok);
                }
            }

            break;

        case IDENT:
        case STRUCT:
        case ARRAY:
            if (!with) {
                warnvoid(c->lf);
            }

            gstack_atomic(c);
            break;

        case SEND:
        case SENDSEND:
            gsend(c, with);
            break;

        case REPLY:
            greply(c);
            break;

        case FUNCTION_CALL:
            gfunc(c, with);
            break;

        case CFUNCTION_CALL:
            if (issimple(c)) {
                if (with) {
                    if (c->type == typefloat) {
                        icode("((float *)sp)[%d] = ", sp);
                    } else {
                        icode("sp[%d] = ", sp);
                    }

                    incrsp(c->tsize);
                    gsimple(c);
                    decrsp(c->tsize);
                    icode(";\n");

                } else {
                    gsimple(c);
                    icode(";\n");
                }

            } else {
                gcfunc(c);
            }

            break;

        case BLOCK:
        case BLOCKT:
            gblock(c, with);
            break;

        case IF:
            if (issimple(c)) {
                if (with) {
                    icode("sp[%d] = ", sp);
                    incrsp(c->tsize);
                    gsimple(c);
                    decrsp(c->tsize);
                    icode(";\n");

                } else {
                    gsimple(c);
                    icode(";\n");
                }

            } else {
                if (issimple(c->left)) {
                    icode("if (");
                    gsimple(c->left);
                    icode(") {\n");

                } else {
                    generate(c->left);
                    icode("if (sp[%d]) {\n", sp);
                }

                generate_gen(c->right, with);

                if (c->e0) {
                    icode("} else {\n");
                    generate_gen(c->e0, with);
                }

                icode("}\n");
            }

            break;

        case FOR:
            generate(c->left);
            incrsp("1");

            if (issimple(c->right)) {
                icode("while (");
                gsimple(c->right);
                icode(") {\n");

            } else {
                icode("while (1) {\n");
                generate(c->right);
                icode("if (!sp[%d]) break;\n", sp);
            }

            generate(c->e1);
            decrsp("1");
            generate(c->e0);
            icode("}\n");
            break;

        case WHILE:
            if (issimple(c->left)) {
                icode("while (");
                gsimple(c->left);
                icode(") {\n");

            } else {
                icode("while (1) {\n");
                generate(c->left);
                icode("if (!sp[%d]) break;\n", sp);
            }

            generate(c->right);
            icode("}\n");
            break;

        case SWITCH:
            gswitch(c);
            break;

        case BREAK:
            icode("break;\n");
            break;

        case RETURN:
            greturn(c->left);
            break;

        case SOURCE:
            icode("sp[%d] = ", sp);
            gsource();
            icode(";\n");
            break;

        case THIS:
            icode("sp[%d] = ", sp);
            gthis();
            icode(";\n");
            break;

        default:
            setlf(c->lf);
            panic("You made a big mistake!, got unexpected token %d\n", c->tok);
            break;
    }
}


void castfloat(typex *type, int sp)
{
    if (type == typefloat) {
        return;
    }

    if (isint_(type)) {
        int len = atoi(type->name + 4);

        if (len > 32) {
            icode("((float *)sp)[%d]=_mli2float(&sp[%d],%d);\n", sp, sp, len);
            warning("Loosing digits in casting int_ to float\n");
            return;
        }

        castinteger(type, sp);
        type = typeinteger;
    }

    if (type == typeinteger) {
        icode("((float *)sp)[%d] = (float) sp[%d];\n", sp, sp);
    } else {
        error("Cannot cast %s to float\n", type->name);
    }
}

void castinteger(typex *type, int sp)
{
    if (type == typeinteger) {
        return;
    }

    if (type == typefloat) {

        icode("sp[%d] = (intptr_t)((double *)sp)[%d];\n", sp, sp);

    } else if (isint_(type)) {
        castint_(type, typeinteger, sp);
    } else {
        panic("Cannot cast this type\n");
    }
}

void castint_(typex *type, typex *totype, int sp)
{
    if (type == totype) {
        return;
    }

    if (type == typefloat) {
        castinteger(type, sp);
        type = typeinteger;
    }

    if (type == typeinteger || isint_(type)) {
        mlicast(type, totype, sp);
    } else {
        panic("Cannot cast this type\n");
    }
}
