/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file are used to generate the typedefinitions the
 * environments of each class and the code, implementing each object (class).
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gimpl.h"
#include "util.h"
#include "iscode.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "ptree.h"
#include "parse.h"
#include "gstate.h"
#include "blockers.h"
#include "generate.h"
#include "g_tokstr.h"
#include "gsimple.h"
#include "Clib_func.h"
#include "type_size.h"
#include "gfinit.h"
#include "gatomic.h"
#include "gfunc.h"

extern symboltable *types;
extern symboltable *basic_types;
extern symboltable *variables;
extern symboltable *classes;
extern specx *specs;


extern int sp;
extern char *ret_sp;
extern int max_meth;

char *ret_label;
char *class_name;
typex *this_reply_type;

extern char *getvarsize();

/*
 * The function 'gglob_types' generates the global type definitions.
 */
void gglob_types(symboltable *types)
{
    for (unsigned int j = 0; j < symbol_getfill(basic_types); j++) {
        typex *tp = (typex *) symbol_getcont(basic_types, j);

        if (isint_(tp)) {
            int x = atoi(tp->name + 4);

            icode("#ifndef type_%s\n#define type_%s\n", tp->name, tp->name);

            if (x <= 32) {
                icode("typedef int %s;\n", tp->name);
            } else {
                icode("typedef struct {int _[%d];} %s;\n", (x - 1) / 32 + 1, tp->name);
            }

            icode("#endif //type_%s\n", tp->name);
        }
    }

    for (unsigned int j = 0; j < symbol_getfill(types); j++) {
        typex *tp = (typex *) symbol_getcont(types, j);

        if (tp->type == TTYPE) {
            icode("#ifndef type_%s\n# define type_%s\n", tp->name, tp->name);
            icode("typedef %s %s;\n", get_c_type((typex *)tp->realtype), tp->name);
            icode("#endif //type_%s\n", tp->name);
        }
    }
}

int dead_symbol1 = 0;
int dead_symbol2;

/*
 * Change henkm, they are not initialized and declared to be external.
 * This is for addition of 'has_reply_*' and 'reply_*'
 */
void gmeth_labels(symboltable *classes)
{
    unsigned int i, j;
    classx *cl;
    mfpx *meth;
    symboltable *hasrepl;

    for (i = 0; i < symbol_getfill(classes); i++) {
        cl = (classx *)symbol_getcont(classes, i);
        icode("// Method labels of class %s\n", cl->name);

        for (j = 0; j < symbol_getfill(cl->func_meths); j++) {
            meth = (mfpx *)symbol_getcont(cl->func_meths, j);

            if (strcmp(meth->name, "statistics")) {
                icode("int method_%s;\n", meth->name);
            }
        }
    }

    hasrepl = gethasreplys();

    if (hasrepl) {
        icode("// Method labels of classes \"has_reply_*\"\n");

        for (i = 0; i < symbol_getfill(hasrepl); i++) {
            cl = (classx *)symbol_getcont(hasrepl, i);

            for (j = 0; j < symbol_getfill(cl->func_meths); j++) {
                meth = (mfpx *)symbol_getcont(cl->func_meths, j);
                icode("int method_%s;\n", meth->name);
            }
        }
    }
}



/*
 * The function 'gclasses' generates the code of each class, and to create the
 * class.
 */
void gclasses(symboltable *classes)
{
    classx *cl;
    mfpx *meth;
    typex *rt;
    int start_label;
    unsigned int i, j;
    char *b;
    int stats_state;

    for (i = 0; i < symbol_getfill(classes); i++) {
        cl = (classx *)symbol_getcont(classes, i);
        class_name = cl->name;

        // method environment structure
        icode("typedef struct %s_env {\n", cl->name);

        icode("intptr_t state;\n");
        icode("intptr_t *sp;\n");
        icode("intptr_t *pp;\n");
        icode("intptr_t do_copy;\n");
        icode("intptr_t jobnr;\n");

        sizedumpinitdecl();
        icode("} %s_env;\n\n", cl->name);

        icode("extern int method_statistics;\n\n");

        // class type cast
        icode("int to_%s(const int job) {\n", cl->name);
        icode("if (job < 0 || numjobs() <= job) {\n");
        icode("  panic(\"to_%s() invalid job id!\");\n", cl->name);
        icode("}\n");
        icode("if (!instanceof(job, \"%s\")) {\n", cl->name);
        icode("  panic(\"%%d is not an instance of %s\", job);\n", cl->name);
        icode("}\n");
        icode("return job;\n");
        icode("}\n\n");

        // method_call()
        icode("int %s_call(%s_env *env) {\n", cl->name, cl->name);
        icode("int return_state = -1;\n");

        icode("intptr_t *ENV = (intptr_t *)(env + 1);\n\n");

        state_switch();
        cl->state_statistics = -1;

        for (j = 0; j < symbol_getfill(cl->func_meths); j++) {
            meth = (mfpx *)symbol_getcont(cl->func_meths, j);

            if (strncmp(meth->name, "reply_", 6) != 0) {
                this_reply_type = (typex *) meth->realreply;
                install_function(meth->paramsize, meth->localsize);
                ilabel("\nF%s:\n", meth->name);
                ret_label = meth->name;
                state_incr();
                state_case();

                if (!strcmp(meth->name, "statistics")) {
                    cl->state_statistics = state_val();
                }

                meth->method_label = state_val();
                ret_sp = meth->paramsize;
                rt = (typex *) meth->realreturn;
                icode("sp += %d;\n", atoi(meth->paramsize) + 4);

                icode("sp[-2] = (intptr_t)pp;\n");

                icode("sp[-1] = return_state;\n");
                icode("pp = sp;\n");

                if (atoi(meth->localsize) != 0) {
                    icode("sp += %s;\n", meth->localsize);
                }

                glocal_vars(meth->locals, 1);
                generate(meth->code);

                ilabel("R%s:\n", ret_label);

                if (atoi(meth->localsize) != 0) {
                    icode("sp -= %s;\n", meth->localsize);
                }

                //if (!strcmp(meth->name, "statistics"))
                //  icode("env->state = sp[-1] - 2;\n");
                //else
                icode("env->state = sp[-1];\n");


                icode("pp = (intptr_t *)sp[-2];\n");

                icode("sp -= %d;\n", atoi(meth->paramsize) + 4);
                icode("goto Start_Switch;\n");

            } else {
                icode("F%s:\n", meth->name);
                state_incr();
                state_case();
                rt = (typex *) meth->realreturn;
                meth->method_label = state_val();

                if (strcmp(rt->typesize, "1") == 0) {
                    icode("sp[%d] = sp[%d];\n", sp, sp + 1);
                }

                else if (strcmp(rt->typesize, "0") != 0) {
                    icode("bcopy(sp+%d, sp+%d, %s * sizeof(intptr_t));\n", sp + 1, sp, rt->typesize);
                    newbcopylist(rt->typesize);
                } else {
                    icode("// Sync only, no value\n");
                }

                icode("env->state = return_state;\n");
                icode("goto Start_Switch;\n");
            }
        }

        if (cl->state_statistics == -1) {
            icode("Fstatistics:\n");
            state_incr();
            state_case();
            cl->state_statistics = state_val();
            icode("env->state = return_state;\n");
            icode("goto Start_Switch;\n");
        }

        state_incr();
        state_case();
        start_label = state_val();
        glocal_vars(cl->localvar, 0);
        generate(cl->code);
        newblocker();
        appendblocker("statistics");
        b = (char *)closeblocker();

        icode("{\n");

        icode("extern intptr_t %s; \n", b);

        icode("if (!pearlreceive(%s, &sp[%d])) {\n", b, sp);
        state_incr();
        icode("env->state = %d;\n", state_val());
        icode("goto Freturn;\n");
        icode("}\n");
        icode("}\n");

        state_case();
        stats_state = state_val();

        if (sp != -1) {
            icode("sp += %d;\n", sp + 1);
        }

        state_incr();
        icode("return_state = %d;\n", state_val());
        icode("goto Fstatistics;\n");

        state_case();

        if (sp != -1) {
            icode("sp -= %d;\n", sp + 1);
        }

        icode("env->state = %d;\n", state_val() - 1);
        icode("goto Freturn;\n");
        state_end();

        icode("return 0;\n");
        icode("}\n\n", cl->name);

        // method_create()

        icode("intptr_t %s_create(char *name) {\n", cl->name);

        icode("%s_env *env;\n", cl->name);
        icode("int l;\n");
        icode("extern int %s_methods[];\n", cl->name);
        // This declaration gives an error in gcc 4.1.2 (debian testing)
        //    icode("%s_env *malloc(), *realloc();\n\n", cl->name);

        icode("env = (%s_env *)malloc(sizeof(%s_env));\n", cl->name, cl->name);
        icode("env->state = %d;\n", start_label);

        icode("env->jobnr = create_proc(name, %s_call, (intptr_t *)env, \"%s\", %d);\n",
              cl->name, cl->name, stats_state);
        icode("install_mesg_q(env->jobnr, %s_methods);\n\n", cl->name, cl->name);

        icode("\nreturn env->jobnr;\n", cl->name);
        icode("}\n\n", cl->name);

        // method_initiate()
        sizedumpinit(cl);
    }
}

void gmodule(char *name)
{
    classx *cl;
    classx *s_cl;
    mfpx *meth;
    symboltable *hasrepl;

    icode("void %s_module() {\n", name);

    hasrepl = gethasreplys();

    if (hasrepl) {
        for (unsigned int i = 0; i < symbol_getfill(hasrepl); i++) {
            cl = (classx *)symbol_getcont(hasrepl, i);

            for (unsigned int j = 0; j < symbol_getfill(cl->func_meths); j++) {
                meth = (mfpx *)symbol_getcont(cl->func_meths, j);
                icode("if (method_%s == 0) {\n", meth->name);
                icode("method_%s = pearl_methodinstall(\"%s\");\n",
                      meth->name, meth->name);
                icode("}\n");
            }
        }
    }

    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        cl = (classx *)symbol_getcont(classes, i);

        for (unsigned int j = 0; j < symbol_getfill(cl->func_meths); j++) {
            meth = (mfpx *)symbol_getcont(cl->func_meths, j);

            if ((meth->flags & METHOD) || strncmp(meth->name, "reply_", 6) == 0) {
                icode("if (method_%s == 0) {\n", meth->name);
                icode("method_%s = pearl_methodinstall(\"%s\");\n",
                      meth->name, meth->name);
                icode("}\n");
            }
        }

        icode("\nint class_id = installclass(%s_create, \"%s\", (void *)%s_initiate);\n",
              cl->name, cl->name, cl->name);

        s_cl = (classx *)searchsymbol1(specs->classes, cl->name);

        if (!s_cl) {
            setlf(cl->lf);
            panic("Class \"%s\" not found in specification file.", cl->name);
        }

        for (unsigned int j = 0; j < symbol_getfill(s_cl->subtypeof); j++)
            icode("installsubtype(class_id, \"%s\");\n",
                  ((namex *)symbol_getcont(s_cl->subtypeof, j))->name);
    }
    icode("}\n\n");

    dumpblockersdef();

    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        extern int defn; // nr of blockers for synchronous comm. replies
        int nr = 2;
        cl = (classx *)symbol_getcont(classes, i);

        for (unsigned int j = 0; j < symbol_getfill(cl->func_meths); j++) {
            meth = (mfpx *)symbol_getcont(cl->func_meths, j);

            if ((meth->flags & METHOD) || strncmp(meth->name, "reply_", 6) == 0) {
                nr++;
            }
        }

        icode("int %s_methods[%d] = {%d};\n", cl->name, nr + defn,
              nr + defn - 1);
    }

    icode("\n");

    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        cl = (classx *)symbol_getcont(classes, i);
        icode("void %s_install_mesgidq(const int meth_id) {\n", cl->name);
        icode("static int ni = 1;\n");
        icode("%s_methods[ni++] = meth_id;\n", cl->name);
        icode("}\n");
    }

    icode("\nvoid %s_blocks() {\n", name);

    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        cl = (classx *)symbol_getcont(classes, i);
        icode("blocker_class();\n");
        defblockers("statistics", cl->state_statistics);
        icode("%s_install_mesgidq(method_statistics);\n", cl->name);

        for (unsigned int j = 0; j < symbol_getfill(cl->func_meths); j++) {
            meth = (mfpx *)symbol_getcont(cl->func_meths, j);

            if ((meth->flags & METHOD) || strncmp(meth->name, "reply_", 6) == 0) {
                defblockers(meth->name, meth->method_label);
                icode("%s_install_mesgidq(method_%s);\n", cl->name, meth->name);
            }
        }

        dumpblockers(cl->name);
    }

    icode("}\n\n");
}

/*
 * Generates the initialisation code for the global variables.
 */
void gsimple_init(parsex *code)
{
    switch (code->tok) {
        case SEMCOL:
            gsimple_init(code->left);
            icode("\n");
            break;

        case PLUS:
        case MIN:
        case MUL:
        case DIV:
        case MOD:
            icode("(");
            gsimple_init(code->left);
            icode(g_tokstr(code->tok));
            gsimple_init(code->right);
            icode(")");
            break;

        case FLTCONST:
        case STRCONST:
            icode(code->str);
            break;

        case INTCONST:
            icode("0x%s", code->str);
            break;

        case BOOLCONST:
            icode(strcmp(code->str, "true") ? "0" : "1");
            break;

        default:
            setlf(code->lf);
            error("Illegal variable initialization\n");
            break;
    }
}



/*
 * Generate the declaration of a local variable. If the variable is
 * initialized, the code for the initialisation is generated by the function
 * 'gsimple_init'.
 */
void glocal_vars(symboltable *vars, int func)
{
    char *typo;

    for (unsigned int i = 0; i < symbol_getfill(vars); i++) {
        varx *vp = (varx *)symbol_getcont(vars, i);

        if (vp->init != NULL) {
            if (issimple(vp->init)) {
                typo = get_c_type((typex *)vp->realtype);
                icode("*(%s*)(%s+%s) = ", typo, func ? "pp" : "ENV", vp->offset);
                gsimple(vp->init);
                icode(";\n");
            } else {
                generate(vp->init);

                if (strcmp(vp->init->tsize, "1") == 0) {
                    icode("%s[%s] = sp[%d];\n", func ? "pp" : "ENV", vp->offset,
                          sp);
                } else {
                    icode("bcopy(sp+%d, &%s[%s], %s*sizeof(int));\n",
                          sp, func ? "pp" : "ENV", vp->offset, vp->init->tsize);
                }
            }
        }
    }
}

/*
 * Generate the declaration of a global variable. If the variable are
 * initialized, the code for the initialisation is generated by the function
 * 'gsimple_init'. Global variables are declared 'extern' in the header file.
 */
void gglob_vars(symboltable *vars)
{
    for (unsigned int i = 0; i < symbol_getfill(vars); i++) {
        varx *vp = (varx *)symbol_getcont(vars, i);
        typex *tp = (typex *) vp->realtype;

        if (!isclasstype((typex *)vp->realtype)) {
            if (tp->type != TARRAY) {
                icode("%s _%s", get_c_type((typex *)vp->realtype), vp->name);
            } else
                icode("%s _%s[%s]", get_c_type((typex *)vp->realtype), vp->name,
                      tp->typesize);

            if (vp->init != NULL) {
                icode(" = ");
                gsimple_init(vp->init);
            }

            icode(";\n");

        } else {
            if (tp->type != TARRAY) {
                icode("int _%s;\n", vp->name);
            } else {
                icode("int _%s[%s];\n", vp->name, tp->typesize);
            }
        }
    }
}

/*
 * Generate the declaration of a global variable. If the variable are
 * initialized, the code for the initialisation is generated by the function
 * 'gsimple_init'. Global variables are declared 'extern' in the header file.
 */
void gglob_vars_impl(symboltable *vars)
{
    for (unsigned int i = 0; i < symbol_getfill(vars); i++) {
        varx *vp = (varx *) symbol_getcont(vars, i);
        typex *tp = (typex *) vp->realtype;

        if (!isclasstype((typex *)vp->realtype)) {
            if (tp->type != TARRAY)
                icode("extern %s _%s;\n", get_c_type((typex *)vp->realtype),
                      vp->name);
            else
                icode("extern %s _%s[%s];\n", get_c_type((typex *)vp->realtype),
                      vp->name, tp->typesize);
        } else {
            if (tp->type != TARRAY) {
                icode("extern intptr_t _%s;\n", vp->name);
            } else {
                icode("extern intptr_t _%s[%s];\n", vp->name, tp->typesize);
            }
        }
    }
}



/*
 * Generate the code for the implementation file.
 */
void gimpl(char *filename)
{
    icode("#include <stdlib.h>\n");
    icode("#include <string.h>\n");
    icode("#include <stdio.h>\n");
    icode("#include <stdint.h>\n\n");

    icode("#include <sesamesim/libsim/misc.h>\n");
    icode("#include <sesamesim/libsim/schedule.h>\n");
    icode("#include <sesamesim/libsim/blocker.h>\n");
    icode("#include <sesamesim/libsim/startup.h>\n");
    icode("#include <sesamesim/libsim/mesq.h>\n");
    icode("#include <sesamesim/libsim/class.h>\n");

    icode("\n");

    //Declare external functions (REQUIRED AS OTHERWISE 64 BIT POINTERS ARE
    //TRUNCATED TO 32 BITS ON MAC-OS)

    icode("extern int method_statistics;\n");
    icode("int numjobs();\n");
    icode("int instanceof(int jobid, char *subtype_name);\n");

    icode("char *who(int i);\n");
    icode("char *whoami();\n");
    icode("char *class_name();\n");
    icode("int virtualChannelID();\n");
    icode("void pearlwaiting(int reply_wait);\n");

    icode("int pearl_methodinstall(char *name);\n");

    icode("unsigned long long int ltimer();\n");

    icode("\n");

    Clib_code();

    icode("\n");

    gglob_types(specs->types);
    gglob_vars(variables);
    gglob_vars_impl(specs->variables);
    icode("\n");
    gmeth_labels(specs->classes);
    gclasses(classes);
    gmodule(filename);
}
