/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * This file contains functions to create symboltables and to add items
 * to the appropriate symboltables.
 */

#include <stdlib.h>
#include <string.h>

#include  "ptree.h"
#include  "symboldef.h"
#include  "util.h"


symboltable *modules;
specx *specs;
specx *totals;
symboltable *basic_types;
symboltable *types;
symboltable *variables;
symboltable *functions;
symboltable *classes;



/*
 * Create global symboltables
 */
void initmodule(void)
{
    specs = (specx *)malloc(sizeof(specx));
    specs->use_files = newsymboltable(sizeof(filex));
    specs->types = newsymboltable(sizeof(typex));
    specs->variables = newsymboltable(sizeof(varx));
    specs->functions = newsymboltable(sizeof(mfpx));
    specs->classes = newsymboltable(sizeof(classx));
    basic_types = newsymboltable(sizeof(typex));
    types = newsymboltable(sizeof(typex));
    variables = newsymboltable(sizeof(varx));
    functions = newsymboltable(sizeof(mfpx));
    classes = newsymboltable(sizeof(classx));
}



void init_moduletable(void)
{
    modules = newsymboltable(sizeof(modulex));
}



/*
 * Add subtype to table t
 */
void addsubtype(symboltable *t, char *name)
{
    namex *newsubtype;

    newsubtype = (namex *)malloc(sizeof(namex));
    newsubtype->name = name;
    addsymbol(t, (char *)newsubtype);
}



/*
 * Add type to table t
 */
void addtype(symboltable *t, char *name, int type, char *basetype,
             symboltable *fields, int scope, linex *lf)
{
    typex *newtype;

    newtype = (typex *)malloc(sizeof(typex));
    newtype->name = name;
    newtype->type = type;
    newtype->realtype = NULL;
    newtype->basetype = basetype;
    newtype->fields = fields;
    newtype->scope = scope;
    newtype->flags = 0x0;
    newtype->lf = lf;
    addsymbol(t, (char *)newtype);
}



/*
 * Add variable to table t
 */
void addvariable(symboltable *t, char *name, char *type, int scope,
                 parsex *init, linex *lf)
{
    varx *newvar;

    newvar = (varx *)malloc(sizeof(varx));
    newvar->name = name;
    newvar->type = type;
    newvar->scope = scope;
    newvar->init = init;
    newvar->realtype = NULL;
    newvar->flags = 0x0;
    newvar->lf = lf;
    addsymbol(t, (char *)newvar);
} //  addvariable



/*
 * Add function to table t
 */
void addfunction(symboltable *t, char *name, char *rettype, char *reptype,
                 symboltable *params, symboltable *locals, parsex *code,
                 bool ismethod, int scope, linex *lf)
{
    mfpx *newfun;

    newfun = (mfpx *)malloc(sizeof(mfpx));
    newfun->name = name;

    if (rettype && strncmp(rettype, "double", 6) == 0) {
        newfun->is_double = 1;
        strcpy(rettype, "float");
    } else {
        newfun->is_double = 0;
    }

    newfun->returntype = rettype;
    newfun->realreturn = NULL;
    newfun->replytype = reptype;
    newfun->realreply = NULL;
    newfun->parameters = params;
    newfun->locals = locals;
    newfun->code = code;
    newfun->scope = scope;
    newfun->flags = 0x0;
    newfun->istriv = 0;
    setflags(&newfun->flags, ismethod ? METHOD : FUNCTION);
    setflags(&newfun->flags, params ? FIXEDARGS : VARARGS);
    newfun->lf = lf;
    addsymbol(t, (char *)newfun);
} //  addfunction



/*
 * Add class to table t
 */
void addclass(symboltable *t, char *name, parsex *code,
              symboltable *types, symboltable *variables,
              symboltable *func_meths, symboltable *subtypeof, int scope,
              linex *lf, symboltable *localvars)
{
    classx *nc;

    nc = (classx *)malloc(sizeof(classx));
    nc->name = name;
    nc->code = code;
    nc->scope = scope;
    nc->types = types;
    nc->variables = variables;
    nc->func_meths = func_meths;
    nc->subtypeof = subtypeof;
    nc->lf = lf;
    nc->verydirty = TCLASS;
    nc->localvar = localvars;
    addsymbol(t, (char *)nc);
} //  addclass



void addmodule(symboltable *t, char *name, specx *specs,
               symboltable *types, symboltable *variables,
               symboltable *functions, symboltable *classes)
{
    modulex *m;

    m = (modulex *)malloc(sizeof(modulex));
    m->name = name;
    m->specs = specs;
    m->types = types;
    m->variables = variables;
    m->functions = functions;
    m->classes = classes;
    m->label_init = FALSE;
    addsymbol(t, (char *)m);
} //  addmodule
