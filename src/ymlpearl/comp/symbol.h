/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 *      Symbol table structure
 *
 * Uses flexible typing.
 * The symbol table contains records with several fields. The size of the record
 * must be given at the creation of the new symbol table.
 *
 * Stolen from pl.
 */



#ifndef SYMBOL_H
#define SYMBOL_H

typedef struct _symboltable symboltable;

symboltable *newsymboltable(unsigned int size);
void addsymbol(symboltable *table, char *item);
void insertsymbol(symboltable *table, char *item);
char *searchsymbol1(symboltable *t, char *str);
int searchsymbol2(symboltable *t, char *str);

unsigned int symbol_getfill(symboltable *t);
char *symbol_getcont(symboltable *t, unsigned int i);

#endif // SYMBOL_H

