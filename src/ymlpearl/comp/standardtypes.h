/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef STANDARDTYPES_H

#define STANDARDTYPES_H

#ifndef GLOBALSYM
#include "symboldef.h"
#endif

#define TYPEVOID  "void"
#define TYPEINTEGER "integer"
#define TYPEINT_  "int_"
#define TYPEFLOAT "float"
#define TYPESTRING  "string"
#define TYPEBOOLEAN "boolean"
#define TYPETIME  "time"
#define TYPETIME_BITS 64
extern char realtime[];

extern typex *typevoid;
extern typex *typeinteger;
extern typex *typefloat;
extern typex *typestring;
extern typex *typeboolean;
extern typex *typerealtime;

void initstandardtypes(void);
int isint_(typex *t);
int tc_compatibel(typex *typl, typex *typr, parsex **expr);
typex *tc_asgop(typex *typl, typex *typr, parsex *expr);
typex *tc_arithmeticop(typex *typl, typex *typr, parsex *expr);
typex *tc_compareop(typex *typl, typex *typr, parsex *expr);
void inithasreplys(void);
symboltable *gethasreplys(void);
void addreplytype(char *tstr);
void addreplymethod(symboltable *func_meths, char *tstr);
typex *insertsourcetype(char *tstr);
typex *findsourcetype(char *name);
typex *findhasreply(char *name);
void sizehasreplies(void);

#endif // STANDARDTYPES_H
