/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * This function takes care of the parsing of a specification file by means of
 * functions from the file "pspec.c".
 */

#include "pspecific.h"
#include "ptree.h"
#include "symboldef.h"
#include "util.h"
#include "pspec.h"
#include "spec_check.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


extern specx *specs;
extern symboltable *Clib_functions;


void addclasscastfunc(symboltable *t, parsex *class_spec)
{
    symboltable *parameters = newsymboltable(sizeof(varx));
    char *name = (char *)malloc(strlen(class_spec->left->str) + 4);

    strcpy(name, "to_");
    strcat(name, class_spec->left->str);

    //printf("Adding class cast function %s\n", name);

    addvariable(parameters, strdup("id0"), strdup("integer"),
                LOCALSYM, 0, 0);
    addfunction(t, name, strdup(class_spec->left->str), 0, parameters, 0, 0,
                FALSE, GLOBALSYM, 0);
}

/*
 * Parse all specifications found in the specification. That means: find the
 * "use files", parse the global specifications and parse the class
 * specification.
 */
void pspecific(parsex *parse_tree, char *current_file, bool first_time)
{
    if (parse_tree != NULL) {
        if (parse_tree->left != NULL) {
            pusefiles(parse_tree->left, specs->use_files, current_file);
        }

        if (parse_tree->right != NULL)
            pspec(parse_tree->right, specs->types, specs->variables,
                  specs->functions, NULL, TRUE, FALSE);

        if (parse_tree->e0 != NULL) {
            pclass_spec(specs->classes, parse_tree->e0);
            addclasscastfunc(Clib_functions, parse_tree->e0);
        }
    }

    if (first_time) {
        spec_check();
    }
} //  pspecific
