/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */


#include "gblock.h"
#include "iscode.h"
#include "ptree.h"
#include "parse.h"
#include "util.h"
#include "generate.h"
#include "blockers.h"
#include "gstate.h"

extern char *class_name;
extern int sp;


/*
 * Code generation for block or blockt expression.
 * If methods are present, the mask of applicable messages is generated.
 * A blockt with no methods is translated to an hold of 'time' units.
 * In the other cases it is translated to receive-sequences, with possible
 * timers which value is returned after completion.
 */
void gblock(parsex *p, int with)
{
    bool with_time, with_meth;
    int nmeth = 0;
    parsex *cp;
    char *blocker;

    with_time = p->left != NULL;
    with_meth = p->right != NULL;

    newblocker();

    for (cp = p->right; cp != NULL; cp = cp->right) {
        nmeth++;
        appendblocker(cp->left->str);
    }

    if (!with_time) {
        appendblocker("statistics");
    }

    blocker = (char *)closeblocker();

    if (with_time) {
        if (with_meth) { // With time, with methods blockt(2, aap, noot)
            if (with) {
                icode("sp[%d] = timer();\n", sp);
                sp++;
            }

            generate(p->left);

            if (with) {
                icode("sp[%d] += sp[%d];\n", sp - 1, sp);
            }

            icode("{\n");

            icode("extern intptr_t %s; \n", blocker);

            icode("if (!receive_time(%s, sp[%d], &sp[%d])) {\n", blocker, sp, sp);
            state_incr();
            state_assign();
            icode("goto Freturn;\n");
            icode("}\n");
            icode("}\n");
            state_case();

            if (with) {
                icode("sp[%d] -= timer();\n", sp - 1);
            }

        } else { // With time, no methods blockt(2)
            generate(p->left);
            icode("hold_to(sp[%d]);\n", sp);
            state_incr();
            state_assign();
            icode("goto Freturn;\n");
            state_case();
            icode("sp[%d] = 0;\n", sp);
        }

    } else { // With methods, no time, block(aap)
        state_incr();
        state_case();
        icode("{\n");

        icode("extern intptr_t %s; \n", blocker);

        icode("if (!pearlreceive(%s, &sp[%d])) {\n", blocker, sp);
        state_incr();
        state_assign();
        icode("goto Freturn;\n");
        icode("}\n");
        icode("}\n");
        state_case();
    }

    if (with_meth) {
        if (with_time) {
            icode("if (sp[%d] >= 0) {\n", sp);
        }

        state_incr();
        icode("env->state = sp[%d];\n", sp);

        if (sp) {
            icode("sp += %d;\n", sp);
        }

        icode("return_state = %d;\n", state_val());
        icode("goto Start_Switch;\n");
        state_case();

        if (sp) {
            icode("sp -= %d;\n", sp);
        } else {
            icode(";\n");
        }

        if (with_time) {
            icode("}\n");
        }
    }

    if (with_time) {
        if (with_meth) {
            icode("\n");

            if (with) {
                sp--;
            }
        }
    }
}
