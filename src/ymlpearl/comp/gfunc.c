/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */


#include <string.h>

#include "gfunc.h"
#include "ptree.h"
#include "parse.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "iscode.h"
#include "generate.h"
#include "gstate.h"
#include "gsimple.h"

extern int sp;


/*
 * Generate the code for the actual parameters in a function call. The
 * parameters are placed on the stack.
 */
int gpar(parsex *act_par, int pn, int whichsp)
{
    /* gpar */
    if (act_par == NULL) {
        return pn;
    }

    if (act_par->tok == COMMA) {
        pn = gpar(act_par->left, pn + 1, whichsp);
    }

    generate(act_par->right);
    incrsp2(act_par->right->tsize, whichsp);
    return pn;
}       /* gpar */

void grmpar(char *size, int whichsp)
{
    decrsp2(size, whichsp);
}


/*
 * Generate the actual parameters in the functioncall. The parameters are
 * placed on the stack by the function 'gpar'.
 */
static void gcall_par(mfpx *func, parsex *act_par, int par_no, int par_tot)
{
    /* gcall_par */
    varx *vp;

    if (!act_par) {
        return;
    }

    if (act_par->tok == COMMA) {
        gcall_par(func, act_par->left, par_no - 1, par_tot);
    }

    if (func->parameters) {
        vp = (varx *)symbol_getcont(func->parameters, par_no - 1);
        icode("((%s *) sp)[%d]", get_c_type((typex *)vp->realtype), sp);
        incrsp2(((typex *) vp->realtype)->typesize, 1);
    } else {
        if (act_par->right->type == typefloat) {
            icode(" (double) ((float *)sp)[%d]", sp);
            incrsp2("1", 1);
        } else if (strcmp(act_par->right->tsize, "1")) {
            icode(" (sp+%d)", sp);
            incrsp2(act_par->right->tsize, 1);
        } else {
            icode(" sp[%d]", sp);
            incrsp2("1", 1);
        }
    }

    if (par_no < par_tot) {
        icode(",");
    }
}       /* gcall_par */



/*
 * Generate simple actual parameters in the functioncall.
 */
void gscall_par(parsex *p, parsex *tp)
{
    /* gscall_par */
    if (p == NULL) {
        return;
    }

    if (p->tok == COMMA) {
        gscall_par(p->left, tp);
    }

    gsimple(p->right);

    if (p != tp) {
        icode(", ");
    }
}       /* gscall_par */



/*
 * Generate the code for a functioncall.
 */
void gfunc(parsex *fc, int with)
{
    /* gfunc */
    mfpx *fp;
    typex *rt;

    fp = (mfpx *) fc->sym_entry;
    rt = (typex *) fp->realreturn;

    if (rt != NULL && rt != typevoid) {
        if (!with) {
            setlf(fc->lf);
            warning("Ignoring result of %s\n", fp->name);
        }
    }

    if (fc->right) {
        /* generate parameters */
        sp++;     /* Ret value ? */
        gpar(fc->right, 0, 0);
        sp--;     /* ?? */
        grmpar(fp->paramsize, 0);
    }

    state_incr();

    if (sp) {
        icode("sp += %d;\n", sp);
    }

    icode("return_state = %d;\n", state_val());
    icode("goto F%s;\n", fp->name);
    state_case();

    if (sp) {
        icode("sp -= %d;\n", sp);
    }
}       /* gfunc */



/*
 * Generate the code for a call to a C function. Functioncalls with variable
 * number of arguments that are not simple cannot be generated.
 */
void gcfunc(parsex *fc)
{
    mfpx *fp;
    int np, osp;
    typex *rtyp;
    int endcopy = 0;

    fp = (mfpx *)fc->sym_entry;
    rtyp = (typex *)fp->realreturn;

    if (issimple(fc)) {
        icode("%s(", fc->left->str);

        if (fc->right != NULL) {
            gscall_par(fc->right, fc->right);
        }

        icode(")");

    } else {
        osp = sp;
        np = gpar(fc->right, 0, 1);
        sp = osp;

        if (rtyp != NULL) {
            if (rtyp == typefloat) {
                icode("((float *)sp)[%d] = ", sp);
            } else {
                icode("sp[%d] = ", sp);
            }

            endcopy = strcmp(rtyp->typesize, "1") != 0;
        }

        icode("%s(", fp->name);
        gcall_par(fp, fc->right, np, np);
        sp = osp;
        icode(");\n");

        if (endcopy)

            icode("bcopy(sp[%d], sp+%d, (%s) * sizeof(intptr_t));\n",
                  sp, sp, rtyp->typesize);
    }
}
