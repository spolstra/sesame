/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/


/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */

#include "gcode.h"
#include "iscode.h"
#include "gimpl.h"

/*
 * Generate code for the implementation and specification file.
 */
void gcode(char *filename)
{
    popenfiles(filename, 1);
    gimpl(filename);
    closefiles(filename);
}       /* gcode.c */
