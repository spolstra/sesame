/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file take care of the parsing of the specifications
 * in a specification file, and the creation of the symboltables.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "pspec.h"
#include "parse.h"
#include "util.h"
#include "pdecl.h"


static int dummy = 0;



/*
 * Parse specifications and add found specifications to symboltables containing
 * specifications. These are not the same as the symboltables containing
 * declarations.
 */
void pspec(parsex *spec_tree, symboltable *types,
           symboltable *variables, symboltable *functions,
           symboltable *subtypeof, bool no_meth, bool par_only)
{
    parsex *current, *listptr;
    bool ismethod = FALSE;
    char *type, *name;

    current = spec_tree;

    switch (current->tok) {
        case PARAM:
            par_only = TRUE;

        case COMMA:
            if (current->left != NULL)
                pspec(current->left, types, variables, functions, subtypeof, no_meth,
                      par_only);

            pspec(current->right, types, variables, functions, subtypeof, no_meth,
                  par_only);
            break;

        case TYPE_SPEC:
            ptype_spec(types, current);
            break;

        case VAR_SPEC: //  add one or more variables with the same type to the

            //  symboltable
        case VAR_DECL: //  parameter specifications have token VAR_DECL
            type = current->right->str;
            listptr = current->left;

            while (listptr != NULL) {
                name = listptr->left->str;
                setlf(listptr->left->lf);
                addvariable(variables, name, type,
                            no_meth ? GLOBALSYM : INVISIBLESYM, NULL,
                            listptr->left->lf);
                listptr = listptr->right;
            }

            break;

        case SUBTYPE: //  subtype names are stored in a symboltable too
            if (no_meth) {
                setlf(current->left->lf);
                error("No subtype specifications allowed here !\n");
                break;
            }

            addsubtype(subtypeof, current->left->str);
            break;

        case METHOD_SPEC:
            if (no_meth) { //  outside classes no methods allowed
                setlf(current->left->lf);
                error("No method specifications allowed here !\n");
                break;
            }

            ismethod = TRUE;

        case FUNC_SPEC:
            pfunc_spec(functions, current, ismethod);
            break;

        case IDENT:
            if (!par_only) { //  in parameterspecifications single type
                //  identifiers are allowed
                setlf(current->lf);
                error("No name for variable with type \"%s\" specified\n",
                      current->str);
                break;
            }

            type = current->str;
            name = calloc((unsigned)5, sizeof(char));
            sprintf(name, "%d", dummy++);
            addvariable(variables, name, type, 0, NULL, current->lf);
            break;
    }
}



/*
 * Parse the list of use_files and put them in a symboltable.
 */
void pusefiles(parsex *file_tree, symboltable *use_files,
               char *current_file)
{
    parsex *ptr;
    filex *nf;

    ptr = file_tree;

    while (ptr != NULL) {
        nf = (filex *)malloc(sizeof(filex));

        switch (ptr->left->tok) {
            case (IDENT):
                nf->name = strdup(ptr->left->str);
                break;

            case (STRCONST):
                nf->name = strdup(ptr->left->str + 1);
                nf->name[strlen(ptr->left->str) - 2] = '\0';
                break;

            default:
                error("illegal file specification \n");
                exit(0);
        }

        // only files that aren't already in the list and that are not the same
        // as the current file, are added.
        if ((searchsymbol2(use_files, nf->name) == -1) &&
            (strcmp(current_file, nf->name))) {
            addsymbol(use_files, (char *)nf);
        }

        ptr = ptr->right;
    }
} //  pusefiles



/*
 * Parse type specification. Type specifications look the same as type
 * declarations.
 */
void ptype_spec(symboltable *t, parsex *type_spec)
{
    ptype_decl(t, type_spec, 0);
}



/*
 * Parse function specification
 */
void pfunc_spec(symboltable *t, parsex *mf, bool ismethod)
{
    symboltable *parameters = newsymboltable(sizeof(varx));
    char *rettype = NULL;
    char *reptype = NULL;

    if (mf->right != NULL) { //  parse parameter specifications
        pspec(mf->right, NULL, parameters, NULL, NULL, TRUE, TRUE);
    }

    if (mf->e0 != NULL) { //  find returntype
        rettype = mf->e0->str;
    }

    if (mf->e1 != NULL) { //  find replytype (methods only)
        reptype = mf->e1->str;
    }

    setlf(mf->left->lf);
    addfunction(t, mf->left->str, rettype, reptype, parameters, NULL, NULL,
                ismethod, 0, mf->left->lf);
}



/*
 * Parse class specification. Parse global specifications,
 * subtypespecifiactions and classspecifications.
 */
void pclass_spec(symboltable *t, parsex *class_spec)
{
    symboltable *types = newsymboltable(sizeof(typex));
    symboltable *variables = newsymboltable(sizeof(varx));
    symboltable *func_meths = newsymboltable(sizeof(mfpx));
    symboltable *subtypeof = newsymboltable(sizeof(namex));
    symboltable *localvars = newsymboltable(sizeof(varx));

    if (class_spec->right != NULL)
        pspec(class_spec->right, types, variables, func_meths, subtypeof,
              FALSE, FALSE);

    setlf(class_spec->left->lf);
    addclass(t, class_spec->left->str, NULL, types, variables, func_meths,
             subtypeof, 0, class_spec->left->lf, localvars);
}

