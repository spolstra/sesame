/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file manipulate the list of subtype relations.
 */

#include <string.h>
#include <stdlib.h>

#include "subtype.h"
#include "symboldef.h"
#include "util.h"


extern symboltable *newsymboltable(), *use_files;
symboltable *subtype_table;


/*
 * Creates a table for subtype relations.
 */
void init_subtypetable(symboltable **table)
{
    *table = newsymboltable(sizeof(subtype_rel));
} //  init_subtypetable



/*
 * Insert a subtype relation in the table of subtype relations, only if it
 * is not already there.
 */
void insert_relation(symboltable *table, char *relation)
{
    for (unsigned int i = 0; i < symbol_getfill(table); i++) {
        subtype_rel *rel_i = (subtype_rel *)symbol_getcont(table, i);

        if (!strcmp(rel_i->type, ((subtype_rel *) relation)->type) &&
            !strcmp(rel_i->subtype, ((subtype_rel *) relation)->subtype)) {
            return;  //  do not insert this duplicate relation
        }
    }

    insertsymbol(table, relation);
} //  insert_relation



/*
 * Add subtype relations of all classes.
 */
void add_relations(symboltable *table, symboltable *classes)
{
    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        classx *class = (classx *)symbol_getcont(classes, i);

        for (unsigned int j = 0; j < symbol_getfill(class->subtypeof); j++) {
            namex *st_of = (namex *)symbol_getcont(class->subtypeof, j);
            subtype_rel *new_rel = (subtype_rel *)malloc(sizeof(subtype_rel));
            new_rel->type = st_of->name;
            new_rel->subtype = class->name;
            new_rel->init = FALSE;
            insert_relation(table, (char *)new_rel);
        }
    }
} //  add_relations



/*
 * Returns TRUE if a subtype relation between "type" and "subtype" is found
 * in the table, else FALSE is returned.
 */
bool is_subtypeof(char *type, char *subtype)
{
    for (unsigned int i = 0; i < symbol_getfill(subtype_table); i++) {
        subtype_rel *rel_i = (subtype_rel *)symbol_getcont(subtype_table, i);

        if (!strcmp(rel_i->type, type))
            if (!strcmp(rel_i->subtype, subtype)
                || is_subtypeof(rel_i->subtype, subtype)) {
                return (TRUE);
            }
    }

    //  Add code here to check between automatic subtype realtions

    return (FALSE);
} //  is_subtypeof
