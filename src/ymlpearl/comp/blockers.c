/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "blockers.h"
#include "iscode.h"

extern char *class_name;

static struct blockerx {
    char **method;
    int nmethod;
    int maxmethod;
} *blocks;

static int nblocks = 0;
static int maxblocks = 0;

int defn, defmax;
static char **deflist;

static void stretch(void **pointer, int size, int *max)
{
    int oldmax, extra;

    if (*pointer == NULL) {
        *max = 10;
        *pointer = calloc(*max, size);

    } else {
        oldmax = *max;
        extra = oldmax;
        *max += extra;
        *pointer = realloc(*pointer, *max * size);
        memset(*pointer + oldmax * size, 0, extra * size);
    }
}


int newblocker(void)
{
    if (nblocks == maxblocks) {
        stretch((void *)&blocks, sizeof(struct blockerx), &maxblocks);
    }

    blocks[nblocks].nmethod = 0;

    return nblocks;
}

void appendblocker(char *meth)
{
    struct blockerx *bl = &blocks[nblocks];

    if (bl->nmethod == bl->maxmethod) {
        stretch((void **)&bl->method, sizeof(char *), &bl->maxmethod);
    }

    bl->method[bl->nmethod++] = meth;
}

void appendanonymousblocker(char *meth)
{
    int i;

    appendblocker(meth);

    for (i = 0; i < defn; i++)
        if (strcmp(deflist[i], meth) == 0) {
            return;
        }

    if (defn == defmax) {
        stretch((void *)&deflist, sizeof(char *), &defmax);
    }

    deflist[defn++] = meth;
}


static char *mkname(int i)
{
    static char blockname[32];

    sprintf(blockname, "block_%03d_%s", i, class_name);

    return blockname;
}

char *closeblocker(void)
{
    int i, j;
    int same;

    for (i = 0; i < nblocks; i++) {
        if (blocks[i].nmethod == blocks[nblocks].nmethod) {
            same = 1;

            for (j = 0; j < blocks[nblocks].nmethod; j++)
                if (strcmp(blocks[nblocks].method[j], blocks[i].method[j]) != 0) {
                    same = 0;
                    break;
                }

            if (same) {
                return mkname(i);
            }
        }
    }

    return mkname(nblocks++);
}

void defblockers(char *name, int state)
{
    int i;

    icode("blocker_def(method_%s, %d);\n", name, state);

    for (i = 0; i < defn; i++) {
        if (deflist[i] != NULL && strcmp(deflist[i], name) == 0) {
            deflist[i] = NULL;
        }
    }
}

void dumpblockersdef(void)
{
    int i;

    for (i = 0; i < nblocks; i++) {
        icode("intptr_t %s;\n", mkname(i));
    }
}

void dumpblockers(char *class)
{
    int i, j;

    for (i = 0; i < defn; i++) {
        if (deflist[i] != NULL) {
            icode("blocker_def(method_%s, 10003123);\n", deflist[i]);
            icode("%s_install_mesgidq(method_%s);\n", class, deflist[i]);
        }
    }

    for (i = 0; i < nblocks; i++) {
        icode("blocker_new();\n");

        for (j = 0; j < blocks[i].nmethod; j++) {
            icode("blocker_append(method_%s);\n", blocks[i].method[j]);
        }

        icode("%s  = blocker_close();\n", mkname(i));
    }

    nblocks = 0;
    defn = 0;
}

