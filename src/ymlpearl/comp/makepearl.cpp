/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 * @file makepearl.cpp
 * @author Joseph Coffland
 *
 * @brief Pearl compiler driver and make utility.
 *
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

#include "util.h"
#include "../libutil/absolute.h"
#include "pearllib.h"

#include "../../BasicUtils/BasicProcess.h"
#include "../../BasicUtils/BasicException.h"
#include "../../BasicUtils/BasicDebugger.h"

#include <iostream>
#include <string>

using namespace std;


#define SOLARIS

#ifdef __GNUC__
#define C_COMPILER      "gcc"
#else
#define C_COMPILER      "cc"
#endif

#define PEARL_COMPILER  "ymlpearlc"

int nerr = 0;   //!< Compile error counter.

bool  dolinkflag = TRUE;
bool  boundcheck = FALSE;
bool  debugger = FALSE;
bool  dontlink = FALSE;
bool  dryrun = FALSE;
bool  profiler = FALSE;
bool  verbose = FALSE;
char  *optimize = (char *)"-O1";

#define MAXCLIBS        32
#define MAXLDIRS        32

FILE  *popen(), *fp_spec, *fp_impl;
char  file_spec[MAXPATHLEN], file_impl[MAXPATHLEN];
int   copts = 0;
char  *copt[ MAXCLIBS ];

/**
 * function.
 */
char *strdup(const char *s1)
{
    char *str;

    str = (char *)calloc((unsigned)strlen(s1) + 1, sizeof(char));
    strcpy(str, s1);
    return(str);
}

/**
 * function.
 */
void panic(char *s)
{
    fprintf(stderr, "Panic ? %s\n", s);
    exit(1);
}

struct usefilex {
    string file;
    string module;
    list<string> uses;
    int outdated;
};

list<struct usefilex *> head;

void addusefile(string file)
{
    head.front()->uses.push_front(file);
}


/**
 * function.
 */
void newfile(string file)
{
    struct usefilex *usefile = new usefilex;

    usefile->file = file;

    usefile->module = file.rfind('/') == string::npos ?
                      "" :
                      file.substr(file.rfind('/'));

    if (usefile->module.empty()) {
        usefile->module = usefile->file;
    } else {
        //usefile->module++;
    }

    head.push_front(usefile);
    addusefile(usefile->module);
}


/**
 * function.
 */
int fileoutdated(string filename, string suf1, string suf2)
{
    struct stat statbuf;
    string path;
    int time;

    path = filename;
    path.append(suf1);
    stat(path.c_str(), &statbuf);
    time = statbuf.st_mtime;

    path = filename;
    path.append(suf2);
    stat(path.c_str(), &statbuf);

    return time < statbuf.st_mtime;
}

/**
 * function.
 */
int parseusefile(string filename)
{
    string path;
    char filepath[ MAXPATHLEN ];
    FILE *fd;

    if (librarypath(filename.c_str(), filepath) < 0) {
        exit(1);
    }

    if (fileoutdated(filepath, ".use", ".ps")) {
        if (filename.compare(filepath) == 0) {
            return -1;
        }

        printf("%s.use is outdated\n", filepath);
    }

    path.append(filepath);
    path.append(".use");
    fd = fopen(path.c_str(), "r");

    if (fd == NULL) {
        return -1;
    }

    newfile(filepath);

    char new_path[MAXPATHLEN];

    while (fscanf(fd, "$%s %*[^$]", new_path) != -1) {
        addusefile(new_path);
    }

    fclose(fd);
    return 0;
}

/**
 * function.
 */
string nextfiletobeused()
{
    bool found = false;

    for (list<struct usefilex *>::iterator t = head.begin(); t != head.end(); t++) {
        for (list<string>::iterator u = (*t)->uses.begin(); u != (*t)->uses.end(); u++) {
            found = false;

            for (list<struct usefilex *>::iterator z = head.begin(); z != head.end(); z++) {
                if ((*z)->module.compare((*u)) == 0) {
                    found = true;
                    break;
                }
            }

            if (found == false) {
                return (*u);
            }
        }
    }

    return "";
}


/**
 * function.
 */
void findoutdated()
{
    int **usematrix;
    int *pstimes, *pitimes, *ctimes;
    int modif, outdated;
    string filename;
    struct stat statbuf;

    usematrix = (int **) malloc(sizeof(int *) * head.size());
    pstimes = (int *)malloc(sizeof(int) * head.size());
    pitimes = (int *)malloc(sizeof(int) * head.size());
    ctimes = (int *)malloc(sizeof(int) * head.size());

    for (size_t i = 0; i < head.size(); i++) {
        usematrix[i] = (int *) malloc(sizeof(int) * head.size());

        for (size_t j = 0; j < head.size(); j++) {
            usematrix[i][j] = 0;
        }
    }

    list<struct usefilex *>::iterator u;
    int i;

    for (u = head.begin(), i = 0;
         u != head.end();
         u++, i++) {

        filename = (*u)->file;
        int j = filename.size();
        filename.append(".o");

        if (stat(filename.c_str(), &statbuf) < 0) {
            ctimes[i] = 0;
        } else {
            ctimes[i] = statbuf.st_mtime;
        }

        filename.erase(j);
        filename.append(".pi");
        stat(filename.c_str(), &statbuf);
        pitimes[i] = statbuf.st_mtime;

        filename.erase(j);
        filename.append(".ps");
        stat(filename.c_str(), &statbuf);
        pstimes[i] = statbuf.st_mtime;

        for (list<string>::iterator l = (*u)->uses.begin();
             l != (*u)->uses.end();
             l++) {

            list<struct usefilex *>::iterator z;
            int j;
            bool found = false;

            for (z = head.begin(), j = 0;
                 z != head.end();
                 z++, j++) {

                if ((*z)->module.compare((*l)) == 0) {
                    found = true;
                    break;
                }
            }

            if (found == false) {
                printf("Help, cannot find %s in files\n", (*l).c_str());
                exit(1);
            }

            usematrix[i][j] = 1;
        }
    }

    do {        /* Compute transitive closure, not the fastest way */
        modif = 0;

        for (size_t i = 0; i < head.size(); i++) {
            for (size_t j = 0; j < head.size(); j++) {
                if (i != j && usematrix[i][j]) {
                    for (size_t k = 0; k < head.size(); k++) {
                        if (usematrix[j][k] && !usematrix[i][k]) {
                            usematrix[i][k] = 1;
                            modif = 1;
                        }
                    }
                }
            }
        }
    } while (modif);

    /*
      for(i=0; i<nfile; i++) {
        for(j=0; j<nfile; j++) {
          printf("%d ", usematrix[i][j]);
        }
        printf("\n");
      }
    */

    {
        list<struct usefilex *>::iterator u = head.begin();

        for (size_t i = 0; i < head.size(); i++) {
            if (ctimes[i] < pitimes[i]) {
                outdated = 1;
            } else {
                outdated = 0;

                for (size_t j = 0; j < head.size(); j++) {
                    if (usematrix[i][j] && ctimes[i] < pstimes[j]) {
                        outdated = 1;
                        break;
                    }
                }
            }

            (*u)->outdated = outdated;
            u++;
            free(usematrix[i]);
        }
    }

    free(usematrix);
    free(pstimes);
    free(pitimes);
    free(ctimes);
}

/**
 * function.
 */
string tobecompiled()
{
    for (list<struct usefilex *>::iterator u = head.begin();
         u != head.end();
         u++) {
        if ((*u)->outdated) {
            (*u)->outdated = 0;

            if ((*u)->file.compare((*u)->module) == 0) {
                return (*u)->file;
            } else {
                printf("Library module %s is outdated\n", (*u)->file.c_str());
            }
        }
    }

    return "";
}

/**
 * function.
 */
int doexec(int argc, char *const argv[])
{
    int i = 1; /* non-zero so that wait will return a value */
    int j;

    fprintf(stdout, "@");

    for (j = 0; j < argc; j++) {
        fprintf(stdout, " %s", argv[j]);
    }

    fprintf(stdout, "\n");

    if (dryrun) {
        return 0;
    }

    switch (fork()) {
        case 0:
            execvp(argv[0], argv);
            perror("Error executing child process");
            exit(1);
            break;

        case -1:
            perror("Cannot fork");
            exit(1);
            break;

        default:
            wait(&i);
            break;
    }

    return -i;
}

/**
 * function.
 */
list<string> addLibs(list<string> argv)
{
    char *bArgv[3];

    bArgv[0] = (char *)"sesamesim-config";
    bArgv[1] = (char *)"--ymlpearl-libs";
    bArgv[2] = 0;

    BasicProcess proc;
    BasicPipe *pipe = proc.getChildPipe(BasicProcess::FROM_CHILD, 1);
    proc.exec(bArgv);
    string ary;
    istream *output = pipe->getOutStream();

    while (!output->eof()) {
        ary.push_back((char)output->get());
    }

    // Overwrite eof
    ary[ary.size() - 1] = 0;

    proc.wait();

    // Parse result
    char *buf = strdup(ary.c_str());
    bool inArg = false;

    size_t s1 = 0;

    for (int i = 0; buf[i]; i++) {
        if (inArg) {
            if (strchr(" \t\n\r", buf[i])) {
                buf[i] = 0;

                if (inArg == true) {
                    argv.push_back(&buf[s1]);
                }

                inArg = false;
            }

        } else {
            if (strchr(" \t\n\r", buf[i]) == 0) {
                if (inArg == false) {
                    s1 = i;
                }

                inArg = true;
            }
        }
    }

    return argv;
}

/**
 * Compile a module.
 */
int compile(string filename, std::list<std::string> defines, std::list<std::string> includes)
{
    list<string> argv;
    int argc;
    int i;

    argc = 0;

    argv.push_back(string(PEARL_COMPILER));

    argv.splice(argv.end(), defines);

    if (boundcheck) {
        argv.push_back("-b");
    }

    if (verbose) {
        argv.push_back("-v");
    }

    for (i = 0; i < copts; i++) {
        argv.push_back(copt[i]);
    }

    argv.push_back(filename);


    char **argv_arr = (char **)malloc(sizeof(char *) * argv.size() + 1);
    argc = argv.size();
    argv_arr[argc] = NULL;

    for (size_t i = 0; !argv.empty(); i++) {
        argv_arr[i] = new char[argv.front().size() + 1];
        argv_arr[i][argv.front().size()] = 0;
        memcpy(argv_arr[i], argv.front().c_str(), argv.front().size());

        argv.pop_front();
    }

    if (doexec(argc, argv_arr) != 0) {
        nerr++;
        return -1;
    }

    for (int i = 0; i < argc; i++) {
        free(argv_arr[i]);
    }

    free(argv_arr);


    argv.push_back(C_COMPILER);
    argv.splice(argv.end(), includes);
    argv.push_back("-c");
    argv.push_back("-w");

    if (debugger) {
        argv.push_back("-g3");
        argv.push_back("-gdwarf-2");
    } else {
        argv.push_back(optimize);
    }

    if (profiler) {
        argv.push_back("-pg");
    }

    argv.push_back(filename + ".c");


    argv_arr = (char **)malloc(sizeof(char *) * argv.size() + 1);
    argc = argv.size();
    argv_arr[argc] = NULL;

    for (size_t i = 0; !argv.empty(); i++) {
        argv_arr[i] = new char[argv.front().size() + 1];
        argv_arr[i][argv.front().size()] = 0;
        memcpy(argv_arr[i], argv.front().c_str(), argv.front().size());

        argv.pop_front();
    }

    if (doexec(argc, argv_arr) != 0) {
        nerr++;
        return -1;
    }

    for (int i = 0; i < argc; i++) {
        free(argv_arr[i]);
    }

    free(argv_arr);

    return 0;
}

/**
 * function.
 */
void dolink(string filename, int ldargc, char *ldargv[])
{
    list<string> argv;
    char path[MAXPATHLEN];
    int argc;
    int i;

    argv.push_back(string(C_COMPILER));

    // TODO: linking does not need debugging flags.
    if (debugger) {
        argv.push_back("-g3");
        argv.push_back("-gdwarf-2");
    }

    if (profiler) {
        argv.push_back("-pg");
    }

    argv.push_back("-o");
    argv.push_back(filename);
    argv.push_back(filename + "..o");

    for (list<struct usefilex *>::iterator u = head.begin();
         u != head.end();
         u++) {
        argv.push_back((*u)->file + ".o");
    }

    for (i = 0; i < copts; i++) {
        librarypath(copt[i] + 2, path);
        string tmp = path;
        argv.push_back(tmp + ".o");
    }

    for (i = 0; i < ldargc; i++) {
        argv.push_back(ldargv[i]);
    }

    argv = addLibs(argv);

    char **argv_arr = (char **)malloc(sizeof(char *) * argv.size() + 1);
    argc = argv.size();
    argv_arr[argc] = 0;

    for (size_t i = 0; !argv.empty(); i++) {
        argv_arr[i] = new char[argv.front().size() + 1];
        argv_arr[i][argv.front().size()] = 0;
        memcpy(argv_arr[i], argv.front().c_str(), argv.front().size());

        argv.pop_front();
    }

    if (doexec(argc, argv_arr) < 0) {
        exit(1);
    }

}

static char *newmain = NULL;
static int mainlen = 0, maincurlen = 0;

/**
 * function.
 */
void mainout(string s)
{
    int len = s.size();

    if (mainlen == 0) {
        mainlen = 200;
        newmain = (char *)malloc(mainlen);
    }

    if (len + maincurlen > mainlen) {
        mainlen = mainlen * 2 + len;
        newmain = (char *)realloc(newmain, mainlen);
    }

    memcpy(newmain + maincurlen, s.c_str(), len);
    maincurlen += len;
}

/**
 * function.
 */
void makemain(string filename)
{
    list<string> argv;
    int mn;
    char *x;
    string path;

    path = filename + "..c";

    mainout("int method_statistics;\n");

    mainout("void main_install_modules() {\n");

    for (list<struct usefilex *>::iterator u = head.begin();
         u != head.end();
         u++) {
        mainout("  ");
        mainout((*u)->module);
        mainout("_module();\n");
    }

    mainout("}\n\n");

    mainout("void main_install_blocks() {\n");

    for (list<struct usefilex *>::iterator u = head.begin();
         u != head.end();
         u++) {
        mainout("  ");
        mainout((*u)->module);
        mainout("_blocks();\n");
    }

    mainout("}\n\n");

    mn = open(path.c_str(), O_RDWR | O_CREAT , 0644);

    if (mn < 0) {
        perror(path.c_str());
        exit(1);
    }

    x = (char *)malloc(mainlen + 1);

    if (fileoutdated(filename, "..o", "..c") ||
        read(mn, x, mainlen + 1) != maincurlen ||
        memcmp(x, newmain, maincurlen) != 0) {
        lseek(mn, 0, L_SET);

        if (write(mn, newmain, maincurlen) == -1) {
            /* TODO: Handle error */
        }

        if (ftruncate(mn, maincurlen) == -1) {
            /* TODO: Handle error */
        }

        close(mn);

        argv.push_back(C_COMPILER);
        argv.push_back("-c");

        if (debugger) {
            argv.push_back("-g3");
            argv.push_back("-gdwarf-2");
	    argv.push_back("-Wno-implicit-function-declaration");
        }

        argv.push_back(path);

        char **argv_arr = (char **)malloc(sizeof(char *) * argv.size() + 1);
        int argc = argv.size();
        argv_arr[argc] = NULL;

        for (size_t i = 0; !argv.empty(); i++) {
            argv_arr[i] = new char[argv.front().size() + 1];
            argv_arr[i][argv.front().size()] = 0;
            memcpy(argv_arr[i], argv.front().c_str(), argv.front().size());

            argv.pop_front();
        }

        doexec(argc, argv_arr);

        for (int i = 0; i < argc; i++) {
            free(argv_arr[i]);
        }

        free(argv_arr);
    }

    free(x);
    free(newmain);
    mainlen = maincurlen = 0;
}

/**
 * Print usage information and exit.
 */
void usage(void) {
    printf("Usage: ymlpearl [options] file\n"
            "  -h, --help    Display this information\n"
            "  -v            Verbose mode\n"
            "  -O <arg>      Optimisation level, passed to gcc\n"
            "  -Dname        Define name as 1\n"
            "  -Dname=value  Define name as value\n"
            "  -n            Only print commands, don't execute them\n"
            "  -C            Stop before linking\n"
            "  -s            Don't generate project..c file\n"
            "  -g            Produce debugging information\n"
            "  -p            Enable profiling\n"
            "  -cobject      Link with object\n"
            "  -Llib_path    Add lib_path to library search path\n"
            "  -b            Enable bound checking\n");
    exit(1);
}

/**
 * program
 */
int main(int argc, char *argv[])
{
    try {
#ifdef __DEBUG__
        BasicDebugger::initStackTrace(argv[0]);
#endif

        int first;
        char *s, *cpy;
        string t;
        string path;
        string outputfile;
        list<string> defines;
        list<string> includes;

        // Extract the root directory from the executable path to
        // act as a default to initialise the library path.
        if ((s = strrchr(argv[0], '/'))) {
            // If '/' in path construct: "/root/path/bin/../"
            cpy = (char *)malloc(s - argv[0] + 4 + 1);
            strncpy(cpy, argv[0], s - argv[0]);
            cpy[s - argv[0]] = '\0';
            strcat(cpy, "/../");
            // and use it as a default to initialise #libpath
            initlibpath(cpy);
            initpearlpath(cpy);
            libraryinit(cpy);
        } else {
            // Otherwise use current working directory.
            initlibpath((char *)"./");
            initpearlpath((char *)"./");
            libraryinit((char *)"./");
        }

        // Parse arguments
        for (first = 1; first < argc; first ++) {
            if (! strcmp(argv[first], "-v")) {
                // Pass verbose flag to PEARL_COMPILER (ymlpearlc).
                verbose = TRUE;
            } else if (! strncmp(argv[first], "-O", 2)) {
                // gcc optimisation level. Pass to C_COMPILER.
                optimize = argv[first];
            } else if (! strncmp(argv[first], "-D", 2)) {
                // Pass defines to PEARL_COMPILER (ymlpearlc).
                defines.push_back(argv[first]);
            } else if (! strncmp(argv[first], "-I", 2)) {
                // Pass includes to the C compiler C_COMPILER (gcc)
                includes.push_back(argv[first]);
            } else if (! strcmp(argv[first], "-n")) {
                // Don't execute the commands.
                dryrun = TRUE;
            } else if (! strcmp(argv[first], "-C")) {
                // Compile all Pearl files and create object files but
                // don't link the component object files into the
                // final simulation executable.
                dolinkflag = FALSE;
            } else if (! strcmp(argv[first], "-s")) {
                // Don't generate the project..c file.
                dontlink = TRUE;
            } else if (! strcmp(argv[first], "-g")) {
                // Turn on debugging for C_COMPILER.
                debugger = TRUE;
            } else if (! strcmp(argv[first], "-p")) {
                // Turn on gprof profiling in C_COMPILER.
                profiler = TRUE;
            } else if (! strncmp(argv[first], "-c", 2)) {
                // Pass C object files to ymlpearlc and the linker
                // phase.
                // Both object file and pearl pc file needed, so
                // -cmyfuncs needs myfuncs.o and myfuncs.pc
                copt[ copts++ ] = argv[ first ];
            } else if (! strncmp(argv[first], "-L", 2)) {
                // Add a path to the pearl library search path #ldir.
                if (argv[first][2] == '\0') {
                    addlibdir(argv[ ++first ]);
                } else {
                    addlibdir(argv[ first ] + 2);
                }
            } else if (! strcmp(argv[first], "-b")) {
                // Generate bound checking code.
                boundcheck = TRUE;
            } else if (!strcmp(argv[first], "-h") ||
                       !strcmp(argv[first], "--help")) {
                usage();
            } else {
                break;
            }
        }

        if (first == argc) {
            // No args? Print usage and exit.
            usage();
        }

        // Get main module from commandline
        path.append(argv[first++]);


        // Remove extension.
        if (path.compare(path.size() - 3, 3, ".pi") == 0 ||
            path.compare(path.size() - 3, 3, ".ps") == 0) {
            path = path.erase(path.size() - 3);
        }

        if (parseusefile(path) < 0) {
            if (compile(path, defines, includes) < 0) {
                nerr++;
            }

            if (parseusefile(path) < 0) {
                /* Serious error duiring parse of .ps */
            }
        }

        if (outputfile.empty()) {
            outputfile = path;
        }

        while (!(t = nextfiletobeused()).empty()) {
            if (parseusefile(t) < 0) {
                if (compile(t, defines, includes) < 0) {
                    nerr++;
                }

                if (parseusefile(t) < 0) {
                    /* Serious error during parse of .ps */
                }
            }
        }

        findoutdated();

        while (!(t = tobecompiled()).empty()) {
            if (compile(t, defines, includes) < 0) {
                nerr++;
            }
        }

        if (dontlink || nerr) {
            exit(nerr);
        }

        makemain(outputfile);

        if (nerr == 0 && dolinkflag) {
            dolink(outputfile, argc - first , &argv[ first ]);
        }

        return 0;

    } catch (BasicException &e) {
        cerr << e << endl;
    }

    return 1;
}
