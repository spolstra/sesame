/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The function "new_parsex" is used  to create a new node for the parse tree.
 */

#include <stdlib.h>

#include "ptree.h"
#include "util.h"



/*
 * Create new parse tree node
 */
parsex *new_parsex(int tok, parsex *l, parsex *r, parsex *e0,
                   parsex *e1, parsex *e2)
{
    parsex *p;
    linex *getlf();

    p = (parsex *)malloc(sizeof(parsex));
    p->tok = tok;
    p->str = NULL;
    p->left = l;
    p->right = r;
    p->e0 = e0;
    p->e1 = e1;
    p->e2 = e2;
    p->lf = getlf();
    p->sym_entry = 0;
    p->simple = -1;
    p->tsize = "0";
    p->type = 0;

    return (p);
}
