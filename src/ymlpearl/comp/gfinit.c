/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file generate the code for the initialization
 * function.
 */

#include <stdlib.h>
#include <string.h>

#include "gfinit.h"
#include "ptree.h"
#include "parse.h"
#include "iscode.h"
#include "gsimple.h"
#include "generate.h"
#include "type_size.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

unsigned optstackdepth = 100;
unsigned optmaxstack = 16384;
unsigned optminstackdepth = 2;

extern specx *specs;

/*
 * Find the appropriate initialization for variable 'var'. Return NULL if none
 * is found.
 */
parsex *find_init(char *var, parsex *inits)
{
    while (inits) {
        if (!strcmp(var, inits->right->left->str)) {
            return (inits->right);
        }

        inits = inits->left;
    }

    return NULL;
}



/*
 * Determines the sum of the sizes of the variables in the symboltable
 * 'variables'.
 */
char *getvarsize(symboltable *variables)
{
    char *tsize;
    varx *var;

    tsize = sizestring(0);

    for (unsigned int i = 0; i < symbol_getfill(variables); i++) {
        var = (varx *)symbol_getcont(variables, i);

        if (isclasstype((typex *)var->realtype)) {
            tsize = sizeeval("f+s", tsize, "1");
        } else {
            tsize = sizeeval("f+s", tsize, ((typex *) var->realtype)->typesize);
        }
    }

    return tsize;
}

void varinitstart(classx *cl)
{
    icode("struct initiate_message {\n");

    icode("intptr_t n;\n");
    icode("intptr_t size;\n");
    icode("intptr_t data[1];\n");

    icode("};\n\n");

    icode("%s_env *%s_initiate(struct %s_env *env, ",
          cl->name, cl->name, cl->name);
    icode("struct initiate_message *msg) {\n", cl->name);

    icode("int maxsize;\n");


    icode("intptr_t *msgx, *sp, *ENV;\n");

    //icode("struct %s_env *realloc();\n\n", cl->name);

    icode("msgx = &msg->data[msg->n];\n\n");
}

typedef struct bclistx {
    char *string;
    struct bclistx *next;
} bclistx;


#define MIN_STACK 128
bclistx *bchead = NULL;

void newbcopylist(char *s)
{
    bclistx *t, **l = &bchead;

    if (sizesimple(s) && atoi(s) < MIN_STACK) {
        return;
    }

    for (t = bchead; t != NULL; l = &(t->next), t = t->next)
        if (strcmp(t->string, s) == 0) {
            return;
        }

    t = *l = (bclistx *)malloc(sizeof(bclistx));
    t->next = NULL;
    t->string = s;
}

void dumpbcopylist(void)
{
    bclistx *t, *tt;

    t = bchead;

    while (t != NULL) {
        icode("if (%s > maxsize) maxsize = %s;\n", t->string, t->string);
        tt = t;
        t = t->next;
        free(tt);
    }

    bchead = NULL;
}

void varinitend(classx *cl)
{
    mfpx *mfp;

    icode("maxsize = %d;\n", MIN_STACK);

    for (unsigned int i = 0; i < symbol_getfill(cl->func_meths); i++) {
        mfp = (mfpx *) symbol_getcont(cl->func_meths, i);

        if (!sizesimple(mfp->localsize) || atoi(mfp->localsize) > MIN_STACK)
            icode("if (%s > maxsize) maxsize = %s; /* %s */\n",
                  mfp->localsize, mfp->localsize, mfp->name);

        if (!sizesimple(mfp->paramsize) || atoi(mfp->paramsize) > MIN_STACK)
            icode("if (%s > maxsize) maxsize = %s;\n",
                  mfp->paramsize, mfp->paramsize);
    }

    dumpbcopylist();

    icode("if (maxsize * %u > %u) {\n", optstackdepth, optmaxstack);
    icode("if (maxsize * %u > %u) {\n", optminstackdepth, optmaxstack);
    icode("maxsize *= %u;\n", optminstackdepth);
    icode("printf(\"%s: Warning allocating %%d byte stack\\n\", 2 * maxsize);\n",
          cl->name);
    icode("} else {\n");
    icode("maxsize = %u;\n", optmaxstack);
    icode("}\n");
    icode("} else {\n");
    icode("maxsize *= %u;\n", optstackdepth);
    icode("}\n");

    icode("env->sp = env->pp = (intptr_t *)malloc(maxsize * sizeof(intptr_t));\n\n");

    icode("return env;\n");
    icode("}\n\n", cl->name);
}


/*
 *
 */
void ginitial(varx *vp, classx * UNUSED(cl))
{
    if (issimple(vp->init)) {
        icode("ENV[%s] = ", vp->offset);
        gsimple(vp->init);
        icode(";\n");

    } else {
        icode("sp = malloc(%s);\n", vp->init->tsize);
        generate(vp->init);
        icode("bcopy(sp, ENV + %s, %s * sizeof(intptr_t));\n",
              vp->offset, vp->init->tsize);
        icode("free(sp);\n");
    }
}

/*
 * Generates the initialisation function of a class.
 * First the local initializations of non-class variables are implemented,
 * then the values in the initialization message are copied to the appropriate
 * instance variables. Finally the local initialization of class variables is
 * done and the instances are made.
 */
void dovarinit(classx *cl, varx *vp, int idx)
{
    classx *s_cl;
    typex *tp;
    varx *s_vp = 0;

    s_cl = (classx *)searchsymbol1(specs->classes, cl->name);

    if (!s_cl) {
        setlf(cl->lf);
        panic("Class \"%s\" not found in specification file.", cl->name);
    }

    tp = (typex *)vp->realtype;
    s_vp = (varx *)searchsymbol1(s_cl->variables, vp->name);

    if (s_vp) {
        icode("if (msg->n > %d) {\n", idx, idx);
        icode("if (msg->data[%d]) {\n", idx, idx);

        if (isclasstype((typex *)vp->realtype) ||
            strcmp(tp->typesize, "1") == 0) {

            icode("ENV[%s] = msg->data[msgx[%d]];\n", vp->offset, idx);
            icode("if (msg->data[%d] != 1) {\n", idx);
            icode("    panic(\"%%s: size of init string argument nr %d is %%d, should be 1\", whoami(), ", idx + 1);
            icode("msg->data[%d]);\n}\n", idx);

        } else {

            icode("bcopy(&msg->data[msgx[%d]], ENV + %s, %s * sizeof(intptr_t));\n",
                  idx, vp->offset, tp->typesize);
            icode("if (msg->data[%d] != %s)\n", idx, tp->typesize);
            icode("    panic(\"%%s: size of init string argument nr %d is %%d, should be %%d\", whoami(), ", idx + 1);
            icode("msg->data[%d], %s);\n", idx, tp->typesize);
        }

        icode("}\n");

        if (vp->init) {

            // Initialize with default value and check the default size matches
            // calculated size.  They may differ if this is a variable type
            // with a variable dimension.
            icode("} else {\n");
            icode("if ((%s) != (%s))\n", vp->init->tsize, tp->typesize);
            icode("    panic(\"%%s: size of init string argument nr %d is %%d, shouldbe %%d\", whoami(), ", idx + 1);
            icode("%s, %s);\n", tp->typesize, vp->init->tsize);
            ginitial(vp, cl);
            icode("}\n");

        } else {
            icode("} else {\npanic(\"%%s missing parameter %d\", whoami());\n}\n", idx);
        }

    } else if (vp->init) {
        ginitial(vp, cl);
    }
}
