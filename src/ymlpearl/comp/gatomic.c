/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *           by
 *      B.J. Overeinder & J.G. Stil
 *
 * Code generation for atomic expressions.
 */


#include <string.h>

#include "gatomic.h"
#include "iscode.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "parse.h"
#include "gsimple.h"
#include "generate.h"
#include "gfinit.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

extern int sp;      /* stack pointer of generated object */
extern int boundcheck;


/*
 * Set the size of the parameters and local variables, occupying the stack
 * after a function call.
 */
int install_function(char * UNUSED(paramsize), char * UNUSED(localsize))
{
    return 0;
}



/*
 * Generate atomic expressions, with options for putting atomics on stack
 * and indentation.
 * Constants are regenerated (almost) litterly and identifiers are translated,
 * dependent to their scope, to stack reverences (parameters and locals) or
 * to name reverences (class variables and global variables).
 */
static void gatomic_expr(parsex *p, bool on_stack, int d_sp)
{
    /* gatomic_expr */
    varx *vp;
    char *typo;
    int itypo;

    switch (p->tok) {
        case BOOLCONST:
            if (on_stack) {
                icode("sp[%d] = ", d_sp);
            }

            icode("%s", !strcmp(p->str, "true") ? "1" : "0");
            break;

        case INTCONST:
            if (on_stack) {
                icode("sp[%d] = ", d_sp);
            }

            icode("0x%s", p->str);
            break;

        case FLTCONST:
            if (on_stack) {
                icode("((float *)sp)[%d] = ", d_sp);
            }

            icode("%s", p->str);
            break;

        case STRCONST:
            if (on_stack) {

                icode("sp[%d] = (intptr_t) ", d_sp);
            }

            icode("%s", p->str);
            break;

        case ENUM_ELEM:
            if (on_stack) {
                icode("sp[%d] = (intptr_t) ", d_sp);
            }

            icode("%s_enum[%d]", ((typex *) p->sym_entry)->name,
                  searchsymbol2(((typex *) p->sym_entry)->fields, p->str));
            break;

        case IDENT:
            vp = (varx *) p->sym_entry;

            if (on_stack) {
                icode("sp[%d] = (intptr_t) ", d_sp);  /* Prevent conversion */
            }

            typo = get_c_type((typex *)vp->realtype);

            itypo = strcmp(typo, "intptr_t") == 0;

            switch (vp->scope) {
                case LOCALSYM:
                    if (itypo) {
                        icode("pp[%s]", vp->offset);
                    } else {
                        icode("*(%s*)(pp+%s)", typo, vp->offset);
                    }

                    break;

                case PARAMETER:
                    if (itypo) {
                        icode("pp[%s]", vp->offset);
                    } else {
                        icode("*(%s*)(pp+%s)", typo, vp->offset);
                    }

                    break;

                case CLASSSYM:
                    if (itypo) {
                        icode("ENV[%s]", vp->offset);
                    } else {
                        icode("(*(%s*) (ENV+%s))", typo, vp->offset);
                    }

                    break;

                case GLOBALSYM:
                    icode("_%s", p->str);

                    if (((p->type)->type) == TARRAY) {
                        icode("[0]");
                    }

                    break;

                default:
                    setlf(p->lf);
                    panic("variable \"%s\" has an unknown scope %d.", p->str, vp->scope);
            }

            break;

        case ARRAY:
            typo = get_c_type(p->type);

            itypo = (strcmp(typo, "intptr_t") == 0);

            if (strcmp(p->tsize, "1") == 0 && on_stack) {
                icode("sp[%d] = ", d_sp);
            }

            if (issimple(p->right)) {
                if (itypo) {
                    icode("*(&");
                } else {
                    icode("*(%s *)(&", typo);
                }

                gatomic_expr(p->left, 0, d_sp);
                icode("+%s*(", isclasstype(p->type) ? "1" : p->type->typesize);

                if (boundcheck) {
                    icode("bounderror(");
                    gsimple(p->right);
                    icode(",%s,",
                          ((typex *)symbol_getcont(p->left->type->fields, 1))->typesize);
                    icode("%d)", p->lf->line);
                } else {
                    gsimple(p->right);
                }

                icode("))");
            } else {
                generate(p->right);

                if (boundcheck) {
                    icode("bounderror(sp[%d]", sp);
                    icode(",%s,",
                          ((typex *)symbol_getcont(p->left->type->fields, 1))->typesize);
                    icode("%d);\n", p->lf->line);
                }

                icode("sp[%d] = ", d_sp);

                if (itypo) {
                    icode("*(&");
                } else {
                    icode("*(%s *)(&", typo);
                }

                sp++;
                gatomic_expr(p->left, 0, d_sp);
                sp--;
                icode("+%s * sp[%d])",
                      isclasstype(p->type) ? "1" : p->type->typesize, sp);
            }

            break;

        case STRUCT:
            typo = get_c_type(p->type);

            if (strcmp(p->tsize, "1") == 0 && on_stack) {
                icode("sp[%d] = ", d_sp);
            }

            if (strcmp(typo, "intptr_t") == 0) {
                icode("*(&");
            } else {
                icode("*(%s *)(&", typo);
            }

            gatomic_expr(p->left, 0, d_sp);
            icode(" + %s)", ((varx *) p->right->sym_entry)->offset);
            break;

        default:
            setlf(p->lf);
            panic("this is not an atomic expression: token %d.", p->tok);
    }
}       /* gatomic_expr */



/*
 * Generate atomic expression (sec).
 */
void gatomic(parsex *p)
{
    /* gatomic */
    gatomic_expr(p, FALSE, sp);
}       /* gatomic */



/*
 * Generate atomic expression which is (also) placed on the stack.
 */
void gstack_atomic(parsex *p)
{
    /* gstack_atomic */
    if (strcmp(p->tsize, "1") == 0) {
        gatomic_expr(p, TRUE, sp);
    } else {
        icode("bcopy(&");
        /*
           if (p->tok == STRUCT)
           tp = p->right;
           if ((tp->type == NULL ? ((typex *) ((varx *)tp->sym_entry)->realtype) : tp->type)->type != TARRAY)
           icode("&");
         */
        gatomic(p);

        icode(", &sp[%d], %s*sizeof(intptr_t))", sp, p->tsize);

        newbcopylist(p->tsize);
    }

    icode(";\n");
}       /* gstack_atomic */
