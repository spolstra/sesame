/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The global purpose of these functions is to link identifiers and function/
 * method names, used in expressions, to their entry in the symbol table.
 * These expressions are stored as parse (sub)trees, due to the parse phase,
 * and the overall result is the linkage of parse tree nodes, indicating
 * identifiers, function/method names, etc., to their corresponding definition
 * in the symbol table.
 */


#include <string.h>

#include "link_id.h"
#include "ptree.h"
#include "parse.h"
#include "symboldef.h"
#include "util.h"
#include "standardtypes.h"

#include <stdio.h>


/*
 * Symboltables of imported specification files and of
 * the implementation file itself.
 */
extern specx *specs;
extern symboltable *types, *variables, *functions, *classes;
extern symboltable *Clib_functions;

static classx *currentclass;


/*
 * Check the indices found in the parse tree against the number of indices
 * in the type definition.
 */
bool check_indices(parsex *pt, typex *ctype, typex *basetype, unsigned int i)
{
    unsigned int j, k;
    parsex *cp;

    /*
     * j is number of indices found in the parse tree.
     */
    for (j = 0; i > 0; j++, i--) {
        for (k = 1, cp = pt; k < i; k++, cp = cp->left) {
            /* everything is done in the control part */;
        }

        if (cp->tok != ARRAY) {
            break;
        }
    }

    /*
     * If basetype of the array is an other array, then the remaining
     * indices may be of the basetype array.
     */
    if (basetype->type == TARRAY)
        if (j >= symbol_getfill(ctype->fields) - 1) {
            return (TRUE);
        } else {
            return (FALSE);
        }
    else if (j == symbol_getfill(ctype->fields) - 1) {
        return (TRUE);
    } else {
        return (FALSE);
    }
}



bool ln_enum_type_elem(parsex *pt)
{
    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        for (unsigned int j = 0; j < symbol_getfill(((classx *)symbol_getcont(classes, i))->types); j++) {
            typex *t = (typex *)symbol_getcont(((classx *)symbol_getcont(classes, i))->types, j);

            if (t->type == TENUM) {
                if (searchsymbol1(t->fields, pt->str) != NULL) {
                    pt->tok = ENUM_ELEM;
                    pt->sym_entry = (char *)t;
                    return (TRUE);
                }
            }
        }
    }

    for (unsigned int i = 0; i < symbol_getfill(types); i++) {
        typex *t = (typex *)symbol_getcont(types, i);

        if (t->type == TENUM) {
            if (searchsymbol1(t->fields, pt->str) != NULL) {
                pt->tok = ENUM_ELEM;
                pt->sym_entry = (char *)t;
                return (TRUE);
            }
        }
    }

    for (unsigned int i = 0; i < symbol_getfill(specs->types); i++) {
        typex *t = (typex *) symbol_getcont(specs->types, i);

        if (t->type == TENUM) {
            if (searchsymbol1(t->fields, pt->str) != NULL) {
                pt->tok = ENUM_ELEM;
                pt->sym_entry = (char *)t;
                return (TRUE);
            }
        }
    }

    return (FALSE);
}



/*
 * Link an identifier to the correct symbol table entry, i.e. the
 * last declaration of this identifier.
 */
void ln_id_symtab_id(parsex *pt, symboltable *classvar, symboltable *parvar,
                     symboltable *localvar)
{
    if ((pt->sym_entry = searchsymbol1(localvar, pt->str)) != NULL) {
        return;
    }

    if ((pt->sym_entry = searchsymbol1(parvar, pt->str)) != NULL) {
        return;
    }

    if ((pt->sym_entry = searchsymbol1(classvar, pt->str)) != NULL) {
        return;
    }

    if ((pt->sym_entry = searchsymbol1(variables, pt->str)) != NULL) {
        return;
    }

    if ((pt->sym_entry = searchsymbol1(specs->variables, pt->str)) != NULL) {
        return;
    }

    if (ln_enum_type_elem(pt)) {
        return;
    }

    setlf(pt->lf);
    error("id \"%s\" used, but not declared.", pt->str);
}



/*
 * Link the fields of a structure or the indices of an array (and possibly
 * a mixture of these two) of an identifier of complex type.
 */
void ln_id_symtab_fields(parsex *pt, varx *var_entry, symboltable *classmf,
                         symboltable *classvar, symboltable *parvar,
                         symboltable *localvar)
{
    unsigned int i, j, k;
    parsex *cp;
    typex *ctype, *basetype;

    /*
     * i is the number of fields and indices.
     */
    for (cp = pt, i = 0; cp->tok != IDENT; cp = cp->left, i++) {
        /* everything is done in the control part */;
    }

    /*
     * Link all those fields and indices, one after the other.
     */
    ctype = (typex *) var_entry->realtype;

    while (i > 0) {
        /* ascend to the correct field or index */
        for (j = 0, cp = pt; j < i - 1; j++, cp = cp->left)
            /* everything is done in the control part */;

        if (cp->tok == STRUCT) {
            cp = cp->right;

            if ((cp->sym_entry = searchsymbol1(ctype->fields, cp->str)) != NULL) {
                ctype = (typex *)((varx *) cp->sym_entry)->realtype;
                i -= 1;
            } else {
                setlf(cp->lf);
                error("non-existant field \"%s\" used.", cp->str);
                i = 0;      /* break-while */
            }
        } else if (cp->tok == ARRAY) {
            if (ctype->type != TARRAY) {
                setlf(cp->lf);
                error("Indexing non-array\n");
                return;
            }

            basetype = (typex *)symbol_getcont(ctype->fields, 0);

            if (!strcmp(basetype->name, "ttype")) {
                basetype = (typex *) basetype->realtype;
            }

            if (check_indices(pt, ctype, basetype, i)) {
                /* link all indices */
                for (j = 0; j < symbol_getfill(ctype->fields) - 1; j++) {
                    for (k = 0, cp = pt; k < i - 1; k++, cp = cp->left) {
                        /* everything is done in the control part */;
                    }

                    cp->sym_entry = (char *)basetype;
                    ln_id_symtab_expr(cp->right, classmf, classvar, parvar,
                                      localvar);
                    i -= 1;
                }
            } else {
                setlf(cp->lf);
                error("incorrect number of indices.");
                i = 0;      /* break-while */
            }

            ctype = basetype;
        }
    }
}



/*
 * Link an identifier of complex type, including its fields and indices.
 */
void ln_id_symtab_complex(parsex *pt, symboltable *classmf,
                          symboltable *classvar, symboltable *parvar,
                          symboltable *localvar)
{
    parsex *cp;

    cp = pt;

    while (cp->tok != IDENT) {
        cp = cp->left;
    }

    ln_id_symtab_id(cp, classvar, parvar, localvar);

    if (cp->sym_entry != NULL) {
        ln_id_symtab_fields(pt, (varx *) cp->sym_entry, classmf, classvar,
                            parvar, localvar);
    }
}



/*
 * Link the methods (with their parameters) specified in a send expression.
 */
void ln_id_symtab_method(parsex *pt, classx *class_entry,
                         symboltable *classmf, symboltable *classvar,
                         symboltable *parvar, symboltable *localvar)
{
    if (pt->left->tok != IDENT) {
        ln_id_symtab_method(pt->left, class_entry, classmf, classvar, parvar,
                            localvar);
    }

    ln_id_symtab_expr(pt->e0, classmf, classvar, parvar, localvar);
}



/*
 * Link the class (object) name and the method(s) specified in the send-
 * expression.
 */
void ln_id_symtab_send(parsex *pt, symboltable *classmf,
                       symboltable *classvar, symboltable *parvar,
                       symboltable *localvar)
{
    classx *class_entry = NULL;

    ln_id_symtab_expr(pt->left, classmf, classvar, parvar, localvar);
    ln_id_symtab_method(pt, class_entry, classmf, classvar, parvar,
                        localvar);
}



/*
 * Link the arguments of a block-expression.
 */
void ln_id_symtab_block(parsex *pt, symboltable *classmf,
                        symboltable *classvar, symboltable *parvar,
                        symboltable *localvar)
{
    parsex *cp = pt;


    /*
     * The integer expression.
     */
    if (cp->left != NULL) {
        ln_id_symtab_expr(cp->left, classmf, classvar, parvar, localvar);
    }

    /*
     * The method list.
     */
    if (cp->right != NULL) {
        if (cp->right->tok == ANY) {
            parsex *p = cp;
            classx *thisclass;
            symboltable *methods;
            symboltable *subtypes;
            char *name;
            unsigned int i, j;

            p->right = NULL;
            thisclass = (classx *)searchsymbol1(specs->classes, currentclass->name);
            methods = thisclass->func_meths;

            for (i = 0; i < symbol_getfill(methods); i++) {
                p->right = new_parsex(COMMA, NULL, NULL, NULL, NULL, NULL);
                p = p->right;
                p->left = new_parsex(IDENT, NULL, NULL, NULL, NULL, NULL);
                p->left->str = strdup(*(char **)(symbol_getcont(methods, i)));
            }

            subtypes = thisclass->subtypeof;

            for (j = 0; j < symbol_getfill(subtypes); j++) {
                name = ((namex *)symbol_getcont(subtypes, j))->name;
                methods = ((classx *)searchsymbol1(specs->classes, name))-> func_meths;

                for (i = 0; i < symbol_getfill(methods); i++) {
                    p->right = new_parsex(COMMA, NULL, NULL, NULL, NULL, NULL);
                    p = p->right;
                    p->left = new_parsex(IDENT, NULL, NULL, NULL, NULL, NULL);
                    p->left->str = strdup(*(char **)(symbol_getcont(methods, i)));
                }
            }
        }
    }

    while (cp->right != NULL) {
        cp = cp->right;
        cp->left->sym_entry = searchsymbol1(classmf, cp->left->str);

        if (cp->left->sym_entry == NULL) {
            if (strncmp(cp->left->str, "reply_", 6) != 0) {
                setlf(cp->left->lf);
                error("local method \"%s\" used, but not defined.", cp->left->str);

            } else {
                addreplytype(cp->left->str + 6);
                cp->left->sym_entry = searchsymbol1(classmf, cp->left->str);
            }
        }
    }
}



/*
 * Link the function and its arguments of a function call.
 */
void ln_id_symtab_funccall(parsex *pt, symboltable *classmf,
                           symboltable *classvar, symboltable *parvar,
                           symboltable *localvar)
{
    parsex *cp;

    cp = pt->left;

    if ((pt->sym_entry = searchsymbol1(classmf, cp->str)) != NULL) {
        //printf("linked method %s %s\n", cp->str,
        //     ((typex *)((mfpx *) pt->sym_entry)->realreturn)->name);
        /* found */;

    } else if ((pt->sym_entry = searchsymbol1(functions, cp->str)) != NULL) {
        /* found */;

    } else if ((pt->sym_entry =
                    searchsymbol1(specs->functions, cp->str)) != NULL) {
        /* found */;

    } else if ((pt->sym_entry =
                    searchsymbol1(Clib_functions, cp->str)) != NULL) {
        pt->tok = CFUNCTION_CALL;
        //printf("linked cfunc %s %s\n", cp->str,
        //     ((typex *)((mfpx *) pt->sym_entry)->realreturn)->name);
        /* found */;

    } else {
        setlf(cp->lf);
        error("function \"%s\" used, but not defined.", cp->str);
    }

    ln_id_symtab_expr(pt->right, classmf, classvar, parvar, localvar);
}



/*
 * Link class (object) name and its instance variables.
 * The instance variables can be found in the local symbol table of the
 * initiated class, but the expressions assigned to these instance
 * variables must be linked normally.
 */
void ln_id_symtab_classinst(parsex *pt, symboltable *classmf,
                            symboltable *classvar, symboltable *parvar,
                            symboltable *localvar)
{
    parsex *cp, *tp;
    symboltable *instancevar;

    /*
     * Link class (object) name.
     */
    cp = pt->left;

    if ((cp->sym_entry = searchsymbol1(classes, cp->str)) != NULL) {
        /* found ! */;
    } else if ((cp->sym_entry =
                    searchsymbol1(specs->classes, cp->str)) != NULL) {
        /* found ! */;
    } else {
        setlf(cp->lf);
        error("instantation of non-existant class \"%s\".\n", cp->str);
        return;
    }

    /*
     * Link instance variables and their assigned expressions.
     */
    instancevar = ((classx *) pt->left->sym_entry)->variables;
    cp = pt->right;

    while (cp != NULL) {
        tp = cp->right->left;

        if (cp->tok != COMMA) {
            error("Error in class initialization, use ','\n");
        } else if (cp->right->tok != ASG) {
            error("Error in class initialization, use '='\n");
        } else if (cp->right->left->tok != IDENT) {
            error("Error in class initialization, use 'id ='\n");
        } else if ((tp->sym_entry = searchsymbol1(instancevar, tp->str)) ==
                   NULL) {
            setlf(tp->lf);
            error("variable \"%s\" used, but not declared in class \"%s\".\n",
                  tp->str, pt->left->str);
        } else {
            cp->tok = SEMCOL;
            ln_id_symtab_expr(cp->right->right, classmf, classvar, parvar,
                              localvar);
        }

        cp = cp->left;
    }
}       /* ln_id_symtab_classinst */



/*
 * Link an auxiliary expression (thus also a body of a function, method,
 * class, etc.).
 */
void ln_id_symtab_expr(parsex *code, symboltable *classmf,
                       symboltable *classvar, symboltable *parvar,
                       symboltable *localvar)
{
    /* ln_id_symtab_expr */
    if (code == NULL) {
        return;
    }

    switch (code->tok) {
        case IDENT:
            ln_id_symtab_id(code, classvar, parvar, localvar);
            break;

        case STRUCT:
        case ARRAY:
            ln_id_symtab_complex(code, classmf, classvar, parvar, localvar);
            break;

        case FUNCTION_CALL:
            ln_id_symtab_funccall(code, classmf, classvar, parvar, localvar);
            break;

        case SEND:
        case SENDSEND:
            ln_id_symtab_send(code, classmf, classvar, parvar, localvar);
            break;

        case BLOCK:
        case BLOCKT:
            ln_id_symtab_block(code, classmf, classvar, parvar, localvar);
            break;

        default:
            if (code->left != NULL) {
                ln_id_symtab_expr(code->left, classmf, classvar, parvar, localvar);
            }

            if (code->right != NULL) {
                ln_id_symtab_expr(code->right, classmf, classvar, parvar, localvar);
            }

            if (code->e0 != NULL) {
                ln_id_symtab_expr(code->e0, classmf, classvar, parvar, localvar);
            }

            if (code->e1 != NULL) {
                ln_id_symtab_expr(code->e1, classmf, classvar, parvar, localvar);
            }

            if (code->e2 != NULL) {
                ln_id_symtab_expr(code->e2, classmf, classvar, parvar, localvar);
            }
    }
}       /* ln_id_symtab_expr */



/*
 * Link the variable initiation expressions, i.e. specified by
 * the declaration.
 */
void ln_id_symtab_varinit(symboltable *vartable, symboltable *classmf,
                          symboltable *classvar, symboltable *parvar,
                          symboltable *localvar)
{
    for (unsigned int i = 0; i < symbol_getfill(vartable); i++) {
        varx *vp = (varx *)symbol_getcont(vartable, i);

        if (vp->init != NULL) {
            ln_id_symtab_expr(vp->init, classmf, classvar, parvar, localvar);
        }
    }
}       /* ln_id_symtab_varinit */



/*
 * Link the variable initiation expressions and the body of the functions
 * or methods in symbol table 'mft'.
 */
void ln_id_symtab_mf(symboltable *mft, symboltable *cmft, symboltable *cvt)
{
    for (unsigned int i = 0; i < symbol_getfill(mft); i++) {
        mfpx *mf = (mfpx *)symbol_getcont(mft, i);
        ln_id_symtab_varinit(mf->locals, cmft, cvt, mf->parameters,
                             mf->locals);
        ln_id_symtab_expr(mf->code, cmft, cvt, mf->parameters, mf->locals);
    }
}       /* ln_id_symtab_mf */

/*
 * Link the variable initiation expressions and the body of the functions
 * or methods in symbol table 'mft'.
 */
void ln_id_symtab_types(symboltable *tt, symboltable *cvt)
{
    for (unsigned int i = 0; i < symbol_getfill(tt); i++) {
        typex *tx = (typex *)symbol_getcont(tt, i);

        if (tx->type == TARRAY) {
            if (symbol_getfill(tx->fields) != 2) {
                error("Huh dim != 1\nZ");
            }

            ln_id_symtab_expr(((typex *)symbol_getcont(tx->fields, 1))->range, NULL, cvt,
                              NULL, NULL);
        }
    }
}       /* ln_id_symtab_types */


/*
 * Link the variable initiation expressions, the methods and the body
 * of the classes in symbol table 'ct'.
 */
void ln_id_symtab_class(symboltable *ct)
{
    for (unsigned int i = 0; i < symbol_getfill(ct); i++) {
        classx *ccl = (classx *)symbol_getcont(ct, i);
        currentclass = ccl;
        ln_id_symtab_varinit(ccl->variables, ccl->func_meths, ccl->variables,
                             NULL, NULL);
        ln_id_symtab_types(ccl->types, ccl->variables);
        ln_id_symtab_mf(ccl->func_meths, ccl->func_meths, ccl->variables);
        ln_id_symtab_expr(ccl->code, ccl->func_meths, ccl->variables, NULL,
                          ccl->localvar);
    }
}       /* ln_id_symtab_class */



/*
 * Link the global variable initiation expressions, the functions and
 * the classes.
 */
void link_id(void)
{
    ln_id_symtab_varinit(variables, NULL, NULL, NULL, NULL);
    ln_id_symtab_types(types, NULL);
    ln_id_symtab_types(specs->types, NULL);
    ln_id_symtab_mf(functions, NULL, NULL);
    ln_id_symtab_class(classes);

    for (unsigned int i = 0; i < symbol_getfill(specs->classes); i++) {
        classx *ccl = (classx *)symbol_getcont(specs->classes, i);
        ln_id_symtab_types(ccl->types, ccl->variables);
    }
}       /* link_id */

