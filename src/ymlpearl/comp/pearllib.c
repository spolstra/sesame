/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/


/**
 * @file pearllib.c
 *
 * @brief Functions to manage the Pearl library search path list.
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "util.h"
#include "../libutil/absolute.h"
#include "pearllib.h"

#define MAXLDIRS  32

static char *ldir[MAXLDIRS];    //!< Library search list.
static int nldir = 0;           //!< Number of paths in library search list.

/**
 * Add @p string to #ldir list of library paths.
 *
 * @param string    Path to add to #ldir
 */
void addlibdir(char *string)
{
    ldir[nldir++] = string;
}

/**
 * Initialise the library search path #ldir with the content of the
 * environment variable `PEARLPATH`.
 *
 * @param argv0path     Guess `lib` directory from the executable name
 *                      if `PEARLPATH` is not set.
 */
void libraryinit(char *argv0path)
{
    char path[MAXPATHLEN];
    char *s, *x;

    s = getenv("PEARLPATH");

    if (s != NULL) {
        do {
            x = strchr(s, ':');

            if (x != NULL) {
                *x++ = '\0';
            }

            strcpy(path, s);

            if (path[strlen(path) - 1] != '/') {
                strcat(path, "/");
            }

            addlibdir(strdup(path));
            s = x;
        } while (x != NULL);
    } else {
        sprintf(path, "%s/../lib/", argv0path);
        addlibdir(strdup(path));
    }

    // Add empty string.
    ldir[nldir++] = "";

    // Add machine dependent library directory (only `linux`).
    // Not used in the current installation.
    makepath("", "lib/#/", path);
    path[strlen(path) - 1] = '\0';
    addlibdir(strdup(path));
}

/**
 * Check if there is a file called @p name`.{ps,pi,pc}` in the
 * directory @p dir.
 *
 * @param dir   Directory to check for the file.
 * @param name  Filename to search for.
 * @param path  Buffer to store the full path name.
 * @return      1 if @p name is found in @p dir, 0 otherwise.
 *              If succesful the full path name is returned in @p path.
 */
static int filepipspc(char *dir, const char *name, char *path)
{
    strcpy(path, dir);

    if (path[0]) {
        strcat(path, "/");
    }

    strcat(path, name);
    strcat(path, ".ps");

    if (access(path, R_OK) == 0) {
        return 1;
    }

    path[strlen(path) - 1] = 'i';

    if (access(path, R_OK) == 0) {
        return 1;
    }

    path[strlen(path) - 1] = 'c';

    if (access(path, R_OK) == 0) {
        return 1;
    }

    return 0;
}

/**
 * Search the library search path list #ldir for
 * @p filename`.{ps,pi,pc}`
 *
 * @param filename  Filename to search for.
 * @param filepath  Buffer to return the full path name.
 * @return  0 if @p filename is found, -1 otherwise. If succesful
 *          full path name minus the extension is return in @p filepath.
 */
int librarypath(const char *filename, char *filepath)
{
    int i = 0;

    while (!filepipspc(ldir[i], filename, filepath)) {
        i++;

        if (i >= nldir) {
            fprintf(stderr, "Module %s not found\n", filename);
            return -1;
        }
    }

    filepath[strlen(filepath) - 3] = '\0';  // Remove .pi / .ps
    return 0;
}
