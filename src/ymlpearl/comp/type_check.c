/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file do the type checking. If no type can be found
 * for an expression, NULL is returned.
 */

#include <string.h>

#include "type_check.h"
#include "ptree.h"
#include "parse.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "subtype.h"


char *g_tokstr();

extern symboltable *Clib_functions;
extern symboltable *basic_types;
typex *tc_expr();
static typex *reptype, *rettype;
static classx *class;
static bool myconst = FALSE;

int max_meth;


/*
 * Returns roottype of any type.
 */
typex *roottype(typex *tp)
{
    if (!tp) {
        return NULL;
    }

    if (tp->type == TCLASS) {
        return tp;
    }

    if (tp->realtype) {
        return (roottype((typex *) tp->realtype));
    }

    return (tp);
}


/*
 * Get type of variable
 */
typex *tc_var(parsex *var)
{
    typex *rt;
    varx *entry;

    if ((varx *) var->sym_entry == NULL) {
        return (NULL);
    }

    entry = (varx *) var->sym_entry;
    rt = (typex *) entry->realtype;

    return (rt);
}



/*
 * Determine type of an atomic expression
 */
typex *tc_atomic(parsex *at_expr)
{
    typex *rt;

    switch (at_expr->tok) {
        case IDENT:
            rt = tc_var(at_expr);
            break;

        case STRUCT: // get type of most inside field
            rt = tc_atomic(at_expr->left);
            rt = tc_var(at_expr->right);
            break;

        case ARRAY: // get basetype of array
            rt = tc_expr(at_expr->right, FALSE, FALSE, FALSE);

            if (roottype(rt) != typeinteger) {
                setlf(at_expr->lf);
                error("Index should be integer\n");
            }

            rt = (typex *) at_expr->sym_entry;
            tc_atomic(at_expr->left);
            break;

        default:
            return (NULL);
    }

    if (isclasstype(rt)) {
        at_expr->tsize = "1";
    } else if (rt) {
        at_expr->tsize = rt->typesize;
    }

    at_expr->type = rt;

    return (at_expr->type);
}



/*
 * Check type of send expression
 */
typex *tc_send(parsex *send_expr)
{
    typex *rt;
    mfpx *method;
    parsex *act_par;
    varx *form_par;
    int num_par;

    tc_expr(send_expr->left, FALSE, FALSE, FALSE);
    rt = send_expr->left->type;

    if (!isclasstype(rt)) {
        if (send_expr->tok != SENDSEND ||
            rt->type != TARRAY ||
            !isclasstype(rt =
                             (typex *)((typex *) symbol_getcont(rt->fields, 0))->
                             realtype)) {
            setlf(send_expr->lf);
            error("Left expression of \"%s\" must have (array) class type\n",
                  g_tokstr(send_expr->tok));
        }

    }

    if ((method = (mfpx *)(send_expr->right->sym_entry =
                               searchsymbol1(((classx *) rt)->func_meths,
                                       send_expr->right->str))) == NULL) {
        setlf(send_expr->right->lf);
        error("method \"%s\" used, but not defined in class \"%s\".",
              send_expr->right->str, ((classx *) rt)->name);
        return (NULL);
    }

    rt = (typex *) method->realreply;

    if (rt) {
        findsourcetype(rt->name);
    }

    if (rt == typevoid) {
        rt = NULL;
    }

    num_par = symbol_getfill(method->parameters);

    if (send_expr->e0 != NULL) {
        act_par = send_expr->e0;

        /* Check number of arguments before checking the types. This gives
         * us better error messages. */
        while ((act_par != NULL) && (num_par > 0)) {
            act_par = act_par->left;
            num_par -= 1;
        }

        if ((num_par != 0) || (act_par != NULL)) {
            setlf(send_expr->lf);
            error("Wrong number of arguments in call to method \"%s\"\n",
                  send_expr->right->str);
        }

        /* reset nr and pointer to check argument types.*/
        act_par = send_expr->e0;
        num_par = symbol_getfill(method->parameters);

        while ((act_par != NULL) && (num_par > 0)) {
            form_par = (varx *)symbol_getcont(method->parameters, num_par - 1);

            if (!tc_compatibel
                ((typex *)form_par->realtype,
                 tc_expr(act_par->right, FALSE, FALSE, FALSE),
                 &(act_par->right))) {
                setlf(send_expr->lf);
                error("Par. no. %d of method \"%s\" has wrong type\n", num_par,
                      send_expr->right->str);
            }

            num_par -= 1;
            act_par = act_par->left;
        }
    } else {
        if (num_par != 0 && strcmp(send_expr->right->str, "reply_void") != 0) {
            setlf(send_expr->lf);
            error("Wrong number of arguments in call to method \"%s\"\n",
                  send_expr->right->str);
        }
    }

    if ((send_expr->tok == SEND) && method->realreply == NULL) {
        setlf(send_expr->lf);
        error("Method %s should be called asynchronous\n", method->name);
    }

    if (send_expr->tok != SEND) {
        rt = NULL;
    }

    if (rt) {
        send_expr->tsize = rt->typesize;
    }

    send_expr->type = rt;

    return (send_expr->type);
}



/*
 * Determine returntype of function and check the types of the actual
 * parameters with the formal parameters
 */
typex *tc_func_call(parsex *func_call)
{
    typex *rt;
    parsex *act_par;
    mfpx *function;
    varx *form_par;
    int num_par;

    function = (mfpx *) func_call->sym_entry;
    rt = (typex *) function->realreturn;

    if (!hasvarpar(function->flags)) {
        num_par = symbol_getfill(function->parameters);
        act_par = func_call->right;

        while ((act_par != NULL) && (num_par > 0)) {
            form_par = (varx *)symbol_getcont(function->parameters, num_par - 1);

            if (!tc_compatibel
                ((typex *)form_par->realtype,
                 tc_expr(act_par->right, FALSE, FALSE, FALSE),
                 &(act_par->right))) {
                setlf(func_call->lf);
                error("Par. no. %d in call to function \"%s\" has wrong type\n",
                      num_par, func_call->left->str);
            }

            num_par -= 1;
            act_par = act_par->left;
        }

        if ((num_par != 0) || (act_par != NULL)) {
            setlf(func_call->lf);
            error("Wrong number of parameters in call to function call \"%s\"\n",
                  func_call->left->str);
        }

    } else {
        act_par = func_call->right;

        while (act_par != NULL) {
            tc_expr(act_par->right, FALSE, FALSE, FALSE);
            act_par = act_par->left;
        }
    }

    if (rt) {
        func_call->tsize = rt->typesize;
    } else {
        func_call->tsize = "1";
    }

    func_call->type = roottype(rt);

    return (func_call->type);
}



/*
 * Determine type of block expression. This type only exists if all methods
 * have the same returntype.
 */
typex *tc_block(parsex *block_expr)
{
    int i = 1;
    parsex *mptr;
    typex *rt1, *rt2;

    if (block_expr->right == NULL || block_expr->right->tok == ANY) {
        return (block_expr->type = NULL);
    }

    mptr = block_expr->right;
    rt1 = (typex *)((mfpx *) mptr->left->sym_entry)->realreturn;
    mptr = mptr->right;

    while (mptr != NULL) {
        rt2 = (typex *)((mfpx *) mptr->left->sym_entry)->realreturn;

        if (rt1 != rt2) {
            rt1 = NULL;
        }

        mptr = mptr->right;
        i++;
    }

    if (rt1) {
        block_expr->sym_entry = (char *)rt1;
        block_expr->tsize = rt1->typesize;
    }

    if (max_meth < i) {
        max_meth = i;
    }

    block_expr->type = roottype(rt1);

    return block_expr->type;
}



/*
 * Check type of blockt expression, and check if the specified methods
 * exists in the current class. The first argument of "blockt" must have
 * type integer.
 */
typex *tc_blockt(parsex *blockt_expr)
{
    int i = 0;
    parsex *mptr;
    typex *rt;

    rt = tc_expr(blockt_expr->left, FALSE, FALSE, FALSE);
    blockt_expr->type = typeinteger;

    if (!tc_compatibel(typeinteger, rt, &blockt_expr->left)) {
        setlf(blockt_expr->lf);
        error("First argument of \"blockt\" should have type integer\n");
    }

    blockt_expr->tsize = rt->typesize;

    for (mptr = blockt_expr->right; mptr != NULL; mptr = mptr->right) {
        i++;
    }

    if (max_meth < i) {
        max_meth = i;
    }

    return  blockt_expr->type ;
}


/*
 * Check type of if statement. The condition of "if" must have type integer.
 * "if" only returns a type if the if- and elsepart return the same type.
 */
typex *tc_if(parsex *if_expr, bool break_ok, bool return_ok,
             bool reply_ok)
{
    typex *rt_cond, *rt_if, *rt_else;

    rt_cond = tc_expr(if_expr->left, FALSE, FALSE, FALSE);

    if (!rt_cond || strcmp(rt_cond->name, "boolean")) {
        setlf(if_expr->left->lf);
        error("Conditional in \"if\" should be of type boolean\n");
        return (NULL);
    }

    rt_if = tc_expr(if_expr->right, break_ok, return_ok, reply_ok);

    if (if_expr->e0 != NULL) {
        rt_else = tc_expr(if_expr->e0, break_ok, return_ok, reply_ok);

        if (rt_if == rt_else) {
            if (rt_if) {
                if_expr->sym_entry = (char *)rt_if;
                if_expr->tsize = rt_if->typesize;
            }

            if_expr->type = roottype(rt_if);
            return if_expr->type;
        }
    }

    if_expr->type = NULL;

    return NULL;
}



/*
 * Check type of for statement. "Init" and "update" of "for" must have type
 * integer, "cond" must have type boolean. "For" returns the type of the
 * expressionlist.
 */
typex *tc_for(parsex *for_expr, bool return_ok, bool reply_ok)
{
    typex *rt_init, *rt_cond, *rt_update, *rt;

    setlf(for_expr->lf);

    if (for_expr->left != NULL) {
        rt_init = tc_expr(for_expr->left, FALSE, FALSE, FALSE);

        if (!rt_init || strcmp(rt_init->name, "integer")) {
            error("Init of \"for\" should have type integer\n");
        }
    }

    if (for_expr->right != NULL) {
        rt_cond = tc_expr(for_expr->right, FALSE, FALSE, FALSE);

        if (!rt_cond || strcmp(rt_cond->name, "boolean")) {
            error("Condition of \"for\" should have type boolean\n");
        }
    }

    if (for_expr->e0 != NULL) {
        rt_update = tc_expr(for_expr->e0, FALSE, FALSE, FALSE);

        if (!rt_update || strcmp(rt_update->name, "integer")) {
            error("Update of \"for\" expression should have type integer\n");
        }
    }

    rt = tc_expr(for_expr->e1, TRUE, return_ok, reply_ok);

    if (rt) {
        for_expr->sym_entry = (char *)rt;
        for_expr->tsize = rt->typesize;
    }

    for_expr->type = roottype(rt);

    return (for_expr->type);
}



/*
 * Check type of while statement.
 */
typex *tc_while(parsex *while_expr, bool return_ok, bool reply_ok)
{
    typex *rt_cond;
    typex *rt = NULL;

    rt_cond = tc_expr(while_expr->left, FALSE, FALSE, FALSE);

    if (!rt_cond || strcmp(rt_cond->name, "boolean")) {
        setlf(while_expr->lf);
        error("Conditional in \"while\" should be of type boolean\n");
        return (NULL);
    }

    if (while_expr->right != NULL) {
        rt = tc_expr(while_expr->right, TRUE, return_ok, reply_ok);
    }

    if (rt) {
        while_expr->sym_entry = (char *)rt;
        while_expr->tsize = rt->typesize;
    }

    while_expr->type = roottype(rt);

    return while_expr->type;
}



/*
 * Check if types of labels match with type of switch expression, and
 * look if there is a default label.
 */
void tc_labels(parsex *labels, typex *sw_type, bool *with_default)
{
    if ((labels->tok == COMMA) && (labels->left != NULL)) {
        tc_labels(labels->left, sw_type, with_default);
    }

    if (labels->tok == DEFAULT) {
        *with_default = TRUE;
    } else if (sw_type != tc_expr(labels->right, FALSE, FALSE, FALSE)) {
        setlf(labels->right->lf);
        error("Label has wrong type\n");
    }
}



/*
 * Check types in choices of a switch. The type of the labels is checked against
 * the type of the switch expression.
 */
typex *tc_choices(parsex *choices, typex *sw_type, bool *with_default,
                  bool return_ok, bool reply_ok)
{
    typex *ct1 = 0;
    typex *ct2 = 0;

    tc_labels(choices->right->left, roottype(sw_type), with_default);

    if (choices->tok == COMMA) {
        if (choices->left == NULL) {
            ct1 = tc_expr(choices->right->right, TRUE, return_ok, reply_ok);
            choices->type = roottype(ct1);
            return (ct1);
        } else
            ct1 = tc_choices(choices->left, sw_type, with_default, return_ok,
                             reply_ok);
    }

    ct2 = tc_expr(choices->right->right, TRUE, return_ok, reply_ok);

    if (ct1 == ct2) {
        choices->type = ct2;
        return (ct2);
    }

    return (NULL);
}



/*
 * Check types in switch statement. A switch only returns a type if all choices
 * return the same type.
 */
typex *tc_switch(parsex *sw_expr, bool return_ok, bool reply_ok)
{
    bool with_default = FALSE;
    typex *sw_type, *rt;

    sw_type = tc_expr(sw_expr->left, FALSE, FALSE, FALSE);
    rt = tc_choices(sw_expr->right, sw_type, &with_default, return_ok,
                    reply_ok);

    if (with_default && rt) {
        sw_expr->sym_entry = (char *)rt;
        sw_expr->tsize = rt->typesize;
        sw_expr->type = roottype(rt);
        return (sw_expr->type);

    } else {
        return NULL;
    }
}



/*
 * Check types in an expression. If an expression if followed by a ';' the
 * type of the expression is ignored.
 */
typex *tc_expr(parsex *expr, bool break_ok, bool return_ok, bool reply_ok)
{
    typex *rt1, *rt2, *rt3;
    bool is_class_l = FALSE, is_class_r = FALSE;
    int c;

    if (expr != NULL) {
        switch (expr->tok) {
            case SEMCOL:
                myconst = FALSE;

                if (expr->left != NULL) {
                    tc_expr(expr->left, break_ok, return_ok, reply_ok);
                    myconst = FALSE;
                }

                return (expr->type =
                            tc_expr(expr->right, break_ok, return_ok, reply_ok));

            case ASG:
            case PLUSASG:
            case MINASG:
            case MULASG:
            case DIVASG:
            case MODASG:
            case ANDASG:
            case ORASG:
            case XORASG:
            case SHLEFTASG:
            case SHRIGHTASG:
                rt1 = tc_atomic(expr->left);

                if (expr->tok == ASG &&
                    expr->right->tok == CFUNCTION_CALL &&
                    hasvarpar(((mfpx *) expr->right->sym_entry)->flags) &&
                    (typex *)((mfpx *) expr->right->sym_entry)->realreturn ==
                    NULL) {
                    ((mfpx *) expr->right->sym_entry)->realreturn = (char *)rt1;
                }

                rt2 = tc_expr(expr->right, FALSE, FALSE, FALSE);
                is_class_l = isclasstype(rt1);
                is_class_r = isclasstype(rt1);

                if (is_class_l && is_class_r) {
                    if (rt1 == rt2 ||
                        is_subtypeof(((classx *) rt1)->name, ((classx *) rt2)->name)) {
                        expr->tsize = "1";
                        expr->sym_entry = (char *)rt1;
                        expr->type = rt1;
                        return (rt1);
                    }

                    setlf(expr->lf);
                    error("Incompatible assignment of classes\n");
                    return NULL;
                } else if (is_class_l || is_class_r) {
                    setlf(expr->lf);
                    error("Incompatible assignment between class and non class\n");
                    return NULL;
                } else {
                    setlf(expr->lf);
                    rt1 = tc_asgop(roottype(rt1), roottype(rt2), expr);
                    expr->tsize = rt1->typesize;
                    expr->sym_entry = (char *)rt1;
                    expr->type = rt1;
                    return rt1;
                }

            case MUL:
            case DIV:
            case MOD:
            case PLUS: // left and right operand must have compatible type */
            case MIN:
            case AND:
            case OR:
            case XOR:
            case SHLEFT:
            case SHRIGHT:
                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);
                c = myconst;
                rt2 = tc_expr(expr->right, FALSE, FALSE, FALSE);
                setlf(expr->lf);
                rt1 = tc_arithmeticop(roottype(rt1), roottype(rt2), expr);
                expr->sym_entry = (char *)rt1;
                expr->tsize = rt1->typesize;
                myconst = myconst && c;
                expr->type = rt1;
                return rt1;

            case ANDAND:
            case OROR: // left and right operand must have same boolean compatible type
            case XORXOR:
                expr->tsize = "1";
                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);

                if (myconst) {
                    myconst = FALSE;
                    rt2 = tc_expr(expr->right, FALSE, FALSE, FALSE);

                    if ((rt1 == roottype(rt2)) && !strcmp(rt1->name, "boolean")) {
                        expr->sym_entry = (char *)rt1;
                        expr->type = rt1;
                        return (rt1);
                    }
                } else {
                    rt2 = tc_expr(expr->right, FALSE, FALSE, FALSE);

                    if (myconst) {
                        myconst = FALSE;

                        if ((rt2 == roottype(rt1)) && !strcmp(rt2->name, "boolean")) {
                            expr->sym_entry = (char *)rt2;
                            expr->type = rt2;
                            return (rt2);
                        }
                    } else if (rt1 && (rt1 == rt2)) {
                        rt3 = roottype(rt1);

                        if (!strcmp(rt3->name, "boolean")) {
                            expr->sym_entry = (char *)rt3;
                            expr->type = rt3;
                            return (rt3);
                        }
                    }
                }

                setlf(expr->lf);
                myconst = FALSE;
                error("Operands of \"%s\" must have of boolean compatible type\n",
                      g_tokstr(expr->tok));
                return (NULL);

            case LT:
            case LE: // left and right operand must have type integer
            case GT:
            case GE:

            case EQ:
            case NE: // left and right operand must have type integer
                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);
                c = myconst;
                rt2 = tc_expr(expr->right, FALSE, FALSE, FALSE);
                setlf(expr->lf);
                rt1 = tc_compareop(roottype(rt1), roottype(rt2), expr);
                expr->sym_entry = (char *)rt1;
                expr->tsize = rt1->typesize;
                myconst = myconst && c;
                expr->type = rt1;
                return rt1;

            case IDENT:
            case STRUCT:
            case ARRAY:
                return (tc_atomic(expr));

            case SEND:
            case SENDSEND:
                rt1 = tc_send(expr);

                if (rt1 != NULL) {
                    expr->str = rt1->name;
                } else {
                    expr->str = "void";
                }

                return rt1;

            case FUNCTION_CALL:
            case CFUNCTION_CALL:
                return (tc_func_call(expr));

            case BLOCK:
                return (tc_block(expr));

            case BLOCKT:
                return (tc_blockt(expr));

            case IF:
                return (tc_if(expr, break_ok, return_ok, reply_ok));

            case FOR:
                return (tc_for(expr, return_ok, reply_ok));

            case WHILE:
                return (tc_while(expr, return_ok, reply_ok));

            case SWITCH:
                return (tc_switch(expr, return_ok, reply_ok));

            case BREAK:
                if (!break_ok) {
                    setlf(expr->lf);
                    error("No \"break\" allowed here\n");
                    return (NULL);
                }

                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);

                if (rt1) {
                    expr->tsize = rt1->typesize;
                }

                return (expr->type = rt1);

            case RETURN:
                setlf(expr->lf);

                if (!return_ok) {
                    error("No \"return\" allowed here\n");
                    return (NULL);
                }

                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);
                rt1 = roottype(rt1);

                if (!tc_compatibel(rettype, rt1, &expr->left)) {
                    error("Return expression has wrong type\n");
                    return (NULL);
                }

                if (rt1) {
                    expr->tsize = rt1->typesize;
                }

                return (expr->type = rt1);

            case REPLY:
                setlf(expr->lf);

                if (!reply_ok) {
                    error("No \"reply\" allowed here\n");
                    return (NULL);
                }

                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);

                if (!(reptype == typevoid && rt1 == NULL) &&
                    !tc_compatibel(reptype, rt1, &expr->left)) {
                    error("Reply expression has wrong type\n");
                    return (NULL);
                }

                if (rt1 != NULL) {
                    rt1 = expr->left->type;
                    expr->tsize = rt1->typesize;
                    //findsourcetype(rt1->name);
                    findsourcetype(reptype->name);
                    expr->str = rt1->name;
                } else {
                    findsourcetype(TYPEVOID);
                    expr->str = "void";
                }

                return (expr->type = roottype(rt1));

            case BOOLNOT:
                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);

                if (rt1 != typeboolean) {
                    setlf(expr->left->lf);
                    error("Operand must be of type boolean\n");
                    return (NULL);
                }

                expr->tsize = "1";
                return (expr->type = rt1);

            case BITNOT:
                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);

                if (rt1 != typeinteger && !isint_(rt1)) {
                    setlf(expr->left->lf);
                    error("Operand must be of type integer or int_#\n");
                    return (NULL);
                }

                expr->tsize = rt1->typesize;
                return (expr->type = rt1);

            case UMIN:
                rt1 = tc_expr(expr->left, FALSE, FALSE, FALSE);

                if (rt1 != typeinteger && !isint_(rt1) && rt1 != typefloat) {
                    setlf(expr->left->lf);
                    error("Operand must be of type integer, int_# or float\n");
                    return (NULL);
                }

                expr->tsize = rt1->typesize;
                return (expr->type = rt1);

            case FLTCONST:
                myconst = TRUE;
                expr->tsize = "1";
                return (expr->type = typefloat);

            case INTCONST:
                myconst = TRUE;
                expr->tsize = "1"; // Variable !
                return (expr->type = typeinteger);

            case BOOLCONST:
                myconst = TRUE;
                expr->tsize = "1";
                return (expr->type = typeboolean);

            case STRCONST:
                myconst = TRUE;
                expr->tsize = "1";
                return (expr->type = typestring);

            case ENUM_ELEM:
                expr->tsize = "1";
                return ((typex *) expr->type);

            case SOURCE:
                expr->tsize = "1";

                if (!reptype) {
                    panic("keyword 'source' not allowed here, method has no reply type!");
                }

                expr->type = findsourcetype(reptype->name);
                return (expr->type);

            case THIS:
                expr->tsize = "1";

                if (!class) {
                    panic("keyword 'this' not allowed outside class!");
                }

                expr->type = (typex *)class;
                return (expr->type);

            default:
                setlf(expr->lf);
                panic("What the hell is this kind of expression\n");
        }
    }

    return (NULL);
}



/*
 * Check if inits of variables have the same type as the declared type
 */
void tc_var_inits(symboltable *variables)
{
    if (!variables) {
        return;
    }

    for (unsigned int i = 0; i < symbol_getfill(variables); i++) {
        varx *var = (varx *)symbol_getcont(variables, i);

        if (var->init != NULL) {
            typex *rt = tc_expr(var->init, FALSE, FALSE, FALSE);

            if (!tc_compatibel((typex *) var->realtype, rt, &var->init)) {
                setlf(var->lf);
                error("Initialisation of variable \"%s\" has wrong type\n",
                      var->name);
            }
        }
    }
}



/*
 * Type check inits of local variables and typecheck the body of a function or
 * method.
 */
void tc_func_meth(symboltable *func_meths)
{
    mfpx *function;

    for (unsigned int i = 0; i < symbol_getfill(func_meths); i++) {
        function = (mfpx *)symbol_getcont(func_meths, i);
        tc_var_inits(function->locals);
        reptype = (typex *) function->realreply;
        rettype = (typex *) function->realreturn;
        tc_expr(function->code, FALSE, TRUE, ismethod(function->flags));
        reptype = NULL;
        rettype = NULL;
    }
}



/*
 * Type check variable inits and code
 */
void type_check(symboltable *classes, symboltable *functions,
                symboltable *variables)
{
    max_meth = 0;

    tc_var_inits(variables);
    tc_func_meth(functions);

    if (symbol_getfill(classes) > 0) {
        class = (classx *)symbol_getcont(classes, 0);
        tc_var_inits(class->variables);
        tc_var_inits(class->localvar);
        tc_func_meth(class->func_meths);
        tc_expr(class->code, FALSE, FALSE, FALSE);
    }
}
