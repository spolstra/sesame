/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file take care of the parsing of the declarations
 * in an implementation file, and the creation of the symboltables.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ptree.h"
#include "symboldef.h"
#include "parse.h"
#include "util.h"
#include "symbol.h"
#include "pdecl.h"


extern symboltable *basic_types;

/*
 * Parse all declarations and add found declarations to the appropriate
 * symboltables.
 */
void pdecl(parsex *decl_tree, symboltable *types, symboltable *variables,
           symboltable *functions, int scope, int varonly, int no_meth,
           symboltable *altvars)
{
    char *name, *type = NULL;
    parsex *current, *init, *listptr;
    bool ismethod = FALSE;

    current = decl_tree;

    switch (current->tok) {
        case PARAM:
            varonly = TRUE;

        case COMMA:
        case COMCA:
            if (current->left != NULL)
                pdecl(current->left, types, variables, functions, scope, varonly,
                      no_meth, altvars);

            if (current->tok == COMCA) {
                current->tok = COMMA;
                variables = altvars;
            }

            pdecl(current->right, types, variables, functions, scope, varonly,
                  no_meth, altvars);
            break;

        case TYPE_DECL:
            if (varonly) {
                setlf(current->left->lf);
                error("No type declarations allowed here!\n");
                break;
            }

            ptype_decl(types, current, scope);
            break;

        case VAR_DECL: // add one or more variables to variable symboltable
            type = current->right->str;
            init = current->e0;
            listptr = current->left;

            while (listptr != NULL) {
                name = listptr->left->str;
                setlf(listptr->left->lf);
                addvariable(variables, name, type, scope, init, listptr->left->lf);
                listptr = listptr->right;
            }

            break;

        case METHOD_DECL:
            if (no_meth) { // outside class no methods allowed
                setlf(current->left->lf);
                error("No method declarations allowed here !\n");
                break;
            }

            ismethod = TRUE;

        case FUNC_DECL:
            if (varonly) {
                setlf(current->left->lf);
                error("No function or method declarations allowed here!\n");
                break;
            }

            pfunc_decl(functions, current, ismethod, scope);
            break;
    }
}



/*
 * Parse type declaration and add found type to symboltable of types.
 */
void ptype_decl(symboltable *t, parsex *type_decl, int scope)
{
    parsex *current;
    symboltable *fields = 0;
    char *basetype = NULL;
    int type = 0;

    current = type_decl->right;

    switch (current->tok) {
        case STRUCT: // structure type
            type = TSTRUCT;
            fields = newsymboltable(sizeof(varx));
            {
                parsex *p, q, qq;
                char buf[32];
                static int an = 0;

                for (p = current->left; p != NULL; p = p->left) {
                    if (p->right->right->tok != IDENT) {
                        sprintf(buf, "_Anonymous_%d", an++);
                        q.tok = TYPE_DECL;
                        q.lf = p->right->lf;
                        q.right = p->right->right;
                        q.left = &qq;
                        qq.str = strdup(buf);
                        qq.lf = p->right->lf;
                        ptype_decl(t, &q, scope);
                        p->right->right->tok = IDENT;
                        p->right->right->str = qq.str;
                    }
                }
            }
            pdecl(current->left, NULL, fields, NULL, scope, TRUE, TRUE, fields);
            break;

        case ENUM: //  enumerated type
            type = TENUM;
            fields = newsymboltable(sizeof(varx));
            current = current->left;

            while (current != NULL) {
                setlf(current->left->lf);
                addvariable(fields, current->left->str, NULL, scope, NULL,
                            current->left->lf);
                current = current->right;
            }

            break;

        case IDENT:
            type = TTYPE;
            basetype = type_decl->right->str;
            break;

        case ARRAY: { //  array type
            parsex q, qq;
            char buf[32];
            static int an = 0;

            if (current->left->tok != IDENT) {
                sprintf(buf, "_Anonymous2_%d", an++);
                q.tok = TYPE_DECL;
                q.lf = current->lf;
                q.right = current->left;
                q.left = &qq;
                qq.str = strdup(buf);
                qq.lf = current->lf;
                ptype_decl(t, &q, scope);
                current->left->tok = IDENT;
                current->left->str = qq.str;
            }
        }

        type = TARRAY;
        fields = newsymboltable(sizeof(typex));
        {
            typex *nt;

            nt = (typex *)malloc(sizeof(typex));
            nt->name = strdup("ttype");
            nt->type = TTYPE;
            nt->basetype = current->left->str;
            addsymbol(fields, (char *)nt);
            nt = (typex *)malloc(sizeof(typex));
            nt->name = strdup("0");
            nt->type = TARRAY;
            nt->range = current->right;
            addsymbol(fields, (char *)nt);
        }

        break;

        default:
            setlf(type_decl->left->lf);
            error("Unknown type definition for %s!\n", type_decl->left->str);
    }

    setlf(type_decl->left->lf);
    addtype(t, type_decl->left->str, type, basetype, fields, scope,
            type_decl->left->lf);
}



/*
 * Parse multidimensional array type
 */
void parray_type(parsex *array_ptr, symboltable *t, int deg)
{
    parsex *ptr;
    typex *nt;

    ptr = array_ptr;

    if (ptr->tok == ARRAY) { //  parse other dimensions first
        parray_type(ptr->left, t, deg + 1);
    }

    nt = (typex *)malloc(sizeof(typex));

    switch (ptr->tok) {
        case ARRAY:
            nt->name = calloc((unsigned)5, sizeof(char));
            sprintf(nt->name, "%d", deg); //  dummy name
            nt->type = TARRAY;
            nt->range = ptr->right;
            break;

        case STRUCT:

            //  WALK !!!
        case IDENT:
            nt->name = strdup("ttype");
            nt->type = TTYPE;
            nt->basetype = ptr->str;
            break;

        case ENUM:
            nt->name = strdup("tenum");
            nt->type = TENUM;
            nt->fields = newsymboltable(sizeof(typex));
            ptr = ptr->left;

            while (ptr != NULL) {
                setlf(ptr->left->lf);
                addvariable(nt->fields, ptr->left->str, NULL, 0, NULL,
                            ptr->left->lf);
                ptr = ptr->right;
            }

            break;
    }

    addsymbol(t, (char *)nt);
}



/*
 * Parse function/method declaration and add result to symboltable of
 * functions/methods.
 */
void pfunc_decl(symboltable *t, parsex *mf, bool ismethod, int scope)
{
    symboltable *parameters = newsymboltable(sizeof(varx));
    symboltable *locals = newsymboltable(sizeof(varx));
    char *rettype = NULL;
    char *reptype = NULL;

    if (mf->e0->left != NULL) { //  parse local variables
        pdecl(mf->e0->left, NULL, locals, NULL, LOCALSYM, TRUE, TRUE, locals);
    }

    if (mf->right != NULL) //  parse formal parameters
        pdecl(mf->right, NULL, parameters, NULL, PARAMETER, TRUE, TRUE,
              parameters);

    if (mf->e1 != NULL) { //  find return type
        rettype = mf->e1->str;
    }

    if (mf->e2 != NULL) { //  find reply type (method  only)
        reptype = mf->e2->str;
    }

    setlf(mf->left->lf);
    addfunction(t, mf->left->str, rettype, reptype, parameters, locals,
                mf->e0->right, ismethod, scope, mf->left->lf);
}



/*
 * Parse class declaration
 */
void pclass(symboltable *t, parsex *class, int scope)
{
    symboltable *types = newsymboltable(sizeof(typex));
    symboltable *variables = newsymboltable(sizeof(varx));
    symboltable *func_meths = newsymboltable(sizeof(mfpx));
    symboltable *subtypeof = newsymboltable(sizeof(namex));
    symboltable *localvars = newsymboltable(sizeof(varx));

    if (class->right != NULL) //  parse declarations inside the class
        pdecl(class->right, types, variables, func_meths, CLASSSYM, FALSE,
              FALSE, localvars);

    setlf(class->left->lf);
    addclass(t, class->left->str, class->e0, types, variables, func_meths,
             subtypeof, scope, class->left->lf, localvars);
}
