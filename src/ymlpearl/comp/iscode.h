/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * Various operations for the indentation functions.
 */

#ifndef ISCODE_H
#define ISCODE_H

#define INC 1
#define DEC 2
#define DO  3
#define RESET 4

#include <stdio.h>

void openfile(FILE **fp, char *name, char *ext);
void popenfiles(char *filename, int both);
void closefile(FILE **fp);
void closefiles(char *filename);
void icode(char *pformat, ...);
void ilabel(const char *label, ...);

#endif        // ISCODE_H
