/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *           by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file are used to calculate the size of the various
 * types.
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "symboldef.h"
#include "standardtypes.h"
#include "parse.h"
#include "util.h"
#include "iscode.h"
#include "gfinit.h"
#include "gfunc.h"
#include "gsimple.h"
#include "type_size.h"

#define SIZEOFINT "1"


extern specx *specs;
extern symboltable *basic_types;
extern symboltable *types;
extern symboltable *classes;

typedef struct initlistx {
    struct initlistx *next;
    char *initfmt;
    char *inits0;
    char *inits1;
    char *name;
} initlistx;

initlistx *inithead, *inittail;



char *sizevariable(char *fmt, char *s0, char *s1)
{
    static int varnum = 0;

    char *x = malloc(16);
    sprintf(x, "env->O%d", varnum++);

    initlistx *tmp = (initlistx *)malloc(sizeof(initlistx));

    if (inithead == NULL) {
        inithead = inittail = tmp;
    } else {
        inittail->next = tmp;
        inittail = tmp;
    }

    tmp->next = NULL;
    tmp->name = x;
    tmp->initfmt = fmt;
    tmp->inits0 = strdup(s0);

    //if (fmt[0] == 'V') tmp->inits1 = copystring(s1);
    //else
    tmp->inits1 = s1;

    return x;
}

void sizedumpinitdecl(void)
{
    for (initlistx *tmp = inithead; tmp != NULL; tmp = tmp->next) {
        icode("int %s;\n", tmp->name + 5);
    }
}

void sizedumpinit(classx *cl)
{
    unsigned int i;
    varx *vp;
    parsex *fc;

    varinitstart(cl);

    for (initlistx *tmp = inithead; tmp != NULL; tmp = tmp->next) {
        switch (tmp->initfmt[0]) {
            case 'V':
                vp = (varx *)tmp->inits1;

                switch (vp->scope) {
                    case CLASSSYM:
                        for (i = 0; i < symbol_getfill(cl->variables); i++) {
                            if ((varx *)symbol_getcont(cl->variables, i) == vp) {
                                break;
                            }
                        }

                        icode("if (msg->n > %d)\n", i);
                        icode("    %s = msg->data[msgx[%d]];\n", tmp->name, i);

                        if (vp->init) {
                            error("default initialization of size variable not supported");
                        } else {
                            icode("else  panic(\"missing parameter %d\");\n", i);
                        }

                        break;

                    case GLOBALSYM:
                        icode("%s = _%s;\n", tmp->name, vp->name);
                        break;

                    case INVISIBLESYM:
                        icode("%s = 1; // Never used\n", tmp->name);
                        break;

                    case LOCALSYM:
                    case PARAMETER:
                        error("local \"%s\" may not be used here", vp->name);
                        break;

                    default:
                        panic("variable \"%s\" has scope %d.", vp->name, vp->scope);
                        break;
                }

                break;

            case 'F':
                fc = (parsex *)tmp->inits1;
                icode("%s = ", tmp->name);
                gcfunc(fc);
                icode(";\n");
                break;

            default:
                icode("%s = %s %c %s;\n", tmp->name, tmp->inits0, tmp->initfmt[1],
                      tmp->inits1);
        }
    }

    icode("\n");

    icode("env = realloc(env, sizeof(intptr_t) * ((%s) + 1) + sizeof(*env));\n",
          cl->varsize);
    icode("ENV = (intptr_t *)(env + 1);\n");
    icode("memset(ENV, 0, sizeof(intptr_t) * ((%s) + 1));\n\n", cl->varsize);

    for (i = 0; i < symbol_getfill(cl->variables); i++) {
        vp = (varx *)symbol_getcont(cl->variables, i);
        dovarinit(cl, vp, i);
    }

    icode("\n");

    varinitend(cl);
    freedumpinit();
}

void freedumpinit(void)
{
    initlistx *tmp;

    tmp = inithead;

    while (tmp != NULL) {
        inithead = tmp->next;
        free(tmp->name);
        free(tmp->inits0);

        //if (tmp->initfmt[0] == 'V')
        //  free(tmp->inits1);

        free(tmp);
        tmp = inithead;
    }

    inittail = NULL;
}


char *sizecfunc(parsex *fc)
{
    mfpx *fp;
    typex *rtyp;

    fp = (mfpx *)fc->sym_entry;
    rtyp = (typex *)fp->realreturn;

    if (rtyp != typeinteger) {
        error("Array-bound: Function call should return integer\n");
    }

    return sizevariable("F__", fc->left->str, (char *)fc);
}

char *sizeidentifier(varx *vp)
{
    if ((typex *)vp->realtype != typeinteger) {
        error("Array-bound: Type of %s should be integer\n", vp->name);
    }

    return sizevariable("V__", vp->name, (char *)vp);
}

char *sizestring(const int value)
{
    char *x;

    x = malloc(10);
    sprintf(x, "%d", value);

    return x;
}

int sizesimple(char *string)
{
    if (string[0] == '-' || isdigit((int)string[0])) {
        return 1;
    }

    return 0;
}

char *sizeeval(char *fmt, char *val1, char *val2)
{
    char *result;
    int res = 0;

    if (sizesimple(val1) && sizesimple(val2)) {
        switch (fmt[1]) {
            case '+':
                res = atoi(val1) + atoi(val2);
                break;

            case '-':
                res = atoi(val1) - atoi(val2);
                break;

            case '*':
                res = atoi(val1) * atoi(val2);
                break;

            case '/':
                res = atoi(val1) / atoi(val2);
                break;

            case '%':
                res = atoi(val1) % atoi(val2);
                break;

            default:
                panic("Illegal op in sizeeval : %s\n", fmt);
        }

        result = sizestring(res);

    } else {

        // Simplify identity expressions
        switch (fmt[1]) {
            case '+':
                if (sizesimple(val1) && atoi(val1) == 0) {
                    return val2;
                }

                if (sizesimple(val2) && atoi(val2) == 0) {
                    return val1;
                }

                break;

            case '-':
                if (sizesimple(val2) && atoi(val2) == 0) {
                    return val1;
                }

                break;

            case '*':
                if (sizesimple(val1) && atoi(val1) == 1) {
                    return val2;
                }

                if (sizesimple(val2) && atoi(val2) == 1) {
                    return val1;
                }

                break;

            case '/':
                if (sizesimple(val2) && atoi(val2) == 1) {
                    return val1;
                }

                break;

            case '%':
                break;

            default:
                panic("Illegal op in sizeeval : %s\n", fmt);
        }

        result = sizevariable(fmt, val1, val2);
    }

    return result;
}


/*
 * Evaluate the expression that specifies the array-range. Only '+', '*', '/',
 * '-' (unary and binary) and '%' may occur in the expression.
 */
char *calc_range(parsex *range)
{
    if (!range) {
        return sizestring(1);
    }

    switch (range->tok) {
        case IDENT:
            return sizeidentifier((varx *)range->sym_entry);

        case CFUNCTION_CALL:
            return sizecfunc(range);

        case INTCONST:
            return sizestring(atox(range->str));

        case PLUS:
            return sizeeval("f+f", calc_range(range->left),
                            calc_range(range->right));

        case MIN:
            return sizeeval("f-f", calc_range(range->left),
                            calc_range(range->right));

        case MUL:
            return sizeeval("f*f", calc_range(range->left),
                            calc_range(range->right));

        case DIV:
            return sizeeval("f/f", calc_range(range->left),
                            calc_range(range->right));

        case MOD:
            return sizeeval("f%f", calc_range(range->left),
                            calc_range(range->right));

        case UMIN:
            return sizeeval("s-f", "0", calc_range(range->left));

        default:
            error("Illegal expression(%d) in range of array.\n", range->tok);
            return sizestring(0);
    }
}



/*
 * Determine the size of the types in 'tp_list'.
 */
void calc_type_size(symboltable *tp_list)
{
    typex *tp, *vt, *dim_j;
    varx *vp;
    unsigned int b, i, j;
    char *place;
    symboltable *dimensions;

    for (i = 0; i < symbol_getfill(tp_list); i++) {
        tp = (typex *)symbol_getcont(tp_list, i);
        setlf(tp->lf);

        switch (tp->type) {
            case TBASIC:
                if (!strcmp(tp->name, TYPEINTEGER)) {
                    tp->typesize = SIZEOFINT;

                } else if (!strcmp(tp->name, TYPEBOOLEAN)) {
                    tp->typesize = SIZEOFINT;

                } else if (!strcmp(tp->name, TYPESTRING)) {
                    tp->typesize = SIZEOFINT;

                } else if (!strcmp(tp->name, TYPEFLOAT)) {
                    tp->typesize = SIZEOFINT;

                } else if (!strcmp(tp->name, TYPEVOID)) {
                    tp->typesize = "0";

                } else if (!strncmp(tp->name, TYPEINT_, 4)) {
                    b = atoi(tp->name + 4);
                    tp->typesize = sizestring(((b - 1) / 32) + 1);

                } else {
                    panic("Illegal basic type found\n");
                }

                break;

            case TARRAY:
                dim_j = (typex *)symbol_getcont(tp->fields, 0);

                if (isclasstype((typex *)dim_j->realtype)) {
                    tp->typesize = "1";
                } else {
                    tp->typesize = ((typex *)dim_j->realtype)->typesize;
                }

                dimensions = tp->fields;

                for (j = 1; j < symbol_getfill(dimensions); j++) {
                    dim_j = (typex *)symbol_getcont(dimensions, j);
                    dim_j->typesize = calc_range(dim_j->range);
                    tp->typesize = sizeeval("s*s", tp->typesize, dim_j->typesize);
                }

                break;

            case TSTRUCT:
                place = "0";

                for (j = 0; j < symbol_getfill(tp->fields); j++) {
                    vp = (varx *)symbol_getcont(tp->fields, j);
                    vt = (typex *)vp->realtype;
                    vp->offset = place;

                    if (isclasstype(vt)) { // class references always have size 1
                        place = sizeeval("s+s", place, "1");
                    } else {
                        place = sizeeval("s+s", place, vt->typesize);
                    }
                }

                tp->typesize = place;
                break;

            case TPOINTER:
                tp->typesize = SIZEOFINT;
                break;

            case TENUM:
                panic("No ENUMs anymore\n");
                // tp->typesize = tp->fields->fill * SIZEOFINT;
                break;

            case TTYPE:
                tp->typesize = ((typex *)tp->realtype)->typesize;
                break;

            default:
                error("Illegal kind of type\n");
        }
    }
}



/*
 * Determine size of the basic types, the global types and the local types of a
 * class.
 */
void calc_type_sizes(void)
{
    calc_type_size(basic_types);
    calc_type_size(types);
    calc_type_size(specs->types);

    for (unsigned int i = 0; i < symbol_getfill(specs->classes); i++) {
        classx *c = (classx *)symbol_getcont(specs->classes, i);
        calc_type_size(c->types);
    }

    for (unsigned int i = 0; i < symbol_getfill(classes); i++) {
        classx *c = (classx *)symbol_getcont(classes, i);
        calc_type_size(c->types);
    }
}




char *var_place(varx *varp, char *place)
{
    varp->offset = place;

    if (isclasstype((typex *)varp->realtype)) {
        place = sizeeval("s+s", place, "1");
    } else {
        place = sizeeval("s+s", place, ((typex *)varp->realtype)->typesize);
    }

    return place;
}


char *calc_var_place(symboltable *vt, char *place)
{
    if (!vt) {
        return (NULL);
    }

    for (unsigned int i = 0; i < symbol_getfill(vt); i++) {
        varx *varp = (varx *)symbol_getcont(vt, i);

        if (((typex *)varp->realtype)->type != TARRAY) {
            place = var_place(varp, place);
        }
    }

    for (unsigned int i = 0; i < symbol_getfill(vt); i++) {
        varx *varp = (varx *)symbol_getcont(vt, i);

        if (((typex *)varp->realtype)->type == TARRAY) {
            place = var_place(varp, place);
        }
    }

    return place;
}

static void adj_var_place(symboltable *vt, char *offset)
{
    if (!vt) {
        return;
    }

    for (unsigned int i = 0; i < symbol_getfill(vt); i++) {
        varx *varp = (varx *)symbol_getcont(vt, i);
        varp->offset = sizeeval((char *)"s+f", offset, varp->offset);
    }
}


void fm_var_place(symboltable *fmt)
{
    for (unsigned int i = 0; i < symbol_getfill(fmt); i++) {
        mfpx *fmp = (mfpx *)symbol_getcont(fmt, i);
        fmp->paramsize = calc_var_place(fmp->parameters, sizestring(0));
        adj_var_place(fmp->parameters, sizeeval("s-s", "-3", fmp->paramsize));
        fmp->localsize = calc_var_place(fmp->locals, "0");
    }
}


void class_var_place(symboltable *ct)
{
    for (unsigned int i = 0; i < symbol_getfill(ct); i++) {
        classx *cp = (classx *)symbol_getcont(ct, i);
        cp->cvarsize = calc_var_place(cp->variables, (char *)"0");
        cp->varsize = calc_var_place(cp->localvar, cp->cvarsize);
        fm_var_place(cp->func_meths);
    }
}

