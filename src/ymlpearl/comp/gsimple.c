/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *           by
 *      B.J. Overeinder & J.G. Stil
 *
 * Code generation for simple expressions.
 */



#include <string.h>

#include "gsimple.h"
#include "ptree.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "parse.h"
#include "gatomic.h"
#include "g_tokstr.h"
#include "gfunc.h"
#include "iscode.h"
#include "gsend.h"


extern typex *roottype();
extern symboltable *basic_types;
extern char *class_name;


/*
 * Return the type of the expression.
 */
typex *mytypeof(parsex *expr)
{
    if (expr == NULL) {
        return (NULL);
    }

    switch (expr->tok) {
        case SEMCOL:
            return (mytypeof(expr->right));

        case INTCONST:
            return (typeinteger);

        case FLTCONST:
            return (typefloat);

        case BOOLCONST:
            return (typeboolean);

        case STRCONST:
            return (typestring);

        case IDENT:
            return ((typex *)((varx *) expr->sym_entry)->realtype);

        case STRUCT:
            return (mytypeof(expr->left));

        case ARRAY:
            return ((typex *) expr->sym_entry);

        case SEND:
        case SENDSEND:
            return ((typex *)((mfpx *) expr->right->sym_entry)->realreply);

        case FUNCTION_CALL:
        case CFUNCTION_CALL:
            return ((typex *)((mfpx *) expr->sym_entry)->realreturn);

        case BLOCKT:
            return (typeinteger);

        case MUL:
        case DIV:
        case MOD:
        case PLUS:
        case MIN:
        case AND:
        case OR:
        case XOR:
        case SHLEFT:
        case SHRIGHT:
        case LT:
        case LE:
        case EQ:
        case NE:
        case GE:
        case GT:
        case ANDAND:
        case OROR:
        case XORXOR:
        case ASG:
        case MULASG:
        case DIVASG:
        case MODASG:
        case PLUSASG:
        case MINASG:
        case ANDASG:
        case ORASG:
        case XORASG:
        case SHLEFTASG:
        case SHRIGHTASG:
        case BLOCK:
        case IF:
        case FOR:
        case WHILE:
        case SWITCH:
            return ((typex *) expr->sym_entry);

        case UMIN:
        case BOOLNOT:
        case BITNOT:
        case RETURN:
        case REPLY:
            return (mytypeof(expr->left));

        default:
            return (NULL);
    }
}


static bool issimple_type(typex *tp)
{
    if (tp == NULL) {
        return TRUE;
    }

    if (isclasstype(tp)) {
        return TRUE;
    }

    while (tp->realtype != NULL) {
        tp = (typex *)tp->realtype;
    }

    if (tp == typeboolean || tp == typeinteger ||
        tp == typestring || tp->type == TENUM ||
        !strncmp(tp->name, "has_reply_", 10)) {
        return TRUE;
    } else {
        return FALSE;
    }
}



/*
 * Check whether or not a parse-subtree has a simple type,
 * i.e. boolean, enumerated-type, integer or string.
 */
static bool issimply_typed(parsex *p)
{
    typex *tp;
    varx *vp;

    if (p->tok == ARRAY) {
        tp = (typex *) p->sym_entry;
    } else {
        vp = (varx *) p->sym_entry;

        if (isclasstype((typex *)vp->realtype)) {
            return TRUE;
        }

        tp = (typex *) vp->realtype;
    }

    if (tp->type == TCLASS) {
        return TRUE;
    }

    return issimple_type(tp);
}

/*
 * Check whether or not a parse-subtree a simple expression is.
 */
bool issimple(parsex *c)
{
    /* issimple */
    parsex *cp;

    if (c->simple != -1) {
        return (c->simple);
    }

    c->type = roottype(c->type);

    switch (c->tok) {
        case FLTCONST:
        case BOOLCONST:
        case STRCONST:
        case ENUM_ELEM:
        case SOURCE:
        case THIS:
            return (c->simple = TRUE);

        case INTCONST:
            if (strcmp(c->tsize, "1") == 0) {
                return (c->simple = TRUE);
            } else {
                return (c->simple = FALSE);
            }

        case IDENT:
            if (issimply_typed(c)) {
                return (c->simple = TRUE);
            } else {
                return (c->simple = FALSE);
            }

        case ARRAY:
            if (/*issimply_typed(c) && */ issimple(c->right) && issimple(c->left)) {
                return (c->simple = TRUE);
            } else {
                return (c->simple = FALSE);
            }

        case STRUCT:
            if (issimply_typed(c->right)) {
                return (c->simple = TRUE);
            } else {
                return (c->simple = FALSE);
            }

        case MUL:
        case DIV:
        case MOD:
        case PLUS:
        case MIN:
        case SHLEFT:
        case SHRIGHT:
        case AND:
        case OR:
        case XOR:
        case LT:
        case LE:
        case EQ:
        case NE:
        case GE:
        case GT:
        case ANDAND:
        case OROR:
        case XORXOR:
            return (c->simple = issimple(c->left) && issimple(c->right));

        case UMIN:
        case BOOLNOT:
        case BITNOT:
            return (c->simple = issimple(c->left));

        case ASG:
        case PLUSASG:
        case MINASG:
        case MULASG:
        case DIVASG:
        case MODASG:
        case SHLEFTASG:
        case SHRIGHTASG:
        case ANDASG:
        case ORASG:
        case XORASG:
            return (c->simple = issimple(c->left) && issimple(c->right));

        case IF:
            if (c->e0 != NULL && c->right != NULL)
                return (c->simple = issimple(c->left) &&
                                    issimple(c->right) && issimple(c->e0));
            else {
                return (c->simple = FALSE);
            }

        case CFUNCTION_CALL:
            if (issimple_type((typex *)((mfpx *) c->sym_entry)->realreturn)) {
                c->simple = TRUE;
            } else {
                c->simple = FALSE;
            }

            for (cp = c->right; cp; cp = cp->left)
                if (!issimple(cp->right)) {
                    c->simple = FALSE;
                }

            return (c->simple);

        default:
            return (c->simple = FALSE);
    }
}       /* issimple */



/*
 * Generate the code for a this statement.
 */
void gthis(void)
{
    icode("env->jobnr");
}


/*
 * Generate a simple expression.
 */
void gsimple(parsex *p)
{
    p->type = roottype(p->type);
    icode("(");

    switch (p->tok) {
        case BOOLCONST:
        case FLTCONST:
        case INTCONST:
        case STRCONST:
        case ENUM_ELEM:
        case IDENT:
        case ARRAY:
        case STRUCT:
            gatomic(p);
            break;

        case PLUS:
        case MIN:
        case MUL:
        case DIV:
        case MOD:
        case SHLEFT:
        case SHRIGHT:
        case AND:
        case OR:
        case XOR:
        case LT:
        case LE:
        case EQ:
        case NE:
        case GE:
        case GT:
        case ANDAND:
        case OROR:
        case XORXOR:
            if (!strcmp((mytypeof(p->left))->name, "string")) {
                icode("strcmp(");
                gsimple(p->left);
                icode(",");
                gsimple(p->right);
                icode(") %s 0", g_tokstr(p->tok));
            } else {
                gsimple(p->left);
                icode(" %s ", g_tokstr(p->tok));
                gsimple(p->right);
            }

            break;

        case UMIN:
        case BOOLNOT:
        case BITNOT:
            icode("%s", g_tokstr(p->tok));
            gsimple(p->left);
            break;

        case ASG:
        case PLUSASG:
        case MINASG:
        case MULASG:
        case DIVASG:
        case MODASG:
        case ANDASG:
        case ORASG:
        case XORASG:
        case SHLEFTASG:
        case SHRIGHTASG:
            gatomic(p->left);
            icode(" %s ", g_tokstr(p->tok));
            gsimple(p->right);
            break;

        case IF:
            gsimple(p->left);
            icode(" ? ");
            gsimple(p->right);
            icode(" : ");
            gsimple(p->e0);
            break;

        case CFUNCTION_CALL:
            gcfunc(p);
            break;

        case SOURCE:
            gsource();
            break;

        case THIS:
            gthis();
            break;

        default:
            setlf(p->lf);
            panic("not a simple expression: token %d.", p->tok);
    }

    icode(")");
}       /* gsimple */
