/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef CLIB_FUNC_H
#define CLIB_FUNC_H

#include <stdio.h>
#include "symboldef.h"

void Clib_error(void);
bool match(int c, FILE *fp);
char *get_id(FILE *fp);
symboltable *create_param_table(FILE *fp);
void addCfunction(FILE *fp);
void read_Cfunc_spec(char *fname);
void init_Cfunc_table(void);
void Clib_code(void);

#endif // CLIB_FUNC_H
