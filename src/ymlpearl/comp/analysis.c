/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/* This file implements an analysis pass that checks for missing
 * replies in synchronous functions. */


#include <string.h>
#include <stdio.h>

#include "analysis.h"
#include "ptree.h"
#include "parse.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "subtype.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

/* Private function prototypes */
void analyse_func_meth(symboltable *func_meths);
int rep_an_expr(parsex *expr);
int rep_an_if(parsex *if_expr);
int rep_an_for(parsex *for_expr);
int rep_an_while(parsex *while_expr);
int rep_an_switch(parsex *sw_expr);
int rep_an_choices(parsex *choices, int *has_default);
void rep_an_check_labels(parsex *labels, int *has_default);



#define RETURN_FOUND  -1
#define NO_REPLY_FOUND 0
#define REPLY_FOUND    1

/* Implements a traversal that checks if the expr contains a reply
 * expr.
 * Next step would be to make this traversal generic, so we can reuse
 * it for other analysis.
 */

void reply_analysis(symboltable *classes, symboltable * UNUSED(functions),
                    symboltable * UNUSED(variables))
{
    classx *class;

    if (symbol_getfill(classes) > 0) {
        class = (classx *)symbol_getcont(classes, 0);
        analyse_func_meth(class->func_meths);
    }
}

/*
 * Type check inits of local variables and typecheck the body of a function or
 * method.
 */
void analyse_func_meth(symboltable *func_meths)
{
    for (unsigned int i = 0; i < symbol_getfill(func_meths); i++) {
        mfpx *function = (mfpx *)symbol_getcont(func_meths, i);

        if (function->replytype) {
            setlf(function->lf);

            if (rep_an_expr(function->code) < REPLY_FOUND)
                // RETURN_FOUND (-1) or NO_REPLY_FOUND (0)
            {
                warning("\"%s\": control reaches end of synchronous method\n", function->name);
            }
        }
    }
}


/* This function is based on the type checker traversal.
 * Idea is to make this traversal so general we can refactor it into
 * generic traversal framework. That is why we are checking 'if'
 * conditional expressions for reply expressions..
 */
int rep_an_expr(parsex *expr)
{
    int result;

    if (expr != NULL) {
        setlf(expr->lf);

        switch (expr->tok) {
            case SEMCOL:
                if (expr->left != NULL) {
                    result = rep_an_expr(expr->left);

                    if (result == 1) {
                        return 1;
                    } else if (result == RETURN_FOUND) {
                        return RETURN_FOUND;
                    }
                }

                return rep_an_expr(expr->right);

            case ASG:
            case PLUSASG:
            case MINASG:
            case MULASG:
            case DIVASG:
            case MODASG:
            case ANDASG:
            case ORASG:
            case XORASG:
            case SHLEFTASG:
            case SHRIGHTASG:
            case MUL:
            case DIV:
            case MOD:
            case PLUS:
            case MIN:
            case AND:
            case OR:
            case XOR:
            case SHLEFT:
            case SHRIGHT:
            case ANDAND:
            case OROR:
            case XORXOR:
            case LT:
            case LE:
            case GT:
            case GE:
            case EQ:
            case NE:
            case IDENT:
            case STRUCT:
            case ARRAY:
            case SEND:
            case SENDSEND:
            case FUNCTION_CALL:
            case BLOCK:
            case BLOCKT:
                /* No reply in all these expressions. */
                return NO_REPLY_FOUND;

            case CFUNCTION_CALL:
                if (strcmp(expr->left->str, "panic") == 0) {
                    return REPLY_FOUND;    // panic terminates simulation.
                }

                return NO_REPLY_FOUND;

            case IF:
                return rep_an_if(expr);

            case FOR:
                return rep_an_for(expr);

            case WHILE:
                return rep_an_while(expr);

            case SWITCH:
                return rep_an_switch(expr);

            case BREAK:
                return NO_REPLY_FOUND;

            case RETURN:
                return RETURN_FOUND;

            case REPLY:
                /* Found reply! Set flag of some sort and finish here.
                 * */
                return 1;

            case BOOLNOT:
            case BITNOT:
            case UMIN:
            case FLTCONST:
            case INTCONST:
            case BOOLCONST:
            case STRCONST:
            case ENUM_ELEM:
            case SOURCE:
            case THIS:
                return NO_REPLY_FOUND;

            default:
                setlf(expr->lf);
                panic("What the hell is this kind of expression\n");
        }
    }

    return NO_REPLY_FOUND;
}

int rep_an_if(parsex *if_expr)
{
    int if_result, else_result;

    /* If either if or else encounter a return (before a reply),
     * signal warning that reply might not be executed. */
    if_result = rep_an_expr(if_expr->right);

    if (if_expr->e0 == NULL || if_result == RETURN_FOUND) {
        return if_result;
    }

    else_result = rep_an_expr(if_expr->e0);

    if (else_result == RETURN_FOUND) { // else part encounters a return
        return else_result;    // issue warning.
    }

    /* Both if and else parts need a reply, otherwise signal warning */
    return  if_result && else_result;
}

int rep_an_for(parsex *for_expr)
{
    setlf(for_expr->lf);

    /* init expr */
    if (for_expr->left != NULL)
        if (rep_an_expr(for_expr->left)) {
            return REPLY_FOUND;
        }

    /* condition */
    if (for_expr->right != NULL)
        if (rep_an_expr(for_expr->right)) {
            return REPLY_FOUND;
        }

    /* action */
    /* Cannot guarantee that action is run.
    if (for_expr->e0 != NULL)
        if ( rep_an_expr(for_expr->e0) )
            return REPLY_FOUND;
     */

    /* body */
    /* We cannot be sure that the body is really executed. So we
     * cannot return true if the body guaranties a reply. */
    /* if ( rep_an_expr(for_expr->e1) )
        return REPLY_FOUND; */

    /* no reply found in for expr. */
    return NO_REPLY_FOUND;
}

int rep_an_while(parsex *while_expr)
{
    /* condition */
    if (rep_an_expr(while_expr->left)) {
        return 1;
    }

    /* actually unless condition is true, we cannot know if body will
     * be executed, so should return false in that case. */

    // Cannot really evaluate conditional here. Only check for true.
    if (while_expr->left->tok == BOOLCONST &&
        !strcmp(while_expr->left->str, "true")) {
        /* while body */
        if (while_expr->right != NULL) {
            return rep_an_expr(while_expr->right);
        }

    }

    /* cond unknown */
    return NO_REPLY_FOUND;
}

/* Return true only if _all_ cases contain a reply, and there is a
 * default case. Only then can we be sure a reply will be executed
 * here. */
int rep_an_switch(parsex *sw_expr)
{
    int has_default = 0;
    /* switch condition cannot contain reply */

    /* switch cases */
    /* ok to pass pointer to stack var, we use while its alive. */
    if (rep_an_choices(sw_expr->right, &has_default))
        if (has_default) {
            return REPLY_FOUND;
        }

    return NO_REPLY_FOUND;
}

/* Look for default case in labels, and check for reply in switch
 * case. */
int rep_an_choices(parsex *choices, int *has_default)
{
    /* Without a default label we can skip the cases, so we should
     * return false */
    rep_an_check_labels(choices->right->left, has_default);

    if (!(*has_default)) {
        setlf(choices->lf);
        return NO_REPLY_FOUND;
    }

    /* have a default, so check if all cases have a reply.. */
    if (choices->tok == COMMA) {
        if (choices->left == NULL) { /* last choice */
            return rep_an_expr(choices->right->right);
        } else
            return rep_an_expr(choices->right->right) && \
                   rep_an_choices(choices->left, has_default);
    }

    /* Now at bottom of tree */
    return rep_an_expr(choices->right->right);
}

/* Set has_default to true if default case is found. */
void rep_an_check_labels(parsex *labels, int *has_default)
{
    if ((labels->tok == COMMA) && (labels->left != NULL)) {
        return rep_an_check_labels(labels->left, has_default);
    }

    if (labels->tok == DEFAULT) {
        *has_default = 1;
    }
}
