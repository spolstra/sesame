/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SPEC_CHECK_H
#define SPEC_CHECK_H

#include "symboldef.h"

void spec_type_check(typex *tp, symboltable *sym);
int spec_type_check_complete(typex *t0, typex *t1);
void spec_var_cmp(symboltable *sym0, symboltable *sym1, char *str, int name);
void spec_var_check(varx *vp, symboltable *sym, char *str);
int spec_func_check(mfpx *fp, symboltable *sym);
int strnullcmp(char *s0, char *s1);
int spec_func_check_complete(mfpx *f0, mfpx *f1);
void spec_check(void);
void check_subtypes(void);

#endif // SPEC_CHECK_H
