/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The function 'spec_check' is used to check if everything in specified in the
 * specification file is also declared in the implementation file.
 */

#include <string.h>

#include "spec_check.h"



extern symboltable *modules;
extern symboltable *subtype_table;
extern specx *specs;
extern symboltable *types;
extern symboltable *variables;
extern symboltable *functions;
extern symboltable *classes;


void spec_type_check(typex *tp, symboltable *sym)
{
    int x;

    setlf(tp->lf);

    if ((x = searchsymbol2(sym, tp->name)) == -1) {
        error("Type \"%s\" not found in implementation file.\n", tp->name);
    } else if (spec_type_check_complete(tp, (typex *)symbol_getcont(sym, x)) < 0) {
        error("Type \"%s\" does not match implementation.\n", tp->name);
    };
}

int spec_type_check_complete(typex *t0, typex *t1)
{
    typex *t2, *t3;

    if (t0->type != t1->type) {
        return -1;
    }

    switch (t0->type) {
        case TARRAY:
            if (symbol_getfill(t0->fields) != symbol_getfill(t1->fields)) {
                return -1;
            }

            t2 = (typex *)symbol_getcont(t0->fields, 0);
            t3 = (typex *)symbol_getcont(t1->fields, 0);

            if (strcmp(t2->basetype, t3->basetype)) {
                return -1;
            }

            // Size not checked !!
            break;

        case TSTRUCT:
            spec_var_cmp(t0->fields, t1->fields, "Struct field", 1);
            break;

        default:
            break;
    }

    return 0;
}

void spec_var_cmp(symboltable *sym0, symboltable *sym1, char *str,
                  int name)
{
    varx *v0, *v1;

    if (symbol_getfill(sym0) != symbol_getfill(sym1)) {
        error("Wrong number of %ss in implementation\n", str);
        return;
    }

    for (unsigned int i = 0; i < symbol_getfill(sym0); i++) {
        v0 = (varx *)symbol_getcont(sym0, i);
        v1 = (varx *)symbol_getcont(sym1, i);

        if (name && strcmp(v0->name, v1->name)) {
            error("%s number %d has different name %s/%s in implementation\n",
                  str, i, v0->name, v1->name);
        }

        if (strcmp(v0->type, v1->type)) {
            error("%s %s has non matching types %s/%s in implementation\n", str,
                  v0->name, v0->type, v1->type);
        }
    }
}

void spec_var_check(varx *vp, symboltable *sym, char *str)
{
    int x;
    varx *v0;

    setlf(vp->lf);

    if ((x = searchsymbol2(sym, vp->name)) == -1) {
        error("%s \"%s\" not in implementation file.\n", str, vp->name);
    }

    else {
        v0 = (varx *)symbol_getcont(sym, x);

        if (strcmp(vp->type, v0->type)) {
            error("%s \"%s\" does not match implementation.\n", str, vp->name);
        }
    }
}

int spec_func_check(mfpx *fp, symboltable *sym)
{
    int x;

    setlf(fp->lf);

    if ((x = searchsymbol2(sym, fp->name)) == -1) {
        error("Function \"%s\" not found in implementation file.\n", fp->name);
    } else if (spec_func_check_complete(fp, (mfpx *)symbol_getcont(sym, x)) < 0) {
        error("Function \"%s\" does not match implementation.\n", fp->name);
    }

    if (x != -1) {
        fp = (mfpx *)symbol_getcont(sym, x);

        for (unsigned int i = 0; i < symbol_getfill(fp->parameters); i++) {
            for (unsigned int j = 0; j < symbol_getfill(fp->locals); j++) {
                if (strcmp(((varx *)symbol_getcont(fp->parameters, i))->name,
                           ((varx *)symbol_getcont(fp->locals, j))->name) == 0) {
                    setlf(((varx *)symbol_getcont(fp->locals, j))->lf);
                    error("Local variable %s hides parameter\n",
                          ((varx *)symbol_getcont(fp->locals, j))->name);
                }
            }
        }
    }

    return x;
}

int strnullcmp(char *s0, char *s1)
{
    if (s0 != NULL && s1 != NULL) {
        if (strcmp(s0, s1)) {
            return 1;
        }
    } else if (s0 != s1) {
        return 1;
    }

    return 0;
}

int spec_func_check_complete(mfpx *f0, mfpx *f1)
{
    if (strnullcmp(f0->replytype, f1->replytype)) {
        return -1;
    }

    if (strnullcmp(f0->returntype, f1->returntype)) {
        return -1;
    }

    spec_var_cmp(f0->parameters, f1->parameters, "Parameter", 0);

    return 0;
}

/*
 * This function checks if the types, variables, functions and classes, found
 * in the specification file, are in the implementation file too.
 */
void spec_check(void)
{
    classx *cp, *cp_i;
    typex *tp;
    varx *vp;
    mfpx *fp, *mp;
    int x;
    varx *v0;

    for (unsigned int i = 0; i < symbol_getfill(specs->types); i++) {
        tp = (typex *)symbol_getcont(specs->types, i);

        if (strncmp(tp->name, "_Anonymous_", 11) == 0) {
            continue;
        }

        spec_type_check(tp, types);
    }

    for (unsigned int i = 0; i < symbol_getfill(specs->variables); i++) {
        vp = (varx *)symbol_getcont(specs->variables, i);
        spec_var_check(vp, variables, "Variable");
    }

    for (unsigned int i = 0; i < symbol_getfill(specs->functions); i++) {
        fp = (mfpx *)symbol_getcont(specs->functions, i);
        spec_func_check(fp, functions);
    }

    for (unsigned int i = 0; i < symbol_getfill(specs->classes); i++) {
        cp = (classx *)symbol_getcont(specs->classes, i);

        if ((cp_i = (classx *)searchsymbol1(classes, cp->name)) == NULL) {
            setlf(cp->lf);
            error("Class \"%s\" not found in implementation file.\n", cp->name);

        } else {
            for (unsigned int j = 0; j < symbol_getfill(cp->types); j++) {
                tp = (typex *)symbol_getcont(cp->types, j);

                if (strncmp(tp->name, "_Anonymous_", 11) == 0) {
                    continue;
                }

                spec_type_check(tp, cp_i->types);
            }

            unsigned int k = 0;

            for (unsigned int j = 0; j < symbol_getfill(cp->variables); j++) {
                vp = (varx *)symbol_getcont(cp->variables, j);
                setlf(vp->lf);

                for (; k < symbol_getfill(cp_i->variables); k++) {
                    v0 = (varx *)symbol_getcont(cp_i->variables, k);

                    if (strcmp(vp->name, v0->name) == 0) {
                        if (strcmp(vp->type, v0->type))
                            error("Instance var \"%s\" does not match implementation.\n",
                                  vp->name);

                        break;
                    }
                }

                if (k == symbol_getfill(cp_i->variables))
                    error
                    ("Either you declared the vars in the wrong order\n(implementation restriction) or you forgot %s : %s\n",
                     vp->name, vp->type);
            }

            for (unsigned int j = 0; j < symbol_getfill(cp->func_meths); j++) {
                mp = (mfpx *)symbol_getcont(cp->func_meths, j);
                x = spec_func_check(mp, cp_i->func_meths);

                if (x != -1) {
                    ((mfpx *)symbol_getcont(cp_i->func_meths, x))->flags |= METHOD;
                }
            }
        }
    }
}


extern specx *specs;

/*
 * Check if everything specified in a superclass is also specified in a
 * subclass
 */
void check_subtypes(void)
{
    for (unsigned int i = 0; i < symbol_getfill(subtype_table); i++) {
        subtype_rel *rel_i = (subtype_rel *)symbol_getcont(subtype_table, i);

        classx *class = (classx *)searchsymbol1(specs->classes, rel_i->type);

        if (class == NULL) {
            error("Finding class %s\n", rel_i->type);
            continue;
        }

        classx *subclass = (classx *)searchsymbol1(specs->classes, rel_i->subtype);

        if (subclass == NULL) {
            error("Finding subclass %s\n", rel_i->subtype);
            continue;
        }

        for (unsigned int j = 0; j < symbol_getfill(class->types); j++) {
            typex *tp = (typex *)symbol_getcont(class->types, j);

            if (searchsymbol2(subclass->types, tp->name) == -1) {
                setlf(NULL);
                error("Type \"%s\" in class \"%s\" not found in subclass \"%s\".",
                      tp->name, class->name, subclass->name);
            }
        }

        for (unsigned int j = 0; j < symbol_getfill(class->variables); j++) {
            varx *vp = (varx *)symbol_getcont(class->variables, j);

            if (searchsymbol2(subclass->variables, vp->name) == -1) {
                setlf(NULL);
                error("Variable \"%s\" in class \"%s\" not found in subclass \"%s\".",
                      vp->name, class->name, subclass->name);
            }
        }

        for (unsigned int j = 0; j < symbol_getfill(class->func_meths); j++) {
            mfpx *mp = (mfpx *)symbol_getcont(class->func_meths, j);

            if (searchsymbol2(subclass->func_meths, mp->name) == -1) {
                setlf(NULL);
                error("Method \"%s\" in class \"%s\" not found in subclass \"%s\".",
                      mp->name, class->name, subclass->name);
            }
        }
    }
}
