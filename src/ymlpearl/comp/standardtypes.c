/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "ptree.h"
#include "symboldef.h"
#include "standardtypes.h"
#include "util.h"
#include "parse.h"
#include "g_tokstr.h"
#include "type_size.h"
#include "link_type.h"
#include "subtype.h"

extern symboltable *basic_types;

typex *typevoid;
typex *typeinteger;
typex *typefloat;
typex *typestring;
typex *typeboolean;
typex *typerealtime;
extern typex *roottype();

void initstandardtypes(void)
{
    typevoid = (typex *)searchsymbol1(basic_types, TYPEVOID);
    typeinteger = (typex *)searchsymbol1(basic_types, TYPEINTEGER);
    typefloat = (typex *)searchsymbol1(basic_types, TYPEFLOAT);
    typestring = (typex *)searchsymbol1(basic_types, TYPESTRING);
    typeboolean = (typex *)searchsymbol1(basic_types, TYPEBOOLEAN);
    typerealtime = (typex *)searchsymbol1(basic_types, realtime);
    calc_type_size(basic_types);
}

int isint_(typex *t)
{
    if (strncmp(t->name, "int_", 4) == 0) {
        return 1;
    }

    return 0;
}

static int largertype(typex *a, typex *b)
{
    int a_ = isint_(a);
    int b_ = isint_(b);

    if (a_ && b_) {
        return strcmp(a->name, b->name) > 0;
    }

    if (!a_ && !b_) {
        return 0;
    }

    if (a_) {
        b = a;
    }

    return atoi(b->name + 4) > 32;
}

static void nomatch(typex *typl, typex *typr, int operator)
{
    error("Non matching types %s %s, operator '%s'",
          typl ? typl->name : "void", typr ? typr->name : "void",
          g_tokstr(operator));
}

static typex *insertcastop(parsex **expr, typex *totype)
{
    parsex *t = new_parsex(CASTTYPE, *expr, NULL, NULL, NULL, NULL);

    t->lf = (*expr)->lf;
    t->tsize = totype->typesize;
    t->type = totype;
    *expr = t;
    return totype;
}

int tc_compatibel(typex *typl, typex *typr, parsex **expr)
{
    typr = roottype(typr);
    typl = roottype(typl);

    if (typr == typl) {
        return 1;
    }

    if (typl == NULL || typr == NULL) {
        return 0;
    }

    if (typl == typeinteger || isint_(typl) || typl == typefloat) {
        if (typr == typeinteger || isint_(typr) || typr == typefloat) {
            insertcastop(expr, typl);
            return 1;
        }
    }

    if (strcmp(typr->name, typl->name) == 0) {
        return 1;
    }

    if (isclasstype(typr) && isclasstype(typl) &&
        is_subtypeof(((classx *) typl)->name, ((classx *) typr)->name)) {
        return 1;
    }

    if (isclasstype(typr), typl == typeinteger) {
        return 1;
    }

    return 0;
}

typex *tc_asgop(typex *typl, typex *typr, parsex *expr)
{
    if (!tc_compatibel(typl, typr, &expr->right)) {
        nomatch(typl, typr, expr->tok);
    }

    return typl;
}

typex *tc_arithmeticop(typex *typl, typex *typr, parsex *expr)
{
    if (typl == NULL || typr == NULL) {
        nomatch(typl, typr, expr->tok);
    }

    if (expr->tok == SHLEFT || expr->tok == SHRIGHT) {
        if ((typr == typeinteger || isint_(typr)) &&
            (typl == typeinteger || isint_(typl))) {

            if (largertype(typl, typeinteger)) {
                return typl;
            } else {
                return typeinteger;
            }
        }

        nomatch(typl, typr, expr->tok);
        return typl;
    }

    if (typl == typeinteger) {
        if (typr == typeinteger) {
            return typeinteger;
        } else if (isint_(typr)) {
            if (largertype(typl, typr)) {
                return insertcastop(&expr->right, typl);
            } else {
                return insertcastop(&expr->left, typr);
            }
        } else if (typr == typefloat) {
            return insertcastop(&expr->left, typefloat);
        }
    } else if (isint_(typl)) {
        if (typr == typeinteger || isint_(typr)) {
            if (largertype(typl, typr)) {
                return insertcastop(&expr->right, typl);
            } else {
                return insertcastop(&expr->left, typr);
            }
        } else if (typr == typefloat) {
            return insertcastop(&expr->left, typefloat);
        }
    } else if (typl == typefloat) {
        if (typr == typeinteger || isint_(typr)) {
            return insertcastop(&expr->right, typefloat);
        } else if (typr == typefloat) {
            return typefloat;
        }
    }

    if (!tc_compatibel(typl, typr, &expr->right)) {
        nomatch(typl, typr, expr->tok);
    }

    return typl;
}

typex *tc_compareop(typex *typl, typex *typr, parsex *expr)
{
    tc_arithmeticop(typl, typr, expr);

    return typeboolean;
}


symboltable *has_replys;

void inithasreplys(void)
{
    has_replys = NULL;
}

symboltable *gethasreplys(void)
{
    return has_replys;
}

void addreplytype(char *tstr)
{
    symboltable *params;
    symboltable *func_meths;
    extern symboltable *classes;
    char *s = malloc(strlen(tstr) + 7);
    mfpx *mfp;
    varx *vp;

    if (symbol_getfill(classes) == 0) {
        error("Shit\n");
    }

    func_meths = ((classx *)symbol_getcont(classes, 0))->func_meths;
    strcpy(s, "reply_");
    strcat(s, tstr);

    if (searchsymbol1(func_meths, s) != NULL) {
        free(s);
        return;
    }

    params = newsymboltable(sizeof(varx));
    addvariable(params, "#x", strdup(tstr), PARAMETER, NULL, NULL);
    vp = (varx *)symbol_getcont(params, 0);
    vp->realtype = (char *)get_realtype(tstr);
    addfunction(func_meths, s, strdup(tstr), NULL, params,
                newsymboltable(sizeof(varx)), NULL, 1, CLASSSYM, NULL);
    mfp = (mfpx *) searchsymbol1(func_meths, s);
    mfp->realreply = NULL;
    mfp->realreturn = vp->realtype;
    mfp->paramsize = ((typex *) vp->realtype)->typesize;
    fm_var_place(func_meths);
}

void addreplymethod(symboltable *func_meths, char *tstr)
{
    symboltable *params = newsymboltable(sizeof(varx));
    mfpx *mf;

    addvariable(params, "#x", strdup(tstr + 6), PARAMETER, NULL, NULL);
    addfunction(func_meths, strdup(tstr), strdup(tstr + 6), NULL, params,
                newsymboltable(sizeof(varx)), NULL, 1, CLASSSYM, NULL);
    mf = (mfpx *) searchsymbol1(func_meths, tstr);
    setlf(mf->lf);
    mf->realreturn = (char *)get_realtype(mf->returntype);
    setflags(&mf->flags, TYPERET);
    ln_typeid_vars(params);
    mf->paramsize = ((typex *) mf->realreturn)->typesize;
}

typex *insertsourcetype(char *tstr)
{
    symboltable *func_meths = newsymboltable(sizeof(mfpx));

    if (has_replys == NULL) {
        has_replys = newsymboltable(sizeof(classx));
    }

    if (get_realtype(tstr + 10) == NULL)
        error("%s is not a valid type, type \"%s\" does not exist\n", tstr,
              tstr + 10);

    addreplymethod(func_meths, tstr + 4);
    addclass(has_replys, strdup(tstr), NULL,
             newsymboltable(sizeof(typex)),
             newsymboltable(sizeof(varx)),
             func_meths,
             newsymboltable(sizeof(namex)),
             0, NULL, newsymboltable(sizeof(varx)));
    return (typex *) searchsymbol1(has_replys, tstr);
}

typex *findsourcetype(char *name)
{
    char buf[80];
    typex *ret;

    if (strlen(name) > sizeof(buf) - 11) {
        panic("Out of string space in findsourcetype\n");
    }

    strcpy(buf, "has_reply_");
    strcat(buf, name);
    ret = (typex *) searchsymbol1(has_replys, buf);

    if (ret == NULL) {
        ret = insertsourcetype(buf);
    }

    return roottype(ret);
}

typex *findhasreply(char *name)
{
    typex *ret;

    ret = (typex *) searchsymbol1(has_replys, name);

    if (ret == NULL) {
        ret = insertsourcetype(name);
    }

    return ret;
}

void sizehasreplies(void)
{
    if (has_replys) {
        class_var_place(has_replys);
    }
}
