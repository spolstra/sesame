/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1988 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */



#ifndef LEX_AN_H

#define LEX_AN_H


/*
 * Filename and linenumber, used to locate errors.
 */
typedef struct linex {
    int line;
    char *file;
} linex;



linex *getlf();
void lineno(int);
int lllineno();
int yylex();
void setlf(linex *l);
void error(char *fmt, ...);
void panic(char *fmt, ...);
void warning(char *fmt, ...);

#endif        // LEX_AN_H
