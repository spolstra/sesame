/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef TYPE_CHECK_H
#define TYPE_CHECK_H

#include "symboldef.h"

typex *roottype(typex *tp);
typex *tc_var(parsex *var);
typex *tc_atomic(parsex *at_expr);
typex *tc_send(parsex *send_expr);
typex *tc_func_call(parsex *func_call);
typex *tc_block(parsex *block_expr);
typex *tc_blockt(parsex *blockt_expr);
typex *tc_if(parsex *if_expr, bool break_ok, bool return_ok, bool reply_ok);
typex *tc_for(parsex *for_expr, bool return_ok, bool reply_ok);
typex *tc_while(parsex *while_expr, bool return_ok, bool reply_ok);
void tc_labels(parsex *labels, typex *sw_type, bool *with_default);
typex *tc_choices(parsex *choices, typex *sw_type, bool *with_default,
                  bool return_ok, bool reply_ok);
typex *tc_switch(parsex *sw_expr, bool return_ok, bool reply_ok);
typex *tc_expr(parsex *expr, bool break_ok, bool return_ok, bool reply_ok);
void tc_var_inits(symboltable *variables);
void tc_func_meth(symboltable *func_meths);
void type_check(symboltable *classes, symboltable *functions,
                symboltable *variables);

#endif // TYPE_CHECK_H
