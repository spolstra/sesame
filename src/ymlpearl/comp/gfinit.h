/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef GFINIT_H
#define GFINIT_H

#include "symboldef.h"

parsex *find_init(char *var, parsex *inits);
char *getvarsize(symboltable *variables);
void varinitstart(classx *cl);
void newbcopylist(char *s);
void dumpbcopylist(void);
void varinitend(classx *cl);
void dovarinit(classx *cl, varx *vp, int idx);

#endif // GFINIT_H
