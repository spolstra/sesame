/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file generate the code for the switch statement.
 * A conventional switch is generated, but the code for the alternatives is
 * replaced bij goto statements with labels. The code for the alternatives is
 * written below the switch, preceded bij labels.
 */

#include "gswitch.h"
#include "ptree.h"
#include "parse.h"
#include "iscode.h"
#include "gsimple.h"
#include "generate.h"


extern int sp;


/*
 * Generate the labels for a choice.
 */
void gchoices(parsex *c)
{
    /* gchoices */
    parsex *cp;

    if (!c) {
        return;
    }

    if (c->tok == COMMA) {
        gchoices(c->left);
    }

    c = c->right;

    if (c->left->tok == COMMA)
        for (cp = c->left; cp; cp = cp->left) {
            icode("case ");
            gsimple(cp->right);
            icode(":\n");
        }
    else {
        icode("default:\n");
    }

    icode("goto L0x%x;\n", c);
}       /* gchoices */



/*
 * Generate the code for an alternative of the switch statement.
 */
void gch_code(parsex *c, parsex *after_switch)
{
    /* gch_code */
    if (!c) {
        return;
    }

    if (c->tok == COMMA) {
        gch_code(c->left, after_switch);
    }

    c = c->right;
    icode("L0x%x:\n", c);
    generate(c->right);
    icode("goto L0x%x;\n", after_switch);
}       /* gch_code */



/*
 * Generate code for switch statement.
 */
void gswitch(parsex *c)
{
    /* gswitch */
    if (issimple(c->left)) {
        icode("switch (");
        gsimple(c->left);
        icode(") {\n");
    } else {
        generate(c->left);
        icode("switch (sp[%d]) {\n", sp);
    }

    gchoices(c->right);
    icode("}\n");

    /* If there is no default case we don't want to fall thru to first case
     * if none of the cases match. So we jump to after switch label. */
    icode("goto L0x%x;\n", c->right);

    gch_code(c->right, c->right);
    icode("L0x%x:;\n", c->right);
}       /* gswitch */
