/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */

#include <stdio.h>
#include <string.h>

#include "gsend.h"
#include "ptree.h"
#include "parse.h"
#include "iscode.h"
#include "symboldef.h"
#include "gstate.h"
#include "blockers.h"
#include "gsimple.h"
#include "gfinit.h"
#include "gatomic.h"
#include "generate.h"
#include "gfunc.h"

extern int sp;
extern typex *this_reply_type;

/*
 * Generate the code for a source statement.
 */
void gsource(void)
{
    icode("pp[-3]");
}

/*
 * Generate the code for a reply statement.
 */
void greply(parsex *c)
{
    char *tsize;

    if (c->left != NULL) {
        generate(c->left);
        tsize = c->left->tsize;

    } else {
        tsize = "0";
    }

    icode("pearlsend(");
    gsource();
    icode(", method_reply_%s, &sp[%d], %s);\n",
          this_reply_type->name, sp, tsize);
}



/*
 * Generate code for a send statement. In case of a synchronous send, code for
 * the receiving of a reply message is also generated.
 */
void gsend(parsex *c, int with)
{
    char *rep_size;
    mfpx *mp;
    char *blocker;
    char tempstr[128];
    int complex = 0;

    mp = (mfpx *)c->right->sym_entry;

    if (mp->realreply) {
        if (isclasstype((typex *)mp->realreply)) {
            rep_size = "1";
        } else {
            rep_size = ((typex *) mp->realreply)->typesize;
        }

    } else {
        rep_size = "0";
    }

    if (isclasstype(c->left->type)) {
        if (!issimple(c->left)) {
            complex = 1;
            generate(c->left);
            sp++;
        }

        if (c->e0) {
            gpar(c->e0, 0, 0);
            grmpar(mp->paramsize, 0);
        }

        if (complex) {
            icode("pearlsend(sp[%d], method_%s, &sp[%d], %s);\n", sp - 1,
                  mp->name, sp, mp->paramsize);
            sp--;

        } else {
            icode("pearlsend(");
            gsimple(c->left);
            icode(", method_%s, &sp[%d], %s);\n", mp->name, sp, mp->paramsize);
        }

    } else {
        if (!issimple(c->left)) {
            complex = 1;
            generate(c->left);
            incrsp(c->left->tsize);
        }

        if (c->e0) {
            gpar(c->e0, 0, 0);
            grmpar(mp->paramsize, 0);
        }

        if (complex) {
            decrsp(c->left->tsize);
        }

        icode("{\n");
        icode("int __icnt;\n");
        icode("for (__icnt = 0; __icnt < %s; __icnt ++) {\n",
              ((typex *)symbol_getcont(c->left->type->fields, 1))->typesize);

        if (complex) {
            icode("pearlsend(sp[%d+__icnt], method_%s, &sp[%d+%s], %s);\n", sp,
                  mp->name, sp, c->left->tsize, mp->paramsize);

        } else {
            icode("pearlsend(");
            gatomic(c->left);
            icode("[__icnt], method_%s, &sp[%d], %s);\n", mp->name, sp,
                  mp->paramsize);
        }

        icode("}\n");
        icode("}\n");
    }

    if (c->tok == SEND) {
        strcpy(tempstr, "reply_");
        strcat(tempstr, c->str);

        newblocker();
        appendanonymousblocker(strdup(tempstr));
        // SP: it seems we cannot handle statistics here
        // I propose we comment the next line, see ticket #10
        //appendanonymousblocker(strdup("statistics"));
        blocker = (char *)closeblocker();

        icode("{\n");

        icode("extern intptr_t %s;\n", blocker);

        icode("if (!pearlreceive(%s, &sp[%d])) {\n", blocker, sp);
        state_incr();
        state_assign();
        icode("pearlwaiting(1);\n");
        icode("goto Freturn;\n");
        icode("}\n");
        icode("}\n");

        state_case();
        icode("pearlwaiting(0);\n");

        if (with) {
            if (strcmp(rep_size, "1") == 0) {
                icode("sp[%d] = sp[%d];\n", sp, sp + 1);

            } else if (strcmp(rep_size, "0") != 0) {

                icode("bcopy(sp + %d, sp + %d, %s * sizeof(intptr_t));\n", sp + 1, sp,
                      rep_size);

                newbcopylist(rep_size);

            } else {
                icode("// Sync only, no value\n");
            }
        }
    }
}
