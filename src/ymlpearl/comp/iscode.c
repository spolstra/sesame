/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The functions in this file are used to write on the .c and .h file where
 * the result of the translation has to come.
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdarg.h>

#include  "iscode.h"
#include "lex_an.h"

#if defined(__cplusplus)
#define UNUSED(x)       // = nothing
#elif defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

#define INDENTSPACING 4

FILE *fp_impl_out;

static int ilevel = 0;

/*
 * Concatenate name and extension and open file.
 */
void openfile(FILE **fp, char *name, char *ext)
{
    char filename[128];

    strcpy(filename, name);
    strcat(filename, ext);

    if ((*fp = fopen(filename, "w")) == NULL) {
        setlf(NULL);
        error("Cannot open file \"%s\" for writing\n", filename);
    }
}



/*
 * Opens files for generated code.
 */
void popenfiles(char *filename, int both)
{

    if (both) {
        openfile(&fp_impl_out, filename, ".c");
    } else {
        openfile(&fp_impl_out, "/dev/null", "");
    }

    ilevel = 0;
}



/*
 * Close file.
 */
void closefile(FILE **fp)
{
    fclose(*fp);
}



/*
 * Close files.
 */
void closefiles(char * UNUSED(filename))
{
    closefile(&fp_impl_out);
}


static void prline(char *s, int withnewline)
{
    static int indentneeded = 0;
    int strls = strlen(s);

    if (s[0] == '}') {
        ilevel -= INDENTSPACING;
    }

    if (ilevel < 0) {
        ilevel = 0;
    }

    if (indentneeded) {
        if (s[strls - 1] == ':' || (s[strls - 2] == ':' && s[strls - 1] == ';')) {
            fprintf(fp_impl_out, "%*s", ilevel - INDENTSPACING / 2, "");
        } else {
            fprintf(fp_impl_out, "%*s", ilevel, "");
        }
    }

    fputs(s, fp_impl_out);

    if (s[0] == '{' || s[strlen(s) - 1] == '{') {
        ilevel += INDENTSPACING;
    }

    if (withnewline) {
        putc('\n', fp_impl_out);
        indentneeded = 1;
    } else {
        indentneeded = 0;
    }
}

/*
 * Write arguments on the file containing the implementation part of the
 * generated code.
 */
void icode(char *pformat, ...)
{
    va_list ap;
    char buffer[512];
    char *x;
    char *line;

    va_start(ap, pformat);
    vsprintf(buffer, pformat, ap);
    line = buffer;

    do {
        x = strchr(line, '\n');

        if (x != NULL) {
            *x = '\0';
            prline(line, 1);
            line = x + 1;
        } else if (*line != '\0') {
            prline(line, 0);
        }
    } while (x != NULL);
}

/**
 * Write a label to the generated code
 */
void ilabel(const char *label, ...)
{
    va_list ap;
    char buffer[512];

    va_start(ap, label);
    vsprintf(buffer, label, ap);

    fprintf(fp_impl_out, "%s", buffer);
}
