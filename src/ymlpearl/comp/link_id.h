/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef LINK_ID_H
#define LINK_ID_H

#include "symboldef.h"

bool check_indices(parsex *pt, typex *ctype, typex *basetype, unsigned int i);
bool ln_enum_type_elem(parsex *pt);
void ln_id_symtab_id(parsex *pt, symboltable *classvar, symboltable *parvar,
                     symboltable *localvar);
void ln_id_symtab_fields(parsex *pt, varx *var_entry, symboltable *classmf,
                         symboltable *classvar, symboltable *parvar,
                         symboltable *localvar);
void ln_id_symtab_complex(parsex *pt, symboltable *classmf,
                          symboltable *classvar, symboltable *parvar,
                          symboltable *localvar);
void ln_id_symtab_method(parsex *pt, classx *class_entry, symboltable *classmf,
                         symboltable *classvar, symboltable *parvar,
                         symboltable *localvar);
void ln_id_symtab_send(parsex *pt, symboltable *classmf, symboltable *classvar,
                       symboltable *parvar, symboltable *localvar);
void ln_id_symtab_block(parsex *pt, symboltable *classmf,
                        symboltable *classvar, symboltable *parvar,
                        symboltable *localvar);
void ln_id_symtab_funccall(parsex *pt, symboltable *classmf,
                           symboltable *classvar, symboltable *parvar,
                           symboltable *localvar);
void ln_id_symtab_classinst(parsex *pt, symboltable *classmf,
                            symboltable *classvar, symboltable *parvar,
                            symboltable *localvar);
void ln_id_symtab_expr(parsex *code, symboltable *classmf,
                       symboltable *classvar, symboltable *parvar,
                       symboltable *localvar);
void ln_id_symtab_varinit(symboltable *vartable, symboltable *classmf,
                          symboltable *classvar, symboltable *parvar,
                          symboltable *localvar);
void ln_id_symtab_mf(symboltable *mft, symboltable *cmft, symboltable *cvt);
void ln_id_symtab_types(symboltable *tt, symboltable *cvt);
void ln_id_symtab_class(symboltable *ct);
void link_id(void);

#endif // LINK_ID_H
