/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef GENERATE_H
#define GENERATE_H

#include "symboldef.h"

void incrsp(char *value);
void decrsp(char *value);
void incrsp2(char *value, int optok);
void decrsp2(char *value, int optok);
void generate(parsex *c);
void generate_void(parsex *c);
void warnvoid(linex *lf);
void glvalue(parsex *c);
void gscope(parsex *c);
void generate_gen(parsex *c, int with);
void castfloat(typex *type, int sp);
void castinteger(typex *type, int sp);
void castint_(typex *type, typex *totype, int sp);

#endif // GENERATE_H
