/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef SUBTYPE_H
#define SUBTYPE_H

#include "symboldef.h"

void init_subtypetable(symboltable **table);
void insert_relation(symboltable *table, char *relation);
void add_relations(symboltable *table, symboltable *classes);
bool is_subtypeof(char *type, char *subtype);

#endif // SUBTYPE_H
