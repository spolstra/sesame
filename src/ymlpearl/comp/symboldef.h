/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/**
 *
 * @file symboldef.h
 * @author B.J. Overeinder & J.G. Stil
 * @date   Fri May 16 13:08:41 CEST 2014
 *
 * @brief
 * This file contains the structures that are used as items in the various
 * symboltables. Further some contstants are defined.
 */


#ifndef SYMBOLDEF_H
#define SYMBOLDEF_H

#include "ptree.h"
#include "symbol.h"
#include "util.h"


#define GLOBALSYM 1 //  defines global scope 
#define CLASSSYM  2 //  defines class scope 
#define PARAMETER 3 //  defines parameter scope 
#define LOCALSYM  4 //  defines local scope 
#define INVISIBLESYM  12 //  defines scope in imported specclass 


#define TBASIC    5
#define TARRAY    6
#define TPOINTER  7
#define TENUM   8
#define TSTRUCT   9
#define TTYPE   10
#define TCLASS    11


// Symboltable entries

typedef struct specx {
    symboltable *use_files;
    symboltable *types;
    symboltable *variables;
    symboltable *functions;
    symboltable *classes;
} specx;


typedef struct typex {
    char *name;
    char *basetype;
    char *realtype;
    int type;
    symboltable *fields;
    parsex *range;
    int scope;
    int flags;
    char *typesize;
    linex *lf;
} typex;



typedef struct varx {
    char *name;
    char *type;
    char *realtype;
    parsex *init;
    int scope;
    int flags;
    char *offset; //  Offset in frame, can be constant
    linex *lf;
} varx;


typedef struct mfpx {
    char *name;
    char *replytype;
    char *realreply;
    char *returntype;
    char *realreturn;
    symboltable *parameters;
    symboltable *locals;
    parsex *code;
    int scope;
    int flags;
    int is_double;
    char *paramsize; //  Size of parameters; variable
    char *localsize; //  Size of locals; variable
    int method_label;
    linex *lf;
    int istriv;
} mfpx;


/**
 * \typedef
 * \struct classx "symboldef.h"
 * Compile time class info datastructure.
 * (For some reason doxygen merges this with classx in runtime)
 */
typedef struct classx {
    char *name;                 //!< Class name
    symboltable *types;
    symboltable *variables;
    int verydirty; // This must be here so classx and typex match
    symboltable *func_meths;
    symboltable *subtypeof;
    int scope;
    int state_statistics;
    parsex *code;
    linex *lf;
    symboltable *localvar;
    char *varsize;
    char *cvarsize;
} classx;


typedef struct modulex {
    char *name;
    specx *specs;
    symboltable *types;
    symboltable *variables;
    symboltable *functions;
    symboltable *classes;
    bool label_init;
} modulex;


typedef struct namex {
    char *name;
    bool to_compile;
} namex;


typedef struct filex {
    char *name;
    parsex *spec, *impl;
} filex;


typedef struct subtype_rel {
    char *type;
    char *subtype;
    bool init;
} subtype_rel;

void initmodule(void);
void init_moduletable(void);
void addsubtype(symboltable *t, char *name);
void addtype(symboltable *t, char *name, int type, char *basetype,
             symboltable *fields, int scope, linex *lf);
void addvariable(symboltable *t, char *name, char *type, int scope,
                 parsex *init, linex *lf);
void addfunction(symboltable *t, char *name, char *rettype, char *reptype,
                 symboltable *params, symboltable *locals, parsex *code,
                 bool ismethod, int scope, linex *lf);
void addclass(symboltable *t, char *name, parsex *code, symboltable *types,
              symboltable *variables, symboltable *func_meths,
              symboltable *subtypeof, int scope, linex *lf,
              symboltable *localvars);
void addmodule(symboltable *t, char *name, specx *specs, symboltable *types,
               symboltable *variables, symboltable *functions,
               symboltable *classes);

#endif // SYMBOLDEF_H
