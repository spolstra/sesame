/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef LINK_TYPE_H
#define LINK_TYPE_H

#include "symboldef.h"

void init_basic_types(void);
char *get_realtype(char *tstr);
void ln_typeid_basetype(typex *type);
void ln_typeid_vars(symboltable *vtable);
void ln_typeid_struct(typex *type);
void ln_typeid_array(typex *type);
void ln_typeid_types(symboltable *ttable);
void ln_typeid_mf(symboltable *mftable);
void ln_typeid_class(symboltable *ctable);
void ln_typeid_Cfunc(void);
void ln_typeid_spec(void);
void ln_typeid_impl(void);
void link_typeid(void);
void link_typeid_withoutimpl(void);

#endif // LINK_TYPE_H
