/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef GFUNC_H
#define GFUNC_H

#include "symboldef.h"

int gpar(parsex *act_par, int pn, int whichsp);
void grmpar(char *size, int whichsp);
void gscall_par(parsex *p, parsex *tp);
void gfunc(parsex *fc, int with);
void gcfunc(parsex *fc);

#endif // GFUNC_H
