/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * The global purpose of these functions is to link the user defined types
 * to their primitives, i.e.
 *  year = integer
 *  man = {
 *      age: year
 *      employed: boolean
 *        }
 * The definition of 'year', stored in the symbol table of types, is linked with
 * the symbol table entry of 'integer', and in case of the structure 'man' each
 * field is linked separately.
 * In this way the types of variables, return and reply types of methods or
 * functions, etc., are linked.
 */



#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "link_type.h"
#include "symboldef.h"
#include "util.h"
#include "standardtypes.h"


/*
 * Symbol tables of imported specification files, basic types and of the
 * implementation file.
 */
extern specx *specs;
extern symboltable *basic_types;
extern symboltable *Clib_functions;
extern symboltable *types;
extern symboltable *variables;
extern symboltable *functions;
extern symboltable *classes;



/*
 * Global variables to this file, describing whether or not we are situated
 * in an implementation file and inside a class, and whether or not the type
 * returned by 'get_realtype()' is a class.
 */
static bool implementation = FALSE;
static bool class = FALSE;
static bool classtype;


/*
 * Local types inside a class.
 */
static symboltable *class_types;


/*
 * The basic (standard) types.
 */
char *basictypes[] = {
    TYPEINTEGER,
    TYPEBOOLEAN,
    TYPESTRING,
    TYPEFLOAT,
    TYPEVOID,
    /* TYPETIME , not included, added later on! */
    NULL
};
char realtime[10];


/*
 * Initiate the symbol table for basic types.
 * The maximum number of bits in int_# depends on the constant MAXINTLENGHT,
 * defined in 'symboldef.h'.
 */
void init_basic_types(void)
{
    for (int i = 0; basictypes[i] != NULL; i++) {
        addtype(basic_types, basictypes[i], TBASIC, NULL, NULL, GLOBALSYM,
                NULL);
    }

    sprintf(realtime, "int_%d", TYPETIME_BITS);
    addtype(basic_types, realtime, TBASIC, NULL, NULL, GLOBALSYM, NULL);
    initstandardtypes();
}


/*
 * Find the entry in the type symbol tables with 'name' equal to 'tstr'.
 */
char *get_realtype(char *tstr)
{
    char *t = NULL;

    if (class)
        if ((t = searchsymbol1(class_types, tstr)) != NULL) {
            classtype = FALSE;
            return (t);
        }

    if (implementation)
        if ((t = searchsymbol1(types, tstr)) != NULL) {
            classtype = FALSE;
            return (t);
        }

    if (implementation)
        if ((t = searchsymbol1(classes, tstr)) != NULL) {
            classtype = TRUE;
            return (t);
        }

    if ((t = searchsymbol1(specs->types, tstr)) != NULL) {
        classtype = FALSE;
        return (t);
    }

    if ((t = searchsymbol1(specs->classes, tstr)) != NULL) {
        classtype = TRUE;
        return (t);
    }

    if ((t = searchsymbol1(basic_types, tstr)) != NULL) {
        classtype = FALSE;
        return (t);
    }

    if (strcmp(tstr, TYPETIME) == 0) {
        classtype = FALSE;
        return searchsymbol1(basic_types, realtime);
    }

    if (strncmp(tstr, "int_", 4) == 0) {
        char buf[10];
        classtype = FALSE;
        sprintf(buf, "%d", atoi(tstr + 4));

        if (strcmp(buf, tstr + 4) != 0) {
            error("%s is not a valid type name\n", tstr);
        }

        addtype(basic_types, tstr, TBASIC, NULL, NULL, GLOBALSYM, NULL);
        return searchsymbol1(basic_types, tstr);
    }

    if (strncmp(tstr, "has_reply_", 10) == 0) {
        t = (char *)findhasreply(tstr);
        classtype = TRUE;
        return t;
    }

    if (t == NULL) {
        setlf(NULL);
        error("type \"%s\" used, but not defined.", tstr);
    }

    return (t);
}



/*
 * Link field 'realtype' to symbol table entry of 'basetype', and set the
 * flag according to the sort of type: class or type.
 */
void  ln_typeid_basetype(typex *type)
{
    type->realtype = get_realtype(type->basetype);
    setflags(&type->flags, classtype ? CLASSTYPE : TYPETYPE);
}



/*
 * Link types in symbol table 'vtable', i.e. symbol table for variables.
 */
void ln_typeid_vars(symboltable *vtable)
{
    if (vtable == NULL) {
        return;
    }

    for (unsigned int i = 0; i < symbol_getfill(vtable); i++) {
        varx *vp = (varx *)symbol_getcont(vtable, i);
        setlf(vp->lf);
        vp->realtype = get_realtype(vp->type);
        setflags(&vp->flags, classtype ? CLASSTYPE : TYPETYPE);
    }
}



/*
 * Link the fields of the stucture 'type'.
 */
void ln_typeid_struct(typex *type)
{
    ln_typeid_vars(type->fields);
}



/*
 * Link base type of array 'type'.
 */
void ln_typeid_array(typex *type)
{
    switch (((typex *)symbol_getcont(type->fields, 0))->type) {
        case TTYPE:
            ln_typeid_basetype((typex *)symbol_getcont(type->fields, 0));
            break;

        case TSTRUCT:
            ln_typeid_struct((typex *)symbol_getcont(type->fields, 0));
            break;

        case TENUM:
            /* there's nothing to link */
            ;
            break;

        default:
            error("unknown type definition for \"%s\".", type->name);
    }
}



/*
 * Link auxiliary user type definitions.
 */
void ln_typeid_types(symboltable *ttable)
{
    for (unsigned int i = 0; i < symbol_getfill(ttable); i++) {
        setlf(((typex *)symbol_getcont(ttable, i))->lf);

        switch (((typex *)symbol_getcont(ttable, i))->type) {
            case TTYPE:
                ln_typeid_basetype((typex *)symbol_getcont(ttable, i));
                break;

            case TSTRUCT:
                ln_typeid_struct((typex *)symbol_getcont(ttable, i));
                break;

            case TARRAY:
                ln_typeid_array((typex *)symbol_getcont(ttable, i));
                break;

            case TENUM:
                /* there's nothing to link */
                ;
                break;

            default:
                error("unknown type definition for \"%s\".",
                      *(char **)symbol_getcont(ttable, i));
        }
    }
}



/*
 * Link return and reply type of method or function, and link types of
 * parameters and local variables.
 */
void ln_typeid_mf(symboltable *mftable)
{
    for (unsigned int i = 0; i < symbol_getfill(mftable); i++) {
        mfpx *mf = (mfpx *)symbol_getcont(mftable, i);
        setlf(mf->lf);

        if (mf->returntype != NULL) {
            mf->realreturn = get_realtype(mf->returntype);
            setflags(&mf->flags, classtype ? CLASSRET : TYPERET);

            //printf("return type = %s %s\n", mf->returntype,
            //     ((typex *)mf->realreturn)->name);
        }

        if (ismethod(mf->flags) && mf->replytype != NULL) {
            mf->realreply = get_realtype(mf->replytype);
            setflags(&mf->flags, classtype ? CLASSREP : TYPEREP);
        }

        ln_typeid_vars(mf->parameters);
        ln_typeid_vars(mf->locals);
    }
}



/*
 * Link local types, variables and methods of the classes in
 * symbol table 'ctable'.
 */
void ln_typeid_class(symboltable *ctable)
{
    class = TRUE;

    for (unsigned int i = 0; i < symbol_getfill(ctable); i++) {
        classx *cl = (classx *)symbol_getcont(ctable, i);
        setlf(cl->lf);
        class_types = cl->types;
        ln_typeid_types(cl->types);
        ln_typeid_vars(cl->variables);
        ln_typeid_vars(cl->localvar);
        ln_typeid_mf(cl->func_meths);
    }

    class = FALSE;
}



/*
 * Link the C functions used by the Pearl language.
 */
void ln_typeid_Cfunc(void)
{
    ln_typeid_mf(Clib_functions);
}



/*
 * Link the defined types, and used by variables, functions/methods and
 * classes, in the specification files.
 */
void ln_typeid_spec(void)
{
    implementation = FALSE;
    ln_typeid_types(specs->types);
    ln_typeid_vars(specs->variables);
    ln_typeid_mf(specs->functions);
    ln_typeid_class(specs->classes);
}



/*
 * Link the defined types, and used by variables, functions/methods and
 * classes, in the implementation file.
 */
void ln_typeid_impl(void)
{
    implementation = TRUE;
    ln_typeid_types(types);
    ln_typeid_vars(variables);
    ln_typeid_mf(functions);
    ln_typeid_class(classes);
}



/*
 * Initiate basic (standard) types, link the types of the C functions used by
 * Pearl and link the types in the imported specifiction and implementation
 * files.
 */
void link_typeid(void)
{
    init_basic_types();
    ln_typeid_Cfunc();
    ln_typeid_spec();
    ln_typeid_impl();
}

