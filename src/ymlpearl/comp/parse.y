%{
/*
 *            Pearl Compiler (c) 1989
 *                  by
 *          B.J. Overeinder & J.G. Stil
 *
 *              The Parser
 */

#include <stdio.h>
#include <string.h>

#include "lex_an.h"
#include "ptree.h"      /* parse tree definitions */
#include "util.h"       /* general utilities      */
#include "pearlcomp.h"



/*
 * specification and implementation filepointers, filenames and parse trees
 */
extern FILE *yyin;
extern FILE *fp_spec, *fp_impl, *yyin;
extern char file_spec[], file_impl[], *filename;
extern int  verbose;
extern int  yylineno;
extern char *yytext;
parsex      *spec, *impl;

parsex      *tmp;

int yylineno;

void yyerror(register char *s);
%}



%union {
    int     tok;        /* token returned by the lexical analyser */
    parsex  *node;      /* parse tree node            */
}



/*
 * Tokens returned by the lexical analyser.
 */
%token IF ELSE WHILE RETURN REPLY SWITCH DEFAULT FOR BLOCK BLOCKT CLASS ANY USE
%token SUBTYPE BOOLCONST STRCONST INTCONST IDENT LBRACK RBRACK LSQBRACK RSQBRACK
%token LPAR RPAR NOT COLON COMCA COMMA PERIOD SEND SENDSEND ARROW
%token BREAK SOURCE THIS STATISTICS INTEGER STRING BOOLEAN INT_D FUNCTION_BODY
%token FUNCTION_CALL CHOICE IMPL SPEC TYPE_DECL VAR_DECL FUNC_DECL
%token METHOD_DECL VAR_SPEC FUNC_SPEC METHOD_SPEC TYPE_SPEC ENUM STRUCT ARRAY
%token UMIN BOOLNOT BITNOT PARAM SEMCOL CFUNCTION_CALL ENUM_ELEM
%token FLTCONST CASTTYPE



/*
 * Non-terminals used in the yacc grammar.
 */
%type <tok>  semcol rbrack rpar optsemicomma

%type <node> program specification use_files use_file specs spec class_spec
%type <node> type_spec subtypo variable_spec function_spec method_spec binary
%type <node> unary implementation class decls decl type_decl variable_decls
%type <node> variable_decl function_decl method_decl type_def record_decls
%type <node> record_decl array_range formal_list param_specs param_spec expr
%type <node> function_body expression_list ne_expressions expression assignment
%type <node> ne_expression atomic_expr function_call send_expression parameters
%type <node> block method_set if for while switch choices choice label break
%type <node> constant id_list id string_constant error



/*
 * Precedence table.
 */
%right ASG PLUSASG MINASG MULASG DIVASG MODASG ANDASG ORASG XORASG SHLEFTASG SHRIGHTASG
%left  XORXOR
%left  OROR
%left  ANDAND
%left  LT LE EQ NE GE GT
%left  PLUS MIN OR XOR
%left  MUL DIV MOD AND
%left  SHLEFT SHRIGHT
%left  UNARYOP

%start program

%%

program:
    { if ( verbose ) printf("\nParsing specification file \"%s\"\n",
            file_spec);
      yyin = fp_spec; filename = file_spec;
      lineno(1); yylineno = 1;
      if ( verbose )
        printf("##########################################");
      if ( verbose )
        printf("######################################\n");
    }
  specification
    { spec = $2;
      yyclearin;
      if( strcmp( file_impl, EMPTY_FILE) ) {
          if ( verbose )
            printf("\nParsing implementation file \"%s\"\n",file_impl);
          yyin = fp_impl; filename = file_impl;
          lineno(1); yylineno = 1;
          if ( verbose )
            printf("##########################################");
          if ( verbose )
            printf("######################################\n");
      }
    }
  implementation
    { impl = $4;
      if ( verbose ) printf("\n\n######################################");
      if ( verbose ) printf("##########################################\n");
    }
;

specification:
    use_files
    specs
    class_spec { $$ = new_parsex(SPEC, $1, $2, $3, NULL, NULL); yyerrok; }
    | error
;

use_files:
    /* empty */             { $$ = NULL; }
    | use_file use_files    { $$ = new_parsex(COMMA, $1, $2, NULL, NULL, NULL); yyerrok; }
    | use_file error
;

use_file: USE id            { $$ = $2; addusefile( $2->str ) ; }
    | USE string_constant   { $$ = $2; addusefile( $2->str ) ; }
;

specs:
    /* empty */         { $$ = NULL; }
    | specs spec        { $$ = new_parsex(COMMA, $1, $2, NULL, NULL, NULL);
                          yyerrok; }
    | specs error
;

spec: subtypo           { $$ = $1; }
    | type_spec         { $$ = $1; }
    | variable_spec     { $$ = $1; }
    | function_spec     { $$ = $1; }
    | method_spec       { $$ = $1; }
;

class_spec:
    /* empty */         { $$ = NULL; }
    | CLASS id specs    { $$ = new_parsex(CLASS, $2, $3, NULL, NULL, NULL); }
;

type_spec:
    id ASG type_def { $$ = new_parsex(TYPE_SPEC, $1, $3, NULL, NULL, NULL); }
;

subtypo: SUBTYPE id { $$ = new_parsex(SUBTYPE, $2, NULL, NULL, NULL, NULL); }
;

variable_spec:
    id COLON id  { tmp = new_parsex(COMMA, $1, NULL, NULL, NULL, NULL);
                   $$ = new_parsex(VAR_SPEC, tmp, $3, NULL, NULL, NULL); }
    | id COMMA id_list COLON id
                 { tmp = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
                   $$ = new_parsex(VAR_SPEC, tmp, $5, NULL, NULL, NULL); }
;

function_spec:
    id COLON formal_list
                    { $$ = new_parsex(FUNC_SPEC, $1, $3, NULL, NULL, NULL); }
    | id COLON id formal_list
                    { $$ = new_parsex(FUNC_SPEC, $1, $4, $3, NULL, NULL); }
;

method_spec: id COLON formal_list ARROW id
              { $$ = new_parsex(METHOD_SPEC, $1, $3, NULL, $5, NULL); }
    | id COLON id formal_list ARROW id
              { $$ = new_parsex(METHOD_SPEC, $1, $4, $3, $6, NULL); }

;

implementation:
    decls class { $$ = new_parsex(IMPL, $1, $2, NULL, NULL, NULL); yyerrok; }
    | error
;

class:
    /* empty */             { $$ = NULL; }
    | CLASS id decls
/*        expression_list   { $$ = new_parsex(CLASS, $2, $3, $4, NULL, NULL); } */
      LBRACK variable_decls expression rbrack
            {
              if( $5 == NULL ) {
                $$ = new_parsex(CLASS, $2, $3, $6, NULL, NULL);
              } else {
                $$ = new_parsex(CLASS, $2,
                   new_parsex(COMCA, $3, $5, NULL, NULL, NULL),
                 $6, NULL, NULL);
              }
            }
;

decls:
    /* empty */     { $$ = NULL; }
    | decls decl    { $$ = new_parsex(COMMA, $1, $2, NULL, NULL, NULL);
                      yyerrok;
                    }
    | decls error
;

decl: type_decl         { $$ = $1; }
    | variable_decl     { $$ = $1; }
    | function_decl     { $$ = $1; }
    | method_decl       { $$ = $1; }
;

type_decl:
    id ASG type_def   { $$ = new_parsex(TYPE_DECL, $1, $3, NULL, NULL, NULL); }
;

variable_decls:
    /* empty */       { $$ = NULL; }
    | variable_decls variable_decl
                      { $$ = new_parsex(COMMA, $1, $2, NULL, NULL, NULL); }
;

variable_decl:
    id COLON id       { tmp = new_parsex(COMMA, $1, NULL, NULL, NULL, NULL);
                      $$ = new_parsex(VAR_DECL, tmp, $3, NULL, NULL, NULL); }
    | id COMMA id_list COLON id
            { tmp = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
              $$ = new_parsex(VAR_DECL, tmp, $5, NULL, NULL, NULL); }
    | id COLON id ASG ne_expression
            { tmp = new_parsex(COMMA, $1, NULL, NULL, NULL, NULL);
              $$ = new_parsex(VAR_DECL, tmp, $3, $5, NULL, NULL); }
    | id COMMA id_list COLON id ASG ne_expression
            { tmp = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
              $$ = new_parsex(VAR_DECL, tmp, $5, $7, NULL, NULL); }
;

function_decl:
    id COLON formal_list function_body
        { $$ = new_parsex(FUNC_DECL, $1, $3, $4, NULL, NULL); }
    | id COLON id formal_list function_body
        { $$ = new_parsex(FUNC_DECL, $1, $4, $5, $3, NULL); }
;

method_decl : id COLON formal_list ARROW id function_body
            { $$ = new_parsex(METHOD_DECL, $1, $3, $6, NULL, $5); }
        | id COLON id formal_list ARROW id function_body
            { $$ = new_parsex(METHOD_DECL, $1, $4, $7, $3, $6); }
        ;

type_def:
    id          { $$ = $1; }
    | LT id_list GT
                { $$ = new_parsex(ENUM, $2, NULL, NULL, NULL, NULL); }
    | LBRACK record_decls rbrack
                { $$ = new_parsex(STRUCT, $2, NULL, NULL, NULL, NULL); }
    | LSQBRACK array_range RSQBRACK type_def
                { $$ =new_parsex(ARRAY, $4, $2, NULL, NULL, NULL); }
;

record_decls:
    /* empty */     { $$ = NULL; }
    | record_decls optsemicomma record_decl
                    { $$ = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
                      yyerrok; }
    | record_decls error
;

record_decl:
    id_list COLON type_def
            { $$ = new_parsex(VAR_DECL, $1, $3, NULL, NULL, NULL); }
;

array_range:
    expr          { $$ = $1; }
;

formal_list:
    LPAR rpar               { $$ = NULL; }
    | LPAR param_specs rpar { $$ = $2; }
;

param_specs:
    param_spec      { $$ = new_parsex(PARAM, NULL, $1, NULL, NULL, NULL); }
    | param_specs semcol param_spec
                    { $$ = new_parsex(PARAM, $1, $3, NULL, NULL, NULL);
                      yyerrok; }
    | param_specs COMMA param_spec
                    { $$ = new_parsex(PARAM, $1, $3, NULL, NULL, NULL);
                      yyerrok; }
    | error
    | param_specs error
    | param_specs semcol error
    | param_specs COMMA error
;

param_spec:
    id              { $$ = $1; }
    | id COLON id   { tmp = new_parsex(COMMA, $1, NULL, NULL, NULL, NULL);
                      $$ = new_parsex(VAR_DECL, tmp, $3, NULL, NULL, NULL); }
    | id COMMA id_list COLON id
                    { tmp = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
                      $$ = new_parsex(VAR_DECL, tmp, $5, NULL, NULL, NULL); }
;

function_body:
    LBRACK variable_decls expression rbrack
        { $$ = new_parsex(FUNCTION_BODY, $2, $3, NULL, NULL, NULL); }
;

expression_list:
    LBRACK expression rbrack    { $$ = $2; }
;

ne_expressions:
    ne_expression   { $$ = new_parsex(COMMA, NULL, $1, NULL, NULL, NULL); }
    | ne_expressions COMMA ne_expression
                    { $$ = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
                      yyerrok; }
    | error
    | ne_expressions error
    | ne_expressions COMMA error
;

/* expression   : expr          { $$ = new_parsex(SEMCOL, NULL, $1, NULL, NULL, NULL); } */

expression:
    expr        { $$ = $1 ; }
    | expression semcol expr
                { $$ = new_parsex(SEMCOL, $1, $3, NULL, NULL, NULL); yyerrok; }
    | error
    | expression error
    | expression semcol error
;

expr:
    /* empty */         { $$ = NULL; }
    | ne_expression     { $$ = $1; }
;

ne_expression:
    constant                { $$ = $1; }
    | LPAR expression rpar  { $$ = $2; }
    | assignment            { $$ = $1; }
    | binary                { $$ = $1; }
    | unary                 { $$ = $1; }
    | atomic_expr           { $$ = $1; }
    | send_expression       { $$ = $1; }
    | function_call         { $$ = $1; }
    | block                 { $$ = $1; }
    | switch                { $$ = $1; }
    | while                 { $$ = $1; }
    | for                   { $$ = $1; }
    | break                 { $$ = $1; }
    | if                    { $$ = $1; }
;

atomic_expr:
    id          { $$ = $1; }
    | atomic_expr PERIOD id
                { $$ = new_parsex(STRUCT, $1, $3, NULL, NULL, NULL); }
    | atomic_expr LSQBRACK ne_expression RSQBRACK
                { $$ = new_parsex(ARRAY, $1, $3, NULL, NULL, NULL); }
;

function_call:
    id LPAR parameters rpar
        { $$ = new_parsex(FUNCTION_CALL, $1, $3, NULL, NULL, NULL); }
;

send_expression:
    ne_expression SENDSEND id LPAR parameters rpar
                    { $$ = new_parsex(SENDSEND, $1, $3, $5, NULL, NULL); }
    | ne_expression SEND id LPAR parameters rpar
                    { $$ = new_parsex(SEND, $1, $3, $5, NULL, NULL); }
    | REPLY LPAR expr rpar
                    { $$ = new_parsex(REPLY, $3, NULL, NULL, NULL, NULL); }
;

parameters:
    /* empty */         { $$ = NULL; }
    | ne_expressions    { $$ = $1; }
;

block:
    BLOCK LPAR rpar
                { $$ = new_parsex(BLOCK, NULL, NULL, NULL, NULL, NULL); }
    | BLOCK LPAR method_set rpar
                { $$ = new_parsex(BLOCK, NULL, $3, NULL, NULL, NULL); }
    | BLOCKT LPAR ne_expression rpar
                { $$ = new_parsex(BLOCKT, $3, NULL, NULL, NULL, NULL); }
    | BLOCKT LPAR ne_expression COMMA method_set rpar
                { $$ = new_parsex(BLOCKT, $3, $5, NULL, NULL, NULL); }
;

method_set:
    id_list     { $$ = $1; }
    | ANY       { $$ = new_parsex(ANY, NULL, NULL, NULL, NULL, NULL ) ; }
;

if:
    IF LPAR ne_expression rpar expression_list
                { $$ = new_parsex(IF, $3, $5, NULL, NULL, NULL); }
    | IF LPAR ne_expression rpar expression_list ELSE expression_list
                { $$ = new_parsex(IF, $3, $5, $7, NULL, NULL); }
    | IF LPAR ne_expression rpar expression_list ELSE if
                { $$ = new_parsex(IF, $3, $5, $7, NULL, NULL); }
;

for:
    FOR LPAR expr SEMCOL expr SEMCOL expr rpar expression_list
        { $$ = new_parsex(FOR, $3, $5, $7, $9, NULL); }
;

while:
    WHILE LPAR ne_expression rpar expression_list
        { $$ = new_parsex(WHILE, $3, $5, NULL, NULL, NULL); }
;

switch:
    SWITCH LPAR ne_expression rpar LBRACK choices rbrack
        { $$ = new_parsex(SWITCH, $3, $6, NULL, NULL, NULL); }
;

choices:
    choice              { $$ = new_parsex(COMMA, NULL, $1, NULL, NULL, NULL); }
    | choices choice    { $$ = new_parsex(COMMA, $1, $2, NULL, NULL, NULL); }
;

choice:
    label expression_list
        { $$ = new_parsex(CHOICE, $1, $2, NULL, NULL, NULL); }
;

label:
    DEFAULT         { $$ = new_parsex(DEFAULT, NULL, NULL, NULL, NULL, NULL); }
    | ne_expressions    { $$ = $1; }
;

break:
    RETURN expr     { $$ = new_parsex(RETURN, $2, NULL, NULL, NULL, NULL); }
    | BREAK expr    { $$ = new_parsex(BREAK, $2, NULL, NULL, NULL, NULL); }
;

binary:
    ne_expression SHLEFT ne_expression
                { $$ = new_parsex(SHLEFT, $1, $3, NULL, NULL, NULL); }
    | ne_expression SHRIGHT ne_expression
                { $$ = new_parsex(SHRIGHT, $1, $3, NULL, NULL, NULL); }
    | ne_expression MUL ne_expression
                { $$ = new_parsex(MUL, $1, $3, NULL, NULL, NULL); }
    | ne_expression DIV ne_expression
                { $$ = new_parsex(DIV, $1, $3, NULL, NULL, NULL); }
    | ne_expression MOD ne_expression
                { $$ = new_parsex(MOD, $1, $3, NULL, NULL, NULL); }
    | ne_expression AND ne_expression
                { $$ = new_parsex(AND, $1, $3, NULL, NULL, NULL); }
    | ne_expression PLUS ne_expression
                { $$ = new_parsex(PLUS, $1, $3, NULL, NULL, NULL); }
    | ne_expression MIN ne_expression
                { $$ = new_parsex(MIN, $1, $3, NULL, NULL, NULL); }
    | ne_expression OR ne_expression
                { $$ = new_parsex(OR, $1, $3, NULL, NULL, NULL); }
    | ne_expression XOR ne_expression
                { $$ = new_parsex(XOR, $1, $3, NULL, NULL, NULL); }
    | ne_expression LT ne_expression
                { $$ = new_parsex(LT, $1, $3, NULL, NULL, NULL); }
    | ne_expression LE ne_expression
                { $$ = new_parsex(LE, $1, $3, NULL, NULL, NULL); }
    | ne_expression EQ ne_expression
                { $$ = new_parsex(EQ, $1, $3, NULL, NULL, NULL); }
    | ne_expression NE ne_expression
                { $$ = new_parsex(NE, $1, $3, NULL, NULL, NULL); }
    | ne_expression GT ne_expression
                { $$ = new_parsex(GT, $1, $3, NULL, NULL, NULL); }
    | ne_expression GE ne_expression
                { $$ = new_parsex(GE, $1, $3, NULL, NULL, NULL); }
    | ne_expression ANDAND ne_expression
                { $$ = new_parsex(ANDAND, $1, $3, NULL, NULL, NULL); }
    | ne_expression OROR ne_expression
                { $$ = new_parsex(OROR, $1, $3, NULL, NULL, NULL); }
    | ne_expression XORXOR ne_expression
                { $$ = new_parsex(XORXOR, $1, $3, NULL, NULL, NULL); }
;

unary:
    MIN ne_expression     %prec UNARYOP
                { $$ = new_parsex(UMIN, $2, NULL, NULL, NULL, NULL); }
    | SEND ne_expression        %prec UNARYOP
                { $$ = new_parsex(BOOLNOT, $2, NULL, NULL, NULL, NULL); }
    | NOT ne_expression     %prec UNARYOP
                { $$ = new_parsex(BITNOT, $2, NULL, NULL, NULL, NULL); }
;

assignment:
    atomic_expr ASG ne_expression
                    { $$ = new_parsex(ASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr PLUSASG ne_expression
                { $$ = new_parsex(PLUSASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr MINASG ne_expression
                { $$ = new_parsex(MINASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr MULASG ne_expression
                { $$ = new_parsex(MULASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr DIVASG ne_expression
                { $$ = new_parsex(DIVASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr MODASG ne_expression
                { $$ = new_parsex(MODASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr ANDASG ne_expression
                { $$ = new_parsex(ANDASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr ORASG ne_expression
                { $$ = new_parsex(ORASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr XORASG ne_expression
                { $$ = new_parsex(XORASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr SHLEFTASG ne_expression
                { $$ = new_parsex(SHLEFTASG, $1, $3, NULL, NULL, NULL); }
    | atomic_expr SHRIGHTASG ne_expression
                { $$ = new_parsex(SHRIGHTASG, $1, $3, NULL, NULL, NULL); }
;

constant:
    INTCONST    { $$ = new_parsex(INTCONST, NULL, NULL, NULL, NULL, NULL);
                    $$->str = valconv(yytext); }
    | FLTCONST  { $$ = new_parsex(FLTCONST, NULL, NULL, NULL, NULL, NULL);
                    $$->str = strdup(yytext); }
    | BOOLCONST { $$ = new_parsex(BOOLCONST, NULL, NULL, NULL, NULL, NULL);
                    $$->str = strdup(yytext); }
    | STRCONST  { $$ = new_parsex(STRCONST, NULL, NULL, NULL, NULL, NULL);
                    $$->str = strdup(yytext); }
    | SOURCE    { $$ = new_parsex(SOURCE, NULL, NULL, NULL, NULL, NULL);
                    $$->str = strdup(yytext); }
    | THIS      { $$ = new_parsex(THIS, NULL, NULL, NULL, NULL, NULL);
                    $$->str = strdup(yytext); }
;

id_list:
    id              { $$ = new_parsex(COMMA, $1, NULL, NULL, NULL, NULL); }
    | id COMMA id_list
                    { $$ = new_parsex(COMMA, $1, $3, NULL, NULL, NULL);
                        yyerrok; }
    | error
    | id error
;

id: IDENT           { $$ = new_parsex(IDENT, NULL, NULL, NULL, NULL, NULL);
                        $$->str = strdup(yytext); }
;

string_constant:
    STRCONST        { $$ = new_parsex(STRCONST, NULL, NULL, NULL, NULL, NULL);
                        $$->str = strdup(yytext); }
;

optsemicomma:
    COMMA               { $$ = COMMA ; yyerrok; }
    | SEMCOL            { $$ = COMMA ; yyerrok; }
    | /* optional */    { $$ = COMMA ; yyerrok; }
;

semcol:
    SEMCOL              { $$ = SEMCOL; yyerrok; }
;

rbrack:
    RBRACK              { $$ = RBRACK; yyerrok; }
;

rpar:
    RPAR                { $$ = RPAR; yyerrok; }
;

%%


/*extern FILE   *yyerfp;    error stream */

extern int  nerror;



/*
 * Input position for yyparse()
 */
void yywhere() {
    register int    i;

    if (filename)
        fprintf(stderr, "\"%s\", ", filename);
    fprintf(stderr, "line %d, ", lllineno() - (*yytext == '\n' || ! *yytext));
    if (*yytext) {
    for (i = 0; i < 20; i++)
        if (! yytext[i] || yytext[i] == '\n')
        break;
    if (i)
        fprintf(stderr, "near \"%.*s\": ", i, yytext);
    }
}    /* yywhere */



/*
 * Error routine used by the parser 'yyparse()' on encountering
 * an error in the input.
 */
void yyerror(register char *s) {
    nerror += 1;
    yywhere();
    fputs(s, stderr);
    fputs(".\n", stderr);
}    /* yyerror */
