/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */

#include <string.h>

#include "greturn.h"
#include "ptree.h"
#include "iscode.h"
#include "gsimple.h"
#include "gfinit.h"
#include "generate.h"

extern int sp;
char *ret_sp;
extern char *ret_label;


/*
 * Code generation for return statement.
 */
void greturn(parsex *c)
{
    if (c) {
        if (issimple(c)) {
            icode("pp[-(%s)-4] = ", ret_sp);
            gsimple(c);
            icode(";\n");

        } else {
            generate(c);

            if (strcmp(c->tsize, "1") == 0) {
                icode("pp[-(%s)-4] = sp[%d];\n", ret_sp, sp);
            }

            else {

                icode("bcopy(sp+%d, pp-(%s)-4, %s * sizeof(intptr));\n",

                      sp, ret_sp, c->tsize);
                newbcopylist(c->tsize);
            }
        }
    }

    icode("goto R%s;\n", ret_label);
}
