/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 *
 * Generate state machine.
 */


#include "gstate.h"
#include "iscode.h"

static int state = 0;

/*
 * Increment state counter.
 */
void state_incr()
{
    state++;
}


/*
 * Return value of 'state'.
 */
int state_val()
{
    return state;
}


/*
 * Generate code for the state switch construction.
 */
void state_switch(void)
{
    icode("intptr_t *sp = env->sp;\n");
    icode("intptr_t *pp = env->pp;\n\n");

    ilabel("Start_Switch:\n");
    icode("switch (env->state) {\n");
}


/*
 * Code generation for the end sequence of the state switch construction.
 */
void state_end(void)
{
    icode("default:\n");
    icode("panic(\"Illegal state '%d' entered.\", env->state);\n");
    icode("}\n\n");

    ilabel("Freturn:\n");
    icode("env->sp = sp;");
    icode("env->pp = pp;\n");
    state = 0;
}


/*
 * Code generation for state assignment to the environment variable 'state'.
 */
void state_assign()
{
    icode("env->state = %d;\n", state);
}


/*
 * Code generation for the state case of the state switch construction.
 */
void state_case()
{
    icode("case %d:;\n", state);
}
