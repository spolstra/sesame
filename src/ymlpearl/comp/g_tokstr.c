/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

/*
 *        Pearl Compiler (c) 1989 University of Amsterdam
 *            by
 *      B.J. Overeinder & J.G. Stil
 */


#include "g_tokstr.h"
#include "ptree.h"
#include "parse.h"
#include "util.h"


/*
 * Return for several tokens the representive string.
 */
char *g_tokstr(int op)
{
    /* g_tokstr */
    switch (op) {
        case ASG:
            return ("=");

        case PLUSASG:
            return ("+=");

        case MINASG:
            return ("-=");

        case MULASG:
            return ("*=");

        case DIVASG:
            return ("/=");

        case MODASG:
            return ("%=");

        case ANDASG:
            return ("&=");

        case ORASG:
            return ("|=");

        case XORASG:
            return ("^=");

        case SHLEFTASG:
            return ("<<=");

        case SHRIGHTASG:
            return (">>=");

        case MUL:
            return ("*");

        case DIV:
            return ("/");

        case MOD:
            return ("%");

        case PLUS:
            return ("+");

        case MIN:
            return ("-");

        case AND:
            return ("&");

        case OR:
            return ("|");

        case XOR:
            return ("^");

        case ANDAND:
            return ("&&");

        case OROR:
            return ("||");

        case XORXOR:
            return ("^^");

        case LT:
            return ("<");

        case LE:
            return ("<=");

        case GT:
            return (">");

        case GE:
            return (">=");

        case EQ:
            return ("==");

        case NE:
            return ("!=");

        case BOOLNOT:
            return ("!");

        case SHLEFT:
            return ("<<");

        case SHRIGHT:
            return (">>");

        case BITNOT:
            return ("~");

        case UMIN:
            return ("-");

        case STRUCT:
            return (".");

        case SEND:
            return ("!");

        case SENDSEND:
            return ("!!");

        default:
            warning("%d is not a legal token.\n", op);
            return ("????");
    }
}       /* g_tokstr */
