/* Analysis functions */

#ifndef ANALYSIS_H
#define ANALYSIS_H

#include "symboldef.h"

void reply_analysis(symboltable *classes, symboltable *functions,
                    symboltable *variables);

#endif // TYPE_CHECK_H
