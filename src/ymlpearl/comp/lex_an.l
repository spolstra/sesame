%{
/*
 * 			  Pearl Compiler (c) 1988 
 *				    by
 *	  	        B.J. Overeinder & J.G. Stil
 *
 * 			   The LEXical ANalyser.
 */

#include "ptree.h"
#include "parse.h"

#include <stdarg.h>

#define YY_NO_INPUT

extern int verbose;

void dolf(char *str);
%}



D		[0-9]
O		[0-7]
B		[0-1]
H		[a-fA-F0-9]
L		[a-zA-Z]

%option nounput


%%



"if"			{ if ( verbose ) ECHO; return( IF ); }
"else"			{ if ( verbose ) ECHO; return( ELSE ); }
"while"			{ if ( verbose ) ECHO; return( WHILE ); }
"return"		{ if ( verbose ) ECHO; return( RETURN ); }
"reply"			{ if ( verbose ) ECHO; return( REPLY ); }
"break"			{ if ( verbose ) ECHO; return( BREAK ); }
"switch"		{ if ( verbose ) ECHO; return( SWITCH ); }
"default"		{ if ( verbose ) ECHO; return( DEFAULT ); }
"for"			{ if ( verbose ) ECHO; return( FOR ); }
"any"			{ if ( verbose ) ECHO; return( ANY ); }
"block"			{ if ( verbose ) ECHO; return( BLOCK ); }
"blockt"		{ if ( verbose ) ECHO; return( BLOCKT ); }
"class"			{ if ( verbose ) ECHO; return( CLASS ); }
"use"			{ if ( verbose ) ECHO; return( USE ); }
"subtypeof"		{ if ( verbose ) ECHO; return( SUBTYPE ); }
"true"			{ if ( verbose ) ECHO; return( BOOLCONST ); }
"false"			{ if ( verbose ) ECHO; return( BOOLCONST ); }
"source"		{ if ( verbose ) ECHO; return( SOURCE ); }
"this"		        { if ( verbose ) ECHO; return( THIS ); }

\"(\\(.|\n)|[^\\\n"])*\"	{ if ( verbose ) ECHO; return( STRCONST ); }

0[bB]{B}+		{ if ( verbose ) ECHO; return( INTCONST ); }
0[oO]{O}+		{ if ( verbose ) ECHO; return( INTCONST ); }
{D}+			{ if ( verbose ) ECHO; return( INTCONST ); }
0[xX]{H}+		{ if ( verbose ) ECHO; return( INTCONST ); }
{D}+\.{D}+((e|E)[+-]{D}+)?	{ if ( verbose ) ECHO; return( FLTCONST ); }

"{"			{ if ( verbose ) ECHO; return( LBRACK ); }
"}"			{ if ( verbose ) ECHO; return( RBRACK ); }
"["			{ if ( verbose ) ECHO; return( LSQBRACK ); }
"]"			{ if ( verbose ) ECHO; return( RSQBRACK ); }
"("			{ if ( verbose ) ECHO; return( LPAR ); }
")"			{ if ( verbose ) ECHO; return( RPAR ); }
"*"			{ if ( verbose ) ECHO; return( MUL ); }
"/"			{ if ( verbose ) ECHO; return( DIV ); }
"%"			{ if ( verbose ) ECHO; return( MOD ); }
"&"			{ if ( verbose ) ECHO; return( AND ); }
"+"			{ if ( verbose ) ECHO; return( PLUS ); }
"-"			{ if ( verbose ) ECHO; return( MIN ); }
"|"			{ if ( verbose ) ECHO; return( OR ); }
"^"			{ if ( verbose ) ECHO; return( XOR ); }
"<"			{ if ( verbose ) ECHO; return( LT ); }
"<="			{ if ( verbose ) ECHO; return( LE ); }
"=="			{ if ( verbose ) ECHO; return( EQ ); }
"!="			{ if ( verbose ) ECHO; return( NE ); }
">"			{ if ( verbose ) ECHO; return( GT ); }
">="			{ if ( verbose ) ECHO; return( GE ); }
"~"			{ if ( verbose ) ECHO; return( NOT ); }
":"			{ if ( verbose ) ECHO; return( COLON ); }
";"			{ if ( verbose ) ECHO; return( SEMCOL ); }
","			{ if ( verbose ) ECHO; return( COMMA ); }
"."			{ if ( verbose ) ECHO; return( PERIOD ); }
"!"			{ if ( verbose ) ECHO; return( SEND ); }
"!!"			{ if ( verbose ) ECHO; return( SENDSEND ); }
"->"			{ if ( verbose ) ECHO; return( ARROW ); }
"||"			{ if ( verbose ) ECHO; return( OROR ); }
"&&"			{ if ( verbose ) ECHO; return( ANDAND ); }
"^^"			{ if ( verbose ) ECHO; return( XORXOR ); }
"<<"			{ if ( verbose ) ECHO; return( SHLEFT ); }
">>"			{ if ( verbose ) ECHO; return( SHRIGHT ); }
"="			{ if ( verbose ) ECHO; return( ASG ); }
"+="			{ if ( verbose ) ECHO; return( PLUSASG ); }
"-="			{ if ( verbose ) ECHO; return( MINASG ); }
"*="			{ if ( verbose ) ECHO; return( MULASG ); }
"/="			{ if ( verbose ) ECHO; return( DIVASG ); }
"%="			{ if ( verbose ) ECHO; return( MODASG ); }
"&="			{ if ( verbose ) ECHO; return( ANDASG ); }
"|="			{ if ( verbose ) ECHO; return( ORASG ); }
"^="			{ if ( verbose ) ECHO; return( XORASG ); }
"<<="			{ if ( verbose ) ECHO; return( SHLEFTASG ); }
">>="			{ if ( verbose ) ECHO; return( SHRIGHTASG ); }
({L}|_)({L}|{D}|_)*	{ if ( verbose ) ECHO; return( IDENT ); }
#.*\n			{ dolf( yytext ) ; }
[ \t\f]                 { if ( verbose ) ECHO; /* ignore layout characters */ }
\n			{ if ( verbose ) ECHO; lineno(0); }
.                       { if ( verbose ) ECHO; return(yytext[0]); }



%%



#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdarg.h>
#include "lex_an.h"
#include "util.h"



int		nerror = 0, nwarning = 0;
extern bool coredump ;

char		*filename;		/* current filename */
static linex	*lf;			/* current filename and linenumber */



/*
 *
 */
int yywrap() {
    return( 1 );
}


void dolf(char *str) {
  int line ;
  static char file[ 128 ] ;
  sscanf( str, "# %d \"%[^\"]\"", &line, file ) ;
  if ( !(strstr(filename, ".pi") || strstr(filename, ".ps")) )
      filename = file;
  lineno( line ) ;
}

/*
 * Creates a new linex node referenced by 'lf', with current filename 
 * and linenumber.
 */
void lineno(int i) {
    if (i == 0) {
	i = lf->line + 1;
    }
    lf = (linex *)malloc(sizeof(linex));
    lf->file = strdup(filename);
    lf->line = i;
}	 /* lineno */

int lllineno() {
  return lf->line ;
}


/*
 * Set 'lf' to 'l'.
 */
void setlf(linex *l) {
    lf = l;
}	 /* setlf */



/* 
 * Return 'lf'.
 */
linex *getlf() {
    return(lf);
}	 /* getlf */



/*
 * Error function with variable argument list, according to the 
 * definitions in include file <varargs.h>.
 */


void error(char *fmt, ...)
{
    va_list argp;

    if (lf)
       fprintf(stderr, "\"%s\", line %d: ", lf->file, lf->line);

    fprintf(stderr, "error: ");
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    fprintf(stderr, "\n");

    nerror += 1;
}



/*
 * Error function with variable argument list, according to the 
 * definitions in include file <varargs.h>.
 */
void warning(char *fmt, ...) {
    va_list argp;

    if (lf)
       fprintf(stderr, "\"%s\", line %d: ", lf->file, lf->line);

    fprintf(stderr, "warning: ");
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    fprintf(stderr, "\n");

    nwarning += 1;
}



/*
 * Error function with variable argument list, according to the 
 * definitions in include file <varargs.h>.
 */
void panic(char *fmt, ...) {
    va_list argp;
    fprintf(stderr, "panic: ");
    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
    fprintf(stderr, "\n");

    exit( 1 ) ;
}
