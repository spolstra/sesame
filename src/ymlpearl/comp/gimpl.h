/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef GIMPL_H
#define GIMPL_H

#include "symboldef.h"

void gglob_types(symboltable *types);
void gclass_types(symboltable *types);
void gmeth_labels(symboltable *classes);
void gclasses(symboltable *classes);
void gmodule(char *name);
void gsimple_init(parsex *code);
void glocal_vars(symboltable *vars, int func);
void gglob_vars(symboltable *vars);
void gglob_vars_impl(symboltable *vars);
void gimpl(char *filename);

#endif // GIMPL_H
