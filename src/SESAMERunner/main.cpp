/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "../BasicUtils/BasicException.h"
#include "../BasicUtils/BasicLockable.h"
#include "../BasicUtils/BasicProcess.h"
#include "../BasicUtils/BasicThread.h"

using namespace std;

BasicLockable outputLock;

class Pumper : public BasicThread
{
        FILE *file;
        string prefix;

    public:
        Pumper(FILE *file, string prefix = "") : file(file), prefix(prefix) {}
        virtual ~Pumper() {}

        virtual void run() {
            char buf[256];

            while (fgets(buf, 256, file) && !shutdown) {
                outputLock.lock();
                cout << prefix << buf << flush;
                outputLock.unlock();
            }
        }
};

void printCommand(const list<string> argv)
{
    cout << "#";

    for (list<string>::const_iterator i = argv.begin(); i != argv.end(); i++) {
        cout << " " << (*i);
    }

    cout << endl;
}

void Syntax(const string name)
{
    cerr << "Syntax: " << name
         << " <arch simulator> <app yml> <map yml> <arch yml>" << endl;
}

int main(int argc, char *argv[])
{
    BasicDebugger::initStackTrace(argv[0]);

    if (argc < 5) {
        Syntax(argv[0]);
        exit(1);
    }

    try {
        const string appPrefix = "app: ";
        const string archPrefix = "arch: ";


        // Check files
        ifstream file;

        for (int i = 0; i < 3; i++) {
            file.open(argv[i + 2]);
            ASSERT_OR_THROW(string("Could not open file '") + argv[i + 2] + "'!",
                            file.is_open());
            file.close();
        }

        const string archsim = argv[1];
        const string appfile = argv[2];
        const string mapfile = argv[3];
        const string archfile = argv[4];

        list <string> pArgv;

        // Execute application simulation
        pArgv.push_back("PNRunner");
        pArgv.push_back("-m");
        pArgv.push_back(mapfile);
        BasicProcess::parseArgs(getenv("APPFLAGS"), pArgv);
        pArgv.push_back(appfile);

        BasicProcess appProc;
        BasicPipe *appOutPipe = appProc.getChildPipe(BasicProcess::FROM_CHILD, 1);
        appProc.replaceChildFD(2, 1);
        printCommand(pArgv);
        appProc.exec(pArgv);


        // Look for mapping key
        const string keyTag = "Mapping key is ";
        string key;

        FILE *appOutFile = fdopen(appOutPipe->getOutFD(), "r");
        char buf[256];

        while (fgets(buf, 256, appOutFile)) {
            cout << appPrefix << buf << flush;

            if (keyTag.compare(0, keyTag.size(), buf, keyTag.size()) == 0) {
                for (char *ptr = &buf[keyTag.size()]; isdigit(*ptr); ptr++) {
                    key.push_back(*ptr);
                }

                break;
            }
        }

        ASSERT_OR_THROW("Failed to parse mapping key!",
                        !key.empty() && atoi(key.c_str()) > 0);


        // Execute architecture simulation
        pArgv.clear();
        pArgv.push_back(archsim);
        pArgv.push_back("-m");
        pArgv.push_back(mapfile);
        pArgv.push_back("-K");
        pArgv.push_back(key);
        BasicProcess::parseArgs(getenv("ARCHFLAGS"), pArgv);
        pArgv.push_back(archfile);

        BasicProcess archProc;
        BasicPipe *archOutPipe =
            archProc.getChildPipe(BasicProcess::FROM_CHILD, 1);
        archProc.replaceChildFD(2, 1);
        FILE *archOutFile = fdopen(archOutPipe->getOutFD(), "r");
        printCommand(pArgv);
        archProc.exec(pArgv);


        // Pump application output
        Pumper pumpApp(appOutFile, appPrefix);
        pumpApp.setRunning(true);
        pumpApp.start();

        // Pump architecture output
        Pumper pumpArch(archOutFile, archPrefix);
        pumpArch.setRunning(true);
        pumpArch.start();


        // Watch process status
        try {
            while (appProc.isRunning() || archProc.isRunning()) {

                if (!appProc.isRunning() && appProc.getReturnCode()) {
                    outputLock.lock();
                    cerr << "Application process returned " << appProc.getReturnCode()
                         << ", killing architecture process!" << endl;
                    outputLock.unlock();
                    archProc.kill(1);
                    break;
                }

                if (!archProc.isRunning() && archProc.getReturnCode()) {
                    outputLock.lock();
                    cerr << "Architecture process returned " << archProc.getReturnCode()
                         << ", killing application process!" << endl;
                    outputLock.unlock();
                    appProc.kill(1);
                    break;
                }

                usleep(100);
            }
        } catch (BasicException &e) {
            cerr << "Badness " << e << endl;
        } catch (...) {
            cerr << "Unknown Badness" << endl;
        }

        // Wait for children to exit
        if (appProc.isRunning()) {
            appProc.wait();
        }

        if (archProc.isRunning()) {
            archProc.wait();
        }

        pumpApp.join();
        pumpArch.join();

        return appProc.getReturnCode() | archProc.getReturnCode();

    } catch (BasicException &e) {
        outputLock.lock();
        cerr << e << endl;
        outputLock.unlock();
    }

    return 1;
}
