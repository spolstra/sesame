/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string>

#include "../BasicUtils//BasicException.h"

#include "../libyml/ymlloader.h"
#include "../libyml/defaultymlentityfactory.h"
#include "../libyml/perlscriptinterpreter.h"

#include "../libyml/ymlentity.h"

#include "../libpn/pntypeinfogen.h"

using namespace std;

void Syntax(char *name)
{
    cout << endl;
    cout << "Compile Date: " << __DATE__ << " at " << __TIME__ << endl << endl;
    cout << "Syntax: " << name << " <options> <YML file>" << endl;

    cout << "\t-l <lang>   \tlanguage" << endl;
    cout << "\t-o <file>   \toutput file" << endl;
    cout << "\t\tAvailable languages:" << endl;
    cout << "\t\tc       \tC stub" << endl;
    cout << "\t\tc++     \tC++ stub" << endl;
    cout << "\t-u <file>   \tuser include file" << endl;
    exit(1);
}

int main(int argc, char *argv[], char ** /*env*/)
{
    try {

        string lang = "c++";
        string userInclude;
        string outputfile;

        int c;

        while ((c = getopt(argc, argv, "l:o:u:")) != -1) {
            switch (c) {
                case 'l':
                    lang = optarg;
                    break;

                case 'o':
                    outputfile = optarg;
                    break;

                case 'u':
                    userInclude = optarg;
                    break;

                case '?':
                default:
                    Syntax(argv[0]);
                    break;
            }
        }

        string ymlfile;

        if (optind < argc) {
            ymlfile = argv[optind++];
        } else {
            cerr << "Missing YML file." << endl;
            Syntax(argv[0]);
        }

        DefaultYMLEntityFactory factory;
        Interpreter *interpreter = PerlScriptInterpreter::getSingleton();
        interpreter->start();
        YMLLoader loader(&factory, interpreter);
        YMLEntity *entity = (YMLEntity *)loader.parse(ymlfile);
        interpreter->stop();
        ASSERT_OR_THROW("Expected yml network type!",
                        entity->getType() == ymlNetwork);

        if (lang == "c") {
        } else if (strcasecmp(lang.c_str(), "c++") == 0) {

            if (!outputfile.empty()) {
                ofstream output(outputfile.c_str());

                if (!output.is_open()) {
                    cerr << "Error opening '" << outputfile << "' for output!" << endl;
                    exit(1);
                }

                dumpCPPTypeImplementation((YMLNetwork *)entity, output,
                                          userInclude);

                output.close();

            } else {
                dumpCPPTypeImplementation((YMLNetwork *)entity, cout,
                                          userInclude);
            }
        } else {
            cerr << "Unrecgonized langauge '" << lang << "'" << endl;
            Syntax(argv[0]);
        }

        return 0;

    } catch (BasicException &e) {
        cerr << "Exception: " << e << endl;
    }

    return 1;
}
