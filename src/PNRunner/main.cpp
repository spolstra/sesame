/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <iostream>
#include <list>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../BasicUtils//BasicException.h"
#include "../BasicUtils//BasicProcess.h"
#include "../BasicUtils//BasicDebugger.h"
#include "../BasicUtils//BasicString.h"
#include "../BasicUtils//BasicLockable.h"

#include "../libymlmap/sharedmemmapping.h"
#include "../libymlmap/msgqmuxmapping.h"
#include "../libymlmap/filemapping.h"
#include "../libymlmap/tracecontext.h"

#include "../libpn/pn.h"
#include "../libyml/ymlloader.h"
#include "../libpn/pnymlentityfactory.h"
#include "../libbasic/libmanager.h"

using namespace std;

void Syntax(char *name)
{
    cerr << endl
         << "PNRunner" << endl
         << "Joe Coffland and Simon Polstra" << endl
         << "University of Amsterdam" << endl
         << "Compile Date: " << __DATE__ << " at " << __TIME__ << endl << endl
         << "Syntax: " << name << " [OPTION]... <YML file> [variable=value]..."
         << endl << endl
         << "\t-C <directory>     \tChange to directory before running." << endl
         << "\t-K <mapping key>   \tSpecify mapping key." << endl
         << "\t-L <path>          \tAdd path to the library search path." << endl
         << "\t-l <library>       \tLoad specified application library." << endl
         << "\t-t                 \tLoad only.  Don't run the network." << endl
         << "\t-m <YML mapping>   \tOutput traces with supplied mapping." << endl
         << "\t-P <# seconds>     \tPrint process state every # of seconds."
         << endl
         << "\t-p                 \tPrint network." << endl
         << "\t-q                 \tSupress interpreter output. (except errors)"
         << endl
         << "\t-S                 \tUse shared memory for mapping." << endl
         << "\t-T <basename>      \tUse file mapping.  Trace files are " << endl
         << "\t                   \t  written as basename<process id>." << endl
         << "\t-x                 \tPrint full stack trace on errors." << endl
         << endl << endl;
    exit(1);
}

// This lock is used by the debug code in the yapi read/write methods in
// ioconnections.h. If an application model is compiled with -DDEBUG all
// reads and writes are logged to stderr.
BasicLockable g_debug_output_lock = BasicLockable();

int main(int argc, char *argv[], char ** /*env*/)
{
    try {
        list<string> ppArgs;
        list<string> libs;

        // Process Arguments
        mapping_t mappingType = M_MSGQ;

        key_t mappingKey = 0;
        string traceBaseName;
        string mappingFileName;

        int printState = 0;
        bool printNetwork = false;
        bool loadOnly = false;
        string changeToDir;
        int c;

        while ((c = getopt(argc, argv, "C:K:L:l:tm:P:pqST:x")) != -1) {
            switch (c) {
                case 'C':
                    changeToDir = optarg;
                    break;

                case 'K':
                    mappingKey = atoi(optarg);
                    break;

                case 'L':
                    LibManager::addSearchPath(optarg);
                    break;

                case 'l':
                    libs.push_back(optarg);
                    break;

                case 't':
                    loadOnly = true;
                    break;

                case 'm':
                    mappingFileName = optarg;
                    break;

                case 'P':
                    printState = atoi(optarg);
                    break;

                case 'p':
                    printNetwork = true;
                    break;

                case 'q':
                    ppArgs.push_front("-q");
                    break;

                case 'S':
                    mappingType = M_SHM;
                    break;

                case 'T':
                    traceBaseName = optarg;
                    mappingType = M_FILE;
                    break;

                case 'x':
                    BasicDebugger::initStackTrace(argv[0]);
                    ppArgs.push_back("-x");
                    break;

                default:
                    Syntax(argv[0]);
                    break;
            }
        }

        // Filename argument
        string fileName;

        if (optind < argc) {
            fileName = argv[optind++];
        } else {
            cerr << "Missing 'YML file' argument." << endl;
            Syntax(argv[0]);
        }

        ppArgs.push_back(fileName);

        // Variable definition arguments
        for (; optind < argc; optind++) {
            char *ptr = strchr(argv[optind], '=');

            if (!ptr) {
                cerr << "Unrecognized variable argument \'";
                cerr << argv[optind] << "'." << endl;
                Syntax(argv[0]);
            }

            ppArgs.push_back(argv[optind]);
        }

        // Add yml file directory to library search path
        if (fileName.rfind("/") != fileName.npos) {
            LibManager::addSearchPath(fileName.substr(0, fileName.rfind("/")));
        }

        // Load mapping
        Mapping *mapping = NULL;

        if (!mappingFileName.empty()) {
            switch (mappingType) {
                case M_MSGQ: {
                    MsgQMUXMapping *msgqmuxmapping = new MsgQMUXMapping(mappingFileName, SOURCE_SIDE);

                    if (!msgqmuxmapping->open(mappingKey)) {
                        THROW("Opening message queue mapping!");
                    }

                    cout << "Mapping key is " << msgqmuxmapping->getKey() << endl;
                    mapping = msgqmuxmapping;
                }
                break;

                case M_SHM: {
                    SharedMemMapping *shmmapping = new SharedMemMapping(mappingFileName, SOURCE_SIDE);

                    mappingKey = shmmapping->openKeyChannel(mappingKey);
                    cout << "Mapping key is " << mappingKey << endl;

                    if (!shmmapping->open()) {
                        THROW("Opening shared memory mapping!");
                    }

                    mapping = shmmapping;
                }
                break;

                case M_FILE: {
                    FileMapping *fileMapping = new FileMapping(mappingFileName, SOURCE_SIDE);

                    if (!fileMapping->open(traceBaseName)) {
                        THROW("Failed to open file mapping!");
                    }

                    mapping = fileMapping;
                }
                break;
            }
        }


        // Spawn preprocessor
        BasicProcess ppProc;
        BasicPipe *ppPipe = ppProc.getChildPipe(BasicProcess::FROM_CHILD);
        ppArgs.push_front(BasicString(ppPipe->getInFD()));
        ppArgs.push_front("-p");
        ppArgs.push_front("ymlpp");

        istream *ppPipeStream = ppPipe->getOutStream();
        ppProc.exec(ppArgs);


        // Change directory if necessary
        char cwd[512];

        if (!changeToDir.empty()) {
            if (!getcwd(cwd, 512)) {
                THROW("Could not get current working directory!");
            }

            if (chdir(changeToDir.c_str())) {
                THROW(string("Could not change to directory '") + changeToDir + "'!");
            }
        }


        // Load preprocessed YML
        TraceContext *traceContext = 0;

        if (mapping != NULL) {
            ASSERT_OR_THROW("Empty trace context mapping!",
                            mapping->getTraceContext());
            traceContext = mapping->getTraceContext();
        }

        PNYMLEntityFactory factory;
        YMLLoader loader(&factory, NULL);
        PN *pn = (PN *)loader.parse(*ppPipeStream, traceContext);

        // Load command line libraries
        list<string>::iterator lit;

        for (lit = libs.begin(); lit != libs.end(); lit++) {
            pn->getLibManager()->loadLibrary(lit->c_str());
        }

        ppProc.wait();

        if (printNetwork) {
            cout << *pn << endl;
        }

        // Run it!
        if (!loadOnly) {
            pn->start();

            while (printState && pn->isRunning()) {
                char timeBuf[32];
                time_t now = time(NULL);
                strftime(timeBuf, 32, "%T", gmtime(&now));

                cerr << "<state time=\"" << timeBuf << "\">" << endl;
                pn->printState(cerr, 2);
                cerr << "</state>" << endl;
                sleep(printState);
            }

            pn->join();
        }


        // Change directory back
        if (!changeToDir.empty() && (chdir(cwd))) {
            THROW(string("Could not change to directory '") + changeToDir + "'!");
        }


        if (mapping != NULL) {
            mapping->close();
            delete mapping;
        }

        delete pn;

        return 0;

    } catch (BasicException &e) {
        cerr << e << endl;
    }

    return 1;
}
