/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICMUXMESSAGE_H
#define BASICMUXMESSAGE_H

#include <string.h>

class BasicMUXMessageBase
{
    protected:
        typedef struct {
            long type;
            long key;
            char *data;
        } message_t;

        size_t size;
        message_t *msg;

        BasicMUXMessageBase() : size(0), msg(NULL) {
        }

    public:
        BasicMUXMessageBase(const void *data, size_t size) : size(size) {
            msg = new message_t;
            msg->data = new char[size];

            if (data) {
                memcpy(msg->data, data, size);
            }
        }

        BasicMUXMessageBase(size_t size) : size(size) {
            msg = new message_t;
            msg->data = new char[size];
        }

        ~BasicMUXMessageBase() {
            if (msg) {
                delete msg->data;
                delete msg;
            }
        }

        size_t getSize() {
            return size;
        }
        void *getData() {
            return msg->data;
        }

        friend class BasicMUXChannelBase;
        friend class BasicMsgQueueMUX;
};

template<class T> class BasicMUXChannel;

template<class T>
class BasicMUXMessage : public BasicMUXMessageBase
{
    public:
        BasicMUXMessage() {
            size = sizeof(T);
            msg = new message_t;
            msg->data = new char[size];
        }
        BasicMUXMessage(T &data) {
            size = sizeof(T);
            msg = (message_t *)new char[sizeof(message_t) + size];
            (*(T *)msg->data) = data;
        }

        T &operator->() {
            return *(T *)getData();
        }
        T &getDataRef() {
            return *(T *)getData();
        }

        friend class BasicMUXChannel<T>;
};
#endif
