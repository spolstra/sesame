/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICIPCMSGQUEUE_H
#define BASICIPCMSGQUEUE_H

#include <sys/types.h>

#include "basiccleanuphandler.h"

class BasicIPCMsgQueue
{
        key_t key;
        int id;
        bool amOwner;

        BasicCleanupFunctor<BasicIPCMsgQueue> cleanupFunctor;

    public:
        BasicIPCMsgQueue();
        virtual ~BasicIPCMsgQueue();

        virtual bool open(key_t key, const unsigned int mode = 0666);
        virtual key_t create(key_t key, const unsigned int mode = 0666);
        virtual void close();

        virtual bool isOpen();
        virtual bool isOwner() {
            return amOwner;
        }

        virtual bool send(const void *msgp, const size_t size, const bool blocking = true);
        virtual size_t receive(void *msgp, const size_t size, const long type, const bool blocking = true);
};
#endif
