/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "basicsemaphore.h"

#include <sys/types.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>

#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
/* union semun is defined by including <sys/sem.h> */
#elif defined(__APPLE_CC__)
/* Snow leopard also defined union semun */
#else
/* according to X/OPEN we have to define it ourselves */
union semun {
    int val;                  /* value for SETVAL */
    struct semid_ds *buf;     /* buffer for IPC_STAT, IPC_SET */
    unsigned short *array;    /* array for GETALL, SETALL */
    /* Linux specific part: */
    struct seminfo *__buf;    /* buffer for IPC_INFO */
};
#endif


BasicSemaphore::BasicSemaphore() : amOwner(false), isLocked(false),
    id(0), key(0)
{
    srand(time(NULL));
}

BasicSemaphore::~BasicSemaphore()
{
    if (isConnected()) {
        destroy();
    }
}

bool BasicSemaphore::isConnected()
{
    return id != 0;
}

key_t BasicSemaphore::create(key_t _key)
{
    key = _key;

    while (true) {
        if (!_key) {
            key = (key_t)rand();
        }

        if ((id = semget(key, 1, 0666 | IPC_CREAT | IPC_EXCL)) == -1) {
            if (errno == EEXIST) {
                continue;
            }

            perror("BasicSemaphore::create() semget");
            key = 0;
            id = 0;
            return 0;
        }

        break;
    }

    // initialize semaphore #0 to 1
    union semun arg;
    arg.val = 1;

    if (semctl(id, 0, SETVAL, arg) == -1) {
        perror("BasicSemaphore::create() semctl");
        key = 0;
        return -1;
    }

    amOwner = true;
    return key;
}

void BasicSemaphore::destroy()
{
    if (id && amOwner) {
        if (semctl(id, 0, IPC_RMID) == -1) {
            perror("BasicSemaphore::destroy() semctl");
        }
    }

    amOwner = false;
    id = 0;
    key = 0;
}

int BasicSemaphore::open(key_t _key)
{
    if ((id = semget(_key, 0, 0)) == -1) {
        perror("BasicSemaphore::open() semget");
        id = 0;
        return 0;
    }

    key = _key;
    return key;
}


bool BasicSemaphore::tryLock()
{
    struct sembuf sb = {0, -1, SEM_UNDO | IPC_NOWAIT};

    if (semop(id, &sb, 1) == -1) {
        if (errno != EAGAIN) {
            perror("BasicSemaphore::lock() semop");
        }

        return false;
    }

    return true;
}

void BasicSemaphore::lock()
{
    struct sembuf sb = {0, -1, SEM_UNDO};

    if (semop(id, &sb, 1) == -1) {
        perror("BasicSemaphore::lock() semop");
    }
}

void BasicSemaphore::unlock()
{
    struct sembuf sb = {0, 1, SEM_UNDO};

    if (semop(id, &sb, 1) == -1) {
        perror("BasicSemaphore::unlock() semop");
    }
}

#ifdef BASICSEMAPHORE_TEST
#include <iostream>
#include <unistd.h>

int main(int argc, char **argv)
{
    BasicSemaphore sem;

    if (argc < 2) {
        key_t key = sem.create(0);
        cout << "Created sem key = " << key << "." << endl;

        while (true) {
            sem.lock();
            cout << "Server Locked" << endl;
            sleep(5);
            sem.unlock();
            cout << "Server Unlocked" << endl;
            sleep(5);
        }

    } else {
        key_t key = atoi(argv[1]);

        if (key = sem.open(key)) {
            cout << "Opened sem key = " << key << endl;
        } else {
            cerr << "Failed to open sem." << endl;
            return 1;
        }

        while (true) {
            while (!sem.tryLock()) {
                cout << "Client trying" << endl;
                sleep(1);
            }

            cout << "Client Locked" << endl;
            sleep(1);
            sem.unlock();
            cout << "Client Unlocked" << endl;
            sleep(1);
        }
    }
}
#endif
