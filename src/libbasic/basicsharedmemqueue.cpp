/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "basicsharedmemqueue.h"

#include "../BasicUtils/BasicException.h"

#include <sys/shm.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <string>

#define MIN(x, y) (x < y ? x : y)
#define MAX(x, y) (x > y ? x : y)
#define FMOD(x,y) (x < y ? x : (x) - (y))
//#define FMOD(x,y) ((x) % (y))
#define BUF_SPACE (FMOD((control->size - control->write) + control->read - 1, control->size))
#define BUF_FILLED (FMOD((control->size - control->read) + control->write, control->size))

#define SEM_LOCK if (control && control->host_attached) sem.lock();
#define SEM_UNLOCK if (control && control->host_attached) sem.unlock();


BasicSharedMemQueue::BasicSharedMemQueue() :
    key(0),  amOwner(false), control(NULL),
    cleanupFunctor(this, &BasicSharedMemQueue::close)
{
    srand(time(NULL));

    BasicCleanupHandler::initialize();
}

BasicSharedMemQueue::~BasicSharedMemQueue()
{
    if (isOpen()) {
        close();
    }
}

bool BasicSharedMemQueue::isConnected()
{
    if (!isOpen()) {
        return false;
    }

    if (!amOwner) {
        return true;
    }

    return control->client_attached;
}

bool BasicSharedMemQueue::isOpen()
{
    if (!control || !id) {
        return false;
    }

    return true;
}

bool BasicSharedMemQueue::isEmpty()
{
    if (!isOpen()) {
        return true;
    }

    SEM_LOCK;
    bool empty = control->read == control->write;
    SEM_UNLOCK;
    return empty;
}

bool BasicSharedMemQueue::isFull()
{
    if (!isOpen()) {
        return true;
    }

    SEM_LOCK;
    bool full = control->read == (control->write + 1) % control->size;
    SEM_UNLOCK;
    return full;
}

key_t BasicSharedMemQueue::open(int _key)
{
    lock();

    if (isOpen()) {
        unlock();
        return key;
    }

    key = _key;

    // Open shared mem buffer
    if ((id = shmget(key, 0, 0)) >= 0) {

        // Map shared mem buffer
        if ((control = (control_t *)shmat(id, 0, 0))) {

            // Check control structure
            if (control->magic == BasicSharedMemQueueMagic) {

                // Open semaphore
                if (sem.open(control->sem_key)) {

                    SEM_LOCK;

                    if (!control->client_attached) {
                        control->client_attached = 1;
                        SEM_UNLOCK;
                        BasicCleanupHandler::regCleanup(&cleanupFunctor);
                        unlock();
                        return key;

                    } else {
                        fprintf(stderr, "BasicSharedMemQueue::open() client already attached.\n");
                    }

                    SEM_UNLOCK;

                }

            } else {
                fprintf(stderr, "BasicSharedMemQueue::open() invalid shared buffer or wrong version.\n");
            }

            if (shmdt((char *)control) == -1) {
                perror("BasicSharedMemQueue::close() shmdt");
            }

            control = NULL;
        } else {
            perror("BasicSharedMemQueue::open() shmat");
        }

        id = 0;
    } else {
        char keyStr[11];
        sprintf(keyStr, "%x", (unsigned int) key);
        std::string errStr = "BasicSharedMemQueue::open() key = ";
        errStr += keyStr;
        errStr += " shmget";
        perror(errStr.c_str());
    }

    key = 0;

    unlock();
    return 0;
}

void BasicSharedMemQueue::close()
{
    lock();

    if (!isOpen()) {
        unlock();
        return;
    }

    // Release client
    //if (!amOwner)
    //  control->client_attached = 0;

    // Detatch host
    if (amOwner) {
        control->host_attached = 0;
    }

    // Unmap shared buffer
    if (shmdt((char *)control) == -1) {
        perror("BasicSharedMemQueue::close() shmdt");
    }

    control = NULL;

    if (amOwner) {

        // Destroy shared buffer
        if (shmctl(id, IPC_RMID, NULL) == -1) {
            perror("BasicSharedMemQueue::close() shmctl");
        }

        // Destroy semaphore
        sem.destroy();

        amOwner = false;
    }

    id = 0;
    key = 0;

    BasicCleanupHandler::unregCleanup(&cleanupFunctor);
    unlock();
}

key_t BasicSharedMemQueue::create(key_t _key, long size)
{
    lock();

    if (isOpen()) {
        unlock();
        return key;
    }

    key = _key;

    // Create semaphore
    key_t sem_key;

    if ((sem_key = sem.create(key ? key + 1 : 0))) {

        // Create shared mem buffer
        while (true) {
            if (!_key) {
                key = (key_t)rand();
            }

            if ((id = shmget(key, sizeof(control_t) + size + 1, 0666 | IPC_CREAT | IPC_EXCL)) >= 0) {

                // Map shared mem buffer
                if ((control = (control_t *)shmat(id, 0, 0))) {

                    // Initialize control structure
                    control->sem_key = sem_key;
                    control->size = size + 1;
                    control->space = size;
                    control->filled = 0;
                    control->read = control->write = 0;
                    control->host_attached = 1;
                    control->client_attached = 0;
                    control->magic = BasicSharedMemQueueMagic;

                    amOwner = true;
                    BasicCleanupHandler::regCleanup(&cleanupFunctor);
                    unlock();

                    return key;

                } else {
                    perror("BasicSharedMemQueue::create() shmat");
                }

                if (shmctl(id, IPC_RMID, NULL) == -1) {
                    perror("BasicSharedMemQueue::close() shmctl");
                }

                id = 0;
            } else {
                if (errno == EEXIST) {
                    continue;
                }

                perror("BasicSharedMemQueue::create() shmget");
            }

            break;
        }

        sem.destroy();
    }

    key = 0;

    unlock();
    return 0;
}


int circularwrite(unsigned char *dest, size_t size, const unsigned char *src,
                  size_t start, size_t n)
{
    ASSERT_OR_THROW("Invalid start!", size >= start);
    ASSERT_OR_THROW("Invalid range!", 0 < n && n <= size);

    size_t ptr = start;

    for (unsigned int i = 0; i < n; i++) {
        dest[ptr] = src[i];
        ptr = (ptr == size - 1 ? 0 : ptr + 1);
    }

    return ptr;
}

int BasicSharedMemQueue::write(const unsigned char *data, unsigned long size)
{
    unsigned long rem = size;

    while (rem) {
        lock();

        if (!isOpen()) {
            unlock();
            return 0;
        }

        SEM_LOCK;

        if (control->space) {
            unsigned long wsize = MIN(rem, control->space);

            control->write =
                circularwrite(control->buffer, control->size,
                              &data[size - rem], control->write, wsize);

            rem -= wsize;
            control->space -= wsize;
            control->filled += wsize;
        }

        SEM_UNLOCK;

        unlock();

        if (rem) {
            usleep(100);
        }
    }

    return size - rem;
}

int circularread(unsigned char *dest, const unsigned char *src,
                 size_t size, size_t start, size_t n)
{
    ASSERT_OR_THROW("Invalid start!", size >= start);
    ASSERT_OR_THROW("Invalid range!", 0 < n && n <= size);

    unsigned int ptr = start;

    for (unsigned int i = 0; i < n; i++) {
        dest[i] = src[ptr];
        ptr = (ptr == size - 1 ? 0 : ptr + 1);
    }

    return ptr;
}

int BasicSharedMemQueue::read(unsigned char *data, unsigned long size)
{
    ASSERT_OR_THROW("NULL control!", control);

    unsigned long rem = size;

    while (rem) {
        lock();

        if (!isOpen()) {
            unlock();
            return 0;
        }

        SEM_LOCK;

        if (control->filled) {
            long rsize = MIN(rem, control->filled);

            control->read =
                circularread(&data[size - rem], control->buffer,
                             control->size, control->read, rsize);

            rem -= rsize;
            control->space += rsize;
            control->filled -= rsize;
        }

        SEM_UNLOCK;

        unlock();

        if (rem) {
            usleep(100);
        }
    }

    return size - rem;
}

//#define BASICSHAREDMEMQUEUE_TEST
#ifdef BASICSHAREDMEMQUEUE_TEST
#include <iostream>
#include <unistd.h>

using namespace std;

int main(int argc, char **argv)
{
    BasicSharedMemQueue Q;

    int count = 1000000;

    key_t key;

    if (argc < 2) {
        if ((key = Q.create(0, 10000))) {
            cout << "Created BasicSharedMemQueue key = " << key << endl;
        } else {
            cerr << "Failed to create BasicSharedMemQueue." << endl;
            return 1;
        }

        while (!Q.isConnected()) {
            usleep(100);
        }

        int i;

        for (i = 0; i < count; i++) {
            int num = rand();
            int notNum = ~num;
            Q.write((unsigned char *)&num, sizeof(int));
            Q.write((unsigned char *)&notNum, sizeof(int));
        }

        while (Q.isConnected()) {
            usleep(100);
        }

    } else {
        key = atoi(argv[1]);

        if ((key = Q.open(key))) {
            cout << "Opened BasicSharedMemQueue key = " << key << endl;
        } else {
            cerr << "Failed to open BasicSharedMemQueue." << endl;
            return 1;
        }

        int i;

        for (i = 0; i < count; i++) {
            int num[2];
            Q.read((unsigned char *)&num, 2 * sizeof(int));

            if (num[0] != ~num[1]) {
                cout << "Pair mismatch at " << i << endl;
            }

            if (i % 1000 == 0) {
                cout << '.' << flush;
            }
        }
    }

    return 0;
}
#endif



