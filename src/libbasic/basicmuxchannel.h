/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICMUXCHANNEL_H
#define BASICMUXCHANNEL_H

#include "../BasicUtils/BasicCondition.h"
#include "basicmuxmessage.h"

#include <list>

class BasicMsgQueueMUX;

class BasicMUXChannelBase
{
        long key;
        long msgSize;
        long qLength;

        BasicMsgQueueMUX *parent;
        long filled;

        typedef std::list<BasicMUXMessageBase *> queue_t;
        queue_t readQ;
        queue_t writeQ;

        BasicCondition readLock;
        BasicCondition writeLock;

    public:
        BasicMUXChannelBase(long key, long msgSize, long qLength);
        virtual ~BasicMUXChannelBase();

        long getKey() {
            return key;
        }

        BasicMUXMessageBase *read(const bool blocking = true);

        bool write(BasicMUXMessageBase *msg, const bool blocking = true);
        void flush();

        virtual BasicMUXMessageBase *allocateMsg() {
            return new BasicMUXMessageBase(msgSize);
        }

    protected:
        void setParent(BasicMsgQueueMUX *parent) {
            this->parent = parent;
        }
        void enqueue(BasicMUXMessageBase *msg);
        BasicMUXMessageBase *dequeue();
        bool writeQueueIsEmpty();
        void msgCleared();

        friend class BasicMsgQueueMUX;
};

template <class T>
class BasicMUXChannel : public BasicMUXChannelBase
{
    public:
        BasicMUXChannel(long key, long qLength) :
            BasicMUXChannelBase(key, sizeof(T), qLength) {
        }

        T readT(const bool blocking = true) {
            T data;
            BasicMUXMessage<T> *msg = (BasicMUXMessage<T> *)read(blocking);

            if (msg) {
                data = msg->getDataRef();
                delete msg;
            } else {
                memset(&data, 0, sizeof(T));
            }

            return data;
        }

        bool writeT(T &data, const bool blocking = true) {
            return write(new BasicMUXMessage<T>(data), blocking);
        }

        virtual BasicMUXMessageBase *allocateMsg() {
            return new BasicMUXMessage<T>;
        }
};
#endif
