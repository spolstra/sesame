/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICSHAREDMEMQUEUE_H
#define BASICSHAREDMEMQUEUE_H

#include <sys/ipc.h>
#include "../BasicUtils/BasicThread.h"
#include "basicsemaphore.h"
#include "basiccleanuphandler.h"

#define BasicSharedMemQueueVersion  1
#define BasicSharedMemQueueMagic (BasicSharedMemQueueVersion |\
                                  (unsigned int)'S' << 8 |\
                                  (unsigned int)'M' << 16 |\
                                  (unsigned int)'Q' << 24)

class BasicSharedMemQueue : public BasicLockable
{
        BasicSemaphore sem;
        int id;
        key_t key;
        bool amOwner;

        typedef struct {
            int magic;
            key_t sem_key;
            unsigned long size;
            unsigned long read;
            unsigned long write;
            unsigned long space;
            unsigned long filled;
            int host_attached;
            int client_attached;
            unsigned char buffer[];
        } control_t;

        control_t *control;

        BasicCleanupFunctor<BasicSharedMemQueue> cleanupFunctor;

    public:
        BasicSharedMemQueue();
        virtual ~BasicSharedMemQueue();

        virtual bool isOpen();
        virtual bool isConnected();
        virtual bool isEmpty();
        virtual bool isFull();

        virtual key_t open(key_t key);
        virtual void close();
        virtual key_t create(key_t key, long size);

        virtual int write(const unsigned char *data, unsigned long size);
        virtual int read(unsigned char *data, unsigned long size);
};
#endif
