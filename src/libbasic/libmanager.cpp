/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "libmanager.h"

#ifndef __USE_GNU
#define __USE_GNU
#endif
#include <dlfcn.h>
#include <iostream>
#include <string>
#include <list>

#include "../BasicUtils/BasicException.h"

using namespace std;

LibManager::pathList_t LibManager::pathList;

LibManager::LibManager() : parent(0) {}
LibManager::LibManager(LibManager *parent) : parent(parent) {}

LibManager::~LibManager()
{
    list<void *>::iterator it;

    for (it = libList.begin(); it != libList.end(); it++) {
        dlclose(*it);
    }
}

void LibManager::loadLibrary(const string name)
{
    ASSERT_OR_THROW("called with empty library name", !name.empty());

    list<string> dlerrors;

    void *lib  = dlopen(name.c_str(), RTLD_LAZY | RTLD_GLOBAL);

    if (!lib) {
        dlerrors.push_back(dlerror());
    }

    for (pathList_t::iterator it = pathList.begin(); !lib && it != pathList.end(); it++) {
        string path = (*it) + "/" + name;
        lib = dlopen(path.c_str(), RTLD_LAZY | RTLD_GLOBAL);

        if (!lib) {
            dlerrors.push_back(dlerror());
        }
    }

    if (!lib) {
        string errStr = string("Error opening '") + name + "':";
        list<string>::iterator it;

        for (it = dlerrors.begin(); it != dlerrors.end(); it++) {
            errStr = errStr + "\n  " + *it;
        }

        THROW(errStr);
    }

    libList.push_back(lib);
}

void *LibManager::loadSymbol(const string sym)
{

    void *ptr = 0;
    list<void *>::iterator it;

    for (it = libList.begin(); it != libList.end() && !ptr; it++) {
        ptr = dlsym(*it, sym.c_str());
    }

    if (!ptr && parent) {
        return parent->loadSymbol(sym);
    }

    ptr = dlsym(RTLD_DEFAULT, sym.c_str());

    if (!ptr) {
        THROW(string("Error loading symbol '") + sym + "': " + dlerror());
    }

    return ptr;
}

void LibManager::addSearchPath(const string path)
{
    pathList.push_back(path);
}

