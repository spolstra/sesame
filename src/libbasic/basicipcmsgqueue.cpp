/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "basicipcmsgqueue.h"

#include "../BasicUtils/BasicException.h"

#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>

BasicIPCMsgQueue::BasicIPCMsgQueue() : key(0), id(0),
    amOwner(false), cleanupFunctor(this, &BasicIPCMsgQueue::close)
{

    srand(time(NULL));
    BasicCleanupHandler::initialize();
}

BasicIPCMsgQueue::~BasicIPCMsgQueue()
{
    if (isOpen()) {
        close();
    }
}

bool BasicIPCMsgQueue::open(key_t _key, const unsigned int mode)
{
    if (isOpen()) {
        return false;
    }

    ASSERT_OR_THROW("NULL key!", _key);

    if ((id = msgget(_key, (mode & 0777))) == -1) {
#ifdef BASICIPCMSGQUEUE_DEBUG
        perror("BasicIPCMsgQueue::open()");
#endif
        id = 0;
        return false;
    }

    key = _key;
    BasicCleanupHandler::regCleanup(&cleanupFunctor);
    return true;
}

key_t BasicIPCMsgQueue::create(key_t _key, const unsigned int mode)
{
    if (isOpen()) {
        return key;
    }

    while (true) {
        key = _key;

        if (!key) {
            key = rand();
        }

        if ((id = msgget(key, (mode & 0777) | IPC_CREAT | IPC_EXCL)) == -1) {
            key = 0;
            id = 0;

            if (!_key && errno == EEXIST) {
                continue;
            }

#ifdef BASICIPCMSGQUEUE_DEBUG
            perror("BasicIPCMsgQueue::create()");
#endif
            return 0;
        }

        amOwner = true;
        BasicCleanupHandler::regCleanup(&cleanupFunctor);
        return key;
    }
}

void BasicIPCMsgQueue::close()
{
    if (!isOpen()) {
        return;
    }

    BasicCleanupHandler::unregCleanup(&cleanupFunctor);

    if (amOwner) {
        msgctl(id, IPC_RMID, NULL);
    }

    id = 0;
    key = 0;
}

bool BasicIPCMsgQueue::isOpen()
{
    return id && key;
}

bool BasicIPCMsgQueue::send(const void *msgp,
                            const size_t size,
                            const bool blocking)
{
    if (!isOpen()) {
        return false;
    }

    ASSERT_OR_THROW("NULL message pointer!", msgp);
    ASSERT_OR_THROW("NULL message!", *(long *)msgp);

    if (msgsnd(id, msgp, size, blocking ? 0 : IPC_NOWAIT) == -1) {
        if (!blocking && errno == EAGAIN) {
            return 0;
        }

#ifdef BASICIPCMSGQUEUE_DEBUG
        perror("BasicIPCMsgQueue::send()");
#endif
        return false;
    }

    return true;
}

size_t BasicIPCMsgQueue::receive(void *msgp,
                                 const size_t size,
                                 const long type,
                                 const bool blocking)
{
    if (!isOpen()) {
        return false;
    }

    ASSERT_OR_THROW("NULL message!", msgp);

    ssize_t len =
        msgrcv(id, msgp, size, type, blocking ? 0 : IPC_NOWAIT);

    if (len  == -1) {
        if (!blocking && errno == ENOMSG) {
            return 0;
        }

#ifdef BASICIPCMSGQUEUE_DEBUG
        perror("BasicIPCMsgQueue::receive()");
#endif
        return 0;
    }

    return len;
}


//#define BASICIPCMSGQUEUE_TEST
#ifdef BASICIPCMSGQUEUE_TEST
#include <iostream>

#define MSG_SIZE 100

struct my_msgbuf {
    long mtype;
    char mtext[100];
};

int main(int argc, char **argv)
{
    key_t key = 0;

    if (argc == 2) {
        key = atoi(argv[1]);
    }

    BasicIPCMsgQueue q;

    my_msgbuf msg;

    if (key) {
        if (!q.open(key)) {
            cerr << "Error opening message queue." << endl;
            exit(1);
        }

        msg.mtype = 1;
        q.receive(&msg, sizeof(my_msgbuf));
        cout << "Client received '" << msg.mtext << "'." << endl;

        msg.mtype = 2;
        strcpy(msg.mtext, "Hello from client!");
        q.send(&msg, sizeof(my_msgbuf));

    } else {
        if (!(key = q.create(0))) {
            cerr << "Error creating queue." << endl;
            exit(1);
        }

        cout << "Key is " << key << endl;

        msg.mtype = 1;
        strcpy(msg.mtext, "Hello from server!");
        q.send(&msg, sizeof(my_msgbuf));

        msg.mtype = 2;
        q.receive(&msg, sizeof(my_msgbuf));
        cout << "Server received '" << msg.mtext << "'." << endl;
    }
}
#endif
