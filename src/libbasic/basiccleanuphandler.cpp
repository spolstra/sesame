/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "basiccleanuphandler.h"

bool BasicCleanupHandler::initialized = false;

BasicCleanupHandler::object_list_t *BasicCleanupHandler::object_list = NULL;
BasicCleanupHandler::object_map_t *BasicCleanupHandler::object_map = NULL;

#define NUM_SIGNALS 6

const int BasicCleanupHandler::numSignals = NUM_SIGNALS;
int BasicCleanupHandler::signals[] =
{SIGHUP, SIGINT, SIGQUIT, SIGABRT, SIGPIPE, SIGTERM};

BasicCleanupHandler::sighandler_t BasicCleanupHandler::oldHandlers[NUM_SIGNALS];

int BasicCleanupHandler::cleanupSig = 0;
BasicThread *BasicCleanupHandler::cleaner = NULL;

