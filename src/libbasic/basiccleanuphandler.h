/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICCLEANUPHANDLER_H
#define BASICCLEANUPHANDLER_H

#include <iostream>
#include <map>
#include <list>
#include <typeinfo>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

#include "../BasicUtils/BasicThread.h"
#include "../BasicUtils/BasicLockable.h"
#include "../BasicUtils/BasicException.h"

//#define BASICCLEANUPHANDLER_DEBUG

struct BasicCleanupFunctorBase {
    virtual ~BasicCleanupFunctorBase() {}
    virtual void operator()() = 0;
};

template <class T>
struct BasicCleanupFunctor : public BasicCleanupFunctorBase {
        void (T::*fpt)();   // pointer to member function
        T *pt2Object;  // pointer to object

    public:
        BasicCleanupFunctor(T *_pt2Object, void(T::*_fpt)()) {
            pt2Object = _pt2Object;
            fpt = _fpt;
        };

        virtual void operator()() {
            (*pt2Object.*fpt)();
        }
};


class BasicCleanupHandler
{
        static bool initialized;

        typedef std::list<BasicCleanupFunctorBase *> object_list_t;
        typedef std::map<BasicCleanupFunctorBase *, object_list_t::iterator>
        object_map_t;

        static object_list_t *object_list;
        static object_map_t *object_map;

        static const int numSignals;
        static int signals[];

        typedef void (*sighandler_t)(int);
        static sighandler_t oldHandlers[6];

        static int cleanupSig;
        static BasicThread *cleaner;

    public:

        static void signalHandler(int sig) {
#ifdef BASICCLEANUPHANDLER_DEBUG
            std::cerr << "Entering signal handler" << std::endl;
#endif

            // Restore old handlers
            for (int i = 0; i < numSignals; i++) {
                signal(signals[i], SIG_IGN);
            }

            cleanupSig = sig;
        }

        static void doCleanup() {
            int cleanCnt = 0;

            object_map->clear();

            while (!object_list->empty()) {
                BasicCleanupFunctorBase *obj;
                obj = object_list->front();
                object_list->pop_front();

#ifdef BASICCLEANUPHANDLER_DEBUG
                cout << "Cleaning up  at " << obj << endl;
#endif

                (*obj)();
                cleanCnt++;
            }

            if (cleanCnt)
                std::cerr << "BasicCleanupHandler cleaned up " << cleanCnt
                          << " orphaned resource(s)!" << std::endl;
        }

        static void *cleanup(void *) {
#ifdef BASICCLEANUPHANDLER_DEBUG
            cout << "Started cleaner" << endl;
#endif

            while (!cleanupSig) {
                usleep(250);
            }

            doCleanup();

            exit(1);
        }

        static void initialize() {
            if (initialized) {
                return;
            }

            object_list = new object_list_t;
            object_map = new object_map_t;

            int i;

            for (i = 0; i < numSignals; i++) {
                sighandler_t handler = signal(signals[i], signalHandler);

                if (handler == SIG_IGN) {
                    signal(signals[i], SIG_IGN);
                } else if (handler != signalHandler) {
                    oldHandlers[i] = handler;
                }
            }

            cleaner = new BasicThread(cleanup);
            cleaner->start();

            atexit(doCleanup);

            initialized = true;
        }

        static void regCleanup(BasicCleanupFunctorBase *obj) {
#ifdef BASICCLEANUPHANDLER_DEBUG
            cout << "Registering resource " << obj << endl;
#endif

            ASSERT_OR_THROW("NULL obj!", obj);
            object_list->push_front(obj);
            (*object_map)[obj] = object_list->begin();
        }

        static void unregCleanup(BasicCleanupFunctorBase *obj) {
            ASSERT_OR_THROW("NULL obj!", obj);

            object_map_t::iterator it = object_map->find(obj);

            if (it == object_map->end()) {
                return;
            }

#ifdef BASICCLEANUPHANDLER_DEBUG
            cout << "Unregistering resource " << obj << endl;
#endif

            object_list_t::iterator pos = (*it).second;
            object_list->erase(pos);
            object_map->erase(it);
        }
};
#endif
