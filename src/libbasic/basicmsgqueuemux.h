/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICMSGQUEUEMUX_H
#define BASICMSGQUEUEMUX_H

#include "../BasicUtils/BasicThread.h"
#include "../BasicUtils/BasicFunctor.h"
#include "basicipcmsgqueue.h"
#include "basicmuxmessage.h"

#include <map>
#include <list>

class BasicMUXChannelBase;

class BasicMsgQueueMUX
{
        typedef enum {MSGQ_OWNER = 1, MSGQ_CLIENT} message_types;

        BasicIPCMsgQueue msgQueue;

        bool writerFlushAndExit;

        BasicFunctor<BasicMsgQueueMUX> readFunctor;
        BasicFunctor<BasicMsgQueueMUX> writeFunctor;

        BasicThread readerThread;
        BasicThread writerThread;

        typedef std::map<int, BasicMUXChannelBase *> channelMap_t;
        channelMap_t channelMap;

        typedef std::list<BasicMUXChannelBase *> channelList_t;
        channelList_t channelList;

        typedef std::list<int> msgClearedList_t;
        msgClearedList_t msgClearedList;

        typedef struct {
            long type;
            long key;
        } clear_message_t;

    public:

        BasicMsgQueueMUX();
        virtual ~BasicMsgQueueMUX();

        bool addChannel(BasicMUXChannelBase *channel);
        void removeChannel(BasicMUXChannelBase *channel);

        bool isOpen();
        bool open(key_t key);
        key_t create(key_t key);
        void close(const bool flushFirst = true);

    protected:
        void msgCleared(int key);
        void wakeWriter();

        void writeRun();
        void readRun();

    private:
        bool writesAllClear();
        int sendClears(int max);
        int sendMessages(int max);

        void readClear(clear_message_t *clearMsg);
        void readMessage(BasicMUXMessageBase::message_t *msg, int len);

        friend class BasicMUXChannelBase;
        friend struct BasicFunctor<BasicMsgQueueMUX>;
};
#endif
