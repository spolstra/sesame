/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef BASICSEMAPHORE_H
#define BASICSEMAPHORE_H

#include <sys/ipc.h>

class BasicSemaphore
{
        bool amOwner;
        bool isLocked;
        int id;
        key_t key;

    public:
        BasicSemaphore();
        virtual ~BasicSemaphore();

        virtual bool isConnected();

        virtual key_t create(key_t key);
        virtual void destroy();
        virtual int open(key_t key);

        virtual bool tryLock();
        virtual void lock();
        virtual void unlock();
};
#endif
