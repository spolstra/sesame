/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "basicmsgqueuemux.h"
#include "basicmuxchannel.h"

#include "../BasicUtils/BasicException.h"

#include <string.h>

//#define BASICMSGQUEUEMUX_DEBUG
#ifdef BASICMSGQUEUEMUX_DEBUG
#include "basiclockable.h"
#include <iostream>
#include <stdio.h>
using namespace std;
#endif

BasicMsgQueueMUX::BasicMsgQueueMUX() :
    writerFlushAndExit(false),
    readFunctor(this, &BasicMsgQueueMUX::readRun),
    writeFunctor(this, &BasicMsgQueueMUX::writeRun),
    readerThread(&readFunctor),
    writerThread(&writeFunctor)
{
}

BasicMsgQueueMUX::~BasicMsgQueueMUX()
{
}


bool BasicMsgQueueMUX::addChannel(BasicMUXChannelBase *channel)
{
    ASSERT_OR_THROW("NULL channel!", channel);
    channelMap_t::iterator it = channelMap.find(channel->getKey());

    if (it != channelMap.end()) {
        return false;
    }

    channelMap[channel->getKey()] = channel;
    channelList.push_back(channel);
    channel->setParent(this);
    return true;
}

void BasicMsgQueueMUX::removeChannel(BasicMUXChannelBase *channel)
{
    ASSERT_OR_THROW("NULL channel!", channel);
    channelMap.erase(channel->getKey());
    channelList.remove(channel);
}

bool BasicMsgQueueMUX::isOpen()
{
    return msgQueue.isOpen();
}

bool BasicMsgQueueMUX::open(key_t key)
{
    if (isOpen()) {
        return false;
    }

    if (msgQueue.open(key)) {

        writerFlushAndExit = false;
        readerThread.start();
        writerThread.start();
        return true;
    }

    return false;
}

key_t BasicMsgQueueMUX::create(key_t key)
{
    if (isOpen()) {
        return 0;
    }

    key_t _key;

    if ((_key = msgQueue.create(key))) {

        writerFlushAndExit = false;
        readerThread.start();
        writerThread.start();
        return _key;
    }

    return 0;
}

void BasicMsgQueueMUX::close(const bool flushFirst)
{
    if (!isOpen()) {
        return;
    }

    readerThread.cancel();

    if (flushFirst) {
        writerThread.lock();
        writerFlushAndExit = true;
        writerThread.signal();
        writerThread.unlock();

    } else {
        writerThread.cancel();
    }

    // Must be done first to release a blocked reader thread
    msgQueue.close();

    readerThread.join();
    writerThread.join();
}

void BasicMsgQueueMUX::msgCleared(int key)
{
    writerThread.lock();
    msgClearedList.push_back(key);
    writerThread.signal();
    writerThread.unlock();

#ifdef BASICMSGQUEUEMUX_DEBUG
    cout << "Message cleared key " << key << endl;
#endif
}

void BasicMsgQueueMUX::wakeWriter()
{
    writerThread.lock();
    writerThread.signal();
    writerThread.unlock();
}

bool BasicMsgQueueMUX::writesAllClear()
{
    if (!msgClearedList.empty()) {
        return false;
    }

    channelList_t::iterator it;

    for (it = channelList.begin(); it != channelList.end(); it++)
        if (!(*it)->writeQueueIsEmpty()) {
            return false;
        }

    return true;
}

int BasicMsgQueueMUX::sendClears(int max)
{
    clear_message_t clearMsg;
    int i = 0;

    writerThread.lock();

    for (; i < max && !msgClearedList.empty(); i++) {
        clearMsg.type = msgQueue.isOwner() ? MSGQ_CLIENT : MSGQ_OWNER;

        clearMsg.key = msgClearedList.front();
        msgClearedList.pop_front();

        writerThread.unlock();

        if (!msgQueue.send(&clearMsg, sizeof(clear_message_t))) {
            return -(i + 1);
        }

        writerThread.lock();

#ifdef BASICMSGQUEUEMUX_DEBUG
        cout << "Writer thread sent clear " << clearMsg.key << " to "
             << (msgQueue.isOwner() ? "Client" : "Owner")
             << " cleared list size = " << msgClearedList.size() << endl;
#endif
    }

    writerThread.unlock();

    return i;
}


int BasicMsgQueueMUX::sendMessages(int max)
{
    channelList_t::iterator it;
    int count = 0;

    for (it = channelList.begin(); it != channelList.end(); it++) {
        BasicMUXChannelBase *channel = *it;
        BasicMUXMessageBase *msg;

        for (int i = 0; i < max && (msg = channel->dequeue()); i++) {
            BasicMUXMessageBase::message_t *message = msg->msg;
            message->type = msgQueue.isOwner() ? MSGQ_CLIENT : MSGQ_OWNER;

            if (!msgQueue.send(message, sizeof(BasicMUXMessageBase::message_t) + msg->size)) {
                return -(count + 1);
            }

#ifdef BASICMSGQUEUEMUX_DEBUG
            cout << "Writer thread sent message to "
                 << (msgQueue.isOwner() ? "Client" : "Owner") << endl;
#endif

            delete msg;
            count++;
        }
    }

    return count;
}

void BasicMsgQueueMUX::writeRun()
{
#ifdef BASICMSGQUEUEMUX_DEBUG
    cout << "Writer thread entered." << endl;
#endif

    while (true) {
        int n = channelList.size();

        if (sendClears(n) < 0) {
#ifdef BASICMSGQUEUEMUX_DEBUG
            cerr << "Error sending clear." << endl;
#endif
            break;
        }

        if (sendMessages(n) < 0) {
#ifdef BASICMSGQUEUEMUX_DEBUG
            cerr << "Error sending message." << endl;
#endif
            break;
        }


        writerThread.lock();

        if (writesAllClear()) {
            if (writerFlushAndExit) {
                break;
            }

            writerThread.wait();
        }

        writerThread.unlock();
    }

#ifdef BASICMSGQUEUEMUX_DEBUG
    cout << "Writer thread exiting." << endl;
#endif
}

void BasicMsgQueueMUX::readClear(clear_message_t *clearMsg)
{
    BasicMUXChannelBase *channel = channelMap[clearMsg->key];

    if (channel) {
        channel->msgCleared();

#ifdef BASICMSGQUEUEMUX_DEBUG
        cout << "Reader thread recieved clear from "
             << (msgQueue.isOwner() ? "Client" : "Owner")
             << " for channel " << clearMsg->key << endl;
    } else {
        cerr << "Error finding channel with key = " << clearMsg->key
             << " for clear." << endl;
#endif
    }
}

void BasicMsgQueueMUX::readMessage(BasicMUXMessageBase::message_t *msg, int len)
{
    BasicMUXChannelBase *channel = channelMap[msg->key];

    if (channel) {
        BasicMUXMessageBase *message = channel->allocateMsg();
        int totalSize = sizeof(BasicMUXMessageBase::message_t) + message->size;
        memcpy(message->msg, msg, len < totalSize ? len : totalSize);

#ifdef BASICMSGQUEUEMUX_DEBUG

        if (totalSize != len)
            cerr << "Recieved message size " << len << " dosen't equal channel message size "
                 << message->size << "." << endl;

        cout << "Reader thread recieved message from "
             << (msgQueue.isOwner() ? "Client" : "Owner")
             << " for channel " << msg->key << endl;
#endif

        channel->enqueue(message);

#ifdef BASICMSGQUEUEMUX_DEBUG
    } else {
        cerr << "Error finding channel with key = " << msg->key
             << " for message delivery." << endl;
#endif
    }
}

// Fix this
#define MSGMAX 8196

void BasicMsgQueueMUX::readRun()
{
    char msgBuf[MSGMAX];
    size_t len;

#ifdef BASICMSGQUEUEMUX_DEBUG
    cout << "Reader thread entered." << endl;
#endif

    while (true) {
        long type = msgQueue.isOwner() ? MSGQ_OWNER : MSGQ_CLIENT;

        if (!(len = msgQueue.receive(msgBuf, MSGMAX, type))) {
#ifdef BASICMSGQUEUEMUX_DEBUG
            cerr << "Error receiving message." << endl;
#endif
            break;
        }

        if (len == sizeof(clear_message_t)) {
            readClear((clear_message_t *)msgBuf);
        } else {
            readMessage((BasicMUXMessageBase::message_t *)msgBuf, len);
        }
    }

#ifdef BASICMSGQUEUEMUX_DEBUG
    cerr << "Read thread exiting." << endl;
#endif
}

//#define BASICMSGQUEUEMUX_TEST
#ifdef BASICMSGQUEUEMUX_TEST
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include "BasicString.h"

using namespace std;

int main(int argc, char **argv)
{
    BasicMsgQueueMUX muxQ;
    long key;
    const int numChannels = 10;
    const int msgSize = 1024;
    const int channelLength = 1;
    const int numMessages = 1000;
    BasicMUXChannelBase *channels[numChannels];

    for (int i = 0; i < numChannels; i++) {
        channels[i] = new BasicMUXChannelBase(i + 1, msgSize, channelLength);

        if (!muxQ.addChannel(channels[i])) {
            THROW(string("Error adding channel key = ") + BasicString(i));
        }
    }

    BasicMUXMessageBase *msg;

    if (argc > 1) {
        key = atoi(argv[1]);

        if (!key) {
            cout << "Enter mux key: " << flush;
            cin >> key;
        }

        ASSERT_OR_THROW("Invalid key!", key);

        if (!muxQ.open(key)) {
            THROW("Error opening MUX Q!");
        }

        for (int j = 0; j < numMessages; j++) {
            for (int i = 0; i < numChannels; i++) {
                msg = channels[i]->read();

                if (!msg) {
                    cerr << "Error receiving message " << j << " for channel " << i
                         << endl;
                } else {
                    cout << "Message recieve " << j << " on channel " << i << ": "
                         << (char *)msg->getData() << endl;
                    delete msg;
                }
            }
        }

    } else {

        if (!(key = muxQ.create(0))) {
            cerr << "Error creating MUX Q." << endl;
            exit(1);
        }

        cout << "MUX Q key = " << key << endl;

        char buf[msgSize];

        for (int j = 0; j < numMessages; j++) {
            for (int i = 0; i < numChannels; i++) {
                sprintf(buf, "Hello %d from creator on channel %d", j, i);
                msg = new BasicMUXMessageBase(buf, msgSize);
                channels[i]->write(msg);
                cout << "Creator sent message " << j << " to channel " << i << endl;
            }
        }

        // Flush channels
        for (int i = 0; i < numChannels; i++) {
            channels[i]->flush();
        }
    }

    muxQ.close(); // Flushes all buffers
}
#endif
