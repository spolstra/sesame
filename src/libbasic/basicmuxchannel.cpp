/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include "../BasicUtils/BasicException.h"

#include "basicmuxchannel.h"
#include "basicmsgqueuemux.h"

BasicMUXChannelBase::BasicMUXChannelBase(long key, long msgSize,
        long qLength) :
    key(key), msgSize(msgSize), qLength(qLength), parent(NULL), filled(0)
{
}

BasicMUXChannelBase::~BasicMUXChannelBase()
{
}

BasicMUXMessageBase *BasicMUXChannelBase::read(const bool blocking)
{
    readLock.lock();

    if (readQ.empty() && !blocking) {
        readLock.unlock();
        return NULL;
    }

    if (readQ.empty()) {
        readLock.wait();
    }

    ASSERT_OR_THROW("Read queue empty!", !readQ.empty());

    BasicMUXMessageBase *msg = readQ.front();
    readQ.pop_front();

    readLock.unlock();
    ASSERT_OR_THROW("NULL parent!", parent);
    parent->msgCleared(key);

    return msg;
}

bool BasicMUXChannelBase::write(BasicMUXMessageBase *msg, const bool blocking)
{
    writeLock.lock();

    if (filled == qLength && !blocking) {
        return false;
    }

    if (filled == qLength) {
        writeLock.wait();
    }

    ASSERT_OR_THROW("Overflow!", filled < qLength);

    msg->msg->key = key;
    writeQ.push_front(msg);
    filled++;

    writeLock.unlock();
    ASSERT_OR_THROW("NULL parent!", parent);
    parent->wakeWriter();
    return true;
}

void BasicMUXChannelBase::flush()
{
    writeLock.lock();

    while (filled) {
        writeLock.wait();
    }

    writeLock.unlock();
}

void BasicMUXChannelBase::enqueue(BasicMUXMessageBase *msg)
{
    readLock.lock();
    readQ.push_back(msg);
    readLock.signal();
    readLock.unlock();
}

BasicMUXMessageBase *BasicMUXChannelBase::dequeue()
{
    writeLock.lock();

    if (writeQ.empty()) {
        writeLock.unlock();
        return NULL;
    }

    BasicMUXMessageBase *msg = writeQ.back();
    writeQ.pop_back();

    writeLock.unlock();
    return msg;
}

bool BasicMUXChannelBase::writeQueueIsEmpty()
{
    writeLock.lock();
    bool isEmpty = writeQ.empty();
    writeLock.unlock();
    return isEmpty;
}

void BasicMUXChannelBase::msgCleared()
{
    writeLock.lock();
    filled--;
    writeLock.signal();
    writeLock.unlock();
}
