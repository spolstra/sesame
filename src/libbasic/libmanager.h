/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#ifndef LIBMANAGER_H
#define LIBMANAGER_H

#include <list>
#include <string>

class LibManager
{
        typedef std::list<void *> libList_t;
        libList_t libList;

        LibManager *parent;

        typedef std::list<std::string> pathList_t;
        static pathList_t pathList;

    public:
        LibManager();
        LibManager(LibManager *parent);
        ~LibManager();

        void setParent(LibManager *parent) {
            this->parent = parent;
        }
        LibManager *getParent() {
            return parent;
        }
        void loadLibrary(const std::string name);
        void *loadSymbol(const std::string sym);
        static void addSearchPath(const std::string path);
};

#endif // LIBMANAGER_H

