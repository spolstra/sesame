#include "XMLSerializer.h"
#include "XMLSerializerImpl.h"

using namespace std;

XMLSerializer *XMLSerializer::createInstance(ostream &out)
{
    return new XMLSerializerImpl(out);
}
