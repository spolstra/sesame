/*******************************************************************\

              Copyright (C) 2003 Joseph Coffland

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
        of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
             GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
                           02111-1307, USA.

            For information regarding this software email
                   jcofflan@users.sourceforge.net

\*******************************************************************/
#ifndef XMLEVENTTYPES_H
#define XMLEVENTTYPES_H

/**
 * XMLPullParser event types.
 *
 * These types can be or'ed together to create an parse event mask.
 */
class XMLEventTypes
{
    public:
        typedef int event_t;
        /// No event.
        static const event_t NULL_EVENT             = 0;

        /// The start XML document event.
        static const event_t START_DOCUMENT         = 1;

        /// The start XML element event.
        static const event_t START_ELEMENT          = 2;

        /// XML text event.
        static const event_t TEXT                   = 4;

        /// The end XML element event.
        static const event_t END_ELEMENT            = 8;

        /// The end XML document event.
        static const event_t END_DOCUMENT           = 16;

        /// XML comment event.
        static const event_t COMMENT                = 32;

        /// End line event.
        static const event_t END_LINE               = 64;

        /// A mask of all regular event types except
        static const event_t DEFAULT_MASK = 1 + 2 + 4 + 8 + 16;
};

#endif // XMLEVENTTYPES_H
