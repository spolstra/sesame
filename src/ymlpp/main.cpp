/*****************************************************************************

  This file is part of SESAME.

  Copyright (C) 2002-2012  University of Amsterdam

  SESAME is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  SESAME is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SESAME.  If not, see <http://www.gnu.org/licenses/>.

*****************************************************************************/

#include <iostream>
#include <fstream>
#include <ext/stdio_filebuf.h>

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

#include "../BasicUtils//BasicException.h"

#include "../XMLCereal/XMLSerializerImpl.h"

#include "../libyml/ymlloader.h"
#include "../libyml/defaultymlentityfactory.h"
#include "../libyml/perlscriptinterpreter.h"
#include "../libyml/ymlnetwork.h"

using namespace std;

void Syntax(char *name)
{
    cerr << endl
         << "ymlpp" << endl
         << "Joe Coffland" << endl
         << "University of Amsterdam" << endl
         << "Compile Date: " << __DATE__ << " at " << __TIME__ << endl << endl
         << "Syntax: " << name << " [OPTION]... <YML file> [variable=value]... "
         << endl
         << endl
         << "\t-d                 \tPrint DOT graph." << endl
         << "\t-f                 \tFlaten DOT graph." << endl
         << "\t-q                 \tSupress interpreter output. (except errors)"
         << endl
         << "\t-x                 \tPrint full stack trace on errors." << endl
         << endl;
    exit(1);
}

int main(int argc, char *argv[], char ** /*env*/)
{
    try {

        // Start interpreter
        Interpreter *interpreter = PerlScriptInterpreter::getSingleton();
        ASSERT_OR_THROW("NULL interpreter!", interpreter);
        interpreter->start();

        // Process Arguments
        bool printDOT = false;
        bool flatenDOT = false;
        bool quiteInterpreter = false;
        int pipeFD = 1;
        int c;

        while ((c = getopt(argc, argv, "dfqp:x")) != -1) {
            switch (c) {
                case 'd':
                    printDOT = true;
                    break;

                case 'f':
                    flatenDOT = true;
                    break;

                case 'q':
                    quiteInterpreter = true;
                    break;

                case 'p':
                    pipeFD = atoi(optarg);
                    break;

                case 'x':
                    BasicDebugger::initStackTrace(argv[0]);
                    break;

                default:
                    Syntax(argv[0]);
                    break;
            }
        }

        // Filename argument
        const char *fileName = NULL;

        if (optind < argc) {
            fileName = argv[optind];
        } else {
            cerr << "Missing 'YML file' argument." << endl;
            Syntax(argv[0]);
        }

        // Variable definition arguments:
        // optind is the index if the yml file. Everything after that
        // (stuff between optind+1 and argc) should be
        // variable definitions.
        for (int def_index = optind + 1; def_index < argc; def_index++) {
            char *ptr = strchr(argv[def_index], '=');

            if (!ptr) {
                cerr << "Unrecognized variable argument \'";
                cerr << argv[def_index] << "'." << endl;
                Syntax(argv[0]);
            }

            *ptr++ = '\0';
            interpreter->define(argv[def_index], ptr);
        }

        DefaultYMLEntityFactory factory;
        YMLLoader loader(&factory, interpreter);

        int save_stdout = 0;

        if (quiteInterpreter) {
            save_stdout = dup(1);
            close(1);

            if (open("/dev/null", 0) != 1) {
                THROW("Wrong file descriptor!");
            }
        }

        ifstream ymlFile(fileName);

        if (!ymlFile.is_open())
            THROW(string("ymlpp: Error opening \'") + fileName +
                  "\' for processing.");

        YMLNetwork *net = (YMLNetwork *)loader.parse(fileName);
        interpreter->stop();
        delete interpreter;

        if (quiteInterpreter) {
            fflush(stdout);
            close(1);

            if (dup(save_stdout) != 1) {
                THROW("Wrong file descriptor!");
            }
        }

        // C file descriptor to C++ streams magic
        // NOTE: This only works in GCC 3.2 and newer
        //       Hopefully they will leave the API alone now!
        std::__c_file *pipeFile = fdopen(pipeFD, "w");
        __gnu_cxx::stdio_filebuf<char> pipeBuf(pipeFile, std::ios::out);
        ofstream out;
        out.basic_ios<char>::rdbuf(&pipeBuf);

        if (printDOT) {
            net->dumpDOT(out, 0, flatenDOT);
        } else {
            XMLSerializerImpl stream(out);
            stream.startDocument();
            net->dumpXML(stream);
            stream.endDocument();
        }

        delete net;
        return 0;

    } catch (BasicException &e) {
        cerr << "Exception: " << e << endl;
    }

    return 1;
}
