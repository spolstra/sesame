Introduction
------------

The Sesame project uses the following three repositories:

1. Sesame: the simulation framework itself. Contains the compiler,
   runtime system and support libraries for the application layer and
   the architecture layer.
2. Mappingmodule: the python library used to interact with the xml
   descriptions of the applications, architectures and mappings.
3. DSE Framework: Design space exploration framework based on genetic
   algorithms.

System requirements
~~~~~~~~~~~~~~~~~~~
The Sesame simulation environment is tested on the following Linux
distributions:

* Debian Wheezy and Debian Jessie.
* Ubuntu 12.04 upto 17.04
* CentOS 6.6

Sesame should be easy to install on most other Debian based
distributions such as Linux Mint.

Installation
------------

Download installation script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The easiest way to install Sesame is to use the installation script
located here: https://csa.science.uva.nl/download/sesame/installer/

To download the script directly from your shell you can use the
command:

::

    wget https://csa.science.uva.nl/download/sesame/installer/setup_sesame.sh


Installing the dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you pass the installation the ``-l`` option it will list the
debian packages that the software depends on.
But before you can execute the installation script you need to set the
execute permissions:

::

    chmod +x setup_sesame.sh
    ./setup_sesame.sh -h    # show help info
    ./setup_sesame.sh -l    # list dependencies

Since we are going to install from the git repositories you will also
need to install the second set of packages listed (git, autoconf and make).
If your Linux distribution is not based on Debian you will need to
find the equivalent packages for that distribution.

Running the installer
~~~~~~~~~~~~~~~~~~~~~
For the installation you can use the publicly accessible versions of
the repositories or the private version of the repositories.  If you
intend to contribute to the Sesame project you can request access to
the private repositories.

To install Sesame from the private repositories you need to run the
script with the following options:

::

    ./setup_sesame.sh -R -p /absolute/path/to/installdir

To use the public version use the ``-r`` flag instead of ``-R``.
If all goes well the installation will end with a ``make check`` where
all the test cases pass.  If you encounter testcase failures please
contact us.

The installation creates a sesame.env script that contains the
environment (``PATH`` and ``PYTHONPATH``) settings for Sesame.
You can setup the environment with the command:

::

    source sesame.env

Installing the DSE Framework
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The DSE Framework is an optional part of the Sesame project that uses
genetic algorithms to perform to explore the mappings from
applications to architecture platforms.

The DSE Framework can be downloaded as tar archive at:
https://csa.science.uva.nl/download/sesame/dse_framework/

The archive contains a file ``readme.pdf`` that explains how to install
the framework.
