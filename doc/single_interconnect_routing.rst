Initialisation
~~~~~~~~~~~~~~

The template components together with their architecture components
negotiate component identifiers and routes. The virtual layer
components call their architecture components to get architecture ids
and to trigger the architecture components to set-up their mapping
tables. If needed components that have an application channel mapped
onto them will add an entry in the route table of the connected
component.

The exact behaviour depends on the templates that are used by the model.
We will describe the behaviour of the templates ``vproc`` and ``vmembus``,
which are the templates for a generic virtual processor and for shared
memory communication.


.. Should be mentioned somewhere here.
    The virtual layer can access the unique channel identifier via
    the pearl function virtualChannelID(). This function
    gives the virtual channel id that matches the id of the
    corresponding application channel. [..]

.. all comp names in `` `` ?

.. figure:: figs/initialisation.pdf
    :scale: 90

    Figure 1: Initialisation steps for communication mapping.


vproc initialisation
.....................

Figure 1 shows the initialisation steps to setup communication mapping
for a single application channel. The application channel ``c``
between tasks ``A`` and ``B`` is mapped onto the memory component. The
numbered steps in Figure 1 are described below.

The virtual channel used here is ``vmembus`` since that is the
template associated with the memory component. Channel templates are
all subclasses of ``vchannel``.

.. mention #<comptype> syntax here?

In step 1 the ``vproc B`` will ask each connected virtual channel for its
virtual channel id (vid).  The virtual channel gets this virtual channel id
with the library call ``virtualChannelID()`` and returns it.  The
``vproc`` uses this information to create a table that maps a virtual
channel id to an index in the array of channel objects (table in step
1b)::

  MAP: virtual channel id -> array index into vproc's channels array.


``vproc`` then calls its mapped architecture component to ask what id to
use when referring to this virtual channel with this call::

  chid = arch ! get_channel_id(vchannels[i] ! get_channel_id());

The parameter for this call ``vchannels[i] ! get_channel_id()``
gets the channel id from virtual channel with the calls sequence::

  vproc: vchannels[i] ! get_channel_id() (step 2)
    vmembus: arch ! get_id()             (step 3)
      memory: bus ! get_id()             (step 4)
        bus: reply(this)                 (step 5)

So the channel id for this virtual channel is the pearl id of the
bus. Note that the virtual component is actually mapped onto the memory, but the
Pearl id of the bus is returned. The processor will pass the message
to the bus and the bus has a routing table to send the message on to
the memory component.


Note that the virtual processor ``vproc`` has now obtained two id's:

The virtual channel id (vid)
    Which is used to refer to a channel at the virtual virtual layer.
    And because the same id is used at the application layer the
    read and write event use the correct virtual channel.
The channel id (chid)
    An application channel (and it's corresponding virtual channel)
    needs to be mapped onto an architecture component. The channel id
    is the Pearl id of that architecture component. In our example
    this is the pearl id of the bus component.


Because the processor is connected to the ``arch`` port, its
``get_channel_id()`` method is called with this bus pearl id (step 6).  The
``processor`` checks its own channel array to find the pearl id that
was passed, and returns the array index which matches the bus id to
the ``vproc`` [1]_ (step 7).  This allows the ``processor`` to route messages
from this virtual channel to the bus when the simulation is running.

.. [#] This works because the processor and memory are both directly
    connected to the bus.  Communication between two components connected
    via multiple busses will not work with this approach.

The ``vproc`` then stores the mapping of virtual channel id to
the array index returned by the processor (step 8)::

  MAP: virtual channel id -> array index into processor's channels array.

This mapping will be used when the ``vproc`` calls a read/write
method.  ``vproc`` will pass an array index which the ``processor``
can just use to lookup the correct arch port in its channels array.

After initialisation the virtual processor will have two tables (see
step 1b and 8). The first one is used to translate the virtual channel
id (vid) into the correct virtual channel. The second table
translates the vid to a array index that is used by the processor to
route the message.

vfifo initialisation
.....................

The template of a fifo channel is simple. It allocates space on the
component that it is mapped at startup. This is for resource modeling
only, the space is not used and tokensize is not taken into
consideration.

vmembus initialisation
......................

The vmembus template calls create channel on a
architecture component of type memory during initialisation.
The memory receives this ``create_channel(vid)`` call, and
calls the bus to add a route vid --> memory for the bus.
The bus will use this route when it handles read/writes from the
processor. This is why the vid (virtual channel id) is passed with
these calls.
The memory is telling the bus: If you get a read or write
request with this vid id send it to me.

An example
~~~~~~~~~~

Once the initialisation is completed the system is ready to accept
trace events. The traces generated by the
application model can now be used to drive the architecture simulation.

Figure 2 shows how a read trace event is handled from application to
architecture.  The numbered steps are explained below:

1. Application task B does a read on channel c.

2. The ``vproc`` of B (``VProc_B``) gets the trace event with nextTrace()
   A trace event contains four fields: type, operId, size,
   commChannelId (see section Application events).
   The commChannelId is the id of the virtual channel (vid) of the read
   event.

3. The ``vproc`` looks up this commChannelId in a map created during
   initialisation to find the correct virtual channel.
   The result of the lookup is an index into the vchannels array of
   virtual channel connections.
   The picture shows only one virtual channel but a ``vproc`` is
   normally connected with multiple virtual channels.

4. VProc_B then calls check_data() on the virtual channel.
   This synchronous call can block the vproc until data arrives on
   the virtual channel. This is the synchronisation part of the
   communication modeling.

5. First a table lookup is done to retrieve the architecture component
   onto which the virtual channel is 'mapped'. The lookup returns the
   index into the ``channels`` array of the processor component that
   contains the bus pearl id.
   ``vproc`` then calls the read method on the architecture component
   ``Proc Y``. It passes the architecture channel array index and the
   virtual channel id as arguments.

6. ``Proc Y`` receives the read method call, and forwards the
   read method call to the correct communication component (in this case
   the bus).

7. The bus receives the read. It then performs a lookup in its route table
   using the virtual channel id. The lookup will return the Pearl id
   of the memory component.
   It then calls the read method of the memory component.
8. The memory receives the read method, handles it and returns.
   This causes Bus to return, which allows Proc Y to return 'control'
   to VProc B.

9. At this point the communication latency of the read has
   been modeled, and we model the completion at synchronisation level
   by calling ``signal_room()`` on the vchannel.

..  for 5 (also size of the trace event. This is set to the size of the data
     type of the application channel. TODO: Check for order detail?)
     The virtual id is used in routing.
     Finally the vproc calls signal_room() in the virtual channel.


.. figure:: figs/simple-read.pdf
    :scale: 90

    Figure 2: Method calls when executing a read event.


.. From here on it's still a draft version. Needs work.


