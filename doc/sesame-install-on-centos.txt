 sudo yum install git
 sudo yum install gcc
 sudo yum install gcc-c++
 sudo yum install make
 sudo yum install libtool-ltdl
 sudo yum install libtool-ltdl-devel
 sudo yum install perl-ExtUtils-Embed-1.28-144.el6

# python 2.7
 sudo yum groupinstall -y 'development tools'
 sudo yum install -y zlib-dev openssl-devel sqlite-devel bzip2-devel
 sudo yum install xz-libs

 wget http://www.python.org/ftp/python/2.7.10/Python-2.7.10.tar.xz
 xz -d Python-2.7.10.tar.xz 
 tar tf Python-2.7.10.tar 
 cd Python-2.7.10
 ./configure --prefix=/usr/local
 make
 make altinstall

# setuptools
 wget  https://bitbucket.org/pypa/setuptools/downloads/ez_setup.py
 sudo /usr/local/bin/python2.7 ez_setup.py 

 
 sudo /usr/local/bin/easy_install python-networkx
 sudo /usr/local/bin/easy_install networkx
 
 sudo yum install libxslt-devel libxml2-devel
 sudo /usr/local/bin/easy_install lxml

# xerces
# downloaded xerces-c-3.2.1.tar.gz from archive (mirror was broken)
 ./configure
 make
 sudo make install

# Imagemagick
 sudo yum install ImageMagick.x86_64

# Fixes
# xerces needs help finding its libraries
export LD_LIBRARY_PATH=/usr/local/lib

# Fixes that need sudo.
# sesame uses python, make sure it's python2.7 and not python2.6
spolstra@centos:/usr/local/bin$ sudo ln -s python2.7 python

# fixed issues, notes
libtool should add  -I/usr/lib64/perl5/CORE/ when compiling /libyml.
It doesn't because we where missing:
sudo yum install perl-ExtUtils-Embed-1.28-144.el6
spolstra@centos:~/projects/sesame/sesame/src/libyml$ /bin/sh ../../libtool --tag=CXX   --mode=compile g++ -DHAVE_CONFIG_H   -I . -I/usr/lib64/perl5/CORE/  -Wall -Wextra -O2 -MT perlscriptinterpreter.lo -MD -MP -MF .deps/perlscriptinterpreter.Tpo -c -o perlscriptinterpreter.lo perlscriptinterpreter.cpp

# need pip2.7?
 sudo /usr/local/bin/easy_install pip
