-------------------
Generating mappings
-------------------

Requirements
------------
You will need the latest version of the master branch for

* Sesame project
* DSE Framework

For installation of the Sesame project we refer to the installation
script on the csa website:
``https://csa.science.uva.nl/download/sesame/installer/``.  The
installation of the DSE Framework is explained below.

Generation Methods
------------------
There are currently two ways to quickly generate a lot of mappings:

1. The DSE framework has an option to save all the mapping that is
   evaluates during exploration.
2. Sesame has a script to generate mappings for an
   application-architecture  pair.

The application we use here is mp3 decoder example from the Sesame
framework. The architecture is a fully connected fifo platform with
5 processors that was generated with the following command::

    $ FifoFullConnect 5 5proc_arch.yml

Using the DSE Framework
~~~~~~~~~~~~~~~~~~~~~~~
The following steps are needed to install the DSE Framework::

    # Select a directory for the code
    $ cd ~/projects

    # Get the code.
    $ git clone https://csa.science.uva.nl/git/dse/sesame/dse_framework.git

    # Setup the environment
    export PATH+=:~/projects/dse_framework/bin
    export PYTHONPATH+=:~/projects/dse_framework/lib

To start an exploration and save all the mappings type with the
``--store`` flag::

    # we use the mp3-fifo as an example here
    cd mp3-fifo
    RunDSE --store -j4 media.yml

Use ``RunDSE -h`` or check its readme.rst for more information on the
DSE Framework.  After the exploration we collect all the intermediate
and the pareto front mappings::

    # Collect the mappings from all workers in maps
    $ mkdir maps
    # Include Pareto mappings (mapXX.yml)
    cp map[0-9]*.yml PoolWorker-?/[0-9]*_map.yml maps

One options is to convert the mappings to graphs::

    $ cd maps
    $ for i in `ls *.yml`; do MapToDot $i; \
        dot -Tpng dgraph.dot > `basename $i .yml`.png; done
    $ eog *.png

Convert all YML mappings to SCC DAL style XML with::

    $ for i in `ls *.yml`; do MapToSCCMap $i; \
        mv SCCmap.xml `basename $i .yml`.xml; done

Using GenerateMappings
~~~~~~~~~~~~~~~~~~~~~~
The aptly named script ``GenerateMappings`` can generate enumerate all
mappings between an application and an architecture. Since this is
infeasible for any non trivial project, a limit to the number of
generated mappings can be specified.  For some applications the mappings
enumerated in this way are not very interesting because they are all
very similar. In that case the user can choose to generated random
mappings instead. The command to generate 20 random mappings for the
``mp3-fifo`` testcase is::

    GenerateMappings --random app/mp3_app.yml arch/5test.yml
