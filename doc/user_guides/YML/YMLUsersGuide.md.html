**YML Users Guide**
<!--
update:
Joseph Coffland
jcofflan@users.sourceforge.net
April 5, 2006
-->

Introduction
============

This document describes YML or Y-Chart Modeling Language an XML
derivative and how to use it. A basic knowledge of XML is assumed. See
http://www.w3.org/TR/REC-xml/ for information about XML itself.

YML is a language for describing simulation models as directed graphs.
YML was designed to be flexible enough to describe many different types
of simulations. We currently use it to describe Kahn process network
applications and event driven architectures. The basic components are
links, nodes and networks. Properties can be added to these components
to give simulation specific information. Scripting and sets were added
to simplify the description of complicated simulation structures.

YML is a general purpose simulation description language. It can be
easily interfaced to any existing simulation tool by means of a YML
translator or direct interface between YML and the simulators internal
data structures. This can be done either with existing XML parsers alone
or with assistance from the ymllib C++ programming interface or the
python mapping module package.
<!-- See the LibYMLInterface document for more information. -->

Flat YML
========

Any piece of YML code can be translated to Flat YML by running it
through the YML preprocessor (`ymlpp`). See `ymlpp` manual for more
information. Flat YML consists only of network, node, port, link,
property, and doc elements.  These elements are the building blocks
for simulation descriptions.

node
----

Node elements can represent processes or hardware components within a
simulation. From YML's perspective it does not matter what the node
represents. It is up to the specific simulator to interpret the exact
meaning. Node elements must have a `name` attribute and optionally a
`class` attribute. The `class` attribute is most often used by
simulators to designate the specific type of node. For example with
PNRunner, the process network application simulator, the `class`
attribute defines nodes to be C, C++, or Java process. Node elements
can contain property, doc, and port elements.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~html
<node name="process0" class="CPPProcess">
</node>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

port
----

Port elements provide connection points for nodes and networks. They
require both a `name` and `dir` or direction attribute. The `dir`
attribute can contain the values `in`, `out`, or `both`. If it is left
blank `both` is assumed. Port elements can contain doc and property
elements.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~html
<port name="port0" dir="in">
</port>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

link
----

Link elements connect the port of nodes and networks together. They
require `innode`, `inport`, `outnode`, `outport` attributes. Object
attributes specify a node or network with in the current network. The
special keyword `this` can be used to specify the current network as an
object. `innode` and `outnode` can never both be `this`. Port attributes
name the port within the specified object. If any of the ports or
objects do not exist libyml will report errors. Link elements can
contain doc or property elements.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<link innode="this" inport="port0" outnode="proc0" outport="port0"/>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

network
-------

Network elements can encapsulate networks of nodes and links and even
subnetworks. They provide hierarchy in simulation descriptions. Network
elements require a `name` and optionally a `class` attribute. These
attributes are used in the same manner as in node elements. Network
elements can contain network, node, link, port, property, and doc
elements.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<network name="net0" class="KahnProcessNetwork">
  <network name="net1" class="KahnProcessNetwork">
    <port name="port0" dir="out" />
    <node name="process1" class="CPPProcess">
      <port name="port0" dir="out"/>
    </node>
    <link innode="process1" inport="port0"
          outnode="this" outport="port0"/>
  </network>

  <node name="process0" class="CPPProcess">
    <port name="port0" dir="in"/>
  </node>

  <link innode="net1" inport="port0"
        outnode="process0" outport="port0"/>
</network>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

property
--------

Property elements add information to YML objects. They are often
specific to a simulator. A port for example may have a `type` property
which designates the data type used for communication. Property elements
require a `name` and `value` attribute. Properties of the same name may
be repeated. Care should be taken when choosing property names. It is
important that two applications which use the same YML file don't use
the same property names for different purposes. To help avoid this
problem properties names which cannot be known in advance should be
scoped. For example process nodes may wish to have properties naming
operations to be performed by the process. The operations could be
`add`, `sub`, `mul`. However these operations may not be know ahead of
time so the proper way to add these properties is to prefix the
operations with a scope such as `"operation:"`. Property elements can only
contain doc elements.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<property name="operation:add" value="0"/>
<property name="operation:sub" value="1"/>
<property name="operation:mul" value="2"/>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

doc
---

Doc elements can contain documentation about a YML object. They can not
contain any other elements.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<doc>Some documentation</doc>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Object Naming Conventions
-------------------------

Object names can contain any printable character other than `'.'`. However
white space and characters special to languages such as C or Perl should
be avoided. For example `node-2` could be mistakenly interpreted as the
expression `node - 2`. Recommended practice is to use names which would
also make valid C variables. The `'.'` is used in full names to separate
parents from children. It is often useful to have unique names for YML
objects. libyml provides support for this by concatenating all the
parents starting from the top most down to the child separated by `'.'`'s.
For example `net0.net1.node0.outport` is the full name of `outport` of
`node0` of `net1` of `net0` where `net0` is the top most network in the model
description. Finally names starting with `'$'` will be evaluated as script
variables. See the next section.

Scripting
=========

The script element can be used to make dynamic YML. Currently only Larry
Wall's Perl is supported as a scripting language, but any scripting
language for which a libYML `Interpreter` interface has been written can
be used within YML.
<!-- See the *libYML Users Guide* for more information. -->
The script element takes no attributes. The text within a script element
will be processed by the Interpreter in the order it appears in the YML
file. Attributes which begin with a `'$'` will be evaluated as variables
within the current context of the interpreter. Currently we have not
found a good method for providing scope within Perl so users must be
aware that all variables are global. This is especially dangerous when
including external entities which contain script as it is not
immediately obvious which variables they modify.

Sets
====

Set elements further simplify the description of complex network
structures by providing a for loop like definition of sets of data. The
set element takes three attributes `init`, `cond`, and `loop` which are
interpreted as script code. `init` is evaluated once at the beginning of
set processing. `cond` is evaluated once at the beginning of each pass
of set processing. The value of `cond` is interpreted as a boolean
value. When it is false or 0 set processing stops. Finally `loop` is
processed once at the end of each pass of set processing.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<set init="$i = 0" cond="$i &lt; 5" loop="$i++">
  <script>
    $nodename="node$i";
   < /script>
  <node name="$nodename" class="">
    <port name="port0" dir="out"/>
  </node>
</set>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note: The space in the closing script tag is a formatting
artifact]

When processed this example will create five nodes named `node0` through
node4 each with a port named `port0`.
Set elements cannot be used to generate nodes that contain templates or
template mappings. Templates are needed to map an application task or
channel onto a component, so set elements cannot be used for such nodes.

Parameterizing YML
==================

It is useful to use YML's scripting features to parameterize YML files.
Both PNRunner and YMLPearl support passing of variables from the command
line to the interpreter via the YML preprocessor (`ymlpp`). Here is an
example of using this feature to command line parameterize a PNRunner
process from the MJPEG case study.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<?xml version="1.0" encoding="UTF-8"?>
<network xmlns="http://sesamesim.sourceforge.net/YML" name="application" class="KPN">
  <node name="Control" class="CPP_Process">
    <property name="class" value="Control"/>
    . . .
    <script>
      <!-- These lines give default values for min and max threshold -->
      if (!(defined $min_threshold)) {$min_threshold = 0;}
      if (!(defined $max_threshold)) {$max_threshold = 2;}
      printf "\n";
      printf "Control parameters: min_threshold=$min_threshold,\n";
      printf " max_threshold=$max_threshold\n";
    < /script>
    <property name="arg" value="$min_threshold"/>
    <property name="arg" value="$max_threshold"/>
    . . .
  </node>
  . . .
</network>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The above example gives the variables `min_threshold` and
`max_threshold` default values of 0 and 2 respectively. When the script
is processed without the `-q` option the current value of the parameters
are displayed by the printf statements. The values of these variables
are passed on construction to the `Control` process. Executing the
following command will set `min_threshold` to 1 and run the MJPEG
case study application.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PNRunner mjpeg_app.yml min_threshold=1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Including external YML files
============================

A large YML file can be split into multiple modules using the XInclude
mechanism. The following example shows how a producer application node
is included into an application model.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<?xml version="1.0" encoding="UTF-8"?>
<network xmlns="http://sesamesim.sourceforge.net/YML"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         name="application" class="KPN">
    . . .
    <script>
        $some_producer_var = "A";
    < /script>
    <xi:include href="producer.yml"/>
    . . .
</network>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To include the YML code into the main YML file run it through a XInclude processor such as `xmllint`.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
xmllint --xinclude simple_app_with_xincludes.yml > simple_app.yml
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the above example `xmllint` replaces the `xi:include` element with
the contents of the file `producer.yml`.  If `some_producer_var` is
used as the name or class of a node the YML file needs to be
preprocessed with ymlpp to replace `some_producer_var` with `"A"`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ymlpp simple_app.yml > simple_app.pp.yml
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If the variable `some_producer_var` is used in a property element PNRunner
will replace it before it will run the application model.

Mapping
=======

The mapping layer in YML (YMLMap) enables users to associate the `node`,
`network` and `port` elements of one simulation to those of another. We
use this to map events, such as read, write and execute, of an
application simulation onto an architecture simulation. YMLMaps do not
name specific files and can therefore be used in other mappings by
inclusion with XInclude statements. XInclude is an XML standard see
http://www.w3.org/TR/xinclude/ for more information.

YMLMap is defined by the `YML_map.xsd` XML schema. Four XML elements are
used to describe mappings: `mapping`, `map`, `port` and `instruction`.
YMLMaps map a source simulation to a destination simulation. These are
referred to as `source` and `dest`.

mapping
-------

The `mapping` element is used to define the mapping context. It requires
two attributes `side` and `name`. The `side` attribute can have either
the value `dest` or `source`. The `name` attribute refers to a `network`
within the current mapping context on the specified side. `mapping`
elements may contain any of the other YMLMap elements.

The mapping context defines where in the simulation descriptions `name`
attributes refer. There are really two contexts to keep track of. The
source context and the destination context. The initial mapping contexts
are above the root node in both of the simulations.

In the following example descendants of `mapping` will refer to
descendants of network `A` on the `source` side.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<mapping side="source" name="A">
    . . .
</mapping>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

map
---

The `map` element maps simulation nodes. The attributes `source` and
`dest` refer to YML `node`s in the source and destination simulations
respectively. This element like `mapping`s sets the context for its
descendants. In this case the context is set by the referenced nodes.
`map` elements may only contain `port` or `instruction` elements.

This example maps the node `A` in the source simulation to node `X` in
the destination simulation in the current mapping context.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<map source="A" dest="X">
  . . .
</map>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

port
----

The `port` element maps ports between simulations. The attributes
`source` and `dest` refer to ports of the source and destination
simulations within the current mapping context. This element may not
contain any children.

This example maps the port `A` in the source simulation to port `X` in
the destination simulation with in the current mapping context.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<script type="preformatted">
<port source="A" dest="X"/>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<!-- instruction mapping was disabled with the move to python ymlprocessors
instruction
-----------

The `instruction` element associates events on one side of the
simulation to those on the other by name. The attributes `source` and
`dest` refer to the string names of execution events. `instruction`
elements which are direct children of `mapping` elements are available
to all descendants of that `mapping` element.

This example maps the instruction `ADD` in the source simulation to
instruction `PLUS` in the destination simulation for both child maps
`A` to `X` and `B` to `Y`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 <script type="preformatted">
    <mapping side="source" name="A">
      <map source="A" dest="X">
      </map>
      <map source="B" dest="Y">
      </map>
      <instruction source="ADD" dest="PLUS"/>
    </mapping>
 </script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-->


<style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="../config/markdeep.min.js"></script><script src="../config/markdeep.min.js?"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
