**YMLEditor Users Guide**
<!--
update:
Joseph Coffland
jcofflan@users.sourceforge.net
April 5, 2006
-->


<!--
Copyright 2004
University of Amsterdam. Permission is granted to copy, distribute
and/or modify this document under the terms of the GNU Free
Documentation License, Version 1.2 or any later version published by
the Free Software Foundation; with no Invariant Sections, no
Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is
included with this documentation, GNU Free Documentation License.
-->


A Brief Overview of Sesame and the YMLEditor
============================================

The YMLEditor is an integrated development environment for the Sesame
simulation system.  Sesame employs event driven cosimulation to
simulate embedded systems.  Sesame follows the Y-Chart methodology for
embedded system development.  Y-Chart recognizes distinct application,
architecture and virtual layers within an embedded system simulation.
This means applications are simulated separately from the
architecture. Applications generate event traces which describe the
actions of the running application. These event traces are mapped on
to an architecture. The virtual layer is also sometimes referred to
as the synchronization layer because it synchronizes event traces of
different application processes before they reach the architecture.
However, it is also responsible for scheduling and trace rewriting.
The architecture simulation responds to application events by
simulating their timing consequences. When the simulation is finished
the user can analyze the measured simulation results and use this
information to improved the application simulation, the architecture
simulation, the mapping or all three. Thus the user runs through
iterations of simulating and refining the simulation models until
satisfied with the embedded systems performance.  It allows you to
create new Sesame projects, edit the structure of application and
architecture simulations and their source code and generate default
templates for new application and architecture classes. The YMLEditor
also allows you to edit the mapping between the application and
architecture, automatically generate the virtual layer based on the
supplied mapping and architecture templates and build and run Sesame
simulations from the convenience of a single application.  The
remainder of this document describes how to use the YMLEditor to
accomplish these tasks. Starting from the Project Management section
this documentation will follow a simple example co-simulation using
the YMLEditor.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
Notes will be displayed in a box such as this one. These notes
provide additional information about the usage of the YMLEditor and
may explain some of the nuances of the interface. A "Technical Note"
is used to explain details not necessary for normal use of the
YMLEditor. Technical notes can be safely ignored by most users.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

Installing and Running
======================

If you are reading this documentation from the YMLEditor's online help
system then you can safely skip this section.

System Requirements
-------------------

- The script `ymleditor` used to run the YMLEditor performs various tasks
  such as creating new projects and opening external files and
  requires the bash command shell.
- The YMLEditor requires Java version 1.4.2 or better. In order
  for the YMLEditor to find Java the `java` executable must either be
  on your `PATH` or the `JAVA_HOME` environment variable must be set to
  the root directory of your Java installation. If `java` is both on your
  `PATH` and `JAVA_HOME` is set, `JAVA_HOME` will take priority.
- In order to edit source code within the YMLEditor `nedit` (optional) is
  required. `nedit-nc` or `nc` must be on your path. `nedit` can be
  installed from your package manager of from [sourceforge][nedit]. If you
  would like to use a different editor you can modify the `ymleditor`
  script.
- For graph auto-routing to work you must install GraphViz (optional).
  The dot executable must be on your `PATH`. Install GraphViz with your
  package manager or from the [Graphviz website][graphviz].
- The YMLEditor needs the `sesamesim-config` from the Sesame
  installation. This program tells the ymleditor where Sesame is
  installed.

Installing
----------

Once the requirements of the previous section have been met you are
ready to begin the installation. The YMLEditor distribution can be
found [here][ymleditor].  To install the YMLEditor simply untar and
uncompress the distribution. This can be accomplished with the
following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
tar xf ymleditor-x.x.x.tar.gz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This will create a directory called `ymleditor-x.x.x`, where `x.x.x` is
the version of the ymleditor you are installing.

Running
-------

Once you have installed the YMLEditor and all of its requirements you
may run the program by calling the script `ymleditor` which is in the
root directory of the YMLEditor installation.

Exiting
-------

You can exit the YMLEditor either by closing the main window or by
selecting Exit from the Project menu. Make sure you first save your
work.

Project Management
==================

In order to do anything with the YMLEditor you must first either
create a project or open an existing one. The following sections will
show you how to do this. The project management controls can all be
found under the project menu.

Creating a New Project
----------------------

You can create a new project by selecting `New` from the `Project` menu.
An open file dialog will appear. Navigate the file dialog in order to
locate the directory where you would like to create your project. Type
the name of your new project in the `File Name` text box and click the
`Open` button. The project name cannot be `template` as this name is used
by the YMLEditor.

![Creating a new project](new_project.png)

This will create the following files:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
example/
example/app
example/app/Makefile
example/app/example_userdefs.h
example/arch
example/arch/Makefile
example/arch/example.pi
example/arch/example.ps
example/Makefile
example/example.yml
example/example_app.yml
example/example_map.yml
example/example_arch.yml
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
The YMLEditor creates a new project by executing the
following command:

ymleditor new project_name

Where project_name is the actual name provided by the user. The
ymleditor script creates a new project by untaring the file
`template.tar` and replacing all instances of the word 'template' in the
filenames and in the files themselves. You can change the default new
project by changing the files in 'template.tar'.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Technical Note]

Saving, Closing, and Opening Projects
-------------------------------------

You can save a project by selecting `Save` from the `Project` menu. This
will write any changes you have made to the project to disk. To close a
project select `Close` from the Project menu.

To open an existing project select `Open` from the `Project` menu. An open
file dialog will appear. Select the Sesame project YML file and click
`open`. The project YML file is the XML file whose top level element is
named `simulation`.  In our example this file is named `example.yml`.

The Graph Editor
================

Once you have created a new project or opened an existing one you can
start editing the application and architecture simulation structure
using the graph editor. This section explains the menus and mouse
actions used to operate the graph editor.

Parts of the Graph Editor
-------------------------

In the picture below, the parts of the graph editor are labeled with
numbers.  Each part is referenced by its number and explained below.

![The graph editor](graph_editor.png)

1. simulation layer: There is a graph editor for each of the simulation
   layers. The name of the simulation layer is displayed here.
2. location bar: The location bar indicates where the displayed
   network resides in the graph hierarchy. It also displays an
   identifier for the currently selected graph object.
3. mouse coordinate: The mouse coordinate displays the last position of
   the mouse within the graph editor.
4. graph scale: The scale dialog indicates the current graph scale. The
   graph scale can be changed though this dialog. Clicking on the
   arrows will increase or decrease the graph scale. Entering a
   numerical value will set the scale.
5. graph origin: The cross-hair in the middle of the graph indicates
   coordinate (0,0).
6. scroll bars: The scroll bars allow you to change the position of
   the graph view port.
7. parent network: The parent network is represented by the blue box
   which encompasses the current graph. When you first create a new
   project you will see only a blue box in the center. This box
   represents the root network. In the beginning it will be empty. As
   nodes are added to the network this box will automatically change
   its shape and position so that it always marks the outer bounds of
   the network.
8. node: Nodes represent simulation components. They can either be
   displayed as an ellipse or a rectangle with image. The name of the
   node is shown either under the image or in the center of the
   ellipse.
9. sub-network: Sub-networks are blue instead of black like nodes.
   Subnetworks can contain other components inside. Other than this
   difference sub-networks can be treated like nodes.
10. port: Ports are the communication points of nodes and networks.
11. link: Links symbolize communication channels between simulation
   components.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none 
With a three button mouse, middle clicking anywhere on the graph
editor will cause it to scale the current graph to fit and snap the
view port to the nearest part of the graph.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

Creating Objects
----------------

### Nodes

Graph nodes are used to represent simulation components. Nodes appear
as black ovals with the node name in the center. If a node is selected
it will appear red instead of black. If a node has an associated image
it will appear rectangular instead of oval and its name will appear
under the image.  You can create a new node by right clicking anywhere
in the graph editor.  This will bring up a popup menu which looks like
this:

![Right click pop-up menu](graph_popup.png)

Select the `New Node` option and the dialog shown below will appear.

![New node window](new_node.png)

Enter the `Name` of the new node. Valid names meet the following conditions:

- A new name cannot be the same as another node or sub-network name
  within the current network.
- A new name cannot be empty or only contain white-space characters.
  White-space characters are automatically trimmed from the front and
  back of all names.
- A new name cannot be `this`, because `this` is a reserved name used in
  YML to refer to the current network itself.

You may also set the node `Class`. The `Class` is used by the specific
simulator to load the simulation component's code at run-time. If the
node `Class` is not set it is assumed to be the same as the node `Name`.
If the corresponding class does not already exist one will be
created. For applications this will be a C++ class with `.h` and
`.cpp` files. For architecture and virtual layer components the class
files are `.ps` and `.pi` files.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
The applications PNRunnerYMLTool and PearlYMLTool are
responsible for creating node templates. See the
--create-node-template option. Also notice that in the case of the
application layer the --create-base-classes option is used to maintain
application node base classes which contain each node's ports.
Application nodes which share the same class MUST have the same ports!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

Finally, you can attach an image to the node by clicking on the
button, which initially says `No Image`. Select the `Set Image` option
from the buttons popup menu. This will open a file dialog from which
you can choose an image file. Supported image formats are GIF, JPEG
and PNG.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
The YMLEditor now encodes image files as text using the base 64
encoding scheme described in MIME RFC2045 and copies the data directly
into the YML image property. This makes it much easier to keep image
files and YML together. You no longer need to keep a copy of the image
in the simulation project.

To extract an image from a YML file you can copy the encoded data to
its own file and decode with a base 64 decoding program such as
uudeview.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

Click the `Ok` button to create the new node. If the node name you have
entered is valid a new node will appear in the graph editor at the
position you first right clicked. The bounding box of the current
network may change shape in order to encompass the new node.

The example below shows the application graph editor with two nodes
named `A` and `B`.

![Application layer](example_a_b.png)

### Networks

Sesame utilizes hierarchical component networks. This means that a
whole network of components may be used just like a single component.
In fact a subnetwork looks just like a regular component except that
its border is colored blue instead of black when not selected. You can
"zoom in" to a sub-network by double clicking on it. This will bring
up a popup menu giving you the option `Open Subnetwork`. This allows
you to move down the network hierarchy.  To move back up right click
on the graph editor to bring up the popup menu and selected the `Up`
option. Selecting the `Up` option in the root network has no effect.
Each of the graph editors has a location bar in the top left. This
indicates where you are in the graph hierarchy. The location bar is
blank only if no project is loaded. Otherwise it displays a path to
the current network and current selected component if any.

To create a new network right click on the graph editor to bring up
the popup menu and select `New Node`. You must select the `Network` radio
button to create a network instead of a node. New network names must
meet the same requirements as new node names.

![New network dialog](new_network.png)

The example below shows two nodes with images and a subnetwork. Notice
the subnetwork is blue. Although the example does not show it,
subnetworks can also have images.

![Subnetwork example](example_subnet.png)

### Ports

Nodes and networks can have ports which define their communication
points.  Ports appear as small green boxes on the border of a node or
network. They appear red when selected.

To add a new port right click on a node or network and select `New Port`
from the popup menu. A dialog will appear like the one below:

![New port dialog](new_port.png)


The `Name` is the port's identifier and must be a unique port name
within its parent component and must not be empty. The `Direction` can
be either `In` or `Out` this indicates which direction data will travel
over this port to and from its parent component. The direction will
restrict how you can connect links to the port. The port's `Type`
indicates what kind of data is allowed to travel over this port.

Click the `Ok` button and if the new port's name is valid a new port
will appear on the selected component. The example below shows some
newly created ports.

![Application layer with ports added](example_ports.png)

In the case of the application editor (i.e. PNRunner) added ports will
be automatically added to the underlying source code via the
application processes' parent class. Note that it is up to the user to
ensure that if two nodes share the same class that they also have the
same ports. Future version of Sesame will alleviate this burden.

When you "zoom in" to a sub-network its ports will appear on the outer
edge of the network. A port may have a different position on the
network when viewed from the inside than it does from the outside of
the network.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
If you created a port and entered a valid port name and the port
did not seem to appear, it may have appeared on top of another port.
Try moving ports around to separate them.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

### Links

Links represent connections between simulation components. Links must
be attached to ports. A link is represented as a black line from one
port to another with an arrow at one end which indicates the direction
of the connection. The port connected to the arrow must be an `in` port
and the port at the other end must be an `out` port.

To connect two ports first place the mouse pointer over one of the
ports, then while holding down the shift key and the left mouse button
drag the mouse pointer over to the other port. As you are dragging a
dotted green line will appear which indicates where the link will be
made. The picture below shows a link and a link being created.

![Creating a new link between ports](new_link.png)

Selecting
---------

Selecting graph objects allows you to perform actions such as move and
delete on a group of graph objects. There are two main selection
modes. Either simple selection using mouse clicks and the control key
or by using the selection box to select groups of objects.

### Simple Selection

To select a single object simply left click on it. This will deselect
any previously selected objects. The newly selected object will turn
red indicating that it has be selected. You can select more than on
object by holding down the control key and left clicking on more
objects to add them to the selection. The control key can also be used
to remove object from the selection. Simply left click on a selected
object while holding down the control key. The picture below shows six
out of ten possible objects selected.

![Selecting multiple objects](multiple_selection.png)

### The Selection Box

The selection box can be used to quickly select several objects at
once. To use the selection box place the mouse outside of the current
network. Then hold down the left mouse button and drag the mouse. A
dashed red box will appear.  When you let go of the mouse button all
of the object which are completely enclosed in the selection box will
be selected. Any object not in the selection box will be deselected.

Holding down the control key while dragging the selection box allows
you to add objects to the existing selection. Unlike simple selection
the control key will not remove previously selected object from the
selection.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
The default action when you left click and drag within the current
network is to move the network. Holding down the control key makes the
selection box the default action.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

The examples below show the selection box and the resulting selection.

![Selecting objects with a selection box](selection_box.png)

![Resulting selection](selection_box.png)

Cut, Copy and Paste Support
---------------------------

As a convenience the YMLEditor provides `Cut`, `Copy` and `Paste` functions.
These options are available via the graph editor's popup menu. `Copy`
will send a copy of the currently selected elements to the clipboard.
`Cut` also copies the current selection to the clipboard, but also
removes the selection from the graph.  `Paste` will insert the contents
of the clipboard into the graph at the position the popup menu was
accessed. Note that you may copy nodes from one graph and paste them
into another and the associated source files will be copied into the
directory associated with the new graph. Pasted graph elements will be
renamed as necessary.

Editing Objects
---------------

Any object in the graph editor can be edited by double clicking on the
object.  An appropriate dialog will appear for the selected object. If
you are satisfied with your changes click the `Ok` button and the
changes will be reflected in the graph. Below is an example of the
`Node Editor`.

![Node editor window](edit_node.png)

Properties
----------

Each graph object can have any number of properties. These properties
have different meanings depending on the application looking at the
resulting YML.  You will notice the `pos` property. This is added by the
YMLEditor itself and keeps track of the object's position in graph
layout.

### Editing

You can edit an existing property either by double clicking on the
property or right clicking on the property frame and selecting the
`Edit Property` option from the popup menu. The `Edit Property` dialog,
shown below, will appear.

![Edit Property dialog](edit_property.png)

### Deleting

You can delete an property in one of two ways. Either by selecting the
property and pressing the delete key or by right clicking on the
property and selecting `Delete Property` from the popup menu.

### Creating

To create a new property right click on the property frame and select
`New Property` from the popup menu. This will open the `New Property`
dialog.  When you are satisfied with the property's name and value
click the `Ok` button to create the property. The new property will
appear in the list.

### Source Code

You can edit the source code for nodes or networks from within the
YMLEditor as long as the source is listed as either a `header` or `source`
property. A node's popup-menu will contain an `Edit Source` sub-menu if
these properties exist.  Selecting an item from this menu will open
the source file in the YMLEditor's text edit. `nedit` by default.
Saving, building, running or closing the project will automatically
save any changes made in the source code editor.

### Deleting

Object can be deleted either by selecting one or more object and
pressing the delete key or by right clicking on an object and
selecting the `Delete` option from the popup menu. Deleting an object
may cause other object to be deleted if they depend on the deleted
object. Deleting a port will also delete any links attached to it.
Deleting a node will also delete its ports and any links attached to
them. Deleting a network will delete its ports, any links attached to
them and everything inside the network.

Moving Objects
--------------

You can move a single object by placing the mouse pointer over the
object, holding down the left mouse button and dragging the object to
its desired position. To move multiple object select the desired
objects, place the mouse pointer over one of the selected objects, then
while holding down the left mouse button drag to the desired position.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
You may notice that when moving multiple objects, when you first press
the left mouse button your selection seems to disappear and then
reappear when you drag the mouse. This is because the editor is not
yet sure if you are just clicking or dragging. Clicking deselects all
other object and selects the one you clicked on.  Dragging moves the
selected group.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

Object movement is not completely free. The object follow certain
rules described below.

### Nodes

Nodes can be move freely, but will also move the ports and links
attached to them. Moving a node can also cause its parent network to
change shape.

### Networks

Moving a network will move all the object inside the network along with it.

### Ports

Port movement is restricted to the outer edge of the port's parent.
Moving a port will also move any links attached to it.

### Links

Cannot be move on their own unless they have control points. Then all
the control points will be moved together. See
section [Manually Routing Links] for more information.

Graph Routing
-------------

Graph routing is the process of setting the position of the graphs
components, either manually or with an automatic router.

### Edge Controls

A new link has only two control points. They are the ports at either
end. This only allows for links as straight lines. To enable routing
links around other objects the path of a link can be described by a
series of bezier curves. Bezier curves have control points which
define the path of the curve. In the YMLEditor these control points
are called edge controls. By default edge controls are invisible. To
make them visible you can right click anywhere on the graph editor to
bring up the popup menu and make sure the `Show Edge Controls` box is
checked. If there are any edge controls they will appear as two small
boxes connected by a dashed line. Place the mouse pointer over an edge
control and drag the mouse while holding down the left mouse button to
move the edge control. This will allow you to manipulate the bezier
curve. The picture below show an example of a link with edge controls.

![Using edge controls](edge_controls.png)

### Manually Routing Links

You can manually route the path of a link by adding bezier curves and
moving edge controls. To add a bezier curve to a link double click on
the link while holding down the control key. The first time this is
done the link will be changed from a simple line to a bezier curve.
Subsequent additions will add bezier curve sections. If `Show Edge
Controls` is turned on two new boxes connected by a dashed line will
appear when you add edge controls. You can remove a section of edge
controls by holding down the shift key and double clicking on one of
the edge controls in a pair.

### Auto-Routing

Graph routing by hand can be tedious especially with large graphs. To
autoroute the current graph right click on the graph editor and select
`Auto Route Graph` from the popup menu. If the GraphViz program `dot` was
found at startup the graph will be routed. See the
System Requirements section for more about this requirement.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
If after auto-routing you would like to manually route links, all the
extra edge controls added by the auto-router can become a nuisance.
Right clicking on the graph editor and selecting Remove Edge Controls
from the popup menu will delete all edge controls in the current
graph. This will turn all the edges into straight lines.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Note]

Graph Tree View
---------------

On the left side of each graph editor is a tree view of the same graph
data. This editor may be closed at program startup and can be opened
by holding down the left mouse button and dragging the panel divider
to the right. Many of the same operations can be performed via the
tree views popup menu which is accessed with a right mouse click.
Items in the tree view support drag and drop copying. To perform this
action place the mouse pointer over an element in the graph tree view,
hold down the left mouse button, drag the item to the desired location
and release the mouse. The item will be copied to the new location in
a manner similar to the functions described in the
Cut, Copy and Paste Support section.

![Tree view of a model](graph_tree_view.png)

The View Menu
=============

The view menu, shown below allows you to arrange the various graph
editors.

![View menu](view_menu.png)

The first two options select either vertical or horizontal
arrangement. The remaining options either show or hide the various
graph editors. The tool bar also provides several short cuts for commonly
used layouts.

Architecture Templates
======================
<!-- yml generators should be links to their documentation -->

Architecture templates are used by the Virtual Layer Generator to
create the virtual layer. Each architecture component may have one or
more templates associated with it. Mapping an application component
onto an architecture component tells the Virtual Layer Generator to
use its template for generating that part of the virtual layer.

![Architecture templates](templates_popup.png)

The image above shows the template editing functions available from
the architecture layer editor's popup menu. This menu can be reached
by right clicking on a node or network in the architecture layer. From
this menu architecture templates can be created, deleted and selected
for editing.

The `Create Processor Template` and `Create Channel Template` options
provide two different default templates. Either one may be used, but
the Virtual Layer Generator treats channel mappings and process
mappings differently.  The Virtual Layer Generator will connect the
`read` and `write` ports of channel templates to the `channels` port of the
processor templates. These two menu options automatically create these
ports.

In order to view and edit templates the Template Layer graph editor
must be viewable. It can be selected via the `View` menu. Clicking on an
architecture component which has at least one template will display
this template in the Template Layer graph editor. If the architecture
component has more than one template the one displayed in the Template
Layer graph editor can be selected via the `Edit Template` submenu. The
YMLEditor will remember this selection via the `defaulttemplate`
property.

Templates can be edited via the graph editor in the normal way. To be
truly useful a template must be mapped on to the architecture. This
mapping specifies how the virtual layer will be connected to the
architecture after it is generated from the templates.
The Mapping Editor section describes mapping.

The Mapping Editor
==================

The mapping editor is used to create mappings between components in
the different simulation layers of Sesame. There are two types of
mapping which differ slightly. Application layer to architecture layer
mappings map nodes and channels in the application layer to
architecture templates. This type of mapping is viewable under the
`Application Mapping` tab of the YMLEditor.  The second type of mapping
maps ports from either the template layer or virtual layer on nodes in
the architecture layer. The tabs `Virtual Mapping` and `Template Mapping`
display and provide the editing features for these layers.

Initially there are no mappings. A screen shot of the mapping editor
is shown below:

![Mapping editor](mapping_editor.png)

Application Mapping
-------------------

Mapping an application process to an architecture component tells the
simulator to run the application process on the target architecture
component during simulation. Likewise a channel mapping indicates that
communication over the specified application channel should be
simulated on the specified architecture communication path. The current
version of Sesame maps application processes and channels onto
architecture templates. These templates are then used to generate the
virtual layer which does the actual application to architecture
mapping at simulation time.

To create a new mapping right click on the `Application Mapping` list
and select `Create New Map` from the popup menu. If there are mappable
nodes or channels this will open a dialog which displays possible
mappings. Select the source and destination from the drop down menus.
An application node or channel can only be mapped on to one
architecture template. However, more than one application node or
channel can be mapped on to the same architecture template.

The mapping editors will only show mappings between the currently
displayed networks. In order to map nodes in sub-networks or parent
networks you must first open these networks by double clicking or
selecting `Up` option from the popup menu.

Deleting application mappings will also delete the corresponding
virtual layer nodes. This loses any changes made directly to the
virtual layer. For this reasons is recommended to make changes to
architecture templates when at all possible.  Otherwise save a copy
of the virtual layer before making any mapping changes.  This feature,
however, makes it much easier to change application mappings and rerun
the simulation reflecting these changes because the virtual layer is
automatically corrected by the Virtual Layer Generator.

Virtual Layer and Template Mapping
----------------------------------

Virtual and template layer mapping works similarly except rather than
nodes and channels being mapped to architecture templates, ports are
mapped on to architecture nodes. For virtual and template mappings to
be viewable and editable the networks to which they apply must be
loaded in their respective graph viewers.

For the most part the YMLEditor keeps mappings synchronized with name
changes and deletions in the graph editors. However the current
implementation of the YMLEditor does not update virtual and template
mappings when they are not active, i.e. loaded into the graph editor.
This means that name changes and deletions in the architecture layer
may invalidate virtual and template layer mappings. In such case these
invalid mappings must be deleted and recreated.  This is a known bug
in the YMLEditor and will be corrected in a future release.

Virtual and template layer mappings are stored in mapping YML
properties. In the virtual layer there is only one mapping property
per node.  Architecture templates may contain more than one mapping.
Mappings are differentiated by a name. This name is stored in the
property value attribute.  This name is used to identify the matching
template property. This is demonstrated in the example below.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
<property name="template" value="A">
. . .
</property>
<property name="template" value="B">
. . .
</property>

<property name="mapping" value="A">
. . .
</property>
<property name="mapping" value="B">
. . .
</property>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the example there are two templates A and B with corresponding
mappings.

Visualizing Mappings
--------------------

To visualize mappings click on the mapping in one of the mapping
editors. If the source and destination components or ports are visible
in the application, architecture, virtual or template layer graph
editors they will be selected and highlighted red. The example below
demonstrates this:

![Visualizing mappings](mapping_selected.png)

Library Support
===============

The library editor allows you to create and use libraries of Sesame
simulation components. Cut, Copy, Paste, Drag and Drop are all
supported by the library editor. See
the Cut, Copy and Paste Support section
and the Graph Tree View section for more information on these
features.

The library editor creates XML files which contain, YML data,
component source code and images all in one file. When a component is
copied to a library its source code, which is specified through the
`header` and `source` properties, is copied into the library file. Copying
from a library to a simulation will write the source code into the
simulation directory. The simulation directory is the same directory
where the simulation's YML file is located. However, files will not be
overwritten, so you must take care that source code files that differ
have unique names. Also, if a simulation component depends on some
file which is not listed in either a header or source property that
file will not be copied into the library.

You can open, close, save and create new libraries via the `Library`
menu.

![Component library](library.png)

The Build Menu
==============

The build menu allows you to call `make` commands from the YMLEditor
in order to build and run simulations. `Build All` will call `make` in
the project's root directory. `Clean All`, `Generate YML`, `Clean YML`
and `Run` will call the Makefile targets `clean`, `yml`, `clean yml`
and `run` respectively. A default Makefile that will handle each of
these make targets is included with each new project.  Executing `Run`
will `Build All` and `Generate YML` first if necessary and then run your
simulation.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
The YMLEditor actually calls the ymleditor script and executes the
commands buildall, cleanall, genyml, cleanyml and runsim.  You could
make the YMLEditor work with a different project build system by
changing this script. You can also execute ymleditor help to get more
information about the command line options to the ymleditor script.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Technical Note]

[nedit]: https://sourceforge.net/projects/nedit
[graphviz]: http://www.graphviz.org
[ymleditor]: https://csa.science.uva.nl/download/sesame/ymleditor/


<style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="../config/markdeep.min.js"></script><script src="../config/markdeep.min.js?"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
