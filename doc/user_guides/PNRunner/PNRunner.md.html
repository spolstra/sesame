**PNRunner Users Guide**
<!--
update:
Joseph Coffland
jcofflan@users.sourceforge.net
April 5, 2006
-->

<!-- examples need to be updated: PNRunner now uses
    Base classes and constructor is expected to have type:
    A::A(Id, A_Ports*)
-->

Introduction
============
PNRunner stands for Process Network Runner and is the application
simulator used in Sesame. PNRunner implements Kahn Process Networks
(KPN) in C++. KPNs are networks of processes which communicate with
each other via uni-directional and theoretically infinite
communication channels. Kahn processes block on reads until data is
available. In practice Kahn processes may also block on writes due to
the space limitations of real computers. A nice feature of KPNs is
that their execution is deterministic for a given input.  PNRunner
also provides an API for creating KPNs and for the generation of trace
events. Traces events are read, write or execute events produced by a
running KPN. Communication events are automatically generated when the
KPN read and write functions are called.  Computation events must be
added to the application code by the model creator.

Command Line Usage
==================
See the `PNRunner(1) man page`. It is also necessary to use
`PNRunnerYMLTool(1)` in order to create stub loaders for your
application processes before running an application simulation with
PNRunner. The Makefile of the application model will take care of
this.


YML Usage
=========
`PNRunner` reads YML application descriptions files. This section
describes parts of the YML description specific to `PNRunner`. For
general YML programming see the `YML Users Guide`. YML mapping files
are also described in the `YML Users Guide`.
First off the **class** attribute of **network** elements should
always contain the value `KPN` to specify Kahn Process Network. The
**class** attribute of **node** elements should contain `CPP_Process`,
`C_Process`s, or `Java_Process` depending on the application code
type. However, `C_Process`, and `Java_Process` are not yet
implemented.


node properties
---------------

library
:   This properties value specifies the location of the shared library
    containing the C++ code for this process.  Each application node
    can have a different shared library, but normally the KPN
    network element has a library property that provides the
    code for all the classes used in the application.

class
:   This properties value specifies the C++ class name of the
    current process.

port properties
---------------

type
: This properties value specifies the C++ data type which is
  communicated over this port. This may be a base type such as int or
  double, or a class or structure. Pointer references should not be
  passed over communication channels.

An example `PNRunner` application YML file demonstrating these
properties is given below:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~html
<script type="preformatted">
<network name="Application" class="KPN">
  <node name="ProcessA" class="CPP_Process">
    <property name="class" value="A"/>
    <property name="library" value="libAClass.so"/>

    <port name="in0" dir="in">
      <property name="type" value="int"/>
    </port>
    <port name="out0" dir="out">
      <property name="type" value="struct DataBlock"/>
    </port>
  </node>
</network>
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Listing [ymlA]: Description of ProcessA]


Constructor Arguments
---------------------
It is possible to pass arguments from YML or the command line to a
`PNRunner` process via the `arg` property. `arg` properties are passed as
*string* arguments to the process in the order they appear in the YML.
The following example YML demonstrates this usage.
    
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~html
<script type="preformatted">
. . .
  <node name="ProcessA" class="CPP_Process">
    <property name="class" value="A"/>
    . . .
    <property name="arg" value="$inputfile"/>
    <property name="arg" value="$count"/>
  </node>
. . .
</script>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The example above will cause `PNRunner` to pass two arguments to the
stub loader function. The arguments are variable references that must
be set from the PNRunner command line as variable value pairs. An
example PNRunner invocation follows:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
PNRunner application.yml inputfile=input.dat count=10
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The string value of these arguments can be accessed via the
`Process::getArgv()` and `Process::getArgc()` functions. For example


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cpp
void SomeNode::main() {
  string inputfile = "default";
  int count = 0;

  if (getArgc() > 1) inputfile = getArgv()[1];
  if (getArgc() > 2) count = atoi(getArgv()[2]);

  . . .
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


C++ Programmers API
===================
This section describes the C++ interface to `PNRunner` for application
processes. Familiarity with C++ is assumed.

Creating a New Process
----------------------
A new C++ `PNRunner` class named `A` should have the
following structure:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cpp
#include "A_Base.h"

class A : public A_Base {
    public:
        A(Id n, A_Ports *ports);
        virtual ~A();

        void main();
};

A::A(Id n, A_Ports *ports) : A_Base(n, ports) {
    // Process initialization code
}

A::~A() {}

void A::main() {
    // Process execution
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Listing [template]: Starting template for a new class `A`]

The class `A` in the example above inherits functionality from its
base class `A_Base.h`.  The `PNRunnerYMLTool` generates a
convience base class for every class found in the 
application YML description. It uses a naming scheme that prefixes the class name
to every new type it generates.  The base class `A_Base` has a data
member `A_Ports` which contains all the ports specified in the
application YML.

Class `A` passes `Id` and `A_Ports` on to its base class and implements the
pure virtual function `main()`.  `PNRunnerYMLTool` generates stub
code that will call the constructor with the correct arguments.
When `PNRunner` executes the process it will first call this stub
function that calls the constructor to create a new instance. Any
initialization code should go in the constructor. After the object is
constructed `PNRunner` will call the processes `main()` function. User
code should be placed here.

The code listing [template] is a standard template that can be
generated with the following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
PNRunnerYMLTool --create-node-template <new_class_name>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Listing [pnrunnercommand]: Create template code for a new class]

The following sections explain how to write process code that can
communicate with other processes via ports and how to annotate this
code to emit trace events.

Port implementation
-------------------
A base class is constructed with a `Ports` class that contains
`InPort` and `OutPort` template classes. The template parameter
specifies the type of data that will be communicated over the port.
Only ports of the same data type can be connected. 

Ports are connected by `PNRunner` at construction time by passing
communication objects to the processes constructor. This is hidden
from the application programmer in the generated base classes and stub
functions. This makes reading and writing data simple as the following
example shows:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cpp
. . .
ProcessA::main() {
    int number;
    struct DataBlock block;
  
    while (true) {
        ports->in0.read(number);
  
        // Do something with packet1 and packet2.
        block.value = number;
  
        ports->out0.write(block);
  
        // Also, some condition should alow ProcessA to
        // eventualy break out of this loop.
    }
}
. . .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[Listing [comm]: Reading and writing data]

You may read from `InPort`s and write to `OutPort`s via the `read` and
`write` functions. Listing [comm] shows how to read and write using
the ports created from the YML application description shown in Listing [ymlA].

Passing pointers via ports is illegal. There are several
reasons for this. First, you cannot assume that processes share the
same memory space. Second, `PNRunner` makes a copy of the memory buffer
without calling constructors before placing the data in the message
queue.  Finally, processes execute concurrently and sharing memory
would cause a race condition. A direct consequence of this is that you
cannot pass classes such as the `std::string` because `std::string`
uses pointers internally. In fact if you do you will get very nasty
results.  Once created ports may be used from a processes main
function. You may not read or write ports from the constructor.

Emitting Traces
---------------
Calls to read and write will automatically emit trace events.
Computation events must be emitted explicitly with the execute
function. The example below demonstrates the execute function.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cpp
. . .
ProcessA::main() {
    int number;
    struct DataBlock block;
  
    while (true) {
        ports->in0.read(number);
  
        // Do something with packet1 and packet2.
        execute("Operation1");
        block.value = number;
  
        ports->out0.write(block);
  
        execute("Operation2");
        // Also, some condition should alow ProcessA to
        // eventualy break out of this loop.
    }
}
. . .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The labels `"Operation1"` and `"Operation2"` specify the execute type
of the trace event.  These labels must be listed as properties in the
application YML file.

Workflow
========
The workflow for writing an application model can be summarized in the following steps:

1. If starting completely from scratch, first generate the project directory
   structure with the command: `ymleditor new <projectname>`.
   The application model is located in the directory `<projectname>/app`. All the steps
   that follow should be executed from this `app/` directory.
2. Define the application model in the YML file `<projectname>_app.yml`
3. Generate the empty application template code shown in listing [template] for every class
   with the `PNRunnerYMLTool` command shown in Listing [pnrunnercommand].
4. Generate the application base classes and loader stub code with the build
   command: `make build`
5. Add the `C++` code for the desired functional behaviour in the `main()`
   function of every class to get an application model that can be
   executed with `PNRunner`.

The resulting application model can be executed by typing `make run`
from the `app/` directory or `make runapp` from the project root
directory.

<style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="../config/markdeep.min.js"></script><script src="../config/markdeep.min.js?"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
