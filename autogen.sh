libtoolize --copy --force --automake &&\
aclocal -I config/m4 &&\
autoheader &&\
autoconf &&\
automake -Wno-portability --add-missing -c
rm -f config.cache
