NAME=simple

YML=$(NAME)_app.yml
APPVIRTUAL_MAP=../$(NAME)_appvirt_map.yml

APP_LIB=lib$(NAME).so
STUB=$(NAME)_stub

NODESRCS := $(shell PNRunnerYMLTool --sources $(YML))
CLASSES := $(shell PNRunnerYMLTool --classes $(YML))
BASES := $(patsubst %,%_Base.h,$(CLASSES))
SOURCES := $(STUB).cpp $(NODESRCS)
OBJS := $(patsubst %.cpp,%.o,$(SOURCES))

CXX=g++
CFLAGS += `sesamesim-config --PNRunner-cflags` -Wall -Werror -g -g3 -gdwarf-2
LIBS += `sesamesim-config --PNRunner-libs`
APPFLAGS := $(APPFLAGS) -L . -l $(APP_LIB)

all: build

build: base_classes $(APP_LIB)

base_classes: $(YML)
	for i in $(CLASSES); do\
	  PNRunnerYMLTool --create-base-class $(YML) $$i >$${i}_Base.tmp;\
	  diff $${i}_Base.h $${i}_Base.tmp 2>/dev/null >/dev/null;\
	  if [ $$? -ne 0 ]; then mv $${i}_Base.tmp $${i}_Base.h;\
	  else rm -f $${i}_Base.tmp; fi;\
	done

$(APP_LIB): $(OBJS)
	$(CXX) $(CFLAGS) $(OBJS) $(LIBS) -o $@

$(STUB).cpp: $(YML)
	PNRunnerYMLTool --create-stubs $(YML) >$@.tmp
	diff $@ $@.tmp 2>/dev/null >/dev/null;\
	if [ $$? -ne 0 ]; then mv $@.tmp $@;\
	else rm -f $@.tmp; fi

%.o: %.cpp %.h %_Base.h
	$(CXX) -c $(CFLAGS) $<

%.o: %.cpp %.h
	$(CXX) -c $(CFLAGS) $<

%.o: %.cpp
	$(CXX) -c $(CFLAGS) $<

run: build
	PNRunner $(APPFLAGS) $(YML)

runmap: build
	PNRunner $(APPFLAGS) -m $(APPVIRTUAL_MAP) $(YML)

runtrace: build
	PNRunner $(APPFLAGS) -T ../trace -m $(APPVIRTUAL_MAP) $(YML)

clean:
	rm -rf *.o *~ \#* *.jpg core* \
	$(APP_LIB) $(STUB).cpp $(BASES)
