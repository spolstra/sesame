#include "Q.h"

Q::Q(Id n, Q_Ports *ports) : Q_Base(n, ports) {}
Q::~Q() {}

// Quantize() quantizes an input matrix and puts the output in matrix.
void Q::Quantize(int *matrix, int *qmatrix)
{
    for (int i = 0; i < BLOCKSIZE; i++) {
        if (matrix[i] > 0) {        // Rounding is different for +/- coeffs
            matrix[i] = (matrix[i] + qmatrix[i] / 2) / qmatrix[i];
        } else {
            matrix[i] = (matrix[i] - qmatrix[i] / 2) / qmatrix[i];
        }
    }
}

/*
  ZigzagMatrix() performs a zig-zag translation on the input imatrix
  and puts the output in omatrix.
*/
void Q::ZigzagMatrix(int *imatrix, int *omatrix)
{
    for (int i = 0; i < BLOCKSIZE; i++) {
        omatrix[zigzag_index[i]] = imatrix[i];
    }
}

void Q::main()
{
    TBlockData input, output;
    TQTables   LuminanceQTable, ChrominanceQTable;
    TCommand   Command;

    while (1) {
        ports->Command.read(Command);

        if (Command == NewTables) {
            ports->QTables.read(LuminanceQTable);
            ports->QTables.read(ChrominanceQTable);
        }

        for (int i = 0; i < 2; i++) {
            ports->BlockData_in.read(input);

            Quantize(input.pixel, LuminanceQTable.QCoef);
            execute("op_Quantize");
            ZigzagMatrix(input.pixel, output.pixel);

            ports->BlockData_out.write(output);
        }

        for (int j = 0; j < 2; j++) {
            ports->BlockData_in.read(input);

            Quantize(input.pixel, ChrominanceQTable.QCoef);
            execute("op_Quantize");
            ZigzagMatrix(input.pixel, output.pixel);

            ports->BlockData_out.write(output);
        }
    }
}
