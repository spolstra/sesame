#include "Video_in.h"
#include <stdlib.h>

Video_in::Video_in(Id n, Video_in_Ports *ports) :
    Video_in_Base(n, ports), filename("input/CIF"),
    xsize(352), ysize(288), nrFrames(11)
{
}

Video_in::~Video_in() {}

//mropen() opens a given filename as the input read stream.
FILE *Video_in::mropen(const char *filename)
{
    FILE *fh;

    if ((fh = fopen(filename, "rb")) == NULL) {
        cout << "Cannot read input file:" << filename << endl;
        exit(0);
    }

    return(fh);
}

void Video_in::main()
{
    if (getArgc() > 1) {
        filename = getArgv()[1];
    }

    if (getArgc() > 2) {
        xsize = atoi(getArgv()[2]);
    }

    if (getArgc() > 3) {
        ysize = atoi(getArgv()[3]);
    }

    if (getArgc() > 4) {
        nrFrames = atoi(getArgv()[4]);
    }


    TBlockData  R_block, G_block, B_block;
    THeaderInfo HeaderInfo;
    FILE *fh1, *fh2, *fh3;
    char indexString[20];

    //------------------RGB frame----------------------
    for (int t = 0; t < nrFrames; t++) {
        // read file header
        HeaderInfo.FrameSize.Ver = ysize;
        HeaderInfo.FrameSize.Hor = xsize;
        int blocksV = HeaderInfo.FrameSize.Ver / 8;
        int blocksH = HeaderInfo.FrameSize.Hor / 8;
        HeaderInfo.NumOfBlocks   = blocksV * blocksH;
        HeaderInfo.BlockType     = RGB;

        ports->HeaderInfo.write(HeaderInfo);

        // open image files
        sprintf(indexString, "%4.4d", t);

        std::string Rfile = filename + indexString + ".R";
        std::string Gfile = filename + indexString + ".G";
        std::string Bfile = filename + indexString + ".B";

        fh1 = mropen(Rfile.c_str());
        fh2 = mropen(Gfile.c_str());
        fh3 = mropen(Bfile.c_str());

        // alloc stripe memories (= 8 full-width lines)
        unsigned char *stripeMemR =
            (unsigned char *)malloc(sizeof(char) * 8 * HeaderInfo.FrameSize.Hor);
        unsigned char *stripeMemG =
            (unsigned char *)malloc(sizeof(char) * 8 * HeaderInfo.FrameSize.Hor);
        unsigned char *stripeMemB =
            (unsigned char *)malloc(sizeof(char) * 8 * HeaderInfo.FrameSize.Hor);

        int stripe_size = 8 * HeaderInfo.FrameSize.Hor;

        for (int i = 0; i < blocksV; i++) {
            // read in the stripe
            fread(stripeMemR, stripe_size, 1, fh1);
            fread(stripeMemG, stripe_size, 1, fh2);
            fread(stripeMemB, stripe_size, 1, fh3);

            // output the blocks
            for (int n = 0; n < blocksH; n++) {
                int p = 0;

                for (int k = 0; k < 8; k++) {
                    for (int l = 0; l < 8; l++) {
                        int index = (k * HeaderInfo.FrameSize.Hor) + 8 * n + l;

                        R_block.pixel[p] = stripeMemR[index];
                        G_block.pixel[p] = stripeMemG[index];
                        B_block.pixel[p] = stripeMemB[index];

                        p++;
                    }
                }

                // write the block to DMUX
                ports->BlockData.write(R_block);
                ports->BlockData.write(G_block);
                ports->BlockData.write(B_block);
            }
        }

        // dealloc stripe memories
        free(stripeMemR);
        free(stripeMemG);
        free(stripeMemB);

        // close image files
        fclose(fh1);
        fclose(fh2);
        fclose(fh3);

        execute("op_ElaborateFrame");
    }
}
