#include "DMUX.h"

DMUX::DMUX(Id n, DMUX_Ports *ports) : DMUX_Base(n, ports) {}
DMUX::~DMUX() {}

void DMUX::main()
{
    THeaderInfo HeaderInfo;
    TBlockData  BlockData;

    while (1) {
        ports->HeaderInfo.read(HeaderInfo);
        ports->FrameSize.write(HeaderInfo.FrameSize);
        ports->NumOfBlocks.write(HeaderInfo.NumOfBlocks);

        if (HeaderInfo.BlockType == RGB) {
            for (int i = 0; i < HeaderInfo.NumOfBlocks; i++) {
                ports->BlockType.write(HeaderInfo.BlockType);  // control token

                ports->BlockData.read(BlockData);               // read R block
                ports->BlockData1.write(BlockData);            // send R block

                ports->BlockData.read(BlockData);               // read G block
                ports->BlockData1.write(BlockData);            // send G block

                ports->BlockData.read(BlockData);               // read B block
                ports->BlockData1.write(BlockData);            // send B block
                execute("op_RouteData");
            }

        } else {
            for (int j = 0; j < HeaderInfo.NumOfBlocks / 2; j++) {
                ports->BlockType.write(HeaderInfo.BlockType);  // control token

                ports->BlockData.read(BlockData);               // read Y block
                ports->BlockData2.write(BlockData);            // send Y block

                ports->BlockData.read(BlockData);               // read Y block
                ports->BlockData2.write(BlockData);            // send Y block

                ports->BlockData.read(BlockData);               // read U block
                ports->BlockData2.write(BlockData);            // send U block

                ports->BlockData.read(BlockData);               // read V block
                ports->BlockData2.write(BlockData);            // send V block
                execute("op_RouteData");
            }

        }

    }

}
