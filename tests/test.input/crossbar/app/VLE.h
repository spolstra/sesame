/*******************************************************************\

       SESAME project software license

        Copyright (C) 2005 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

      For information regarding the SESAME software project see
        http://sesamesim.sourceforge.net or email
        jcofflan@users.sourceforge.net

\*******************************************************************/
#ifndef VLE_H
#define VLE_H

#include "VLE_Base.h"

class VLE : public VLE_Base
{
        int current_write_byte, write_position, byte_index;
        TBitStreamPacket current_packet;
        int LastDC_Y, LastDC_U, LastDC_V, BitRate;
        int LastDC_sY, LastDC_sU, LastDC_sV;
        int ACFrequency[257], DCFrequency[257];
        int ehufsi[257], ehufco[257];

    public:
        VLE(Id n, VLE_Ports *ports);
        virtual ~VLE();

        void FrequencyAC(int *matrix);
        void EncodeAC(int *matrix);
        void FrequencyDC(int coef, int *LastDC);
        void EncodeDC(int coef, int *LastDC);
        void EncodeHuffman(int value);
        void fputv(int n, int b);
        void UseHuffman(int *code, int *size);
        void SetFrequency(int *DC, int *AC);

        void main();
};
#endif // VLE_H
