#include "DCT.h"

DCT::DCT(Id n, DCT_Ports *ports) : DCT_Base(n, ports), DataPrecision(0) {}
DCT::~DCT() {}

// ReferenceDct() does a reference DCT on the input (matrix) and output(new matrix).
void DCT::ReferenceDct(int *matrix, int *newmatrix)
{
    int sourcematrix[BLOCKSIZE], destmatrix[BLOCKSIZE];

    // Copy (Not sure if this is necessary)
    for (int i = 0; i < BLOCKSIZE; i++) {
        sourcematrix[i] = matrix[i];
    }

    // Do DCT on rows
    for (int i = 0; i < BLOCKSIZE; i += BLOCKWIDTH) {
        ReferenceDct1D(&sourcematrix[i], &destmatrix[i]);
    }

    TransposeMatrix(destmatrix, sourcematrix);  // Transpose

    // Do DCT on columns
    for (int i = 0; i < BLOCKSIZE; i += BLOCKWIDTH) {
        ReferenceDct1D(&sourcematrix[i], &destmatrix[i]);
    }

    TransposeMatrix(destmatrix, sourcematrix);  // Transpose

    // NB: Inversion on counter
    for (int i = 0; i < BLOCKSIZE; i++) {
        newmatrix[i] = sourcematrix[i];
    }
}

/*
  ReferenceDCT1D() does a 8 point dct on an array of
  input and places the result in output.
*/
void DCT::ReferenceDct1D(int *ivect, int *ovect)
{
    int k = 0;

    for (int j = 0; j < BLOCKWIDTH; j++) {
        ovect[j] = 0;

        // 1d dct is just matrix multiply
        for (int i = 0; i < BLOCKWIDTH; i++) {
            ovect[j] += ivect[i] * DctMatrix[k++];
        }

        ovect[j] /= 100000;
    }
}

/*
  TransposeMatrix transposes a matrix and puts the
  output in newmatrix.
*/
void DCT::TransposeMatrix(int *matrix, int *newmatrix)
{
    for (int i = 0; i < BLOCKSIZE; i++) {
        newmatrix[i] = matrix[transpose_index[i]];
    }
}

/*
  PreshiftDctMatrix() subtracts 128 (2048) from all 64 elements of an 8x8
  matrix.  This results in a balanced DCT without any DC bias.
*/
void DCT::PreshiftDctMatrix(int *matrix, int shift)
{
    for (int i = 0; i < BLOCKSIZE; i++) {
        matrix[i] -= shift;
    }
}

/*
  BoundDctMatrix() clips the Dct matrix such that it is no larger than
  a 10 (1023) bit word or 14 bit word (4095).
*/
void DCT::BoundDctMatrix(int *matrix, int Bound)
{
    for (int i = 0; i < BLOCKSIZE; i++)
        if (matrix[i] < -Bound) {
            matrix[i] = -Bound;
        } else if (matrix[i] > Bound) {
            matrix[i] = Bound;
        }
}

extern void printMatrix(int *m);

void DCT::main()
{
    TBlockData input, output;
    TBlockType ChannelSelector;
    int        DCTBound, DCTShift;

    DCTBound = (DataPrecision ? 16383 : 1023);
    DCTShift = (DataPrecision ? 2048 : 128);

    while (1) {
        ports->BlockType.read(ChannelSelector);

        if (ChannelSelector == RGB) {
            ports->BlockType.read(ChannelSelector);
        }

        for (int i = 0; i < 2; i++) {
            if (ChannelSelector == RGB) {
                ports->BlockData1.read(input);
            } else {
                ports->BlockData2.read(input);
            }

            PreshiftDctMatrix(input.pixel, DCTShift);     // Shift
            ReferenceDct(input.pixel, output.pixel);      // DCT
            execute("op_DCT");
            BoundDctMatrix(output.pixel, DCTBound);       // Bound, limit

            ports->BlockData.write(output);
        }

        for (int i = 0; i < 2; i++) {
            if (ChannelSelector == RGB) {
                ports->BlockData1.read(input);
            } else {
                ports->BlockData2.read(input);
            }

            ReferenceDct(input.pixel, output.pixel);      // DCT
            execute("op_DCT");
            BoundDctMatrix(output.pixel, DCTBound);       // Bound, limit

            ports->BlockData.write(output);
        }
    }
}
