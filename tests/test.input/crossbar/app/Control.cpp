#include "Control.h"
#include <stdio.h>
#include <stdlib.h>

Control::Control(Id n, Control_Ports *ports) : Control_Base(n, ports),
    MinThreshold(0), MaxThreshold(2) {}

Control::~Control() {}

// MAX and MIN macros
#define MAX(x, y) ((x) > (y) ? x : y)
#define MIN(x, y) ((x) > (y) ? y : x)


void Control::ClearDCFrequency(int *freq)
{
    for (int i = 0; i < 12; i++) {
        freq[i] = 1;
    }

    for (int i = 12; i < 257; i++) {
        freq[i] = 0;
    }
}

void Control::ClearACFrequency(int *freq)
{
    int j;

    for (int i = 0; i < 257; i++) {
        j = i & 0x0000000f;

        if ((j == 0) || (j > 10)) {
            freq[i] = 0;
        } else {
            freq[i] = 1;
        }
    }

    freq[0] = freq[240] = 1;
}

/*
  CodeSize() is used to size up which codes are found. This part merely
  generates a series of code lengths of which any particular usage is
  determined by the order of frequency of access. Note that the code
  word associated with 0xffff has been restricted.
*/
void Control::CodeSize()
{
    int i;
    int least_value, next_least_value;
    int least_value_index, next_least_value_index;

    frequency[256] = 1;         // Add an extra code to ensure 0xffff not taken.

    // Clear Arrays
    for (i = 0; i < 257; codesize[i++] = 0);

    for (i = 0; i < 257; others[i++] = -1);

    while (1) {
        least_value = next_least_value = 0x7fffffff;  // largest word
        least_value_index = next_least_value_index = -1;

        for (i = 0; i < 257; i++) {                          // Find two smallest values
            if (frequency[i]) {
                if (frequency[i] <= least_value) {
                    next_least_value = least_value;
                    least_value = frequency[i];
                    next_least_value_index = least_value_index;
                    least_value_index = i;

                } else if (frequency[i] <= next_least_value) {
                    next_least_value = frequency[i];
                    next_least_value_index = i;
                }
            }
        }

        if (next_least_value_index == -1) {           // If only one value, finished
            break;
        }

        frequency[least_value_index] += frequency[next_least_value_index];
        frequency[next_least_value_index] = 0;
        codesize[least_value_index]++;

        while (others[least_value_index] != -1) {
            least_value_index = others[least_value_index];
            codesize[least_value_index]++;
        }

        others[least_value_index] = next_least_value_index;

        do {
            codesize[next_least_value_index]++;
        } while ((next_least_value_index = others[next_least_value_index]) != -1);
    }
}

/*
  CountBits() tabulates a histogram of the number of codes with a give
  bit-length.
*/
void Control::CountBits()
{
    // Clear Bits
    for (int i = 0; i < 36; bits[i++] = 0);

    for (int i = 256; i >= 0; i--)
        if (codesize[i]) {
            bits[codesize[i]]++;
        }
}

/*
  AdjustBits() is used to trim the Huffman code tree into 16 bit code
  words only.
*/
void Control::AdjustBits()
{
    int i, j;

    i = 32;

    while (1) {
        if (bits[i] > 0) {
            j = i - 1;

            while (!bits[--j]);  // Change from JPEG Manual

            bits[i] -= 2;       // Remove 2 of the longest hufco
            bits[i - 1]++;        // Add one hufco to its prefix
            bits[j]--;          // Remove hufco from next length
            bits[j + 1] += 2;     // to be prefix to one hufco
            // from j term and the one
            // hufco from the i (longest) term.
        } else if (--i == 16) {
            break;
        }
    }

    while (!bits[i]) {
        i--;  // If fortunate enough not to use
    }

    // any 16 bit codes, then find out
    // where last codes are.

    bits[i]--;                  // Get rid of the extra code that generated 0xffff
}

/*
  SortInput() assembles the codes in increasing order with code length.
  Since we know the bit-lengths in increasing order, they will
  correspond to the codes with decreasing frequency. This sort is O(mn),),
  not the greatest.
*/
void Control::SortInput()
{
    int i, j, p;

    for (p = 0, i = 1; i < 33; i++) {          // Designate a length in i.
        for (j = 0; j < 256; j++) {         // Find all codes with a given length.
            if (codesize[j] == i) {
                huffval[p++] = j;  // Add that value to be associated
            }

            // with the next largest code.
        }
    }
}

/*
  SizeTable() is used to associate a size with the code in increasing
  length. For example, it would be 44556677... in huffsize[].  Lastp is
  the number of codes used.
*/
void Control::SizeTable()
{
    int i, j, p;

    for (p = 0, i = 1; i < 17; i++) {
        for (j = 1; j <= bits[i]; j++) {
            huffsize[p++] = i;
        }
    }

    huffsize[p] = 0;
    lastp = p;
}

/*
  CodeTable() is used to generate the codes once the hufsizes are known.
*/
void Control::CodeTable()
{
    int p, code, size;

    p = 0;
    code = 0;
    size = huffsize[0];

    while (1) {
        do {
            huffcode[p++] = code++;
        } while ((huffsize[p] == size) && (p < 257)); // Overflow Detection

        if (!huffsize[p]) {
            break;  // All finished.
        }

        do {                                  // Shift next code to expand prefix. */
            code <<= 1;
            size++;
        } while (huffsize[p] != size);
    }
}

/*
  OrderCodes() reorders from the monotonically increasing Huffman-code
  words into an array which is indexed on the actual value represented
  by the codes.
*/
void Control::OrderCodes()
{
    int index, p;

    for (p = 0; p < 257; p++) {
        ehufco[p] = ehufsi[p] = 0;
    }

    for (p = 0; p < lastp; p++) {
        index = huffval[p];
        ehufco[index] = huffcode[p];
        ehufsi[index] = huffsize[p];
    }
}

/*
  MakeHuffman() is used to create the Huffman table from the frequency
  passed into it.
*/
void Control::MakeHuffman(int *freq)
{
    for (int i = 0; i < 257; i++) {
        frequency[i] = freq[i];
    }

    CodeSize();
    CountBits();
    AdjustBits();
    SortInput();
    SizeTable();
    CodeTable();
    OrderCodes();
}

/*
  SpecifiedHuffman() is used to create the Huffman table from the bits
  and the huffvals passed into it.
*/
void Control::SpecifiedHuffman(int *bts, int *hvls)
{
    int i;
    int accum;

    bits[0] = 0;

    for (accum = 0, i = 0; i < 16; i++) {
        accum += bts[i];
        bits[i + 1] = bts[i];  // Shift offset for internal specs.
    }

    for (i = 0; i < accum; i++) {
        huffval[i] = hvls[i];
    }

    SizeTable();
    CodeTable();
    OrderCodes();
}


/*
  SetHuffman() sets the current Huffman structure.
*/
void Control::SetHuffman(int *code, int *size)
{
    for (int i = 0; i < 257; i++) {
        size[i] = ehufsi[i];
        code[i] = ehufco[i];
    }
}

void Control::SetQTableInfo(TTablesInfo *TablesInfo, TQTables *QTab)
{
    for (int i = 0; i < BLOCKSIZE; i++) {
        TablesInfo->QTable.QCoef[i]   = QTab->QCoef[i];
    }
}

void Control::SetDCHuffTableInfo(TTablesInfo *TablesInfo, int *Bits, int *Val)
{
    int accum = 0;

    for (int j = 1; j <= 16; j++) {
        accum += Bits[j];
        TablesInfo->DCHuffBits[j - 1]  = Bits[j];
    }

    for (int k = 0; k < accum; k++) {
        TablesInfo->DCHuffVal[k]   = Val[k];
    }
}

void Control::SetACHuffTableInfo(TTablesInfo *TablesInfo, int *Bits, int *Val)
{
    int accum = 0;

    for (int j = 1; j <= 16; j++) {
        accum += Bits[j];
        TablesInfo->ACHuffBits[j - 1]  = Bits[j];
    }

    for (int k = 0; k < accum; k++) {
        TablesInfo->ACHuffVal[k] = Val[k];
    }
}

/*
  ScaleMatrix() does a matrix scale appropriate to the old Q-factor.  It
  returns the matrix created.
*/
int *Control::ScaleMatrix(int Numerator, int Denominator, int LongFlag, int *Matrix)
{
    int *Temp;
    int Limit;
    Limit = (LongFlag ? 65535 : 255);

    if (!(Temp = (int *)calloc(BLOCKSIZE, sizeof(int)))) {
        cout << "Cannot allocate space for new matrix" << endl;
        exit(0);
    }

    for (int i = 0; i < BLOCKSIZE; i++) {
        Temp[i] = (Matrix[i] * Numerator) / Denominator;

        if (Temp[i] > Limit) {
            Temp[i] = Limit;
        } else if (Temp[i] < 1) {
            Temp[i] = 1;
        }
    }

    return Temp;
}

/*
  AddFrequency() is used to combine the first set of frequencies denoted
  by the first pointer to the second set of frequencies denoted by the
  second pointer.
*/
void Control::AddFrequency(int *ptr1, int *ptr2)
{
    int i;

    for (i = 0; i < 256; i++) {
        ptr1[i] += ptr2[i];
    }

    ptr1[i] = MAX(ptr1[i], ptr2[i]);
}


void Control::main()
{
    if (getArgc() > 1) {
        MinThreshold = atoi(getArgv()[1]);
    }

    if (getArgc() > 2) {
        MaxThreshold = atoi(getArgv()[2]);
    }

    enum TTablesChangeFlag {NoChange, HuffTablesChanged, QandHuffTablesChanged};

    TNumOfBlocks NumOfBlocks;
    TQTables     LuminanceQTable, ChrominanceQTable;
    THuffTables  LuminanceHuffTables, ChrominanceHuffTables;
    TStatistics  Statistics;
    TTablesInfo  LuminanceTablesInfo, ChrominanceTablesInfo;

    TTablesChangeFlag TablesChangeFlag = QandHuffTablesChanged;

    int          CompressionRatio;
    int          *QLum, *QChrom;
    char         Counter = 0;

    for (int i = 0; i < BLOCKSIZE; i++) {
        LuminanceQTable.QCoef[i]   = LuminanceQuantization[i];    // standard Luminance quantization table
        ChrominanceQTable.QCoef[i] = ChrominanceQuantization[i];  // standard Chrominance quantization table
    }

    SetQTableInfo(&LuminanceTablesInfo, &LuminanceQTable);
    SetQTableInfo(&ChrominanceTablesInfo, &ChrominanceQTable);

    SpecifiedHuffman(LuminanceDCBits, LuminanceDCValues);          // standard LuminanceDC Huffman table
    SetHuffman(LuminanceHuffTables.DCcode, LuminanceHuffTables.DCsize);
    SetDCHuffTableInfo(&LuminanceTablesInfo, bits, huffval);

    SpecifiedHuffman(LuminanceACBits, LuminanceACValues);          // standard LuminanceAC Huffman table
    SetHuffman(LuminanceHuffTables.ACcode, LuminanceHuffTables.ACsize);
    SetACHuffTableInfo(&LuminanceTablesInfo, bits, huffval);

    SpecifiedHuffman(ChrominanceDCBits, ChrominanceDCValues);      // standard ChrominanceDC Huffman table
    SetHuffman(ChrominanceHuffTables.DCcode, ChrominanceHuffTables.DCsize);
    SetDCHuffTableInfo(&ChrominanceTablesInfo, bits, huffval);

    SpecifiedHuffman(ChrominanceACBits, ChrominanceACValues);      // standard ChrominanceAC Huffman table
    SetHuffman(ChrominanceHuffTables.ACcode, ChrominanceHuffTables.ACsize);
    SetACHuffTableInfo(&ChrominanceTablesInfo, bits, huffval);

    while (1) {
        ports->NumOfBlocks.read(NumOfBlocks);

        // clear the accumulated statistics of previous frame;
        ClearDCFrequency(LuminanceDCFrequency);
        ClearACFrequency(LuminanceACFrequency);
        ClearDCFrequency(ChrominanceDCFrequency);
        ClearACFrequency(ChrominanceACFrequency);
        BitRate = 0;

        // send information about the Q and Huff. tables used within the current frame
        ports->TablesInfo.write(LuminanceTablesInfo);
        ports->TablesInfo.write(ChrominanceTablesInfo);

        // send tables and commands for the first block of the frame
        switch (TablesChangeFlag) {
            case HuffTablesChanged:
                ports->HuffTables.write(LuminanceHuffTables);
                ports->HuffTables.write(ChrominanceHuffTables);
                ports->Command1.write(OldTables);
                ports->Command2.write(NewTables);
                break;

            case QandHuffTablesChanged:
                ports->QTables.write(LuminanceQTable);
                ports->QTables.write(ChrominanceQTable);
                ports->HuffTables.write(LuminanceHuffTables);
                ports->HuffTables.write(ChrominanceHuffTables);
                ports->Command1.write(NewTables);
                ports->Command2.write(NewTables);
                break;

            default:
                ports->Command1.write(OldTables);
                ports->Command2.write(OldTables);
                break;
        }

        // send commands for the other blocks of the frame;
        for (int i = 0; i < NumOfBlocks / 2; i++) {
            if (i != 0) {
                ports->Command1.write(OldTables);
                ports->Command2.write(OldTables);
            }

            // accumulate statistics;
            for (int i = 0; i < 2; i++) {
                ports->Statistics.read(Statistics);

                AddFrequency(LuminanceDCFrequency, Statistics.DCFrequency);
                AddFrequency(LuminanceACFrequency, Statistics.ACFrequency);
                BitRate += Statistics.BitRate;
                execute("op_AccStatistics");
            }

            for (int i = 0; i < 2; i++) {
                ports->Statistics.read(Statistics);

                AddFrequency(ChrominanceDCFrequency, Statistics.DCFrequency);
                AddFrequency(ChrominanceACFrequency, Statistics.ACFrequency);
                BitRate += Statistics.BitRate;
                execute("op_AccStatistics");
            }
        }

        ports->Command2.write(EndOfFrame);

        // make a decision for changing the tables
        CompressionRatio = (int)((3 * NumOfBlocks * BLOCKSIZE * sizeof(char) * 8) / BitRate);
        cout << "Compression ratio: " << CompressionRatio
             << " Bit rate: " << BitRate << endl;

        const int Epsilon = 1;
        bool doUpdateHuffTables = false;
        const int AverageThres = (int)((MaxThreshold + MinThreshold + 1) / 2);

        if (((CompressionRatio < MinThreshold) &&
             (((MinThreshold - CompressionRatio) > Epsilon) || Counter > 2)) ||
            ((CompressionRatio > MaxThreshold) &&
             (((CompressionRatio - MaxThreshold) > Epsilon) || Counter > 2))) {
            cout << "Updating Q and H" << endl;
            doUpdateHuffTables = true;
            Counter = 0;

            QLum   = ScaleMatrix(AverageThres, CompressionRatio, 1, LuminanceQTable.QCoef);
            QChrom = ScaleMatrix(AverageThres, CompressionRatio, 1, ChrominanceQTable.QCoef);

            for (int j = 0; j < BLOCKSIZE; j++) {
                LuminanceQTable.QCoef[j]   = QLum[j];
                ChrominanceQTable.QCoef[j] = QChrom[j];
            }

            SetQTableInfo(&LuminanceTablesInfo, &LuminanceQTable);
            SetQTableInfo(&ChrominanceTablesInfo, &ChrominanceQTable);
            free(QLum);
            free(QChrom);
            TablesChangeFlag = QandHuffTablesChanged; // Quantization and Huffman tables are changed

        } else if (CompressionRatio < MinThreshold) {
            // below thresholds, but within Epsilon
            // (when above thresholds, updating will only get further above)
            cout << "Updating H" << endl;
            ++Counter; // use counter; after 2 tries, do full update
            doUpdateHuffTables = true;
            TablesChangeFlag = HuffTablesChanged;     // Only Huffman tables are changed

        } else {
            Counter = 0;
            cout << "Not updating" << endl;
            doUpdateHuffTables = false;
            TablesChangeFlag = NoChange;  // Tables are not changed
        }

        if (doUpdateHuffTables) {
            // make Huffman tables
            MakeHuffman(LuminanceDCFrequency);
            SetHuffman(LuminanceHuffTables.DCcode, LuminanceHuffTables.DCsize);
            SetDCHuffTableInfo(&LuminanceTablesInfo, bits, huffval);

            MakeHuffman(LuminanceACFrequency);
            SetHuffman(LuminanceHuffTables.ACcode, LuminanceHuffTables.ACsize);
            SetACHuffTableInfo(&LuminanceTablesInfo, bits, huffval);

            MakeHuffman(ChrominanceDCFrequency);
            SetHuffman(ChrominanceHuffTables.DCcode, ChrominanceHuffTables.DCsize);
            SetDCHuffTableInfo(&ChrominanceTablesInfo, bits, huffval);

            MakeHuffman(ChrominanceACFrequency);
            SetHuffman(ChrominanceHuffTables.ACcode, ChrominanceHuffTables.ACsize);
            SetACHuffTableInfo(&ChrominanceTablesInfo, bits, huffval);
            execute("op_QControl");
        }
    }
}
