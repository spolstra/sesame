#include "Video_out.h"

#include "marker.h"

#include <stdlib.h>

Video_out::Video_out(Id n, Video_out_Ports *ports) :
    Video_out_Base(n, ports), filename("image")
{

    hf[0] = 2;
    hf[1] = 1;
    hf[2] = 1;
    vf[0] = 1;
    vf[1] = 1;
    vf[2] = 1;
    tq[0] = 0;
    tq[1] = 1;
    tq[2] = 1;
    ci[0] = 1;
    ci[1] = 2;
    ci[2] = 3;
    td[0] = 0;
    td[1] = 1;
    td[2] = 1;
    ta[0] = 0;
    ta[1] = 1;
    ta[2] = 1;
    SSS = 0 ;
    SSE = 63;
    SAH = 0;
    SAL = 0;
}

Video_out::~Video_out() {}

#define Zigzag(i) izigzag_index[i]
#define bputw(word) {bputc(((word)>>8)&0xff); bputc((word)&0xff);}
#define bputn(nybbleh,nybblel) {bputc(((nybbleh&0x0f)<<4)|(nybblel&0x0f));}
#define pputc(val) {bputc(val); if (val==MARKER_MARKER) bputc(0);}

//mwopen() opens a given filename as the output write stream.
FILE *Video_out::mwopen(const char *filename)
{
    FILE *fh;

    if ((fh = fopen(filename, "wb")) == NULL) {
        cout << "Cannot write output file:" << filename << endl;
        exit(0);
    }

    return(fh);
}

//mwclose() closes the output write stream.
void Video_out::mwclose(FILE *fh)
{
    fclose(fh);
}

//bputc() puts a character to the stream.
void Video_out::bputc(int c)
{
    putc(c, fh);
}

// WriteSoi() puts an SOI marker onto the stream.
void Video_out::WriteSoi()
{
    bputc(MARKER_MARKER);
    bputc(MARKER_SOI);
}

// WriteEoi() puts an EOI marker onto the stream.
void Video_out::WriteEoi()
{
    bputc(MARKER_MARKER);
    bputc(MARKER_EOI);
}

/*
  WriteJfif() puts an JFIF APP0 marker onto the stream.  This is a
  generic 1x1 aspect ratio, no thumbnail specification.
*/
void Video_out::WriteJfif()
{
    bputc(MARKER_MARKER);
    bputc(MARKER_APP);
    bputw(16);
    bputc(0x4a);
    bputc(0x46);
    bputc(0x49);
    bputc(0x46);
    bputc(0x00);
    bputc(0x01);
    bputc(0x02);   //Version 1.02
    bputc(0x00);                // No absolute DPI
    bputw(1);                   // Aspect ratio
    bputw(1);
    bputc(0x00);                // No thumbnails
    bputc(0x00);
}

// WriteSof() puts an SOF marker onto the stream.
void Video_out::WriteSof()
{
    bputc(MARKER_MARKER);
    bputc(MARKER_SOF | (0x1 & 0xf));   // Type of encoding = Extended Sequential DCT = 0xC1
    bputw((8 + 3 * GlobalNumberComponents)); // Frame header length
    bputc(0x08);                       // Sampel precision = 8-bits, 12-bits or 16-bits
    bputw(V_size);                     // Number of lines
    bputw(H_size);                     // Number of samples per line
    bputc(GlobalNumberComponents);     // Number of the components in the frame (Color reprezentation)

    for (int i = 0; i < GlobalNumberComponents; i++) {
        bputc(i + 1);                  // Store off in index
        bputn(hf[i], vf[i]);           // sub-sampling format
        bputc(tq[i]);                  // Q-table destination selector
    }
}


// WriteSos() writes a start of scan marker.
void Video_out::WriteSos()
{
    bputc(MARKER_MARKER);
    bputc(MARKER_SOS);
    bputw((6 + 2 * NumberComponents));
    bputc(NumberComponents);          // Number of components in the scan

    for (int i = 0; i < NumberComponents; i++) {
        bputc(ci[i]);                 // Scan component selector
        bputn(td[i], ta[i]);           // DC and AC table destination selectors
    }

    bputc(SSS);                       // Start of spectral or predictor selection
    bputc(SSE);                       // End of spectral selection
    bputn(SAH, SAL);                  // Successive approximation bit position high/low
}

// WriteDqt() writes out the Quantization tables
void Video_out::WriteDqt(int *QLumMatrix, int *QChromMatrix)
{
    bputc(MARKER_MARKER);
    bputc(MARKER_DQT);
    bputw((2 + 2 * (65 + 64)));
    bputc((0x10 | tq[0])); // Precision defined for big numbers

    for (int j = 0; j < 64; j++) {
        bputw(QLumMatrix[Zigzag(j)]);
    }

    bputc((0x10 | tq[1])); // Precision defined for big numbers

    for (int j = 0; j < 64; j++) {
        bputw(QChromMatrix[Zigzag(j)]);
    }
}

//WriteDht() writes out the Huffman tables
void Video_out::WriteDht(TTablesInfo *Lum, TTablesInfo *Chrom)
{
    int Start, End;

    bputc(MARKER_MARKER);
    bputc(MARKER_DHT);
    Start = ftell(fh);
    bputw(0);
    // write Luminance Huffman tables
    bputc(td[0]);
    WriteHuffman((*Lum).DCHuffBits, (*Lum).DCHuffVal);
    bputc(ta[0] | 0x10);
    WriteHuffman((*Lum).ACHuffBits, (*Lum).ACHuffVal);
    //write Chrominance Huffman tables
    bputc(td[1]);
    WriteHuffman((*Chrom).DCHuffBits, (*Chrom).DCHuffVal);
    bputc(ta[1] | 0x10);
    WriteHuffman((*Chrom).ACHuffBits, (*Chrom).ACHuffVal);

    End = ftell(fh);
    fseek(fh, Start, SEEK_SET);
    bputw((End - Start));
    fseek(fh, End, SEEK_SET);
}

void Video_out::WriteHuffman(int *bits, int *huffval)
{
    int i, accum;

    for (accum = 0, i = 0; i < 16; i++) {
        bputc(bits[i]);
        accum += bits[i];
    }

    for (i = 0; i < accum; i++) {
        bputc(huffval[i]);
    }
}


void Video_out::main()
{
    if (getArgc() > 1) {
        filename = getArgv()[1];
    }

    TFrameSize   FrameSize;
    TPacketFlag  PacketFlag;
    TTablesInfo  LuminanceInfoTables, ChrominanceInfoTables;
    TBitStreamPacket BitStreamPacket;
    char indexString[20];
    int framecount = 0;

    while (1) {
        ports->FrameSize.read(FrameSize);
        ports->TablesInfo.read(LuminanceInfoTables);
        ports->TablesInfo.read(ChrominanceInfoTables);

        sprintf(indexString, "%4.4d", framecount);
        std::string JPGfile = filename + indexString + ".jpg";
        fh = mwopen(JPGfile.c_str());
        ++framecount;

        V_size = FrameSize.Ver;
        H_size = FrameSize.Hor;
        WriteSoi();
        WriteJfif();
        WriteDqt(LuminanceInfoTables.QTable.QCoef, ChrominanceInfoTables.QTable.QCoef);
        WriteSof();
        WriteDht(&LuminanceInfoTables, &ChrominanceInfoTables);
        WriteSos();

        do {
            ports->PacketFlag.read(PacketFlag);
            ports->BitStream.read(BitStreamPacket);

            for (int i = 0; i < PACKETSIZE; i++) {
                pputc(BitStreamPacket.byte[i]);
            }
        } while (PacketFlag == NLP);

        WriteEoi();
        mwclose(fh);
        // system("gimp photo.jpg");
        execute("op_FormatCodedData");
    }
}
