/*******************************************************************\

       SESAME project software license

        Copyright (C) 2002 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

      For information regarding the SESAME software project see
        http://sesamesim.sourceforge.net or email
        jcofflan@users.sourceforge.net

\*******************************************************************/

/*
*******************************************************************
tables.h

This file contains the default huffman and quantization tables.
Also contains some other tables used in the process of compression.

*******************************************************************
*/

#ifndef TABLES_DONE
#define TABLES_DONE

extern int LuminanceQuantization[];

extern int ChrominanceQuantization[];

extern int LuminanceDCBits[];

extern int LuminanceDCValues[];

extern int ChrominanceDCBits[];

extern int ChrominanceDCValues[];

extern int LuminanceACBits[];

extern int LuminanceACValues[];

extern int ChrominanceACBits[];

extern int ChrominanceACValues[];

extern int DctMatrix[];

extern int transpose_index[];          // Is a transpose map for a 8x8 matrix

extern int zigzag_index[];             // Is zig-zag map for a 8x8 matrix

extern int izigzag_index[];

extern int lmask[];                  // This is 2^{i+1}-1

#endif

