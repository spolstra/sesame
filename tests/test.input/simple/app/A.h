#ifndef A_H
#define A_H

#include "A_Base.h"

class A : public A_Base
{
    public:
        A(Id n, A_Ports *ports);
        virtual ~A();

        void main();
};
#endif // A_H
