// This is set in arch/Makefile
// #define VERBOSE 1

#ifdef VERBOSE
#define VP(s) (printf s);
#else
#define VP(s) ;
#endif

#ifdef PANIC_IF_TOKENS
#define COND_PANIC(s) (panic(s));
#else
#define COND_PANIC(s) ;
#endif


#define assert(E, M)   (if (!(E)) { printf("*** Assertion failed: ***\n"); printf M; printf("\n"); exit(2)})

